.class public Lcom/samsung/musicplus/service/PlayerListManager;
.super Ljava/lang/Object;
.source "PlayerListManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;,
        Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;,
        Lcom/samsung/musicplus/service/PlayerListManager$OnListChangeListener;
    }
.end annotation


# static fields
.field public static final FIRST:I = 0x1

.field private static final HEX_DIGITS:[C

.field public static final LAST:I = 0x4

.field public static final NEXT:I = 0x3

.field public static final NOW:I = 0x2

.field static final REPEAT_ALL:I = 0x2

.field static final REPEAT_OFF:I = 0x0

.field static final REPEAT_ONE:I = 0x1

.field static final SHUFFLE_NONE:I = 0x0

.field static final SHUFFLE_NORMAL:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MusicListManager"


# instance fields
.field private mBaseUri:Landroid/net/Uri;

.field private final mContext:Landroid/content/Context;

.field private mFirstIndex:I

.field private mIsMusicSquareUpdaterRegistered:Z

.field private mIsRegistered:Z

.field private mKeyWord:Ljava/lang/String;

.field private mListType:I

.field private mListener:Lcom/samsung/musicplus/service/PlayerListManager$OnListChangeListener;

.field private mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

.field private mMusicSquareUpdateObserver:Landroid/database/ContentObserver;

.field private final mNonShuffleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mObserver:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

.field private mPlayList:[J

.field private mPlayListLength:I

.field private mPlayPos:I

.field private mRepeatMode:I

.field private final mShuffleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mShuffleMode:I

.field private mShufflePlayPos:I

.field private mThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 224
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/service/PlayerListManager;->HEX_DIGITS:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 83
    iput v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    .line 92
    iput v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    .line 106
    iput v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mRepeatMode:I

    .line 111
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mIsRegistered:Z

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mNonShuffleList:Ljava/util/ArrayList;

    .line 615
    iput v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    .line 1545
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mIsMusicSquareUpdaterRegistered:Z

    .line 1563
    new-instance v0, Lcom/samsung/musicplus/service/PlayerListManager$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/service/PlayerListManager$2;-><init>(Lcom/samsung/musicplus/service/PlayerListManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMusicSquareUpdateObserver:Landroid/database/ContentObserver;

    .line 157
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    .line 161
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PlayerListManager content observer"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mThread:Landroid/os/HandlerThread;

    .line 162
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/service/PlayerListManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerListManager;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->doOrganizeQueue()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/service/PlayerListManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerListManager;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->unRegisterMusicSquareUpdateObserver()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/service/PlayerListManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerListManager;
    .param p1, "x1"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerListManager;->setListType(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/service/PlayerListManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerListManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private addToList([JI)V
    .locals 9
    .param p1, "list"    # [J
    .param p2, "position"    # I

    .prologue
    const/4 v8, 0x0

    .line 1475
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-nez v3, :cond_1

    .line 1476
    const-string v3, "MusicListManager"

    const-string v4, "mPlayList is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1479
    const v3, 0x20001

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4, p1, p2}, Lcom/samsung/musicplus/service/PlayerListManager;->setList(ILjava/lang/String;[JI)V

    .line 1513
    :cond_0
    :goto_0
    return-void

    .line 1482
    :cond_1
    array-length v0, p1

    .line 1483
    .local v0, "addlen":I
    if-gez p2, :cond_2

    .line 1484
    iput v8, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    .line 1485
    const/4 p2, 0x0

    .line 1487
    :cond_2
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/2addr v3, v0

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/service/PlayerListManager;->ensurePlayListCapacity(I)V

    .line 1488
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-le p2, v3, :cond_3

    .line 1489
    iget p2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    .line 1493
    :cond_3
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    sub-int/2addr v3, p2

    add-int/lit8 v2, v3, -0x1

    .line 1494
    .local v2, "tailsize":I
    move v1, v2

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_4

    .line 1495
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    add-int v4, p2, v1

    add-int/2addr v4, v0

    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    add-int v6, p2, v1

    aget-wide v6, v5, v6

    aput-wide v6, v3, v4

    .line 1494
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 1499
    :cond_4
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v0, :cond_5

    .line 1500
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    add-int v4, p2, v1

    aget-wide v6, p1, v1

    aput-wide v6, v3, v4

    .line 1499
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1502
    :cond_5
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/2addr v3, v0

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    .line 1505
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-lt v3, p2, :cond_6

    .line 1506
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    add-int/2addr v3, v0

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 1509
    :cond_6
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 1511
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    invoke-direct {p0, v3, v8}, Lcom/samsung/musicplus/service/PlayerListManager;->createShuffleIndex(IZ)V

    goto :goto_0
.end method

.method private appendWithBaseUri(J)Landroid/net/Uri;
    .locals 5
    .param p1, "audioId"    # J

    .prologue
    .line 812
    const/4 v0, 0x0

    .line 813
    .local v0, "uri":Landroid/net/Uri;
    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    .line 814
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurrentBaseUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 816
    :cond_0
    return-object v0
.end method

.method private calculateNextPosition()Z
    .locals 6

    .prologue
    const v5, 0x20004

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 841
    sget-boolean v2, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PLAYLIST_REPEAT_US_ATT_FEATURE:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 843
    const-string v2, "MusicListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ATT_10776 calculateNextPosition() mPlayPos="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mFirstIndex="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mPlayListLength="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/lit8 v3, v3, -0x1

    if-le v2, v3, :cond_0

    .line 846
    iput v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    .line 847
    const-string v2, "MusicListManager"

    const-string v3, "ATT_10776 calculateNextPosition() mPlayListLength had been changed reset first index to 0"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    :cond_0
    sget-boolean v2, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PLAYLIST_REPEAT_US_ATT_FEATURE:Z

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v2

    if-ne v2, v5, :cond_3

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    if-lez v2, :cond_3

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    if-nez v2, :cond_3

    .line 855
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mRepeatMode:I

    if-nez v2, :cond_1

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_1

    .line 856
    const-string v1, "MusicListManager"

    const-string v2, "ATT_10776 calculateNextPosition() : REPEAT_OFF, last song was played, so stop."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    :goto_0
    return v0

    .line 859
    :cond_1
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mRepeatMode:I

    if-ne v0, v1, :cond_2

    .line 861
    const-string v0, "MusicListManager"

    const-string v2, "calculateNextPosition() : REPEAT_ONE, current song is played again."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move v0, v1

    .line 880
    goto :goto_0

    .line 863
    :cond_2
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    invoke-direct {p0, v0, v2, v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getNextPosition(IIZ)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    goto :goto_1

    .line 868
    :cond_3
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mRepeatMode:I

    if-nez v2, :cond_6

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    if-nez v2, :cond_4

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/lit8 v3, v3, -0x1

    if-eq v2, v3, :cond_5

    :cond_4
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    if-ne v2, v1, :cond_6

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_6

    .line 871
    :cond_5
    const-string v1, "MusicListManager"

    const-string v2, "calculateNextPosition() : REPEAT_OFF, last song was played, so stop."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 873
    :cond_6
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mRepeatMode:I

    if-ne v0, v1, :cond_7

    .line 875
    const-string v0, "MusicListManager"

    const-string v2, "calculateNextPosition() : REPEAT_ONE, current song is played again."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 877
    :cond_7
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    invoke-direct {p0, v0, v2, v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getNextPosition(IIZ)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    goto :goto_1
.end method

.method private changeBaseUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 289
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerListManager;->registerContentObserver(Landroid/net/Uri;)V

    .line 290
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mBaseUri:Landroid/net/Uri;

    .line 291
    return-void
.end method

.method private createShuffleIndex(IZ)V
    .locals 8
    .param p1, "position"    # I
    .param p2, "rearrange"    # Z

    .prologue
    .line 640
    monitor-enter p0

    .line 641
    :try_start_0
    const-string v5, "MusicListManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "createShuffleIndex start position:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", rearrange="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-gtz v5, :cond_1

    .line 643
    :cond_0
    monitor-exit p0

    .line 690
    :goto_0
    return-void

    .line 646
    :cond_1
    const/4 v3, -0x1

    .line 647
    .local v3, "remainCount":I
    const/4 v2, -0x1

    .line 649
    .local v2, "randomIndex":I
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 650
    .local v4, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-ge v0, v5, :cond_2

    .line 651
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 650
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 653
    :cond_2
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mNonShuffleList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 654
    const/4 v5, 0x0

    iput v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    .line 655
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 656
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 660
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    iget v6, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 666
    iget v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-ltz v5, :cond_3

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget v6, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    add-int/lit8 v6, v6, 0x1

    if-ge v5, v6, :cond_4

    .line 667
    :cond_3
    const-string v5, "MusicListManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to createShuffleIndex - tempList.size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mPlayPos: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    monitor-exit p0

    goto :goto_0

    .line 689
    .end local v0    # "i":I
    .end local v2    # "randomIndex":I
    .end local v3    # "remainCount":I
    .end local v4    # "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 671
    .restart local v0    # "i":I
    .restart local v2    # "randomIndex":I
    .restart local v3    # "remainCount":I
    .restart local v4    # "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_4
    :try_start_1
    iget v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 673
    new-instance v1, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v1, v6, v7}, Ljava/util/Random;-><init>(J)V

    .line 674
    .local v1, "rand":Ljava/util/Random;
    const/4 v0, 0x1

    :goto_2
    iget v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-ge v0, v5, :cond_6

    .line 675
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    .line 676
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 677
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 674
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 679
    :cond_5
    iget v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    sub-int v3, v5, v0

    .line 680
    invoke-virtual {v1, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 681
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 685
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 688
    :cond_6
    const-string v5, "MusicListManager"

    const-string v6, "createShuffleIndex end"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method private doOrganizeQueue()V
    .locals 6

    .prologue
    .line 478
    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListener:Lcom/samsung/musicplus/service/PlayerListManager$OnListChangeListener;

    if-eqz v4, :cond_1

    .line 479
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurAudioId()J

    move-result-wide v2

    .line 482
    .local v2, "prev":J
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->isChangedDoOrganizeQueue()Z

    move-result v1

    .line 484
    .local v1, "queueChanged":Z
    const/4 v0, 0x0

    .line 485
    .local v0, "currentSongChanged":Z
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurAudioId()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 486
    const/4 v0, 0x1

    .line 488
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListener:Lcom/samsung/musicplus/service/PlayerListManager$OnListChangeListener;

    invoke-interface {v4, v0, v1}, Lcom/samsung/musicplus/service/PlayerListManager$OnListChangeListener;->onChanged(ZZ)V

    .line 492
    .end local v0    # "currentSongChanged":Z
    .end local v1    # "queueChanged":Z
    .end local v2    # "prev":J
    :goto_0
    return-void

    .line 490
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->isChangedDoOrganizeQueue()Z

    goto :goto_0
.end method

.method private ensurePlayListCapacity(I)V
    .locals 4
    .param p1, "size"    # I

    .prologue
    const/4 v3, 0x0

    .line 1516
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    array-length v1, v1

    if-le p1, v1, :cond_2

    .line 1520
    :cond_0
    mul-int/lit8 v1, p1, 0x2

    new-array v0, v1, [J

    .line 1521
    .local v0, "newlist":[J
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-eqz v1, :cond_1

    .line 1522
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1525
    :cond_1
    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    .line 1529
    .end local v0    # "newlist":[J
    :cond_2
    return-void
.end method

.method private getNextPosition(IIZ)I
    .locals 2
    .param p1, "position"    # I
    .param p2, "shufflePosition"    # I
    .param p3, "updateShuffle"    # Z

    .prologue
    .line 727
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 728
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p2, v0, :cond_1

    .line 729
    add-int/lit8 p2, p2, 0x1

    .line 733
    :goto_0
    if-eqz p3, :cond_0

    .line 734
    iput p2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    .line 737
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 738
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 749
    :goto_1
    return p1

    .line 731
    :cond_1
    const/4 p2, 0x0

    goto :goto_0

    .line 740
    :cond_2
    const/4 p1, 0x0

    goto :goto_1

    .line 743
    :cond_3
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_4

    .line 744
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 746
    :cond_4
    const/4 p1, 0x0

    goto :goto_1
.end method

.method private getPrevPosition(IIZ)I
    .locals 2
    .param p1, "position"    # I
    .param p2, "shufflePosition"    # I
    .param p3, "updateShuffle"    # Z

    .prologue
    .line 753
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 754
    if-lez p2, :cond_1

    .line 755
    add-int/lit8 p2, p2, -0x1

    .line 759
    :goto_0
    if-eqz p3, :cond_0

    .line 760
    iput p2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    .line 763
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 764
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 775
    :goto_1
    return p1

    .line 757
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 p2, v0, -0x1

    goto :goto_0

    .line 766
    :cond_2
    const/4 p1, 0x0

    goto :goto_1

    .line 769
    :cond_3
    if-lez p1, :cond_4

    .line 770
    add-int/lit8 p1, p1, -0x1

    goto :goto_1

    .line 772
    :cond_4
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/lit8 p1, v0, -0x1

    goto :goto_1
.end method

.method private getRealCursorIndex(Landroid/content/ContentResolver;[J)[J
    .locals 12
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "list"    # [J

    .prologue
    const/4 v9, 0x0

    .line 573
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 574
    .local v11, "where":Ljava/lang/StringBuilder;
    const-string v0, "_id IN ("

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 575
    array-length v10, p2

    .line 576
    .local v10, "size":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v10, :cond_1

    .line 577
    aget-wide v0, p2, v8

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 578
    add-int/lit8 v0, v10, -0x1

    if-ge v8, v0, :cond_0

    .line 579
    const-string v0, ","

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 576
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 582
    :cond_1
    const-string v0, ")"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 588
    const/4 v6, 0x0

    .line 590
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurrentBaseUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "_id"

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 594
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 595
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v10

    .line 597
    new-array v9, v10, [J

    .line 599
    .local v9, "index":[J
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 601
    .local v7, "colidx":I
    const/4 v8, 0x0

    :goto_1
    if-ge v8, v10, :cond_2

    .line 602
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    aput-wide v0, v9, v8

    .line 603
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 601
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 608
    :cond_2
    if-eqz v6, :cond_3

    .line 609
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 612
    .end local v7    # "colidx":I
    .end local v9    # "index":[J
    :cond_3
    :goto_2
    return-object v9

    .line 608
    :cond_4
    if-eqz v6, :cond_3

    .line 609
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 608
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 609
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private isChangedDoOrganizeQueue()Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 501
    monitor-enter p0

    .line 502
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getQueue()[J

    move-result-object v1

    .line 503
    .local v1, "list":[J
    array-length v5, v1

    .line 507
    .local v5, "size":I
    if-nez v5, :cond_0

    .line 510
    monitor-exit p0

    .line 546
    :goto_0
    return v0

    .line 513
    :cond_0
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-direct {p0, v7, v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getRealCursorIndex(Landroid/content/ContentResolver;[J)[J

    move-result-object v2

    .line 514
    .local v2, "realList":[J
    if-nez v2, :cond_1

    .line 515
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->resetList()V

    .line 516
    monitor-exit p0

    move v0, v6

    goto :goto_0

    .line 519
    :cond_1
    const/4 v0, 0x0

    .line 520
    .local v0, "isChanged":Z
    array-length v3, v2

    .line 526
    .local v3, "realSize":I
    if-nez v3, :cond_3

    .line 528
    const/4 v6, 0x0

    invoke-virtual {p0, v6, v5}, Lcom/samsung/musicplus/service/PlayerListManager;->removeTracksInternal(II)I

    .line 529
    const/4 v0, 0x1

    .line 546
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 547
    .end local v0    # "isChanged":Z
    .end local v1    # "list":[J
    .end local v2    # "realList":[J
    .end local v3    # "realSize":I
    .end local v5    # "size":I
    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 531
    .restart local v0    # "isChanged":Z
    .restart local v1    # "list":[J
    .restart local v2    # "realList":[J
    .restart local v3    # "realSize":I
    .restart local v5    # "size":I
    :cond_3
    :try_start_1
    invoke-direct {p0, v1, v2}, Lcom/samsung/musicplus/service/PlayerListManager;->removeDeletedItemsInDb([J[J)I

    move-result v4

    .line 532
    .local v4, "removednum":I
    if-lez v4, :cond_2

    .line 533
    const/4 v0, 0x1

    .line 534
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getQueue()[J

    move-result-object v1

    .line 535
    array-length v5, v1

    .line 536
    if-nez v5, :cond_2

    .line 537
    const/4 v2, 0x0

    .line 538
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v6

    goto :goto_0
.end method

.method private registerContentObserver(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->unregisterContentObserver()V

    .line 295
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mObserver:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 296
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mIsRegistered:Z

    .line 297
    return-void
.end method

.method private registerMusicSquareUpdateObserver()V
    .locals 4

    .prologue
    .line 1548
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->unRegisterMusicSquareUpdateObserver()V

    .line 1549
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquareCellUpdator()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMusicSquareUpdateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1551
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mIsMusicSquareUpdaterRegistered:Z

    .line 1552
    return-void
.end method

.method private reloadQueue(Ljava/lang/String;)V
    .locals 10
    .param p1, "q"    # Ljava/lang/String;

    .prologue
    .line 186
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 187
    .local v4, "qlen":I
    :goto_0
    const/4 v6, 0x1

    if-le v4, v6, :cond_5

    .line 188
    const-string v6, "MusicListManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "loaded queue: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const/4 v3, 0x0

    .line 190
    .local v3, "plen":I
    const/4 v2, 0x0

    .line 191
    .local v2, "n":I
    const/4 v5, 0x0

    .line 192
    .local v5, "shift":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v4, :cond_4

    .line 193
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 194
    .local v0, "c":C
    const/16 v6, 0x3b

    if-ne v0, v6, :cond_1

    .line 195
    add-int/lit8 v6, v3, 0x1

    invoke-direct {p0, v6}, Lcom/samsung/musicplus/service/PlayerListManager;->ensurePlayListCapacity(I)V

    .line 196
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    int-to-long v8, v2

    aput-wide v8, v6, v3

    .line 197
    add-int/lit8 v3, v3, 0x1

    .line 198
    const/4 v2, 0x0

    .line 199
    const/4 v5, 0x0

    .line 192
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 186
    .end local v0    # "c":C
    .end local v1    # "i":I
    .end local v2    # "n":I
    .end local v3    # "plen":I
    .end local v4    # "qlen":I
    .end local v5    # "shift":I
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 201
    .restart local v0    # "c":C
    .restart local v1    # "i":I
    .restart local v2    # "n":I
    .restart local v3    # "plen":I
    .restart local v4    # "qlen":I
    .restart local v5    # "shift":I
    :cond_1
    const/16 v6, 0x30

    if-lt v0, v6, :cond_2

    const/16 v6, 0x39

    if-gt v0, v6, :cond_2

    .line 202
    add-int/lit8 v6, v0, -0x30

    shl-int/2addr v6, v5

    add-int/2addr v2, v6

    .line 210
    :goto_3
    add-int/lit8 v5, v5, 0x4

    goto :goto_2

    .line 203
    :cond_2
    const/16 v6, 0x61

    if-lt v0, v6, :cond_3

    const/16 v6, 0x66

    if-gt v0, v6, :cond_3

    .line 204
    add-int/lit8 v6, v0, 0xa

    add-int/lit8 v6, v6, -0x61

    shl-int/2addr v6, v5

    add-int/2addr v2, v6

    goto :goto_3

    .line 207
    :cond_3
    const/4 v3, 0x0

    .line 213
    .end local v0    # "c":C
    :cond_4
    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    .line 215
    .end local v1    # "i":I
    .end local v2    # "n":I
    .end local v3    # "plen":I
    .end local v5    # "shift":I
    :cond_5
    return-void
.end method

.method private removeDeletedItemsInDb([J[J)I
    .locals 8
    .param p1, "prevList"    # [J
    .param p2, "realList"    # [J

    .prologue
    .line 560
    const/4 v2, 0x0

    .line 561
    .local v2, "removed":I
    array-length v3, p1

    add-int/lit8 v1, v3, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 562
    aget-wide v4, p1, v1

    .line 563
    .local v4, "trackid":J
    invoke-static {p2, v4, v5}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v0

    .line 564
    .local v0, "crsridx":I
    if-gez v0, :cond_0

    .line 565
    const-string v3, "MusicNowPlaying"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "item no longer exists in db: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    invoke-virtual {p0, v4, v5}, Lcom/samsung/musicplus/service/PlayerListManager;->removeTrack(J)I

    move-result v3

    add-int/2addr v2, v3

    .line 561
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 569
    .end local v0    # "crsridx":I
    .end local v4    # "trackid":J
    :cond_1
    return v2
.end method

.method private removeTracksPosition(I)V
    .locals 9
    .param p1, "position"    # I

    .prologue
    const/4 v8, 0x1

    .line 1365
    monitor-enter p0

    .line 1366
    :try_start_0
    const-string v3, "MusicListManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "removeTracksInternal position : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1367
    if-gez p1, :cond_0

    .line 1368
    const/4 p1, 0x0

    .line 1370
    :cond_0
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lt p1, v3, :cond_1

    .line 1371
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/lit8 p1, v3, -0x1

    .line 1374
    :cond_1
    const/4 v0, 0x0

    .line 1375
    .local v0, "gotonext":Z
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-ne v3, p1, :cond_4

    .line 1378
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 1379
    const/4 v0, 0x1

    .line 1384
    :cond_2
    :goto_0
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    if-le v3, p1, :cond_3

    .line 1388
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    .line 1391
    :cond_3
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    sub-int/2addr v3, p1

    add-int/lit8 v2, v3, -0x1

    .line 1392
    .local v2, "num":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_5

    .line 1393
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    add-int v4, p1, v1

    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    add-int/lit8 v6, p1, 0x1

    add-int/2addr v6, v1

    aget-wide v6, v5, v6

    aput-wide v6, v3, v4

    .line 1392
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1380
    .end local v1    # "i":I
    .end local v2    # "num":I
    :cond_4
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-le v3, p1, :cond_2

    .line 1381
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    goto :goto_0

    .line 1416
    .end local v0    # "gotonext":Z
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1395
    .restart local v0    # "gotonext":Z
    .restart local v1    # "i":I
    .restart local v2    # "num":I
    :cond_5
    :try_start_1
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    .line 1397
    if-eqz v0, :cond_6

    .line 1398
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-nez v3, :cond_7

    .line 1399
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 1400
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    if-ne v3, v8, :cond_6

    .line 1403
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    .line 1416
    :cond_6
    :goto_2
    monitor-exit p0

    .line 1417
    return-void

    .line 1406
    :cond_7
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    iget v4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lt v3, v4, :cond_6

    .line 1407
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 1408
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    if-ne v3, v8, :cond_6

    .line 1411
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method private setList(Landroid/net/Uri;ILjava/lang/String;[JI)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "listType"    # I
    .param p3, "keyWord"    # Ljava/lang/String;
    .param p4, "list"    # [J
    .param p5, "position"    # I

    .prologue
    .line 321
    const-string v0, "MusicListManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setList() uri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " listType : 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " keyWord: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    if-nez p1, :cond_0

    .line 347
    :goto_0
    return-void

    .line 327
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurrentBaseUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 328
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerListManager;->changeBaseUri(Landroid/net/Uri;)V

    .line 331
    :cond_1
    const v0, 0x2000d

    if-ne p2, v0, :cond_3

    .line 332
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->acquireWakeLock(Landroid/content/Context;)V

    .line 337
    :goto_1
    const v0, 0x2000b

    if-ne p2, v0, :cond_2

    .line 340
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    const-string v1, "DLNA"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 343
    :cond_2
    iput p2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    .line 344
    iput-object p3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mKeyWord:Ljava/lang/String;

    .line 346
    invoke-virtual {p0, p4, p5}, Lcom/samsung/musicplus/service/PlayerListManager;->setList([JI)V

    goto :goto_0

    .line 334
    :cond_3
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->releaseWakeLock()V

    goto :goto_1
.end method

.method private setListType(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 448
    const-string v1, "MusicListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setListType() mTabFrom : 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    .line 451
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    const-string v2, "music_service_pref"

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 453
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "list_type"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getListType()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 454
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 455
    return-void
.end method

.method private unRegisterMusicSquareUpdateObserver()V
    .locals 2

    .prologue
    .line 1555
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mIsMusicSquareUpdaterRegistered:Z

    if-eqz v0, :cond_0

    .line 1556
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMusicSquareUpdateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1557
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mIsMusicSquareUpdaterRegistered:Z

    .line 1559
    :cond_0
    return-void
.end method

.method private unregisterContentObserver()V
    .locals 2

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mIsRegistered:Z

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mObserver:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 305
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mIsRegistered:Z

    .line 307
    :cond_0
    return-void
.end method


# virtual methods
.method public enqueue([JI)V
    .locals 1
    .param p1, "list"    # [J
    .param p2, "action"    # I

    .prologue
    .line 1455
    monitor-enter p0

    .line 1456
    packed-switch p2, :pswitch_data_0

    .line 1464
    const v0, 0x7fffffff

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/service/PlayerListManager;->addToList([JI)V

    .line 1465
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-gez v0, :cond_0

    .line 1466
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 1470
    :cond_0
    :goto_0
    monitor-exit p0

    .line 1471
    return-void

    .line 1458
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/service/PlayerListManager;->addToList([JI)V

    goto :goto_0

    .line 1470
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1461
    :pswitch_1
    :try_start_1
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/service/PlayerListManager;->addToList([JI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1456
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getAlbum()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1097
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v0, :cond_0

    .line 1098
    const/4 v0, 0x0

    .line 1100
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget-object v1, v1, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->album:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getAlbumArt()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1113
    const/4 v0, 0x0

    .line 1114
    .local v0, "uri":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-eqz v1, :cond_0

    .line 1115
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget-object v0, v1, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->albumArt:Ljava/lang/String;

    .line 1117
    :cond_0
    const-string v1, "MusicListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAlbumArt() uri : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1118
    return-object v0
.end method

.method public getAlbumId()J
    .locals 5

    .prologue
    .line 1104
    const-wide/16 v0, -0x1

    .line 1105
    .local v0, "id":J
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-eqz v2, :cond_0

    .line 1106
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget-wide v0, v2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->albumId:J

    .line 1108
    :cond_0
    const-string v2, "MusicListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAlbumId() id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    return-wide v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v0, :cond_0

    .line 1091
    const/4 v0, 0x0

    .line 1093
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget-object v1, v1, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->artist:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getAudioId(I)J
    .locals 5
    .param p1, "pos"    # I

    .prologue
    .line 693
    const-wide/16 v0, -0x1

    .line 694
    .local v0, "audioId":J
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lez v2, :cond_1

    if-ltz p1, :cond_1

    .line 696
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lt p1, v2, :cond_0

    .line 697
    const/4 p1, 0x0

    .line 699
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    aget-wide v0, v2, p1

    .line 702
    :cond_1
    const-string v2, "MusicListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAudioId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    return-wide v0
.end method

.method public getBitDepth()I
    .locals 4

    .prologue
    .line 1159
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v1, :cond_0

    .line 1160
    const/4 v0, -0x1

    .line 1164
    :goto_0
    return v0

    .line 1162
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget v0, v1, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->bitDepth:I

    .line 1163
    .local v0, "bitDepth":I
    const-string v1, "MusicListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getBitDepth() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getBottomAudioId()J
    .locals 5

    .prologue
    .line 1221
    const-wide/16 v0, -0x1

    .line 1222
    .local v0, "audioId":J
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-ltz v2, :cond_0

    .line 1223
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/lit8 v3, v3, -0x1

    aget-wide v0, v2, v3

    .line 1226
    :cond_0
    const-string v2, "MusicListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getBottomAudioId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mPlayListLength = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    return-wide v0
.end method

.method public getCurAudioId()J
    .locals 5

    .prologue
    .line 1194
    const-wide/16 v0, -0x1

    .line 1195
    .local v0, "audioId":J
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lez v2, :cond_1

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-ltz v2, :cond_1

    .line 1197
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lt v2, v3, :cond_0

    .line 1198
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 1200
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    aget-wide v0, v2, v3

    .line 1202
    :cond_1
    const-string v2, "MusicListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCurAudioId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    return-wide v0
.end method

.method public getCurrentBaseUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 950
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mBaseUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 951
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerListManager;->changeBaseUri(Landroid/net/Uri;)V

    .line 953
    :cond_0
    const-string v0, "MusicListManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCurrentBaseUri() Uri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mBaseUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getCurrentBaseUriString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 941
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurrentBaseUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentUri()Landroid/net/Uri;
    .locals 6

    .prologue
    .line 925
    const/4 v2, 0x0

    .line 926
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurAudioId()J

    move-result-wide v0

    .line 927
    .local v0, "audioId":J
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-eqz v3, :cond_0

    .line 928
    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/PlayerListManager;->appendWithBaseUri(J)Landroid/net/Uri;

    move-result-object v2

    .line 930
    :cond_0
    const-string v3, "MusicListManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentUri() Uri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    return-object v2
.end method

.method public getDeviceId()J
    .locals 2

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v0, :cond_0

    .line 1130
    const-wide/16 v0, -0x1

    .line 1132
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget-wide v0, v0, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->deviceId:J

    goto :goto_0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1136
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v0, :cond_0

    .line 1137
    const/4 v0, 0x0

    .line 1139
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->dmsName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDlnaContentsSeed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1143
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v0, :cond_0

    .line 1144
    const/4 v0, 0x0

    .line 1146
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->seed:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDuration()J
    .locals 5

    .prologue
    .line 1177
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v2, :cond_0

    .line 1178
    const-wide/16 v0, -0x1

    .line 1182
    :goto_0
    return-wide v0

    .line 1180
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget-wide v0, v2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    .line 1181
    .local v0, "duration":J
    const-string v2, "MusicListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDuration() from DB "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1067
    const/4 v0, 0x0

    .line 1068
    .local v0, "path":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-eqz v1, :cond_0

    .line 1069
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget-object v0, v1, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->filePath:Ljava/lang/String;

    .line 1071
    :cond_0
    const-string v1, "MusicListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFilePath() path : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1072
    return-object v0
.end method

.method public getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1122
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v0, :cond_0

    .line 1123
    const/4 v0, 0x0

    .line 1125
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->genre:Ljava/lang/String;

    goto :goto_0
.end method

.method public getKeyWord()Ljava/lang/String;
    .locals 3

    .prologue
    .line 458
    const-string v0, "MusicListManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getKeyWord() mKeyWord: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mKeyWord:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mKeyWord:Ljava/lang/String;

    return-object v0
.end method

.method public getListItemCount()I
    .locals 1

    .prologue
    .line 723
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    return v0
.end method

.method public getListPosition()I
    .locals 3

    .prologue
    .line 713
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 714
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    .line 715
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 718
    :cond_0
    const-string v0, "MusicListManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCurrentPosition : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    return v0
.end method

.method public getListType()I
    .locals 3

    .prologue
    .line 443
    const-string v0, "MusicListManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getListType() mTabFrom : 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    return v0
.end method

.method public getMediaInfo(Landroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1063
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    invoke-static {v0, v1, p1}, Lcom/samsung/musicplus/util/ListUtils;->getMediaInfo(Landroid/content/Context;ILandroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    move-result-object v0

    return-object v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 1076
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v0, :cond_0

    .line 1077
    const/4 v0, 0x0

    .line 1079
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget v0, v0, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->mediaType:I

    goto :goto_0
.end method

.method public getMime()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1150
    const/4 v0, 0x0

    .line 1151
    .local v0, "mime":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-eqz v1, :cond_0

    .line 1152
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget-object v0, v1, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->mimeType:Ljava/lang/String;

    .line 1154
    :cond_0
    const-string v1, "MusicListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMime() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    return-object v0
.end method

.method public getMoodValue()I
    .locals 4

    .prologue
    .line 1232
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v1, :cond_0

    .line 1233
    const/4 v0, -0x1

    .line 1237
    :goto_0
    return v0

    .line 1235
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget v0, v1, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->moodValue:I

    .line 1236
    .local v0, "mood":I
    const-string v1, "MusicListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMoodValue() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getNextMediaUri()Landroid/net/Uri;
    .locals 5

    .prologue
    .line 784
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/service/PlayerListManager;->getNextPosition(IIZ)I

    move-result v0

    .line 785
    .local v0, "position":I
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-nez v2, :cond_1

    .line 786
    :cond_0
    const/4 v1, 0x0

    .line 791
    :goto_0
    return-object v1

    .line 789
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    aget-wide v2, v2, v0

    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/service/PlayerListManager;->appendWithBaseUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 790
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "MusicListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNextMediaUri() Uri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getOrganizedQueue()[J
    .locals 1

    .prologue
    .line 551
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->doOrganizeQueue()V

    .line 552
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getQueue()[J

    move-result-object v0

    return-object v0
.end method

.method public getPersonalMode()I
    .locals 4

    .prologue
    .line 1241
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v1, :cond_0

    .line 1242
    const/4 v0, -0x1

    .line 1246
    :goto_0
    return v0

    .line 1244
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget v0, v1, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->personalMode:I

    .line 1245
    .local v0, "personalMode":I
    const-string v1, "MusicListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPersonalMode() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getPrevMediaUri()Landroid/net/Uri;
    .locals 5

    .prologue
    .line 801
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/service/PlayerListManager;->getPrevPosition(IIZ)I

    move-result v0

    .line 802
    .local v0, "position":I
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-nez v2, :cond_1

    .line 803
    :cond_0
    const/4 v1, 0x0

    .line 808
    :goto_0
    return-object v1

    .line 806
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    aget-wide v2, v2, v0

    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/service/PlayerListManager;->appendWithBaseUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 807
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "MusicListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getPrevMediaUri() Uri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getQueue()[J
    .locals 5

    .prologue
    .line 463
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-nez v2, :cond_1

    .line 464
    :cond_0
    const-string v2, "MusicListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "return empty list, mPlayList: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mPlayListLength: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    const/4 v2, 0x0

    new-array v1, v2, [J

    .line 473
    :goto_0
    return-object v1

    .line 468
    :cond_1
    monitor-enter p0

    .line 469
    :try_start_0
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    new-array v1, v2, [J

    .line 470
    .local v1, "list":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-ge v0, v2, :cond_2

    .line 471
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    aget-wide v2, v2, v0

    aput-wide v2, v1, v0

    .line 470
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 473
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 474
    .end local v0    # "i":I
    .end local v1    # "list":[J
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getQueueToSave()Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 234
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 236
    .local v6, "start":J
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 238
    .local v3, "q":Ljava/lang/StringBuilder;
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    .line 239
    .local v2, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_3

    .line 240
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    aget-wide v4, v8, v1

    .line 241
    .local v4, "n":J
    cmp-long v8, v4, v10

    if-gez v8, :cond_0

    .line 239
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 243
    :cond_0
    cmp-long v8, v4, v10

    if-nez v8, :cond_1

    .line 244
    const-string v8, "0;"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 246
    :cond_1
    :goto_2
    cmp-long v8, v4, v10

    if-eqz v8, :cond_2

    .line 247
    const-wide/16 v8, 0xf

    and-long/2addr v8, v4

    long-to-int v0, v8

    .line 248
    .local v0, "digit":I
    const/4 v8, 0x4

    ushr-long/2addr v4, v8

    .line 249
    sget-object v8, Lcom/samsung/musicplus/service/PlayerListManager;->HEX_DIGITS:[C

    aget-char v8, v8, v0

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 251
    .end local v0    # "digit":I
    :cond_2
    const-string v8, ";"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 254
    .end local v4    # "n":J
    :cond_3
    const-string v8, "MusicListManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "created queue string in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v6

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ms"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method public getRepeatMode()I
    .locals 1

    .prologue
    .line 1186
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mRepeatMode:I

    return v0
.end method

.method public getSamplingRate()I
    .locals 4

    .prologue
    .line 1168
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v1, :cond_0

    .line 1169
    const/4 v0, -0x1

    .line 1173
    :goto_0
    return v0

    .line 1171
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget v0, v1, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->samplingRate:I

    .line 1172
    .local v0, "sampling":I
    const-string v1, "MusicListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSamplingRate() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getShuffle()I
    .locals 1

    .prologue
    .line 618
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1083
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    if-nez v0, :cond_0

    .line 1084
    const/4 v0, 0x0

    .line 1086
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    iget-object v1, v1, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTopAudioId()J
    .locals 5

    .prologue
    .line 1209
    const-wide/16 v0, -0x1

    .line 1210
    .local v0, "audioId":J
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-ltz v2, :cond_0

    .line 1211
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    const/4 v3, 0x0

    aget-wide v0, v2, v3

    .line 1214
    :cond_0
    const-string v2, "MusicListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getTopAudioId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    return-wide v0
.end method

.method public movePosition(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 895
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PLAYLIST_REPEAT_US_ATT_FEATURE:Z

    if-eqz v0, :cond_0

    .line 896
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getQueue()[J

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/samsung/musicplus/service/PlayerListManager;->setList([JI)V

    .line 897
    const-string v0, "MusicListManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ATT_10776 now playing list case, movePosition : mFirstIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    :goto_0
    return-void

    .line 901
    :cond_0
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 902
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 903
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 904
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    .line 907
    :cond_1
    const-string v0, "MusicListManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "movePosition() position : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public moveQueueItem(II)V
    .locals 6
    .param p1, "index1"    # I
    .param p2, "index2"    # I

    .prologue
    .line 1327
    monitor-enter p0

    .line 1328
    :try_start_0
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lt p1, v1, :cond_0

    .line 1329
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/lit8 p1, v1, -0x1

    .line 1331
    :cond_0
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lt p2, v1, :cond_1

    .line 1332
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/lit8 p2, v1, -0x1

    .line 1334
    :cond_1
    if-ge p1, p2, :cond_6

    .line 1335
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    aget-wide v2, v1, p1

    .line 1336
    .local v2, "tmp":J
    move v0, p1

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_2

    .line 1337
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    add-int/lit8 v5, v0, 0x1

    aget-wide v4, v4, v5

    aput-wide v4, v1, v0

    .line 1336
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1339
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    aput-wide v2, v1, p2

    .line 1340
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-ne v1, p1, :cond_5

    .line 1341
    iput p2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 1357
    .end local v0    # "i":I
    .end local v2    # "tmp":J
    :cond_3
    :goto_1
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    const/4 v4, 0x1

    if-ne v1, v4, :cond_4

    .line 1359
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    const/4 v4, 0x0

    invoke-direct {p0, v1, v4}, Lcom/samsung/musicplus/service/PlayerListManager;->createShuffleIndex(IZ)V

    .line 1361
    :cond_4
    monitor-exit p0

    .line 1362
    return-void

    .line 1342
    .restart local v0    # "i":I
    .restart local v2    # "tmp":J
    :cond_5
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-lt v1, p1, :cond_3

    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-gt v1, p2, :cond_3

    .line 1343
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    goto :goto_1

    .line 1361
    .end local v0    # "i":I
    .end local v2    # "tmp":J
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1345
    :cond_6
    if-ge p2, p1, :cond_3

    .line 1346
    :try_start_1
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    aget-wide v2, v1, p1

    .line 1347
    .restart local v2    # "tmp":J
    move v0, p1

    .restart local v0    # "i":I
    :goto_2
    if-le v0, p2, :cond_7

    .line 1348
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    add-int/lit8 v5, v0, -0x1

    aget-wide v4, v4, v5

    aput-wide v4, v1, v0

    .line 1347
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 1350
    :cond_7
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    aput-wide v2, v1, p2

    .line 1351
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-ne v1, p1, :cond_8

    .line 1352
    iput p2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    goto :goto_1

    .line 1353
    :cond_8
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-lt v1, p2, :cond_3

    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-gt v1, p1, :cond_3

    .line 1354
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public moveToNext(Z)Z
    .locals 3
    .param p1, "ignoreRepeatOne"    # Z

    .prologue
    const/4 v0, 0x1

    .line 827
    if-eqz p1, :cond_1

    .line 828
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    invoke-direct {p0, v1, v2, v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getNextPosition(IIZ)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 834
    :cond_0
    :goto_0
    return v0

    .line 830
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->calculateNextPosition()Z

    move-result v1

    if-nez v1, :cond_0

    .line 831
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToPrev()V
    .locals 3

    .prologue
    .line 884
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/musicplus/service/PlayerListManager;->getPrevPosition(IIZ)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 885
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 1540
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->releaseWakeLock()V

    .line 1541
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 1542
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->unregisterContentObserver()V

    .line 1543
    return-void
.end method

.method public reloadAllTrackIfNoListExist()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const v1, 0x20001

    .line 376
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getListItemCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 377
    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v0

    iget-object v6, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 378
    .local v6, "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    const/4 v7, 0x0

    .line 380
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v2, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v3, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v4, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v5, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 382
    invoke-static {v7}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v8

    .line 383
    .local v8, "list":[J
    const v0, 0x20001

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v8, v2}, Lcom/samsung/musicplus/service/PlayerListManager;->setList(ILjava/lang/String;[JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    if-eqz v7, :cond_0

    .line 386
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 391
    .end local v6    # "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    .end local v7    # "c":Landroid/database/Cursor;
    .end local v8    # "list":[J
    :cond_0
    return-void

    .line 385
    .restart local v6    # "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    .restart local v7    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_1

    .line 386
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public reloadList(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "queue"    # Ljava/lang/String;
    .param p4, "position"    # I

    .prologue
    .line 267
    const-string v1, "MusicListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reloadList() mTabFrom : 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mKeyWord: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mKeyWord:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    if-eq v1, p1, :cond_0

    .line 270
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->convertToUri(I)Landroid/net/Uri;

    move-result-object v0

    .line 271
    .local v0, "uri":Landroid/net/Uri;
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerListManager;->changeBaseUri(Landroid/net/Uri;)V

    .line 273
    .end local v0    # "uri":Landroid/net/Uri;
    :cond_0
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    .line 274
    iput-object p2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mKeyWord:Ljava/lang/String;

    .line 276
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/service/PlayerListManager;->reloadQueue(Ljava/lang/String;)V

    .line 277
    iput p4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 280
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getOrganizedQueue()[J

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getListPosition()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/PlayerListManager;->setList([JI)V

    .line 281
    return-void
.end method

.method public removeTrack(J)I
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 1250
    const/4 v1, 0x0

    .line 1251
    .local v1, "numremoved":I
    monitor-enter p0

    .line 1252
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-ge v0, v2, :cond_1

    .line 1253
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    aget-wide v2, v2, v0

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 1254
    invoke-virtual {p0, v0, v0}, Lcom/samsung/musicplus/service/PlayerListManager;->removeTracksInternal(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 1255
    add-int/lit8 v0, v0, -0x1

    .line 1252
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1258
    :cond_1
    monitor-exit p0

    .line 1259
    return v1

    .line 1258
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public removeTracksInternal(II)I
    .locals 9
    .param p1, "first"    # I
    .param p2, "last"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 1263
    monitor-enter p0

    .line 1264
    :try_start_0
    const-string v4, "MusicListManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeTracksInternal first : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " last : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    if-ge p2, p1, :cond_0

    .line 1266
    monitor-exit p0

    .line 1316
    :goto_0
    return v3

    .line 1268
    :cond_0
    if-gez p1, :cond_1

    .line 1269
    const/4 p1, 0x0

    .line 1271
    :cond_1
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lt p2, v3, :cond_2

    .line 1272
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/lit8 p2, v3, -0x1

    .line 1275
    :cond_2
    const/4 v0, 0x0

    .line 1276
    .local v0, "gotonext":Z
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-gt p1, v3, :cond_4

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-gt v3, p2, :cond_4

    .line 1279
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 1280
    const/4 v0, 0x1

    .line 1285
    :cond_3
    :goto_1
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    sub-int/2addr v3, p2

    add-int/lit8 v2, v3, -0x1

    .line 1287
    .local v2, "num":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v2, :cond_5

    .line 1288
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    add-int v4, p1, v1

    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    add-int/lit8 v6, p2, 0x1

    add-int/2addr v6, v1

    aget-wide v6, v5, v6

    aput-wide v6, v3, v4

    .line 1287
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1281
    .end local v1    # "i":I
    .end local v2    # "num":I
    :cond_4
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    if-le v3, p2, :cond_3

    .line 1282
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    sub-int v4, p2, p1

    add-int/lit8 v4, v4, 0x1

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    goto :goto_1

    .line 1317
    .end local v0    # "gotonext":Z
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1290
    .restart local v0    # "gotonext":Z
    .restart local v1    # "i":I
    .restart local v2    # "num":I
    :cond_5
    :try_start_1
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    sub-int v4, p2, p1

    add-int/lit8 v4, v4, 0x1

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    .line 1292
    if-eqz v0, :cond_6

    .line 1293
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-nez v3, :cond_8

    .line 1294
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 1295
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    .line 1296
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    if-ne v3, v8, :cond_6

    .line 1299
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I

    .line 1312
    :cond_6
    :goto_3
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    if-ne v3, v8, :cond_7

    .line 1314
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lcom/samsung/musicplus/service/PlayerListManager;->createShuffleIndex(IZ)V

    .line 1316
    :cond_7
    sub-int v3, p2, p1

    add-int/lit8 v3, v3, 0x1

    monitor-exit p0

    goto :goto_0

    .line 1302
    :cond_8
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    iget v4, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lt v3, v4, :cond_6

    .line 1303
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 1304
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    if-ne v3, v8, :cond_6

    .line 1307
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShufflePlayPos:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method

.method public removeTracksInternal([I)I
    .locals 4
    .param p1, "position"    # [I

    .prologue
    .line 1420
    const-string v2, "MusicListManager"

    const-string v3, "removeTracksInternal list.."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1421
    const/4 v1, 0x0

    .line 1422
    .local v1, "numberRemoved":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 1423
    aget v2, p1, v0

    sub-int/2addr v2, v1

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/service/PlayerListManager;->removeTracksPosition(I)V

    .line 1426
    add-int/lit8 v1, v1, 0x1

    .line 1422
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1428
    :cond_0
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 1430
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/service/PlayerListManager;->createShuffleIndex(IZ)V

    .line 1432
    :cond_1
    array-length v2, p1

    return v2
.end method

.method public resetList()V
    .locals 1

    .prologue
    .line 1535
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    .line 1536
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    .line 1537
    return-void
.end method

.method public savePlayedInfo()V
    .locals 8

    .prologue
    .line 961
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurrentBaseUri()Landroid/net/Uri;

    move-result-object v2

    .line 962
    .local v2, "baseUri":Landroid/net/Uri;
    sget-object v3, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    invoke-virtual {v3, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 984
    :cond_0
    :goto_0
    return-void

    .line 967
    :cond_1
    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    const v6, 0x20004

    if-ne v3, v6, :cond_2

    .line 968
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mKeyWord:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 969
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mKeyWord:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 970
    .local v4, "pid":J
    const-wide/16 v6, -0xd

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    const-wide/16 v6, -0xc

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 979
    .end local v4    # "pid":J
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurAudioId()J

    move-result-wide v0

    .line 980
    .local v0, "audioId":J
    const-wide/16 v6, 0x0

    cmp-long v3, v0, v6

    if-lez v3, :cond_0

    .line 982
    new-instance v3, Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;

    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v6, v2, v0, v1}, Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;-><init>(Landroid/content/Context;Landroid/net/Uri;J)V

    invoke-virtual {v3}, Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;->start()V

    goto :goto_0
.end method

.method public setList(ILjava/lang/String;[JI)V
    .locals 6
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I

    .prologue
    .line 365
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    .line 366
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->convertToUri(I)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/service/PlayerListManager;->setList(Landroid/net/Uri;ILjava/lang/String;[JI)V

    .line 367
    return-void
.end method

.method public setList([JI)V
    .locals 5
    .param p1, "list"    # [J
    .param p2, "position"    # I

    .prologue
    const/4 v4, 0x1

    .line 400
    monitor-enter p0

    .line 401
    :try_start_0
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    .line 402
    if-nez p1, :cond_4

    .line 403
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    .line 408
    :goto_0
    if-gez p2, :cond_0

    .line 409
    if-eqz p1, :cond_0

    array-length v1, p1

    if-lez v1, :cond_0

    .line 412
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    .line 413
    .local v0, "rand":Ljava/util/Random;
    array-length v1, p1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result p2

    .line 414
    const/4 v1, 0x1

    iput v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    .line 417
    .end local v0    # "rand":Ljava/util/Random;
    :cond_0
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-lt p2, v1, :cond_1

    .line 418
    const-string v1, "MusicListManager"

    const-string v2, "position is over length so adjust it"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    add-int/lit8 p2, v1, -0x1

    .line 421
    :cond_1
    iput p2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    .line 422
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    if-ne v1, v4, :cond_2

    .line 423
    const/4 v1, 0x0

    invoke-direct {p0, p2, v1}, Lcom/samsung/musicplus/service/PlayerListManager;->createShuffleIndex(IZ)V

    .line 427
    :cond_2
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PLAYLIST_REPEAT_US_ATT_FEATURE:Z

    if-eqz v1, :cond_3

    .line 428
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    iput v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    .line 429
    const-string v1, "MusicListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ATT_10776 setList : mFirstIndex="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mFirstIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :cond_3
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListType:I

    const v2, 0x2000a

    if-ne v1, v2, :cond_5

    .line 432
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->registerMusicSquareUpdateObserver()V

    .line 437
    :goto_1
    const-string v1, "MusicListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setList()  mPlayListLength : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mPlayPos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    monitor-exit p0

    .line 440
    return-void

    .line 405
    :cond_4
    array-length v1, p1

    iput v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    goto/16 :goto_0

    .line 439
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 434
    :cond_5
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->unRegisterMusicSquareUpdateObserver()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public setRepeatMode(I)V
    .locals 0
    .param p1, "repeatMode"    # I

    .prologue
    .line 1190
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mRepeatMode:I

    .line 1191
    return-void
.end method

.method public setShuffle(I)V
    .locals 3
    .param p1, "shuffleMode"    # I

    .prologue
    .line 624
    const-string v0, "MusicListManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mIsShuffle : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " setShuffle(): mPlayListLength: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayList:[J

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayListLength:I

    if-gtz v0, :cond_1

    .line 628
    :cond_0
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    .line 637
    :goto_0
    return-void

    .line 632
    :cond_1
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    if-eq v0, p1, :cond_2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 633
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mPlayPos:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/PlayerListManager;->createShuffleIndex(IZ)V

    .line 635
    :cond_2
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mShuffleMode:I

    goto :goto_0
.end method

.method public setSlinkWakeLock()V
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->acquireWakeLock(Landroid/content/Context;)V

    .line 354
    return-void
.end method

.method public startContentObserving(Lcom/samsung/musicplus/service/PlayerListManager$OnListChangeListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/musicplus/service/PlayerListManager$OnListChangeListener;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mListener:Lcom/samsung/musicplus/service/PlayerListManager$OnListChangeListener;

    .line 172
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 173
    new-instance v0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mObserver:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    .line 174
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mObserver:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    new-instance v1, Lcom/samsung/musicplus/service/PlayerListManager$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/PlayerListManager$1;-><init>(Lcom/samsung/musicplus/service/PlayerListManager;)V

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->setContentChangeListener(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;)V

    .line 180
    return-void
.end method

.method public declared-synchronized updateMediaInfo()Z
    .locals 2

    .prologue
    .line 1050
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurrentUri()Landroid/net/Uri;

    move-result-object v0

    .line 1051
    .local v0, "uri":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 1052
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    .line 1056
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 1057
    const/4 v1, 0x0

    .line 1059
    :goto_1
    monitor-exit p0

    return v1

    .line 1054
    :cond_0
    :try_start_1
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getMediaInfo(Landroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager;->mMediaInfo:Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1050
    .end local v0    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1059
    .restart local v0    # "uri":Landroid/net/Uri;
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method
