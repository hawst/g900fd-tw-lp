.class Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$1;
.super Ljava/lang/Object;
.source "PlayerListManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;)V
    .locals 0

    .prologue
    .line 1622
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$1;->this$0:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1626
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$1;->this$0:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    # setter for: Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mLastLoadCompleteTime:J
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->access$402(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;J)J

    .line 1627
    const-string v0, "MusicListManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "do run ! mLastLoadCompleteTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$1;->this$0:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    # getter for: Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mLastLoadCompleteTime:J
    invoke-static {v2}, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->access$400(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1628
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$1;->this$0:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    # getter for: Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mListener:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->access$500(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;)Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1629
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$1;->this$0:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    # getter for: Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mListener:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->access$500(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;)Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;->onChanged()V

    .line 1631
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$1;->this$0:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mWaiting:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->access$602(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;Z)Z

    .line 1632
    return-void
.end method
