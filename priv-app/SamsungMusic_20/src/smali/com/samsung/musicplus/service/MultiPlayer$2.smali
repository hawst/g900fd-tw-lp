.class Lcom/samsung/musicplus/service/MultiPlayer$2;
.super Ljava/lang/Object;
.source "MultiPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/MultiPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/MultiPlayer;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer$2;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "percent"    # I

    .prologue
    .line 289
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBufferingUpdate percent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$2;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mBufferPercent:I
    invoke-static {v0, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->access$802(Lcom/samsung/musicplus/service/MultiPlayer;I)I

    .line 291
    return-void
.end method
