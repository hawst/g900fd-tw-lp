.class public Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;
.super Landroid/app/Activity;
.source "SviewCoverActivity.java"

# interfaces
.implements Lcom/samsung/musicplus/service/PlayerServiceStateAction;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mConnection:Landroid/content/ServiceConnection;

.field private mServiceToken:Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

.field private mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 27
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity$1;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;
    .param p1, "x1"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->dismissSviewCoverDialog()V

    return-void
.end method

.method private dismissSviewCoverDialog()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 76
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dismissSviewCoverDialog() - mSviewCoverDialog: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->setOnDismissCallback(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$OnDismissCallback;)V

    .line 79
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->dismiss()V

    .line 80
    iput-object v3, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .line 81
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->finish()V

    .line 83
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() - savedInstanceState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x4080000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 54
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mConnection:Landroid/content/ServiceConnection;

    invoke-static {p0, v0}, Lcom/samsung/musicplus/util/ServiceUtils;->bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mServiceToken:Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

    .line 55
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 59
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->dismiss()V

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mServiceToken:Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->unbindFromService(Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;)V

    .line 64
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 65
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 70
    if-nez p1, :cond_0

    .line 71
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->dismissSviewCoverDialog()V

    .line 73
    :cond_0
    return-void
.end method
