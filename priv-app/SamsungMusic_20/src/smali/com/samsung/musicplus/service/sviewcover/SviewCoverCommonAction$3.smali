.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$3;
.super Landroid/content/BroadcastReceiver;
.source "SviewCoverCommonAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 139
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mMediaReceiver - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 143
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentPath()Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "currentPath":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 145
    .local v2, "removedPath":Ljava/lang/String;
    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "currentPath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", removedPath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-static {v1}, Lcom/samsung/musicplus/util/UiUtils;->isLocalContents(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 148
    iget-object v3, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mOnMediaListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;
    invoke-static {v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->access$300(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;->OnMediaRemoved()V

    .line 151
    .end local v1    # "currentPath":Ljava/lang/String;
    .end local v2    # "removedPath":Ljava/lang/String;
    :cond_1
    return-void
.end method
