.class Lcom/samsung/musicplus/service/MultiPlayer$16;
.super Ljava/lang/Object;
.source "MultiPlayer.java"

# interfaces
.implements Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/MultiPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/MultiPlayer;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0

    .prologue
    .line 1821
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer$16;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion()V
    .locals 2

    .prologue
    .line 1825
    const-string v0, "MusicPlayer"

    const-string v1, "AV player onCompletion"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1826
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$16;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->playComplete()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1700(Lcom/samsung/musicplus/service/MultiPlayer;)V

    .line 1827
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$16;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->stopBuffering()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1000(Lcom/samsung/musicplus/service/MultiPlayer;)V

    .line 1828
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$16;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->resetDmrRequest()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3900(Lcom/samsung/musicplus/service/MultiPlayer;)V

    .line 1829
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$16;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1202(Lcom/samsung/musicplus/service/MultiPlayer;I)I

    .line 1833
    return-void
.end method
