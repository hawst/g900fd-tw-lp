.class public interface abstract Lcom/samsung/musicplus/service/MediaCommandAction$CommandMood;
.super Ljava/lang/Object;
.source "MediaCommandAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/MediaCommandAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CommandMood"
.end annotation


# static fields
.field public static final CALM:Ljava/lang/String; = "calm"

.field public static final EXCITING:Ljava/lang/String; = "exciting"

.field public static final JOYFUL:Ljava/lang/String; = "joyful"

.field public static final PASSIONATE:Ljava/lang/String; = "passionate"
