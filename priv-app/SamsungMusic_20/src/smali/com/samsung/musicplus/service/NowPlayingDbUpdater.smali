.class public Lcom/samsung/musicplus/service/NowPlayingDbUpdater;
.super Ljava/lang/Object;
.source "NowPlayingDbUpdater.java"


# static fields
.field private static final NOWPLAYING_LIST_NAME:Ljava/lang/String; = "now playing list 0123456789"

.field private static final TAG:Ljava/lang/String; = "MusicNowPlayingDbUpdater"

.field private static final UNDEFINED:I = -0x1

.field private static sNowPlayingListId:J

.field private static sNowPlayingListToken:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->sNowPlayingListId:J

    .line 33
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->sNowPlayingListToken:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;)J
    .locals 2
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-static {p0}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->getNowPlayingListId(Landroid/content/Context;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static getNowPlayingIdByData(Landroid/content/Context;)J
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 141
    const/4 v6, 0x0

    .line 145
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 146
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI_INCLUDING_NESTED_LIST:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "_data LIKE \"%now playing list 0123456789\""

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 151
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 152
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 155
    if-eqz v6, :cond_0

    .line 156
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 159
    :cond_0
    :goto_0
    return-wide v2

    .line 155
    :cond_1
    if-eqz v6, :cond_2

    .line 156
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 159
    :cond_2
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 155
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    .line 156
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method private static getNowPlayingIdByName(Landroid/content/Context;)J
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    const/4 v6, 0x0

    .line 102
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 103
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI_INCLUDING_NESTED_LIST:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "name= ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "now playing list 0123456789"

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 109
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 110
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 113
    if-eqz v6, :cond_0

    .line 114
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 117
    :cond_0
    :goto_0
    return-wide v2

    .line 113
    :cond_1
    if-eqz v6, :cond_2

    .line 114
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 117
    :cond_2
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 113
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    .line 114
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method private static getNowPlayingIdFromAll(Landroid/content/Context;)J
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 203
    const/4 v6, 0x0

    .line 205
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 206
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "_data LIKE \"%now playing list 0123456789\""

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 211
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 212
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 215
    if-eqz v6, :cond_0

    .line 216
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 219
    :cond_0
    :goto_0
    return-wide v2

    .line 215
    :cond_1
    if-eqz v6, :cond_2

    .line 216
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 219
    :cond_2
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 215
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    .line 216
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method private static getNowPlayingListId(Landroid/content/Context;)J
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v4, 0x0

    .line 54
    sget-wide v0, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->sNowPlayingListId:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    .line 55
    sget-object v1, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->sNowPlayingListToken:Ljava/lang/Object;

    monitor-enter v1

    .line 56
    :try_start_0
    sget-wide v2, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->sNowPlayingListId:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    .line 57
    if-nez p0, :cond_2

    .line 58
    const-string v0, "MusicNowPlayingDbUpdater"

    const-string v2, "try to getNowPlayingListId but id is minus value and context is null. Please check your logic"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    :cond_0
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :cond_1
    sget-wide v0, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->sNowPlayingListId:J

    return-wide v0

    .line 61
    :cond_2
    :try_start_1
    invoke-static {p0}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->getNowPlayingListIdInternal(Landroid/content/Context;)J

    move-result-wide v2

    sput-wide v2, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->sNowPlayingListId:J

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static getNowPlayingListIdInternal(Landroid/content/Context;)J
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v6, -0x1

    .line 70
    invoke-static {p0}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->getNowPlayingIdByName(Landroid/content/Context;)J

    move-result-wide v0

    .line 72
    .local v0, "id":J
    cmp-long v2, v0, v6

    if-nez v2, :cond_0

    .line 73
    invoke-static {p0}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->getNowPlayingIdByData(Landroid/content/Context;)J

    move-result-wide v0

    .line 74
    const-string v2, "MusicNowPlayingDbUpdater"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail to search by name so research it by data : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    cmp-long v2, v0, v6

    if-eqz v2, :cond_0

    .line 78
    invoke-static {p0, v0, v1}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->updateNowPlayingName(Landroid/content/Context;J)V

    .line 81
    :cond_0
    cmp-long v2, v0, v6

    if-nez v2, :cond_1

    .line 82
    const-string v2, "MusicNowPlayingDbUpdater"

    const-string v3, "Playlist but there are no now playing list. So make it."

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-static {p0}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->makeNowPlayingList(Landroid/content/Context;)J

    move-result-wide v0

    .line 86
    :cond_1
    cmp-long v2, v0, v6

    if-nez v2, :cond_2

    .line 87
    const-string v2, "MusicNowPlayingDbUpdater"

    const-string v3, "getNowPlayingListIdInternal : failed to make new NowPlayingList DB."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-static {p0}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->getNowPlayingIdFromAll(Landroid/content/Context;)J

    move-result-wide v0

    .line 89
    cmp-long v2, v0, v6

    if-eqz v2, :cond_2

    .line 90
    const-string v2, "MusicNowPlayingDbUpdater"

    const-string v3, "updateNowPlayingListMediaType : Media DB error!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-static {p0, v0, v1}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->updateNowPlayingMediaType(Landroid/content/Context;J)V

    .line 95
    :cond_2
    const-string v2, "MusicNowPlayingDbUpdater"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNowplayingListId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    return-wide v0
.end method

.method private static makeNowPlayingList(Landroid/content/Context;)J
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 164
    new-instance v3, Landroid/content/ContentValues;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 165
    .local v3, "value":Landroid/content/ContentValues;
    const-string v4, "name"

    const-string v5, "now playing list 0123456789"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 168
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v2, 0x0

    .line 170
    .local v2, "uri":Landroid/net/Uri;
    :try_start_0
    sget-object v4, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI_WITH_OUT_NOTI:Landroid/net/Uri;

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 174
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 175
    invoke-virtual {v2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x3

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 179
    :goto_1
    return-wide v4

    .line 171
    :catch_0
    move-exception v1

    .line 172
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "MusicNowPlayingDbUpdater"

    const-string v5, "makeNowPlayingList() - IllegalArgumentException occur"

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 178
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    const-string v4, "MusicNowPlayingDbUpdater"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "makeNowplayingList but fail to get a play list id, returned uri is : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-wide/16 v4, -0x1

    goto :goto_1
.end method

.method public static saveNowPlayingQueue(Landroid/content/Context;[J)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "list"    # [J

    .prologue
    .line 222
    new-instance v0, Lcom/samsung/musicplus/service/NowPlayingDbUpdater$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater$1;-><init>(Landroid/content/Context;[J)V

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater$1;->start()V

    .line 245
    return-void
.end method

.method private static updateNowPlayingMediaType(Landroid/content/Context;J)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    .line 182
    const-string v2, "external"

    invoke-static {v2}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/musicplus/provider/MusicContents;->getNotifyDisabledUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 185
    .local v0, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 186
    .local v1, "value":Landroid/content/ContentValues;
    const-string v2, "media_type"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 188
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 190
    return-void
.end method

.method private static updateNowPlayingName(Landroid/content/Context;J)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    .line 121
    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI_WITH_OUT_NOTI:Landroid/net/Uri;

    invoke-static {v2}, Lcom/samsung/musicplus/provider/MusicContents;->getNotifyDisabledUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 124
    .local v0, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 125
    .local v1, "value":Landroid/content/ContentValues;
    const-string v2, "name"

    const-string v3, "now playing list 0123456789"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 129
    return-void
.end method
