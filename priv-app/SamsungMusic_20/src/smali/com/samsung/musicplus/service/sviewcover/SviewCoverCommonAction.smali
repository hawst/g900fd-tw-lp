.class public Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;
.super Ljava/lang/Object;
.source "SviewCoverCommonAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;,
        Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;,
        Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mMediaReceiver:Landroid/content/BroadcastReceiver;

.field private mOnMediaListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;

.field private mOnPlayerServiceListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;

.field private mOnSystemListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;

.field private final mPlayerServiceReceiver:Landroid/content/BroadcastReceiver;

.field private final mSystemReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$1;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mPlayerServiceReceiver:Landroid/content/BroadcastReceiver;

    .line 109
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$2;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mSystemReceiver:Landroid/content/BroadcastReceiver;

    .line 136
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$3;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    .line 28
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SviewCoverCommonManager() - context: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mContext:Landroid/content/Context;

    .line 30
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mOnPlayerServiceListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mOnSystemListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mOnMediaListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;

    return-object v0
.end method

.method private registerMediaReceiver()V
    .locals 3

    .prologue
    .line 83
    sget-object v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->TAG:Ljava/lang/String;

    const-string v2, "registerMediaReceiver()"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 85
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 86
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 87
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 88
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 89
    return-void
.end method

.method private registerPlayerServiceReceiver()V
    .locals 3

    .prologue
    .line 64
    sget-object v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->TAG:Ljava/lang/String;

    const-string v2, "registerPlayerServiceReceiver()"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 66
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 67
    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 68
    const-string v1, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 69
    const-string v1, "com.samsung.musicplus.action.META_EDITED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 70
    const-string v1, "com.samsung.musicplus.action.PREPARE_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mPlayerServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 72
    return-void
.end method

.method private registerSystemReceiver()V
    .locals 3

    .prologue
    .line 75
    sget-object v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->TAG:Ljava/lang/String;

    const-string v2, "registerSystemReceiver()"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 77
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.musicplus.dlna.connectivitychanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 78
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 79
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mSystemReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 80
    return-void
.end method


# virtual methods
.method public registerReceiver()V
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->TAG:Ljava/lang/String;

    const-string v1, "registerReceiver()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->registerPlayerServiceReceiver()V

    .line 47
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->registerSystemReceiver()V

    .line 48
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->registerMediaReceiver()V

    .line 49
    return-void
.end method

.method public setOnMediaListener(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;)V
    .locals 0
    .param p1, "onMediaListener"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mOnMediaListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;

    .line 42
    return-void
.end method

.method public setOnPlayerServiceListener(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;)V
    .locals 0
    .param p1, "onPlayerServiceListener"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mOnPlayerServiceListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;

    .line 34
    return-void
.end method

.method public setOnSystemListener(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;)V
    .locals 0
    .param p1, "onSystemListener"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mOnSystemListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;

    .line 38
    return-void
.end method

.method public unregisterReceiver()V
    .locals 4

    .prologue
    .line 52
    sget-object v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->TAG:Ljava/lang/String;

    const-string v2, "unregisterReceiver()"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mPlayerServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 55
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mSystemReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 56
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregisterReceiver() - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
