.class public interface abstract Lcom/samsung/musicplus/service/PlayerServiceStateAction;
.super Ljava/lang/Object;
.source "PlayerServiceStateAction.java"


# static fields
.field public static final ACTION_SETTING_CHANGED:Ljava/lang/String; = "com.android.music.settingchanged"

.field public static final ACTION_SOUND_AVLIVE_CHANGED:Ljava/lang/String; = "com.samsung.musicplus.action.SOUND_AVLIE_CHANGED"

.field public static final AV_PLAYER_STATE_CHANGED:Ljava/lang/String; = "com.samsung.musicplus.action.AV_STATE_CHANGED"

.field public static final EXTRA_ALBUM:Ljava/lang/String; = "album"

.field public static final EXTRA_ALBUM_ID:Ljava/lang/String; = "albumId"

.field public static final EXTRA_ARTIST:Ljava/lang/String; = "artist"

.field public static final EXTRA_AUDIO_ID:Ljava/lang/String; = "id"

.field public static final EXTRA_BASE_URI:Ljava/lang/String; = "base_uri"

.field public static final EXTRA_FROM:Ljava/lang/String; = "from"

.field public static final EXTRA_IS_BUFFERING:Ljava/lang/String; = "is_buffering"

.field public static final EXTRA_LIST_COUNT:Ljava/lang/String; = "mediaCount"

.field public static final EXTRA_MEDIA_DURATION:Ljava/lang/String; = "trackLength"

.field public static final EXTRA_MEDIA_POSITION:Ljava/lang/String; = "position"

.field public static final EXTRA_PLAYING:Ljava/lang/String; = "playing"

.field public static final EXTRA_REPEAT:Ljava/lang/String; = "repeat"

.field public static final EXTRA_SHUFFLE:Ljava/lang/String; = "shuffle"

.field public static final EXTRA_TRACK:Ljava/lang/String; = "track"

.field public static final FAVOURITE_STATE_CHANGED:Ljava/lang/String; = "com.samsung.musicplus.favouritechanged"

.field public static final META_CHANGED:Ljava/lang/String; = "com.android.music.metachanged"

.field public static final META_EDITED:Ljava/lang/String; = "com.samsung.musicplus.action.META_EDITED"

.field public static final MUSIC_PLAYER:Ljava/lang/String; = "MusicPlayer"

.field public static final PLAYER_BUFFERING:Ljava/lang/String; = "com.samsung.musicplus.action.PLAYER_BUFFERING"

.field public static final PLAYER_CHANGED:Ljava/lang/String; = "com.samsung.musicplus.action.PLAYER_CHANGED"

.field public static final PLAYSTATE_CHANGED:Ljava/lang/String; = "com.android.music.playstatechanged"

.field public static final PREPARE_COMPLETED:Ljava/lang/String; = "com.samsung.musicplus.action.PREPARE_COMPLETED"

.field public static final QUEUE_CHANGED:Ljava/lang/String; = "com.samsung.musicplus.action.QUEUE_CHANGED"

.field public static final QUEUE_COMPLETED:Ljava/lang/String; = "com.samsung.musicplus.action.QUEUE_COMPLETED"

.field public static final TIME_OUT:Ljava/lang/String; = "time_out"

.field public static final TOGGLE_FAVOURITE:Ljava/lang/String; = "com.samsung.musicplus.action.TOGGLE_FAVORITE"

.field public static final WIFI_DISPLAY:Ljava/lang/String; = "android.intent.action.WIFI_DISPLAY"
