.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$1;
.super Ljava/lang/Object;
.source "SviewCoverService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 92
    const-string v0, "MusicSCover"

    const-string v1, "onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    move-result-object v0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->updateControllerView(I)V

    .line 97
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 101
    const-string v0, "MusicSCover"

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->handleClose()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    .line 103
    return-void
.end method
