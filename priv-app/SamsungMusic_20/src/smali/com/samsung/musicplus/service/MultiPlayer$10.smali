.class Lcom/samsung/musicplus/service/MultiPlayer$10;
.super Ljava/lang/Object;
.source "MultiPlayer.java"

# interfaces
.implements Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/service/MultiPlayer;->setPlayReadyListener(Lcom/samsung/musicplus/library/drm/DrmManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/MultiPlayer;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0

    .prologue
    .line 634
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAcquireStatus(Ljava/lang/String;I)V
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "status"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v8, 0x4

    const/16 v7, 0x18

    .line 638
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAcquireStatus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p2}, Lcom/samsung/musicplus/util/DebugUtils;->getFieldsStringValueNameForDebugging(Ljava/lang/Object;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 644
    .local v6, "data":Landroid/os/Bundle;
    const-string v0, "byUser"

    invoke-virtual {v6, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 645
    packed-switch p2, :pswitch_data_0

    .line 673
    :goto_0
    return-void

    .line 647
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v8, v7, v3, v6}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 652
    :pswitch_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2200(Lcom/samsung/musicplus/service/MultiPlayer;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2300(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2200(Lcom/samsung/musicplus/service/MultiPlayer;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z
    invoke-static {v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$500(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v3

    iget-object v4, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mSeekPosition:J
    invoke-static {v4}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1300(Lcom/samsung/musicplus/service/MultiPlayer;)J

    move-result-wide v4

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->setDataSourceInternal(Landroid/net/Uri;Ljava/lang/String;ZJ)Z
    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2400(Lcom/samsung/musicplus/service/MultiPlayer;Landroid/net/Uri;Ljava/lang/String;ZJ)Z

    .line 657
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v8, v7, v2, v6}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 662
    :pswitch_2
    const-string v0, "type"

    invoke-virtual {v6, v0, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 663
    const-string v0, "text1"

    const/4 v1, 0x5

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 665
    const-string v0, "path"

    invoke-virtual {v6, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$10;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v8, v7, v2, v6}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 645
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
