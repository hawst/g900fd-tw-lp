.class Lcom/samsung/musicplus/service/PlayerService$4;
.super Landroid/os/Handler;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 676
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$4;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 680
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$4;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$4;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$1800(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$4;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mServiceInUse:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$2300(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$4;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$4;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$2400(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 694
    :cond_0
    :goto_0
    return-void

    .line 689
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$4;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$4;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mServiceStartId:I
    invoke-static {v1}, Lcom/samsung/musicplus/service/PlayerService;->access$2500(Lcom/samsung/musicplus/service/PlayerService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->stopSelf(I)V

    .line 691
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 692
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$4;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mIsStopPacakgeAfterDestroyed:Z
    invoke-static {v0, v2}, Lcom/samsung/musicplus/service/PlayerService;->access$2602(Lcom/samsung/musicplus/service/PlayerService;Z)Z

    goto :goto_0
.end method
