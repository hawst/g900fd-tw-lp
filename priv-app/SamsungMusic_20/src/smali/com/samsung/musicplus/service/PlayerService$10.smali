.class Lcom/samsung/musicplus/service/PlayerService$10;
.super Landroid/content/BroadcastReceiver;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mIsRinging:Z

.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 1

    .prologue
    .line 1207
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService$10;->mIsRinging:Z

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1213
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1214
    .local v0, "action":Ljava/lang/String;
    const-string v6, "MusicService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mSystemReceiver action : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220
    const-string v6, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1221
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z
    invoke-static {v6}, Lcom/samsung/musicplus/service/PlayerService;->access$2400(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1230
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v6, v9}, Lcom/samsung/musicplus/service/PlayerService;->stopForeground(Z)V

    .line 1231
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->updateNotificationAllInfo()V
    invoke-static {v6}, Lcom/samsung/musicplus/service/PlayerService;->access$4300(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 1289
    :cond_0
    :goto_0
    return-void

    .line 1233
    :cond_1
    const-string v6, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1234
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->stop()V
    invoke-static {v6}, Lcom/samsung/musicplus/service/PlayerService;->access$4400(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 1236
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v6}, Lcom/samsung/musicplus/service/PlayerService;->stopSelf()V

    goto :goto_0

    .line 1237
    :cond_2
    const-string v6, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1238
    const-string v6, "status"

    invoke-virtual {p2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 1240
    .local v5, "status":I
    const-string v6, "level"

    const/4 v7, -0x1

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1241
    .local v2, "level":I
    const-string v6, "MusicService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mSystemReceiver - batteryLevel: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " batteryStatus: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1242
    const/4 v6, 0x2

    if-eq v5, v6, :cond_0

    if-gt v2, v9, :cond_0

    .line 1244
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v6}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    .line 1245
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v6}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/musicplus/util/UiUtils;->isMusicUiTop(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1246
    new-instance v1, Landroid/content/Intent;

    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const-class v7, Lcom/samsung/musicplus/dialog/LowBatteryPopup;

    invoke-direct {v1, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1247
    .local v1, "i":Landroid/content/Intent;
    const/high16 v6, 0x40000000    # 2.0f

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1248
    const/high16 v6, 0x10000000

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1249
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v6, v1}, Lcom/samsung/musicplus/service/PlayerService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1251
    .end local v1    # "i":Landroid/content/Intent;
    :cond_3
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const v7, 0x7f1000a3

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->simpleWarningNoti(I)V
    invoke-static {v6, v7}, Lcom/samsung/musicplus/service/PlayerService;->access$4500(Lcom/samsung/musicplus/service/PlayerService;I)V

    .line 1252
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->stop()V
    invoke-static {v6}, Lcom/samsung/musicplus/service/PlayerService;->access$4400(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 1253
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v6}, Lcom/samsung/musicplus/service/PlayerService;->stopSelf()V

    goto/16 :goto_0

    .line 1256
    .end local v2    # "level":I
    .end local v5    # "status":I
    :cond_4
    const-string v6, "android.intent.action.PHONE_STATE"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1258
    const-string v6, "state"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1259
    .local v4, "state":Ljava/lang/String;
    sget-object v6, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1260
    iput-boolean v9, p0, Lcom/samsung/musicplus/service/PlayerService$10;->mIsRinging:Z

    .line 1261
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const-string v7, "Call On"

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->unregisterGestureListener(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/musicplus/service/PlayerService;->access$4600(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1262
    :cond_5
    sget-object v6, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1263
    iput-boolean v10, p0, Lcom/samsung/musicplus/service/PlayerService$10;->mIsRinging:Z

    .line 1264
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const-string v7, "Call Off"

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->registerGestureListener(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/musicplus/service/PlayerService;->access$4700(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1266
    .end local v4    # "state":Ljava/lang/String;
    :cond_6
    const-string v6, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1267
    const-string v6, "noConnectivity"

    invoke-virtual {p2, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 1269
    .local v3, "noNetwork":Z
    const-string v6, "MusicService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " There are no network : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1270
    if-eqz v3, :cond_0

    .line 1271
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isSlinkTrackList()Z
    invoke-static {v6}, Lcom/samsung/musicplus/service/PlayerService;->access$4800(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1273
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->resetNowPlayingList()V
    invoke-static {v6}, Lcom/samsung/musicplus/service/PlayerService;->access$4900(Lcom/samsung/musicplus/service/PlayerService;)V

    goto/16 :goto_0

    .line 1278
    .end local v3    # "noNetwork":Z
    :cond_7
    iget-boolean v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->mIsRinging:Z

    if-nez v6, :cond_0

    .line 1279
    const-string v6, "android.intent.action.SCREEN_ON"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1280
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const-string v7, "Screen on"

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->registerGestureListener(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/musicplus/service/PlayerService;->access$4700(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V

    .line 1281
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->updateNotificationTextMarquee(Z)V
    invoke-static {v6, v9}, Lcom/samsung/musicplus/service/PlayerService;->access$5000(Lcom/samsung/musicplus/service/PlayerService;Z)V

    goto/16 :goto_0

    .line 1282
    :cond_8
    const-string v6, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    const-string v6, "android.intent.action.USER_PRESENT"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1284
    :cond_9
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const-string v7, "Screen off or USER_PRESENT"

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->unregisterGestureListener(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/musicplus/service/PlayerService;->access$4600(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V

    .line 1285
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$10;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->updateNotificationTextMarquee(Z)V
    invoke-static {v6, v10}, Lcom/samsung/musicplus/service/PlayerService;->access$5000(Lcom/samsung/musicplus/service/PlayerService;Z)V

    goto/16 :goto_0
.end method
