.class Lcom/samsung/musicplus/service/PlayerService$16;
.super Landroid/os/Handler;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 2170
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$16;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2173
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDlnaHandler what : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2174
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$16;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$6300(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/dlna/DlnaManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2187
    :goto_0
    return-void

    .line 2177
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2179
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$16;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$6300(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/dlna/DlnaManager;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->searchAudioContents(Ljava/lang/String;)V

    goto :goto_0

    .line 2182
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$16;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$6300(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/dlna/DlnaManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->refresh()V

    goto :goto_0

    .line 2177
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
