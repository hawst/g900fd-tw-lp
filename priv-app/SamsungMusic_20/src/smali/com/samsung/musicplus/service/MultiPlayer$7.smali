.class Lcom/samsung/musicplus/service/MultiPlayer$7;
.super Ljava/lang/Object;
.source "MultiPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/MultiPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/MultiPlayer;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0

    .prologue
    .line 387
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer$7;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 390
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError what : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " extra : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    packed-switch p2, :pswitch_data_0

    .line 400
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$7;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->handlingExtraErrors(II)Z
    invoke-static {v0, p2, p3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1900(Lcom/samsung/musicplus/service/MultiPlayer;II)Z

    move-result v0

    :goto_0
    return v0

    .line 394
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$7;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 395
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$7;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->recreateMediaPlayer()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1800(Lcom/samsung/musicplus/service/MultiPlayer;)V

    .line 396
    const/4 v0, 0x1

    goto :goto_0

    .line 392
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method
