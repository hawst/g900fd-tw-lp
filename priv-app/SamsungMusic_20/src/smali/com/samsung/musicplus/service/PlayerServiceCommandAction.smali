.class public interface abstract Lcom/samsung/musicplus/service/PlayerServiceCommandAction;
.super Ljava/lang/Object;
.source "PlayerServiceCommandAction.java"


# static fields
.field public static final ACTION_AUDIO_PATH_CHANGED:Ljava/lang/String; = "com.samsung.musicplus.action.AUDIO_PATH_CHANGED"

.field public static final ACTION_CLEAR_COVER_MUSIC_UPDATE:Ljava/lang/String; = "com.samsung.musicplus.intent.action.CLEAR_COVER_MUSIC_UPDATE"

.field public static final ACTION_DRM_REQUEST:Ljava/lang/String; = "com.samsung.musicplus.action.DRM_REQUEST"

.field public static final ACTION_FF_DOWN:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.ff.down"

.field public static final ACTION_FF_UP:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.ff.up"

.field public static final ACTION_GOOGLE_LEGACY_COMMAND:Ljava/lang/String; = "com.android.music.musicservicecommand"

.field public static final ACTION_HIDE_CONTEXTUAL_WIDGET:Ljava/lang/String; = "com.sec.android.app.music.intent.action.HIDE_CONTEXTUAL_WIDGET"

.field public static final ACTION_MUSIC_AUTO_OFF:Ljava/lang/String; = "com.samsung.musicplus.intent.action.MUSIC_AUTO_OFF"

.field public static final ACTION_MUSIC_CLOSE:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.ACTION_MUSIC_CLOSE"

.field public static final ACTION_MUSIC_K2HD:Ljava/lang/String; = "com.sec.android.app.music.intent.action.k2hd"

.field public static final ACTION_MUSIC_STATUS_INFO:Ljava/lang/String; = "com.samsung.musicplus.info"

.field public static final ACTION_PALM_DOWN:Ljava/lang/String; = "android.intent.action.PALM_DOWN"

.field public static final ACTION_PLAYBACK_FORWARD:Ljava/lang/String; = "com.samsung.musicplus.action.PLAYBACK_FORWARD"

.field public static final ACTION_PLAYBACK_REWIND:Ljava/lang/String; = "com.samsung.musicplus.action.PLAYBACK_REWIND"

.field public static final ACTION_PLAY_WIDGET_LIST:Ljava/lang/String; = "com.samsung.musicplus.intent.action.PLAY_WIDGET_LIST"

.field public static final ACTION_REQUEST_MUSIC_INFO:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.checkplaystatus"

.field public static final ACTION_REW_DOWN:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.rew.down"

.field public static final ACTION_REW_UP:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.rew.up"

.field public static final ACTION_SEND_MUSIC_INFO:Ljava/lang/String; = "com.sec.android.music.musicservicecommnad.mediainfo"

.field public static final ACTION_SERVICE_CMD:Ljava/lang/String; = "com.samsung.musicplus.action.SERVICE_COMMAND"

.field public static final ACTION_SHOW_CONTEXTUAL_WIDGET:Ljava/lang/String; = "com.sec.android.app.music.intent.action.SHOW_CONTEXTUAL_WIDGET"

.field public static final ACTION_SS_NEXT:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.next"

.field public static final ACTION_SS_PAUSE:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.pause"

.field public static final ACTION_SS_PREVIOUS:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.previous"

.field public static final ACTION_SS_TOGGLEPAUSE:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.togglepause"

.field public static final ACTION_SS_TOGGLE_REPEAT:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.repeat"

.field public static final ACTION_SS_TOGGLE_SHUFFLE:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.shuffle"

.field public static final ACTION_UPDATE_WIDGET:Ljava/lang/String; = "com.samsung.musicplus.intent.action.UPDATE_WIDGET"

.field public static final ACTION_WRITE_LOG:Ljava/lang/String; = "com.sec.android.app.music.intent.action.WRITE_LOG"

.field public static final BROADCAST_PERMISSTION:Ljava/lang/String; = "com.sec.android.app.musiccontroller.broastcasting.permission"

.field public static final EXTRA_CMD_FASTFORWARD:Ljava/lang/String; = "fastforward"

.field public static final EXTRA_CMD_FASTFORWARD_DOWN:Ljava/lang/String; = "fastforward_down"

.field public static final EXTRA_CMD_FASTFORWARD_UP:Ljava/lang/String; = "fastforward_up"

.field public static final EXTRA_CMD_IS_BT:Ljava/lang/String; = "is_bt"

.field public static final EXTRA_CMD_NAME:Ljava/lang/String; = "command"

.field public static final EXTRA_CMD_NEXT:Ljava/lang/String; = "next"

.field public static final EXTRA_CMD_OPEN:Ljava/lang/String; = "openList"

.field public static final EXTRA_CMD_PAUSE:Ljava/lang/String; = "pause"

.field public static final EXTRA_CMD_PLAY:Ljava/lang/String; = "play"

.field public static final EXTRA_CMD_PREVIOUS:Ljava/lang/String; = "previous"

.field public static final EXTRA_CMD_REWIND:Ljava/lang/String; = "rewind"

.field public static final EXTRA_CMD_REWIND_DOWN:Ljava/lang/String; = "rewind_down"

.field public static final EXTRA_CMD_REWIND_UP:Ljava/lang/String; = "rewind_up"

.field public static final EXTRA_CMD_START_ACQUIRE_RIGHTS:Ljava/lang/String; = "startRights"

.field public static final EXTRA_CMD_STOP:Ljava/lang/String; = "stop"

.field public static final EXTRA_CMD_SUCCESS_ACQUIRE_RIGHTS:Ljava/lang/String; = "successRights"

.field public static final EXTRA_CMD_TOGGLEPAUSE:Ljava/lang/String; = "togglepause"

.field public static final EXTRA_CMD_VOLUME_DOWN:Ljava/lang/String; = "volume_down"

.field public static final EXTRA_CMD_VOLUME_UP:Ljava/lang/String; = "volume_up"

.field public static final EXTRA_DMR_DEVICE:Ljava/lang/String; = "dmr_device"

.field public static final EXTRA_FORCE:Ljava/lang/String; = "force"

.field public static final EXTRA_KEY:Ljava/lang/String; = "listQueryKey"

.field public static final EXTRA_LIST:Ljava/lang/String; = "list"

.field public static final EXTRA_LIST_POSITION:Ljava/lang/String; = "listPosition"

.field public static final EXTRA_SVOICE:Ljava/lang/String; = "svoice_command"

.field public static final EXTRA_TYPE:Ljava/lang/String; = "listType"

.field public static final MUSIC_SERVICE_TIMEOUT:I = 0x1d4c0
