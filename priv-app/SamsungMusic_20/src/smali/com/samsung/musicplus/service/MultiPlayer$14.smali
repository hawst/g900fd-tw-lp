.class Lcom/samsung/musicplus/service/MultiPlayer$14;
.super Ljava/lang/Object;
.source "MultiPlayer.java"

# interfaces
.implements Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/MultiPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/MultiPlayer;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0

    .prologue
    .line 1682
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStateChanged(I)V
    .locals 5
    .param p1, "state"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1687
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AV player onStateChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
    invoke-static {v2}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2700(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/samsung/musicplus/util/DebugUtils;->getFieldsStringValueNameForDebugging(Ljava/lang/Object;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1694
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3100(Lcom/samsung/musicplus/service/MultiPlayer;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1695
    const-string v0, "MusicPlayer"

    const-string v1, "Current mode is not dmr player so ignore updating "

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1697
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.musicplus.action.AV_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1785
    :goto_0
    return-void

    .line 1701
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1779
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsBuffering:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3202(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    .line 1784
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.musicplus.action.AV_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 1703
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsBuffering:Z
    invoke-static {v0, v4}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3202(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    goto :goto_1

    .line 1706
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsPlayRequest:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3300(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1710
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->stopBuffering()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1000(Lcom/samsung/musicplus/service/MultiPlayer;)V

    .line 1711
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$500(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1712
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$502(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    .line 1713
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->updatePlayingState()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3400(Lcom/samsung/musicplus/service/MultiPlayer;)V

    .line 1721
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsBuffering:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3202(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    goto :goto_1

    .line 1724
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsPauseRequest:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3502(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    .line 1725
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$500(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1726
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$502(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    .line 1727
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->updatePlayingState()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3400(Lcom/samsung/musicplus/service/MultiPlayer;)V

    .line 1729
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->stopBuffering()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1000(Lcom/samsung/musicplus/service/MultiPlayer;)V

    .line 1730
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsBuffering:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3202(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    goto :goto_1

    .line 1733
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsPrepareRequest:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3602(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    goto :goto_1

    .line 1736
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsPrepareRequest:Z
    invoke-static {v0, v4}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3602(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    goto :goto_1

    .line 1739
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsBuffering:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3202(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    .line 1741
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsPauseRequest:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3502(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    .line 1742
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1200(Lcom/samsung/musicplus/service/MultiPlayer;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    .line 1743
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsPrepareRequest:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3600(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1744
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsPrepareRequest:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3602(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    .line 1745
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    const/4 v1, 0x5

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1202(Lcom/samsung/musicplus/service/MultiPlayer;I)I

    .line 1746
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mMultiPlayerPreparedListener:Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1600(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1757
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$502(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    .line 1758
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mMultiPlayerPreparedListener:Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1600(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;->onPrepared(Z)V

    .line 1766
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$500(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1769
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z
    invoke-static {v0, v4}, Lcom/samsung/musicplus/service/MultiPlayer;->access$502(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    .line 1770
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->updatePlayingState()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3400(Lcom/samsung/musicplus/service/MultiPlayer;)V

    .line 1772
    :cond_5
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->stopBuffering()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1000(Lcom/samsung/musicplus/service/MultiPlayer;)V

    .line 1774
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3800(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1775
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3800(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    goto/16 :goto_1

    .line 1761
    :cond_6
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsPlayRequest:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3300(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSeekRequest:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3700(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsPrepareRequest:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3600(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1762
    :cond_7
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsPlayRequest:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3302(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    .line 1763
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSeekRequest:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3702(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    .line 1764
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$14;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsPrepareRequest:Z
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3602(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z

    goto :goto_2

    .line 1701
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_6
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
