.class Lcom/samsung/musicplus/service/PlayerService$11;
.super Ljava/lang/Object;
.source "PlayerService.java"

# interfaces
.implements Lcom/samsung/musicplus/service/PlayerListManager$OnListChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/service/PlayerService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 1347
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$11;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged(ZZ)V
    .locals 3
    .param p1, "currentSongChanged"    # Z
    .param p2, "queueChanged"    # Z

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$11;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1358
    const-string v0, "MusicService"

    const-string v1, "onChanged, media server died!"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    if-eqz p2, :cond_0

    .line 1365
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$11;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const-string v1, "com.samsung.musicplus.action.QUEUE_CHANGED"

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$5100(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V

    .line 1367
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$11;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getListItemCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 1368
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$11;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->stop()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$4400(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 1369
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$11;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.musiccontroller.TIMEOUT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.sec.android.app.musiccontroller.broastcasting.permission"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1388
    :cond_1
    :goto_0
    return-void

    .line 1373
    :cond_2
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChanged currentSongChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " queueChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1375
    if-eqz p1, :cond_3

    .line 1376
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$11;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$11;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v1

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->openInternal(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$300(Lcom/samsung/musicplus/service/PlayerService;Z)V

    .line 1378
    :cond_3
    if-eqz p2, :cond_4

    .line 1381
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$11;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const-string v1, "com.samsung.musicplus.action.QUEUE_CHANGED"

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$5100(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V

    .line 1384
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$11;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getListItemCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 1385
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$11;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->stop()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$4400(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 1386
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$11;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.musiccontroller.TIMEOUT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.sec.android.app.musiccontroller.broastcasting.permission"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0
.end method
