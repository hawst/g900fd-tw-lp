.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity$1;
.super Ljava/lang/Object;
.source "SviewCoverActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 30
    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;

    new-instance v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;

    invoke-direct {v1, v2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->access$102(Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .line 32
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    move-result-object v0

    new-instance v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity$1$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity$1$1;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity$1;)V

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->setOnDismissCallback(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$OnDismissCallback;)V

    .line 38
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->mSviewCoverDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->show()V

    .line 39
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 43
    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->dismissSviewCoverDialog()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;->access$200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverActivity;)V

    .line 45
    return-void
.end method
