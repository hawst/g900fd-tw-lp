.class Lcom/samsung/musicplus/service/PlayerService$1;
.super Landroid/os/Handler;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mCurrentVolume:F

.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 1

    .prologue
    .line 375
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 376
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mMaxVolume:F
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$000(Lcom/samsung/musicplus/service/PlayerService;)F

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    return-void
.end method

.method private handleAudioFocus(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    .line 606
    packed-switch p1, :pswitch_data_0

    .line 645
    :pswitch_0
    const-string v0, "MusicService"

    const-string v1, "Unknown audio focus change code"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    :cond_0
    :goto_0
    return-void

    .line 608
    :pswitch_1
    const-string v0, "MusicService"

    const-string v1, "AudioFocus: received AUDIOFOCUS_LOSS"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$1400(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 610
    const-string v0, "MusicService"

    const-string v1, "AudioFocus: received AUDIOFOCUS_LOSS in DMR case so ignore it."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 613
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    goto :goto_0

    .line 619
    :pswitch_2
    const-string v0, "MusicService"

    const-string v1, "AudioFocus: received AUDIOFOCUS_LOSS_TRANSIENT"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$1400(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 621
    const-string v0, "MusicService"

    const-string v1, "AudioFocus: received AUDIOFOCUS_LOSS_TRANSIENT in DMR case so ignore it."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 625
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 626
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$1802(Lcom/samsung/musicplus/service/PlayerService;Z)Z

    .line 629
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    goto :goto_0

    .line 633
    :pswitch_3
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioFocus: received AUDIOFOCUS_GAIN paused by focus before ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v2}, Lcom/samsung/musicplus/service/PlayerService;->access$1800(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$1800(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 636
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    .line 637
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->setVolume(F)V

    .line 638
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->play(Z)V

    goto/16 :goto_0

    .line 640
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 641
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 606
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private handleDrmRequest(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x0

    .line 531
    const/4 v1, 0x0

    .line 532
    .local v1, "data":Landroid/os/Bundle;
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v3, v3, Landroid/os/Bundle;

    if-eqz v3, :cond_0

    .line 533
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v1    # "data":Landroid/os/Bundle;
    check-cast v1, Landroid/os/Bundle;

    .line 535
    .restart local v1    # "data":Landroid/os/Bundle;
    :cond_0
    if-nez v1, :cond_1

    .line 536
    .local v0, "byUser":Z
    :goto_0
    iget v3, p1, Landroid/os/Message;->arg2:I

    packed-switch v3, :pswitch_data_0

    .line 553
    invoke-direct {p0, v1, v0}, Lcom/samsung/musicplus/service/PlayerService$1;->normalDrmRequest(Landroid/os/Bundle;Z)V

    .line 556
    :goto_1
    return-void

    .line 535
    .end local v0    # "byUser":Z
    :cond_1
    const-string v3, "byUser"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 538
    .restart local v0    # "byUser":Z
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v3}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/musicplus/service/MultiPlayer;->stop()V

    .line 540
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.samsung.musicplus.action.DRM_REQUEST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 541
    .local v2, "i":Landroid/content/Intent;
    const-string v3, "command"

    const-string v4, "startRights"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 542
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v3, v2}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 547
    .end local v2    # "i":Landroid/content/Intent;
    :pswitch_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.samsung.musicplus.action.DRM_REQUEST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 548
    .restart local v2    # "i":Landroid/content/Intent;
    const-string v3, "command"

    const-string v4, "successRights"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 549
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v3, v2}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 536
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private normalDrmRequest(Landroid/os/Bundle;Z)V
    .locals 4
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "byUser"    # Z

    .prologue
    .line 565
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isMusicUiTop(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 566
    const-string v0, "MusicService"

    const-string v1, "open drm fail ui top"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->stop()V

    .line 580
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$1$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/musicplus/service/PlayerService$1$1;-><init>(Lcom/samsung/musicplus/service/PlayerService$1;Landroid/os/Bundle;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/musicplus/service/PlayerService$1;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 603
    :cond_0
    :goto_0
    return-void

    .line 595
    :cond_1
    const-string v0, "MusicService"

    const-string v1, "open drm fail ui background"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mErrorCount:I
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$200(Lcom/samsung/musicplus/service/PlayerService;)I

    move-result v0

    const/16 v1, 0x14

    if-gt v0, v1, :cond_0

    .line 600
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # operator++ for: Lcom/samsung/musicplus/service/PlayerService;->mErrorCount:I
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$208(Lcom/samsung/musicplus/service/PlayerService;)I

    .line 601
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->errorHandleToPrevNext(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$1000(Lcom/samsung/musicplus/service/PlayerService;Z)V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v8, 0x1e

    const/16 v6, 0xf

    const v5, 0x3e4ccccd    # 0.2f

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 380
    const-string v2, "MusicService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mPlayerHandler what : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 523
    :cond_0
    :goto_0
    return-void

    .line 383
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$100(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/PlayerListManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerListManager;->moveToNext(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 385
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mErrorCount:I
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$202(Lcom/samsung/musicplus/service/PlayerService;I)I

    .line 387
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v1

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->openInternal(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$300(Lcom/samsung/musicplus/service/PlayerService;Z)V

    goto :goto_0

    .line 389
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->isDrm()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 392
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->openInternal(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$300(Lcom/samsung/musicplus/service/PlayerService;Z)V

    .line 397
    :goto_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mIsSeekReady:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$502(Lcom/samsung/musicplus/service/PlayerService;Z)Z

    .line 398
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 399
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v6, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 401
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 404
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->setCancel()V

    .line 407
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.musicplus.action.QUEUE_COMPLETED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 394
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    .line 395
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/musicplus/service/PlayerService;->seek(J)J

    goto :goto_1

    .line 411
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mIsSeekReady:Z
    invoke-static {v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->access$502(Lcom/samsung/musicplus/service/PlayerService;Z)Z

    goto :goto_0

    .line 414
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService$1;->handleDrmRequest(Landroid/os/Message;)V

    goto :goto_0

    .line 417
    :pswitch_3
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->sendPlayerBuffering(Z)V
    invoke-static {v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->access$800(Lcom/samsung/musicplus/service/PlayerService;Z)V

    goto/16 :goto_0

    .line 420
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->sendPlayerBuffering(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$800(Lcom/samsung/musicplus/service/PlayerService;Z)V

    goto/16 :goto_0

    .line 423
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isMusicUiTop(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 424
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 428
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->savePlayPosition()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$900(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 429
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    goto/16 :goto_0

    .line 432
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mErrorCount:I
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$200(Lcom/samsung/musicplus/service/PlayerService;)I

    move-result v0

    const/16 v2, 0x14

    if-le v0, v2, :cond_5

    .line 433
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    goto/16 :goto_0

    .line 436
    :cond_5
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # operator++ for: Lcom/samsung/musicplus/service/PlayerService;->mErrorCount:I
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$208(Lcom/samsung/musicplus/service/PlayerService;)I

    .line 437
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->errorHandleToPrevNext(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$1000(Lcom/samsung/musicplus/service/PlayerService;Z)V

    goto/16 :goto_0

    .line 441
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.musicplus.action.PLAYER_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 442
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->updateNotificationConnectivityStatus()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$1100(Lcom/samsung/musicplus/service/PlayerService;)V

    goto/16 :goto_0

    .line 445
    :pswitch_7
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$1200(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->notifyPlayStatusChanged()V

    .line 446
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$1200(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->updateNotificationPlayStatus()V

    .line 447
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isPreparing()Z

    move-result v2

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v3

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->position()J

    move-result-wide v4

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getPlaySpeed()F

    move-result v6

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->setPlaybackState(ZZJF)V
    invoke-static/range {v1 .. v6}, Lcom/samsung/musicplus/service/PlayerService;->access$1300(Lcom/samsung/musicplus/service/PlayerService;ZZJF)V

    goto/16 :goto_0

    .line 450
    :pswitch_8
    iget v2, p1, Landroid/os/Message;->arg1:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    .line 451
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/service/MultiPlayer;->changeToDmrPlayer(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 453
    :cond_6
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z
    invoke-static {v2}, Lcom/samsung/musicplus/service/PlayerService;->access$1400(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 454
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerService;->isPreparing()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 457
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const v3, 0x7f1001b2

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->showToast(I)V
    invoke-static {v2, v3}, Lcom/samsung/musicplus/service/PlayerService;->access$1500(Lcom/samsung/musicplus/service/PlayerService;I)V

    .line 462
    :cond_7
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v2}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg2:I

    const/16 v4, 0x64

    if-ne v3, v4, :cond_8

    :goto_2
    invoke-virtual {v2, v0}, Lcom/samsung/musicplus/service/MultiPlayer;->changeToDefaultPlayer(Z)V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_2

    .line 466
    :pswitch_9
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 470
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->play(Z)V

    goto/16 :goto_0

    .line 476
    :cond_9
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->openInternal(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$300(Lcom/samsung/musicplus/service/PlayerService;Z)V

    goto/16 :goto_0

    .line 480
    :pswitch_a
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    const v1, 0x3d4ccccd    # 0.05f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    .line 481
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_a

    .line 482
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 486
    :goto_3
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->setVolume(F)V

    goto/16 :goto_0

    .line 484
    :cond_a
    iput v5, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    goto :goto_3

    .line 489
    :pswitch_b
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    const v1, 0x3c23d70a    # 0.01f

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    .line 490
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mMaxVolume:F
    invoke-static {v1}, Lcom/samsung/musicplus/service/PlayerService;->access$000(Lcom/samsung/musicplus/service/PlayerService;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_b

    .line 491
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 495
    :goto_4
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->setVolume(F)V

    goto/16 :goto_0

    .line 493
    :cond_b
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mMaxVolume:F
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$000(Lcom/samsung/musicplus/service/PlayerService;)F

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->mCurrentVolume:F

    goto :goto_4

    .line 500
    :pswitch_c
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService$1;->handleAudioFocus(I)V

    goto/16 :goto_0

    .line 503
    :pswitch_d
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mRepeatCount:I
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$1602(Lcom/samsung/musicplus/service/PlayerService;I)I

    goto/16 :goto_0

    .line 506
    :pswitch_e
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;
    invoke-static {v1}, Lcom/samsung/musicplus/service/PlayerService;->access$100(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/PlayerListManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getOrganizedQueue()[J

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->saveNowPlayingQueue(Landroid/content/Context;[J)V

    goto/16 :goto_0

    .line 510
    :pswitch_f
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isPreparing()Z

    move-result v2

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v3

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->position()J

    move-result-wide v4

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getPlaySpeed()F

    move-result v6

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->setPlaybackState(ZZJF)V
    invoke-static/range {v1 .. v6}, Lcom/samsung/musicplus/service/PlayerService;->access$1300(Lcom/samsung/musicplus/service/PlayerService;ZZJF)V

    goto/16 :goto_0

    .line 513
    :pswitch_10
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mIgnoreNoisy:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$1702(Lcom/samsung/musicplus/service/PlayerService;Z)Z

    goto/16 :goto_0

    .line 516
    :pswitch_11
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 517
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$1;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->setCancel()V

    goto/16 :goto_0

    .line 381
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_9
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_f
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_1
        :pswitch_e
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method
