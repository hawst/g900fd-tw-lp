.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;
.super Lcom/samsung/musicplus/widget/RemoteViewBuilder;
.source "SviewCoverReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SviewCoverRemoteViewBuilder"
.end annotation


# instance fields
.field private final mBinder:Landroid/os/Binder;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutId"    # I

    .prologue
    .line 212
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;-><init>(Landroid/content/Context;I)V

    .line 214
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mBinder:Landroid/os/Binder;

    .line 215
    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;
    .param p1, "x1"    # Z

    .prologue
    .line 207
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->showProgress(Z)V

    return-void
.end method

.method private showProgress(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 288
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v2, 0x7f0d01ba

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 289
    return-void

    .line 288
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 1
    .param p1, "album"    # Landroid/graphics/Bitmap;

    .prologue
    .line 267
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v0

    return-object v0
.end method

.method protected setButtons()Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 219
    invoke-super {p0}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setButtons()Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 221
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.samsung.musicplus.musicservicecommand.rew.down"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v9, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 223
    .local v4, "rewindButtonDown":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.samsung.musicplus.musicservicecommand.rew.up"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v9, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 226
    .local v5, "rewindButtonUp":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.samsung.musicplus.musicservicecommand.ff.down"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v9, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 228
    .local v0, "fastforwardButtonDown":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.samsung.musicplus.musicservicecommand.ff.up"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v9, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 231
    .local v1, "fastforwardButtonUp":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v7, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mBinder:Landroid/os/Binder;

    const v8, 0x7f0d00f9

    invoke-static {v6, v7, v8, v4, v5}, Lcom/samsung/musicplus/library/view/RemoteViewsCompat;->setOnLongClickPendingIntent(Landroid/widget/RemoteViews;Landroid/os/IBinder;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 233
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v7, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mBinder:Landroid/os/Binder;

    const v8, 0x7f0d00fc

    invoke-static {v6, v7, v8, v0, v1}, Lcom/samsung/musicplus/library/view/RemoteViewsCompat;->setOnLongClickPendingIntent(Landroid/widget/RemoteViews;Landroid/os/IBinder;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 236
    new-instance v2, Landroid/content/Intent;

    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mContext:Landroid/content/Context;

    const-class v7, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 237
    .local v2, "i":Landroid/content/Intent;
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mContext:Landroid/content/Context;

    invoke-static {v6, v9, v2, v9}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 238
    .local v3, "pi":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v7, 0x7f0d0054

    invoke-static {v6, v7, v3}, Lcom/samsung/musicplus/library/view/RemoteViewsCompat;->setLaunchPendingIntent(Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;)V

    .line 240
    return-object p0
.end method

.method public setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 1
    .param p1, "listType"    # I

    .prologue
    .line 245
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v0

    return-object v0
.end method

.method public setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 4
    .param p1, "listType"    # I
    .param p2, "isPlaying"    # Z

    .prologue
    const v3, 0x7f0d00fa

    .line 251
    if-eqz p2, :cond_0

    .line 252
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0200c7

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 254
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mContext:Landroid/content/Context;

    const v2, 0x7f100193

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 262
    :goto_0
    return-object p0

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0200cb

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 259
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mContext:Landroid/content/Context;

    const v2, 0x7f100194

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 6
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const v4, 0x7f0d00d6

    const v3, 0x7f0d00d5

    .line 272
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 273
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v4, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 274
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v3, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 281
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const-string v1, "setSelected"

    invoke-virtual {v0, v4, v1, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 282
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const-string v1, "setSelected"

    invoke-virtual {v0, v3, v1, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 284
    return-object p0

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f100104

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 278
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const-string v1, ""

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0
.end method
