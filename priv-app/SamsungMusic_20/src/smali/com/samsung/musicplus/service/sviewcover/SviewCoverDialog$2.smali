.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$2;
.super Landroid/os/Handler;
.source "SviewCoverDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$2;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 207
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$2;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->showTitleLayout()V
    invoke-static {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V

    .line 208
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 222
    :goto_0
    return-void

    .line 210
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 211
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 212
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$2;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 213
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$2;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mBackgroundView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$300(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$2;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$2;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$400(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f02003b

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getDefaulAlbumArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 216
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$2;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mBackgroundView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$300(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$2;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$400(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f020039

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getDefaulAlbumArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 208
    nop

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
    .end packed-switch
.end method
