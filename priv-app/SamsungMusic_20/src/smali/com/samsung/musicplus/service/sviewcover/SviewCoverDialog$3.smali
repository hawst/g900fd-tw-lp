.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;
.super Ljava/lang/Object;
.source "SviewCoverDialog.java"

# interfaces
.implements Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMetaChanged()V
    .locals 4

    .prologue
    .line 235
    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onMetaChanged()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListItemCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->invokeDismissCallback()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V

    .line 244
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getArtist()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->updateSongInfo(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$600(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getAlbumId()J

    move-result-wide v2

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v1

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->updateAlbumArt(JI)V
    invoke-static {v0, v2, v3, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$700(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;JI)V

    .line 242
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$800(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->setPlayingId()V

    .line 243
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$800(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->invalidateViews()V

    goto :goto_0
.end method

.method public onMetaEditedOrQueueChanged()V
    .locals 2

    .prologue
    .line 258
    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onMetaEditedOrQueueChanged()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$800(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->invalidateViews()V

    .line 260
    return-void
.end method

.method public onPlayStateChanged()V
    .locals 2

    .prologue
    .line 248
    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPlayStateChanged()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListItemCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->invokeDismissCallback()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V

    .line 254
    :goto_0
    return-void

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$800(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->invalidateViews()V

    goto :goto_0
.end method

.method public onPrepareCompleted()V
    .locals 2

    .prologue
    .line 264
    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPrepareCompleted()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtProgress:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$900(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$800(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->invalidateViews()V

    .line 267
    return-void
.end method
