.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;
.super Landroid/os/Handler;
.source "SviewCoverReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 64
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    move-result-object v0

    if-nez v0, :cond_1

    .line 66
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->CLASSNAME:Ljava/lang/String;

    const-string v1, "Failed to get network album art - album art thread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->clearAlbumCache()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;)V

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    # setter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mAlbumArtCache:Landroid/graphics/Bitmap;
    invoke-static {v1, v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->access$202(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mAlbumArtCache:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->access$200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 72
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    move-result-object v0

    const/4 v1, 0x0

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->showProgress(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->access$300(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;Z)V

    .line 73
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;
    invoke-static {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;
    invoke-static {v2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->sendScoverBroadcast(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/Boolean;)V
    invoke-static {v0, v1, v2, v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->access$400(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/Boolean;)V

    goto :goto_0
.end method
