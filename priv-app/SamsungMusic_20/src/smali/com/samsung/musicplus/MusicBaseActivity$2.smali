.class Lcom/samsung/musicplus/MusicBaseActivity$2;
.super Ljava/lang/Object;
.source "MusicBaseActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/MusicBaseActivity;->initWindowSizeObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/MusicBaseActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/MusicBaseActivity;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/samsung/musicplus/MusicBaseActivity$2;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .prologue
    .line 249
    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity$2;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-virtual {v1}, Lcom/samsung/musicplus/MusicBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 250
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 251
    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity$2;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/musicplus/MusicBaseActivity;->onGlobalWindowSizeChanged(II)V

    .line 253
    :cond_0
    return-void
.end method
