.class public Lcom/samsung/musicplus/PalmTouchTutorialActivity;
.super Landroid/app/Activity;
.source "PalmTouchTutorialActivity.java"


# instance fields
.field private final ALBUM_NAME:Ljava/lang/String;

.field private final ARTIST_NAME:Ljava/lang/String;

.field protected final CLASSNAME:Ljava/lang/String;

.field private final TITLE_NAME:Ljava/lang/String;

.field private final mActivityUpdateReceiver:Landroid/content/BroadcastReceiver;

.field private final mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field protected mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

.field mHandler:Landroid/os/Handler;

.field protected mMediaPlayer:Landroid/media/MediaPlayer;

.field private mPlayPauseBtn:Landroid/widget/ImageView;

.field private mPlaySound:Z

.field private mTouchHelpText:Landroid/widget/TextView;

.field private mUri:Ljava/lang/String;

.field mWork:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 42
    const-class v0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    .line 60
    const-string v0, "Over the Horizon"

    iput-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->TITLE_NAME:Ljava/lang/String;

    .line 62
    const-string v0, "Samsung"

    iput-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->ARTIST_NAME:Ljava/lang/String;

    .line 64
    const-string v0, "Galaxy"

    iput-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->ALBUM_NAME:Ljava/lang/String;

    .line 218
    new-instance v0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity$1;-><init>(Lcom/samsung/musicplus/PalmTouchTutorialActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mWork:Ljava/lang/Runnable;

    .line 225
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mHandler:Landroid/os/Handler;

    .line 229
    new-instance v0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;-><init>(Lcom/samsung/musicplus/PalmTouchTutorialActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mActivityUpdateReceiver:Landroid/content/BroadcastReceiver;

    .line 260
    new-instance v0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity$3;-><init>(Lcom/samsung/musicplus/PalmTouchTutorialActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-void
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/PalmTouchTutorialActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/PalmTouchTutorialActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mPlaySound:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/PalmTouchTutorialActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->stopSound()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/PalmTouchTutorialActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mPlayPauseBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/PalmTouchTutorialActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mTouchHelpText:Landroid/widget/TextView;

    return-object v0
.end method

.method private disableAllChildView(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 163
    if-eqz p1, :cond_1

    .line 164
    instance-of v4, p1, Landroid/view/ViewGroup;

    if-eqz v4, :cond_0

    move-object v3, p1

    .line 165
    check-cast v3, Landroid/view/ViewGroup;

    .line 166
    .local v3, "viewGroup":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 167
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 168
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 169
    .local v0, "child":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->disableAllChildView(Landroid/view/View;)V

    .line 167
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 172
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childCount":I
    .end local v2    # "index":I
    .end local v3    # "viewGroup":Landroid/view/ViewGroup;
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 174
    :cond_1
    return-void
.end method

.method private initializeViews()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 120
    const v11, 0x7f0d00d6

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 121
    .local v8, "title":Landroid/widget/TextView;
    const-string v11, "Over the Horizon"

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    const v11, 0x7f0d00d5

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 123
    .local v3, "artist":Landroid/widget/TextView;
    const-string v11, "Samsung"

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    const v11, 0x7f0d00d7

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 125
    .local v0, "album":Landroid/widget/TextView;
    const-string v11, "Galaxy"

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    const v11, 0x7f0d0054

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 128
    .local v1, "albumArt":Landroid/widget/ImageView;
    const v11, 0x7f020039

    invoke-virtual {v1, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 130
    const v11, 0x7f0d0121

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 131
    .local v7, "star":Landroid/widget/ImageView;
    if-eqz v7, :cond_0

    .line 132
    const v11, 0x7f020079

    invoke-virtual {v7, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 133
    invoke-virtual {v7, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 135
    :cond_0
    const v11, 0x7f0d00f5

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 136
    .local v4, "currentTime":Landroid/widget/TextView;
    const-string v11, "02:41"

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    const v11, 0x7f0d00cb

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 138
    .local v9, "totalTime":Landroid/widget/TextView;
    const-string v11, "03:20"

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    const v11, 0x7f0d00f4

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/SeekBar;

    .line 140
    .local v6, "seekBar":Landroid/widget/SeekBar;
    const/16 v11, 0x325

    invoke-virtual {v6, v11}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 142
    const v11, 0x7f0d0116

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 143
    .local v5, "listBtn":Landroid/widget/ImageView;
    if-eqz v5, :cond_1

    .line 144
    invoke-virtual {v5, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 146
    :cond_1
    const v11, 0x7f0d00fa

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    iput-object v11, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mPlayPauseBtn:Landroid/widget/ImageView;

    .line 147
    iget-object v11, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mPlayPauseBtn:Landroid/widget/ImageView;

    const v12, 0x7f02006c

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 148
    const v11, 0x7f0d011c

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    .line 154
    .local v2, "animatedplay":Lcom/samsung/musicplus/widget/AnimatedToggleButton;
    const/4 v11, 0x4

    invoke-virtual {v2, v11}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setVisibility(I)V

    .line 156
    const v11, 0x7f0d018e

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mTouchHelpText:Landroid/widget/TextView;

    .line 158
    const v11, 0x7f0d0106

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 159
    .local v10, "view":Landroid/view/View;
    invoke-direct {p0, v10}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->disableAllChildView(Landroid/view/View;)V

    .line 160
    return-void
.end method

.method private startSound()V
    .locals 5

    .prologue
    .line 185
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    iget-object v2, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    const/4 v4, 0x2

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    if-nez v1, :cond_0

    .line 187
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    const-string v2, "StartSound - request audiofocus failed. "

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :goto_0
    return-void

    .line 192
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 193
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    .line 195
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    .line 196
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mUri:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 197
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mUri:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 198
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    .line 199
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 200
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "preparePlay - IOExceptionException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 202
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    const-string v2, "Can\'t find an uri for \'Over the Horizon\'."

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 206
    :catch_1
    move-exception v0

    .line 207
    .local v0, "e":Ljava/lang/IllegalStateException;
    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "preparePlay - IllegalStateException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private stopSound()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 215
    :cond_0
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 306
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 307
    iget-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onConfigurationChanged() is called"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x400

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 69
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->IS_DEFAULT_LAND_MODEL:Z

    if-eqz v0, :cond_2

    .line 70
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->setRequestedOrientation(I)V

    .line 75
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 77
    .local v7, "context":Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    .line 84
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->requestWindowFeature(I)Z

    .line 85
    invoke-virtual {p0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 88
    const v0, 0x7f040081

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->setContentView(I)V

    .line 89
    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    .line 90
    .local v8, "f":Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.PALM_DOWN"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mActivityUpdateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0, v8}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 93
    invoke-direct {p0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->initializeViews()V

    .line 95
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 96
    iput-boolean v2, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mPlaySound:Z

    .line 103
    const/4 v6, 0x0

    .line 105
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 106
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "title = \'Over the Horizon\' COLLATE NOCASE"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 109
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mUri:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    :cond_0
    if-eqz v6, :cond_1

    .line 114
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 117
    :cond_1
    return-void

    .line 72
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "context":Landroid/content/Context;
    .end local v8    # "f":Landroid/content/IntentFilter;
    :cond_2
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->setRequestedOrientation(I)V

    goto/16 :goto_0

    .line 113
    .restart local v6    # "c":Landroid/database/Cursor;
    .restart local v7    # "context":Landroid/content/Context;
    .restart local v8    # "f":Landroid/content/IntentFilter;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 114
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mActivityUpdateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 287
    iget-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 290
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 291
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 295
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 300
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 297
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->finish()V

    .line 298
    const/4 v0, 0x1

    goto :goto_0

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 279
    invoke-direct {p0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->stopSound()V

    .line 280
    iget-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    iget-object v1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 281
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 282
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 312
    const/4 v0, 0x0

    return v0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mPlaySound:Z

    if-eqz v0, :cond_0

    .line 179
    invoke-direct {p0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->startSound()V

    .line 181
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 182
    return-void
.end method
