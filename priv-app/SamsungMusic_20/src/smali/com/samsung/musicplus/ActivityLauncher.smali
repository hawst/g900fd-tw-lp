.class public Lcom/samsung/musicplus/ActivityLauncher;
.super Landroid/app/Activity;
.source "ActivityLauncher.java"

# interfaces
.implements Lcom/samsung/musicplus/service/PlayerServiceCommandAction;
.implements Lcom/samsung/musicplus/service/PlayerServiceStateAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/ActivityLauncher$1;,
        Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;
    }
.end annotation


# static fields
.field private static final ACTION_DLNA_DIRECT_DMC:Ljava/lang/String; = "com.android.music.DIRECT_DMC"

.field private static final ACTION_DLNA_OPEN_INTENT:Ljava/lang/String; = "com.samsung.android.allshare.intent.action.AUDIOPLAYER"

.field private static final ACTION_PLAY:Ljava/lang/String; = "com.sec.android.music.intent.action.PLAY"

.field private static final ACTION_PLAY_BY_MOOD:Ljava/lang/String; = "com.sec.android.app.music.intent.action.PLAY_BY_MOOD"

.field private static final ACTION_PLAY_NEXT:Ljava/lang/String; = "com.sec.android.app.music.intent.action.PLAY_NEXT"

.field private static final ACTION_PLAY_PREVIOUS:Ljava/lang/String; = "com.sec.android.app.music.intent.action.PLAY_PREVIOUS"

.field private static final ACTION_PLAY_SCONNECT_DMR:Ljava/lang/String; = "com.samsung.android.sconnect.action.MUSIC_DMR"

.field private static final ACTION_PLAY_SLINK:Ljava/lang/String; = "android.intent.action.START_SLINK_PLAYBACK"

.field private static final ACTION_PLAY_VIA:Ljava/lang/String; = "com.sec.android.app.music.intent.action.PLAY_VIA"

.field private static final ACTION_SHUFFL_OFF:Ljava/lang/String; = "com.sec.android.app.music.intent.action.SUFFLE_OFF"

.field private static final ACTION_SHUFFL_ON:Ljava/lang/String; = "com.sec.android.app.music.intent.action.SUFFLE_ON"

.field private static final ACTION_STOP:Ljava/lang/String; = "com.sec.android.app.music.intent.action.STOP"

.field private static final ALBUM:Ljava/lang/String; = "album"

.field private static final ARTIST:Ljava/lang/String; = "artist"

.field private static final DEBUG:Z = false

.field private static final EXTRA_CALM:Ljava/lang/String; = "calm"

.field private static final EXTRA_EXCITING:Ljava/lang/String; = "exciting"

.field private static final EXTRA_JOYFUL:Ljava/lang/String; = "joyful"

.field private static final EXTRA_LAUNCH_MUSIC_PLAYER:Ljava/lang/String; = "launchMusicPlayer"

.field private static final EXTRA_LAUNCH_MUSIC_SQUARE:Ljava/lang/String; = "launchMusicSquare"

.field private static final EXTRA_NAME:Ljava/lang/String; = "extra_name"

.field private static final EXTRA_PASSIONATE:Ljava/lang/String; = "passionate"

.field private static final EXTRA_TYPE:Ljava/lang/String; = "extra_type"

.field private static final PLAYLIST:Ljava/lang/String; = "playlist"

.field private static final TAG:Ljava/lang/String; = "MusicExtraLauncher"

.field private static final TITLE:Ljava/lang/String; = "title"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 506
    return-void
.end method

.method private convertUrisToLongArray(Ljava/util/ArrayList;)[J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)[J"
        }
    .end annotation

    .prologue
    .line 377
    .local p1, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez p1, :cond_1

    .line 378
    const/4 v4, 0x0

    .line 387
    :cond_0
    return-object v4

    .line 381
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v4, v6, [J

    .line 382
    .local v4, "list":[J
    const/4 v0, 0x0

    .line 383
    .local v0, "i":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 384
    .local v5, "uri":Landroid/net/Uri;
    invoke-virtual {v5}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    .line 385
    .local v3, "id":Ljava/lang/String;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v4, v0

    move v0, v1

    .line 386
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method private getAllTrakPosition(Landroid/database/Cursor;Ljava/lang/String;Z)I
    .locals 8
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "searchKey"    # Ljava/lang/String;
    .param p3, "audioIdSearch"    # Z

    .prologue
    .line 687
    const/4 v3, 0x0

    .line 688
    .local v3, "position":I
    if-eqz p1, :cond_0

    .line 689
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 690
    .local v2, "len":I
    const-string v5, "_id"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 691
    .local v1, "idIndex":I
    const-string v5, "title"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 692
    .local v4, "titleIndex":I
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 693
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 695
    if-eqz p3, :cond_1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 696
    move v3, v0

    .line 707
    .end local v0    # "i":I
    .end local v1    # "idIndex":I
    .end local v2    # "len":I
    .end local v4    # "titleIndex":I
    :cond_0
    :goto_1
    const-string v5, "MusicExtraLauncher"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getAllTrakPositionByTitle() - position: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    return v3

    .line 699
    .restart local v0    # "i":I
    .restart local v1    # "idIndex":I
    .restart local v2    # "len":I
    .restart local v4    # "titleIndex":I
    :cond_1
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 700
    move v3, v0

    .line 701
    goto :goto_1

    .line 703
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 693
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getKeyword(ILjava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "listType"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 712
    const/4 v7, 0x0

    .line 714
    .local v7, "keyword":Ljava/lang/String;
    const/4 v1, 0x0

    .line 715
    .local v1, "uri":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 716
    .local v2, "projection":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 717
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v11, [Ljava/lang/String;

    aput-object p2, v4, v10

    .line 720
    .local v4, "selectionArgs":[Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 765
    :goto_0
    return-object v5

    .line 722
    :pswitch_0
    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->ARTISTS_ALBUM_ID_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 723
    new-array v2, v11, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const-string v0, "_id"

    aput-object v0, v2, v10

    check-cast v2, [Ljava/lang/String;

    .line 728
    .restart local v2    # "projection":[Ljava/lang/String;
    const-string v3, "artist COLLATE NOCASE = ? "

    .line 753
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/ActivityLauncher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 755
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 756
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 757
    const-string v0, "MusicExtraLauncher"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "keyword: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 760
    :cond_0
    if-eqz v6, :cond_1

    .line 761
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 764
    :cond_1
    const-string v0, "MusicExtraLauncher"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getKeyword() - keyword: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v7

    .line 765
    goto :goto_0

    .line 731
    .end local v6    # "c":Landroid/database/Cursor;
    :pswitch_1
    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->ALBUMS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 732
    new-array v2, v11, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const-string v0, "_id"

    aput-object v0, v2, v10

    check-cast v2, [Ljava/lang/String;

    .line 737
    .restart local v2    # "projection":[Ljava/lang/String;
    const-string v3, "album COLLATE NOCASE = ? "

    .line 738
    goto :goto_1

    .line 740
    :pswitch_2
    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 741
    invoke-static {p2}, Lcom/samsung/musicplus/util/UiUtils;->isFavoritePlaylist(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 742
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v8, "secFilter"

    const-string v9, "include"

    invoke-virtual {v0, v8, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 744
    :cond_2
    new-array v2, v11, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const-string v0, "_id"

    aput-object v0, v2, v10

    .line 747
    .restart local v2    # "projection":[Ljava/lang/String;
    const-string v3, "name COLLATE NOCASE = ? "

    .line 748
    goto :goto_1

    .line 760
    .restart local v6    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 761
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 720
    :pswitch_data_0
    .packed-switch 0x20002
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private handleDlnaOpenIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "i"    # Landroid/content/Intent;

    .prologue
    .line 391
    const/4 v3, 0x0

    .line 392
    .local v3, "queryUri":Landroid/net/Uri;
    const/4 v5, 0x0

    .line 393
    .local v5, "selection":Ljava/lang/String;
    const/4 v6, 0x0

    .line 395
    .local v6, "selectionArgs":[Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lcom/samsung/musicplus/library/dlna/DlnaOpenIntentManager;->getContentUri(Landroid/content/Intent;)Landroid/net/Uri;

    move-result-object v12

    .line 396
    .local v12, "uri":Landroid/net/Uri;
    if-nez v12, :cond_0

    .line 451
    :goto_0
    return-void

    .line 400
    :cond_0
    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "http://"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 403
    invoke-static/range {p2 .. p2}, Lcom/samsung/musicplus/library/dlna/DlnaOpenIntentManager;->getDlnaOpenItemData(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v9

    .line 404
    .local v9, "data":Landroid/os/Bundle;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v9}, Lcom/samsung/musicplus/ActivityLauncher;->updateDlnaOpenIntentContentsDb(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 423
    .end local v9    # "data":Landroid/os/Bundle;
    :goto_1
    const/4 v8, 0x0

    .line 424
    .local v8, "c":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 426
    .local v11, "list":[J
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v13, "_id"

    aput-object v13, v4, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 429
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 430
    invoke-static {v8}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 433
    :cond_1
    if-eqz v8, :cond_2

    .line 434
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 438
    :cond_2
    const-string v4, "MusicExtraLauncher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getList() - audioId: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v11, :cond_6

    const-string v2, "null"

    :goto_2
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    if-nez v11, :cond_7

    .line 441
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f1001b2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/musicplus/ActivityLauncher;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    invoke-static {v2, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 443
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->finish()V

    goto :goto_0

    .line 411
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v11    # "list":[J
    :cond_3
    const-string v2, "file"

    invoke-virtual {v12}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 412
    sget-object v3, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    .line 413
    const-string v5, "_data COLLATE NOCASE = ?"

    .line 414
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {v12}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v2

    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    goto :goto_1

    .line 419
    :cond_4
    move-object v3, v12

    goto :goto_1

    .line 433
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v11    # "list":[J
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_5

    .line 434
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2

    .line 438
    :cond_6
    const/4 v2, 0x0

    aget-wide v14, v11, v2

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_2

    .line 447
    :cond_7
    const-string v2, "com.samsung.android.allshare.intent.extra.ALLSHARE_ENABLED"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 451
    .local v10, "enableDmr":Z
    goto/16 :goto_0
.end method

.method private handlePlayFromSearch(Landroid/content/Intent;)V
    .locals 16
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 268
    const-string v1, "android.intent.extra.focus"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 269
    .local v13, "mediaFocus":Ljava/lang/String;
    const-string v1, "query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 273
    .local v14, "query":Ljava/lang/String;
    const-string v1, "android.intent.extra.album"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 274
    .local v2, "album":Ljava/lang/String;
    const-string v1, "android.intent.extra.artist"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 275
    .local v3, "artist":Ljava/lang/String;
    const-string v1, "android.intent.extra.genre"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 276
    .local v4, "genre":Ljava/lang/String;
    const-string v1, "android.intent.extra.playlist"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 277
    .local v8, "playlist":Ljava/lang/String;
    const-string v1, "android.intent.extra.radio_channel"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 278
    .local v5, "rchannel":Ljava/lang/String;
    const-string v1, "android.intent.extra.title"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 281
    .local v6, "title":Ljava/lang/String;
    if-nez v13, :cond_1

    .line 283
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v14}, Lcom/samsung/musicplus/util/player/PlayUtils;->playUnstructuredSearch(Landroid/content/Context;Ljava/lang/String;)V

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 285
    :cond_1
    const-string v1, "vnd.android.cursor.item/*"

    invoke-virtual {v13, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 286
    if-eqz v14, :cond_2

    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 288
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/player/PlayUtils;->playResumeLastPlaylist(Landroid/content/Context;)V

    goto :goto_0

    .line 291
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v14}, Lcom/samsung/musicplus/util/player/PlayUtils;->playUnstructuredSearch(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 294
    :cond_4
    const-string v1, "vnd.android.cursor.item/genre"

    invoke-virtual {v13, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 296
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/samsung/musicplus/util/player/PlayUtils;->playGenre(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 298
    :cond_5
    const-string v1, "vnd.android.cursor.item/artist"

    invoke-virtual {v13, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 300
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3, v4}, Lcom/samsung/musicplus/util/player/PlayUtils;->playArtist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 302
    :cond_6
    const-string v1, "vnd.android.cursor.item/album"

    invoke-virtual {v13, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_7

    .line 304
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/musicplus/util/player/PlayUtils;->playAlbum(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 306
    :cond_7
    const-string v1, "vnd.android.cursor.item/audio"

    invoke-virtual {v13, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_9

    .line 307
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v6, v2, v3, v4}, Lcom/samsung/musicplus/util/player/PlayUtils;->playSong(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v15

    .line 308
    .local v15, "songPlay":Z
    if-eqz v15, :cond_8

    .line 310
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->launchMainActivity()V

    .line 311
    invoke-static/range {p0 .. p0}, Lcom/samsung/musicplus/util/UiUtils;->launchPlayerActivity(Landroid/app/Activity;)V

    goto :goto_0

    .line 317
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->launchMainActivity()V

    .line 318
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/musicplus/ActivityLauncher;->launchQueryActivity(Ljava/lang/String;)V

    goto :goto_0

    .line 321
    .end local v15    # "songPlay":Z
    :cond_9
    const-string v1, "vnd.android.cursor.item/radio"

    invoke-virtual {v13, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_a

    .line 323
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static/range {v1 .. v6}, Lcom/samsung/musicplus/util/player/PlayUtils;->playRadioChannel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 326
    :cond_a
    const-string v1, "vnd.android.cursor.item/playlist"

    invoke-virtual {v13, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 328
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    move-object v9, v2

    move-object v10, v3

    move-object v11, v4

    move-object v12, v6

    invoke-static/range {v7 .. v12}, Lcom/samsung/musicplus/util/player/PlayUtils;->playPlaylist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private handlePlayViaIntent(Landroid/content/Intent;Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;)Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;
    .locals 9
    .param p1, "i"    # Landroid/content/Intent;
    .param p2, "info"    # Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;

    .prologue
    const v8, 0x20001

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 519
    const-string v4, "intent_extra_data_key"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->dataKey:Ljava/lang/String;

    .line 520
    const-string v4, "intent_extra_from"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->from:Ljava/lang/String;

    .line 521
    iget-object v4, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->from:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->from:Ljava/lang/String;

    const-string v7, "com.samsung.android.app.galaxyfinder"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v5

    :goto_0
    iput-boolean v4, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->triggerFromGalaxyFinder:Z

    .line 524
    const-string v4, "global_search"

    iget-object v7, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->dataKey:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-boolean v4, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->triggerFromGalaxyFinder:Z

    if-eqz v4, :cond_2

    .line 527
    const-string v4, "intent_extra_target_type"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 529
    .local v2, "targetType":I
    const/4 v1, 0x0

    .line 531
    .local v1, "isPlayRequest":Z
    packed-switch v2, :pswitch_data_0

    .line 550
    const-string v3, "title"

    .line 551
    .local v3, "type":Ljava/lang/String;
    iput v8, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->listType:I

    .line 554
    :goto_1
    iput-boolean v5, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->launchPlayer:Z

    .line 555
    if-eqz v1, :cond_0

    .line 556
    iget-object v4, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->dataKey:Ljava/lang/String;

    invoke-direct {p0, v3, v4, v5}, Lcom/samsung/musicplus/ActivityLauncher;->startPlayback(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 572
    .end local v1    # "isPlayRequest":Z
    .end local v2    # "targetType":I
    .end local v3    # "type":Ljava/lang/String;
    :cond_0
    :goto_2
    return-object p2

    :cond_1
    move v4, v6

    .line 521
    goto :goto_0

    .line 533
    .restart local v1    # "isPlayRequest":Z
    .restart local v2    # "targetType":I
    :pswitch_0
    const-string v3, "title"

    .line 534
    .restart local v3    # "type":Ljava/lang/String;
    iput v8, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->listType:I

    .line 535
    const/4 v1, 0x1

    .line 536
    goto :goto_1

    .line 538
    .end local v3    # "type":Ljava/lang/String;
    :pswitch_1
    const-string v3, "album"

    .line 539
    .restart local v3    # "type":Ljava/lang/String;
    const v4, 0x20002

    iput v4, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->listType:I

    goto :goto_1

    .line 542
    .end local v3    # "type":Ljava/lang/String;
    :pswitch_2
    const-string v3, "artist"

    .line 543
    .restart local v3    # "type":Ljava/lang/String;
    const v4, 0x20003

    iput v4, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->listType:I

    goto :goto_1

    .line 546
    .end local v3    # "type":Ljava/lang/String;
    :pswitch_3
    const-string v3, "playlist"

    .line 547
    .restart local v3    # "type":Ljava/lang/String;
    const v4, 0x20004

    iput v4, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->listType:I

    goto :goto_1

    .line 558
    .end local v1    # "isPlayRequest":Z
    .end local v2    # "targetType":I
    .end local v3    # "type":Ljava/lang/String;
    :cond_2
    const-string v4, "global_search"

    iget-object v5, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->dataKey:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 562
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 564
    .local v0, "dataName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 565
    iput-boolean v6, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->launchPlayer:Z

    .line 566
    const-string v4, "title"

    invoke-direct {p0, v4, v0, v6}, Lcom/samsung/musicplus/ActivityLauncher;->startPlayback(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2

    .line 569
    .end local v0    # "dataName":Ljava/lang/String;
    :cond_3
    const-string v4, "extra_type"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "extra_name"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5, v6}, Lcom/samsung/musicplus/ActivityLauncher;->startPlayback(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 570
    const-string v4, "launchMusicPlayer"

    invoke-virtual {p1, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p2, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->launchPlayer:Z

    goto :goto_2

    .line 531
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private launchMainActivity()V
    .locals 2

    .prologue
    .line 248
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/MusicMainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 249
    .local v0, "i":Landroid/content/Intent;
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 250
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/ActivityLauncher;->startActivity(Landroid/content/Intent;)V

    .line 251
    return-void
.end method

.method private launchMusicMainActivity(Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;)V
    .locals 5
    .param p1, "info"    # Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;

    .prologue
    const/4 v4, 0x1

    .line 334
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/MusicMainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 335
    .local v0, "i":Landroid/content/Intent;
    iget-boolean v1, p1, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->triggerFromGalaxyFinder:Z

    if-eqz v1, :cond_1

    .line 336
    iget v1, p1, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->listType:I

    const v2, 0x20001

    if-ne v1, v2, :cond_0

    .line 341
    invoke-static {p0}, Lcom/samsung/musicplus/util/UiUtils;->launchPlayerActivity(Landroid/app/Activity;)V

    .line 352
    :goto_0
    return-void

    .line 344
    :cond_0
    const-string v1, "track_launch"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 345
    const-string v1, "extra_type"

    iget v2, p1, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->listType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 346
    const-string v1, "extra_keyword"

    iget v2, p1, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->listType:I

    iget-object v3, p1, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->dataKey:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/ActivityLauncher;->getKeyword(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    const-string v1, "extra_name"

    iget-object v2, p1, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->dataKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    const-string v1, "intent_extra_from"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 350
    :cond_1
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 351
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/ActivityLauncher;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private launchQueryActivity(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 254
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 255
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "keyword"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/ActivityLauncher;->startActivity(Landroid/content/Intent;)V

    .line 257
    return-void
.end method

.method private play(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 10
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "searckKey"    # Ljava/lang/String;
    .param p4, "audioIdSearch"    # Z

    .prologue
    .line 769
    const/4 v7, 0x0

    .line 773
    .local v7, "c":Landroid/database/Cursor;
    invoke-static {p1, p2}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v0

    iget-object v6, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 776
    .local v6, "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/ActivityLauncher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v2, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v3, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v4, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v5, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 778
    invoke-static {v7}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v8

    .line 779
    .local v8, "list":[J
    const v0, 0x20001

    if-ne p1, v0, :cond_1

    .line 780
    invoke-direct {p0, v7, p3, p4}, Lcom/samsung/musicplus/ActivityLauncher;->getAllTrakPosition(Landroid/database/Cursor;Ljava/lang/String;Z)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 785
    .local v9, "position":I
    :goto_0
    if-eqz v7, :cond_0

    .line 786
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 790
    :cond_0
    invoke-direct {p0, p1, p2, v8, v9}, Lcom/samsung/musicplus/ActivityLauncher;->play(ILjava/lang/String;[JI)V

    .line 791
    return-void

    .line 782
    .end local v9    # "position":I
    :cond_1
    const/4 v9, 0x0

    .restart local v9    # "position":I
    goto :goto_0

    .line 785
    .end local v8    # "list":[J
    .end local v9    # "position":I
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 786
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private play(ILjava/lang/String;[JI)V
    .locals 6
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I

    .prologue
    .line 667
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/ActivityLauncher;->play(ILjava/lang/String;[JILjava/lang/String;)V

    .line 668
    return-void
.end method

.method private play(ILjava/lang/String;[JILjava/lang/String;)V
    .locals 4
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I
    .param p5, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 671
    const-string v1, "MusicExtraLauncher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "play() list type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " deviceId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 676
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.samsung.musicplus.action.SERVICE_COMMAND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 677
    const-string v1, "command"

    const-string v2, "openList"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 678
    const-string v1, "listType"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 679
    const-string v1, "listQueryKey"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 680
    const-string v1, "list"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 681
    const-string v1, "listPosition"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 682
    const-string v1, "dmr_device"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 683
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/ActivityLauncher;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 684
    return-void
.end method

.method private playAndStartPlayerActivity(ILjava/lang/String;[JI)V
    .locals 6
    .param p1, "listType"    # I
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I

    .prologue
    .line 361
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/ActivityLauncher;->playAndStartPlayerActivity(ILjava/lang/String;[JILjava/lang/String;)V

    .line 362
    return-void
.end method

.method private playAndStartPlayerActivity(ILjava/lang/String;[JILjava/lang/String;)V
    .locals 0
    .param p1, "listType"    # I
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I
    .param p5, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 366
    invoke-direct/range {p0 .. p5}, Lcom/samsung/musicplus/ActivityLauncher;->play(ILjava/lang/String;[JILjava/lang/String;)V

    .line 373
    invoke-static {p0}, Lcom/samsung/musicplus/util/UiUtils;->launchPlayerActivity(Landroid/app/Activity;)V

    .line 374
    return-void
.end method

.method private playByMood(Ljava/lang/String;)V
    .locals 5
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 576
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/ActivityLauncher;->selectSquareModeType(Ljava/lang/String;)V

    .line 577
    const/4 v1, 0x0

    .line 578
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "calm"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 579
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquareCalmUri()Landroid/net/Uri;

    move-result-object v1

    .line 587
    :cond_0
    :goto_0
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/ActivityLauncher;->getMoodSongIds(Landroid/net/Uri;)[J

    move-result-object v0

    .line 588
    .local v0, "songIds":[J
    if-eqz v0, :cond_1

    array-length v2, v0

    if-lez v2, :cond_1

    .line 594
    const v2, 0x2000a

    const-string v3, "_id"

    invoke-static {v3, v0}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v0, v4}, Lcom/samsung/musicplus/ActivityLauncher;->play(ILjava/lang/String;[JI)V

    .line 597
    :cond_1
    return-void

    .line 580
    .end local v0    # "songIds":[J
    :cond_2
    const-string v2, "exciting"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 581
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquareExcitingUri()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 582
    :cond_3
    const-string v2, "joyful"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 583
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquareJoyfulUri()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 584
    :cond_4
    const-string v2, "passionate"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 585
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquarePassionateUri()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method private printLog([J)V
    .locals 6
    .param p1, "mediaIds"    # [J

    .prologue
    .line 355
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 356
    const-string v1, "MusicExtraLauncher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-wide v4, p1, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 358
    :cond_0
    return-void
.end method

.method private selectSquareModeType(Ljava/lang/String;)V
    .locals 6
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x5

    .line 803
    invoke-virtual {p0}, Lcom/samsung/musicplus/ActivityLauncher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0003

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 805
    .local v0, "column":I
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 806
    .local v2, "values":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v3, "exciting"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 807
    if-ne v0, v5, :cond_0

    .line 808
    const-string v3, "0"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 809
    const-string v3, "1"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 810
    const-string v3, "2"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 811
    const-string v3, "3"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 812
    const-string v3, "4"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 813
    const-string v3, "5"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 814
    const-string v3, "6"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 815
    const-string v3, "7"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 816
    const-string v3, "8"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 817
    const-string v3, "9"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 860
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 861
    const-string v3, "music_player_pref"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/samsung/musicplus/ActivityLauncher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 863
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "savedSquare"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 865
    .end local v1    # "sp":Landroid/content/SharedPreferences;
    :cond_1
    return-void

    .line 819
    :cond_2
    const-string v3, "calm"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 820
    if-ne v0, v5, :cond_0

    .line 821
    const-string v3, "15"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 822
    const-string v3, "16"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 823
    const-string v3, "17"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 824
    const-string v3, "18"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 825
    const-string v3, "19"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 826
    const-string v3, "20"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 827
    const-string v3, "21"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 828
    const-string v3, "22"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 829
    const-string v3, "23"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 830
    const-string v3, "24"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 832
    :cond_3
    const-string v3, "passionate"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 833
    if-ne v0, v5, :cond_0

    .line 834
    const-string v3, "0"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 835
    const-string v3, "1"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 836
    const-string v3, "5"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 837
    const-string v3, "6"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 838
    const-string v3, "10"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 839
    const-string v3, "11"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 840
    const-string v3, "15"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 841
    const-string v3, "16"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 842
    const-string v3, "20"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 843
    const-string v3, "21"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 845
    :cond_4
    const-string v3, "joyful"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 846
    if-ne v0, v5, :cond_0

    .line 847
    const-string v3, "3"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 848
    const-string v3, "4"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 849
    const-string v3, "8"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 850
    const-string v3, "9"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 851
    const-string v3, "13"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 852
    const-string v3, "14"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 853
    const-string v3, "18"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 854
    const-string v3, "19"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 855
    const-string v3, "23"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 856
    const-string v3, "24"

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private setShuffleMode(Z)V
    .locals 4
    .param p1, "on"    # Z

    .prologue
    .line 794
    const-string v1, "MusicExtraLauncher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setShuffleMode() - on : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 796
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.android.music.settingchanged"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 797
    const-string v2, "shuffle"

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 799
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/ActivityLauncher;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 800
    return-void

    .line 797
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private startPlayback(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "searckKey"    # Ljava/lang/String;
    .param p3, "audioIdSearch"    # Z

    .prologue
    .line 642
    const-string v2, "MusicExtraLauncher"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getPlayListInfo() - type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " searckKey: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    const-string v2, "title"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 647
    const v1, 0x20001

    .line 648
    .local v1, "listType":I
    const/4 v0, 0x0

    .line 663
    .local v0, "keyWord":Ljava/lang/String;
    :goto_0
    invoke-direct {p0, v1, v0, p2, p3}, Lcom/samsung/musicplus/ActivityLauncher;->play(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 664
    .end local v0    # "keyWord":Ljava/lang/String;
    .end local v1    # "listType":I
    :cond_0
    return-void

    .line 649
    :cond_1
    const-string v2, "album"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 650
    const v1, 0x20002

    .line 651
    .restart local v1    # "listType":I
    invoke-direct {p0, v1, p2}, Lcom/samsung/musicplus/ActivityLauncher;->getKeyword(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "keyWord":Ljava/lang/String;
    goto :goto_0

    .line 652
    .end local v0    # "keyWord":Ljava/lang/String;
    .end local v1    # "listType":I
    :cond_2
    const-string v2, "artist"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 653
    const v1, 0x20003

    .line 654
    .restart local v1    # "listType":I
    invoke-direct {p0, v1, p2}, Lcom/samsung/musicplus/ActivityLauncher;->getKeyword(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "keyWord":Ljava/lang/String;
    goto :goto_0

    .line 655
    .end local v0    # "keyWord":Ljava/lang/String;
    .end local v1    # "listType":I
    :cond_3
    const-string v2, "playlist"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 656
    const v1, 0x20004

    .line 657
    .restart local v1    # "listType":I
    invoke-direct {p0, v1, p2}, Lcom/samsung/musicplus/ActivityLauncher;->getKeyword(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "keyWord":Ljava/lang/String;
    goto :goto_0
.end method

.method private startService(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "command"    # Ljava/lang/String;
    .param p3, "force"    # Z

    .prologue
    .line 632
    const-string v1, "MusicExtraLauncher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startService() context : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " command: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " force: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 635
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.samsung.musicplus.action.SERVICE_COMMAND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 636
    const-string v1, "command"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 637
    const-string v1, "force"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 638
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 639
    return-void
.end method

.method private updateDlnaOpenIntentContentsDb(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 454
    if-nez p2, :cond_0

    .line 504
    :goto_0
    return-void

    .line 458
    :cond_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 460
    .local v3, "value":Landroid/content/ContentValues;
    const-string v6, "artist"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 461
    .local v2, "itemInfo":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v6, ""

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 462
    const-string v6, "artist"

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :cond_1
    const-string v6, "album"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 465
    if-eqz v2, :cond_2

    const-string v6, ""

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 466
    const-string v6, "album"

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_2
    const-string v6, "title"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 469
    if-eqz v2, :cond_3

    const-string v6, ""

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 470
    const-string v6, "title"

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    :cond_3
    const-string v6, "mime_type"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 473
    if-eqz v2, :cond_4

    const-string v6, ""

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 474
    const-string v6, "mime_type"

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :cond_4
    const-string v6, "extension"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 477
    if-eqz v2, :cond_5

    const-string v6, ""

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 478
    const-string v6, "extension"

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    :cond_5
    const-string v6, "seed"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 481
    if-eqz v2, :cond_6

    .line 482
    const-string v6, "seed"

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    :cond_6
    const-string v6, "_data"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 485
    if-eqz v2, :cond_7

    .line 486
    const-string v6, "_data"

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    :cond_7
    const-string v6, "album_art"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 489
    if-eqz v2, :cond_8

    .line 490
    const-string v6, "album_art"

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    :cond_8
    const-string v6, "genre_name"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 493
    if-eqz v2, :cond_9

    const-string v6, ""

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 494
    const-string v6, "genre_name"

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    :cond_9
    const-string v6, "duration"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 498
    .local v0, "duration":J
    const-string v6, "duration"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 499
    const-string v6, "file_size"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 500
    .local v4, "size":J
    const-string v6, "file_size"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 502
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContentsExtra;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6, v7, v8, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 503
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContentsExtra;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6, v7, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_0
.end method


# virtual methods
.method public getMoodSongIds(Landroid/net/Uri;)[J
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 600
    if-eqz p1, :cond_0

    .line 601
    const/4 v6, 0x0

    .line 603
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/ActivityLauncher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "source_id AS _id"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string v3, "mood_cell"

    aput-object v3, v2, v1

    const/4 v1, 0x2

    const-string v3, "year_cell"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "title_key"

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 612
    invoke-static {v6}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 614
    if-eqz v6, :cond_0

    .line 615
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 619
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    return-object v0

    .line 614
    .restart local v6    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 615
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public handleIntent(Landroid/content/Intent;)V
    .locals 25
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 136
    const-string v5, "MusicExtraLauncher"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleIntent intent : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    .line 138
    .local v13, "context":Landroid/content/Context;
    new-instance v16, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;

    const/4 v5, 0x0

    move-object/from16 v0, v16

    invoke-direct {v0, v5}, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;-><init>(Lcom/samsung/musicplus/ActivityLauncher$1;)V

    .line 140
    .local v16, "info":Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;
    const-string v5, "launchMusicPlayer"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v18

    .line 141
    .local v18, "launchMusic":Z
    const-string v5, "launchMusicSquare"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v19

    .line 143
    .local v19, "launchSquare":Z
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v11

    .line 144
    .local v11, "action":Ljava/lang/String;
    const-string v5, "MusicExtraLauncher"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleIntent: action: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", lauch music : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", lauch music square : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v5, "com.sec.android.app.music.intent.action.PLAY_BY_MOOD"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 148
    const-string v5, "extra_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/musicplus/ActivityLauncher;->playByMood(Ljava/lang/String;)V

    .line 240
    :cond_0
    :goto_0
    if-eqz v18, :cond_11

    .line 241
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/ActivityLauncher;->launchMusicMainActivity(Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;)V

    .line 245
    :cond_1
    :goto_1
    return-void

    .line 149
    :cond_2
    const-string v5, "com.sec.android.app.music.intent.action.PLAY_VIA"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 151
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/ActivityLauncher;->handlePlayViaIntent(Landroid/content/Intent;Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;)Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;

    move-result-object v16

    .line 152
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lcom/samsung/musicplus/ActivityLauncher$GalaxyFinderListInfo;->launchPlayer:Z

    move/from16 v18, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 153
    :catch_0
    move-exception v15

    .line 154
    .local v15, "e":Ljava/lang/IllegalArgumentException;
    const-string v5, "MusicExtraLauncher"

    const-string v6, "handleIntent() - IllegalArgumentException occur - Do not launchMusic"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    const/16 v18, 0x0

    .line 156
    goto :goto_0

    .line 157
    .end local v15    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    const-string v5, "com.sec.android.music.intent.action.PLAY"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 158
    const-string v5, "play"

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v5, v6}, Lcom/samsung/musicplus/ActivityLauncher;->startService(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 159
    :cond_4
    const-string v5, "com.sec.android.app.music.intent.action.PLAY_PREVIOUS"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 160
    const-string v5, "previous"

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v5, v6}, Lcom/samsung/musicplus/ActivityLauncher;->startService(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 161
    :cond_5
    const-string v5, "com.sec.android.app.music.intent.action.PLAY_NEXT"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 162
    const-string v5, "next"

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v5, v6}, Lcom/samsung/musicplus/ActivityLauncher;->startService(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 163
    :cond_6
    const-string v5, "com.sec.android.app.music.intent.action.STOP"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 166
    const-string v5, "pause"

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v5, v6}, Lcom/samsung/musicplus/ActivityLauncher;->startService(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 167
    :cond_7
    const-string v5, "com.sec.android.app.music.intent.action.SUFFLE_ON"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 168
    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/musicplus/ActivityLauncher;->setShuffleMode(Z)V

    goto :goto_0

    .line 169
    :cond_8
    const-string v5, "com.sec.android.app.music.intent.action.SUFFLE_OFF"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 170
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/musicplus/ActivityLauncher;->setShuffleMode(Z)V

    goto/16 :goto_0

    .line 171
    :cond_9
    const-string v5, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 172
    invoke-direct/range {p0 .. p1}, Lcom/samsung/musicplus/ActivityLauncher;->handlePlayFromSearch(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 173
    :cond_a
    const-string v5, "com.samsung.android.allshare.intent.action.AUDIOPLAYER"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 175
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v13, v1}, Lcom/samsung/musicplus/ActivityLauncher;->handleDlnaOpenIntent(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 176
    :cond_b
    const-string v5, "com.android.music.DIRECT_DMC"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 178
    const-string v5, "DMRUDN"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 182
    :cond_c
    const-string v5, "com.samsung.android.sconnect.action.MUSIC_DMR"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 183
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v14

    .line 184
    .local v14, "data":Landroid/os/Bundle;
    const-string v5, "DEVICE"

    invoke-virtual {v14, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 186
    .local v10, "deviceId":Ljava/lang/String;
    const-string v5, "android.intent.extra.STREAM"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v24

    .line 187
    .local v24, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/ActivityLauncher;->convertUrisToLongArray(Ljava/util/ArrayList;)[J

    move-result-object v8

    .line 188
    .local v8, "mediaIds":[J
    if-eqz v8, :cond_0

    .line 200
    const v6, 0x20001

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v10}, Lcom/samsung/musicplus/ActivityLauncher;->playAndStartPlayerActivity(ILjava/lang/String;[JILjava/lang/String;)V

    goto/16 :goto_0

    .line 203
    .end local v8    # "mediaIds":[J
    .end local v10    # "deviceId":Ljava/lang/String;
    .end local v14    # "data":Landroid/os/Bundle;
    .end local v24    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_d
    const-string v5, "com.samsung.musicplus.intent.action.PLAY_CONTENTS"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 205
    const-string v5, "base_uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Landroid/net/Uri;

    .line 208
    .local v23, "uri":Landroid/net/Uri;
    if-eqz v23, :cond_1

    .line 211
    invoke-static/range {v23 .. v23}, Lcom/samsung/musicplus/util/ListUtils;->convertToListType(Landroid/net/Uri;)I

    move-result v21

    .line 212
    .local v21, "listType":I
    const/4 v5, -0x1

    move/from16 v0, v21

    if-eq v0, v5, :cond_1

    .line 218
    const-string v5, "list"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v20

    .line 219
    .local v20, "list":[J
    const-string v5, "listPosition"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v22

    .line 220
    .local v22, "position":I
    const-string v5, "listQueryKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 222
    .local v17, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v17

    move-object/from16 v3, v20

    move/from16 v4, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/ActivityLauncher;->playAndStartPlayerActivity(ILjava/lang/String;[JI)V

    goto/16 :goto_0

    .line 223
    .end local v17    # "key":Ljava/lang/String;
    .end local v20    # "list":[J
    .end local v21    # "listType":I
    .end local v22    # "position":I
    .end local v23    # "uri":Landroid/net/Uri;
    :cond_e
    const-string v5, "android.intent.action.START_SLINK_PLAYBACK"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 224
    const/4 v12, 0x0

    .line 226
    .local v12, "c":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/ActivityLauncher;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->getCursor(Landroid/content/Context;Landroid/content/Intent;)Landroid/database/Cursor;

    move-result-object v12

    .line 227
    if-eqz v12, :cond_f

    .line 228
    invoke-interface {v12}, Landroid/database/Cursor;->getPosition()I

    move-result v22

    .line 229
    .restart local v22    # "position":I
    invoke-static {v12}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v20

    .line 230
    .restart local v20    # "list":[J
    const v5, 0x2000d

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v22

    invoke-direct {v0, v5, v6, v1, v2}, Lcom/samsung/musicplus/ActivityLauncher;->playAndStartPlayerActivity(ILjava/lang/String;[JI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233
    .end local v20    # "list":[J
    .end local v22    # "position":I
    :cond_f
    if-eqz v12, :cond_0

    .line 234
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 233
    :catchall_0
    move-exception v5

    if-eqz v12, :cond_10

    .line 234
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_10
    throw v5

    .line 242
    .end local v12    # "c":Landroid/database/Cursor;
    :cond_11
    if-eqz v19, :cond_1

    goto/16 :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    const-string v1, "MusicExtraLauncher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/samsung/musicplus/ActivityLauncher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 102
    .local v0, "i":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/samsung/musicplus/ActivityLauncher;->finish()V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/ActivityLauncher;->handleIntent(Landroid/content/Intent;)V

    .line 107
    invoke-virtual {p0}, Lcom/samsung/musicplus/ActivityLauncher;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 131
    const-string v0, "MusicExtraLauncher"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 133
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 112
    const-string v0, "MusicExtraLauncher"

    const-string v1, "onNewIntent"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    if-nez p1, :cond_0

    .line 114
    invoke-virtual {p0}, Lcom/samsung/musicplus/ActivityLauncher;->finish()V

    .line 120
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/ActivityLauncher;->handleIntent(Landroid/content/Intent;)V

    .line 118
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 119
    invoke-virtual {p0}, Lcom/samsung/musicplus/ActivityLauncher;->finish()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 124
    const-string v0, "MusicExtraLauncher"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p0}, Lcom/samsung/musicplus/ActivityLauncher;->finish()V

    .line 126
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 127
    return-void
.end method
