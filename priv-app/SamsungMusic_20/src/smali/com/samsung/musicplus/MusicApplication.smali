.class public Lcom/samsung/musicplus/MusicApplication;
.super Landroid/app/Application;
.source "MusicApplication.java"


# static fields
.field private static final STRICT_MODE:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 48
    invoke-static {p0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->initCache(Landroid/content/Context;)V

    .line 49
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->theme:I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicApplication;->setTheme(I)V

    .line 53
    return-void
.end method
