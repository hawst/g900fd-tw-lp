.class public final Lcom/sec/android/app/music/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/music/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final actionbar_check_box_animator_list:I = 0x7f020000

.field public static final actionbar_detail_edit_btn_selector:I = 0x7f020001

.field public static final actionbar_detail_search_btn_selector:I = 0x7f020002

.field public static final actionbar_detail_tags_btn_selector:I = 0x7f020003

.field public static final btn_square_help:I = 0x7f020004

.field public static final detail_arrow_left:I = 0x7f020005

.field public static final detail_arrow_right:I = 0x7f020006

.field public static final detail_biography_bottom_bg:I = 0x7f020007

.field public static final detail_biography_star_select:I = 0x7f020008

.field public static final detail_biography_star_unselect:I = 0x7f020009

.field public static final detail_thumbnail_shadow:I = 0x7f02000a

.field public static final f_airview_popup_bg_bottom_w:I = 0x7f02000b

.field public static final header_button_icon_move_knox:I = 0x7f02000c

.field public static final help_popup_picker_bg_w_01:I = 0x7f02000d

.field public static final hover_music_bg_c:I = 0x7f02000e

.field public static final hover_music_bg_l:I = 0x7f02000f

.field public static final hover_music_bg_r:I = 0x7f020010

.field public static final hover_popup_basic_black_bg:I = 0x7f020011

.field public static final hover_popup_basic_white_bg:I = 0x7f020012

.field public static final ic_wifi_direct_audio:I = 0x7f020013

.field public static final ic_wifi_direct_camera:I = 0x7f020014

.field public static final ic_wifi_direct_computer:I = 0x7f020015

.field public static final ic_wifi_direct_displays:I = 0x7f020016

.field public static final ic_wifi_direct_game_devices:I = 0x7f020017

.field public static final ic_wifi_direct_input_device:I = 0x7f020018

.field public static final ic_wifi_direct_multimedia:I = 0x7f020019

.field public static final ic_wifi_direct_network_infra:I = 0x7f02001a

.field public static final ic_wifi_direct_printer:I = 0x7f02001b

.field public static final ic_wifi_direct_storage:I = 0x7f02001c

.field public static final ic_wifi_direct_telephone:I = 0x7f02001d

.field public static final knox_container_icon1:I = 0x7f02001e

.field public static final knox_container_icon2:I = 0x7f02001f

.field public static final list_divider:I = 0x7f020020

.field public static final list_divider_divices:I = 0x7f020021

.field public static final list_divider_full:I = 0x7f020022

.field public static final list_item_pressed_selector:I = 0x7f020023

.field public static final list_item_pressed_selector_sviewcover:I = 0x7f020024

.field public static final list_shuffle_icon:I = 0x7f020025

.field public static final list_track_group_bg:I = 0x7f020026

.field public static final mini_play_control_unbounded_ripple:I = 0x7f020027

.field public static final mini_player_tw_progressive_point:I = 0x7f020028

.field public static final music_albumart_default_background:I = 0x7f020029

.field public static final music_detail_actionbar_icon_tags_music:I = 0x7f02002a

.field public static final music_detail_actionbar_icon_tags_music_dim:I = 0x7f02002b

.field public static final music_detail_arrow_left:I = 0x7f02002c

.field public static final music_detail_arrow_left_dim:I = 0x7f02002d

.field public static final music_detail_arrow_right:I = 0x7f02002e

.field public static final music_detail_arrow_right_dim:I = 0x7f02002f

.field public static final music_fullplayer_album_art_bg:I = 0x7f020030

.field public static final music_fullplayer_personal_icon:I = 0x7f020031

.field public static final music_fullplayer_sidecast_album_art_bg:I = 0x7f020032

.field public static final music_library_add_playlist:I = 0x7f020033

.field public static final music_library_add_playlist_02:I = 0x7f020034

.field public static final music_library_add_playlist_now_play:I = 0x7f020035

.field public static final music_library_album_detail_frame:I = 0x7f020036

.field public static final music_library_artist_default:I = 0x7f020037

.field public static final music_library_artist_thumbnail_more:I = 0x7f020038

.field public static final music_library_default_large:I = 0x7f020039

.field public static final music_library_default_medium:I = 0x7f02003a

.field public static final music_library_default_small:I = 0x7f02003b

.field public static final music_library_device_ic_refresh:I = 0x7f02003c

.field public static final music_library_icon_folder:I = 0x7f02003d

.field public static final music_library_music_square:I = 0x7f02003e

.field public static final music_library_music_square_info:I = 0x7f02003f

.field public static final music_library_tracks_playing_k2hd:I = 0x7f020040

.field public static final music_library_tracks_playing_uhq:I = 0x7f020041

.field public static final music_library_tracks_shuffle:I = 0x7f020042

.field public static final music_library_tracks_shuffle_press:I = 0x7f020043

.field public static final music_list_bg_pressed:I = 0x7f020044

.field public static final music_list_line:I = 0x7f020045

.field public static final music_list_nowplaying_01:I = 0x7f020046

.field public static final music_list_nowplaying_02:I = 0x7f020047

.field public static final music_list_nowplaying_03:I = 0x7f020048

.field public static final music_list_personal_icon:I = 0x7f020049

.field public static final music_list_split_divider_selector:I = 0x7f02004a

.field public static final music_logo_amg:I = 0x7f02004b

.field public static final music_miniplay_control_ff_02:I = 0x7f02004c

.field public static final music_miniplay_control_miniplay:I = 0x7f02004d

.field public static final music_miniplay_control_next_btn:I = 0x7f02004e

.field public static final music_miniplay_control_pause:I = 0x7f02004f

.field public static final music_miniplay_control_pause_btn:I = 0x7f020050

.field public static final music_miniplay_control_play_btn:I = 0x7f020051

.field public static final music_miniplay_control_prev_btn:I = 0x7f020052

.field public static final music_miniplay_control_rew_02:I = 0x7f020053

.field public static final music_miniplay_menu_off_shuffle:I = 0x7f020054

.field public static final music_miniplay_menu_on_shuffle:I = 0x7f020055

.field public static final music_miniplay_menu_rep_1:I = 0x7f020056

.field public static final music_miniplay_menu_rep_all:I = 0x7f020057

.field public static final music_miniplay_menu_rep_noff:I = 0x7f020058

.field public static final music_miniplayer_controller_bg:I = 0x7f020059

.field public static final music_miniplayer_seekbar:I = 0x7f02005a

.field public static final music_noitem:I = 0x7f02005b

.field public static final music_now_playing_01:I = 0x7f02005c

.field public static final music_now_playing_02:I = 0x7f02005d

.field public static final music_now_playing_03:I = 0x7f02005e

.field public static final music_play_allshare_vol_bg:I = 0x7f02005f

.field public static final music_play_allshare_vol_divider:I = 0x7f020060

.field public static final music_play_allshare_vol_down:I = 0x7f020061

.field public static final music_play_allshare_vol_mute:I = 0x7f020062

.field public static final music_play_allshare_vol_up:I = 0x7f020063

.field public static final music_play_control_allshare_vol_down:I = 0x7f020064

.field public static final music_play_control_allshare_vol_mute:I = 0x7f020065

.field public static final music_play_control_allshare_vol_up:I = 0x7f020066

.field public static final music_play_control_ff:I = 0x7f020067

.field public static final music_play_control_ff_hover:I = 0x7f020068

.field public static final music_play_control_list_btn:I = 0x7f020069

.field public static final music_play_control_next_btn:I = 0x7f02006a

.field public static final music_play_control_pause:I = 0x7f02006b

.field public static final music_play_control_pause_btn:I = 0x7f02006c

.field public static final music_play_control_play:I = 0x7f02006d

.field public static final music_play_control_play_btn:I = 0x7f02006e

.field public static final music_play_control_prev_btn:I = 0x7f02006f

.field public static final music_play_control_rew:I = 0x7f020070

.field public static final music_play_control_rew_hover:I = 0x7f020071

.field public static final music_play_control_sa:I = 0x7f020072

.field public static final music_play_control_sound_alive_btn:I = 0x7f020073

.field public static final music_play_control_unbounded_ripple:I = 0x7f020074

.field public static final music_play_menu_addplaylist:I = 0x7f020075

.field public static final music_play_menu_allshare_connected:I = 0x7f020076

.field public static final music_play_menu_allshare_disabled:I = 0x7f020077

.field public static final music_play_menu_allshare_normal:I = 0x7f020078

.field public static final music_play_menu_favorite_off:I = 0x7f020079

.field public static final music_play_menu_favorite_on:I = 0x7f02007a

.field public static final music_play_menu_list:I = 0x7f02007b

.field public static final music_play_menu_list_hover:I = 0x7f02007c

.field public static final music_play_menu_off_shuffle:I = 0x7f02007d

.field public static final music_play_menu_on_shuffle:I = 0x7f02007e

.field public static final music_play_menu_rep_1:I = 0x7f02007f

.field public static final music_play_menu_rep_all:I = 0x7f020080

.field public static final music_play_menu_rep_off:I = 0x7f020081

.field public static final music_play_menu_repeat_all_btn:I = 0x7f020082

.field public static final music_play_menu_shuffle_off_btn:I = 0x7f020083

.field public static final music_player_mini_control_ic_next_airview:I = 0x7f020084

.field public static final music_player_mini_control_ic_prev_airview:I = 0x7f020085

.field public static final music_player_seekbar:I = 0x7f020086

.field public static final music_player_thumbnail_focus:I = 0x7f020087

.field public static final music_player_thumbnail_press:I = 0x7f020088

.field public static final music_player_uhq:I = 0x7f020089

.field public static final music_soundalive_card_hall_off:I = 0x7f02008a

.field public static final music_soundalive_card_hall_on:I = 0x7f02008b

.field public static final music_soundalive_card_none_off:I = 0x7f02008c

.field public static final music_soundalive_card_none_on:I = 0x7f02008d

.field public static final music_soundalive_card_room_off:I = 0x7f02008e

.field public static final music_soundalive_card_room_on:I = 0x7f02008f

.field public static final music_soundalive_card_studio_off:I = 0x7f020090

.field public static final music_soundalive_card_studio_on:I = 0x7f020091

.field public static final music_soundalive_card_tubeamp_off:I = 0x7f020092

.field public static final music_soundalive_card_tubeamp_on:I = 0x7f020093

.field public static final music_soundalive_card_virtual71_off:I = 0x7f020094

.field public static final music_soundalive_card_virtual71_on:I = 0x7f020095

.field public static final music_square_grid_selector:I = 0x7f020096

.field public static final music_thumb_personal_icon:I = 0x7f020097

.field public static final music_volume_controller_launch:I = 0x7f020098

.field public static final music_volume_controller_mute:I = 0x7f020099

.field public static final music_volume_controller_popup_bg:I = 0x7f02009a

.field public static final new_soundalive_advanced_effect_focus:I = 0x7f02009b

.field public static final new_soundalive_advanced_effect_selector:I = 0x7f02009c

.field public static final new_soundalive_effect_selector:I = 0x7f02009d

.field public static final nowplaying_ripple:I = 0x7f02009e

.field public static final pattern_01_main:I = 0x7f02009f

.field public static final play_speed_minus_btn:I = 0x7f0200a0

.field public static final play_speed_plus_btn:I = 0x7f0200a1

.field public static final playlist_icon_create:I = 0x7f0200a2

.field public static final playspeed_popup_icon_minus:I = 0x7f0200a3

.field public static final playspeed_popup_icon_plus:I = 0x7f0200a4

.field public static final quick_panel_music_close:I = 0x7f0200a5

.field public static final quick_panel_music_ff:I = 0x7f0200a6

.field public static final quick_panel_music_pause:I = 0x7f0200a7

.field public static final quick_panel_music_play:I = 0x7f0200a8

.field public static final quick_panel_music_repeat_all:I = 0x7f0200a9

.field public static final quick_panel_music_repeat_off:I = 0x7f0200aa

.field public static final quick_panel_music_repeat_one:I = 0x7f0200ab

.field public static final quick_panel_music_rew:I = 0x7f0200ac

.field public static final quick_panel_music_shuffle_off:I = 0x7f0200ad

.field public static final quick_panel_music_shuffle_on:I = 0x7f0200ae

.field public static final quick_panel_player_icon_group_play:I = 0x7f0200af

.field public static final quickpanel_btn_pause:I = 0x7f0200b0

.field public static final quickpanel_btn_play:I = 0x7f0200b1

.field public static final selector_unregisterd_delete_btn:I = 0x7f0200b2

.field public static final sound_alive_card_bg_focus:I = 0x7f0200b3

.field public static final sound_alive_focus:I = 0x7f0200b4

.field public static final sound_alive_line_off:I = 0x7f0200b5

.field public static final sound_alive_line_off_press:I = 0x7f0200b6

.field public static final sound_alive_line_on:I = 0x7f0200b7

.field public static final sound_alive_line_on_press:I = 0x7f0200b8

.field public static final sound_alive_num_popup_bg:I = 0x7f0200b9

.field public static final soundalive_indicator_ripple_material:I = 0x7f0200ba

.field public static final soundalive_square_bg_normal_ripple:I = 0x7f0200bb

.field public static final soundalive_square_bg_pressed_ripple:I = 0x7f0200bc

.field public static final square_grid_focus:I = 0x7f0200bd

.field public static final stat_notify_voice_input:I = 0x7f0200be

.field public static final stat_pause:I = 0x7f0200bf

.field public static final stat_play:I = 0x7f0200c0

.field public static final sview_cover_list_dvline:I = 0x7f0200c1

.field public static final sview_cover_music_play_control_ff:I = 0x7f0200c2

.field public static final sview_cover_music_play_control_ff_focus:I = 0x7f0200c3

.field public static final sview_cover_music_play_control_ff_press:I = 0x7f0200c4

.field public static final sview_cover_music_play_control_next_btn:I = 0x7f0200c5

.field public static final sview_cover_music_play_control_pause:I = 0x7f0200c6

.field public static final sview_cover_music_play_control_pause_btn:I = 0x7f0200c7

.field public static final sview_cover_music_play_control_pause_focus:I = 0x7f0200c8

.field public static final sview_cover_music_play_control_pause_press:I = 0x7f0200c9

.field public static final sview_cover_music_play_control_play:I = 0x7f0200ca

.field public static final sview_cover_music_play_control_play_btn:I = 0x7f0200cb

.field public static final sview_cover_music_play_control_play_focus:I = 0x7f0200cc

.field public static final sview_cover_music_play_control_play_press:I = 0x7f0200cd

.field public static final sview_cover_music_play_control_prev_btn:I = 0x7f0200ce

.field public static final sview_cover_music_play_control_rw:I = 0x7f0200cf

.field public static final sview_cover_music_play_control_rw_focus:I = 0x7f0200d0

.field public static final sview_cover_music_play_control_rw_press:I = 0x7f0200d1

.field public static final sview_cover_music_player_close:I = 0x7f0200d2

.field public static final sview_cover_music_player_close_btn:I = 0x7f0200d3

.field public static final sview_cover_music_player_close_focus:I = 0x7f0200d4

.field public static final sview_cover_music_player_close_press:I = 0x7f0200d5

.field public static final sview_cover_music_player_ff:I = 0x7f0200d6

.field public static final sview_cover_music_player_ff_focus:I = 0x7f0200d7

.field public static final sview_cover_music_player_ff_press:I = 0x7f0200d8

.field public static final sview_cover_music_player_list_bg:I = 0x7f0200d9

.field public static final sview_cover_music_player_list_btn:I = 0x7f0200da

.field public static final sview_cover_music_player_next_btn:I = 0x7f0200db

.field public static final sview_cover_music_player_pause:I = 0x7f0200dc

.field public static final sview_cover_music_player_pause_btn:I = 0x7f0200dd

.field public static final sview_cover_music_player_pause_focus:I = 0x7f0200de

.field public static final sview_cover_music_player_pause_press:I = 0x7f0200df

.field public static final sview_cover_music_player_play:I = 0x7f0200e0

.field public static final sview_cover_music_player_play_btn:I = 0x7f0200e1

.field public static final sview_cover_music_player_play_focus:I = 0x7f0200e2

.field public static final sview_cover_music_player_play_press:I = 0x7f0200e3

.field public static final sview_cover_music_player_playing_ani_01:I = 0x7f0200e4

.field public static final sview_cover_music_player_playing_ani_02:I = 0x7f0200e5

.field public static final sview_cover_music_player_playing_ani_03:I = 0x7f0200e6

.field public static final sview_cover_music_player_playlist:I = 0x7f0200e7

.field public static final sview_cover_music_player_playlist_focus:I = 0x7f0200e8

.field public static final sview_cover_music_player_playlist_press:I = 0x7f0200e9

.field public static final sview_cover_music_player_prev_btn:I = 0x7f0200ea

.field public static final sview_cover_music_player_rw:I = 0x7f0200eb

.field public static final sview_cover_music_player_rw_focus:I = 0x7f0200ec

.field public static final sview_cover_music_player_rw_press:I = 0x7f0200ed

.field public static final sview_cover_music_player_scroll_bg:I = 0x7f0200ee

.field public static final sview_cover_music_player_title_album_bg:I = 0x7f0200ef

.field public static final sview_cover_music_player_title_album_bg_focus:I = 0x7f0200f0

.field public static final sview_cover_music_player_title_album_bg_press:I = 0x7f0200f1

.field public static final sview_cover_music_player_title_album_btn:I = 0x7f0200f2

.field public static final sview_cover_music_player_title_bg:I = 0x7f0200f3

.field public static final transparent_widget_scroll:I = 0x7f0200f4

.field public static final tw_action_bar_icon_download_holo:I = 0x7f0200f5

.field public static final tw_action_bar_sub_tab_bg_holo:I = 0x7f0200f6

.field public static final tw_action_bar_sub_tab_bg_holo_dim:I = 0x7f0200f7

.field public static final tw_actionbar_btn_add_normal:I = 0x7f0200f8

.field public static final tw_actionbar_btn_addtoplaylist_normal:I = 0x7f0200f9

.field public static final tw_actionbar_btn_delete_normal:I = 0x7f0200fa

.field public static final tw_actionbar_btn_edittitle_disable:I = 0x7f0200fb

.field public static final tw_actionbar_btn_edittitle_normal:I = 0x7f0200fc

.field public static final tw_actionbar_btn_search_disable:I = 0x7f0200fd

.field public static final tw_actionbar_btn_search_normal:I = 0x7f0200fe

.field public static final tw_btn_check_to_on_mtrl_000:I = 0x7f0200ff

.field public static final tw_btn_check_to_on_mtrl_001:I = 0x7f020100

.field public static final tw_btn_check_to_on_mtrl_002:I = 0x7f020101

.field public static final tw_btn_check_to_on_mtrl_003:I = 0x7f020102

.field public static final tw_btn_check_to_on_mtrl_004:I = 0x7f020103

.field public static final tw_btn_check_to_on_mtrl_005:I = 0x7f020104

.field public static final tw_btn_check_to_on_mtrl_006:I = 0x7f020105

.field public static final tw_btn_check_to_on_mtrl_007:I = 0x7f020106

.field public static final tw_btn_check_to_on_mtrl_008:I = 0x7f020107

.field public static final tw_btn_check_to_on_mtrl_009:I = 0x7f020108

.field public static final tw_btn_check_to_on_mtrl_010:I = 0x7f020109

.field public static final tw_btn_check_to_on_mtrl_011:I = 0x7f02010a

.field public static final tw_btn_check_to_on_mtrl_012:I = 0x7f02010b

.field public static final tw_btn_check_to_on_mtrl_013:I = 0x7f02010c

.field public static final tw_btn_check_to_on_mtrl_014:I = 0x7f02010d

.field public static final tw_btn_check_to_on_mtrl_015:I = 0x7f02010e

.field public static final tw_expander_close:I = 0x7f02010f

.field public static final tw_expander_close_01_holo:I = 0x7f020110

.field public static final tw_expander_open:I = 0x7f020111

.field public static final tw_expander_open_01_holo:I = 0x7f020112

.field public static final tw_list_divider_holo:I = 0x7f020113

.field public static final tw_list_focused_holo_light:I = 0x7f020114

.field public static final tw_list_icon_minus_holo_dark:I = 0x7f020115

.field public static final tw_list_icon_reorder_holo:I = 0x7f020116

.field public static final tw_list_section_divider_holo:I = 0x7f020117

.field public static final tw_list_selected_holo_light:I = 0x7f020118

.field public static final tw_no_item_bg_holo:I = 0x7f020119

.field public static final tw_preference_contents_list_left_split_default_dark:I = 0x7f02011a

.field public static final tw_preference_contents_list_left_split_default_press_dark:I = 0x7f02011b

.field public static final tw_spinner_mtrl_am_alpha:I = 0x7f02011c

.field public static final tw_tab_divider_holo:I = 0x7f02011d

.field public static final tw_tab_indicator_material:I = 0x7f02011e

.field public static final tw_tab_indicator_mtrl_alpha:I = 0x7f02011f

.field public static final tw_tab_indicator_ripple_material:I = 0x7f020120

.field public static final tw_toast_frame_holo_light:I = 0x7f020121

.field public static final unbounded_ripple_light:I = 0x7f020122

.field public static final unknwon_dmr:I = 0x7f020123

.field public static final widget_control_pause_btn:I = 0x7f020124

.field public static final widget_control_play_btn:I = 0x7f020125

.field public static final widget_music_btn_next_normal:I = 0x7f020126

.field public static final widget_music_btn_pause_normal:I = 0x7f020127

.field public static final widget_music_btn_play_normal:I = 0x7f020128

.field public static final widget_music_btn_prev_normal:I = 0x7f020129

.field public static final widget_music_btn_repeat_1_normal:I = 0x7f02012a

.field public static final widget_music_btn_repeat_all_normal:I = 0x7f02012b

.field public static final widget_music_btn_repeat_normal:I = 0x7f02012c

.field public static final widget_music_btn_shuffle_off_normal:I = 0x7f02012d

.field public static final widget_music_btn_shuffle_on_normal:I = 0x7f02012e

.field public static final widget_music_preview:I = 0x7f02012f

.field public static final widget_shadow:I = 0x7f020130

.field public static final winset_color:I = 0x7f020131


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
