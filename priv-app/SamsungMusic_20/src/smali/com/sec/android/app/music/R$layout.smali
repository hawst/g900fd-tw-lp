.class public final Lcom/sec/android/app/music/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/music/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final custom_action_mode_title_view:I = 0x7f040000

.field public static final custom_action_mode_view:I = 0x7f040001

.field public static final custom_cancel_done:I = 0x7f040002

.field public static final custom_progress_dialog:I = 0x7f040003

.field public static final device_no_item:I = 0x7f040004

.field public static final dialog:I = 0x7f040005

.field public static final dialog_list_view_main:I = 0x7f040006

.field public static final dlna_dmr_list_row:I = 0x7f040007

.field public static final full_player_album_view_phone:I = 0x7f040008

.field public static final full_player_k2hd_tag_stub_phone:I = 0x7f040009

.field public static final full_player_lyric_panel_common:I = 0x7f04000a

.field public static final full_player_nowplaying_list_phone:I = 0x7f04000b

.field public static final full_player_personal_mode_stub_phone:I = 0x7f04000c

.field public static final full_player_progress_scrubbing_popup_common:I = 0x7f04000d

.field public static final full_player_uhq_tag_stub_phone:I = 0x7f04000e

.field public static final hover_music_player_full_text_popup_black:I = 0x7f04000f

.field public static final hover_music_player_full_text_popup_white:I = 0x7f040010

.field public static final hover_music_popup_black:I = 0x7f040011

.field public static final hover_music_popup_playlist_black:I = 0x7f040012

.field public static final hover_music_popup_playlist_white:I = 0x7f040013

.field public static final hover_music_popup_white:I = 0x7f040014

.field public static final hover_music_square_list_popup_white:I = 0x7f040015

.field public static final hover_preview_popup_white:I = 0x7f040016

.field public static final hover_seekbar_popup:I = 0x7f040017

.field public static final hover_track_list_full_text_popup_black:I = 0x7f040018

.field public static final hover_track_list_full_text_popup_white:I = 0x7f040019

.field public static final list_category:I = 0x7f04001a

.field public static final list_category_device:I = 0x7f04001b

.field public static final list_content:I = 0x7f04001c

.field public static final list_content_albums_track:I = 0x7f04001d

.field public static final list_content_common:I = 0x7f04001e

.field public static final list_content_header_common:I = 0x7f04001f

.field public static final list_content_header_row:I = 0x7f040020

.field public static final list_content_header_view:I = 0x7f040021

.field public static final list_content_reorder:I = 0x7f040022

.field public static final list_content_split_sub_container:I = 0x7f040023

.field public static final list_content_square:I = 0x7f040024

.field public static final list_cover_view:I = 0x7f040025

.field public static final list_cover_view_album:I = 0x7f040026

.field public static final list_cover_view_text:I = 0x7f040027

.field public static final list_edit_dialog:I = 0x7f040028

.field public static final list_grid_item:I = 0x7f040029

.field public static final list_grid_view_stub:I = 0x7f04002a

.field public static final list_header_button:I = 0x7f04002b

.field public static final list_header_now_playing_button:I = 0x7f04002c

.field public static final list_header_select_all:I = 0x7f04002d

.field public static final list_header_shuffle:I = 0x7f04002e

.field public static final list_header_square_track:I = 0x7f04002f

.field public static final list_item_albumart_button:I = 0x7f040030

.field public static final list_item_albumart_group_two_line:I = 0x7f040031

.field public static final list_item_albumart_icon:I = 0x7f040032

.field public static final list_item_albumart_text_two_line:I = 0x7f040033

.field public static final list_item_animation:I = 0x7f040034

.field public static final list_item_checkbox_stub:I = 0x7f040035

.field public static final list_item_devices:I = 0x7f040036

.field public static final list_item_header_albumart:I = 0x7f040037

.field public static final list_item_now_playing:I = 0x7f040038

.field public static final list_item_text_count_one_line:I = 0x7f040039

.field public static final list_item_text_one_line:I = 0x7f04003a

.field public static final list_item_title_albumart_two_line:I = 0x7f04003b

.field public static final list_item_title_image:I = 0x7f04003c

.field public static final list_item_title_one_line:I = 0x7f04003d

.field public static final list_item_track_bigpond_top10:I = 0x7f04003e

.field public static final list_item_track_reorder:I = 0x7f04003f

.field public static final list_item_variable_text:I = 0x7f040040

.field public static final media_info_amg_logo:I = 0x7f040041

.field public static final media_info_body_layout:I = 0x7f040042

.field public static final media_info_default_layout:I = 0x7f040043

.field public static final media_info_default_text_item:I = 0x7f040044

.field public static final media_info_dialog_list_item:I = 0x7f040045

.field public static final media_info_edit_layout:I = 0x7f040046

.field public static final media_info_edit_text_item:I = 0x7f040047

.field public static final media_info_header:I = 0x7f040048

.field public static final media_info_list_album_item:I = 0x7f040049

.field public static final media_info_list_layout:I = 0x7f04004a

.field public static final media_info_list_subtitle:I = 0x7f04004b

.field public static final media_info_list_text_item1:I = 0x7f04004c

.field public static final media_info_list_text_item2:I = 0x7f04004d

.field public static final media_info_main_pager:I = 0x7f04004e

.field public static final media_info_plain_layout:I = 0x7f04004f

.field public static final mini_player:I = 0x7f040050

.field public static final mini_player_personal_mode_stub_phone:I = 0x7f040051

.field public static final mini_player_square:I = 0x7f040052

.field public static final mini_player_square_new:I = 0x7f040053

.field public static final music_content_no_item:I = 0x7f040054

.field public static final music_k2hd_tag_stub_phone:I = 0x7f040055

.field public static final music_main_activity:I = 0x7f040056

.field public static final music_main_pager_activity:I = 0x7f040057

.field public static final music_main_pager_fragment:I = 0x7f040058

.field public static final music_main_tab_activity:I = 0x7f040059

.field public static final music_main_tab_fragment:I = 0x7f04005a

.field public static final music_no_item:I = 0x7f04005b

.field public static final music_personal_stub_phone:I = 0x7f04005c

.field public static final music_player:I = 0x7f04005d

.field public static final music_player_allshare_details:I = 0x7f04005e

.field public static final music_player_control:I = 0x7f04005f

.field public static final music_player_control_button:I = 0x7f040060

.field public static final music_player_option_view:I = 0x7f040061

.field public static final music_player_progress_seekbar:I = 0x7f040062

.field public static final music_player_recommended_music_text:I = 0x7f040063

.field public static final music_player_side_cast_grid_view_item:I = 0x7f040064

.field public static final music_player_side_cast_view:I = 0x7f040065

.field public static final music_player_title:I = 0x7f040066

.field public static final music_player_title_bottom:I = 0x7f040067

.field public static final music_player_title_top:I = 0x7f040068

.field public static final music_player_volume_panel:I = 0x7f040069

.field public static final music_player_volume_panel_dmr:I = 0x7f04006a

.field public static final music_progress_stub_phone:I = 0x7f04006b

.field public static final music_selector_list_activity:I = 0x7f04006c

.field public static final music_selector_list_tab_activity:I = 0x7f04006d

.field public static final music_setting_menu_reorder_item:I = 0x7f04006e

.field public static final music_setting_new_soundalive_activity:I = 0x7f04006f

.field public static final music_setting_new_soundalive_layout:I = 0x7f040070

.field public static final music_setting_play_speed:I = 0x7f040071

.field public static final music_setting_sound_alive:I = 0x7f040072

.field public static final music_setting_sound_alive_eq:I = 0x7f040073

.field public static final music_setting_sound_alive_extended:I = 0x7f040074

.field public static final music_square:I = 0x7f040075

.field public static final music_square_for_sweep:I = 0x7f040076

.field public static final music_square_grid_view:I = 0x7f040077

.field public static final music_square_grid_view_sweep:I = 0x7f040078

.field public static final music_uhq_tag_stub_phone:I = 0x7f040079

.field public static final my_menu_reorder_content:I = 0x7f04007a

.field public static final new_soundalive_advanced_effect:I = 0x7f04007b

.field public static final new_soundalive_effects:I = 0x7f04007c

.field public static final new_soundalive_preset:I = 0x7f04007d

.field public static final new_soundalive_squarecell:I = 0x7f04007e

.field public static final notification_big_music_control:I = 0x7f04007f

.field public static final notification_music_control:I = 0x7f040080

.field public static final palm_touch_tutorial:I = 0x7f040081

.field public static final personal_folder_list_item:I = 0x7f040082

.field public static final popup_menu_item_layout:I = 0x7f040083

.field public static final query_browser_activity:I = 0x7f040084

.field public static final resolver_grid:I = 0x7f040085

.field public static final resolver_list_item:I = 0x7f040086

.field public static final search_view_action_bar:I = 0x7f040087

.field public static final set_as_activity:I = 0x7f040088

.field public static final star_off_image:I = 0x7f040089

.field public static final star_on_image:I = 0x7f04008a

.field public static final sview_cover_music_dialog:I = 0x7f04008b

.field public static final sview_cover_music_player:I = 0x7f04008c

.field public static final sview_cover_music_player_control:I = 0x7f04008d

.field public static final sview_cover_music_player_nowplaying_list:I = 0x7f04008e

.field public static final sview_cover_music_player_nowplaying_list_item:I = 0x7f04008f

.field public static final tab_indicator:I = 0x7f040090

.field public static final track_activity:I = 0x7f040091

.field public static final tutorial_complete_popup:I = 0x7f040092

.field public static final user_eq_control_bar:I = 0x7f040093

.field public static final user_ext_level:I = 0x7f040094

.field public static final widget_layout:I = 0x7f040095

.field public static final widget_list_item_track:I = 0x7f040096


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
