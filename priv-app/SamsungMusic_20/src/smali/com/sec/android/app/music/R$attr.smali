.class public final Lcom/sec/android/app/music/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/music/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final albumOpacity:I = 0x7f010015

.field public static final albumSize:I = 0x7f010016

.field public static final albumSpacing:I = 0x7f010017

.field public static final albumSpacingAdditional:I = 0x7f010018

.field public static final backgroundColor:I = 0x7f010019

.field public static final buttonAnimationTime:I = 0x7f01000f

.field public static final cacheColorHint:I = 0x7f010043

.field public static final checkable:I = 0x7f01001f

.field public static final checkableId:I = 0x7f010010

.field public static final circleRadius:I = 0x7f01000c

.field public static final clickAnimation:I = 0x7f010013

.field public static final columnWidth:I = 0x7f01004a

.field public static final drawSelectorOnTop:I = 0x7f01003f

.field public static final dualcolorProgress:I = 0x7f01002d

.field public static final edgeAnimationTime:I = 0x7f01000e

.field public static final edgeColor:I = 0x7f01000d

.field public static final edgeWidth:I = 0x7f01000a

.field public static final focusedImageA:I = 0x7f010006

.field public static final focusedImageB:I = 0x7f010007

.field public static final gl_artistTextStyle:I = 0x7f010000

.field public static final gl_titleTextStyle:I = 0x7f010001

.field public static final gravity:I = 0x7f010002

.field public static final gridViewStyle:I = 0x7f010003

.field public static final horizontalSpacing:I = 0x7f010047

.field public static final imageA:I = 0x7f010004

.field public static final imageB:I = 0x7f010005

.field public static final listSelector:I = 0x7f01003e

.field public static final maskRadius:I = 0x7f01000b

.field public static final maxTiltedAngle:I = 0x7f010011

.field public static final numColumns:I = 0x7f01004c

.field public static final numRows:I = 0x7f01004d

.field public static final pressedImageA:I = 0x7f010008

.field public static final pressedImageB:I = 0x7f010009

.field public static final progress:I = 0x7f010025

.field public static final rowHeight:I = 0x7f01004b

.field public static final scrollDirectionLandscape:I = 0x7f010046

.field public static final scrollDirectionPortrait:I = 0x7f010045

.field public static final scrollingCache:I = 0x7f010041

.field public static final scrollingFriction:I = 0x7f01001d

.field public static final secondaryProgress:I = 0x7f010026

.field public static final selectedAlbumBorder:I = 0x7f01001a

.field public static final selectedAlbumSize:I = 0x7f01001b

.field public static final shadowBitmap:I = 0x7f01001e

.field public static final smoothScrollbar:I = 0x7f010044

.field public static final stackFromBottom:I = 0x7f010040

.field public static final stretchMode:I = 0x7f010049

.field public static final textLayout:I = 0x7f01001c

.field public static final theme:I = 0x7f010032

.field public static final tiltedK:I = 0x7f010012

.field public static final tiltedReturnAnimation:I = 0x7f010014

.field public static final transcriptMode:I = 0x7f010042

.field public static final twBackgroundColor:I = 0x7f01002a

.field public static final twBackgroundDrawable:I = 0x7f010027

.field public static final twDualColorProgressColor:I = 0x7f01002f

.field public static final twDualColorProgressDrawable:I = 0x7f01002e

.field public static final twIndicatorThickness:I = 0x7f010030

.field public static final twMax:I = 0x7f010024

.field public static final twMaxHeight:I = 0x7f010023

.field public static final twMaxWidth:I = 0x7f010021

.field public static final twMinHeight:I = 0x7f010022

.field public static final twMinWidth:I = 0x7f010020

.field public static final twProgressBarStyle:I = 0x7f01003d

.field public static final twProgressColor:I = 0x7f01002b

.field public static final twProgressDrawable:I = 0x7f010028

.field public static final twProgressOrientation:I = 0x7f010031

.field public static final twSecondaryColor:I = 0x7f01002c

.field public static final twSecondaryDrawable:I = 0x7f010029

.field public static final twSeekBarDisableAlpha:I = 0x7f010036

.field public static final twSeekBarIncrement:I = 0x7f010035

.field public static final twSeekBarSeekable:I = 0x7f010037

.field public static final twSeekBarStyle:I = 0x7f01003c

.field public static final twSeekBarThumb:I = 0x7f010033

.field public static final twSeekBarThumbOffset:I = 0x7f010034

.field public static final twSeekThumbFontBoldStyle:I = 0x7f01003a

.field public static final twSeekThumbFontColor:I = 0x7f010038

.field public static final twSeekThumbFontEnable:I = 0x7f01003b

.field public static final twSeekThumbFontSize:I = 0x7f010039

.field public static final verticalSpacing:I = 0x7f010048


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
