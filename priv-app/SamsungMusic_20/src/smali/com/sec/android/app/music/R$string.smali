.class public final Lcom/sec/android/app/music/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/music/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final IDS_MUSIC_BODY_UNABLE_TO_CREATE_PLAYLIST_MAXIMUM_NUMBER_OF_PLAYLISTS_REACHED:I = 0x7f100000

.field public static final IDS_MUSIC_POP_UNABLE_TO_ADD_MORE_MUSIC_MAX_REACHED_MSG:I = 0x7f100001

.field public static final K2HD:I = 0x7f100002

.field public static final K2HD_dialog_msg:I = 0x7f100003

.field public static final K2HD_on:I = 0x7f100004

.field public static final K2HD_summary:I = 0x7f100005

.field public static final adapt_sound:I = 0x7f100006

.field public static final adapt_sound_check_again:I = 0x7f100007

.field public static final add_1_item_1_item_exists:I = 0x7f100008

.field public static final add_1_item_n_item_exist:I = 0x7f100009

.field public static final add_music_explanation:I = 0x7f1001da

.field public static final add_n_item_n_item_exist:I = 0x7f10000a

.field public static final add_n_items_1_item_exist:I = 0x7f10000b

.field public static final add_to_now_playing:I = 0x7f10000c

.field public static final added_to_personal_page_1_item:I = 0x7f10000d

.field public static final added_to_personal_page_n_items:I = 0x7f10000e

.field public static final addto:I = 0x7f10000f

.field public static final advanced:I = 0x7f100010

.field public static final after_15_min:I = 0x7f100011

.field public static final after_1_hour:I = 0x7f100012

.field public static final after_1_hour_30_min:I = 0x7f100013

.field public static final after_1_hr:I = 0x7f100014

.field public static final after_1_hr_1_min:I = 0x7f100015

.field public static final after_1_hr_n_mins:I = 0x7f100016

.field public static final after_1_min:I = 0x7f100017

.field public static final after_2_hour:I = 0x7f100018

.field public static final after_30_min:I = 0x7f100019

.field public static final after_n_hrs:I = 0x7f10001a

.field public static final after_n_hrs_1_min:I = 0x7f10001b

.field public static final after_n_hrs_n_mins:I = 0x7f10001c

.field public static final after_n_mins:I = 0x7f10001d

.field public static final album:I = 0x7f10001e

.field public static final albums:I = 0x7f10001f

.field public static final already_exist_file_copy:I = 0x7f100020

.field public static final amg_lisence_line:I = 0x7f100021

.field public static final and_n_more:I = 0x7f100022

.field public static final app_name:I = 0x7f100023

.field public static final app_not_available:I = 0x7f100024

.field public static final apply_to_all_items:I = 0x7f100025

.field public static final artist:I = 0x7f100026

.field public static final artists:I = 0x7f100027

.field public static final auto:I = 0x7f100028

.field public static final auto_recommendation:I = 0x7f100029

.field public static final auto_recommendation_help:I = 0x7f10002a

.field public static final available_devices:I = 0x7f10002b

.field public static final barge_in_notification:I = 0x7f10002c

.field public static final basic:I = 0x7f10002d

.field public static final bass_boost:I = 0x7f10002e

.field public static final bigpond_menu_download:I = 0x7f10002f

.field public static final bigpond_network_error_msg:I = 0x7f100030

.field public static final bigpond_top_10:I = 0x7f100031

.field public static final bigpond_unable_to_download:I = 0x7f100032

.field public static final bigpond_unable_to_download_image:I = 0x7f100033

.field public static final bigpond_update:I = 0x7f100034

.field public static final biography:I = 0x7f100035

.field public static final bit_depth:I = 0x7f100036

.field public static final blank:I = 0x7f100037

.field public static final buffering:I = 0x7f100038

.field public static final buy:I = 0x7f1001db

.field public static final cafe:I = 0x7f100039

.field public static final cancel:I = 0x7f10003a

.field public static final change_horizontal_axis:I = 0x7f10003b

.field public static final change_image:I = 0x7f1001dc

.field public static final change_language:I = 0x7f1001dd

.field public static final change_player:I = 0x7f10003c

.field public static final changeplayer_descrpition_mirroror:I = 0x7f10003d

.field public static final changeplayer_descrpition_playvia:I = 0x7f10003e

.field public static final classic:I = 0x7f10003f

.field public static final clean_repeat:I = 0x7f100040

.field public static final club:I = 0x7f100041

.field public static final composers:I = 0x7f100042

.field public static final concert_hall:I = 0x7f100043

.field public static final connecting:I = 0x7f100044

.field public static final connection_check_hdmi:I = 0x7f100045

.field public static final connection_check_title_hdmi:I = 0x7f100046

.field public static final create:I = 0x7f100047

.field public static final credits:I = 0x7f100048

.field public static final custom:I = 0x7f100049

.field public static final dance:I = 0x7f10004a

.field public static final data_check_help_mobile_body:I = 0x7f10004b

.field public static final data_check_help_mobile_title:I = 0x7f10004c

.field public static final data_check_help_wifi_body:I = 0x7f10004d

.field public static final data_check_help_wifi_title:I = 0x7f10004e

.field public static final data_connection_failed:I = 0x7f10004f

.field public static final default_playlists:I = 0x7f100050

.field public static final deleted:I = 0x7f100051

.field public static final deleting_items:I = 0x7f100052

.field public static final deregisterd_device:I = 0x7f100053

.field public static final deregisterd_device_message:I = 0x7f100054

.field public static final deregistered_progress_message:I = 0x7f100055

.field public static final deselect_all:I = 0x7f100056

.field public static final devices:I = 0x7f100057

.field public static final disc_n:I = 0x7f100058

.field public static final discography:I = 0x7f100059

.field public static final disconnected_devices:I = 0x7f10005a

.field public static final dlna_no_item:I = 0x7f10005b

.field public static final dlna_refresh_no_result_msg:I = 0x7f10005c

.field public static final dlna_refresh_option:I = 0x7f10005d

.field public static final dlna_refresh_start_msg:I = 0x7f10005e

.field public static final do_not_show_again:I = 0x7f10005f

.field public static final done:I = 0x7f100060

.field public static final download_all:I = 0x7f100061

.field public static final download_completed:I = 0x7f1001de

.field public static final download_failed:I = 0x7f1001df

.field public static final drm_acquiring_license:I = 0x7f100062

.field public static final drm_failed_acquire_license:I = 0x7f100063

.field public static final drm_no_data_connectivity:I = 0x7f100064

.field public static final drm_no_longer_available:I = 0x7f100065

.field public static final drm_play_now_q:I = 0x7f100066

.field public static final drm_server_problem_msg:I = 0x7f100067

.field public static final drm_sorry_license_expired:I = 0x7f100068

.field public static final drm_unable_access_server_msg:I = 0x7f100069

.field public static final drm_want_delete_q:I = 0x7f10006a

.field public static final drm_want_unlock_q:I = 0x7f10006b

.field public static final duration_format_long:I = 0x7f10006c

.field public static final duration_format_short:I = 0x7f10006d

.field public static final during_sidesync:I = 0x7f10006e

.field public static final edit_meta_explanation1:I = 0x7f10006f

.field public static final edit_meta_explanation2:I = 0x7f100070

.field public static final edit_meta_saved:I = 0x7f100071

.field public static final edit_meta_saved_unicode:I = 0x7f100072

.field public static final effect:I = 0x7f100073

.field public static final encoding_type:I = 0x7f100074

.field public static final eq_1:I = 0x7f100075

.field public static final eq_2:I = 0x7f100076

.field public static final eq_3:I = 0x7f100077

.field public static final eq_4:I = 0x7f100078

.field public static final eq_5:I = 0x7f100079

.field public static final eq_6:I = 0x7f10007a

.field public static final eq_7:I = 0x7f10007b

.field public static final equaliser:I = 0x7f10007c

.field public static final error:I = 0x7f10007d

.field public static final error_not_support_type:I = 0x7f10007e

.field public static final error_playing_other_device:I = 0x7f10007f

.field public static final error_unknown:I = 0x7f100080

.field public static final ext_1:I = 0x7f100081

.field public static final ext_2:I = 0x7f100082

.field public static final ext_3:I = 0x7f100083

.field public static final ext_4:I = 0x7f100084

.field public static final ext_5:I = 0x7f100085

.field public static final extended:I = 0x7f100086

.field public static final externalization:I = 0x7f100087

.field public static final failed_to_connect_network:I = 0x7f100088

.field public static final failed_to_move:I = 0x7f100089

.field public static final favourite_tracks:I = 0x7f10008a

.field public static final favourites_explanation:I = 0x7f1001e0

.field public static final file_name_in_use:I = 0x7f10008b

.field public static final file_size:I = 0x7f10008c

.field public static final finding:I = 0x7f10008d

.field public static final folders:I = 0x7f10008e

.field public static final format:I = 0x7f10008f

.field public static final genre:I = 0x7f100090

.field public static final genres:I = 0x7f100091

.field public static final get_signature:I = 0x7f100092

.field public static final go_to_storage:I = 0x7f100093

.field public static final go_to_store:I = 0x7f1001e1

.field public static final go_to_store_explanation:I = 0x7f1001e2

.field public static final google_drive:I = 0x7f100094

.field public static final help:I = 0x7f100095

.field public static final image:I = 0x7f1001e3

.field public static final information:I = 0x7f100096

.field public static final invalid_character:I = 0x7f100097

.field public static final jazz:I = 0x7f100098

.field public static final knox:I = 0x7f100099

.field public static final lgt_checking_licence:I = 0x7f10009a

.field public static final lgt_licences_reset:I = 0x7f10009b

.field public static final lgt_mp3_ringtone:I = 0x7f10009c

.field public static final lgt_reset_licences:I = 0x7f10009d

.field public static final lgt_unable_to_access_msg:I = 0x7f10009e

.field public static final library_update:I = 0x7f1001e4

.field public static final library_update_help:I = 0x7f1001e5

.field public static final list_menu_move_to_knox:I = 0x7f10009f

.field public static final list_menu_remove_from_KNOX:I = 0x7f1000a0

.field public static final loading:I = 0x7f1000a1

.field public static final location:I = 0x7f1001e6

.field public static final low_battery:I = 0x7f1000a2

.field public static final low_battery_warning_msg:I = 0x7f1000a3

.field public static final lyrics:I = 0x7f1000a4

.field public static final max_char_reached_msg:I = 0x7f1000a5

.field public static final menu_add_to_favourites:I = 0x7f1000a6

.field public static final menu_add_to_nowplaying:I = 0x7f1000a7

.field public static final menu_add_to_personal_page:I = 0x7f1000a8

.field public static final menu_add_to_playlist:I = 0x7f1000a9

.field public static final menu_add_tracks:I = 0x7f1000aa

.field public static final menu_change_order:I = 0x7f1000ab

.field public static final menu_create_playlist:I = 0x7f1000ac

.field public static final menu_delete:I = 0x7f1000ad

.field public static final menu_details:I = 0x7f1000ae

.field public static final menu_download:I = 0x7f1000af

.field public static final menu_edit:I = 0x7f1000b0

.field public static final menu_edit_image:I = 0x7f1001e7

.field public static final menu_edit_title:I = 0x7f1000b1

.field public static final menu_end:I = 0x7f1000b2

.field public static final menu_hide_music_view:I = 0x7f1001e8

.field public static final menu_list:I = 0x7f1000b3

.field public static final menu_play_via_group_play:I = 0x7f1000b4

.field public static final menu_remove:I = 0x7f1000b5

.field public static final menu_remove_from_favourites:I = 0x7f1000b6

.field public static final menu_remove_from_personal_page:I = 0x7f1000b7

.field public static final menu_samsung_music:I = 0x7f1000b8

.field public static final menu_save_as_playlist:I = 0x7f1000b9

.field public static final menu_search_my_music:I = 0x7f1000ba

.field public static final menu_select:I = 0x7f1000bb

.field public static final menu_set_as:I = 0x7f1000bc

.field public static final menu_share_music:I = 0x7f1001e9

.field public static final menu_share_via:I = 0x7f1000bd

.field public static final menu_share_with_other_devices:I = 0x7f1000be

.field public static final menu_show_music_view:I = 0x7f1001ea

.field public static final menu_shuffle:I = 0x7f1000bf

.field public static final menu_smart_bt:I = 0x7f1000c0

.field public static final menu_thumbnail:I = 0x7f1000c1

.field public static final menu_via_bluetooth:I = 0x7f1000c2

.field public static final menu_via_device:I = 0x7f1000c3

.field public static final menu_view:I = 0x7f1001eb

.field public static final menu_view_as:I = 0x7f1000c4

.field public static final mood_calm:I = 0x7f1000c5

.field public static final mood_exciting:I = 0x7f1000c6

.field public static final mood_joyful:I = 0x7f1000c7

.field public static final mood_new:I = 0x7f1000c8

.field public static final mood_old:I = 0x7f1000c9

.field public static final mood_passionate:I = 0x7f1000ca

.field public static final moods:I = 0x7f1000cb

.field public static final most_played:I = 0x7f1000cc

.field public static final move_failed:I = 0x7f1000cd

.field public static final move_to_1item:I = 0x7f1000ce

.field public static final move_to_knox_1item:I = 0x7f1000cf

.field public static final move_to_knox_nitem:I = 0x7f1000d0

.field public static final move_to_nitem:I = 0x7f1000d1

.field public static final moving:I = 0x7f1000d2

.field public static final moving_playing_song_confirmed_moved:I = 0x7f1000d3

.field public static final moving_playing_song_confirmed_removed:I = 0x7f1000d4

.field public static final music_auto_off:I = 0x7f1000d5

.field public static final music_does_not_exist:I = 0x7f1000d6

.field public static final music_menu:I = 0x7f1001ec

.field public static final music_square:I = 0x7f1000d7

.field public static final music_square_explanation:I = 0x7f1000d8

.field public static final my_device:I = 0x7f1000d9

.field public static final my_music:I = 0x7f1001ed

.field public static final my_playlist:I = 0x7f1000da

.field public static final my_playlists:I = 0x7f1000db

.field public static final n_bit:I = 0x7f1000dc

.field public static final n_deleted:I = 0x7f1000dd

.field public static final n_items_selected:I = 0x7f1000de

.field public static final n_khz:I = 0x7f1000df

.field public static final nearby_devices:I = 0x7f1000e0

.field public static final nearby_devices_found:I = 0x7f1000e1

.field public static final nearby_devices_not_found_tap_refreshicon:I = 0x7f1000e2

.field public static final network_error_occurred_msg:I = 0x7f1000e3

.field public static final new_soundAlive_not_applied_during_pause:I = 0x7f1000e4

.field public static final new_sound_alive_auto_off:I = 0x7f1000e5

.field public static final new_sound_alive_bass:I = 0x7f1000e6

.field public static final new_sound_alive_club_explanation:I = 0x7f1000e7

.field public static final new_sound_alive_concert_hall_explanation:I = 0x7f1000e8

.field public static final new_sound_alive_instrument:I = 0x7f1000e9

.field public static final new_sound_alive_none_explanation:I = 0x7f1000ea

.field public static final new_sound_alive_studio_explanation:I = 0x7f1000eb

.field public static final new_sound_alive_treble:I = 0x7f1000ec

.field public static final new_sound_alive_tube_amp_effect_explanation:I = 0x7f1000ed

.field public static final new_sound_alive_virtual_71_ch_explanation:I = 0x7f1000ee

.field public static final new_sound_alive_vocal:I = 0x7f1000ef

.field public static final new_soundalive_3D:I = 0x7f1000f0

.field public static final new_soundalive_bass:I = 0x7f1000f1

.field public static final new_soundalive_clarity:I = 0x7f1000f2

.field public static final no:I = 0x7f1000f3

.field public static final no_albums:I = 0x7f1000f4

.field public static final no_artists:I = 0x7f1000f5

.field public static final no_composers:I = 0x7f1000f6

.field public static final no_device_contents:I = 0x7f1000f7

.field public static final no_device_how_to_register:I = 0x7f1000f8

.field public static final no_dms:I = 0x7f1000f9

.field public static final no_dms_for_chn:I = 0x7f1000fa

.field public static final no_dms_wifi_on:I = 0x7f1000fb

.field public static final no_folders:I = 0x7f1000fc

.field public static final no_genres:I = 0x7f1000fd

.field public static final no_items:I = 0x7f1000fe

.field public static final no_music_files:I = 0x7f1000ff

.field public static final no_playlists:I = 0x7f100100

.field public static final no_recordings:I = 0x7f100101

.field public static final no_results:I = 0x7f100102

.field public static final no_slink_device_unsigned:I = 0x7f100103

.field public static final no_songs_playing:I = 0x7f100104

.field public static final no_square_selected:I = 0x7f100105

.field public static final no_tracks:I = 0x7f100106

.field public static final no_years:I = 0x7f100107

.field public static final none:I = 0x7f100108

.field public static final normal:I = 0x7f100109

.field public static final not_connected:I = 0x7f10010a

.field public static final not_enough_memory_popup:I = 0x7f10010b

.field public static final not_enough_memory_toast:I = 0x7f10010c

.field public static final not_enough_memory_toast_app_start:I = 0x7f10010d

.field public static final not_registered:I = 0x7f10010e

.field public static final not_support_ff:I = 0x7f10010f

.field public static final not_support_rew:I = 0x7f100110

.field public static final notice_low_storage:I = 0x7f100111

.field public static final now_playing:I = 0x7f100112

.field public static final off:I = 0x7f100113

.field public static final ok:I = 0x7f100114

.field public static final on:I = 0x7f100115

.field public static final one_item_delete:I = 0x7f1001ee

.field public static final option_alarm_tone:I = 0x7f100116

.field public static final option_caller_ringtone:I = 0x7f100117

.field public static final option_phone_ringtone:I = 0x7f100118

.field public static final option_set_as:I = 0x7f100119

.field public static final others:I = 0x7f10011a

.field public static final overwrite:I = 0x7f10011b

.field public static final palm_touch_toast_finish:I = 0x7f10011c

.field public static final palm_touch_toast_start:I = 0x7f10011d

.field public static final palm_tutorial_title:I = 0x7f10011e

.field public static final path:I = 0x7f10011f

.field public static final personal_mode:I = 0x7f100120

.field public static final play:I = 0x7f100121

.field public static final play_from_beginning:I = 0x7f100122

.field public static final play_speed:I = 0x7f100123

.field public static final play_tracks_explanation:I = 0x7f1001ef

.field public static final playback_failed_msg:I = 0x7f100124

.field public static final player_is_not_available_msg:I = 0x7f100125

.field public static final player_settings:I = 0x7f1001f0

.field public static final playlist_name_already_exists:I = 0x7f100126

.field public static final playlists:I = 0x7f100127

.field public static final pop:I = 0x7f100128

.field public static final processing:I = 0x7f100129

.field public static final processing_set_as:I = 0x7f10012a

.field public static final recently_added:I = 0x7f10012b

.field public static final recently_played:I = 0x7f10012c

.field public static final recording_date:I = 0x7f10012d

.field public static final recordings:I = 0x7f10012e

.field public static final refresh:I = 0x7f10012f

.field public static final register:I = 0x7f100130

.field public static final register_device_body:I = 0x7f100131

.field public static final register_device_header:I = 0x7f100132

.field public static final registered_devices:I = 0x7f100133

.field public static final registration_guide:I = 0x7f100134

.field public static final removed_from_favourites:I = 0x7f1001f1

.field public static final removed_from_personal_page_1_item:I = 0x7f100135

.field public static final removed_from_personal_page_1_item_no_place:I = 0x7f100136

.field public static final removed_from_personal_page_n_items:I = 0x7f100137

.field public static final removed_from_personal_page_n_items_no_place:I = 0x7f100138

.field public static final rename:I = 0x7f100139

.field public static final replace:I = 0x7f10013a

.field public static final reset:I = 0x7f10013b

.field public static final result_for_p_p:I = 0x7f1001f2

.field public static final retrieving_additional_tag_data:I = 0x7f10013c

.field public static final review:I = 0x7f10013d

.field public static final rich_info_list_item_text_n_p:I = 0x7f10013e

.field public static final ringtone_added:I = 0x7f10013f

.field public static final rock:I = 0x7f100140

.field public static final s_link_charge_warning_info:I = 0x7f100141

.field public static final s_link_load_the_content:I = 0x7f100142

.field public static final s_link_load_the_content_question:I = 0x7f100143

.field public static final s_link_refresh_option:I = 0x7f100144

.field public static final sampling_rate:I = 0x7f100145

.field public static final samsung_link:I = 0x7f100146

.field public static final save:I = 0x7f100147

.field public static final saving:I = 0x7f100148

.field public static final scrubbing_half_speed_msg:I = 0x7f100149

.field public static final scrubbing_normal_speed_msg:I = 0x7f10014a

.field public static final scrubbing_quarter_speed_msg:I = 0x7f10014b

.field public static final search:I = 0x7f10014c

.field public static final search_device:I = 0x7f10014d

.field public static final search_label:I = 0x7f1001f3

.field public static final search_no_keyword:I = 0x7f1001f4

.field public static final search_settings_description:I = 0x7f10014e

.field public static final search_with:I = 0x7f10014f

.field public static final select_album:I = 0x7f100150

.field public static final select_all:I = 0x7f100151

.field public static final select_device:I = 0x7f100152

.field public static final select_sim:I = 0x7f100153

.field public static final select_tracks:I = 0x7f100154

.field public static final selected_item_delete:I = 0x7f1001f5

.field public static final selected_list_empty:I = 0x7f100155

.field public static final set_default:I = 0x7f100156

.field public static final settings:I = 0x7f100157

.field public static final settings_playlists:I = 0x7f100158

.field public static final sign_in:I = 0x7f100159

.field public static final similar_albums:I = 0x7f10015a

.field public static final similar_artists:I = 0x7f10015b

.field public static final size:I = 0x7f10015c

.field public static final size_b:I = 0x7f10015d

.field public static final size_kb:I = 0x7f10015e

.field public static final size_mb:I = 0x7f10015f

.field public static final slink_how_to_register:I = 0x7f100160

.field public static final smart_volume:I = 0x7f100161

.field public static final smart_volume_adjust_msg:I = 0x7f100162

.field public static final smart_volume_on:I = 0x7f100163

.field public static final smart_volume_summary:I = 0x7f100164

.field public static final sound_3d_effect_not_supported:I = 0x7f100165

.field public static final sound_alive:I = 0x7f100166

.field public static final sound_effect_not_supported_by_bluetooth:I = 0x7f100167

.field public static final sound_effect_not_supported_by_hdmi:I = 0x7f100168

.field public static final sound_effect_not_supported_by_line_out:I = 0x7f100169

.field public static final sound_effect_not_supported_by_speaker:I = 0x7f1001f6

.field public static final sound_effect_supported_only_headphones:I = 0x7f10016a

.field public static final sound_effect_works_in_earphone_bt_only:I = 0x7f10016b

.field public static final sound_effect_works_in_earphone_only:I = 0x7f10016c

.field public static final split_view:I = 0x7f1001f7

.field public static final split_view_summary:I = 0x7f1001f8

.field public static final srs_surround:I = 0x7f10016d

.field public static final srs_wow_hd:I = 0x7f10016e

.field public static final stms_appgroup:I = 0x7f10016f

.field public static final stop_playing_music_q:I = 0x7f100170

.field public static final studio:I = 0x7f100171

.field public static final suggestions_for_you:I = 0x7f100172

.field public static final tabs:I = 0x7f100173

.field public static final tags:I = 0x7f100174

.field public static final take_photo:I = 0x7f1001f9

.field public static final tap_to_collapse_the_list:I = 0x7f100175

.field public static final tap_to_expand_the_list:I = 0x7f100176

.field public static final time:I = 0x7f100177

.field public static final title:I = 0x7f100178

.field public static final track:I = 0x7f100179

.field public static final track_1:I = 0x7f1001fa

.field public static final track_length:I = 0x7f10017a

.field public static final track_list:I = 0x7f10017b

.field public static final track_more:I = 0x7f1001fb

.field public static final track_number:I = 0x7f10017c

.field public static final tracks:I = 0x7f10017d

.field public static final treble_boost:I = 0x7f10017e

.field public static final tts_add_to_playlist:I = 0x7f10017f

.field public static final tts_albumcover:I = 0x7f100180

.field public static final tts_back:I = 0x7f1001fc

.field public static final tts_button:I = 0x7f100181

.field public static final tts_cell:I = 0x7f100182

.field public static final tts_close:I = 0x7f100183

.field public static final tts_control_bar:I = 0x7f100184

.field public static final tts_double_tap_to_collapse_the_list:I = 0x7f100185

.field public static final tts_double_tap_to_expand_the_list:I = 0x7f100186

.field public static final tts_duration:I = 0x7f1001fd

.field public static final tts_fast_forward:I = 0x7f100187

.field public static final tts_favourite_off:I = 0x7f100188

.field public static final tts_favourite_on:I = 0x7f100189

.field public static final tts_go_details_page:I = 0x7f1001fe

.field public static final tts_go_select_category_page:I = 0x7f1001ff

.field public static final tts_go_to_music:I = 0x7f10018a

.field public static final tts_header:I = 0x7f10018b

.field public static final tts_hide_player:I = 0x7f100200

.field public static final tts_hours:I = 0x7f10018c

.field public static final tts_list:I = 0x7f10018d

.field public static final tts_minutes:I = 0x7f10018e

.field public static final tts_more_options:I = 0x7f10018f

.field public static final tts_music_player:I = 0x7f100190

.field public static final tts_music_view:I = 0x7f100201

.field public static final tts_navigate_up:I = 0x7f100202

.field public static final tts_next:I = 0x7f100191

.field public static final tts_not_selected:I = 0x7f100192

.field public static final tts_pause:I = 0x7f100193

.field public static final tts_play:I = 0x7f100194

.field public static final tts_play_radio:I = 0x7f100203

.field public static final tts_play_speed_minus:I = 0x7f100195

.field public static final tts_play_speed_plus:I = 0x7f100196

.field public static final tts_playing:I = 0x7f100204

.field public static final tts_previous:I = 0x7f100197

.field public static final tts_private:I = 0x7f100198

.field public static final tts_repeat_1:I = 0x7f100199

.field public static final tts_repeat_all:I = 0x7f10019a

.field public static final tts_repeat_off:I = 0x7f10019b

.field public static final tts_rewind:I = 0x7f10019c

.field public static final tts_seconds:I = 0x7f10019d

.field public static final tts_select_mode:I = 0x7f100205

.field public static final tts_selected:I = 0x7f10019e

.field public static final tts_shuffle_off:I = 0x7f10019f

.field public static final tts_shuffle_on:I = 0x7f1001a0

.field public static final tts_size_b:I = 0x7f1001a1

.field public static final tts_size_kb:I = 0x7f1001a2

.field public static final tts_size_mb:I = 0x7f1001a3

.field public static final tts_stop:I = 0x7f100206

.field public static final tts_sub_title:I = 0x7f100207

.field public static final tts_swipe_two_fingers:I = 0x7f1001a4

.field public static final tts_tab:I = 0x7f1001a5

.field public static final tts_tab_currently_set:I = 0x7f1001a6

.field public static final tts_tab_n_of_n:I = 0x7f1001a7

.field public static final tts_volume:I = 0x7f1001a8

.field public static final tts_volume_down:I = 0x7f1001a9

.field public static final tts_volume_mute:I = 0x7f1001aa

.field public static final tts_volume_up:I = 0x7f1001ab

.field public static final tube_amp_effect:I = 0x7f1001ac

.field public static final tutorial_complete:I = 0x7f1001ad

.field public static final tw_seekbarsplit_seekbar_control:I = 0x7f1001ae

.field public static final twstr_indexlist:I = 0x7f1001af

.field public static final uhq:I = 0x7f1001b0

.field public static final unable_to_find_item:I = 0x7f1001b1

.field public static final unable_to_play:I = 0x7f1001b2

.field public static final unable_to_play_during_call:I = 0x7f1001b3

.field public static final unable_to_save_file:I = 0x7f1001b4

.field public static final unable_to_scan_device:I = 0x7f1001b5

.field public static final unable_to_scan_device_for_chn:I = 0x7f1001b6

.field public static final unable_to_scan_device_for_unsupported_wfd:I = 0x7f1001b7

.field public static final unable_to_scan_header:I = 0x7f1001b8

.field public static final unable_to_use_ff_and_rew_msg:I = 0x7f1001b9

.field public static final unalbe_show_lyric_with_no_local_song:I = 0x7f1001ba

.field public static final unknown:I = 0x7f1001bb

.field public static final unknown_album:I = 0x7f1001bc

.field public static final unknown_artist:I = 0x7f1001bd

.field public static final unsupported_file_type:I = 0x7f100208

.field public static final update_complete:I = 0x7f100209

.field public static final updating:I = 0x7f10020a

.field public static final various_artists:I = 0x7f1001be

.field public static final virtual_71_ch:I = 0x7f1001bf

.field public static final vocal:I = 0x7f1001c0

.field public static final voice_call_ringtone:I = 0x7f1001c1

.field public static final voice_call_ringtone2:I = 0x7f1001c2

.field public static final voice_call_ringtone_slot1:I = 0x7f1001c3

.field public static final voice_call_ringtone_slot2:I = 0x7f1001c4

.field public static final voice_control:I = 0x7f1001c5

.field public static final voice_control_summary:I = 0x7f1001c6

.field public static final volume_format:I = 0x7f1001c7

.field public static final warning:I = 0x7f1001c8

.field public static final wfd:I = 0x7f1001c9

.field public static final wfd_disconnect_noti:I = 0x7f1001ca

.field public static final wfd_dongle_explanation:I = 0x7f1001cb

.field public static final wfd_error_limited_contents:I = 0x7f1001cc

.field public static final wfd_error_power_saving_mode_on:I = 0x7f1001cd

.field public static final wfd_error_running_all_together:I = 0x7f1001ce

.field public static final wfd_error_running_group_play:I = 0x7f1001cf

.field public static final wfd_error_running_sidesync:I = 0x7f1001d0

.field public static final wfd_error_wifi_direct:I = 0x7f1001d1

.field public static final wfd_error_wifi_direct_for_chn:I = 0x7f1001d2

.field public static final wfd_error_wifi_hotspot:I = 0x7f1001d3

.field public static final wfd_error_wifi_hotspot_for_chn:I = 0x7f1001d4

.field public static final wfd_unable_to_scan_for_devices:I = 0x7f1001d5

.field public static final wifi:I = 0x7f1001d6

.field public static final wifi_for_chn:I = 0x7f1001d7

.field public static final years:I = 0x7f1001d8

.field public static final yes:I = 0x7f1001d9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
