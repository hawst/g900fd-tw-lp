.class Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;
.super Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;
.source "MusicAppWidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MusicWidgetRemoteViewBuilder"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutId"    # I

    .prologue
    .line 295
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;-><init>(Landroid/content/Context;I)V

    .line 296
    return-void
.end method

.method private updateRepeatBtn(I)V
    .locals 4
    .param p1, "repeat"    # I

    .prologue
    const v3, 0x7f0d01c0

    .line 345
    packed-switch p1, :pswitch_data_0

    .line 367
    :goto_0
    return-void

    .line 347
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f02012b

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mContext:Landroid/content/Context;

    const v2, 0x7f10019a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 353
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f02012a

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mContext:Landroid/content/Context;

    const v2, 0x7f100199

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 359
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f02012c

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mContext:Landroid/content/Context;

    const v2, 0x7f10019b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 345
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateShuffleBtn(I)V
    .locals 4
    .param p1, "shuffle"    # I

    .prologue
    const v3, 0x7f0d01bf

    .line 370
    packed-switch p1, :pswitch_data_0

    .line 387
    :goto_0
    return-void

    .line 372
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f02012d

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mContext:Landroid/content/Context;

    const v2, 0x7f10019f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 379
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f02012e

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mContext:Landroid/content/Context;

    const v2, 0x7f1001a0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 370
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 1
    .param p1, "album"    # Landroid/graphics/Bitmap;

    .prologue
    .line 312
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->showProgress(Z)V

    .line 313
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;->setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v0

    return-object v0
.end method

.method public setButtons()Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 4

    .prologue
    .line 300
    invoke-super {p0}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;->setButtons()Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0d01bf

    iget-object v2, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mContext:Landroid/content/Context;

    const-string v3, "com.samsung.musicplus.musicservicecommand.shuffle"

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->getPlayerServiceIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0d01c0

    iget-object v2, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->mContext:Landroid/content/Context;

    const-string v3, "com.samsung.musicplus.musicservicecommand.repeat"

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->getPlayerServiceIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 307
    return-object p0
.end method

.method public setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 1
    .param p1, "listType"    # I

    .prologue
    .line 323
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;->setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v0

    return-object v0
.end method

.method public setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 1
    .param p1, "listType"    # I
    .param p2, "isPlaying"    # Z

    .prologue
    .line 318
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;->setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v0

    return-object v0
.end method

.method public setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;

    .prologue
    .line 328
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;->setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v0

    return-object v0
.end method

.method public updateExtraButtons(II)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 0
    .param p1, "shuffle"    # I
    .param p2, "repeat"    # I

    .prologue
    .line 339
    invoke-direct {p0, p1}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->updateShuffleBtn(I)V

    .line 340
    invoke-direct {p0, p2}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->updateRepeatBtn(I)V

    .line 341
    return-object p0
.end method
