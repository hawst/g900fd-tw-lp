.class Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;
.super Ljava/lang/Object;
.source "WidgetListService.java"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# static fields
.field private static final NOWPLAYING_LIST_NAME:Ljava/lang/String; = "now playing list 0123456789"

.field private static final TAG:Ljava/lang/String; = "MusicWidgetList"

.field private static final UNDEFINED:I = -0x1


# instance fields
.field private mAudioIdIdx:I

.field private final mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private mList:[J

.field private mText1Idx:I

.field private mText2Idx:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, -0x1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput v0, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mAudioIdIdx:I

    .line 67
    iput v0, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mText1Idx:I

    .line 69
    iput v0, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mText2Idx:I

    .line 76
    const-string v0, "MusicWidgetList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ListRemoteViewsFactory intent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iput-object p1, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mContext:Landroid/content/Context;

    .line 78
    return-void
.end method

.method private getAllTrackIds()[J
    .locals 9

    .prologue
    .line 285
    const v0, 0x20001

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v0

    iget-object v6, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 286
    .local v6, "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v2, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v3, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v4, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v5, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 288
    .local v7, "c":Landroid/database/Cursor;
    invoke-static {v7}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v8

    .line 289
    .local v8, "songList":[J
    iput-object v7, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    .line 290
    return-object v8
.end method

.method private getNowPlayingList(Landroid/content/Context;J)[J
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 269
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 270
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 271
    .local v1, "uri":Landroid/net/Uri;
    const/4 v5, 0x3

    new-array v2, v5, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v8

    const-string v5, "title"

    aput-object v5, v2, v9

    const/4 v5, 0x2

    const-string v7, "artist"

    aput-object v7, v2, v5

    .line 276
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "_id in (select audio_id from audio_playlists_map where playlist_id = ?)"

    .line 277
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v9, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 278
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 279
    .local v6, "songList":[J
    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    .line 280
    iget-object v5, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-static {v5}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v6

    .line 281
    return-object v6
.end method

.method private getNowPlayingListId(Landroid/content/Context;)J
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 248
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 249
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 251
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI_INCLUDING_NESTED_LIST:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "name= ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "now playing list 0123456789"

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 257
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 258
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 261
    if-eqz v6, :cond_0

    .line 262
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 265
    :cond_0
    :goto_0
    return-wide v2

    .line 261
    :cond_1
    if-eqz v6, :cond_2

    .line 262
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 265
    :cond_2
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 261
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    .line 262
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method private prepareDefaultCursorAndIndex(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 8
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    const/4 v7, 0x0

    .line 206
    const-string v4, "MusicWidgetList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " prepareDefaultCursorAndIndex service : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    if-nez p1, :cond_5

    .line 208
    const-wide/16 v2, -0x1

    .line 209
    .local v2, "nowPlayListId":J
    iget-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4}, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->getNowPlayingListId(Landroid/content/Context;)J

    move-result-wide v2

    .line 210
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_4

    .line 211
    iget-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v2, v3}, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->getNowPlayingList(Landroid/content/Context;J)[J

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mList:[J

    .line 223
    .end local v2    # "nowPlayListId":J
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mList:[J

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mList:[J

    array-length v4, v4

    if-nez v4, :cond_2

    .line 224
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_1

    .line 226
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 227
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->getAllTrackIds()[J

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mList:[J

    .line 236
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    if-eqz v4, :cond_3

    .line 237
    iget-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    const-string v5, "_id"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mAudioIdIdx:I

    .line 238
    iget-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    const-string v5, "title"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mText1Idx:I

    .line 239
    iget-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    const-string v5, "artist"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mText2Idx:I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 245
    :cond_3
    :goto_2
    return-void

    .line 213
    .restart local v2    # "nowPlayListId":J
    :cond_4
    iput-object v7, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mList:[J

    goto :goto_0

    .line 216
    .end local v2    # "nowPlayListId":J
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentBaseUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 217
    .local v1, "uri":Landroid/net/Uri;
    new-instance v4, Lcom/samsung/musicplus/player/NowPlayingCursor;

    iget-object v5, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getOrganizedQueue()[J

    move-result-object v6

    invoke-direct {v4, v5, v1, v6}, Lcom/samsung/musicplus/player/NowPlayingCursor;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;[J)V

    iput-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    .line 219
    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getQueue()[J

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mList:[J

    goto :goto_0

    .line 228
    .end local v1    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v4, "MusicWidgetList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " NullPointerException : mCursor is null"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 241
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 242
    .restart local v0    # "e":Ljava/lang/NullPointerException;
    const-string v4, "MusicWidgetList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " NullPointerException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public getCount()I
    .locals 4

    .prologue
    .line 101
    const/4 v0, 0x0

    .line 102
    .local v0, "count":I
    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 107
    :goto_0
    const-string v1, "MusicWidgetList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " getCount "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    return v0

    .line 105
    :cond_0
    const-string v1, "MusicWidgetList"

    const-string v2, "getCount() mCursor is null"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 182
    const-string v0, "MusicWidgetList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " getItemId position : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 3

    .prologue
    .line 170
    const-string v0, "MusicWidgetList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " getLoadingView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 11
    .param p1, "position"    # I

    .prologue
    .line 113
    const-string v8, "MusicWidgetList"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " getViewAt position : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const/4 v7, 0x0

    .line 117
    .local v7, "title":Ljava/lang/String;
    const/4 v0, 0x0

    .line 118
    .local v0, "artist":Ljava/lang/String;
    const-wide/16 v2, -0x1

    .line 120
    .local v2, "audioId":J
    iget-object v8, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v8, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 121
    iget v8, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mText1Idx:I

    if-ltz v8, :cond_0

    .line 122
    iget-object v8, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    iget v10, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mText1Idx:I

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 124
    :cond_0
    iget v8, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mText2Idx:I

    if-ltz v8, :cond_1

    .line 125
    iget-object v8, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    iget v10, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mText2Idx:I

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 127
    :cond_1
    iget v8, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mAudioIdIdx:I

    if-ltz v8, :cond_2

    .line 128
    iget-object v8, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    iget v9, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mAudioIdIdx:I

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 133
    :cond_2
    new-instance v5, Landroid/widget/RemoteViews;

    iget-object v8, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f040096

    invoke-direct {v5, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 134
    .local v5, "rv":Landroid/widget/RemoteViews;
    const v8, 0x7f0d01c5

    invoke-virtual {v5, v8, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 135
    const v8, 0x7f0d01c6

    invoke-virtual {v5, v8, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 137
    invoke-static {}, Lcom/samsung/musicplus/service/PlayerService;->getInstance()Lcom/samsung/musicplus/service/PlayerService;

    move-result-object v6

    .line 138
    .local v6, "s":Lcom/samsung/musicplus/service/PlayerService;
    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v8

    cmp-long v8, v2, v8

    if-nez v8, :cond_5

    .line 139
    invoke-virtual {v6}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 140
    const v8, 0x7f0d01c7

    const v9, 0x7f050015

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 146
    :goto_0
    const v8, 0x7f0d01c7

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 151
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 152
    const v8, 0x7f0d01c5

    const/4 v9, 0x0

    invoke-static {v5, v8, v9}, Lcom/samsung/musicplus/library/view/RemoteViewsCompat;->setViewFingerHovered(Landroid/widget/RemoteViews;IZ)V

    .line 153
    const v8, 0x7f0d01c6

    const/4 v9, 0x0

    invoke-static {v5, v8, v9}, Lcom/samsung/musicplus/library/view/RemoteViewsCompat;->setViewFingerHovered(Landroid/widget/RemoteViews;IZ)V

    .line 157
    :cond_3
    new-instance v4, Landroid/content/Intent;

    const-string v8, "com.samsung.musicplus.appwidget.CLICK_LIST"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 158
    .local v4, "i":Landroid/content/Intent;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 159
    .local v1, "extras":Landroid/os/Bundle;
    const-string v8, "list"

    iget-object v9, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mList:[J

    invoke-virtual {v1, v8, v9}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 160
    const-string v8, "_id"

    invoke-virtual {v1, v8, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 161
    const-string v8, "selcted_postion"

    invoke-virtual {v1, v8, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 162
    invoke-virtual {v4, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 163
    const v8, 0x7f0d01c4

    invoke-virtual {v5, v8, v4}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    .line 164
    return-object v5

    .line 143
    .end local v1    # "extras":Landroid/os/Bundle;
    .end local v4    # "i":Landroid/content/Intent;
    :cond_4
    const v8, 0x7f0d01c7

    const v9, 0x7f02005c

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    .line 148
    :cond_5
    const v8, 0x7f0d01c7

    const/16 v9, 0x8

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 3

    .prologue
    .line 176
    const-string v0, "MusicWidgetList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " getViewTypeCount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 3

    .prologue
    .line 188
    const-string v0, "MusicWidgetList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " hasStableIds"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 82
    const-string v0, "MusicWidgetList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onCreate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method public onDataSetChanged()V
    .locals 4

    .prologue
    .line 194
    const-string v1, "MusicWidgetList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onDataSetChanged"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 198
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    .line 201
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/service/PlayerService;->getInstance()Lcom/samsung/musicplus/service/PlayerService;

    move-result-object v0

    .line 202
    .local v0, "s":Lcom/samsung/musicplus/service/PlayerService;
    invoke-direct {p0, v0}, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->prepareDefaultCursorAndIndex(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 203
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 90
    const-string v0, "MusicWidgetList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onDestroy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 95
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/music/appwidget/ListRemoteViewsFactory;->mCursor:Landroid/database/Cursor;

    .line 97
    :cond_1
    return-void
.end method
