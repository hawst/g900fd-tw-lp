.class public final Lcom/sec/android/app/music/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/music/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_mode_layout:I = 0x7f0d003f

.field public static final action_mode_menu1:I = 0x7f0d01cc

.field public static final action_mode_menu2:I = 0x7f0d01d1

.field public static final action_mode_title:I = 0x7f0d0041

.field public static final action_mode_up:I = 0x7f0d0040

.field public static final add_to_favorites:I = 0x7f0d01d2

.field public static final add_to_nowplaying:I = 0x7f0d01f0

.field public static final add_to_personal_folder:I = 0x7f0d01d6

.field public static final add_to_playlist:I = 0x7f0d01cd

.field public static final add_to_playlist_view:I = 0x7f0d0095

.field public static final add_tracks:I = 0x7f0d01e9

.field public static final addplaylist_btn:I = 0x7f0d011a

.field public static final advanced_button:I = 0x7f0d013b

.field public static final advanced_layout:I = 0x7f0d013f

.field public static final alarm_tone:I = 0x7f0d01ab

.field public static final alarm_tone_icon:I = 0x7f0d01ad

.field public static final alarm_tone_radio:I = 0x7f0d01ac

.field public static final alarm_tone_text:I = 0x7f0d01ae

.field public static final album:I = 0x7f0d00d7

.field public static final album_art_button_stub:I = 0x7f0d00c2

.field public static final album_art_icon_stub:I = 0x7f0d00c1

.field public static final album_divider_bottom:I = 0x7f0d009d

.field public static final album_top:I = 0x7f0d012a

.field public static final album_view:I = 0x7f0d0054

.field public static final album_view_button:I = 0x7f0d00fd

.field public static final albumart:I = 0x7f0d00a5

.field public static final albumart2:I = 0x7f0d00b7

.field public static final albumart3:I = 0x7f0d00b8

.field public static final albumart4:I = 0x7f0d00b9

.field public static final albumart5:I = 0x7f0d00ba

.field public static final albumart6:I = 0x7f0d00b4

.field public static final albumart_bigpond_top10:I = 0x7f0d00cf

.field public static final albumart_button:I = 0x7f0d00b2

.field public static final albumart_layout_bigpond_top10:I = 0x7f0d00ce

.field public static final albumart_loading:I = 0x7f0d01ba

.field public static final albumart_progress:I = 0x7f0d01b3

.field public static final all_share_btn:I = 0x7f0d005e

.field public static final allshare_volume_down:I = 0x7f0d0131

.field public static final allshare_volume_mute:I = 0x7f0d0132

.field public static final allshare_volume_panel:I = 0x7f0d012f

.field public static final allshare_volume_up:I = 0x7f0d0130

.field public static final alwaysScroll:I = 0x7f0d0038

.field public static final ani_effect_container:I = 0x7f0d00cd

.field public static final animated_play_pause_btn:I = 0x7f0d011c

.field public static final arrow_button_left:I = 0x7f0d00e8

.field public static final arrow_button_right:I = 0x7f0d00ea

.field public static final artist:I = 0x7f0d00d5

.field public static final artist_thumb:I = 0x7f0d00b3

.field public static final artist_top:I = 0x7f0d0129

.field public static final auto_divider:I = 0x7f0d0197

.field public static final auto_fit:I = 0x7f0d003e

.field public static final basic_button:I = 0x7f0d013a

.field public static final basic_dbscale_layout:I = 0x7f0d0140

.field public static final basic_eq_control0:I = 0x7f0d0142

.field public static final basic_eq_control1:I = 0x7f0d0143

.field public static final basic_eq_control2:I = 0x7f0d0144

.field public static final basic_eq_control3:I = 0x7f0d0145

.field public static final basic_eq_control4:I = 0x7f0d0146

.field public static final basic_eq_control5:I = 0x7f0d0147

.field public static final basic_eq_control6:I = 0x7f0d0148

.field public static final basic_layout:I = 0x7f0d0149

.field public static final bit_depth:I = 0x7f0d00dd

.field public static final bold:I = 0x7f0d0025

.field public static final bottom:I = 0x7f0d0028

.field public static final buffering_progress:I = 0x7f0d00f3

.field public static final caller_ringtone:I = 0x7f0d01a7

.field public static final caller_ringtone_icon:I = 0x7f0d01a9

.field public static final caller_ringtone_radio:I = 0x7f0d01a8

.field public static final caller_ringtone_text:I = 0x7f0d01aa

.field public static final cancel:I = 0x7f0d0200

.field public static final category_divider:I = 0x7f0d0050

.field public static final category_title:I = 0x7f0d004e

.field public static final category_title_main:I = 0x7f0d004d

.field public static final center:I = 0x7f0d0029

.field public static final center_horizontal:I = 0x7f0d002a

.field public static final center_vertical:I = 0x7f0d002b

.field public static final change_axis:I = 0x7f0d01f5

.field public static final change_order:I = 0x7f0d01fb

.field public static final change_player:I = 0x7f0d01ec

.field public static final check:I = 0x7f0d0139

.field public static final checkbox:I = 0x7f0d004c

.field public static final checked:I = 0x7f0d01c8

.field public static final clip_horizontal:I = 0x7f0d002c

.field public static final clip_vertical:I = 0x7f0d002d

.field public static final close:I = 0x7f0d01b4

.field public static final columnWidth:I = 0x7f0d003a

.field public static final content_empty_view_stub:I = 0x7f0d0098

.field public static final context_group_1:I = 0x7f0d01d9

.field public static final context_group_2:I = 0x7f0d01da

.field public static final control_button_layout:I = 0x7f0d018d

.field public static final count:I = 0x7f0d0046

.field public static final create_icon:I = 0x7f0d0094

.field public static final create_playlist:I = 0x7f0d01f8

.field public static final custom_divider:I = 0x7f0d00cc

.field public static final dark:I = 0x7f0d0036

.field public static final delete_item:I = 0x7f0d01d0

.field public static final details:I = 0x7f0d01d8

.field public static final details_info:I = 0x7f0d0115

.field public static final details_type:I = 0x7f0d0114

.field public static final dialog_main_text:I = 0x7f0d004b

.field public static final disabled:I = 0x7f0d0039

.field public static final divider:I = 0x7f0d00e6

.field public static final dmr_description:I = 0x7f0d0053

.field public static final dmr_icon:I = 0x7f0d0051

.field public static final dmr_name:I = 0x7f0d0052

.field public static final done:I = 0x7f0d01ca

.field public static final download:I = 0x7f0d01f3

.field public static final dummy_thumbnail:I = 0x7f0d00b6

.field public static final dummy_thumbnail_layout:I = 0x7f0d00b5

.field public static final duration:I = 0x7f0d00cb

.field public static final duration_time_text:I = 0x7f0d01a1

.field public static final easymode_seek_bar:I = 0x7f0d0123

.field public static final edit:I = 0x7f0d01ea

.field public static final edit_item:I = 0x7f0d01ce

.field public static final empty_view:I = 0x7f0d01c2

.field public static final empty_view_stub:I = 0x7f0d008e

.field public static final end:I = 0x7f0d01e8

.field public static final eq_control0:I = 0x7f0d0153

.field public static final eq_control1:I = 0x7f0d0154

.field public static final eq_control2:I = 0x7f0d0155

.field public static final eq_control3:I = 0x7f0d0156

.field public static final eq_control4:I = 0x7f0d0157

.field public static final eq_control5:I = 0x7f0d0158

.field public static final eq_control6:I = 0x7f0d0159

.field public static final eq_control_bar:I = 0x7f0d01bd

.field public static final eq_control_bar_level_line:I = 0x7f0d0141

.field public static final eq_control_text:I = 0x7f0d01be

.field public static final eq_dbscale_layout:I = 0x7f0d0152

.field public static final eq_spinner:I = 0x7f0d014e

.field public static final ext_control0:I = 0x7f0d015b

.field public static final ext_control1:I = 0x7f0d015d

.field public static final ext_control2:I = 0x7f0d015f

.field public static final ext_control3:I = 0x7f0d0161

.field public static final ext_control4:I = 0x7f0d0163

.field public static final ext_level0:I = 0x7f0d015a

.field public static final ext_level1:I = 0x7f0d015c

.field public static final ext_level2:I = 0x7f0d015e

.field public static final ext_level3:I = 0x7f0d0160

.field public static final ext_level4:I = 0x7f0d0162

.field public static final extra_info_view:I = 0x7f0d00be

.field public static final fill:I = 0x7f0d002e

.field public static final fill_horizontal:I = 0x7f0d002f

.field public static final fill_vertical:I = 0x7f0d0030

.field public static final format:I = 0x7f0d00dc

.field public static final fullplay_star_btn:I = 0x7f0d0121

.field public static final genre:I = 0x7f0d00d9

.field public static final gl_album_view:I = 0x7f0d0055

.field public static final go_to_setting:I = 0x7f0d01e6

.field public static final grid:I = 0x7f0d00a7

.field public static final grid_stub:I = 0x7f0d008d

.field public static final header_button_now_playing_stub:I = 0x7f0d0134

.field public static final header_button_stub:I = 0x7f0d0086

.field public static final header_common_stub:I = 0x7f0d0084

.field public static final header_icon:I = 0x7f0d00b1

.field public static final header_select_all_stub:I = 0x7f0d0085

.field public static final header_square_track_stub:I = 0x7f0d009a

.field public static final header_text:I = 0x7f0d00b0

.field public static final header_title:I = 0x7f0d00e9

.field public static final help_touch_text:I = 0x7f0d018e

.field public static final horizontal:I = 0x7f0d0034

.field public static final horizontal_grid_view:I = 0x7f0d0125

.field public static final horizontal_tab_scroll:I = 0x7f0d0103

.field public static final hover_album_info:I = 0x7f0d006a

.field public static final hover_and_more:I = 0x7f0d007a

.field public static final hover_artist1:I = 0x7f0d0070

.field public static final hover_artist2:I = 0x7f0d0073

.field public static final hover_artist3:I = 0x7f0d0076

.field public static final hover_artist4:I = 0x7f0d0079

.field public static final hover_artist_info:I = 0x7f0d007d

.field public static final hover_artist_info_five_line:I = 0x7f0d0068

.field public static final hover_artist_info_two_line:I = 0x7f0d0069

.field public static final hover_container2:I = 0x7f0d0071

.field public static final hover_container3:I = 0x7f0d0074

.field public static final hover_container4:I = 0x7f0d0077

.field public static final hover_full_text_info:I = 0x7f0d006b

.field public static final hover_list_full_text_popup:I = 0x7f0d007c

.field public static final hover_music_popup_playlist_name:I = 0x7f0d006d

.field public static final hover_music_popup_playlist_number:I = 0x7f0d006e

.field public static final hover_music_popup_text:I = 0x7f0d006c

.field public static final hover_player_full_text_popup:I = 0x7f0d0066

.field public static final hover_title1:I = 0x7f0d006f

.field public static final hover_title2:I = 0x7f0d0072

.field public static final hover_title3:I = 0x7f0d0075

.field public static final hover_title4:I = 0x7f0d0078

.field public static final hover_title_info:I = 0x7f0d0067

.field public static final icon:I = 0x7f0d00bc

.field public static final interceptor:I = 0x7f0d0151

.field public static final italic:I = 0x7f0d0026

.field public static final item_album:I = 0x7f0d00e3

.field public static final item_rating:I = 0x7f0d00eb

.field public static final item_text1:I = 0x7f0d00e1

.field public static final item_text2:I = 0x7f0d00e2

.field public static final k2hd_tag:I = 0x7f0d005b

.field public static final k2hd_tag_text:I = 0x7f0d0101

.field public static final k2hd_view:I = 0x7f0d0188

.field public static final label:I = 0x7f0d01bc

.field public static final layout_custom:I = 0x7f0d00ff

.field public static final left:I = 0x7f0d0031

.field public static final light:I = 0x7f0d0037

.field public static final list:I = 0x7f0d008c

.field public static final listContainer:I = 0x7f0d008b

.field public static final list_animation:I = 0x7f0d00c4

.field public static final list_animation_stub:I = 0x7f0d00c0

.field public static final list_area:I = 0x7f0d0166

.field public static final list_btn:I = 0x7f0d0116

.field public static final list_category_layout:I = 0x7f0d007e

.field public static final list_category_text:I = 0x7f0d0081

.field public static final list_category_text2:I = 0x7f0d0080

.field public static final list_content:I = 0x7f0d0104

.field public static final list_cover_view_layout:I = 0x7f0d009c

.field public static final list_create_playlist_header:I = 0x7f0d0093

.field public static final list_item_albumart_text_two_line_parent:I = 0x7f0d00bd

.field public static final list_item_checkbox_stub:I = 0x7f0d00a6

.field public static final list_item_divider_bottom:I = 0x7f0d00bb

.field public static final list_item_title_image:I = 0x7f0d00c9

.field public static final list_item_title_image_stub:I = 0x7f0d00c7

.field public static final list_item_title_layout:I = 0x7f0d00c6

.field public static final list_item_title_text:I = 0x7f0d00c8

.field public static final list_main_header_edit_text:I = 0x7f0d00a2

.field public static final list_main_header_select_all:I = 0x7f0d00aa

.field public static final list_main_header_select_all_layout:I = 0x7f0d00a9

.field public static final list_main_split_view_divider:I = 0x7f0d0096

.field public static final list_now_playlist_header:I = 0x7f0d00a8

.field public static final list_personal_icon:I = 0x7f0d00bf

.field public static final list_subtitle:I = 0x7f0d00ec

.field public static final list_tab_header_view_layout:I = 0x7f0d008f

.field public static final list_tab_header_view_layout_row:I = 0x7f0d0091

.field public static final list_text_wrapper_layout:I = 0x7f0d01bb

.field public static final list_track_single_cover_view:I = 0x7f0d009e

.field public static final ll_playlist_list_layout:I = 0x7f0d00a3

.field public static final loading_text:I = 0x7f0d008a

.field public static final lv_playlist_list:I = 0x7f0d00a4

.field public static final lyric_panel_scroll_view_real:I = 0x7f0d005f

.field public static final media_info_album_image:I = 0x7f0d00d4

.field public static final media_info_body_layout:I = 0x7f0d00d3

.field public static final media_info_description:I = 0x7f0d00e4

.field public static final media_info_header_layout:I = 0x7f0d00e7

.field public static final media_info_header_stub:I = 0x7f0d00ed

.field public static final media_info_logo_layout:I = 0x7f0d00d2

.field public static final media_info_logo_stub:I = 0x7f0d00ef

.field public static final media_info_plain_image:I = 0x7f0d00f0

.field public static final media_info_plain_text:I = 0x7f0d00f1

.field public static final menu0:I = 0x7f0d01db

.field public static final menu1:I = 0x7f0d01dd

.field public static final menu2:I = 0x7f0d01e0

.field public static final menu3:I = 0x7f0d01e3

.field public static final menu4:I = 0x7f0d01e7

.field public static final menu6:I = 0x7f0d01f6

.field public static final menu7:I = 0x7f0d01f7

.field public static final menu_add_to_favourites:I = 0x7f0d0000

.field public static final menu_add_to_playlist_multiple_items:I = 0x7f0d0001

.field public static final menu_add_to_playlist_single_item:I = 0x7f0d0002

.field public static final menu_add_tracks:I = 0x7f0d0003

.field public static final menu_cancel:I = 0x7f0d0042

.field public static final menu_change_horizontal_axis:I = 0x7f0d0004

.field public static final menu_change_order:I = 0x7f0d0005

.field public static final menu_change_player:I = 0x7f0d0006

.field public static final menu_create_playlist:I = 0x7f0d0007

.field public static final menu_delete_group:I = 0x7f0d0008

.field public static final menu_delete_group_done:I = 0x7f0d0009

.field public static final menu_delete_track:I = 0x7f0d000a

.field public static final menu_delete_track_done:I = 0x7f0d000b

.field public static final menu_deselect_all:I = 0x7f0d01fe

.field public static final menu_details_full_player:I = 0x7f0d000c

.field public static final menu_details_list:I = 0x7f0d000d

.field public static final menu_done:I = 0x7f0d0043

.field public static final menu_grid_view:I = 0x7f0d000e

.field public static final menu_launch_delete_mode:I = 0x7f0d000f

.field public static final menu_launch_music_square:I = 0x7f0d0010

.field public static final menu_launch_remove_mode:I = 0x7f0d0011

.field public static final menu_launch_select_mode:I = 0x7f0d0012

.field public static final menu_launch_setting:I = 0x7f0d0013

.field public static final menu_list_view:I = 0x7f0d0014

.field public static final menu_listen_via_bt:I = 0x7f0d0015

.field public static final menu_listen_via_device:I = 0x7f0d0016

.field public static final menu_remove_from_favourites:I = 0x7f0d0017

.field public static final menu_remove_track:I = 0x7f0d0018

.field public static final menu_remove_track_done:I = 0x7f0d0019

.field public static final menu_reorder_cancel:I = 0x7f0d001a

.field public static final menu_reorder_done:I = 0x7f0d001b

.field public static final menu_save_as_playlist:I = 0x7f0d001c

.field public static final menu_search:I = 0x7f0d001d

.field public static final menu_select_all:I = 0x7f0d01fd

.field public static final menu_set_as_full_player:I = 0x7f0d001e

.field public static final menu_set_as_list:I = 0x7f0d001f

.field public static final menu_share_with_other_devices:I = 0x7f0d01ef

.field public static final menu_shuffle:I = 0x7f0d0020

.field public static final menu_view_as:I = 0x7f0d0021

.field public static final menu_view_as_playlist:I = 0x7f0d0022

.field public static final menu_volume:I = 0x7f0d0023

.field public static final menu_vzcloud:I = 0x7f0d0024

.field public static final mini_animated_play_pause_btn:I = 0x7f0d00fb

.field public static final mini_player_buttons:I = 0x7f0d00f6

.field public static final mini_player_root:I = 0x7f0d00f2

.field public static final mini_player_square_root:I = 0x7f0d00fe

.field public static final minus:I = 0x7f0d014b

.field public static final more_group:I = 0x7f0d01ee

.field public static final move_to_knox:I = 0x7f0d01d4

.field public static final multi_header_select_all_stub:I = 0x7f0d0136

.field public static final music_list:I = 0x7f0d0190

.field public static final music_loading_player_album:I = 0x7f0d0056

.field public static final music_personal_image:I = 0x7f0d0105

.field public static final music_player_album_view:I = 0x7f0d010c

.field public static final music_player_control_panel:I = 0x7f0d0113

.field public static final music_player_control_view:I = 0x7f0d010f

.field public static final music_player_fullplay_view:I = 0x7f0d0107

.field public static final music_player_k2hd_stub:I = 0x7f0d0059

.field public static final music_player_list:I = 0x7f0d0062

.field public static final music_player_lyric_stub:I = 0x7f0d010d

.field public static final music_player_main_view:I = 0x7f0d010b

.field public static final music_player_option_view:I = 0x7f0d0108

.field public static final music_player_playing_info_lyric_scroll_view:I = 0x7f0d005d

.field public static final music_player_playing_info_lyric_text:I = 0x7f0d0060

.field public static final music_player_progress_scrubbing_speed_text:I = 0x7f0d0064

.field public static final music_player_repeat_group:I = 0x7f0d011f

.field public static final music_player_right_pannel:I = 0x7f0d0112

.field public static final music_player_seekbar_common:I = 0x7f0d0122

.field public static final music_player_shuffle_group:I = 0x7f0d011d

.field public static final music_player_similar_music:I = 0x7f0d0126

.field public static final music_player_title_bottom_view:I = 0x7f0d0111

.field public static final music_player_title_top:I = 0x7f0d010e

.field public static final music_player_title_top_parent:I = 0x7f0d0127

.field public static final music_player_uhq_stub:I = 0x7f0d0057

.field public static final music_selector_list_view:I = 0x7f0d0135

.field public static final music_square:I = 0x7f0d01e5

.field public static final music_square_grid_view:I = 0x7f0d0168

.field public static final music_square_grid_view_layout:I = 0x7f0d016c

.field public static final music_square_grid_view_sweep:I = 0x7f0d016d

.field public static final music_square_landscape_view:I = 0x7f0d016b

.field public static final music_sub_list_view:I = 0x7f0d0165

.field public static final music_tab_contents:I = 0x7f0d0102

.field public static final music_widget_list:I = 0x7f0d01c1

.field public static final name:I = 0x7f0d0192

.field public static final new_basic_soundalive:I = 0x7f0d013c

.field public static final new_soundalive_advanced_effect_Button_3d:I = 0x7f0d016f

.field public static final new_soundalive_advanced_effect_Button_Bass:I = 0x7f0d0170

.field public static final new_soundalive_advanced_effect_Button_Clarity:I = 0x7f0d0171

.field public static final new_soundalive_effect_bottomline:I = 0x7f0d0173

.field public static final new_soundalive_effects_club:I = 0x7f0d017b

.field public static final new_soundalive_effects_concert_hall:I = 0x7f0d017d

.field public static final new_soundalive_effects_none:I = 0x7f0d0172

.field public static final new_soundalive_effects_studio:I = 0x7f0d0179

.field public static final new_soundalive_effects_tube_amp_effect:I = 0x7f0d0175

.field public static final new_soundalive_effects_virtual_71_ch:I = 0x7f0d0177

.field public static final new_soundalive_gridview_layout:I = 0x7f0d0180

.field public static final new_soundalive_imageView_club:I = 0x7f0d017c

.field public static final new_soundalive_imageView_concert_hall:I = 0x7f0d017e

.field public static final new_soundalive_imageView_none:I = 0x7f0d0174

.field public static final new_soundalive_imageView_studio:I = 0x7f0d017a

.field public static final new_soundalive_imageView_tube_amp_effect:I = 0x7f0d0176

.field public static final new_soundalive_imageView_virtual_71_ch:I = 0x7f0d0178

.field public static final new_soundalive_preset_auto_checkbox:I = 0x7f0d017f

.field public static final new_soundalive_preset_gridview:I = 0x7f0d0181

.field public static final new_soundalive_preset_left_text:I = 0x7f0d0182

.field public static final new_soundalive_preset_right_text:I = 0x7f0d0183

.field public static final new_soundalive_square_cell_image:I = 0x7f0d0184

.field public static final new_soundalive_square_cell_text:I = 0x7f0d0185

.field public static final next_btn:I = 0x7f0d00fc

.field public static final no_item_bg:I = 0x7f0d0100

.field public static final no_item_button:I = 0x7f0d004a

.field public static final no_item_image:I = 0x7f0d0048

.field public static final no_item_text:I = 0x7f0d0049

.field public static final no_item_text_square:I = 0x7f0d016a

.field public static final none:I = 0x7f0d003b

.field public static final normal:I = 0x7f0d0027

.field public static final normal_radio:I = 0x7f0d0195

.field public static final nowplaying_list_container:I = 0x7f0d0109

.field public static final nowplaying_list_stub:I = 0x7f0d010a

.field public static final nowplaying_thumbnail:I = 0x7f0d0118

.field public static final nowplaying_thumbnail_button:I = 0x7f0d0119

.field public static final nowplaying_thumbnail_view:I = 0x7f0d0117

.field public static final number_text:I = 0x7f0d0090

.field public static final number_text_row:I = 0x7f0d0092

.field public static final numbering:I = 0x7f0d00ca

.field public static final offset_time_text:I = 0x7f0d019e

.field public static final option_stop_music_play:I = 0x7f0d01f4

.field public static final pager:I = 0x7f0d00ee

.field public static final path:I = 0x7f0d00e0

.field public static final percent:I = 0x7f0d0047

.field public static final personal_mode_image:I = 0x7f0d0063

.field public static final personal_mode_stub:I = 0x7f0d0058

.field public static final phone_ringtone:I = 0x7f0d0199

.field public static final phone_ringtone_icon:I = 0x7f0d01a5

.field public static final phone_ringtone_radio:I = 0x7f0d01a4

.field public static final phone_ringtone_text:I = 0x7f0d01a6

.field public static final play_pause_btn:I = 0x7f0d00fa

.field public static final play_pause_btn_container:I = 0x7f0d011b

.field public static final play_via_group_play:I = 0x7f0d01f2

.field public static final played_time:I = 0x7f0d00f5

.field public static final player_parent_view:I = 0x7f0d0106

.field public static final playlist_option_menu1:I = 0x7f0d01f9

.field public static final playlist_option_menu2:I = 0x7f0d01fc

.field public static final playspeed:I = 0x7f0d014a

.field public static final plus:I = 0x7f0d014d

.field public static final prev_btn:I = 0x7f0d00f9

.field public static final processing:I = 0x7f0d0044

.field public static final progress:I = 0x7f0d00c3

.field public static final progressBar:I = 0x7f0d0045

.field public static final progressContainer:I = 0x7f0d0089

.field public static final progress_bar:I = 0x7f0d0133

.field public static final progress_bg:I = 0x7f0d019f

.field public static final progress_info:I = 0x7f0d019d

.field public static final quick_panel_album_art:I = 0x7f0d0187

.field public static final quick_panel_album_art_layout:I = 0x7f0d0186

.field public static final quick_panel_close:I = 0x7f0d018c

.field public static final quick_panel_close_layout:I = 0x7f0d018b

.field public static final quick_panel_nearby_image_album:I = 0x7f0d018a

.field public static final recoding_date:I = 0x7f0d00da

.field public static final recommend:I = 0x7f0d0198

.field public static final recommend_radio:I = 0x7f0d019a

.field public static final refresh_button:I = 0x7f0d0082

.field public static final refresh_button_progress:I = 0x7f0d0083

.field public static final refresh_progressbar:I = 0x7f0d004f

.field public static final remove_from_favorites:I = 0x7f0d01d3

.field public static final remove_from_knox:I = 0x7f0d01d5

.field public static final remove_from_personal_folder:I = 0x7f0d01d7

.field public static final remove_item:I = 0x7f0d01cf

.field public static final reorder_container:I = 0x7f0d016e

.field public static final reorder_icon:I = 0x7f0d0137

.field public static final repeat_btn:I = 0x7f0d00f8

.field public static final repeat_btn_text:I = 0x7f0d0120

.field public static final reset:I = 0x7f0d01ff

.field public static final resolver_grid:I = 0x7f0d0191

.field public static final right:I = 0x7f0d0032

.field public static final sampling_rate:I = 0x7f0d00de

.field public static final samsung_music:I = 0x7f0d01df

.field public static final save_as_playlist:I = 0x7f0d01fa

.field public static final search:I = 0x7f0d01cb

.field public static final search_main:I = 0x7f0d018f

.field public static final search_view:I = 0x7f0d0193

.field public static final seek_bar:I = 0x7f0d00f4

.field public static final seek_bar_container:I = 0x7f0d0110

.field public static final seekbar:I = 0x7f0d014c

.field public static final seekbubble:I = 0x7f0d013e

.field public static final seektime:I = 0x7f0d007b

.field public static final select_all_checkbox:I = 0x7f0d00ab

.field public static final select_all_text:I = 0x7f0d00ac

.field public static final select_item:I = 0x7f0d01dc

.field public static final select_language:I = 0x7f0d00e5

.field public static final set_as:I = 0x7f0d01a3

.field public static final set_as_activity:I = 0x7f0d0194

.field public static final set_as_divider:I = 0x7f0d01a2

.field public static final share_music_via:I = 0x7f0d01f1

.field public static final shuffle_btn:I = 0x7f0d00f7

.field public static final shuffle_btn_text:I = 0x7f0d011e

.field public static final shuffle_header_layout:I = 0x7f0d00ad

.field public static final shuffle_icon:I = 0x7f0d00af

.field public static final shuffle_text:I = 0x7f0d00ae

.field public static final sidecasttitle:I = 0x7f0d0124

.field public static final size:I = 0x7f0d00df

.field public static final slink_device_delete:I = 0x7f0d00c5

.field public static final sound_alive_btn:I = 0x7f0d012d

.field public static final spacingWidth:I = 0x7f0d003c

.field public static final spacingWidthUniform:I = 0x7f0d003d

.field public static final split_resizer:I = 0x7f0d0087

.field public static final square_container:I = 0x7f0d0164

.field public static final square_grid_layout:I = 0x7f0d0167

.field public static final square_help:I = 0x7f0d0169

.field public static final square_layout:I = 0x7f0d013d

.field public static final square_list:I = 0x7f0d009b

.field public static final square_list_container:I = 0x7f0d0099

.field public static final starting_time_text:I = 0x7f0d01a0

.field public static final subtitle_divider_top:I = 0x7f0d007f

.field public static final sview_cover_music_loading_player_albumart:I = 0x7f0d01b8

.field public static final sview_cover_music_player_albumart_temp:I = 0x7f0d01b7

.field public static final sview_cover_music_player_control_root:I = 0x7f0d01b9

.field public static final sview_cover_music_player_fullplay_view:I = 0x7f0d01b6

.field public static final sview_cover_music_player_popup_background:I = 0x7f0d01b0

.field public static final sview_cover_music_player_popup_layout:I = 0x7f0d01b1

.field public static final sview_cover_music_player_popup_layout_root:I = 0x7f0d01af

.field public static final sview_cover_music_title_layout:I = 0x7f0d01b2

.field public static final sview_cover_music_volume_alert:I = 0x7f0d01b5

.field public static final tab_effect:I = 0x7f0d0150

.field public static final tab_eq:I = 0x7f0d014f

.field public static final tabcontent_split_sub:I = 0x7f0d0097

.field public static final tabcontent_split_sub_container:I = 0x7f0d0088

.field public static final tag_text:I = 0x7f0d005c

.field public static final tags:I = 0x7f0d01eb

.field public static final text:I = 0x7f0d0138

.field public static final text1:I = 0x7f0d009f

.field public static final text1_bigpond_top10:I = 0x7f0d00d0

.field public static final text2:I = 0x7f0d00a0

.field public static final text2_bigpond_top10:I = 0x7f0d00d1

.field public static final text3:I = 0x7f0d00a1

.field public static final text_main_1:I = 0x7f0d0196

.field public static final text_main_2:I = 0x7f0d019b

.field public static final text_sub_2:I = 0x7f0d019c

.field public static final title:I = 0x7f0d00d6

.field public static final title_top:I = 0x7f0d0128

.field public static final top:I = 0x7f0d0033

.field public static final total:I = 0x7f0d0061

.field public static final track_length:I = 0x7f0d00d8

.field public static final track_number:I = 0x7f0d00db

.field public static final transfer_to_bt:I = 0x7f0d01e1

.field public static final transfer_to_phone:I = 0x7f0d01e2

.field public static final ts_online_store:I = 0x7f0d01de

.field public static final uhq_tag:I = 0x7f0d005a

.field public static final uhq_tag_text:I = 0x7f0d0065

.field public static final uhq_view:I = 0x7f0d0189

.field public static final unchecked:I = 0x7f0d01c9

.field public static final vertical:I = 0x7f0d0035

.field public static final view_as:I = 0x7f0d01e4

.field public static final volume:I = 0x7f0d01ed

.field public static final volume_panel:I = 0x7f0d012b

.field public static final volume_seekbar:I = 0x7f0d012e

.field public static final volume_text:I = 0x7f0d012c

.field public static final widget_control_repeat:I = 0x7f0d01c0

.field public static final widget_control_shuffle:I = 0x7f0d01bf

.field public static final widget_list_artist:I = 0x7f0d01c6

.field public static final widget_list_playing_anim:I = 0x7f0d01c7

.field public static final widget_list_title:I = 0x7f0d01c5

.field public static final widget_list_view:I = 0x7f0d01c4

.field public static final widget_shadow:I = 0x7f0d01c3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
