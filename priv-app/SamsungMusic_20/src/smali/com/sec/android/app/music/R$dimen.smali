.class public final Lcom/sec/android/app/music/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/music/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_popup_menu_text_size:I = 0x7f0c0000

.field public static final action_bar_tab_max_width:I = 0x7f0c0001

.field public static final action_bar_tab_text_max_width:I = 0x7f0c0002

.field public static final action_bar_title_text_size:I = 0x7f0c0003

.field public static final action_mode_dropdown_height:I = 0x7f0c0004

.field public static final action_mode_dropdown_margin_left:I = 0x7f0c0005

.field public static final action_mode_dropdown_menu_x_offset:I = 0x7f0c0006

.field public static final add_to_create_playlist_left_margin:I = 0x7f0c0007

.field public static final add_to_playlist_one_line_height:I = 0x7f0c0008

.field public static final add_to_playlist_text_left_margin:I = 0x7f0c0009

.field public static final add_to_playlist_text_right_margin:I = 0x7f0c000a

.field public static final big_list_two_line_height:I = 0x7f0c000b

.field public static final big_quick_panel_album_art_height:I = 0x7f0c000c

.field public static final big_quick_panel_album_art_width:I = 0x7f0c000d

.field public static final big_quick_panel_control_button_close_padding_right:I = 0x7f0c000e

.field public static final big_quick_panel_control_button_close_padding_top:I = 0x7f0c000f

.field public static final big_quick_panel_control_button_option_margin:I = 0x7f0c0010

.field public static final big_quick_panel_control_button_play_pause_margin_horizontal:I = 0x7f0c0011

.field public static final big_quick_panel_image_button_margin_left:I = 0x7f0c0012

.field public static final big_quick_panel_image_button_margin_top:I = 0x7f0c0013

.field public static final big_quick_panel_root_layout_height:I = 0x7f0c0014

.field public static final big_quick_panel_title_text_view_margin_left:I = 0x7f0c0015

.field public static final big_quick_panel_title_text_view_margin_right:I = 0x7f0c0016

.field public static final big_quick_panel_title_text_view_margin_top:I = 0x7f0c0017

.field public static final cover_view_album_art_size:I = 0x7f0c0018

.field public static final cover_view_text_top_margin:I = 0x7f0c0019

.field public static final custom_seekbubble_height:I = 0x7f0c001a

.field public static final custom_seekbubble_padding_horizontal:I = 0x7f0c001b

.field public static final device_list_albumart_size:I = 0x7f0c001c

.field public static final dialog_inner_padding_left_right:I = 0x7f0c001d

.field public static final dialog_inner_padding_top_bottm:I = 0x7f0c001e

.field public static final dialog_main_text_size:I = 0x7f0c001f

.field public static final dialog_text_margin_bottom:I = 0x7f0c0020

.field public static final dlna_chekbox_padding:I = 0x7f0c0021

.field public static final edit_text_min_height:I = 0x7f0c0022

.field public static final eq_bubble_text:I = 0x7f0c0023

.field public static final eq_control_bar_padding_bottom:I = 0x7f0c0024

.field public static final eq_control_bar_padding_top:I = 0x7f0c0025

.field public static final eq_control_text:I = 0x7f0c0026

.field public static final eq_dbscale_layout:I = 0x7f0c0027

.field public static final equalizer_control_height:I = 0x7f0c0028

.field public static final equalizer_control_margin_left:I = 0x7f0c0029

.field public static final equalizer_control_margin_right:I = 0x7f0c002a

.field public static final equalizer_seekbar_height:I = 0x7f0c002b

.field public static final equalizer_seekbar_padding_top:I = 0x7f0c002c

.field public static final extened_level_left_margin:I = 0x7f0c002d

.field public static final extened_level_right_margin:I = 0x7f0c002e

.field public static final finger_hover_track_list_margin_top_bottom:I = 0x7f0c002f

.field public static final finger_hover_track_list_max_width:I = 0x7f0c0030

.field public static final finger_hover_track_list_padding_vertical:I = 0x7f0c0031

.field public static final finger_hover_track_list_title_text_size:I = 0x7f0c0032

.field public static final full_player_lyric_min_height:I = 0x7f0c0033

.field public static final full_player_nowplaying_list_height:I = 0x7f0c0034

.field public static final full_player_personal_icon_margin_left:I = 0x7f0c0035

.field public static final full_player_personal_icon_margin_top:I = 0x7f0c0036

.field public static final full_player_progress_scrubbing_speed_text_padding_bottom:I = 0x7f0c0037

.field public static final full_player_progress_scrubbing_speed_text_padding_left:I = 0x7f0c0038

.field public static final full_player_progress_scrubbing_speed_text_padding_right:I = 0x7f0c0039

.field public static final full_player_progress_scrubbing_speed_text_padding_top:I = 0x7f0c003a

.field public static final full_player_thumbnail_size:I = 0x7f0c003b

.field public static final full_player_uhq_tag_padding_left:I = 0x7f0c003c

.field public static final full_player_uhq_tag_padding_top:I = 0x7f0c003d

.field public static final gird_item_line1_text_size:I = 0x7f0c003e

.field public static final gl_music_player_personal_icon_dx:I = 0x7f0c003f

.field public static final gl_music_player_personal_icon_dy:I = 0x7f0c0040

.field public static final gl_music_player_recommended_personal_icon_dx:I = 0x7f0c0041

.field public static final gl_music_player_recommended_personal_icon_dy:I = 0x7f0c0042

.field public static final grid_albumart_bottom_margin:I = 0x7f0c0043

.field public static final grid_height:I = 0x7f0c0044

.field public static final grid_innner_padding:I = 0x7f0c0045

.field public static final grid_item_line2_text_size:I = 0x7f0c0046

.field public static final grid_item_padding_side:I = 0x7f0c0047

.field public static final grid_item_vertical_spacing:I = 0x7f0c0048

.field public static final grid_list_albumart_size:I = 0x7f0c0049

.field public static final grid_width:I = 0x7f0c004a

.field public static final half_speed_scrubbing_height:I = 0x7f0c004b

.field public static final help_popup_picker_text_size:I = 0x7f0c004c

.field public static final help_popup_picker_text_top_margin:I = 0x7f0c004d

.field public static final high_speed_scrubbing_height:I = 0x7f0c004e

.field public static final hover_common_view_max_height:I = 0x7f0c004f

.field public static final hover_common_view_max_width:I = 0x7f0c0050

.field public static final hover_common_view_min_width:I = 0x7f0c0051

.field public static final hover_common_view_padding_bottom:I = 0x7f0c0052

.field public static final hover_common_view_padding_horizontal:I = 0x7f0c0053

.field public static final hover_common_view_padding_top:I = 0x7f0c0054

.field public static final hover_common_view_text_size:I = 0x7f0c0055

.field public static final hover_default_widget_padding_bottom:I = 0x7f0c0056

.field public static final hover_default_widget_padding_top:I = 0x7f0c0057

.field public static final hover_full_text_title:I = 0x7f0c0058

.field public static final hover_playlist_view_max_height:I = 0x7f0c0059

.field public static final hover_playlist_view_max_width:I = 0x7f0c005a

.field public static final hover_playlist_view_padding_bottom:I = 0x7f0c005b

.field public static final hover_playlist_view_padding_horizontal:I = 0x7f0c005c

.field public static final hover_playlist_view_padding_top:I = 0x7f0c005d

.field public static final hover_playlist_view_text_size:I = 0x7f0c005e

.field public static final hover_preview_max_height:I = 0x7f0c005f

.field public static final hover_preview_max_width:I = 0x7f0c0060

.field public static final hover_preview_padding_horizontal:I = 0x7f0c0061

.field public static final hover_preview_padding_vertical:I = 0x7f0c0062

.field public static final hover_preview_text_size:I = 0x7f0c0063

.field public static final hover_seekbar_max_height:I = 0x7f0c0064

.field public static final hover_seekbar_max_width:I = 0x7f0c0065

.field public static final hover_seekbar_text_size:I = 0x7f0c0066

.field public static final hover_track_list_artist_text_size:I = 0x7f0c0067

.field public static final hover_track_list_divider_height:I = 0x7f0c0068

.field public static final hover_track_list_margin_horizontal:I = 0x7f0c0069

.field public static final hover_track_list_title_text_size:I = 0x7f0c006a

.field public static final hover_track_list_view_max_width:I = 0x7f0c006b

.field public static final hover_two_line_view_margin_horizontal:I = 0x7f0c006c

.field public static final hover_two_line_view_margin_horizontal_bottom:I = 0x7f0c006d

.field public static final list_airtist_album_image_height:I = 0x7f0c006e

.field public static final list_albumart_margin_left:I = 0x7f0c006f

.field public static final list_albumart_margin_right:I = 0x7f0c0070

.field public static final list_artist_image_margin_right:I = 0x7f0c0071

.field public static final list_artist_padding_right:I = 0x7f0c0072

.field public static final list_artist_text1_margin_right:I = 0x7f0c0073

.field public static final list_category_text2_padding_more_playlist:I = 0x7f0c0074

.field public static final list_category_text2_padding_no_playlist:I = 0x7f0c0075

.field public static final list_content_header_margin:I = 0x7f0c0076

.field public static final list_cover_view_hieght:I = 0x7f0c0077

.field public static final list_cover_view_main_text_size:I = 0x7f0c0078

.field public static final list_cover_view_padding_bottom:I = 0x7f0c0079

.field public static final list_cover_view_padding_left:I = 0x7f0c007a

.field public static final list_cover_view_padding_right:I = 0x7f0c007b

.field public static final list_cover_view_padding_top:I = 0x7f0c007c

.field public static final list_cover_view_sub_text_size:I = 0x7f0c007d

.field public static final list_cover_view_width:I = 0x7f0c007e

.field public static final list_create_playlist_header_height:I = 0x7f0c01bd

.field public static final list_header_select_all_layout_height:I = 0x7f0c007f

.field public static final list_header_select_all_padding_side:I = 0x7f0c0080

.field public static final list_item_animation_right_margin:I = 0x7f0c0081

.field public static final list_item_animation_size:I = 0x7f0c0082

.field public static final list_item_check_box_left_margin:I = 0x7f0c0083

.field public static final list_item_check_box_right_adjust_margin:I = 0x7f0c0084

.field public static final list_item_check_box_right_margin:I = 0x7f0c0085

.field public static final list_item_duration_left_margin:I = 0x7f0c0086

.field public static final list_item_duration_text_size:I = 0x7f0c0087

.field public static final list_item_line1_text_size:I = 0x7f0c0088

.field public static final list_item_line2_text_size:I = 0x7f0c0089

.field public static final list_item_padding_side:I = 0x7f0c008a

.field public static final list_item_padding_side_devices:I = 0x7f0c008b

.field public static final list_item_text3_size:I = 0x7f0c008c

.field public static final list_main_header_edit_text_topbottom_margin:I = 0x7f0c008d

.field public static final list_main_split_view_divider_margin:I = 0x7f0c008e

.field public static final list_main_split_view_divider_width:I = 0x7f0c008f

.field public static final list_padding_right:I = 0x7f0c0090

.field public static final list_reorder_item_padding_side:I = 0x7f0c0091

.field public static final list_shuffle_item_height:I = 0x7f0c0092

.field public static final list_shuffle_text_size:I = 0x7f0c0093

.field public static final list_subtitle_height:I = 0x7f0c0094

.field public static final list_text1_margin_right:I = 0x7f0c0095

.field public static final maxWidth:I = 0x7f0c0096

.field public static final media_info_default_album_shadow_size:I = 0x7f0c0097

.field public static final media_info_default_album_size:I = 0x7f0c0098

.field public static final media_info_list_item_album_size:I = 0x7f0c0099

.field public static final media_info_list_item_small_album_size:I = 0x7f0c009a

.field public static final media_info_margin:I = 0x7f0c009b

.field public static final minWidth:I = 0x7f0c009c

.field public static final mini_player_height:I = 0x7f0c009d

.field public static final mini_player_icon_marginRight:I = 0x7f0c009e

.field public static final mini_player_info_album_marginRight:I = 0x7f0c009f

.field public static final mini_player_info_marginBottom:I = 0x7f0c00a0

.field public static final mini_player_info_marginLeft:I = 0x7f0c00a1

.field public static final mini_player_info_marginRight:I = 0x7f0c00a2

.field public static final mini_player_least_width:I = 0x7f0c00a3

.field public static final mini_player_min_width:I = 0x7f0c00a4

.field public static final mini_player_play_pause_btn_circle_radius:I = 0x7f0c00a5

.field public static final mini_player_play_pause_btn_edge_width:I = 0x7f0c00a6

.field public static final mini_player_play_pause_btn_margin:I = 0x7f0c00a7

.field public static final mini_player_play_pause_btn_size:I = 0x7f0c00a8

.field public static final mini_player_prev_next_btn_size:I = 0x7f0c00a9

.field public static final mini_player_shuffle_repeat_btn_size:I = 0x7f0c00aa

.field public static final mini_player_side_btn_margin:I = 0x7f0c00ab

.field public static final mini_player_square_height:I = 0x7f0c00ac

.field public static final mini_player_square_info_album_marginRight:I = 0x7f0c00ad

.field public static final mini_player_square_info_marginRight:I = 0x7f0c00ae

.field public static final mini_player_square_info_marginTop:I = 0x7f0c00af

.field public static final mini_player_square_play_pause_btn_circle_radius:I = 0x7f0c00b0

.field public static final mini_player_square_play_pause_btn_margin:I = 0x7f0c00b1

.field public static final mini_player_square_play_pause_btn_size:I = 0x7f0c00b2

.field public static final mini_player_square_prev_next_btn_size:I = 0x7f0c00b3

.field public static final mini_player_square_shuffle_repeat_btn_size:I = 0x7f0c00b4

.field public static final mini_player_square_side_btn_margin:I = 0x7f0c00b5

.field public static final mini_player_square_thumbnail_marginLeft:I = 0x7f0c00b6

.field public static final mini_player_square_thumbnail_size:I = 0x7f0c00b7

.field public static final mini_player_thumbnail_marginLeft:I = 0x7f0c00b8

.field public static final mini_player_thumbnail_size:I = 0x7f0c00b9

.field public static final music_mini_player_info_text:I = 0x7f0c00ba

.field public static final music_play_speed_text_size:I = 0x7f0c00bb

.field public static final music_player_album_height:I = 0x7f0c00bc

.field public static final music_player_album_height_with_shade:I = 0x7f0c00bd

.field public static final music_player_album_maring_bottom:I = 0x7f0c00be

.field public static final music_player_album_maring_top:I = 0x7f0c00bf

.field public static final music_player_album_maring_top_with_shade:I = 0x7f0c00c0

.field public static final music_player_album_scrolling_friction:I = 0x7f0c00c1

.field public static final music_player_album_spacing:I = 0x7f0c00c2

.field public static final music_player_album_spacing_additional:I = 0x7f0c00c3

.field public static final music_player_control_btn_size:I = 0x7f0c00c4

.field public static final music_player_control_button_margin_bottom:I = 0x7f0c01a2

.field public static final music_player_control_button_margin_top:I = 0x7f0c01a3

.field public static final music_player_control_container_height:I = 0x7f0c00c5

.field public static final music_player_control_margin:I = 0x7f0c00c6

.field public static final music_player_control_margin_bottom:I = 0x7f0c00c7

.field public static final music_player_dmr_volume_width:I = 0x7f0c00c8

.field public static final music_player_duration_text:I = 0x7f0c00c9

.field public static final music_player_easy_mode_progress_info_margin_top:I = 0x7f0c00ca

.field public static final music_player_easy_mode_repear_shuffle_text_width:I = 0x7f0c00cb

.field public static final music_player_easymode_duration_text:I = 0x7f0c00cc

.field public static final music_player_easymode_option_view_margin_top:I = 0x7f0c00cd

.field public static final music_player_easymode_option_view_padding_left:I = 0x7f0c00ce

.field public static final music_player_easymode_option_view_padding_right:I = 0x7f0c00cf

.field public static final music_player_easymode_option_view_padding_top:I = 0x7f0c00d0

.field public static final music_player_easymode_progress_info_bottom_margin:I = 0x7f0c00d1

.field public static final music_player_easymode_progress_info_height:I = 0x7f0c00d2

.field public static final music_player_easymode_progress_info_top_margin:I = 0x7f0c00d3

.field public static final music_player_easymode_progress_info_width:I = 0x7f0c00d4

.field public static final music_player_easymode_title_main:I = 0x7f0c00d5

.field public static final music_player_easymode_title_sub:I = 0x7f0c00d6

.field public static final music_player_fullplay_view_width:I = 0x7f0c00d7

.field public static final music_player_lyric_padding:I = 0x7f0c00d8

.field public static final music_player_lyrics_edge_margin:I = 0x7f0c00d9

.field public static final music_player_no_side_cast_album_view_size:I = 0x7f0c00da

.field public static final music_player_not_selected_album_height:I = 0x7f0c00db

.field public static final music_player_nowplaying_list_header_height:I = 0x7f0c00dc

.field public static final music_player_nowplaying_list_item_height:I = 0x7f0c00dd

.field public static final music_player_nowplaying_list_item_left_padding:I = 0x7f0c00de

.field public static final music_player_nowplaying_list_item_right_padding:I = 0x7f0c00df

.field public static final music_player_option_button_margin:I = 0x7f0c00e0

.field public static final music_player_option_shuffle_repeat_btn_width:I = 0x7f0c00e1

.field public static final music_player_option_view_height:I = 0x7f0c00e2

.field public static final music_player_option_view_margin_bottom:I = 0x7f0c00e3

.field public static final music_player_option_view_margin_top:I = 0x7f0c00e4

.field public static final music_player_play_pause_btn_circle_radius:I = 0x7f0c00e5

.field public static final music_player_play_pause_btn_edge_width:I = 0x7f0c00e6

.field public static final music_player_play_pause_btn_margin:I = 0x7f0c00e7

.field public static final music_player_played_time_text:I = 0x7f0c00e8

.field public static final music_player_playing_info_lyric_text:I = 0x7f0c00e9

.field public static final music_player_progress_info_edge_margin:I = 0x7f0c00ea

.field public static final music_player_progress_info_margin_bottom:I = 0x7f0c00eb

.field public static final music_player_progress_info_margin_top:I = 0x7f0c00ec

.field public static final music_player_progress_info_padding_top_easymode:I = 0x7f0c00ed

.field public static final music_player_repeat_group_margin_right:I = 0x7f0c00ee

.field public static final music_player_seekbar_margin:I = 0x7f0c00ef

.field public static final music_player_seekbar_margin_bottom:I = 0x7f0c00f0

.field public static final music_player_seekbar_margin_top:I = 0x7f0c00f1

.field public static final music_player_seekbar_padding_bottom:I = 0x7f0c00f2

.field public static final music_player_seekbar_panel_land_height:I = 0x7f0c01a4

.field public static final music_player_seekbar_thumb_offset:I = 0x7f0c00f3

.field public static final music_player_shuffle_group_margin_left:I = 0x7f0c00f4

.field public static final music_player_side_cast_album_size:I = 0x7f0c00f5

.field public static final music_player_side_cast_album_view_size:I = 0x7f0c00f6

.field public static final music_player_side_cast_bottom_margin:I = 0x7f0c00f7

.field public static final music_player_side_cast_grid_view_height:I = 0x7f0c00f8

.field public static final music_player_side_cast_main_title_bottom_margin:I = 0x7f0c00f9

.field public static final music_player_side_cast_main_title_top_margin:I = 0x7f0c00fa

.field public static final music_player_side_cast_title_text_size:I = 0x7f0c00fb

.field public static final music_player_time_width:I = 0x7f0c00fc

.field public static final music_player_title_artist_margin_bottom:I = 0x7f0c00fd

.field public static final music_player_title_artist_margin_top:I = 0x7f0c00fe

.field public static final music_player_title_edge_margin:I = 0x7f0c00ff

.field public static final music_player_title_main:I = 0x7f0c0100

.field public static final music_player_title_main_top:I = 0x7f0c0101

.field public static final music_player_title_margin:I = 0x7f0c0102

.field public static final music_player_title_margin_top:I = 0x7f0c0103

.field public static final music_player_title_sub:I = 0x7f0c0104

.field public static final music_player_title_sub_top:I = 0x7f0c0105

.field public static final music_player_title_view:I = 0x7f0c0106

.field public static final music_player_top_title_view_width:I = 0x7f0c0107

.field public static final music_player_view_lyric_view_padding_top:I = 0x7f0c0108

.field public static final music_player_volume_height:I = 0x7f0c0109

.field public static final music_player_volume_margin_top:I = 0x7f0c010a

.field public static final music_player_volume_panel_margin_right:I = 0x7f0c010b

.field public static final music_player_volume_panel_margin_top:I = 0x7f0c010c

.field public static final music_player_volume_sa_margin_bottom:I = 0x7f0c010d

.field public static final music_player_volume_seekbar_height:I = 0x7f0c010e

.field public static final music_player_volume_seekbar_margin_top:I = 0x7f0c010f

.field public static final music_player_volume_seekbar_width:I = 0x7f0c0110

.field public static final music_player_volume_text:I = 0x7f0c0111

.field public static final music_player_volume_text_height:I = 0x7f0c0112

.field public static final music_player_volume_width:I = 0x7f0c0113

.field public static final music_square_expandable_button_height:I = 0x7f0c0114

.field public static final music_square_expandable_text:I = 0x7f0c0115

.field public static final music_square_grid_view_column_padding:I = 0x7f0c0116

.field public static final music_square_grid_view_margin:I = 0x7f0c0117

.field public static final music_square_grid_view_margin_bottom_sweep:I = 0x7f0c0118

.field public static final music_square_grid_view_margin_sweep:I = 0x7f0c0119

.field public static final music_square_grid_view_margin_top_sweep:I = 0x7f0c01a5

.field public static final music_square_grid_view_padding:I = 0x7f0c011a

.field public static final music_square_grid_view_padding_left_sweep:I = 0x7f0c01a6

.field public static final music_square_grid_view_padding_right_sweep:I = 0x7f0c01a7

.field public static final music_square_grid_view_size:I = 0x7f0c011b

.field public static final music_square_grid_view_size_sweep:I = 0x7f0c01a8

.field public static final music_square_header_text_width:I = 0x7f0c011c

.field public static final music_square_help_bottom_margin:I = 0x7f0c011d

.field public static final music_square_help_top_margin:I = 0x7f0c011e

.field public static final music_square_help_view_margin_end:I = 0x7f0c01a9

.field public static final music_square_help_view_margin_top:I = 0x7f0c01aa

.field public static final music_square_landscape_grid_view_padding:I = 0x7f0c01ab

.field public static final music_square_landscape_grid_view_padding_left:I = 0x7f0c01ac

.field public static final music_square_landscape_grid_view_padding_right:I = 0x7f0c01ad

.field public static final music_square_landscape_view_height_size:I = 0x7f0c01ae

.field public static final music_square_landscape_view_height_size_sweep:I = 0x7f0c01af

.field public static final music_square_landscape_view_size:I = 0x7f0c01b0

.field public static final music_square_landscape_view_size_sweep:I = 0x7f0c011f

.field public static final music_square_left_text_padding_bottom:I = 0x7f0c0120

.field public static final music_square_no_item_side_padding:I = 0x7f0c0121

.field public static final music_square_no_item_text:I = 0x7f0c0122

.field public static final music_square_no_item_text_sweep:I = 0x7f0c0123

.field public static final music_square_no_item_textview_margin_top:I = 0x7f0c0124

.field public static final music_square_no_item_textview_margin_top_sweep:I = 0x7f0c0125

.field public static final music_square_no_item_textview_size:I = 0x7f0c0126

.field public static final music_square_no_item_textview_size_sweep:I = 0x7f0c0127

.field public static final music_square_spacing_from_square:I = 0x7f0c0128

.field public static final new_soundAlive_effect_text2_size:I = 0x7f0c0129

.field public static final new_soundalive_advanced_effect_margin_bottom:I = 0x7f0c012a

.field public static final new_soundalive_advanced_effect_width:I = 0x7f0c01b1

.field public static final new_soundalive_basic_advanced_height:I = 0x7f0c01b2

.field public static final new_soundalive_basic_advanced_width:I = 0x7f0c01b3

.field public static final new_soundalive_basic_layout_height:I = 0x7f0c01b4

.field public static final new_soundalive_basic_layout_width:I = 0x7f0c01b5

.field public static final new_soundalive_cell_size:I = 0x7f0c012b

.field public static final new_soundalive_effect_layout_height:I = 0x7f0c012c

.field public static final new_soundalive_effect_subtext_paddingLeft:I = 0x7f0c012d

.field public static final new_soundalive_effect_subtext_width:I = 0x7f0c012e

.field public static final new_soundalive_effects_layout_width:I = 0x7f0c01b6

.field public static final new_soundalive_eq_control_bar_width:I = 0x7f0c01b7

.field public static final new_soundalive_preset_auto_checkbox_margin_left:I = 0x7f0c012f

.field public static final new_soundalive_preset_auto_checkbox_margin_top:I = 0x7f0c0130

.field public static final new_soundalive_preset_auto_checkbox_max_width:I = 0x7f0c01b8

.field public static final new_soundalive_preset_gridview_layout_width:I = 0x7f0c0131

.field public static final new_soundalive_preset_gridview_margin_left:I = 0x7f0c01b9

.field public static final new_soundalive_preset_gridview_margin_right:I = 0x7f0c01ba

.field public static final new_soundalive_preset_gridview_margin_top:I = 0x7f0c01bb

.field public static final new_soundalive_preset_gridview_width:I = 0x7f0c0132

.field public static final new_soundalive_preset_left_text_margin_left:I = 0x7f0c0133

.field public static final new_soundalive_preset_right_text_margin_right:I = 0x7f0c0134

.field public static final new_soundalive_preset_text_margin_bottom:I = 0x7f0c0135

.field public static final new_soundalive_preset_text_margin_top:I = 0x7f0c0136

.field public static final new_soundalive_square_layout_width:I = 0x7f0c01bc

.field public static final new_soundalive_squarecell_text1_size:I = 0x7f0c0137

.field public static final normal_list_albumart_size:I = 0x7f0c0138

.field public static final normal_list_one_line_height:I = 0x7f0c0139

.field public static final normal_list_two_line_height:I = 0x7f0c013a

.field public static final nowplaying_list_item_artist_size:I = 0x7f0c013b

.field public static final nowplaying_list_item_title_size:I = 0x7f0c013c

.field public static final nowplaying_player_duration_text:I = 0x7f0c013d

.field public static final nowplaying_player_played_time_text:I = 0x7f0c013e

.field public static final padding_Bottom_popup_picker:I = 0x7f0c013f

.field public static final padding_Left_popup_picker:I = 0x7f0c0140

.field public static final padding_Right_popup_picker:I = 0x7f0c0141

.field public static final padding_top_popup_picker:I = 0x7f0c0142

.field public static final palm_motion_text_margin_left:I = 0x7f0c0143

.field public static final palm_motion_text_margin_right:I = 0x7f0c0144

.field public static final palm_motion_text_width:I = 0x7f0c0145

.field public static final palm_tuto_done_text_size:I = 0x7f0c0146

.field public static final playlist_item_padding_right:I = 0x7f0c0147

.field public static final quick_panel_album_art_height:I = 0x7f0c0148

.field public static final quick_panel_album_art_margin_left:I = 0x7f0c0149

.field public static final quick_panel_album_art_margin_top:I = 0x7f0c014a

.field public static final quick_panel_album_art_width:I = 0x7f0c014b

.field public static final quick_panel_artist_text_view_text_size:I = 0x7f0c014c

.field public static final quick_panel_control_area_margin_left:I = 0x7f0c014d

.field public static final quick_panel_control_area_padding_left:I = 0x7f0c014e

.field public static final quick_panel_control_area_padding_right:I = 0x7f0c014f

.field public static final quick_panel_control_button_close_margin_left:I = 0x7f0c0150

.field public static final quick_panel_control_button_play_pause_margin_horizontal:I = 0x7f0c0151

.field public static final quick_panel_root_layout_height:I = 0x7f0c0152

.field public static final quick_panel_title_text_view_text_size:I = 0x7f0c0153

.field public static final recommended_text_title_top_padding:I = 0x7f0c0154

.field public static final refresh_icon_padding:I = 0x7f0c0155

.field public static final refresh_icon_size:I = 0x7f0c0156

.field public static final reorder_list_icon_padding_start:I = 0x7f0c0157

.field public static final seektime_margin_bottom:I = 0x7f0c0158

.field public static final select_all_text_size:I = 0x7f0c0159

.field public static final set_as_activity_icon_size:I = 0x7f0c015a

.field public static final set_as_activity_list_height:I = 0x7f0c015b

.field public static final set_as_activity_section_height:I = 0x7f0c015c

.field public static final set_as_activity_text_size:I = 0x7f0c015d

.field public static final set_as_icon_left_space:I = 0x7f0c015e

.field public static final set_as_left_space:I = 0x7f0c015f

.field public static final set_as_right_space:I = 0x7f0c0160

.field public static final square_layout_height:I = 0x7f0c0161

.field public static final square_layout_size_sweep:I = 0x7f0c0162

.field public static final square_layout_size_sweep_height:I = 0x7f0c0163

.field public static final square_layout_width:I = 0x7f0c0164

.field public static final square_mood_text:I = 0x7f0c0165

.field public static final square_mood_text_sweep:I = 0x7f0c0166

.field public static final sview_cover_hover_album_view_max_width:I = 0x7f0c0167

.field public static final sview_cover_hover_view_max_width:I = 0x7f0c0168

.field public static final sview_cover_music_dialog_header_album_art_margin_left:I = 0x7f0c0169

.field public static final sview_cover_music_dialog_header_album_art_size:I = 0x7f0c016a

.field public static final sview_cover_music_dialog_header_close_button_margin_right:I = 0x7f0c016b

.field public static final sview_cover_music_dialog_header_height:I = 0x7f0c016c

.field public static final sview_cover_music_dialog_header_text_main:I = 0x7f0c016d

.field public static final sview_cover_music_dialog_header_text_margin_horizontal:I = 0x7f0c016e

.field public static final sview_cover_music_dialog_header_text_margin_horizontal_right:I = 0x7f0c016f

.field public static final sview_cover_music_dialog_header_text_sub:I = 0x7f0c0170

.field public static final sview_cover_music_dialog_list_item_layout_height:I = 0x7f0c0171

.field public static final sview_cover_music_dialog_list_item_text_main:I = 0x7f0c0172

.field public static final sview_cover_music_dialog_list_item_text_sub:I = 0x7f0c0173

.field public static final sview_cover_music_dialog_list_padding_left:I = 0x7f0c0174

.field public static final sview_cover_music_dialog_list_padding_right:I = 0x7f0c0175

.field public static final sview_cover_music_dialog_list_view_margin_left:I = 0x7f0c0176

.field public static final sview_cover_music_dialog_list_view_margin_right:I = 0x7f0c0177

.field public static final sview_cover_music_dialog_notification_height:I = 0x7f0c0178

.field public static final sview_cover_music_player_albumart:I = 0x7f0c0179

.field public static final sview_cover_music_player_albumart_margin_top:I = 0x7f0c017a

.field public static final sview_cover_music_player_albumart_padding:I = 0x7f0c017b

.field public static final sview_cover_music_player_albumart_size:I = 0x7f0c017c

.field public static final sview_cover_music_player_button_margin_left:I = 0x7f0c017d

.field public static final sview_cover_music_player_control_button_margin_inside:I = 0x7f0c017e

.field public static final sview_cover_music_player_control_button_margin_top:I = 0x7f0c017f

.field public static final sview_cover_music_player_dialog_height:I = 0x7f0c0180

.field public static final sview_cover_music_player_dialog_width:I = 0x7f0c0181

.field public static final sview_cover_music_player_list_height:I = 0x7f0c0182

.field public static final sview_cover_music_player_margin:I = 0x7f0c0183

.field public static final sview_cover_music_player_padding_left:I = 0x7f0c0184

.field public static final sview_cover_music_player_padding_top:I = 0x7f0c0185

.field public static final sview_cover_music_player_text_margin_inside:I = 0x7f0c0186

.field public static final sview_cover_music_player_text_margin_left:I = 0x7f0c0187

.field public static final sview_cover_music_player_text_margin_right:I = 0x7f0c0188

.field public static final sview_cover_music_player_text_margin_top:I = 0x7f0c0189

.field public static final sview_cover_music_player_title:I = 0x7f0c018a

.field public static final sview_cover_music_player_title_main:I = 0x7f0c018b

.field public static final sview_cover_music_player_title_margin:I = 0x7f0c018c

.field public static final sview_cover_music_player_title_margin_bottom:I = 0x7f0c018d

.field public static final sview_cover_music_player_title_margin_top:I = 0x7f0c018e

.field public static final sview_cover_music_player_title_sub:I = 0x7f0c018f

.field public static final sview_cover_music_player_title_width:I = 0x7f0c0190

.field public static final tab_height:I = 0x7f0c0191

.field public static final tab_text_size:I = 0x7f0c0192

.field public static final tab_text_size_soundalive:I = 0x7f0c0193

.field public static final widget_album_art_size:I = 0x7f0c0194

.field public static final widget_control_button_inside_margin:I = 0x7f0c0195

.field public static final widget_control_button_outside_margin:I = 0x7f0c0196

.field public static final widget_control_button_repeat_right_margin:I = 0x7f0c0197

.field public static final widget_control_button_shuffle_left_margin:I = 0x7f0c0198

.field public static final widget_controller_artist:I = 0x7f0c0199

.field public static final widget_controller_title:I = 0x7f0c019a

.field public static final widget_list_item_artist:I = 0x7f0c019b

.field public static final widget_list_item_height:I = 0x7f0c019c

.field public static final widget_list_item_title:I = 0x7f0c019d

.field public static final widget_size_height:I = 0x7f0c019e

.field public static final widget_size_width:I = 0x7f0c019f

.field public static final winset_list_item_line1_text:I = 0x7f0c01a0

.field public static final winset_list_item_line2_text:I = 0x7f0c01a1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
