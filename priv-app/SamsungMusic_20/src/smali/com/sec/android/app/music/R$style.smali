.class public final Lcom/sec/android/app/music/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/music/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AnimatedPlayPauseButton:I = 0x7f110000

.field public static final CustomDlg:I = 0x7f110001

.field public static final CustomDlg_AnimationStyle:I = 0x7f110002

.field public static final DefaultAlbumArt:I = 0x7f110003

.field public static final DefaultAlbumArt_Large:I = 0x7f110004

.field public static final DefaultAlbumArt_Medium:I = 0x7f110005

.field public static final DefaultAlbumArt_Small:I = 0x7f110006

.field public static final Divider:I = 0x7f110007

.field public static final Divider_AddToPlaylist:I = 0x7f110008

.field public static final Divider_SubTitle:I = 0x7f110009

.field public static final DummyStyle:I = 0x7f11000a

.field public static final FingerHoverFullText:I = 0x7f11000b

.field public static final FingerHoverFullText_Dark:I = 0x7f11000c

.field public static final FingerHoverFullText_Light:I = 0x7f11000d

.field public static final FingerHoverListDivider:I = 0x7f11000e

.field public static final FingerHoverListDivider_Dark:I = 0x7f11000f

.field public static final FingerHoverListDivider_Light:I = 0x7f110010

.field public static final FullScreen:I = 0x7f110011

.field public static final FullScreen_CancelDone:I = 0x7f110012

.field public static final FullScreen_Main:I = 0x7f110013

.field public static final FullScreen_Selection:I = 0x7f110014

.field public static final FullScreen_Transparent:I = 0x7f110015

.field public static final GridAlbumArt:I = 0x7f110016

.field public static final K2hdTagInfo:I = 0x7f110017

.field public static final ListAlbumArt:I = 0x7f110018

.field public static final ListAlbumArt_Artist:I = 0x7f110019

.field public static final ListAlbumArt_Device:I = 0x7f11001a

.field public static final ListSubTitle:I = 0x7f11001b

.field public static final ListSubTitleCover:I = 0x7f11001e

.field public static final ListSubTitleLayout:I = 0x7f11001f

.field public static final ListSubTitle_Left:I = 0x7f11001c

.field public static final ListSubTitle_Right:I = 0x7f11001d

.field public static final MusicActionBarStyle:I = 0x7f110020

.field public static final MusicActionBarStyle_CancelDone:I = 0x7f110021

.field public static final MusicActionBarStyle_Main:I = 0x7f110022

.field public static final MusicActionBarStyle_Selection:I = 0x7f110023

.field public static final MusicActionBarTabStyle:I = 0x7f110024

.field public static final MusicActionBarTabTextStyle:I = 0x7f110025

.field public static final MusicActionDropDownStyle:I = 0x7f110026

.field public static final MusicActionModeCloseButtonStyle:I = 0x7f110027

.field public static final MusicDefault_Dark_FullScreen:I = 0x7f110028

.field public static final MusicDefault_FullScreen:I = 0x7f110029

.field public static final MusicDefault_Light_FullScreen:I = 0x7f11002a

.field public static final MusicStoreActionBarStyle:I = 0x7f11002b

.field public static final MusicStoreActionModeCloseButtonStyle:I = 0x7f11002c

.field public static final NoDisplay:I = 0x7f11002d

.field public static final NoItemButton:I = 0x7f11002e

.field public static final Numbering:I = 0x7f11002f

.field public static final PerSonalTagInfoList:I = 0x7f110030

.field public static final PerSonalTagInfoThumb:I = 0x7f110031

.field public static final PlayerActionBarStyle:I = 0x7f110032

.field public static final PlayerActivity:I = 0x7f110033

.field public static final QuickPanelAlbumart:I = 0x7f110034

.field public static final SettingsPrefTitle:I = 0x7f110035

.field public static final SoundAliveAdvancedButton:I = 0x7f110036

.field public static final SoundAliveEffectBottomLine:I = 0x7f110037

.field public static final SoundAliveEffectExplanation:I = 0x7f110038

.field public static final SoundAliveEffectImage:I = 0x7f110039

.field public static final SoundAliveEffectLayout:I = 0x7f11003a

.field public static final SoundAliveEffectTitle:I = 0x7f11003b

.field public static final SoundAliveTab:I = 0x7f11003c

.field public static final Text:I = 0x7f11003d

.field public static final Text_GridItem_Line1:I = 0x7f11003e

.field public static final Text_GridItem_Line2:I = 0x7f11003f

.field public static final Text_ListItem:I = 0x7f110040

.field public static final Text_ListItem_Line1:I = 0x7f110041

.field public static final Text_ListItem_Line2:I = 0x7f110042

.field public static final Text_NoItem:I = 0x7f110043

.field public static final Translucent:I = 0x7f110044

.field public static final UhqTagInfo:I = 0x7f110045

.field public static final allshare_volume_button:I = 0x7f110046

.field public static final allshare_volume_divider:I = 0x7f110047

.field public static final duration_text_one_line:I = 0x7f110048

.field public static final mini_player_progressbar:I = 0x7f110049

.field public static final music_easymode_progressbar:I = 0x7f11004a

.field public static final music_player_progressbar:I = 0x7f11004b

.field public static final square_mood_text:I = 0x7f11004c

.field public static final square_mood_text_sweep:I = 0x7f11004d

.field public static final volume_panel:I = 0x7f11004e

.field public static final volume_seekbar:I = 0x7f11004f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
