.class public final Lcom/sec/android/app/music/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/music/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final action_bar_bg_color:I = 0x7f0b0000

.field public static final action_bar_pressed_text_color:I = 0x7f0b0001

.field public static final action_bar_text_color:I = 0x7f0b0002

.field public static final action_dropdown_disable_text_color:I = 0x7f0b0003

.field public static final action_dropdown_enable_text_color:I = 0x7f0b0004

.field public static final actionbar_cancel_done:I = 0x7f0b0005

.field public static final actionbar_dropdown_text_color_selector:I = 0x7f0b00a7

.field public static final actionbar_text_color_selector:I = 0x7f0b00a8

.field public static final add_to_play_list_item_line1:I = 0x7f0b0006

.field public static final add_to_playlist_count:I = 0x7f0b0007

.field public static final add_to_playlist_header:I = 0x7f0b0008

.field public static final animated_play_pause_button_edge:I = 0x7f0b0009

.field public static final black_opaque:I = 0x7f0b000a

.field public static final checked_bitmap_color_list:I = 0x7f0b00a9

.field public static final colorless:I = 0x7f0b000b

.field public static final default_text:I = 0x7f0b000c

.field public static final dialog_main_text:I = 0x7f0b000d

.field public static final dialog_main_text_black_theme:I = 0x7f0b000e

.field public static final dialog_sub_text:I = 0x7f0b000f

.field public static final dialog_sub_text_black_theme:I = 0x7f0b0010

.field public static final dialog_subtitle_divider:I = 0x7f0b0011

.field public static final dialog_subtitle_text:I = 0x7f0b0012

.field public static final equalizer_bubble_text_color:I = 0x7f0b0013

.field public static final equalizer_default_text_color:I = 0x7f0b0014

.field public static final equalizer_disabled_text_color:I = 0x7f0b0015

.field public static final everglades2:I = 0x7f0b0016

.field public static final full_player_uhq_tag_shadow_text:I = 0x7f0b0017

.field public static final full_player_uhq_tag_text:I = 0x7f0b0018

.field public static final grid_item_line1:I = 0x7f0b0019

.field public static final grid_item_line2:I = 0x7f0b001a

.field public static final hover_and_more_text_white_theme:I = 0x7f0b001b

.field public static final hover_common_view_text_color_dark_theme:I = 0x7f0b001c

.field public static final hover_common_view_text_color_white_theme:I = 0x7f0b001d

.field public static final hover_list_divider_dark_theme:I = 0x7f0b001e

.field public static final hover_list_divider_white_theme:I = 0x7f0b001f

.field public static final hover_list_text1_white_theme:I = 0x7f0b0020

.field public static final hover_list_text2_white_theme:I = 0x7f0b0021

.field public static final hover_playlist_title_text_color_dark_theme:I = 0x7f0b0022

.field public static final hover_playlist_title_text_color_white_theme:I = 0x7f0b0023

.field public static final hover_seekbar_view_text_color:I = 0x7f0b0024

.field public static final list_album_cover_text1:I = 0x7f0b0025

.field public static final list_album_cover_text2:I = 0x7f0b0026

.field public static final list_artist_track_subtitle_bg:I = 0x7f0b0027

.field public static final list_artist_track_subtitle_text:I = 0x7f0b0028

.field public static final list_item_disable:I = 0x7f0b0029

.field public static final list_item_duration:I = 0x7f0b002a

.field public static final list_item_line1:I = 0x7f0b002b

.field public static final list_item_line2:I = 0x7f0b002c

.field public static final list_item_playing:I = 0x7f0b002d

.field public static final list_item_sub_title_background:I = 0x7f0b002e

.field public static final list_select_all_text_color:I = 0x7f0b002f

.field public static final list_shuffle_item_text:I = 0x7f0b0030

.field public static final list_shuffle_item_text_color:I = 0x7f0b00aa

.field public static final list_shuffle_item_text_press:I = 0x7f0b0031

.field public static final list_subtitle_divider:I = 0x7f0b0032

.field public static final list_subtitle_left:I = 0x7f0b0033

.field public static final list_subtitle_right:I = 0x7f0b0034

.field public static final matched_text_color:I = 0x7f0b0035

.field public static final media_info_edit_description:I = 0x7f0b0036

.field public static final media_info_header_title_text:I = 0x7f0b0037

.field public static final media_info_list_subtitle:I = 0x7f0b0038

.field public static final media_info_main_text:I = 0x7f0b0039

.field public static final media_info_sub_text:I = 0x7f0b003a

.field public static final mini_player_ripple_background_color:I = 0x7f0b003b

.field public static final music_fullplayer_progress_bg:I = 0x7f0b003c

.field public static final music_fullplayer_progress_primary:I = 0x7f0b003d

.field public static final music_fullplayer_progress_secondary:I = 0x7f0b003e

.field public static final music_mini_player_info_text:I = 0x7f0b003f

.field public static final music_miniplayer_controller_bg:I = 0x7f0b0040

.field public static final music_miniplayer_played_time:I = 0x7f0b0041

.field public static final music_miniplayer_progress_bg:I = 0x7f0b0042

.field public static final music_miniplayer_progress_primary:I = 0x7f0b0043

.field public static final music_player_duration:I = 0x7f0b0044

.field public static final music_player_easymode_title_sub:I = 0x7f0b0045

.field public static final music_player_list_playing:I = 0x7f0b0046

.field public static final music_player_lyric_text_color:I = 0x7f0b0047

.field public static final music_player_nowplaying_item_divider:I = 0x7f0b0048

.field public static final music_player_nowplaying_list_header_count:I = 0x7f0b0049

.field public static final music_player_nowplaying_list_header_total:I = 0x7f0b004a

.field public static final music_player_nowplaying_list_text1:I = 0x7f0b004b

.field public static final music_player_nowplaying_list_text2:I = 0x7f0b004c

.field public static final music_player_nowplaying_playing_color:I = 0x7f0b004d

.field public static final music_player_nowplaying_ripple:I = 0x7f0b004e

.field public static final music_player_played_time:I = 0x7f0b004f

.field public static final music_player_starting_progress:I = 0x7f0b0050

.field public static final music_player_title_main:I = 0x7f0b0051

.field public static final music_player_title_main_top:I = 0x7f0b0052

.field public static final music_player_title_sub:I = 0x7f0b0053

.field public static final music_player_title_sub_sidecast:I = 0x7f0b0054

.field public static final music_player_title_sub_top:I = 0x7f0b0055

.field public static final music_player_volume_text:I = 0x7f0b0056

.field public static final music_query_no_item_text:I = 0x7f0b0057

.field public static final music_square_expandable_icon_color:I = 0x7f0b0058

.field public static final music_square_expandable_text_color:I = 0x7f0b0059

.field public static final music_square_expandable_text_color_expanded:I = 0x7f0b005a

.field public static final music_square_no_item_text:I = 0x7f0b005b

.field public static final music_square_text_color:I = 0x7f0b005c

.field public static final new_soundAlive_advanced_btn_text_color:I = 0x7f0b005d

.field public static final new_soundAlive_advanced_btn_text_press_color:I = 0x7f0b005e

.field public static final new_soundAlive_auto_text:I = 0x7f0b005f

.field public static final new_soundAlive_effect_text2_color:I = 0x7f0b0060

.field public static final new_soundAlive_effect_text_color:I = 0x7f0b0061

.field public static final new_soundAlive_gridView_text:I = 0x7f0b0062

.field public static final new_soundAlive_gridView_text2:I = 0x7f0b0063

.field public static final no_item_text:I = 0x7f0b0064

.field public static final playspeed_text_color:I = 0x7f0b0065

.field public static final quick_panel_artist_text:I = 0x7f0b0066

.field public static final quick_panel_bg_color:I = 0x7f0b0067

.field public static final quick_panel_title_text:I = 0x7f0b0068

.field public static final ripple_background_color:I = 0x7f0b0069

.field public static final set_as_duration_time_text:I = 0x7f0b006a

.field public static final set_as_starting_time_text:I = 0x7f0b006b

.field public static final set_as_text1:I = 0x7f0b006c

.field public static final set_as_text2:I = 0x7f0b006d

.field public static final setting_pref_title_color:I = 0x7f0b00ab

.field public static final settings_title_disable:I = 0x7f0b006e

.field public static final settings_title_enable:I = 0x7f0b006f

.field public static final sound_alive_advanced_btn_text_color_selector:I = 0x7f0b00ac

.field public static final sound_alive_card_bg:I = 0x7f0b0070

.field public static final sound_alive_card_bg_press:I = 0x7f0b0071

.field public static final sound_alive_card_line_off:I = 0x7f0b0072

.field public static final sound_alive_card_line_on:I = 0x7f0b0073

.field public static final sound_alive_square_bg:I = 0x7f0b0074

.field public static final sound_alive_square_press:I = 0x7f0b0075

.field public static final square1:I = 0x7f0b0076

.field public static final square10:I = 0x7f0b0077

.field public static final square11:I = 0x7f0b0078

.field public static final square12:I = 0x7f0b0079

.field public static final square13:I = 0x7f0b007a

.field public static final square14:I = 0x7f0b007b

.field public static final square15:I = 0x7f0b007c

.field public static final square16:I = 0x7f0b007d

.field public static final square17:I = 0x7f0b007e

.field public static final square18:I = 0x7f0b007f

.field public static final square19:I = 0x7f0b0080

.field public static final square2:I = 0x7f0b0081

.field public static final square20:I = 0x7f0b0082

.field public static final square21:I = 0x7f0b0083

.field public static final square22:I = 0x7f0b0084

.field public static final square23:I = 0x7f0b0085

.field public static final square24:I = 0x7f0b0086

.field public static final square25:I = 0x7f0b0087

.field public static final square3:I = 0x7f0b0088

.field public static final square4:I = 0x7f0b0089

.field public static final square5:I = 0x7f0b008a

.field public static final square6:I = 0x7f0b008b

.field public static final square7:I = 0x7f0b008c

.field public static final square8:I = 0x7f0b008d

.field public static final square9:I = 0x7f0b008e

.field public static final square_selected:I = 0x7f0b008f

.field public static final square_unselected:I = 0x7f0b0090

.field public static final sview_corver_music_player_bg:I = 0x7f0b0091

.field public static final sview_cover_music_dialog_header_bg:I = 0x7f0b0092

.field public static final sview_cover_music_dialog_list_bg:I = 0x7f0b0093

.field public static final sview_cover_music_list_text1:I = 0x7f0b0094

.field public static final sview_cover_music_list_text2:I = 0x7f0b0095

.field public static final sview_cover_music_player_text_color:I = 0x7f0b0096

.field public static final sview_cover_music_player_text_shadow:I = 0x7f0b0097

.field public static final tab_text_default_color:I = 0x7f0b0098

.field public static final tab_text_selected_color:I = 0x7f0b0099

.field public static final title_shadow_color:I = 0x7f0b009a

.field public static final tw_action_bar_sub_tab_bg_holo:I = 0x7f0b009b

.field public static final tw_black:I = 0x7f0b009c

.field public static final tw_color001:I = 0x7f0b009d

.field public static final tw_white:I = 0x7f0b009e

.field public static final unchecked_bitmap_color_list:I = 0x7f0b00ad

.field public static final user_eq_interceptor_background:I = 0x7f0b009f

.field public static final widget_controller_artist:I = 0x7f0b00a0

.field public static final widget_controller_bg:I = 0x7f0b00a1

.field public static final widget_controller_title:I = 0x7f0b00a2

.field public static final widget_list_artist:I = 0x7f0b00a3

.field public static final widget_list_bg:I = 0x7f0b00a4

.field public static final widget_list_title:I = 0x7f0b00a5

.field public static final widget_no_item_text:I = 0x7f0b00a6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
