.class public final Lcom/sec/android/app/music/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/music/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AnimatedToggleButton:[I

.field public static final AnimatedToggleButton_buttonAnimationTime:I = 0xb

.field public static final AnimatedToggleButton_circleRadius:I = 0x8

.field public static final AnimatedToggleButton_edgeAnimationTime:I = 0xa

.field public static final AnimatedToggleButton_edgeColor:I = 0x9

.field public static final AnimatedToggleButton_edgeWidth:I = 0x6

.field public static final AnimatedToggleButton_focusedImageA:I = 0x2

.field public static final AnimatedToggleButton_focusedImageB:I = 0x3

.field public static final AnimatedToggleButton_imageA:I = 0x0

.field public static final AnimatedToggleButton_imageB:I = 0x1

.field public static final AnimatedToggleButton_maskRadius:I = 0x7

.field public static final AnimatedToggleButton_pressedImageA:I = 0x4

.field public static final AnimatedToggleButton_pressedImageB:I = 0x5

.field public static final CheckedRelativeLayout:[I

.field public static final CheckedRelativeLayout_checkableId:I = 0x0

.field public static final GLFlatGalleryView:[I

.field public static final GLFlatGalleryView_albumSize:I = 0x4

.field public static final GLFlatGalleryView_albumSpacing:I = 0x5

.field public static final GLFlatGalleryView_backgroundColor:I = 0x6

.field public static final GLFlatGalleryView_clickAnimation:I = 0x2

.field public static final GLFlatGalleryView_maxTiltedAngle:I = 0x0

.field public static final GLFlatGalleryView_scrollingFriction:I = 0x8

.field public static final GLFlatGalleryView_shadowBitmap:I = 0x9

.field public static final GLFlatGalleryView_textLayout:I = 0x7

.field public static final GLFlatGalleryView_tiltedK:I = 0x1

.field public static final GLFlatGalleryView_tiltedReturnAnimation:I = 0x3

.field public static final GLFlatResizeGalleryView:[I

.field public static final GLFlatResizeGalleryView_albumOpacity:I = 0x0

.field public static final GLFlatResizeGalleryView_albumSize:I = 0x1

.field public static final GLFlatResizeGalleryView_albumSpacing:I = 0x2

.field public static final GLFlatResizeGalleryView_albumSpacingAdditional:I = 0x3

.field public static final GLFlatResizeGalleryView_backgroundColor:I = 0x4

.field public static final GLFlatResizeGalleryView_scrollingFriction:I = 0x8

.field public static final GLFlatResizeGalleryView_selectedAlbumBorder:I = 0x5

.field public static final GLFlatResizeGalleryView_selectedAlbumSize:I = 0x6

.field public static final GLFlatResizeGalleryView_shadowBitmap:I = 0x9

.field public static final GLFlatResizeGalleryView_textLayout:I = 0x7

.field public static final StoreCheckedRelativeLayout:[I

.field public static final StoreCheckedRelativeLayout_checkable:I = 0x0

.field public static final TwProgressBar:[I

.field public static final TwProgressBar_dualcolorProgress:I = 0xd

.field public static final TwProgressBar_progress:I = 0x5

.field public static final TwProgressBar_secondaryProgress:I = 0x6

.field public static final TwProgressBar_theme:I = 0x12

.field public static final TwProgressBar_twBackgroundColor:I = 0xa

.field public static final TwProgressBar_twBackgroundDrawable:I = 0x7

.field public static final TwProgressBar_twDualColorProgressColor:I = 0xf

.field public static final TwProgressBar_twDualColorProgressDrawable:I = 0xe

.field public static final TwProgressBar_twIndicatorThickness:I = 0x10

.field public static final TwProgressBar_twMax:I = 0x4

.field public static final TwProgressBar_twMaxHeight:I = 0x3

.field public static final TwProgressBar_twMaxWidth:I = 0x1

.field public static final TwProgressBar_twMinHeight:I = 0x2

.field public static final TwProgressBar_twMinWidth:I = 0x0

.field public static final TwProgressBar_twProgressColor:I = 0xb

.field public static final TwProgressBar_twProgressDrawable:I = 0x8

.field public static final TwProgressBar_twProgressOrientation:I = 0x11

.field public static final TwProgressBar_twSecondaryColor:I = 0xc

.field public static final TwProgressBar_twSecondaryDrawable:I = 0x9

.field public static final TwSeekBar:[I

.field public static final TwSeekBar_twSeekBarDisableAlpha:I = 0x3

.field public static final TwSeekBar_twSeekBarIncrement:I = 0x2

.field public static final TwSeekBar_twSeekBarSeekable:I = 0x4

.field public static final TwSeekBar_twSeekBarThumb:I = 0x0

.field public static final TwSeekBar_twSeekBarThumbOffset:I = 0x1

.field public static final TwSeekBar_twSeekThumbFontBoldStyle:I = 0x7

.field public static final TwSeekBar_twSeekThumbFontColor:I = 0x5

.field public static final TwSeekBar_twSeekThumbFontEnable:I = 0x8

.field public static final TwSeekBar_twSeekThumbFontSize:I = 0x6

.field public static final TwTheme:[I

.field public static final TwTheme_twProgressBarStyle:I = 0x1

.field public static final TwTheme_twSeekBarStyle:I = 0x0

.field public static final TwoWayAbsListView:[I

.field public static final TwoWayAbsListView_cacheColorHint:I = 0x5

.field public static final TwoWayAbsListView_drawSelectorOnTop:I = 0x1

.field public static final TwoWayAbsListView_listSelector:I = 0x0

.field public static final TwoWayAbsListView_scrollDirectionLandscape:I = 0x8

.field public static final TwoWayAbsListView_scrollDirectionPortrait:I = 0x7

.field public static final TwoWayAbsListView_scrollingCache:I = 0x3

.field public static final TwoWayAbsListView_smoothScrollbar:I = 0x6

.field public static final TwoWayAbsListView_stackFromBottom:I = 0x2

.field public static final TwoWayAbsListView_transcriptMode:I = 0x4

.field public static final TwoWayGridView:[I

.field public static final TwoWayGridView_columnWidth:I = 0x4

.field public static final TwoWayGridView_gravity:I = 0x0

.field public static final TwoWayGridView_horizontalSpacing:I = 0x1

.field public static final TwoWayGridView_numColumns:I = 0x6

.field public static final TwoWayGridView_numRows:I = 0x7

.field public static final TwoWayGridView_rowHeight:I = 0x5

.field public static final TwoWayGridView_stretchMode:I = 0x3

.field public static final TwoWayGridView_verticalSpacing:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/16 v4, 0x9

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3011
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/music/R$styleable;->AnimatedToggleButton:[I

    .line 3177
    new-array v0, v3, [I

    const v1, 0x7f010010

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/music/R$styleable;->CheckedRelativeLayout:[I

    .line 3222
    new-array v0, v5, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/music/R$styleable;->GLFlatGalleryView:[I

    .line 3384
    new-array v0, v5, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/music/R$styleable;->GLFlatResizeGalleryView:[I

    .line 3544
    new-array v0, v3, [I

    const v1, 0x7f01001f

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/music/R$styleable;->StoreCheckedRelativeLayout:[I

    .line 3607
    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/app/music/R$styleable;->TwProgressBar:[I

    .line 3890
    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/sec/android/app/music/R$styleable;->TwSeekBar:[I

    .line 4029
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/sec/android/app/music/R$styleable;->TwTheme:[I

    .line 4078
    new-array v0, v4, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/sec/android/app/music/R$styleable;->TwoWayAbsListView:[I

    .line 4242
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/sec/android/app/music/R$styleable;->TwoWayGridView:[I

    return-void

    .line 3011
    :array_0
    .array-data 4
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
    .end array-data

    .line 3222
    :array_1
    .array-data 4
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010016
        0x7f010017
        0x7f010019
        0x7f01001c
        0x7f01001d
        0x7f01001e
    .end array-data

    .line 3384
    :array_2
    .array-data 4
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
    .end array-data

    .line 3607
    :array_3
    .array-data 4
        0x7f010020
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
        0x7f01002f
        0x7f010030
        0x7f010031
        0x7f010032
    .end array-data

    .line 3890
    :array_4
    .array-data 4
        0x7f010033
        0x7f010034
        0x7f010035
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
        0x7f01003a
        0x7f01003b
    .end array-data

    .line 4029
    :array_5
    .array-data 4
        0x7f01003c
        0x7f01003d
    .end array-data

    .line 4078
    :array_6
    .array-data 4
        0x7f01003e
        0x7f01003f
        0x7f010040
        0x7f010041
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
    .end array-data

    .line 4242
    :array_7
    .array-data 4
        0x7f010002
        0x7f010047
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
