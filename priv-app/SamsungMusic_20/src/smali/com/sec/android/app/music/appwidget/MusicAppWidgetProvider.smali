.class public Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;
.super Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;
.source "MusicAppWidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;
    }
.end annotation


# static fields
.field public static final ACTION_CLICK_LIST:Ljava/lang/String; = "com.samsung.musicplus.appwidget.CLICK_LIST"

.field public static final EXTRA_LIST:Ljava/lang/String; = "list"

.field public static final EXTRA_SELELTED_ID:Ljava/lang/String; = "_id"

.field public static final EXTRA_SELELTED_POSITION:Ljava/lang/String; = "selcted_postion"

.field private static final TAG:Ljava/lang/String; = "MusicWidget"

.field private static sInstance:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;


# instance fields
.field private mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;-><init>()V

    .line 292
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;
    .locals 2

    .prologue
    .line 64
    const-class v1, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->sInstance:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;

    invoke-direct {v0}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;-><init>()V

    sput-object v0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->sInstance:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;

    .line 67
    :cond_0
    sget-object v0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->sInstance:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private onListItemClicked(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 122
    const-string v6, "_id"

    const-wide/16 v8, -0x1

    invoke-virtual {p2, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 123
    .local v0, "audioId":J
    const-string v6, "selcted_postion"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 124
    .local v4, "position":I
    const-string v6, "MusicWidget"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceive ACTION_CLICK_LIST audioId : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " position : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-static {}, Lcom/samsung/musicplus/service/PlayerService;->getInstance()Lcom/samsung/musicplus/service/PlayerService;

    move-result-object v5

    .line 128
    .local v5, "s":Lcom/samsung/musicplus/service/PlayerService;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v6

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    .line 130
    :cond_0
    const-string v6, "list"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v3

    .line 132
    .local v3, "list":[J
    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.samsung.musicplus.intent.action.PLAY_WIDGET_LIST"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 133
    .local v2, "i":Landroid/content/Intent;
    new-instance v6, Landroid/content/ComponentName;

    const-class v7, Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {v6, p1, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 134
    const-string v6, "list"

    invoke-virtual {v2, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 135
    const-string v6, "listPosition"

    invoke-virtual {v2, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 136
    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 142
    .end local v3    # "list":[J
    :goto_0
    return-void

    .line 138
    .end local v2    # "i":Landroid/content/Intent;
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.samsung.musicplus.musicservicecommand.togglepause"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 139
    .restart local v2    # "i":Landroid/content/Intent;
    new-instance v6, Landroid/content/ComponentName;

    const-class v7, Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {v6, p1, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 140
    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private updateK2HDTagView(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 4
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    const/4 v3, 0x0

    .line 240
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/samsung/musicplus/service/PlayerService;->isK2HD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 242
    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getSamplingRate()I

    move-result v1

    .line 243
    .local v1, "samplingRate":I
    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getBitDepth()I

    move-result v0

    .line 245
    .local v0, "bitDepth":I
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isSupportUhqa()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1, v0}, Lcom/samsung/musicplus/util/UiUtils;->isUhqa(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 246
    iget-object v2, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setK2HD(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 253
    .end local v0    # "bitDepth":I
    .end local v1    # "samplingRate":I
    :goto_0
    return-void

    .line 248
    .restart local v0    # "bitDepth":I
    .restart local v1    # "samplingRate":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setK2HD(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    goto :goto_0

    .line 251
    .end local v0    # "bitDepth":I
    .end local v1    # "samplingRate":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setK2HD(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    goto :goto_0
.end method

.method private updateUhqTagView(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 4
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    const/4 v3, 0x0

    .line 226
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/musicplus/util/UiUtils;->isLocalContents(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 227
    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getSamplingRate()I

    move-result v1

    .line 228
    .local v1, "samplingRate":I
    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getBitDepth()I

    move-result v0

    .line 229
    .local v0, "bitDepth":I
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isSupportUhqa()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1, v0}, Lcom/samsung/musicplus/util/UiUtils;->isUhqa(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 230
    iget-object v2, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setUHQ(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 237
    .end local v0    # "bitDepth":I
    .end local v1    # "samplingRate":I
    :goto_0
    return-void

    .line 232
    .restart local v0    # "bitDepth":I
    .restart local v1    # "samplingRate":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setUHQ(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    goto :goto_0

    .line 235
    .end local v0    # "bitDepth":I
    .end local v1    # "samplingRate":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setUHQ(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    goto :goto_0
.end method

.method private updateWidgetList(Landroid/content/Context;Landroid/widget/RemoteViews;[I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "v"    # Landroid/widget/RemoteViews;
    .param p3, "appWidgetIds"    # [I

    .prologue
    const/4 v4, 0x1

    const v5, 0x7f0d01c1

    .line 265
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/music/appwidget/WidgetListService;

    invoke-direct {v0, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 269
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 271
    invoke-virtual {p2, v5, v0}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    .line 272
    const v3, 0x7f0d01c2

    invoke-virtual {p2, v5, v3}, Landroid/widget/RemoteViews;->setEmptyView(II)V

    .line 277
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;

    invoke-direct {v1, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 278
    .local v1, "onClickIntent":Landroid/content/Intent;
    const-string v3, "com.samsung.musicplus.appwidget.CLICK_LIST"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    invoke-virtual {v1, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 283
    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {p1, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 285
    .local v2, "onClickPendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {p2, v5, v2}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    .line 286
    return-void
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    const/4 v7, 0x0

    .line 72
    invoke-static {}, Lcom/samsung/musicplus/service/PlayerService;->getInstance()Lcom/samsung/musicplus/service/PlayerService;

    move-result-object v3

    .line 73
    .local v3, "s":Lcom/samsung/musicplus/service/PlayerService;
    const-string v4, "MusicWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onDeleted appWidgetIds length : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, p2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " id : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, p2, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " service :  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    if-nez v3, :cond_0

    .line 79
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 80
    .local v2, "mgr":Landroid/appwidget/AppWidgetManager;
    new-instance v4, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v4, p1, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v4}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 82
    .local v1, "ids":[I
    new-instance v0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    const v4, 0x7f040095

    invoke-direct {v0, p1, v4}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;-><init>(Landroid/content/Context;I)V

    .line 84
    .local v0, "builder":Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;
    const/4 v4, -0x1

    invoke-virtual {v0, v4, v7}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 86
    invoke-virtual {v0}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 88
    .end local v0    # "builder":Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;
    .end local v1    # "ids":[I
    .end local v2    # "mgr":Landroid/appwidget/AppWidgetManager;
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 89
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 93
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "action":Ljava/lang/String;
    const-string v4, "MusicWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onReceive action : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v4, "com.samsung.musicplus.appwidget.CLICK_LIST"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 97
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->onListItemClicked(Landroid/content/Context;Landroid/content/Intent;)V

    .line 111
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 112
    return-void

    .line 98
    :cond_1
    const-string v4, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 102
    invoke-static {}, Lcom/samsung/musicplus/service/PlayerService;->getInstance()Lcom/samsung/musicplus/service/PlayerService;

    move-result-object v3

    .line 103
    .local v3, "s":Lcom/samsung/musicplus/service/PlayerService;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/samsung/musicplus/service/PlayerService;->getListItemCount()I

    move-result v4

    if-nez v4, :cond_3

    .line 104
    :cond_2
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 105
    .local v2, "mgr":Landroid/appwidget/AppWidgetManager;
    new-instance v4, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v4, p1, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v4}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 107
    .local v1, "appWidgetIds":[I
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->updateList(Landroid/appwidget/AppWidgetManager;[I)V

    .line 109
    .end local v1    # "appWidgetIds":[I
    .end local v2    # "mgr":Landroid/appwidget/AppWidgetManager;
    :cond_3
    const-string v4, "MusicWidget"

    const-string v5, "ACTION_MEDIA_SCANNER_FINISHED"

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAlbumArt(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "album"    # Landroid/graphics/Bitmap;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    sget-object v1, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->sInstance:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->notifyAppWidget(Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;Ljava/lang/Object;)V

    .line 150
    :cond_0
    return-void
.end method

.method protected updateMetaInfo(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[II)V
    .locals 4
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mgr"    # Landroid/appwidget/AppWidgetManager;
    .param p4, "appWidgetIds"    # [I
    .param p5, "layout"    # I

    .prologue
    .line 200
    new-instance v1, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-direct {v1, p2, p5}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getArtist()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 202
    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setButtons()Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getPersonalMode()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setPersonalIcon(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 204
    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getListType()I

    move-result v0

    .line 205
    .local v0, "listType":I
    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v2

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->updateExtraButtons(II)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 208
    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->isLocalMediaTrack()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->isPreparing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 209
    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 210
    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->showProgress(Z)V

    .line 213
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->updateUhqTagView(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 214
    invoke-direct {p0, p1}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->updateK2HDTagView(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v1

    invoke-direct {p0, p2, v1, p4}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->updateWidgetList(Landroid/content/Context;Landroid/widget/RemoteViews;[I)V

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v1

    invoke-virtual {p3, p4, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 222
    return-void
.end method

.method protected updatePlayStatus(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[II)V
    .locals 3
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mgr"    # Landroid/appwidget/AppWidgetManager;
    .param p4, "appWidgetIds"    # [I
    .param p5, "layout"    # I

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-direct {v0, p2, p5}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getListType()I

    move-result v1

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-virtual {p3, p4, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 161
    return-void
.end method

.method protected updateProgress(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[IIZ)V
    .locals 1
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mgr"    # Landroid/appwidget/AppWidgetManager;
    .param p4, "appWidgetIds"    # [I
    .param p5, "layout"    # I
    .param p6, "value"    # Z

    .prologue
    .line 188
    if-nez p6, :cond_1

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    if-nez v0, :cond_0

    .line 190
    new-instance v0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-direct {v0, p2, p5}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v0, p6}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->showProgress(Z)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-virtual {p3, p4, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 195
    :cond_1
    return-void
.end method

.method protected updateSettingStatus(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[II)V
    .locals 3
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mgr"    # Landroid/appwidget/AppWidgetManager;
    .param p4, "appWidgetIds"    # [I
    .param p5, "layout"    # I

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    if-nez v0, :cond_0

    .line 167
    new-instance v0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-direct {v0, p2, p5}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v1

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->updateExtraButtons(II)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-virtual {p3, p4, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 172
    return-void
.end method

.method protected updateTitles(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[II)V
    .locals 3
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mgr"    # Landroid/appwidget/AppWidgetManager;
    .param p4, "appWidgetIds"    # [I
    .param p5, "layout"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    if-nez v0, :cond_0

    .line 178
    new-instance v0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-direct {v0, p2, p5}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getArtist()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->mRemoteViewBuilder:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider$MusicWidgetRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-virtual {p3, p4, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 183
    return-void
.end method
