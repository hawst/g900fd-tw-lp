.class final Lcom/mediacanvas/AndroidLDP/f;
.super Landroid/os/Handler;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/f;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/f;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget v1, p1, Landroid/os/Message;->what:I

    sget-object v2, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MC_IsLyricsHandler nMsgWhat:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V

    goto :goto_0

    :pswitch_1
    invoke-static {v0, v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
