.class final Lcom/mediacanvas/AndroidLDP/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Thread;

.field private i:Z

.field private synthetic j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;


# direct methods
.method public constructor <init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->b:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->c:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->d:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->e:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediacanvas/AndroidLDP/e;->f:J

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->g:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->h:Ljava/lang/Thread;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    iput-object p2, p0, Lcom/mediacanvas/AndroidLDP/e;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/e;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    iput-wide p1, p0, Lcom/mediacanvas/AndroidLDP/e;->f:J

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/e;->b:Ljava/lang/String;

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/e;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/e;->c:Ljava/lang/String;

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/e;->d:Ljava/lang/String;

    return-void
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    return v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/e;->e:Ljava/lang/String;

    return-void
.end method

.method public final d()Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->h:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->h:Ljava/lang/Thread;

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->h:Ljava/lang/Thread;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->h:Ljava/lang/Thread;

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->h:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OnIsLyricStart() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method public final e()Z
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    :try_start_0
    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->h:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->h:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->h:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "OnIsLyricStop() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final run()V
    .locals 11

    const/4 v1, 0x0

    const/4 v9, -0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v3, p0, Lcom/mediacanvas/AndroidLDP/e;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_d

    const-string v2, ""

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    const-string v2, "FAIL_LOAD"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v0, 0x1

    move v8, v1

    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    if-eqz v1, :cond_2

    :cond_0
    move v0, v9

    :goto_1
    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->d(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MC_ISLyricsThread nResult:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->e(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/f;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->e(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/mediacanvas/AndroidLDP/f;->sendEmptyMessage(I)Z

    :cond_1
    return-void

    :cond_2
    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    if-nez v1, :cond_0

    if-nez v8, :cond_3

    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v1, 0x1

    if-ne v8, v1, :cond_4

    if-nez v0, :cond_4

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x2

    if-ne v8, v1, :cond_9

    if-eqz v10, :cond_7

    const-string v1, ""

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "FAIL_LOAD"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "LDP"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "MP3"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v0, "NOLYRIC"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    const/4 v0, 0x4

    goto/16 :goto_1

    :cond_5
    const-string v0, "LDB"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    const/4 v0, 0x3

    goto/16 :goto_1

    :cond_6
    iput-object v10, p0, Lcom/mediacanvas/AndroidLDP/e;->g:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    const/4 v0, 0x2

    goto/16 :goto_1

    :cond_7
    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v2}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;I)V

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->b:Ljava/lang/String;

    iget-wide v4, p0, Lcom/mediacanvas/AndroidLDP/e;->f:J

    invoke-static {v1, v2, v4, v5}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->g:Ljava/lang/String;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/e;->g:Ljava/lang/String;

    const-string v1, "EmptyLyrics"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    const/4 v0, 0x4

    goto/16 :goto_1

    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    const/4 v0, 0x2

    goto/16 :goto_1

    :cond_9
    const/4 v1, 0x3

    if-ne v8, v1, :cond_a

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v2}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;I)V

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/e;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediacanvas/AndroidLDP/e;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/mediacanvas/AndroidLDP/e;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/mediacanvas/AndroidLDP/e;->e:Ljava/lang/String;

    iget-wide v6, p0, Lcom/mediacanvas/AndroidLDP/e;->f:J

    invoke-static/range {v1 .. v7}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->g:Ljava/lang/String;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    iget-wide v2, p0, Lcom/mediacanvas/AndroidLDP/e;->f:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_c

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->g:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "TTT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/mediacanvas/AndroidLDP/e;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/e;->g:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    const/4 v0, 0x2

    goto/16 :goto_1

    :cond_a
    const/4 v1, 0x4

    if-ne v8, v1, :cond_b

    if-nez v0, :cond_b

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->j:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/e;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    const/4 v0, 0x3

    goto/16 :goto_1

    :cond_b
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    const/4 v0, 0x4

    goto/16 :goto_1

    :cond_c
    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/e;->i:Z

    if-nez v1, :cond_0

    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto/16 :goto_0

    :cond_d
    move v8, v1

    goto/16 :goto_0
.end method
