.class final Lcom/mediacanvas/AndroidLDP/b;
.super Landroid/os/AsyncTask;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private synthetic c:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;


# direct methods
.method private constructor <init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/b;->c:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/b;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/b;->b:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/b;-><init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/b;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediacanvas/AndroidLDP/b;->b:Ljava/lang/String;

    return-void
.end method

.method protected final varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/b;->c:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/b;->c:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;I)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/b;->c:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/b;->c:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->g(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/b;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/b;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/b;->c:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v2}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->h(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lldpsdksub/f/a;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/mediacanvas/AndroidLDP/Search/d;->a(Ljava/lang/String;Ljava/lang/String;Lldpsdksub/f/a;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/b;->c:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/b;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Ljava/lang/String;)V

    :cond_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Ljava/lang/Boolean;

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/b;->c:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/b;->c:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;->OnSearchStepTwoListener(Z)V

    :cond_0
    return-void
.end method
