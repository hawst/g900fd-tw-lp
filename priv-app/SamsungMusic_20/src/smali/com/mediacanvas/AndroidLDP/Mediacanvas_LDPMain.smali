.class public Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;
.super Ljava/lang/Object;


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Z

.field public LDB_ID3_FRAMEHDR_SIZE:I

.field public TAYLOR_LDB_SERVICE_INCLUDE:I

.field private a:Landroid/content/Context;

.field private b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:J

.field private i:Lldpsdksub/f/a;

.field private j:I

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Lcom/mediacanvas/AndroidLDP/e;

.field private o:Lcom/mediacanvas/AndroidLDP/f;

.field private p:Lcom/mediacanvas/AndroidLDP/d;

.field private q:Lcom/mediacanvas/AndroidLDP/c;

.field private r:Z

.field private s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

.field private t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

.field private u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

.field private v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

.field private w:Ljava/util/HashMap;

.field private x:Lcom/mediacanvas/AndroidLDP/a;

.field private y:Lcom/mediacanvas/AndroidLDP/b;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->LDB_ID3_FRAMEHDR_SIZE:I

    iput v3, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAYLOR_LDB_SERVICE_INCLUDE:I

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iput-boolean v4, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c:Z

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->d:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->e:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->f:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->g:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->h:J

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->j:I

    iput-boolean v3, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k:Z

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->m:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n:Lcom/mediacanvas/AndroidLDP/e;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->o:Lcom/mediacanvas/AndroidLDP/f;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->p:Lcom/mediacanvas/AndroidLDP/d;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->q:Lcom/mediacanvas/AndroidLDP/c;

    iput-boolean v3, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->r:Z

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->x:Lcom/mediacanvas/AndroidLDP/a;

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->y:Lcom/mediacanvas/AndroidLDP/b;

    iput-boolean v4, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->z:Z

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->A:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->B:Z

    if-eqz p1, :cond_0

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    new-instance v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;

    invoke-direct {v0}, Lcom/mediacanvas/AndroidLDP/Mgr/a;-><init>()V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->a()V

    new-instance v0, Lcom/mediacanvas/AndroidLDP/e;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/mediacanvas/AndroidLDP/e;-><init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n:Lcom/mediacanvas/AndroidLDP/e;

    new-instance v0, Lcom/mediacanvas/AndroidLDP/f;

    invoke-direct {v0, p0}, Lcom/mediacanvas/AndroidLDP/f;-><init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->o:Lcom/mediacanvas/AndroidLDP/f;

    new-instance v0, Lcom/mediacanvas/AndroidLDP/d;

    invoke-direct {v0, p0, v3}, Lcom/mediacanvas/AndroidLDP/d;-><init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;B)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->p:Lcom/mediacanvas/AndroidLDP/d;

    new-instance v0, Lcom/mediacanvas/AndroidLDP/c;

    invoke-direct {v0, p0}, Lcom/mediacanvas/AndroidLDP/c;-><init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->q:Lcom/mediacanvas/AndroidLDP/c;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->g:Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->m:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    const-string v1, "android_mediacanvas_ssp_ldp_sdk"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v0, "android_mediacanvas_ssp_ldp_sdk_value"

    const-string v2, ""

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetRandomId() 000 strUnique:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "-"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    const-string v2, "android_mediacanvas_ssp_ldp_sdk_value"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetRandomId() 111 strUnique:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Ljava/lang/String;J)Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lldpsdksub/a/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    new-instance v0, Lldpsdksub/d/c;

    invoke-direct {v0}, Lldpsdksub/d/c;-><init>()V

    invoke-static {p1}, Lldpsdksub/d/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    invoke-static {v0, p2, p3, v1}, Lcom/mediacanvas/AndroidLDP/Search/b;->a(Ljava/lang/String;JLldpsdksub/f/a;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    .locals 9

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p3, :cond_2

    const-string v0, ""

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p4, :cond_2

    const-string v0, ""

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-lez v0, :cond_2

    invoke-static {p1}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {p1}, Lldpsdksub/a/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lldpsdksub/d/c;

    invoke-direct {v0}, Lldpsdksub/d/c;-><init>()V

    invoke-static {p1}, Lldpsdksub/d/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l:Ljava/lang/String;

    :cond_1
    iget-object v6, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l:Ljava/lang/String;

    iget-object v7, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->m:Ljava/lang/String;

    iget-object v8, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-wide v4, p5

    invoke-static/range {v1 .. v8}, Lcom/mediacanvas/AndroidLDP/Search/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lldpsdksub/f/a;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k:Z

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    invoke-static {v0}, Lldpsdksub/a/a;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k:Z

    :cond_1
    :goto_1
    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "privateSetNetwork() m_nNetworkAbleNum:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_bNetworkAble:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    invoke-static {v0}, Lldpsdksub/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k:Z

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    invoke-static {v0}, Lldpsdksub/a/a;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k:Z

    goto :goto_1

    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    invoke-static {v0}, Lldpsdksub/a/a;->c(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k:Z

    goto :goto_1

    :cond_4
    iput-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k:Z

    goto :goto_1
.end method

.method static synthetic a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c()V

    return-void
.end method

.method static synthetic a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;I)V
    .locals 2

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n:Lcom/mediacanvas/AndroidLDP/e;

    invoke-virtual {v0}, Lcom/mediacanvas/AndroidLDP/e;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n:Lcom/mediacanvas/AndroidLDP/e;

    invoke-virtual {v1}, Lcom/mediacanvas/AndroidLDP/e;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Ljava/lang/String;ILjava/lang/String;)V

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->OnResizeViewForNoLyrcis()V

    :cond_0
    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    invoke-interface {v1, v0, p1}, Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;->IsLyricResult(Ljava/lang/String;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-ltz p2, :cond_0

    const/4 v0, 0x5

    if-gt p2, v0, :cond_0

    invoke-static {p1}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->h(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, ""

    packed-switch p2, :pswitch_data_0

    move-object p3, v0

    :goto_1
    :pswitch_0
    if-eqz p3, :cond_0

    const-string v0, ""

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SetResultISLyrcic() nResultIsLyrics:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SetResultISLyrcic() strPhysicalPath:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SetResultISLyrcic() strValue:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :pswitch_1
    const-string p3, "LDP"

    goto :goto_1

    :pswitch_2
    const-string p3, "MP3"

    goto :goto_1

    :pswitch_3
    const-string p3, "LDB"

    goto :goto_1

    :pswitch_4
    const-string p3, "NOLYRIC"

    goto :goto_1

    :pswitch_5
    const-string p3, "FAIL_LOAD"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;[B)Z
    .locals 2

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    array-length v1, p1

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0, p1, v1}, Lldpsdksub/g/d;->b([BI)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a(Landroid/graphics/Bitmap$Config;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->f()V

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->e()V

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p2, :cond_0

    const-string v1, ""

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    invoke-static {p1}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->j:I

    invoke-direct {p0, v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(I)V

    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    invoke-static {p1, p2, v1}, Lcom/mediacanvas/AndroidLDP/Search/c;->a(Ljava/lang/String;Ljava/lang/String;Lldpsdksub/f/a;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "LoadLyricsFromSamsungLDB() NullPointerException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    :catch_1
    move-exception v1

    sget-object v2, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "LoadLyricsFromSamsungLDB() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p3, :cond_0

    const-string v1, ""

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    invoke-static {p1}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->j:I

    invoke-direct {p0, v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(I)V

    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    invoke-static {p1, p2, p3, v1}, Lcom/mediacanvas/AndroidLDP/Search/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lldpsdksub/f/a;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "LoadLyricsFromSamsungLDB() NullPointerException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    :catch_1
    move-exception v1

    sget-object v2, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "LoadLyricsFromSamsungLDB() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)I
    .locals 1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->j:I

    return v0
.end method

.method static synthetic b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method private b()Z
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    if-nez v0, :cond_0

    new-instance v0, Lldpsdksub/f/a;

    invoke-direct {v0}, Lldpsdksub/f/a;-><init>()V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Search/e;->a(Lldpsdksub/f/a;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;[B)Z
    .locals 2

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    array-length v1, p1

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0, p1, v1}, Lldpsdksub/g/d;->a([BI)I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a(Landroid/graphics/Bitmap$Config;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->f()V

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->e()V

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Ljava/lang/String;)Z
    .locals 1

    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lldpsdksub/a/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->j(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 4

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "OnIsLyricsThread() m_strPhysicalPath:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n:Lcom/mediacanvas/AndroidLDP/e;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n:Lcom/mediacanvas/AndroidLDP/e;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/e;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n:Lcom/mediacanvas/AndroidLDP/e;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/e;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n:Lcom/mediacanvas/AndroidLDP/e;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/e;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n:Lcom/mediacanvas/AndroidLDP/e;

    iget-wide v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->h:J

    invoke-virtual {v0, v2, v3}, Lcom/mediacanvas/AndroidLDP/e;->a(J)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n:Lcom/mediacanvas/AndroidLDP/e;

    invoke-virtual {v0}, Lcom/mediacanvas/AndroidLDP/e;->d()Z

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;I)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->p:Lcom/mediacanvas/AndroidLDP/d;

    invoke-virtual {v0}, Lcom/mediacanvas/AndroidLDP/d;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->r:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    if-eqz v1, :cond_0

    if-ne p1, v3, :cond_0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->OnResizeView()V

    :cond_0
    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->B:Z

    if-eqz v1, :cond_1

    if-ne p1, v3, :cond_1

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v1, v1, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v1}, Lldpsdksub/g/d;->e()V

    :cond_1
    if-nez p1, :cond_2

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Ljava/lang/String;ILjava/lang/String;)V

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->OnResizeViewForNoLyrcis()V

    :cond_2
    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    invoke-interface {v1, v0, p1}, Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;->LoadLyricResult(Ljava/lang/String;I)V

    iput-boolean v3, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->z:Z

    :cond_3
    :goto_0
    return-void

    :cond_4
    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->d()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k:Z

    return v0
.end method

.method static synthetic c(Ljava/lang/String;)Z
    .locals 1

    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lldpsdksub/a/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->A:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->A:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->f()V

    :cond_0
    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->e()V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->p:Lcom/mediacanvas/AndroidLDP/d;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->z:Z

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->p:Lcom/mediacanvas/AndroidLDP/d;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/d;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->p:Lcom/mediacanvas/AndroidLDP/d;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->A:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/d;->b(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method static synthetic d(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l:Ljava/lang/String;

    return-void
.end method

.method static synthetic d(Ljava/lang/String;)[B
    .locals 1

    invoke-static {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->l(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/f;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->o:Lcom/mediacanvas/AndroidLDP/f;

    return-object v0
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->c()V

    goto :goto_0
.end method

.method static synthetic e(Ljava/lang/String;)[B
    .locals 1

    invoke-static {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->m(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/c;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->q:Lcom/mediacanvas/AndroidLDP/c;

    return-object v0
.end method

.method static synthetic f(Ljava/lang/String;)[B
    .locals 1

    invoke-static {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method private g(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_3

    invoke-static {p1}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->h(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->h(Ljava/lang/String;)V

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic g(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Z
    .locals 1

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b()Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lldpsdksub/f/a;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    return-object v0
.end method

.method private h(Ljava/lang/String;)V
    .locals 3

    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoveResultISLyrcic() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic i(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    return-object v0
.end method

.method private static i(Ljava/lang/String;)Z
    .locals 2

    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lldpsdksub/a/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->j(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static j(Ljava/lang/String;)[B
    .locals 4

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    const-string v1, ""

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    invoke-static {p0}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lldpsdksub/a/a;->a(Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    array-length v2, v1

    if-lez v2, :cond_0

    array-length v2, v1

    invoke-static {v1, v2}, Lldpsdksub/a/d;->a([BI)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetMP3TagDataForLDP() NullPointerException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    :catch_1
    move-exception v1

    sget-object v2, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetMP3TagDataForLDP() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method private static k(Ljava/lang/String;)[B
    .locals 4

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    const-string v1, ""

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    invoke-static {p0}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lldpsdksub/a/e;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lldpsdksub/a/a;->a(Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    array-length v2, v1

    if-lez v2, :cond_0

    array-length v2, v1

    invoke-static {v1, v2}, Lldpsdksub/a/a;->a([BI)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetMP3TagDataForLDB() NullPointerException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    :catch_1
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetMP3TagDataForLDB() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method private static l(Ljava/lang/String;)[B
    .locals 3

    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p0}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lldpsdksub/a/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->j(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LoadLyricsFromLDP() NullPointerException:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LoadLyricsFromLDP() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method private static m(Ljava/lang/String;)[B
    .locals 3

    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p0}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lldpsdksub/a/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->j(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LoadLyricsFromMP3() NullPointerException:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LoadLyricsFromMP3() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method private static n(Ljava/lang/String;)[B
    .locals 3

    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p0}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lldpsdksub/a/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->k(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LoadLyricsFromSamsungLDB() NullPointerException:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LoadLyricsFromSamsungLDB() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method private o(Ljava/lang/String;)Z
    .locals 2

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "NOLYRIC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "FAIL_LOAD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public GetAllLyrics()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public GetLDPScrollView()Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public GetLDPStart()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->B:Z

    return v0
.end method

.method public GetLDPVisible()I
    .locals 2

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->r:Z

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->getVisibility()I

    move-result v0

    goto :goto_0
.end method

.method public GetLDP_SDK_Version()Ljava/lang/String;
    .locals 1

    const-string v0, "20130305_0001"

    return-object v0
.end method

.method public GetNonAutoSearchStepOne(Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;)Z
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-nez v0, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    if-nez v0, :cond_1

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    move v0, v6

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_3

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz p2, :cond_3

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    if-lez p3, :cond_3

    if-lez p4, :cond_3

    if-eqz p5, :cond_3

    :try_start_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->x:Lcom/mediacanvas/AndroidLDP/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->x:Lcom/mediacanvas/AndroidLDP/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/a;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->x:Lcom/mediacanvas/AndroidLDP/a;

    :cond_2
    new-instance v0, Lcom/mediacanvas/AndroidLDP/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediacanvas/AndroidLDP/a;-><init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;B)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->x:Lcom/mediacanvas/AndroidLDP/a;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->x:Lcom/mediacanvas/AndroidLDP/a;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/mediacanvas/AndroidLDP/a;->a(Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->x:Lcom/mediacanvas/AndroidLDP/a;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move v0, v7

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetNonAutoSearchStepOne() IllegalStateException:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_3
    :goto_1
    move v0, v6

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetNonAutoSearchStepOne() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method public GetNonAutoSearchStepTwo(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p2, :cond_0

    const-string v2, ""

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->y:Lcom/mediacanvas/AndroidLDP/b;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->y:Lcom/mediacanvas/AndroidLDP/b;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/mediacanvas/AndroidLDP/b;->cancel(Z)Z

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->y:Lcom/mediacanvas/AndroidLDP/b;

    :cond_2
    new-instance v2, Lcom/mediacanvas/AndroidLDP/b;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/mediacanvas/AndroidLDP/b;-><init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;B)V

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->y:Lcom/mediacanvas/AndroidLDP/b;

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->y:Lcom/mediacanvas/AndroidLDP/b;

    invoke-virtual {v2, p2, p1}, Lcom/mediacanvas/AndroidLDP/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->y:Lcom/mediacanvas/AndroidLDP/b;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/mediacanvas/AndroidLDP/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public IsLyricsFromLocal(Ljava/lang/String;)I
    .locals 6

    const/4 v1, 0x3

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    if-eqz p1, :cond_2

    const-string v3, ""

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {p1}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->o(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0, p1, v2, v5}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Ljava/lang/String;ILjava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lldpsdksub/a/e;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->o(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {p1}, Lldpsdksub/a/a;->a(Ljava/lang/String;)[B

    move-result-object v3

    if-eqz v3, :cond_2

    array-length v4, v3

    if-lez v4, :cond_2

    array-length v4, v3

    invoke-static {v3, v4}, Lldpsdksub/a/d;->a([BI)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0, p1, v0, v5}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Ljava/lang/String;ILjava/lang/String;)V

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    array-length v0, v3

    invoke-static {v3, v0}, Lldpsdksub/a/a;->a([BI)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1, v1, v5}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Ljava/lang/String;ILjava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public IsLyricsFromSDK(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "IsLyricsFromSDK() strSongFilePath:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->h(Ljava/lang/String;)V

    :cond_2
    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->d:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->e:Ljava/lang/String;

    iput-object p3, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->f:Ljava/lang/String;

    iput-object p4, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->g:Ljava/lang/String;

    iput-wide p5, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->h:J

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c()V

    :cond_3
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n:Lcom/mediacanvas/AndroidLDP/e;

    invoke-virtual {v0}, Lcom/mediacanvas/AndroidLDP/e;->c()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->n:Lcom/mediacanvas/AndroidLDP/e;

    invoke-virtual {v0}, Lcom/mediacanvas/AndroidLDP/e;->e()Z

    goto :goto_1
.end method

.method public LoadLyrcisData(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->h(Ljava/lang/String;)V

    :cond_2
    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->A:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->z:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->d()V

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public OnClearData()V
    .locals 0

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->e()V

    return-void
.end method

.method public OnDeleteLyrics(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lldpsdksub/a/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->h(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public OnDestroyLDP()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    :cond_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->w:Ljava/util/HashMap;

    :cond_1
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    invoke-virtual {v0}, Lldpsdksub/f/a;->a()V

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i:Lldpsdksub/f/a;

    :cond_2
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->f()V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->b()V

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    :cond_3
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    invoke-virtual {v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->DestroyLDPScrollView()V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    invoke-virtual {v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->destroyDrawingCache()V

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    :cond_4
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    invoke-virtual {v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;->destroyDrawingCache()V

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    :cond_5
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->destroyDrawingCache()V

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    :cond_6
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->o:Lcom/mediacanvas/AndroidLDP/f;

    if-eqz v0, :cond_7

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->o:Lcom/mediacanvas/AndroidLDP/f;

    :cond_7
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->q:Lcom/mediacanvas/AndroidLDP/c;

    if-eqz v0, :cond_8

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->q:Lcom/mediacanvas/AndroidLDP/c;

    :cond_8
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->x:Lcom/mediacanvas/AndroidLDP/a;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->x:Lcom/mediacanvas/AndroidLDP/a;

    invoke-virtual {v0, v2}, Lcom/mediacanvas/AndroidLDP/a;->cancel(Z)Z

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->x:Lcom/mediacanvas/AndroidLDP/a;

    :cond_9
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->y:Lcom/mediacanvas/AndroidLDP/b;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->y:Lcom/mediacanvas/AndroidLDP/b;

    invoke-virtual {v0, v2}, Lcom/mediacanvas/AndroidLDP/b;->cancel(Z)Z

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->y:Lcom/mediacanvas/AndroidLDP/b;

    :cond_a
    return-void
.end method

.method public OnPauseLDP(Z)V
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0, p1}, Lldpsdksub/g/d;->a(Z)V

    :cond_0
    return-void
.end method

.method public OnStartLDP()V
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->B:Z

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->e()V

    :cond_0
    return-void
.end method

.method public OnStopLDP()V
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->B:Z

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->f()V

    :cond_0
    return-void
.end method

.method public SetLDPAutoScrollTime(I)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->r:Z

    if-eqz v1, :cond_0

    if-nez p1, :cond_2

    const/4 p1, 0x2

    :cond_2
    mul-int/lit16 v0, p1, 0x3e8

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    invoke-virtual {v1, v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->SetDelayTimeOfScroll(I)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public SetLDPListener(Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0, p1}, Lldpsdksub/g/d;->a(Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->SetOnLDPClickListener(Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;)V

    :cond_2
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;->SetOnLDPClickListener(Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;)V

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public SetLDPParentView(Landroid/view/ViewGroup;IIIIZ)Z
    .locals 8

    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v2, -0x1

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-nez v1, :cond_0

    move v7, v0

    :goto_0
    return v7

    :cond_0
    iput-boolean p6, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->r:Z

    if-nez p1, :cond_1

    move v7, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    if-le p5, v0, :cond_3

    move v5, v7

    :goto_1
    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->r:Z

    if-eqz v0, :cond_2

    new-instance v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;->SetOnLDPClickListener(Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    invoke-virtual {v0, v7}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;->setOrientation(I)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    invoke-virtual {v0, p3}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;->setMinimumHeight(I)V

    new-instance v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->r:Z

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->SetScrollMode(Z)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->t:Lcom/mediacanvas/AndroidLDP/View/MC_LDPLayOut;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->s:Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->SetLDPMainMgr(Lcom/mediacanvas/AndroidLDP/Mgr/a;)V

    :goto_2
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->SetLDPMainMgr(Lcom/mediacanvas/AndroidLDP/Mgr/a;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->v:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->SetOnLDPClickListener(Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget-boolean v6, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->r:Z

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v6}, Lldpsdksub/g/d;->a(Landroid/view/View;IIIIZ)Z

    goto/16 :goto_0

    :cond_2
    new-instance v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->r:Z

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->SetScrollMode(Z)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v0, p3}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->setMinimumHeight(I)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_2

    :cond_3
    move v5, p5

    goto/16 :goto_1
.end method

.method public SetLDPTextStyle(IIILandroid/graphics/Typeface;)Z
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0, p1, p2, p3, p4}, Lldpsdksub/g/d;->a(IIILandroid/graphics/Typeface;)Z

    move-result v0

    goto :goto_0
.end method

.method public SetLDPVisible(I)Z
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->r:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    if-eqz v1, :cond_0

    if-ne p1, v3, :cond_3

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v0, p1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->setVisibility(I)V

    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    if-nez p1, :cond_4

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v0, p1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    if-ne p1, v2, :cond_2

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v0, p1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->setVisibility(I)V

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    if-eqz v1, :cond_0

    if-ne p1, v3, :cond_6

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v0, p1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->setVisibility(I)V

    goto :goto_1

    :cond_6
    if-nez p1, :cond_7

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v0, p1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->setVisibility(I)V

    goto :goto_1

    :cond_7
    if-ne p1, v2, :cond_2

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->u:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v0, p1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->setVisibility(I)V

    goto :goto_1
.end method

.method public SetPauseTimeForLDB(I)V
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b:Lcom/mediacanvas/AndroidLDP/Mgr/a;

    iget-object v0, v0, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0, p1}, Lldpsdksub/g/d;->b(I)V

    :cond_0
    return-void
.end method

.method public setNetwork(I)V
    .locals 1

    iput p1, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->j:I

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->j:I

    invoke-direct {p0, v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(I)V

    return-void
.end method
