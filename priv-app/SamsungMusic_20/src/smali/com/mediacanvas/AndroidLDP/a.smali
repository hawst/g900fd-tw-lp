.class final Lcom/mediacanvas/AndroidLDP/a;
.super Landroid/os/AsyncTask;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:I

.field private e:Ljava/util/ArrayList;

.field private synthetic f:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;


# direct methods
.method private constructor <init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/a;->f:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/a;->b:Ljava/lang/String;

    iput v1, p0, Lcom/mediacanvas/AndroidLDP/a;->c:I

    iput v1, p0, Lcom/mediacanvas/AndroidLDP/a;->d:I

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/a;->e:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/a;-><init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/a;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediacanvas/AndroidLDP/a;->b:Ljava/lang/String;

    iput p3, p0, Lcom/mediacanvas/AndroidLDP/a;->c:I

    iput p4, p0, Lcom/mediacanvas/AndroidLDP/a;->d:I

    iput-object p5, p0, Lcom/mediacanvas/AndroidLDP/a;->e:Ljava/util/ArrayList;

    return-void
.end method

.method protected final varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/a;->f:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/a;->f:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;I)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/a;->f:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "NO"

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/a;->f:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->g(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "NO"

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/a;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/a;->b:Ljava/lang/String;

    iget v2, p0, Lcom/mediacanvas/AndroidLDP/a;->c:I

    iget v3, p0, Lcom/mediacanvas/AndroidLDP/a;->d:I

    iget-object v4, p0, Lcom/mediacanvas/AndroidLDP/a;->f:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v4}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->h(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lldpsdksub/f/a;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/mediacanvas/AndroidLDP/Search/d;->a(Ljava/lang/String;Ljava/lang/String;IILldpsdksub/f/a;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/a;->e:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lldpsdksub/a/a;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const-string v0, "0"

    goto :goto_0

    :cond_4
    const-string v0, "NO"

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Ljava/lang/String;

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MCSearchStepOneTask() onPostExecute() 00 strResult:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/a;->f:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MCSearchStepOneTask() onPostExecute() 11 strResult:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/a;->f:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;->OnSearchStepOneListener(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/a;->f:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->i(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    move-result-object v0

    const-string v1, "0"

    invoke-interface {v0, v1}, Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;->OnSearchStepOneListener(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    goto :goto_0
.end method
