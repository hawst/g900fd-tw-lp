.class final Lcom/mediacanvas/AndroidLDP/d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Ljava/lang/Thread;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private synthetic d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;


# direct methods
.method private constructor <init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->a:Ljava/lang/Thread;

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mediacanvas/AndroidLDP/d;-><init>(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/d;->b:Ljava/lang/String;

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->a:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->a:Ljava/lang/Thread;

    :cond_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->a:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->a:Ljava/lang/Thread;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->a:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_1
    return-void
.end method

.method public final run()V
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    const-string v3, "NOLYRIC"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    const-string v3, "FAIL_LOAD"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GetLyricsThread() m_ThFilePath:"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediacanvas/AndroidLDP/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GetLyricsThread() m_ThResultIsLyrics:"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    const-string v3, "LDP"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->d(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_7

    array-length v2, v0

    if-lez v2, :cond_7

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    const-string v3, "NOLYRIC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    const-string v3, "FAIL_LOAD"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    const-string v3, "LDB"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v2, v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;[B)Z

    move-result v0

    :goto_1
    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v2}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->f(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/c;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->f(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/c;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/mediacanvas/AndroidLDP/c;->sendEmptyMessage(I)Z

    :cond_0
    :goto_2
    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    const-string v3, "MP3"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->e(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    const-string v3, "LDB"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->f(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v3, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v3}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)I

    move-result v3

    invoke-static {v0, v3}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;I)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->c(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-result v0

    if-eqz v0, :cond_8

    :try_start_2
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    const-string v3, "TTT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "contains(TTT) m_ThResultIsLyrics:"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    const-string v3, "TTT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v3, 0x0

    aget-object v3, v0, v3

    array-length v4, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_9

    const/4 v4, 0x1

    aget-object v0, v0, v4

    :goto_3
    if-eqz v3, :cond_8

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v5, p0, Lcom/mediacanvas/AndroidLDP/d;->b:Ljava/lang/String;

    invoke-static {v4, v5, v0, v3}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    iget-object v3, p0, Lcom/mediacanvas/AndroidLDP/d;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/mediacanvas/AndroidLDP/d;->c:Ljava/lang/String;

    invoke-static {v0, v3, v4}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->a(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;Ljava/lang/String;Ljava/lang/String;)[B
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    sget-object v3, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetLyricsThread() 00 NumberFormatException:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_0

    :catch_1
    move-exception v0

    sget-object v3, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetLyricsThread() 00 NullPointerException:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    sget-object v3, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetLyricsThread() 00 Exception:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    move-object v0, v2

    goto/16 :goto_0

    :catch_3
    move-exception v0

    sget-object v3, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetLyricsThread() 11 NullPointerException:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_0

    :catch_4
    move-exception v0

    sget-object v3, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetLyricsThread() 11 Exception:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v2, v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->b(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;[B)Z

    move-result v0

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/d;->d:Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;->f(Lcom/mediacanvas/AndroidLDP/Mediacanvas_LDPMain;)Lcom/mediacanvas/AndroidLDP/c;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/c;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    :cond_7
    move v0, v1

    goto/16 :goto_1

    :cond_8
    move-object v0, v2

    goto/16 :goto_0

    :cond_9
    move-object v0, v2

    goto/16 :goto_3

    :cond_a
    move-object v0, v2

    move-object v3, v2

    goto/16 :goto_3
.end method
