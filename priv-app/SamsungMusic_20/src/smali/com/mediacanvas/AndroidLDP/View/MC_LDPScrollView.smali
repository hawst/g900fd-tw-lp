.class public Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;
.super Landroid/widget/ScrollView;

# interfaces
.implements Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/Runnable;

.field private c:Lcom/mediacanvas/AndroidLDP/View/c;

.field private d:I

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->b:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->c:Lcom/mediacanvas/AndroidLDP/View/c;

    const/16 v0, 0x64

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->d:I

    const/16 v0, 0xbb8

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->e:I

    iput-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->f:Z

    iput-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->g:Z

    iput-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->h:Z

    iput v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->i:I

    iput v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->j:I

    iput v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->k:I

    iput v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->l:I

    iput v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->m:I

    iput v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->n:I

    iput v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->o:I

    iput v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->p:I

    return-void
.end method

.method static synthetic a(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)V
    .locals 5

    const/4 v1, -0x1

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->f:Z

    if-eqz v0, :cond_0

    iget v3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->l:I

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->m:I

    if-gtz v0, :cond_1

    move v0, v1

    :goto_0
    iget v2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->m:I

    if-gtz v2, :cond_3

    :goto_1
    sub-int v2, v3, v1

    add-int/lit8 v2, v2, -0x1

    iget v4, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->n:I

    if-gt v4, v0, :cond_5

    move v1, v0

    :goto_2
    sub-int v0, v1, v0

    iget v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->k:I

    mul-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->smoothScrollTo(II)V

    :cond_0
    return-void

    :cond_1
    rem-int/lit8 v2, v0, 0x2

    if-nez v2, :cond_2

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    rem-int/lit8 v1, v2, 0x2

    if-nez v1, :cond_4

    div-int/lit8 v1, v2, 0x2

    goto :goto_1

    :cond_4
    add-int/lit8 v1, v2, 0x1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_5
    iget v4, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->n:I

    add-int/2addr v1, v4

    if-gt v3, v1, :cond_6

    move v1, v2

    goto :goto_2

    :cond_6
    iget v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->n:I

    goto :goto_2
.end method

.method static synthetic a(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;I)V
    .locals 0

    iput p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->i:I

    return-void
.end method

.method static synthetic b(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->f:Z

    return v0
.end method

.method static synthetic c(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)I
    .locals 1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->j:I

    return v0
.end method

.method static synthetic d(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)I
    .locals 1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->i:I

    return v0
.end method

.method static synthetic e(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)Lcom/mediacanvas/AndroidLDP/View/c;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->c:Lcom/mediacanvas/AndroidLDP/View/c;

    return-object v0
.end method

.method static synthetic f(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->b:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic g(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)I
    .locals 1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->d:I

    return v0
.end method

.method static synthetic h(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->j:I

    return-void
.end method

.method static synthetic i(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)I
    .locals 1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->e:I

    return v0
.end method


# virtual methods
.method public DestroyLDPScrollView()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->c:Lcom/mediacanvas/AndroidLDP/View/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->c:Lcom/mediacanvas/AndroidLDP/View/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/c;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->c:Lcom/mediacanvas/AndroidLDP/View/c;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/c;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->c:Lcom/mediacanvas/AndroidLDP/View/c;

    :cond_0
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iput-object v2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->b:Ljava/lang/Runnable;

    :cond_1
    return-void
.end method

.method public GetScrollViewHegiht()I
    .locals 1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->p:I

    return v0
.end method

.method public GetScrollViewWidth()I
    .locals 1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->o:I

    return v0
.end method

.method public InitLineScroll(IIII)V
    .locals 1

    const/4 v0, 0x0

    iput p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->k:I

    iput p2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->l:I

    iput p4, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->m:I

    iput p3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->n:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->j:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->i:I

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->h:Z

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->g:Z

    invoke-virtual {p0, v0, v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->smoothScrollTo(II)V

    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->a:Ljava/lang/String;

    return-void
.end method

.method public SetDelayTimeOfScroll(I)V
    .locals 2

    const/4 v1, 0x0

    iput p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->e:I

    iput v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->j:I

    iput v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->i:I

    iput-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->h:Z

    iput-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->g:Z

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->e:I

    if-gez v0, :cond_0

    iput-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->f:Z

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->DestroyLDPScrollView()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->f:Z

    new-instance v0, Lcom/mediacanvas/AndroidLDP/View/c;

    invoke-direct {v0, p0}, Lcom/mediacanvas/AndroidLDP/View/c;-><init>(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->c:Lcom/mediacanvas/AndroidLDP/View/c;

    new-instance v0, Lcom/mediacanvas/AndroidLDP/View/b;

    invoke-direct {v0, p0}, Lcom/mediacanvas/AndroidLDP/View/b;-><init>(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->b:Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public SetLDPMainMgr(Lcom/mediacanvas/AndroidLDP/Mgr/a;)V
    .locals 1

    iget-object v0, p1, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0, p0}, Lldpsdksub/g/d;->a(Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;)V

    return-void
.end method

.method public SetLineChange(IIII)V
    .locals 2

    iput p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->k:I

    iput p2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->l:I

    iput p4, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->m:I

    iput p3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->n:I

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->c:Lcom/mediacanvas/AndroidLDP/View/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediacanvas/AndroidLDP/View/c;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public SetPassiveScroll(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->g:Z

    return-void
.end method

.method public SetTouchDown(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->h:Z

    return-void
.end method

.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->g:Z

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onLayout() changed:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onLayout() getWidth():"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onLayout() getHeight():"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->getHeight()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->o:I

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->p:I

    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onMeasure() widthMeasureSpec:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", heightMeasureSpec:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->onMeasure(II)V

    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->j:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->h:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->g:Z

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->c:Lcom/mediacanvas/AndroidLDP/View/c;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/mediacanvas/AndroidLDP/View/c;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->f:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->h:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->j:I

    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    iput-boolean v2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->h:Z

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iput-boolean v2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->h:Z

    goto :goto_0
.end method
