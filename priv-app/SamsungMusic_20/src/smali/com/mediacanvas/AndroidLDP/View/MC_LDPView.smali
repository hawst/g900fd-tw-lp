.class public Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;
.super Landroid/view/View;

# interfaces
.implements Lldpsdksub/c/a;


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:I

.field private c:I

.field private d:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

.field private e:I

.field private f:I

.field private g:Z

.field private h:Lldpsdksub/g/f;

.field private i:Lldpsdksub/g/g;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Landroid/graphics/Paint;

.field private o:Landroid/graphics/Paint;

.field private p:I

.field private q:I

.field private r:I

.field private s:Z

.field private t:Landroid/graphics/Bitmap;

.field private u:Landroid/graphics/Rect;

.field private v:Landroid/graphics/Rect;

.field private w:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->b:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->c:I

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->d:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->e:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->f:I

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->g:Z

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->h:Lldpsdksub/g/f;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->i:Lldpsdksub/g/g;

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->k:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->m:I

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->n:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->o:Landroid/graphics/Paint;

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->p:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->q:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->r:I

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->s:Z

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->t:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->u:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->v:Landroid/graphics/Rect;

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->w:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->b:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->c:I

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->d:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->e:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->f:I

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->g:Z

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->h:Lldpsdksub/g/f;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->i:Lldpsdksub/g/g;

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->k:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->m:I

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->n:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->o:Landroid/graphics/Paint;

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->p:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->q:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->r:I

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->s:Z

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->t:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->u:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->v:Landroid/graphics/Rect;

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->w:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->b:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->c:I

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->d:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->e:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->f:I

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->g:Z

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->h:Lldpsdksub/g/f;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->i:Lldpsdksub/g/g;

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->k:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->m:I

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->n:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->o:Landroid/graphics/Paint;

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->p:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->q:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->r:I

    iput-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->s:Z

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->t:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->u:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->v:Landroid/graphics/Rect;

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->w:I

    return-void
.end method

.method static synthetic a(Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;)Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->d:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    return-object v0
.end method

.method private c()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->k:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->m:I

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->t:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->u:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->v:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public DrawWaveForLDP(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;I)V
    .locals 1

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->t:Landroid/graphics/Bitmap;

    iput-object p2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->u:Landroid/graphics/Rect;

    iput-object p3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->v:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->v:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->invalidate(Landroid/graphics/Rect;)V

    return-void
.end method

.method public OnResizeView()V
    .locals 2

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->b:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->c:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "OnResizeView() m_nWidth:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "OnResizeView() m_nHeight:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    return-void
.end method

.method public OnResizeViewForNoLyrcis()V
    .locals 2

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->e:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->f:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public SetDrawDataPaint(Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 1

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->n:Landroid/graphics/Paint;

    iput-object p2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->o:Landroid/graphics/Paint;

    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->a:Ljava/lang/String;

    return-void
.end method

.method public SetDrawDataYpos(IIIZ)V
    .locals 0

    iput p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->q:I

    iput p2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->p:I

    iput p3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->r:I

    iput-boolean p4, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->s:Z

    return-void
.end method

.method public SetLDBWordData(Lldpsdksub/g/g;)V
    .locals 2

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->c()V

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->i:Lldpsdksub/g/g;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->i:Lldpsdksub/g/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->i:Lldpsdksub/g/g;

    invoke-virtual {v0}, Lldpsdksub/g/g;->c()I

    move-result v0

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    :cond_0
    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SetLDBWordData() m_nLineTotalCount:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    return-void
.end method

.method public SetLDPMainMgr(Lcom/mediacanvas/AndroidLDP/Mgr/a;)V
    .locals 1

    iget-object v0, p1, Lcom/mediacanvas/AndroidLDP/Mgr/a;->a:Lldpsdksub/g/d;

    invoke-virtual {v0, p0}, Lldpsdksub/g/d;->a(Lldpsdksub/c/a;)V

    new-instance v0, Lcom/mediacanvas/AndroidLDP/View/d;

    invoke-direct {v0, p0}, Lcom/mediacanvas/AndroidLDP/View/d;-><init>(Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;)V

    invoke-virtual {p0, v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public SetLDPWordData(Lldpsdksub/g/f;)V
    .locals 2

    invoke-direct {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->c()V

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->h:Lldpsdksub/g/f;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->h:Lldpsdksub/g/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->h:Lldpsdksub/g/f;

    invoke-virtual {v0}, Lldpsdksub/g/f;->c()I

    move-result v0

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    :cond_0
    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SetLDPWordData() m_nLineTotalCount:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    return-void
.end method

.method public SetLineNumDataForLDBView(III)V
    .locals 0

    iput p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->k:I

    iput p2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    iput p3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->m:I

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->invalidate()V

    return-void
.end method

.method public SetLineNumDataForLDPView(III)V
    .locals 0

    iput p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->k:I

    iput p2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    iput p3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->m:I

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->invalidate()V

    return-void
.end method

.method public SetOnLDPClickListener(Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;)V
    .locals 0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->d:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    return-void
.end method

.method public SetResizeValue(II)V
    .locals 0

    iput p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->b:I

    iput p2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->c:I

    return-void
.end method

.method public SetScreenArea(II)V
    .locals 0

    iput p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->e:I

    iput p2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->f:I

    return-void
.end method

.method public SetScrollMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->g:Z

    return-void
.end method

.method public final a()I
    .locals 1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->b:I

    return v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->c:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->h:Lldpsdksub/g/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->o:Landroid/graphics/Paint;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->n:Landroid/graphics/Paint;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    iget v2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    if-le v0, v2, :cond_0

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    :cond_0
    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->m:I

    iget v2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->k:I

    sub-int v2, v0, v2

    move v0, v1

    :goto_0
    iget v3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    iget v4, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->k:I

    add-int/2addr v4, v0

    if-gt v3, v4, :cond_5

    :cond_1
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->i:Lldpsdksub/g/g;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->o:Landroid/graphics/Paint;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->n:Landroid/graphics/Paint;

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    iget v2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    if-le v0, v2, :cond_2

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->j:I

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    :cond_2
    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->m:I

    iget v2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->k:I

    sub-int/2addr v0, v2

    :goto_1
    iget v2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->l:I

    iget v3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->k:I

    add-int/2addr v3, v1

    if-gt v2, v3, :cond_8

    :cond_3
    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->t:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->t:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_4

    iget v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->w:I

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->v:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->t:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->u:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->v:Landroid/graphics/Rect;

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_4
    return-void

    :cond_5
    iget-object v3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->h:Lldpsdksub/g/f;

    iget v4, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->k:I

    add-int/2addr v4, v0

    invoke-virtual {v3, v4}, Lldpsdksub/g/f;->b(I)Lldpsdksub/g/b;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lldpsdksub/g/b;->g()I

    move-result v4

    iget v5, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->p:I

    iget v6, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->r:I

    mul-int/2addr v6, v0

    add-int/2addr v5, v6

    invoke-virtual {v3}, Lldpsdksub/g/b;->e()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    const-string v6, ""

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    iget-boolean v6, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->s:Z

    if-le v2, v0, :cond_7

    int-to-float v4, v4

    int-to-float v6, v5

    iget-object v7, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :goto_2
    if-ne v2, v0, :cond_6

    iget v3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->q:I

    sub-int v3, v5, v3

    iput v3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->w:I

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_7
    int-to-float v4, v4

    int-to-float v6, v5

    iget-object v7, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_2

    :cond_8
    iget-object v2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->i:Lldpsdksub/g/g;

    iget v3, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->k:I

    add-int/2addr v3, v1

    invoke-virtual {v2, v3}, Lldpsdksub/g/g;->b(I)Lldpsdksub/g/c;

    move-result-object v2

    if-eqz v2, :cond_9

    iget v3, v2, Lldpsdksub/g/c;->d:I

    iget v4, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->p:I

    iget v5, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->r:I

    mul-int/2addr v5, v1

    add-int/2addr v4, v5

    iget-object v2, v2, Lldpsdksub/g/c;->c:Ljava/lang/String;

    if-eqz v2, :cond_9

    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    iget-boolean v5, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->s:Z

    if-lt v0, v1, :cond_a

    int-to-float v3, v3

    int-to-float v4, v4

    iget-object v5, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_9
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    :cond_a
    int-to-float v3, v3

    int-to-float v4, v4

    iget-object v5, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_3
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onLayout() changed:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onLayout() getWidth():"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onLayout() getHeight():"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->getHeight()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->g:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->b:I

    invoke-virtual {p0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->c:I

    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    const/4 v0, 0x0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    move p2, v0

    :goto_0
    :sswitch_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    move p1, v0

    :goto_1
    :sswitch_1
    invoke-virtual {p0, p1, p2}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->setMeasuredDimension(II)V

    return-void

    :sswitch_2
    iget p2, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->c:I

    goto :goto_0

    :sswitch_3
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    goto :goto_0

    :sswitch_4
    iget p1, p0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->b:I

    goto :goto_1

    :sswitch_5
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_2
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_4
        0x0 -> :sswitch_1
        0x40000000 -> :sswitch_5
    .end sparse-switch
.end method
