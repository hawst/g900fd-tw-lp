.class final Lcom/mediacanvas/AndroidLDP/View/c;
.super Landroid/os/Handler;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/c;->a:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/c;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/c;->a:Ljava/lang/ref/WeakReference;

    :cond_0
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/View/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->a(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)V

    invoke-virtual {v0, v2}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->SetPassiveScroll(Z)V

    invoke-virtual {v0, v2}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->SetTouchDown(Z)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;->a(Lcom/mediacanvas/AndroidLDP/View/MC_LDPScrollView;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
