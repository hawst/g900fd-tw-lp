.class public Lcom/mediacanvas/AndroidLDP/Search/e;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/mediacanvas/AndroidLDP/Search/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediacanvas/AndroidLDP/Search/e;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Lldpsdksub/f/c;
    .locals 3

    sget-object v0, Lcom/mediacanvas/AndroidLDP/Search/e;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GetData() strFullUrl:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :try_start_0
    new-instance v0, Lldpsdksub/f/c;

    invoke-direct {v0}, Lldpsdksub/f/c;-><init>()V

    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lldpsdksub/f/c;->a(Ljava/net/URL;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Search/e;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetData() MalformedURLException:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/mediacanvas/AndroidLDP/Search/e;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetData() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Lldpsdksub/f/a;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    const-string v2, "\\<\\#\\$\\!\\@\\%\\>"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    move v3, v0

    :goto_1
    array-length v2, v4

    if-gt v2, v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    aget-object v2, v4, v3

    const-string v5, "\\$\\@\\#\\%\\#\\$\\@"

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    packed-switch v3, :pswitch_data_0

    :cond_3
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :pswitch_0
    invoke-static {}, Lldpsdksub/f/a;->b()V

    invoke-static {}, Lldpsdksub/f/a;->c()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    :catch_0
    move-exception v1

    sget-object v2, Lcom/mediacanvas/AndroidLDP/Search/e;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OnParser() NullPointerException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    move v2, v0

    :goto_3
    :try_start_1
    array-length v6, v5

    if-le v6, v2, :cond_3

    aget-object v6, v5, v2

    invoke-virtual {p1, v6}, Lldpsdksub/f/a;->a(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :pswitch_2
    const/4 v2, 0x0

    aget-object v2, v5, v2

    invoke-virtual {p1, v2}, Lldpsdksub/f/a;->c(Ljava/lang/String;)V

    const/4 v2, 0x1

    aget-object v2, v5, v2

    invoke-virtual {p1, v2}, Lldpsdksub/f/a;->d(Ljava/lang/String;)V

    const/4 v2, 0x2

    aget-object v2, v5, v2

    invoke-virtual {p1, v2}, Lldpsdksub/f/a;->e(Ljava/lang/String;)V

    const/4 v2, 0x3

    aget-object v2, v5, v2

    invoke-virtual {p1, v2}, Lldpsdksub/f/a;->f(Ljava/lang/String;)V

    array-length v2, v5

    const/4 v6, 0x5

    if-ne v2, v6, :cond_3

    const/4 v2, 0x4

    aget-object v2, v5, v2

    invoke-virtual {p1, v2}, Lldpsdksub/f/a;->g(Ljava/lang/String;)V

    sget-object v2, Lcom/mediacanvas/AndroidLDP/Search/e;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "GetData() Sync HashTF URL:"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x4

    aget-object v5, v5, v6

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    sget-object v2, Lcom/mediacanvas/AndroidLDP/Search/e;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OnParser() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_3
    move v2, v0

    :goto_4
    :try_start_2
    array-length v6, v5

    if-le v6, v2, :cond_3

    aget-object v6, v5, v2

    invoke-virtual {p1, v6}, Lldpsdksub/f/a;->b(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :pswitch_4
    sget-object v2, Lcom/mediacanvas/AndroidLDP/Search/e;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "GetData() Sync HashTimeInterval:"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    aget-object v6, v5, v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v2, Lcom/mediacanvas/AndroidLDP/Search/e;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "GetData() Sync TagTimeInterval:"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x1

    aget-object v6, v5, v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v5, v2

    invoke-virtual {p1, v2}, Lldpsdksub/f/a;->h(Ljava/lang/String;)V

    const/4 v2, 0x1

    aget-object v2, v5, v2

    invoke-virtual {p1, v2}, Lldpsdksub/f/a;->i(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Lldpsdksub/f/a;)Z
    .locals 3

    const/4 v1, 0x0

    const/16 v0, 0x3e

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v2}, Lcom/mediacanvas/AndroidLDP/Search/e;->a(Ljava/lang/String;)Lldpsdksub/f/c;

    move-result-object v0

    if-nez v0, :cond_0

    const/16 v0, 0x4b

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v2}, Lcom/mediacanvas/AndroidLDP/Search/e;->a(Ljava/lang/String;)Lldpsdksub/f/c;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lldpsdksub/f/c;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0, p0}, Lcom/mediacanvas/AndroidLDP/Search/e;->a(Ljava/lang/String;Lldpsdksub/f/a;)Z

    move-result v0

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :array_0
    .array-data 1
        0x68t
        0x74t
        0x74t
        0x70t
        0x3at
        0x2ft
        0x2ft
        0x6dt
        0x63t
        0x61t
        0x6et
        0x76t
        0x61t
        0x73t
        0x32t
        0x31t
        0x2et
        0x63t
        0x61t
        0x66t
        0x65t
        0x32t
        0x34t
        0x2et
        0x63t
        0x6ft
        0x6dt
        0x2ft
        0x4ct
        0x59t
        0x52t
        0x49t
        0x43t
        0x53t
        0x2ft
        0x73t
        0x6dt
        0x5ft
        0x6ct
        0x79t
        0x72t
        0x69t
        0x63t
        0x73t
        0x5ft
        0x69t
        0x6et
        0x66t
        0x6ft
        0x5ft
        0x32t
        0x30t
        0x31t
        0x33t
        0x30t
        0x32t
        0x32t
        0x32t
        0x2et
        0x74t
        0x78t
        0x74t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x68t
        0x74t
        0x74t
        0x70t
        0x3at
        0x2ft
        0x2ft
        0x73t
        0x61t
        0x6dt
        0x73t
        0x75t
        0x6et
        0x67t
        0x74t
        0x78t
        0x74t
        0x2et
        0x77t
        0x69t
        0x6et
        0x65t
        0x65t
        0x2et
        0x6et
        0x65t
        0x74t
        0x2ft
        0x53t
        0x61t
        0x6dt
        0x73t
        0x75t
        0x6et
        0x67t
        0x5ft
        0x4ct
        0x44t
        0x42t
        0x5ft
        0x53t
        0x65t
        0x72t
        0x76t
        0x69t
        0x63t
        0x65t
        0x2ft
        0x73t
        0x6dt
        0x5ft
        0x6ct
        0x79t
        0x72t
        0x69t
        0x63t
        0x73t
        0x5ft
        0x69t
        0x6et
        0x66t
        0x6ft
        0x5ft
        0x32t
        0x30t
        0x31t
        0x33t
        0x30t
        0x32t
        0x32t
        0x32t
        0x2et
        0x74t
        0x78t
        0x74t
    .end array-data
.end method
