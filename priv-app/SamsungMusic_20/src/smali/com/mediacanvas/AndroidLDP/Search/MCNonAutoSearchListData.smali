.class public Lcom/mediacanvas/AndroidLDP/Search/MCNonAutoSearchListData;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Search/MCNonAutoSearchListData;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Search/MCNonAutoSearchListData;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediacanvas/AndroidLDP/Search/MCNonAutoSearchListData;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getArtist()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Search/MCNonAutoSearchListData;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Search/MCNonAutoSearchListData;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getLyricsId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Search/MCNonAutoSearchListData;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediacanvas/AndroidLDP/Search/MCNonAutoSearchListData;->b:Ljava/lang/String;

    return-object v0
.end method

.method public setData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mediacanvas/AndroidLDP/Search/MCNonAutoSearchListData;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediacanvas/AndroidLDP/Search/MCNonAutoSearchListData;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/mediacanvas/AndroidLDP/Search/MCNonAutoSearchListData;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/mediacanvas/AndroidLDP/Search/MCNonAutoSearchListData;->d:Ljava/lang/String;

    return-void
.end method
