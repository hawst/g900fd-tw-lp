.class public Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory;
.super Ljava/lang/Object;
.source "KLMSAlertBoxFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$2;,
        Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$DialogType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "KLMSAlertBoxFactory(): "

.field private static mDeviceHandler:Lcom/samsung/klmsagent/services/KLMSServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/klmsagent/services/KLMSServices",
            "<",
            "Lcom/samsung/klmsagent/beans/DeviceData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/samsung/klmsagent/services/i/DeviceServices;

    invoke-direct {v0}, Lcom/samsung/klmsagent/services/i/DeviceServices;-><init>()V

    sput-object v0, Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory;->mDeviceHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static getDialog(Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$DialogType;Landroid/content/Context;Landroid/app/Activity;)Lcom/samsung/klmsagent/components/ShowDialog;
    .locals 15
    .param p0, "type"    # Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$DialogType;
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 37
    const/4 v1, 0x0

    .line 38
    .local v1, "dialog":Lcom/samsung/klmsagent/components/ShowDialog;
    sget-object v12, Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$2;->$SwitchMap$com$samsung$klmsagent$components$KLMSAlertBoxFactory$DialogType:[I

    invoke-virtual {p0}, Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$DialogType;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_0

    .line 96
    const-string v12, "KLMSAlertBoxFactory(): getDialog() : Default"

    invoke-static {v12}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 99
    :goto_0
    return-object v1

    .line 40
    :pswitch_0
    const-string v12, "KLMSAlertBoxFactory(): Taking dialog with text message."

    invoke-static {v12}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 42
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getNotificationAppList()Ljava/util/List;

    move-result-object v7

    .line 43
    .local v7, "pkgList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 45
    .local v2, "handlers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/klmsagent/components/KLMOnClickListener;>;"
    new-instance v6, Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$1;

    invoke-direct {v6}, Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$1;-><init>()V

    .line 68
    .local v6, "listner":Lcom/samsung/klmsagent/components/KLMOnClickListener;
    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Lcom/samsung/klmsagent/components/KLMOnClickListener;->setActivity(Landroid/app/Activity;)V

    .line 70
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v12

    const v13, 0x7f06002b

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-interface {v2, v12, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const/4 v10, 0x0

    .line 73
    .local v10, "v":Landroid/view/View;
    const-string v12, "layout_inflater"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 74
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const/high16 v12, 0x7f030000

    const/4 v13, 0x0

    invoke-virtual {v4, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 75
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v11, "view":Ljava/util/List;, "Ljava/util/List<Landroid/text/Spanned;>;"
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v12

    const v13, 0x7f06002e

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    if-eqz v7, :cond_1

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_1

    .line 79
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 80
    .local v8, "pkgName":Ljava/lang/String;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_0

    .line 81
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f060033

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v8}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->convertFromPkgNameToAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 85
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v8    # "pkgName":Ljava/lang/String;
    :cond_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f060033

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f060030

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_2
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v12

    const v13, 0x7f06002f

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    const/high16 v12, 0x7f090000

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    .line 90
    .local v5, "list":Landroid/widget/ListView;
    new-instance v9, Landroid/widget/ArrayAdapter;

    const v12, 0x1090003

    move-object/from16 v0, p2

    invoke-direct {v9, v0, v12, v11}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 92
    .local v9, "stringArray":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Landroid/text/Spanned;>;"
    invoke-virtual {v5, v9}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 93
    new-instance v1, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;

    .end local v1    # "dialog":Lcom/samsung/klmsagent/components/ShowDialog;
    const v12, 0x7f06002a

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-direct {v1, v0, v12, v10, v2}, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;Ljava/util/Map;)V

    .line 94
    .restart local v1    # "dialog":Lcom/samsung/klmsagent/components/ShowDialog;
    goto/16 :goto_0

    .line 38
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
