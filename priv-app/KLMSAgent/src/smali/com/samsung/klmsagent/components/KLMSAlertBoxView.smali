.class public Lcom/samsung/klmsagent/components/KLMSAlertBoxView;
.super Ljava/lang/Object;
.source "KLMSAlertBoxView.java"

# interfaces
.implements Lcom/samsung/klmsagent/components/ShowDialog;


# instance fields
.field private dialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;Ljava/util/Map;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "contentView"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/klmsagent/components/KLMOnClickListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p4, "handlers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/klmsagent/components/KLMOnClickListener;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;->dialog:Landroid/app/AlertDialog;

    .line 18
    iget-object v3, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v3, p2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 19
    iget-object v3, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v3, p3}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 20
    const/4 v1, 0x0

    .line 21
    .local v1, "i":I
    invoke-interface {p4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 22
    .local v0, "clickHandler":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/klmsagent/components/KLMOnClickListener;>;"
    packed-switch v1, :pswitch_data_0

    .line 39
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 24
    :pswitch_0
    iget-object v5, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;->dialog:Landroid/app/AlertDialog;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v5, v3, v4}, Landroid/app/AlertDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 25
    iget-object v4, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;->dialog:Landroid/app/AlertDialog;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 26
    add-int/lit8 v1, v1, 0x1

    .line 27
    goto :goto_0

    .line 29
    :pswitch_1
    iget-object v5, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;->dialog:Landroid/app/AlertDialog;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v5, v3, v4}, Landroid/app/AlertDialog;->setButton2(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 30
    iget-object v4, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;->dialog:Landroid/app/AlertDialog;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 31
    add-int/lit8 v1, v1, 0x1

    .line 32
    goto :goto_0

    .line 34
    :pswitch_2
    iget-object v5, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;->dialog:Landroid/app/AlertDialog;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v5, v3, v4}, Landroid/app/AlertDialog;->setButton3(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 35
    iget-object v4, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;->dialog:Landroid/app/AlertDialog;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 36
    add-int/lit8 v1, v1, 0x1

    .line 37
    goto :goto_0

    .line 43
    .end local v0    # "clickHandler":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/klmsagent/components/KLMOnClickListener;>;"
    :cond_0
    return-void

    .line 22
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public dismissDialog()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "checking if we have a instance associated to this activity dialog."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;->dialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 54
    const-string v0, "Dismissing the dialog."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 59
    :goto_0
    return-void

    .line 58
    :cond_0
    const-string v0, "no dialog instance associated to this activity."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public show()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxView;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 48
    return-void
.end method
