.class final Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$1;
.super Lcom/samsung/klmsagent/components/KLMOnClickListener;
.source "KLMSAlertBoxFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory;->getDialog(Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$DialogType;Landroid/content/Context;Landroid/app/Activity;)Lcom/samsung/klmsagent/components/ShowDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/klmsagent/components/KLMOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 50
    const-string v0, "KLMSAlertBoxFactory(): Destroying Pop-up When user clicked OK Button."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$1;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 52
    return-void
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 60
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 61
    const-string v0, "KLMSAlertBoxFactory(): forwarding the key event to main activity."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$1;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p3}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    .line 64
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$1;->activity:Landroid/app/Activity;

    .line 56
    return-void
.end method
