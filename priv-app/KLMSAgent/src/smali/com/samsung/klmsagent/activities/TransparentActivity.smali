.class public Lcom/samsung/klmsagent/activities/TransparentActivity;
.super Landroid/app/Activity;
.source "TransparentActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/activities/TransparentActivity$PopUpHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TransparentActivity(): "

.field public static hanlder:Lcom/samsung/klmsagent/activities/TransparentActivity$PopUpHandler;


# instance fields
.field private dialog:Lcom/samsung/klmsagent/components/ShowDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/samsung/klmsagent/activities/TransparentActivity$PopUpHandler;

    invoke-direct {v0}, Lcom/samsung/klmsagent/activities/TransparentActivity$PopUpHandler;-><init>()V

    sput-object v0, Lcom/samsung/klmsagent/activities/TransparentActivity;->hanlder:Lcom/samsung/klmsagent/activities/TransparentActivity$PopUpHandler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 73
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    if-eqz p1, :cond_0

    .line 29
    invoke-static {p1}, Lcom/samsung/klmsagent/context/KLMSContextManager;->setContext(Landroid/content/Context;)V

    .line 30
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->setDataSource()V

    .line 32
    :cond_0
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransparentActivity(): on backButton pressed called in : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/klmsagent/activities/TransparentActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0, p0}, Lcom/samsung/klmsagent/activities/TransparentActivity;->onCompleterActivity(Landroid/app/Activity;)V

    .line 53
    return-void
.end method

.method onCompleterActivity(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/klmsagent/activities/TransparentActivity;->dialog:Lcom/samsung/klmsagent/components/ShowDialog;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/samsung/klmsagent/activities/TransparentActivity;->dialog:Lcom/samsung/klmsagent/components/ShowDialog;

    invoke-interface {v0}, Lcom/samsung/klmsagent/components/ShowDialog;->dismissDialog()V

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/klmsagent/activities/TransparentActivity;->finish()V

    .line 70
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lcom/samsung/klmsagent/activities/TransparentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/klmsagent/activities/TransparentActivity;->init(Landroid/content/Context;)V

    .line 39
    const v1, 0x7f030001

    invoke-virtual {p0, v1}, Lcom/samsung/klmsagent/activities/TransparentActivity;->setContentView(I)V

    .line 40
    const-string v1, "TransparentActivity(): Loading the activity."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 43
    sget-object v0, Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$DialogType;->View:Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$DialogType;

    .line 44
    .local v0, "type":Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$DialogType;
    invoke-virtual {p0}, Lcom/samsung/klmsagent/activities/TransparentActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory;->getDialog(Lcom/samsung/klmsagent/components/KLMSAlertBoxFactory$DialogType;Landroid/content/Context;Landroid/app/Activity;)Lcom/samsung/klmsagent/components/ShowDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/klmsagent/activities/TransparentActivity;->dialog:Lcom/samsung/klmsagent/components/ShowDialog;

    .line 45
    iget-object v1, p0, Lcom/samsung/klmsagent/activities/TransparentActivity;->dialog:Lcom/samsung/klmsagent/components/ShowDialog;

    invoke-interface {v1}, Lcom/samsung/klmsagent/components/ShowDialog;->show()V

    .line 46
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransparentActivity(): on stop called in : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/klmsagent/activities/TransparentActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 59
    invoke-static {}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->HomeButton()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/activities/TransparentActivity;->onCompleterActivity(Landroid/app/Activity;)V

    .line 62
    :cond_0
    return-void
.end method
