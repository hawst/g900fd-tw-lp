.class public Lcom/samsung/klmsagent/activities/TransparentActivity$PopUpHandler;
.super Landroid/os/Handler;
.source "TransparentActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/activities/TransparentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PopUpHandler"
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 78
    const-string v0, "TransparentActivity(): checking if there is any activity associated screen."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/samsung/klmsagent/activities/TransparentActivity$PopUpHandler;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 80
    const-string v0, "TransparentActivity(): Closing the activity now."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/samsung/klmsagent/activities/TransparentActivity$PopUpHandler;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/klmsagent/activities/TransparentActivity$PopUpHandler;->activity:Landroid/app/Activity;

    .line 84
    :cond_0
    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/klmsagent/activities/TransparentActivity$PopUpHandler;->activity:Landroid/app/Activity;

    .line 88
    return-void
.end method
