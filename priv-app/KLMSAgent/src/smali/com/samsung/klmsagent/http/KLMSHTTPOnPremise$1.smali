.class synthetic Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;
.super Ljava/lang/Object;
.source "KLMSHTTPOnPremise.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$samsung$klmsagent$http$CallState$ConnectionType:[I

.field static final synthetic $SwitchMap$com$samsung$klmsagent$util$KLMSConstant$HttpRetryFrequency:[I

.field static final synthetic $SwitchMap$com$samsung$klmsagent$util$KLMSConstant$KLMSRequestType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 274
    invoke-static {}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->values()[Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$http$CallState$ConnectionType:[I

    :try_start_0
    sget-object v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$http$CallState$ConnectionType:[I

    sget-object v1, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTP:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$http$CallState$ConnectionType:[I

    sget-object v1, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    .line 281
    :goto_1
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->values()[Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$HttpRetryFrequency:[I

    :try_start_2
    sget-object v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$HttpRetryFrequency:[I

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->FIVE_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    :try_start_3
    sget-object v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$HttpRetryFrequency:[I

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->THREE_MIN:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_3
    :try_start_4
    sget-object v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$HttpRetryFrequency:[I

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->FIFTEEN_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    .line 257
    :goto_4
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->values()[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$KLMSRequestType:[I

    :try_start_5
    sget-object v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$KLMSRequestType:[I

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ_POLICY:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    return-void

    :catch_0
    move-exception v0

    goto :goto_5

    .line 281
    :catch_1
    move-exception v0

    goto :goto_4

    :catch_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_2

    .line 274
    :catch_4
    move-exception v0

    goto :goto_1

    :catch_5
    move-exception v0

    goto :goto_0
.end method
