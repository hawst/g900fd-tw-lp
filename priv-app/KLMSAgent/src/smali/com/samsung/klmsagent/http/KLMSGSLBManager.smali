.class public Lcom/samsung/klmsagent/http/KLMSGSLBManager;
.super Ljava/lang/Object;
.source "KLMSGSLBManager.java"


# static fields
.field protected static final TAG:Ljava/lang/String; = "KLMSGSLBManager(): "


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static compriseRedirectionRequestJSON(Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;)Lorg/json/JSONObject;
    .locals 10
    .param p0, "redirectionRequestInfo"    # Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;

    .prologue
    .line 156
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 157
    .local v8, "redirectionRequestJSON":Lorg/json/JSONObject;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 158
    .local v6, "dataJSON":Lorg/json/JSONObject;
    const/4 v9, 0x0

    .line 161
    .local v9, "signedData":Ljava/lang/String;
    :try_start_0
    const-string v1, "deviceid"

    invoke-virtual {p0}, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->getDeviceID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 163
    invoke-virtual {p0}, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->getCountryISO()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 164
    const-string v1, "country_iso"

    invoke-virtual {p0}, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->getCountryISO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 166
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->getSalesCode()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 167
    const-string v1, "csc"

    invoke-virtual {p0}, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->getSalesCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 169
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->getMCC()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 170
    const-string v1, "mcc"

    invoke-virtual {p0}, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 173
    :cond_2
    new-instance v0, Lcom/samsung/klmsagent/security/SecureDataGenerator;

    invoke-direct {v0}, Lcom/samsung/klmsagent/security/SecureDataGenerator;-><init>()V

    .line 174
    .local v0, "secureDataGenerator":Lcom/samsung/klmsagent/security/SecureDataGenerator;
    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GSLB"

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/klmsagent/security/SecureDataGenerator;->getSignatureData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v9

    .line 177
    const-string v1, "data"

    invoke-virtual {v6, v1, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 178
    const-string v1, "signature"

    invoke-virtual {v6, v1, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 194
    .end local v0    # "secureDataGenerator":Lcom/samsung/klmsagent/security/SecureDataGenerator;
    :goto_0
    return-object v6

    .line 180
    :catch_0
    move-exception v7

    .line 181
    .local v7, "e":Lorg/json/JSONException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compriseRedirectionRequestJSON : JSONException was occurred : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 182
    const-string v1, "Error Occured."

    invoke-static {v1, v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 184
    const/4 v8, 0x0

    .line 185
    const/4 v6, 0x0

    .line 192
    goto :goto_0

    .line 186
    .end local v7    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v7

    .line 187
    .local v7, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compriseRedirectionRequestJSON : Exception was occurred : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 188
    const-string v1, "Error Occured."

    invoke-static {v1, v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 190
    const/4 v8, 0x0

    .line 191
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static getKLMSServerAddr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0, "connectionType"    # Ljava/lang/String;
    .param p1, "containerType"    # Ljava/lang/String;

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 106
    .local v0, "Address":Ljava/lang/String;
    const/4 v7, 0x0

    .line 107
    .local v7, "port":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "KLMSGSLBManager(): getKLMSServerAddr() : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " | "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 109
    const/4 v9, 0x0

    .line 111
    .local v9, "savedData":Ljava/lang/String;
    :try_start_0
    const-string v10, "b2c"

    invoke-virtual {p1, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 112
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getB2CKLMSServerAddr()Ljava/lang/String;

    move-result-object v9

    .line 119
    :goto_0
    const/4 v2, 0x0

    .line 121
    .local v2, "KLMSServerAddr":Lorg/json/JSONArray;
    if-nez v9, :cond_2

    .line 122
    new-instance v10, Ljava/lang/Exception;

    invoke-direct {v10}, Ljava/lang/Exception;-><init>()V

    throw v10
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    .end local v2    # "KLMSServerAddr":Lorg/json/JSONArray;
    :catch_0
    move-exception v5

    .line 147
    .local v5, "e":Ljava/lang/Exception;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getKLMSServerAddr().RedirectionDataManager has Exception.\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 148
    const/4 v0, 0x0

    move-object v1, v0

    .line 151
    .end local v0    # "Address":Ljava/lang/String;
    .end local v5    # "e":Ljava/lang/Exception;
    .local v1, "Address":Ljava/lang/String;
    :goto_1
    return-object v1

    .line 113
    .end local v1    # "Address":Ljava/lang/String;
    .restart local v0    # "Address":Ljava/lang/String;
    :cond_0
    :try_start_1
    const-string v10, "onPrem"

    invoke-virtual {p1, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 114
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremServerAddr()Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 116
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getKLMSServerAddr()Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 125
    .restart local v2    # "KLMSServerAddr":Lorg/json/JSONArray;
    :cond_2
    new-instance v2, Lorg/json/JSONArray;

    .end local v2    # "KLMSServerAddr":Lorg/json/JSONArray;
    invoke-direct {v2, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 126
    .restart local v2    # "KLMSServerAddr":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 127
    .local v4, "KLMSServerAddrSize":I
    const/4 v6, 0x0

    .local v6, "idx":I
    :goto_2
    if-ge v6, v4, :cond_3

    .line 128
    invoke-virtual {v2, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 129
    .local v3, "KLMSServerAddrObject":Lorg/json/JSONObject;
    const-string v10, "protocol"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 131
    .local v8, "protocol":Ljava/lang/String;
    const-string v10, "HTTP"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const-string v10, "http"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 132
    const-string v10, "url"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    const-string v10, "port"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 134
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 145
    .end local v3    # "KLMSServerAddrObject":Lorg/json/JSONObject;
    .end local v8    # "protocol":Ljava/lang/String;
    :cond_3
    :goto_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "KLMSGSLBManager(): getKLMSServerAddr().RedirectionDataManager : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    move-object v1, v0

    .line 151
    .end local v0    # "Address":Ljava/lang/String;
    .restart local v1    # "Address":Ljava/lang/String;
    goto :goto_1

    .line 138
    .end local v1    # "Address":Ljava/lang/String;
    .restart local v0    # "Address":Ljava/lang/String;
    .restart local v3    # "KLMSServerAddrObject":Lorg/json/JSONObject;
    .restart local v8    # "protocol":Ljava/lang/String;
    :cond_4
    const-string v10, "HTTPS"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    const-string v10, "https"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 139
    const-string v10, "url"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 140
    const-string v10, "port"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 141
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 142
    goto :goto_3

    .line 127
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2
.end method

.method public static postProcessResponse(Ljava/lang/String;Landroid/os/Message;)V
    .locals 11
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "reqObj"    # Landroid/os/Message;

    .prologue
    const/16 v1, 0x22b8

    const/16 v2, 0x384

    const/16 v3, 0x320

    const/16 v0, 0xc8

    .line 198
    const-string v4, "KLMSGSLBManager(): postProcessResponse().START"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 199
    if-eqz p0, :cond_8

    .line 201
    const/4 v9, 0x0

    .line 202
    .local v9, "service":Ljava/lang/String;
    const/4 v7, 0x0

    .line 204
    .local v7, "endpoint":Lorg/json/JSONArray;
    :try_start_0
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 205
    .local v8, "responseJSONData":Lorg/json/JSONObject;
    const-string v4, "service"

    invoke-virtual {v8, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 206
    const-string v4, "endpoint"

    invoke-virtual {v8, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 207
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "KLMSGSLBManager(): postProcessResponse.GSLB type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v10, "type"

    invoke-virtual {v5, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 208
    const-string v4, "klms"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 209
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "onPrem"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 210
    const-string v4, "KLMSGSLBManager(): Set Onprem Server Address."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 211
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setOnPremServerAddr(Lorg/json/JSONArray;)V

    .line 220
    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 221
    const-string v4, "KLMSGSLBManager(): Loading the policy files now."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 222
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->setKLMServerPolicy(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getWaitForGslbResponse()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    sget-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ_PROCESS:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v0

    iput v0, p1, Landroid/os/Message;->what:I

    .line 257
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 260
    :cond_2
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getWaitForGslbResponseForDeviceReg()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 261
    const-string v0, "KLMSGSLBManager(): Pending request is device register , sending to handler to process it."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 262
    sget-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ_PROCESS_DEVICE_REG:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v0

    iput v0, p1, Landroid/os/Message;->what:I

    .line 263
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 274
    :cond_3
    const-string v0, "KLMSGSLBManager(): postProcessResponse().END"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 275
    .end local v7    # "endpoint":Lorg/json/JSONArray;
    .end local v8    # "responseJSONData":Lorg/json/JSONObject;
    .end local v9    # "service":Ljava/lang/String;
    :cond_4
    :goto_1
    return-void

    .line 213
    .restart local v7    # "endpoint":Lorg/json/JSONArray;
    .restart local v8    # "responseJSONData":Lorg/json/JSONObject;
    .restart local v9    # "service":Ljava/lang/String;
    :cond_5
    :try_start_1
    const-string v4, "KLMSGSLBManager(): Set B2B Server Address."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 214
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setKLMSServerAddr(Lorg/json/JSONArray;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 224
    .end local v8    # "responseJSONData":Lorg/json/JSONObject;
    :catch_0
    move-exception v6

    .line 225
    .local v6, "e":Ljava/lang/Exception;
    const-string v4, "Error Occured."

    invoke-static {v4, v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 226
    const-string v4, "KLMSGSLBManager(): GSLB failed. Had an exception in parsing."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 227
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "type"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "b2b"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 228
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "packageName"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 229
    const-string v4, "fail"

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v10, "packageName"

    invoke-virtual {v5, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 234
    :cond_6
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponse(Ljava/lang/Boolean;)V

    .line 235
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponseForDeviceReg(Ljava/lang/Boolean;)V

    goto :goto_1

    .line 216
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v8    # "responseJSONData":Lorg/json/JSONObject;
    :cond_7
    :try_start_2
    const-string v4, "klmb2c"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 217
    const-string v4, "KLMSGSLBManager(): Set B2C Server Address."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 218
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2CKLMSServerAddr(Lorg/json/JSONArray;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 240
    .end local v7    # "endpoint":Lorg/json/JSONArray;
    .end local v8    # "responseJSONData":Lorg/json/JSONObject;
    .end local v9    # "service":Ljava/lang/String;
    :cond_8
    const-string v4, "KLMSGSLBManager(): GSLB failed."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "type"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "b2b"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 242
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "packageName"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 243
    const-string v4, "fail"

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v10, "packageName"

    invoke-virtual {v5, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_9
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponse(Ljava/lang/Boolean;)V

    .line 249
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponseForDeviceReg(Ljava/lang/Boolean;)V

    goto/16 :goto_1
.end method

.method public static postProcessResponseForPolicy(Ljava/lang/String;Landroid/os/Message;)V
    .locals 6
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "reqObj"    # Landroid/os/Message;

    .prologue
    .line 279
    if-eqz p0, :cond_0

    .line 281
    const/4 v2, 0x0

    .line 283
    .local v2, "policy":Lorg/json/JSONObject;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 284
    .local v3, "responseJSONData":Lorg/json/JSONObject;
    const-string v4, "klm"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 285
    const-string v4, "kad"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 286
    .local v1, "kadUrl":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setOnPremiseKADUrlForCA(Ljava/lang/String;)V

    .line 287
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setOnPremisePolicy(Lorg/json/JSONObject;)V

    .line 288
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "KLMSGSLBManager(): postProcessResponseForPolicy() - Policy: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " | KADUrl: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    .end local v1    # "kadUrl":Ljava/lang/String;
    .end local v2    # "policy":Lorg/json/JSONObject;
    .end local v3    # "responseJSONData":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 289
    .restart local v2    # "policy":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 290
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "Error Occured in getting the policy."

    invoke-static {v4, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 294
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "policy":Lorg/json/JSONObject;
    :cond_0
    const-string v4, "KLMSGSLBManager(): GSLB failed to get the policy. postProcessResponseForPolicy.result is NULL."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setKLMSServerAddr(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 11
    .param p0, "extras"    # Landroid/os/Bundle;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 39
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "KLMSGSLBManager(): setKLMSServerAddr().RedirectionDataManager: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 40
    new-instance v3, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;

    invoke-direct {v3}, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;-><init>()V

    .line 42
    .local v3, "redirectionRequestInfo":Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getRandomNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->setDeviceID(Ljava/lang/String;)V

    .line 43
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getSalesCodeFromCSC()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->setSalesCode(Ljava/lang/String;)V

    .line 44
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryISOFromCSC()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->setCountryISO(Ljava/lang/String;)V

    .line 45
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->setMCC(Ljava/lang/String;)V

    .line 47
    const/4 v1, 0x0

    .line 49
    .local v1, "onPrem":Z
    invoke-static {v3}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->compriseRedirectionRequestJSON(Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;)Lorg/json/JSONObject;

    move-result-object v2

    .line 50
    .local v2, "redirectionJSON":Lorg/json/JSONObject;
    if-nez v2, :cond_0

    .line 51
    const-string v6, "KLMSGSLBManager(): error in creating json for gslb"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 85
    :goto_0
    return-void

    .line 55
    :cond_0
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 56
    .local v0, "msg":Landroid/os/Message;
    sget-object v6, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual {v6}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v6

    iput v6, v0, Landroid/os/Message;->what:I

    .line 57
    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 58
    if-eqz p0, :cond_2

    .line 59
    invoke-virtual {v0, p0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 60
    const-string v6, "packageName"

    invoke-virtual {p0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 61
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "packageName"

    const-string v8, "packageName"

    invoke-virtual {p0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_1
    const-string v6, "onPrem"

    invoke-virtual {p0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 64
    const/4 v1, 0x1

    .line 68
    :cond_2
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "type"

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    new-instance v5, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    invoke-direct {v5, v0}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 70
    .local v5, "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    new-instance v4, Lcom/samsung/klmsagent/http/CallState;

    invoke-direct {v4}, Lcom/samsung/klmsagent/http/CallState;-><init>()V

    .line 72
    .local v4, "state":Lcom/samsung/klmsagent/http/CallState;
    if-eqz v1, :cond_3

    .line 73
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "onPrem"

    invoke-virtual {v6, v7, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 77
    :goto_1
    sget-object v6, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    invoke-virtual {v4, v6}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 78
    sget-object v6, Lcom/samsung/klmsagent/http/CallState$CallType;->GSLB:Lcom/samsung/klmsagent/http/CallState$CallType;

    invoke-virtual {v4, v6}, Lcom/samsung/klmsagent/http/CallState;->setCallType(Lcom/samsung/klmsagent/http/CallState$CallType;)V

    .line 79
    invoke-virtual {v5, v4}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->setCall(Lcom/samsung/klmsagent/http/CallState;)V

    .line 80
    const-string v6, "b2c"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 81
    new-array v6, v10, [Ljava/lang/String;

    const-string v7, "/KnoxGSLB/lookup/klmb2c"

    aput-object v7, v6, v9

    invoke-virtual {v5, v6}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 75
    :cond_3
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "onPrem"

    invoke-virtual {v6, v7, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 83
    :cond_4
    new-array v6, v10, [Ljava/lang/String;

    const-string v7, "/KnoxGSLB/lookup/klms"

    aput-object v7, v6, v9

    invoke-virtual {v5, v6}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method

.method public static setKLMServerPolicy(Landroid/os/Bundle;)V
    .locals 6
    .param p0, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 88
    const-string v3, "KLMSGSLBManager(): setKLMSServerPolicy()"

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 90
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 91
    .local v0, "msg":Landroid/os/Message;
    sget-object v3, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ_POLICY:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual {v3}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v3

    iput v3, v0, Landroid/os/Message;->what:I

    .line 92
    if-eqz p0, :cond_0

    .line 93
    invoke-virtual {v0, p0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 95
    :cond_0
    new-instance v2, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;

    invoke-direct {v2, v0}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;-><init>(Landroid/os/Message;)V

    .line 96
    .local v2, "task":Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;
    new-instance v1, Lcom/samsung/klmsagent/http/CallState;

    invoke-direct {v1}, Lcom/samsung/klmsagent/http/CallState;-><init>()V

    .line 97
    .local v1, "state":Lcom/samsung/klmsagent/http/CallState;
    sget-object v3, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    invoke-virtual {v1, v3}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 98
    sget-object v3, Lcom/samsung/klmsagent/http/CallState$CallType;->GSLB:Lcom/samsung/klmsagent/http/CallState$CallType;

    invoke-virtual {v1, v3}, Lcom/samsung/klmsagent/http/CallState;->setCallType(Lcom/samsung/klmsagent/http/CallState$CallType;)V

    .line 99
    invoke-virtual {v2, v1}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->setCall(Lcom/samsung/klmsagent/http/CallState;)V

    .line 100
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "/KnoxGSLB/policy"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 101
    return-void
.end method
