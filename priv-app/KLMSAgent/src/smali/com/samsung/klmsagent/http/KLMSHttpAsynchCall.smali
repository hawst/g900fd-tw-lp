.class public Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
.super Landroid/os/AsyncTask;
.source "KLMSHttpAsynchCall.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final GSLB_CONTENT_TYPE:Ljava/lang/String; = "application/json"

.field protected static final HTTPS_FAILED_CODE:I = -0x1869f

.field private static final KLMS_CONTENT_TYPE:Ljava/lang/String; = "application/knox-crypto-stream"

.field protected static final TAG:Ljava/lang/String; = "KLMSHttpAsynchCall(): "


# instance fields
.field private InitVector:Ljavax/crypto/spec/IvParameterSpec;

.field protected call:Lcom/samsung/klmsagent/http/CallState;

.field protected counter:I

.field private mSecureRandom:Ljava/security/SecureRandom;

.field protected reqObj:Landroid/os/Message;

.field private secretKey:Ljava/security/Key;

.field protected test:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Message;)V
    .locals 2
    .param p1, "reqObj"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 62
    iput-object v1, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->mSecureRandom:Ljava/security/SecureRandom;

    .line 63
    iput-object v1, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->secretKey:Ljava/security/Key;

    .line 64
    iput-object v1, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->counter:I

    .line 69
    iput-object v1, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->test:Ljava/lang/String;

    .line 73
    iput-object p1, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    .line 74
    new-instance v0, Lcom/samsung/klmsagent/http/CallState;

    invoke-direct {v0}, Lcom/samsung/klmsagent/http/CallState;-><init>()V

    iput-object v0, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    .line 75
    return-void
.end method

.method protected static convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 12
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 474
    const-string v6, "KLMSHttpAsynchCall(): In HttpUtils.convertStreamToString()"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 475
    const/4 v4, 0x0

    .line 477
    .local v4, "sb":Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 478
    .local v1, "inStream":Ljava/io/InputStreamReader;
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 479
    .local v3, "reader":Ljava/io/BufferedReader;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 480
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 481
    .local v2, "line":Ljava/lang/String;
    :goto_0
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 482
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 484
    :catch_0
    move-exception v0

    move-object v4, v5

    .line 485
    .end local v1    # "inStream":Ljava/io/InputStreamReader;
    .end local v2    # "line":Ljava/lang/String;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .local v0, "e":Ljava/io/IOException;
    .restart local v4    # "sb":Ljava/lang/StringBuilder;
    :goto_1
    :try_start_2
    new-instance v6, Ljava/lang/Exception;

    const-string v7, "class name: %s method name: %s at timestamp: %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "KLMSHttpAsynchCall(): "

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "httpCall()"

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 488
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v6

    .end local v4    # "sb":Ljava/lang/StringBuilder;
    .restart local v1    # "inStream":Ljava/io/InputStreamReader;
    .restart local v2    # "line":Ljava/lang/String;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 490
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 488
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .restart local v4    # "sb":Ljava/lang/StringBuilder;
    goto :goto_2

    .line 484
    .end local v1    # "inStream":Ljava/io/InputStreamReader;
    .end local v2    # "line":Ljava/lang/String;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private getDecryptIV()Ljavax/crypto/spec/IvParameterSpec;
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x0

    .line 534
    iget-object v4, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    if-eqz v4, :cond_1

    .line 535
    const/4 v2, 0x0

    .line 537
    .local v2, "mResult":Ljavax/crypto/spec/IvParameterSpec;
    new-instance v4, Ljava/math/BigInteger;

    iget-object v5, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {v5}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/math/BigInteger;-><init>([B)V

    new-instance v5, Ljava/math/BigInteger;

    const-string v6, "1"

    invoke-direct {v5, v6}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    .line 538
    .local v1, "mBigInteger":Ljava/math/BigInteger;
    invoke-virtual {v1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v4

    invoke-static {v4, v8}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    .line 539
    .local v3, "src":[B
    new-array v0, v8, [B

    .line 541
    .local v0, "dst":[B
    array-length v4, v3

    array-length v5, v0

    if-gt v4, v5, :cond_0

    .line 542
    array-length v4, v0

    array-length v5, v3

    sub-int/2addr v4, v5

    array-length v5, v3

    invoke-static {v3, v7, v0, v4, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 546
    :goto_0
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    .end local v2    # "mResult":Ljavax/crypto/spec/IvParameterSpec;
    invoke-direct {v2, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 552
    .end local v0    # "dst":[B
    .end local v1    # "mBigInteger":Ljava/math/BigInteger;
    .end local v3    # "src":[B
    :goto_1
    return-object v2

    .line 544
    .restart local v0    # "dst":[B
    .restart local v1    # "mBigInteger":Ljava/math/BigInteger;
    .restart local v2    # "mResult":Ljavax/crypto/spec/IvParameterSpec;
    .restart local v3    # "src":[B
    :cond_0
    invoke-static {v3, v7, v0, v7, v8}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_0

    .line 551
    .end local v0    # "dst":[B
    .end local v1    # "mBigInteger":Ljava/math/BigInteger;
    .end local v2    # "mResult":Ljavax/crypto/spec/IvParameterSpec;
    .end local v3    # "src":[B
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/security/KLMSCipherV3;->getInstance()Lcom/samsung/klmsagent/security/KLMSCipherV3;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/klmsagent/security/KLMSCipherV3;->getInitialVector()Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    .line 552
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    goto :goto_1
.end method

.method private getEncryptIV()Ljavax/crypto/spec/IvParameterSpec;
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    .line 529
    :goto_0
    return-object v0

    .line 528
    :cond_0
    invoke-static {}, Lcom/samsung/klmsagent/security/KLMSCipherV3;->getInstance()Lcom/samsung/klmsagent/security/KLMSCipherV3;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/security/KLMSCipherV3;->getInitialVector()Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    .line 529
    iget-object v0, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    goto :goto_0
.end method

.method private getSessionKey()Ljava/security/Key;
    .locals 4

    .prologue
    .line 510
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->secretKey:Ljava/security/Key;

    if-eqz v2, :cond_0

    .line 511
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->secretKey:Ljava/security/Key;

    .line 522
    :goto_0
    return-object v2

    .line 513
    :cond_0
    const/4 v1, 0x0

    .line 514
    .local v1, "keyGen":Ljavax/crypto/KeyGenerator;
    new-instance v2, Ljava/security/SecureRandom;

    invoke-direct {v2}, Ljava/security/SecureRandom;-><init>()V

    iput-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->mSecureRandom:Ljava/security/SecureRandom;

    .line 516
    :try_start_0
    const-string v2, "AES"

    invoke-static {v2}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v1

    .line 517
    const/16 v2, 0x100

    iget-object v3, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->mSecureRandom:Ljava/security/SecureRandom;

    invoke-virtual {v1, v2, v3}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 518
    invoke-virtual {v1}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->secretKey:Ljava/security/Key;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 522
    :goto_1
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->secretKey:Ljava/security/Key;

    goto :goto_0

    .line 519
    :catch_0
    move-exception v0

    .line 520
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 55
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "objList"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 79
    const/4 v1, 0x0

    .line 82
    .local v1, "response":Ljava/lang/String;
    if-eqz p1, :cond_0

    array-length v2, p1

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 83
    :cond_0
    new-instance v2, Ljava/security/InvalidParameterException;

    const-string v3, "Null Parameter or Empty."

    invoke-direct {v2, v3}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 86
    :cond_1
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KLMSHttpAsynchCall(): doInBackground(): objList[0]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 87
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KLMSHttpAsynchCall(): doInBackground(): reqObj.obj.toString(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    iget-object v3, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 89
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    invoke-virtual {p0, v2, v3}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->httpCall(Ljava/lang/String;Landroid/os/Message;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 94
    :goto_0
    return-object v1

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "doInBackground() has Exception."

    invoke-static {v2, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 92
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    aget-object v3, p1, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->processError(Landroid/os/Message;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getCall()Lcom/samsung/klmsagent/http/CallState;
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    return-object v0
.end method

.method public getCounter()I
    .locals 1

    .prologue
    .line 494
    iget v0, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->counter:I

    return v0
.end method

.method protected httpCall(Ljava/lang/String;Landroid/os/Message;)Ljava/lang/String;
    .locals 37
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "requestObj"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 163
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    .line 164
    .local v24, "startTime":J
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): Start - time: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    new-instance v34, Ljava/util/Date;

    move-object/from16 v0, v34

    move-wide/from16 v1, v24

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, " | "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, ""

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 166
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSUtility;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v33

    if-nez v33, :cond_1

    .line 167
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Network not available, stop the call return with null."

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 168
    const/16 v23, 0x0

    .line 398
    :cond_0
    :goto_0
    return-object v23

    .line 171
    :cond_1
    const/16 v23, 0x0

    .line 172
    .local v23, "serverResponse":Ljava/lang/String;
    const/16 v26, 0x0

    .line 173
    .local v26, "statusCode":I
    const/16 v16, 0x0

    .line 174
    .local v16, "proxy":Lorg/apache/http/HttpHost;
    const/4 v12, 0x0

    .line 178
    .local v12, "httpclient":Lorg/apache/http/client/HttpClient;
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v33

    const-string v34, "url"

    const/16 v35, 0x0

    invoke-virtual/range {v33 .. v35}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    if-nez v33, :cond_7

    .line 179
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Getting url from Normal call"

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v33, v0

    sget-object v34, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v34

    move/from16 v0, v33

    move/from16 v1, v34

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v33, v0

    sget-object v34, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REGISTER_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v34

    move/from16 v0, v33

    move/from16 v1, v34

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v33, v0

    sget-object v34, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->RP_MODE:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v34

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_4

    .line 183
    :cond_2
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Get B2C URL"

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v33, v0

    const-string v34, "b2c"

    move-object/from16 v0, v33

    move-object/from16 v1, p1

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/klmsagent/http/CallState;->getUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 200
    :goto_1
    if-nez p1, :cond_a

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v33, v0

    sget-object v34, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v34

    move/from16 v0, v33

    move/from16 v1, v34

    if-eq v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v33, v0

    sget-object v34, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REGISTER_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v34

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_8

    .line 203
    :cond_3
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Container uninstall for b2c case."

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 204
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): No need to call GSLB as it will never be called back."

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    :goto_2
    const/16 v23, 0x0

    .line 393
    .end local v23    # "serverResponse":Ljava/lang/String;
    if-eqz v12, :cond_0

    .line 394
    invoke-interface {v12}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v33

    if-eqz v33, :cond_0

    .line 395
    invoke-interface {v12}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v33

    invoke-interface/range {v33 .. v33}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_0

    .line 185
    .restart local v23    # "serverResponse":Ljava/lang/String;
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v33, v0

    sget-object v34, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v34

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_5

    .line 186
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Get GSLB URL"

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v33, v0

    const-string v34, "gslb"

    move-object/from16 v0, v33

    move-object/from16 v1, p1

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/klmsagent/http/CallState;->getUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 188
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v33

    const-string v34, "onPrem"

    invoke-virtual/range {v33 .. v34}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v33

    if-eqz v33, :cond_6

    .line 189
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Get OnPremise URL"

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v33, v0

    const-string v34, "onPrem"

    move-object/from16 v0, v33

    move-object/from16 v1, p1

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/klmsagent/http/CallState;->getUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_1

    .line 192
    :cond_6
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Get B2B URL"

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v33, v0

    const-string v34, "b2b"

    move-object/from16 v0, v33

    move-object/from16 v1, p1

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/klmsagent/http/CallState;->getUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_1

    .line 196
    :cond_7
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Retry already set"

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 197
    invoke-virtual/range {p2 .. p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v33

    const-string v34, "url"

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_1

    .line 206
    :cond_8
    invoke-static {}, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;->doGSLBJob()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 389
    :catch_0
    move-exception v8

    .line 390
    .local v8, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_2
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Error ocurred for the call KLMSHttpAsynchCall.httpCall()."

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 391
    throw v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 393
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v33

    :goto_4
    if-eqz v12, :cond_9

    .line 394
    invoke-interface {v12}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v34

    if-eqz v34, :cond_9

    .line 395
    invoke-interface {v12}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v34

    invoke-interface/range {v34 .. v34}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    :cond_9
    throw v33

    .line 211
    :cond_a
    :try_start_3
    new-instance v27, Ljava/net/URL;

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 214
    .local v27, "url":Ljava/net/URL;
    new-instance v13, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v13}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 217
    .end local v12    # "httpclient":Lorg/apache/http/client/HttpClient;
    .local v13, "httpclient":Lorg/apache/http/client/HttpClient;
    :try_start_4
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): HTTPS proxy host : "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "https.proxyHost"

    invoke-static/range {v34 .. v34}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 218
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): HTTPS proxy port : "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "https.proxyPort"

    invoke-static/range {v34 .. v34}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 221
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Landroid/net/Proxy;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    .line 222
    .local v18, "proxyHost":Ljava/lang/String;
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): proxyHost: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 223
    if-nez v18, :cond_b

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/klmsagent/http/CallState;->getCurrentConnection()Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v33

    sget-object v34, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_f

    .line 225
    const-string v33, "https.proxyHost"

    invoke-static/range {v33 .. v33}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 230
    :cond_b
    :goto_5
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): proxyHost: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 231
    if-eqz v18, :cond_d

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSUtility;->hasText(Ljava/lang/String;)Z

    move-result v33

    if-eqz v33, :cond_d

    .line 232
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Landroid/net/Proxy;->getPort(Landroid/content/Context;)I

    move-result v19

    .line 233
    .local v19, "proxyPort":I
    const/16 v33, -0x1

    move/from16 v0, v19

    move/from16 v1, v33

    if-ne v0, v1, :cond_c

    .line 234
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/klmsagent/http/CallState;->getCurrentConnection()Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v33

    sget-object v34, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTP:Lcom/samsung/klmsagent/http/CallState$ConnectionType;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_10

    .line 236
    :try_start_5
    const-string v33, "http.proxyPort"

    invoke-static/range {v33 .. v33}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/Integer;->intValue()I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v19

    .line 250
    :cond_c
    :goto_6
    :try_start_6
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): proxyHost: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 251
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): proxyPort: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 252
    if-eqz v18, :cond_d

    const/16 v33, -0x1

    move/from16 v0, v19

    move/from16 v1, v33

    if-eq v0, v1, :cond_d

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSUtility;->hasText(Ljava/lang/String;)Z

    move-result v33

    if-eqz v33, :cond_d

    if-eqz v19, :cond_d

    .line 253
    new-instance v17, Lorg/apache/http/HttpHost;

    invoke-virtual/range {v27 .. v27}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v19

    move-object/from16 v3, v33

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 254
    .end local v16    # "proxy":Lorg/apache/http/HttpHost;
    .local v17, "proxy":Lorg/apache/http/HttpHost;
    :try_start_7
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v17 .. v17}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, ":"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v17 .. v17}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, ":"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v17 .. v17}, Lorg/apache/http/HttpHost;->getSchemeName()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 255
    invoke-interface {v13}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v33

    const-string v34, "http.route.default-proxy"

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    move-object/from16 v2, v17

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-object/from16 v16, v17

    .line 259
    .end local v17    # "proxy":Lorg/apache/http/HttpHost;
    .end local v19    # "proxyPort":I
    .restart local v16    # "proxy":Lorg/apache/http/HttpHost;
    :cond_d
    :try_start_8
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): uri: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 262
    new-instance v20, Ljava/net/URI;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 263
    .local v20, "reqUri":Ljava/net/URI;
    new-instance v15, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, v20

    invoke-direct {v15, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 266
    .local v15, "post":Lorg/apache/http/client/methods/HttpPost;
    new-instance v10, Lorg/apache/http/message/BasicHeader;

    const-string v33, "HOST"

    invoke-virtual/range {v20 .. v20}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    invoke-direct {v10, v0, v1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    .local v10, "hostHeader":Lorg/apache/http/Header;
    invoke-virtual {v15, v10}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Lorg/apache/http/Header;)V

    .line 270
    const/4 v9, 0x0

    .line 271
    .local v9, "entity":Lorg/apache/http/entity/StringEntity;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/klmsagent/http/CallState;->getCallType()Lcom/samsung/klmsagent/http/CallState$CallType;

    move-result-object v33

    sget-object v34, Lcom/samsung/klmsagent/http/CallState$CallType;->GSLB:Lcom/samsung/klmsagent/http/CallState$CallType;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_11

    .line 272
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Request to GSLB_Server."

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 273
    new-instance v9, Lorg/apache/http/entity/StringEntity;

    .end local v9    # "entity":Lorg/apache/http/entity/StringEntity;
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-direct {v9, v0}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 279
    .restart local v9    # "entity":Lorg/apache/http/entity/StringEntity;
    :goto_7
    invoke-virtual {v15, v9}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 282
    invoke-interface {v13}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v11

    .line 283
    .local v11, "httpParameters":Lorg/apache/http/params/HttpParams;
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v33

    const-string v34, "ro.csc.country_code"

    invoke-static/range {v33 .. v34}, Lcom/samsung/klmsagent/util/KLMSSystemProperties;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 284
    .local v6, "country":Ljava/lang/String;
    if-eqz v6, :cond_12

    const-string v33, "China"

    move-object/from16 v0, v33

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v33

    if-eqz v33, :cond_12

    .line 285
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Set Connection time for CHINA: 25sec"

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 287
    const/16 v33, 0x61a8

    move/from16 v0, v33

    invoke-static {v11, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 289
    const/16 v33, 0x61a8

    move/from16 v0, v33

    invoke-static {v11, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 298
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/klmsagent/http/CallState;->getCallType()Lcom/samsung/klmsagent/http/CallState$CallType;

    move-result-object v33

    sget-object v34, Lcom/samsung/klmsagent/http/CallState$CallType;->GSLB:Lcom/samsung/klmsagent/http/CallState$CallType;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_13

    .line 299
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Request to GSLB server. Don\'t Set additional Header."

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 300
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v33, "Content-type"

    const-string v34, "application/json"

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    invoke-direct {v5, v0, v1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    .local v5, "contentType":Lorg/apache/http/Header;
    invoke-virtual {v15, v5}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Lorg/apache/http/Header;)V

    .line 332
    :goto_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    .line 333
    .local v28, "timeStamp":J
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v33, "client_request_time_stamp"

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, ""

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    invoke-direct {v7, v0, v1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    .local v7, "dateHeader":Lorg/apache/http/Header;
    invoke-virtual {v15, v7}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Lorg/apache/http/Header;)V

    .line 337
    const/4 v14, 0x0

    .line 338
    .local v14, "httpresponse":Lorg/apache/http/HttpResponse;
    if-eqz v16, :cond_18

    .line 339
    move-object/from16 v0, v16

    invoke-interface {v13, v0, v15}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v14

    .line 344
    :goto_a
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v33

    invoke-interface/range {v33 .. v33}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v26

    .line 345
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): Response return HTTPS>>>>STATUS : "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 347
    const/16 v33, 0xc8

    move/from16 v0, v26

    move/from16 v1, v33

    if-lt v0, v1, :cond_1a

    const/16 v33, 0x12c

    move/from16 v0, v26

    move/from16 v1, v33

    if-gt v0, v1, :cond_1a

    .line 348
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v21

    .line 349
    .local v21, "responseEntity":Lorg/apache/http/HttpEntity;
    if-eqz v21, :cond_19

    .line 350
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v23

    .line 354
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): http Response(Success): "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 359
    .end local v21    # "responseEntity":Lorg/apache/http/HttpEntity;
    :goto_b
    const-string v33, "Content-type"

    move-object/from16 v0, v33

    invoke-interface {v14, v0}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v22

    .line 361
    .local v22, "returnHeaders":[Lorg/apache/http/Header;
    const/16 v33, 0x12c

    move/from16 v0, v26

    move/from16 v1, v33

    if-lt v0, v1, :cond_20

    if-eqz v22, :cond_20

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v33, v0

    const/16 v34, 0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_20

    .line 362
    const-string v33, "application/json"

    const/16 v34, 0x0

    aget-object v34, v22, v34

    invoke-interface/range {v34 .. v34}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v33

    if-eqz v33, :cond_e

    const-string v33, "application/knox-crypto-stream"

    const/16 v34, 0x0

    aget-object v34, v22, v34

    invoke-interface/range {v34 .. v34}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v33

    if-nez v33, :cond_1b

    .line 364
    :cond_e
    new-instance v33, Ljava/lang/Exception;

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "Inputstream is not valid, content-type : "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v35, 0x0

    aget-object v35, v22, v35

    invoke-interface/range {v35 .. v35}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-direct/range {v33 .. v34}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v33

    .line 389
    .end local v5    # "contentType":Lorg/apache/http/Header;
    .end local v6    # "country":Ljava/lang/String;
    .end local v7    # "dateHeader":Lorg/apache/http/Header;
    .end local v9    # "entity":Lorg/apache/http/entity/StringEntity;
    .end local v10    # "hostHeader":Lorg/apache/http/Header;
    .end local v11    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .end local v14    # "httpresponse":Lorg/apache/http/HttpResponse;
    .end local v15    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v18    # "proxyHost":Ljava/lang/String;
    .end local v20    # "reqUri":Ljava/net/URI;
    .end local v22    # "returnHeaders":[Lorg/apache/http/Header;
    .end local v28    # "timeStamp":J
    :catch_1
    move-exception v8

    move-object v12, v13

    .end local v13    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v12    # "httpclient":Lorg/apache/http/client/HttpClient;
    goto/16 :goto_3

    .line 226
    .end local v12    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v13    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v18    # "proxyHost":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/klmsagent/http/CallState;->getCurrentConnection()Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v33

    sget-object v34, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTP:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_b

    .line 227
    const-string v33, "http.proxyHost"

    invoke-static/range {v33 .. v33}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_5

    .line 237
    .restart local v19    # "proxyPort":I
    :catch_2
    move-exception v8

    .line 238
    .restart local v8    # "e":Ljava/lang/Exception;
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Error in converting to port number"

    move-object/from16 v0, v33

    invoke-static {v0, v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 239
    const/16 v19, -0x1

    .line 240
    goto/16 :goto_6

    .line 241
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/klmsagent/http/CallState;->getCurrentConnection()Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v33

    sget-object v34, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_c

    .line 243
    :try_start_9
    const-string v33, "https.proxyPort"

    invoke-static/range {v33 .. v33}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/Integer;->intValue()I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result v19

    goto/16 :goto_6

    .line 244
    :catch_3
    move-exception v8

    .line 245
    .restart local v8    # "e":Ljava/lang/Exception;
    :try_start_a
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Error in converting to port number"

    move-object/from16 v0, v33

    invoke-static {v0, v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 246
    const/16 v19, -0x1

    goto/16 :goto_6

    .line 275
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v19    # "proxyPort":I
    .restart local v9    # "entity":Lorg/apache/http/entity/StringEntity;
    .restart local v10    # "hostHeader":Lorg/apache/http/Header;
    .restart local v15    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v20    # "reqUri":Ljava/net/URI;
    :cond_11
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Request to KLMS_Server. Data should be Encrypted."

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 276
    new-instance v9, Lorg/apache/http/entity/StringEntity;

    .end local v9    # "entity":Lorg/apache/http/entity/StringEntity;
    invoke-static {}, Lcom/samsung/klmsagent/security/KLMSCipherV3;->getInstance()Lcom/samsung/klmsagent/security/KLMSCipherV3;

    move-result-object v33

    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-direct/range {p0 .. p0}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getSessionKey()Ljava/security/Key;

    move-result-object v35

    invoke-direct/range {p0 .. p0}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getEncryptIV()Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v36

    invoke-virtual/range {v33 .. v36}, Lcom/samsung/klmsagent/security/KLMSCipherV3;->getEncryptedNetworkData(Ljava/lang/String;Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-direct {v9, v0}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .restart local v9    # "entity":Lorg/apache/http/entity/StringEntity;
    goto/16 :goto_7

    .line 291
    .restart local v6    # "country":Ljava/lang/String;
    .restart local v11    # "httpParameters":Lorg/apache/http/params/HttpParams;
    :cond_12
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Set Connection time for GLOBAL: 10sec"

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 293
    const/16 v33, 0x2710

    move/from16 v0, v33

    invoke-static {v11, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 294
    const/16 v33, 0x2710

    move/from16 v0, v33

    invoke-static {v11, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    goto/16 :goto_8

    .line 393
    .end local v6    # "country":Ljava/lang/String;
    .end local v9    # "entity":Lorg/apache/http/entity/StringEntity;
    .end local v10    # "hostHeader":Lorg/apache/http/Header;
    .end local v11    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .end local v15    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v18    # "proxyHost":Ljava/lang/String;
    .end local v20    # "reqUri":Ljava/net/URI;
    :catchall_1
    move-exception v33

    move-object v12, v13

    .end local v13    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v12    # "httpclient":Lorg/apache/http/client/HttpClient;
    goto/16 :goto_4

    .line 303
    .end local v12    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v6    # "country":Ljava/lang/String;
    .restart local v9    # "entity":Lorg/apache/http/entity/StringEntity;
    .restart local v10    # "hostHeader":Lorg/apache/http/Header;
    .restart local v11    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .restart local v13    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v15    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v18    # "proxyHost":Ljava/lang/String;
    .restart local v20    # "reqUri":Ljava/net/URI;
    :cond_13
    const/4 v4, 0x0

    .line 305
    .local v4, "XSN":Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v33, v0

    sget-object v34, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v34

    move/from16 v0, v33

    move/from16 v1, v34

    if-eq v0, v1, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v33, v0

    sget-object v34, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REGISTER_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v34

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_15

    .line 307
    :cond_14
    sget-object v4, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->B2C_CONTAINER_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    .line 308
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): Request to B2C_CONTAINER Server: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual {v4}, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 320
    :goto_c
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v33, "Content-type"

    const-string v34, "application/knox-crypto-stream"

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    invoke-direct {v5, v0, v1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    .restart local v5    # "contentType":Lorg/apache/http/Header;
    new-instance v32, Lorg/apache/http/message/BasicHeader;

    const-string v33, "X-SS"

    const-string v34, "rna1.2"

    invoke-direct/range {v32 .. v34}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    .local v32, "x_ss":Lorg/apache/http/Header;
    new-instance v31, Lorg/apache/http/message/BasicHeader;

    const-string v33, "X-SN"

    invoke-virtual {v4}, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->getSerialNumebr()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v31

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .local v31, "x_sn":Lorg/apache/http/Header;
    new-instance v30, Lorg/apache/http/message/BasicHeader;

    const-string v33, "X-SK"

    invoke-static {}, Lcom/samsung/klmsagent/security/KLMSCipherV3;->getInstance()Lcom/samsung/klmsagent/security/KLMSCipherV3;

    move-result-object v34

    invoke-direct/range {p0 .. p0}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getSessionKey()Ljava/security/Key;

    move-result-object v35

    invoke-direct/range {p0 .. p0}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getEncryptIV()Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v36

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2, v4}, Lcom/samsung/klmsagent/security/KLMSCipherV3;->getEncryptedAESSessionKey(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;)Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v30

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    .local v30, "x_sk":Lorg/apache/http/Header;
    invoke-virtual {v15, v5}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Lorg/apache/http/Header;)V

    .line 326
    move-object/from16 v0, v32

    invoke-virtual {v15, v0}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Lorg/apache/http/Header;)V

    .line 327
    move-object/from16 v0, v31

    invoke-virtual {v15, v0}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Lorg/apache/http/Header;)V

    .line 328
    move-object/from16 v0, v30

    invoke-virtual {v15, v0}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Lorg/apache/http/Header;)V

    goto/16 :goto_9

    .line 309
    .end local v5    # "contentType":Lorg/apache/http/Header;
    .end local v30    # "x_sk":Lorg/apache/http/Header;
    .end local v31    # "x_sn":Lorg/apache/http/Header;
    .end local v32    # "x_ss":Lorg/apache/http/Header;
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v33

    const-string v34, "onPrem"

    invoke-virtual/range {v33 .. v34}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v33

    if-eqz v33, :cond_16

    .line 310
    sget-object v4, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->ONPREM_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    .line 311
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): Request to ONPREMISE Server: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual {v4}, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 312
    :cond_16
    const-string v33, "b2c"

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v33

    if-eqz v33, :cond_17

    .line 313
    sget-object v4, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->B2C_RP_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    .line 314
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): Request to B2C_RP server: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual {v4}, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 316
    :cond_17
    sget-object v4, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->CLOUD_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    .line 317
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): Request to B2B Server: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual {v4}, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 341
    .end local v4    # "XSN":Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;
    .restart local v5    # "contentType":Lorg/apache/http/Header;
    .restart local v7    # "dateHeader":Lorg/apache/http/Header;
    .restart local v14    # "httpresponse":Lorg/apache/http/HttpResponse;
    .restart local v28    # "timeStamp":J
    :cond_18
    invoke-interface {v13, v15}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v14

    goto/16 :goto_a

    .line 352
    .restart local v21    # "responseEntity":Lorg/apache/http/HttpEntity;
    :cond_19
    new-instance v33, Ljava/lang/Exception;

    const-string v34, "Inputstream is not valid"

    invoke-direct/range {v33 .. v34}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v33

    .line 356
    .end local v21    # "responseEntity":Lorg/apache/http/HttpEntity;
    :cond_1a
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): http Response(Fail): "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 367
    .restart local v22    # "returnHeaders":[Lorg/apache/http/Header;
    :cond_1b
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v21

    .line 368
    .restart local v21    # "responseEntity":Lorg/apache/http/HttpEntity;
    if-eqz v21, :cond_1f

    .line 369
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/klmsagent/http/CallState;->getCallType()Lcom/samsung/klmsagent/http/CallState$CallType;

    move-result-object v33

    sget-object v34, Lcom/samsung/klmsagent/http/CallState$CallType;->GSLB:Lcom/samsung/klmsagent/http/CallState$CallType;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_1d

    .line 370
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v23

    .line 388
    .end local v21    # "responseEntity":Lorg/apache/http/HttpEntity;
    :cond_1c
    :goto_d
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "KLMSHttpAsynchCall(): httpCall(): serverResponse: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 393
    if-eqz v13, :cond_0

    .line 394
    invoke-interface {v13}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v33

    if-eqz v33, :cond_0

    .line 395
    invoke-interface {v13}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v33

    invoke-interface/range {v33 .. v33}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_0

    .line 373
    .restart local v21    # "responseEntity":Lorg/apache/http/HttpEntity;
    :cond_1d
    const/16 v33, 0x193

    move/from16 v0, v26

    move/from16 v1, v33

    if-ne v0, v1, :cond_1c

    :try_start_b
    move-object/from16 v0, p2

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v33, v0

    sget-object v34, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->DEVICE_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v34

    move/from16 v0, v33

    move/from16 v1, v34

    if-eq v0, v1, :cond_1e

    move-object/from16 v0, p2

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v33, v0

    sget-object v34, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->DEVICE_DEACTIVATION:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v34

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1c

    .line 376
    :cond_1e
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): time is not correct in the device."

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 377
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v23

    goto :goto_d

    .line 381
    :cond_1f
    new-instance v33, Ljava/lang/Exception;

    const-string v34, "httpCall(): Inputstream is not valid"

    invoke-direct/range {v33 .. v34}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v33

    .line 384
    .end local v21    # "responseEntity":Lorg/apache/http/HttpEntity;
    :cond_20
    const/16 v33, 0x12c

    move/from16 v0, v26

    move/from16 v1, v33

    if-lt v0, v1, :cond_1c

    .line 385
    const-string v33, "KLMSHttpAsynchCall(): httpCall(): Error in processing the request. It will go in retry logic"

    invoke-static/range {v33 .. v33}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 386
    new-instance v33, Ljava/lang/Exception;

    const-string v34, "Inputstream is not valid"

    invoke-direct/range {v33 .. v34}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v33
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 393
    .end local v5    # "contentType":Lorg/apache/http/Header;
    .end local v6    # "country":Ljava/lang/String;
    .end local v7    # "dateHeader":Lorg/apache/http/Header;
    .end local v9    # "entity":Lorg/apache/http/entity/StringEntity;
    .end local v10    # "hostHeader":Lorg/apache/http/Header;
    .end local v11    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .end local v14    # "httpresponse":Lorg/apache/http/HttpResponse;
    .end local v15    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v16    # "proxy":Lorg/apache/http/HttpHost;
    .end local v20    # "reqUri":Ljava/net/URI;
    .end local v22    # "returnHeaders":[Lorg/apache/http/Header;
    .end local v28    # "timeStamp":J
    .restart local v17    # "proxy":Lorg/apache/http/HttpHost;
    .restart local v19    # "proxyPort":I
    :catchall_2
    move-exception v33

    move-object v12, v13

    .end local v13    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v12    # "httpclient":Lorg/apache/http/client/HttpClient;
    move-object/from16 v16, v17

    .end local v17    # "proxy":Lorg/apache/http/HttpHost;
    .restart local v16    # "proxy":Lorg/apache/http/HttpHost;
    goto/16 :goto_4

    .line 389
    .end local v12    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v16    # "proxy":Lorg/apache/http/HttpHost;
    .restart local v13    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v17    # "proxy":Lorg/apache/http/HttpHost;
    :catch_4
    move-exception v8

    move-object v12, v13

    .end local v13    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v12    # "httpclient":Lorg/apache/http/client/HttpClient;
    move-object/from16 v16, v17

    .end local v17    # "proxy":Lorg/apache/http/HttpHost;
    .restart local v16    # "proxy":Lorg/apache/http/HttpHost;
    goto/16 :goto_3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 55
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 99
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    iget v2, v2, Landroid/os/Message;->arg2:I

    const v3, -0x1869f

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    iget v2, v2, Landroid/os/Message;->arg2:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    if-eqz p1, :cond_2

    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/http/CallState;->getCallType()Lcom/samsung/klmsagent/http/CallState$CallType;

    move-result-object v2

    sget-object v3, Lcom/samsung/klmsagent/http/CallState$CallType;->KLMS:Lcom/samsung/klmsagent/http/CallState$CallType;

    if-ne v2, v3, :cond_2

    .line 105
    :try_start_0
    const-string v2, "KLMSHttpAsynchCall(): onPostExecute(): Response From KLMS_Server. Decrypt data."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 106
    invoke-static {}, Lcom/samsung/klmsagent/security/KLMSCipherV3;->getInstance()Lcom/samsung/klmsagent/security/KLMSCipherV3;

    move-result-object v2

    invoke-direct {p0}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getSessionKey()Ljava/security/Key;

    move-result-object v3

    invoke-direct {p0}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getDecryptIV()Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v4

    invoke-virtual {v2, p1, v3, v4}, Lcom/samsung/klmsagent/security/KLMSCipherV3;->getDecryptedNetworkData(Ljava/lang/String;Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 112
    :cond_2
    :goto_1
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->values()[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    move-result-object v1

    .line 113
    .local v1, "requestArray":[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 115
    :try_start_1
    sget-object v2, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$KLMSRequestType:[I

    iget-object v3, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    iget v3, v3, Landroid/os/Message;->what:I

    aget-object v3, v1, v3

    invoke-virtual {v3}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 151
    const-string v2, "KLMSHttpAsynchCall(): No mapping for this request type."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157
    :goto_2
    const-string v2, "KLMSHttpAsynchCall(): onPostExecute().END"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 107
    .end local v1    # "requestArray":[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "onPostExecute() has Exception."

    invoke-static {v2, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 117
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "requestArray":[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    :pswitch_0
    :try_start_2
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    invoke-static {p1, v2}, Lcom/samsung/klmsagent/b2c/services/ContainerServices;->postProcessResponse(Ljava/lang/String;Landroid/os/Message;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 154
    :catch_1
    move-exception v0

    .line 155
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_3
    const-string v2, "KLMSHttpAsynchCall(): onPostExecute():"

    invoke-static {v2, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 157
    const-string v2, "KLMSHttpAsynchCall(): onPostExecute().END"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 120
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_1
    :try_start_4
    const-string v2, "KLMSHttpAsynchCall(): B2C Conatiner is removed"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 121
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    invoke-static {p1, v2}, Lcom/samsung/klmsagent/b2c/services/ContainerServices;->postProcessResponseUninstall(Ljava/lang/String;Landroid/os/Message;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 157
    :catchall_0
    move-exception v2

    const-string v3, "KLMSHttpAsynchCall(): onPostExecute().END"

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    throw v2

    .line 125
    :pswitch_2
    :try_start_5
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    invoke-static {p1, v2}, Lcom/samsung/klmsagent/services/i/DeviceServices;->postProcessResponse(Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_2

    .line 128
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    invoke-static {p1, v2}, Lcom/samsung/klmsagent/services/i/ContainerServices;->postProcessResponse(Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_2

    .line 131
    :pswitch_4
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    invoke-static {p1, v2}, Lcom/samsung/klmsagent/services/i/ContainerServices;->postProcessResponseUninstall(Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_2

    .line 134
    :pswitch_5
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    invoke-static {p1, v2}, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->postProcessResponseTrackerId(Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_2

    .line 137
    :pswitch_6
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    invoke-static {p1, v2}, Lcom/samsung/klmsagent/services/i/DeviceServices;->postProcessResponseUninstall(Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_2

    .line 140
    :pswitch_7
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    invoke-static {p1, v2}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->postProcessResponse(Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_2

    .line 143
    :pswitch_8
    const-string v2, "KLMSHttpAsynchCall(): Setting the policy file now."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 144
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    invoke-static {p1, v2}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->postProcessResponseForPolicy(Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_2

    .line 147
    :pswitch_9
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->reqObj:Landroid/os/Message;

    invoke-static {p1, v2}, Lcom/samsung/klmsagent/b2c/services/ContainerServices;->postProcessResponseRPMode(Ljava/lang/String;Landroid/os/Message;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method protected processError(Landroid/os/Message;Ljava/lang/String;)V
    .locals 10
    .param p1, "obj"    # Landroid/os/Message;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 402
    invoke-static {}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->values()[Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v7

    .line 403
    .local v7, "typeArray":[Lcom/samsung/klmsagent/http/CallState$ConnectionType;
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->values()[Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    move-result-object v1

    .line 404
    .local v1, "frequencyArray":[Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;
    sget-object v8, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall$1;->$SwitchMap$com$samsung$klmsagent$http$CallState$ConnectionType:[I

    iget-object v9, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v9}, Lcom/samsung/klmsagent/http/CallState;->getCurrentConnection()Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->ordinal()I

    move-result v9

    aget-object v9, v7, v9

    invoke-virtual {v9}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 471
    :goto_0
    return-void

    .line 410
    :pswitch_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSHttpAsynchCall(): processError().Current retry Count is : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v9}, Lcom/samsung/klmsagent/http/CallState;->getTryCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Connection in use is HTTPS."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 412
    iget-object v8, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v8}, Lcom/samsung/klmsagent/http/CallState;->getTryCount()I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_3

    .line 413
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSHttpAsynchCall(): processError().Calling the new url with http type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 414
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 415
    .local v2, "msg":Landroid/os/Message;
    invoke-virtual {v2, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 416
    iget v8, p1, Landroid/os/Message;->arg1:I

    if-eqz v8, :cond_0

    .line 417
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSHttpAsynchCall(): processError().Display the value for license expired set in validate key:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 421
    :cond_0
    invoke-virtual {v2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "onPrem"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 422
    const-string v8, "KLMSHttpAsynchCall(): processError().Using on premise key no need to toggle to http"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .line 425
    :cond_1
    const-string v8, "KLMSHttpAsynchCall(): processError().Failed to call using https, call gslb to update the host name."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 427
    iget-object v8, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v8}, Lcom/samsung/klmsagent/http/CallState;->getCallType()Lcom/samsung/klmsagent/http/CallState$CallType;

    move-result-object v8

    sget-object v9, Lcom/samsung/klmsagent/http/CallState$CallType;->GSLB:Lcom/samsung/klmsagent/http/CallState$CallType;

    if-eq v8, v9, :cond_2

    iget v8, p1, Landroid/os/Message;->what:I

    sget-object v9, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REGISTER_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual {v9}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v9

    if-eq v8, v9, :cond_2

    iget v8, p1, Landroid/os/Message;->what:I

    sget-object v9, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual {v9}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v9

    if-eq v8, v9, :cond_2

    .line 430
    const/4 v8, 0x0

    const-string v9, "b2b"

    invoke-static {v8, v9}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->setKLMSServerAddr(Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 432
    :cond_2
    const-string v8, "KLMSHttpAsynchCall(): processError().Failed to get the URL from the GSLB"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 439
    .end local v2    # "msg":Landroid/os/Message;
    :cond_3
    iget-object v8, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v8}, Lcom/samsung/klmsagent/http/CallState;->getTryCount()I

    move-result v6

    .line 440
    .local v6, "tryCount":I
    const-wide/16 v4, 0x0

    .line 441
    .local v4, "timeDelay":J
    sget-object v8, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$HttpRetryFrequency:[I

    aget-object v9, v1, v6

    invoke-virtual {v9}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    .line 454
    :goto_1
    const/4 v8, 0x1

    iput v8, p1, Landroid/os/Message;->arg2:I

    .line 456
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 457
    .local v0, "data":Landroid/os/Bundle;
    if-nez v0, :cond_4

    .line 458
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "data":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 459
    .restart local v0    # "data":Landroid/os/Bundle;
    :cond_4
    const-string v8, "current_count"

    invoke-virtual {v0, v8, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 460
    const-string v8, "current_call_type"

    sget-object v9, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    invoke-virtual {v9}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->ordinal()I

    move-result v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 461
    invoke-virtual {p1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 462
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    .line 463
    .local v3, "newReq":Landroid/os/Message;
    invoke-virtual {v3, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 464
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSHttpAsynchCall(): processError().Going to retry request after: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 465
    sget-object v8, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 443
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v3    # "newReq":Landroid/os/Message;
    :pswitch_1
    sget-object v8, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->FIVE_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSUtility;->getDelayedTimeInMillis(Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;)J

    move-result-wide v4

    .line 444
    goto :goto_1

    .line 446
    :pswitch_2
    sget-object v8, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->THREE_MIN:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSUtility;->getDelayedTimeInMillis(Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;)J

    move-result-wide v4

    .line 447
    goto :goto_1

    .line 449
    :pswitch_3
    sget-object v8, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->FIFTEEN_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSUtility;->getDelayedTimeInMillis(Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;)J

    move-result-wide v4

    .line 450
    goto :goto_1

    .line 404
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 441
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setCall(Lcom/samsung/klmsagent/http/CallState;)V
    .locals 0
    .param p1, "call"    # Lcom/samsung/klmsagent/http/CallState;

    .prologue
    .line 506
    iput-object p1, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->call:Lcom/samsung/klmsagent/http/CallState;

    .line 507
    return-void
.end method

.method public setCounter(I)V
    .locals 0
    .param p1, "counter"    # I

    .prologue
    .line 498
    iput p1, p0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->counter:I

    .line 499
    return-void
.end method
