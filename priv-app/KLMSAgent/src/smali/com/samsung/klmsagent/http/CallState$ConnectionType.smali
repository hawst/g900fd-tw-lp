.class public final enum Lcom/samsung/klmsagent/http/CallState$ConnectionType;
.super Ljava/lang/Enum;
.source "CallState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/http/CallState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConnectionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/klmsagent/http/CallState$ConnectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/klmsagent/http/CallState$ConnectionType;

.field public static final enum HTTP:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

.field public static final enum HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35
    new-instance v0, Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    const-string v1, "HTTP"

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTP:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    new-instance v0, Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    const-string v1, "HTTPS"

    invoke-direct {v0, v1, v3}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    .line 34
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    sget-object v1, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTP:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->$VALUES:[Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/klmsagent/http/CallState$ConnectionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 34
    const-class v0, Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/klmsagent/http/CallState$ConnectionType;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->$VALUES:[Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    invoke-virtual {v0}, [Lcom/samsung/klmsagent/http/CallState$ConnectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    return-object v0
.end method
