.class public Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;
.super Landroid/os/AsyncTask;
.source "KLMSHTTPOnPremise.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field protected static final HTTPS_FAILED_CODE:I = -0x1869f

.field protected static final TAG:Ljava/lang/String; = "KLMSHTTPOnPremise(): "


# instance fields
.field protected call:Lcom/samsung/klmsagent/http/CallState;

.field protected counter:I

.field protected reqObj:Landroid/os/Message;


# direct methods
.method public constructor <init>(Landroid/os/Message;)V
    .locals 1
    .param p1, "reqObj"    # Landroid/os/Message;

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->counter:I

    .line 50
    iput-object p1, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->reqObj:Landroid/os/Message;

    .line 51
    new-instance v0, Lcom/samsung/klmsagent/http/CallState;

    invoke-direct {v0}, Lcom/samsung/klmsagent/http/CallState;-><init>()V

    iput-object v0, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    .line 52
    return-void
.end method

.method protected static convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 12
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 371
    const-string v6, "KLMSHTTPOnPremise(): In HttpUtils.convertStreamToString()"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 372
    const/4 v4, 0x0

    .line 374
    .local v4, "sb":Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 375
    .local v1, "inStream":Ljava/io/InputStreamReader;
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 376
    .local v3, "reader":Ljava/io/BufferedReader;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 378
    .local v2, "line":Ljava/lang/String;
    :goto_0
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 379
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 381
    :catch_0
    move-exception v0

    move-object v4, v5

    .line 382
    .end local v1    # "inStream":Ljava/io/InputStreamReader;
    .end local v2    # "line":Ljava/lang/String;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .local v0, "e":Ljava/io/IOException;
    .restart local v4    # "sb":Ljava/lang/StringBuilder;
    :goto_1
    :try_start_2
    new-instance v6, Ljava/lang/Exception;

    const-string v7, "class name: %s method name: %s at timestamp: %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "KLMSHTTPOnPremise(): "

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "httpCall()"

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 385
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v6

    .end local v4    # "sb":Ljava/lang/StringBuilder;
    .restart local v1    # "inStream":Ljava/io/InputStreamReader;
    .restart local v2    # "line":Ljava/lang/String;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 387
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 385
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .restart local v4    # "sb":Ljava/lang/StringBuilder;
    goto :goto_2

    .line 381
    .end local v1    # "inStream":Ljava/io/InputStreamReader;
    .end local v2    # "line":Ljava/lang/String;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 40
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "objList"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 56
    const/4 v1, 0x0

    .line 59
    .local v1, "response":Ljava/lang/String;
    if-eqz p1, :cond_0

    array-length v2, p1

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 60
    :cond_0
    new-instance v2, Ljava/security/InvalidParameterException;

    const-string v3, "Null Parameter or Empty."

    invoke-direct {v2, v3}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 63
    :cond_1
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KLMSHTTPOnPremise(): doInBackground(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 65
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->reqObj:Landroid/os/Message;

    invoke-virtual {p0, v2, v3}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->httpCall(Ljava/lang/String;Landroid/os/Message;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 70
    :goto_0
    return-object v1

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "doInBackground() has Exception."

    invoke-static {v2, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 68
    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->reqObj:Landroid/os/Message;

    aget-object v3, p1, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->processError(Landroid/os/Message;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getCall()Lcom/samsung/klmsagent/http/CallState;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    return-object v0
.end method

.method public getCounter()I
    .locals 1

    .prologue
    .line 391
    iget v0, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->counter:I

    return v0
.end method

.method protected httpCall(Ljava/lang/String;Landroid/os/Message;)Ljava/lang/String;
    .locals 29
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "requestObj"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    .line 76
    .local v22, "startTime":J
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): httpCall() exceution. Start time: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 78
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSUtility;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v26

    if-nez v26, :cond_1

    .line 79
    const-string v26, "KLMSHTTPOnPremise(): Network not available, Return null."

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 80
    const/16 v20, 0x0

    .line 243
    :cond_0
    :goto_0
    return-object v20

    .line 82
    :cond_1
    const/16 v20, 0x0

    .line 84
    .local v20, "serverResponse":Ljava/lang/String;
    const/16 v21, 0x0

    .line 85
    .local v21, "statusCode":I
    const/4 v13, 0x0

    .line 86
    .local v13, "proxy":Lorg/apache/http/HttpHost;
    const/4 v10, 0x0

    .line 89
    .local v10, "httpclient":Lorg/apache/http/client/HttpClient;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v26, v0

    const-string v27, "gslb"

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/samsung/klmsagent/http/CallState;->getUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 90
    if-nez p1, :cond_5

    .line 91
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->reqObj:Landroid/os/Message;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v26, v0

    sget-object v27, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->reqObj:Landroid/os/Message;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v26, v0

    sget-object v27, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REGISTER_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_3

    .line 93
    :cond_2
    const-string v26, "KLMSHTTPOnPremise(): Container uninstall for b2c case."

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 94
    const-string v26, "KLMSHTTPOnPremise(): No need to call GSLB as it will never be called back."

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    :goto_1
    const/16 v20, 0x0

    .line 238
    .end local v20    # "serverResponse":Ljava/lang/String;
    if-eqz v10, :cond_0

    .line 239
    invoke-interface {v10}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v26

    if-eqz v26, :cond_0

    .line 240
    invoke-interface {v10}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_0

    .line 96
    .restart local v20    # "serverResponse":Ljava/lang/String;
    :cond_3
    :try_start_1
    invoke-static {}, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;->doGSLBJob()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 234
    :catch_0
    move-exception v6

    .line 235
    .local v6, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_2
    const-string v26, "KLMSHTTPOnPremise(): Error ocurred for the call KLMSHttpAsynchCall.httpCall()."

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 236
    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 238
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v26

    :goto_3
    if-eqz v10, :cond_4

    .line 239
    invoke-interface {v10}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v27

    if-eqz v27, :cond_4

    .line 240
    invoke-interface {v10}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    :cond_4
    throw v26

    .line 100
    :cond_5
    :try_start_3
    new-instance v24, Ljava/net/URL;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 102
    .local v24, "url":Ljava/net/URL;
    new-instance v11, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v11}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 105
    .end local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    .local v11, "httpclient":Lorg/apache/http/client/HttpClient;
    :try_start_4
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): Http proxy host: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "http.proxyHost"

    invoke-static/range {v27 .. v27}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 106
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): Http proxy port: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "http.proxyPort"

    invoke-static/range {v27 .. v27}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 107
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): Https proxy host: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "https.proxyHost"

    invoke-static/range {v27 .. v27}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 108
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): Https proxy port: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "https.proxyPort"

    invoke-static/range {v27 .. v27}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 111
    const-string v26, "KLMSHTTPOnPremise(): using proxy if available."

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 112
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Landroid/net/Proxy;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    .line 113
    .local v15, "proxyHost":Ljava/lang/String;
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): Proxy is : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 114
    if-nez v15, :cond_6

    .line 115
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/klmsagent/http/CallState;->getCurrentConnection()Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v26

    sget-object v27, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTP:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-ne v0, v1, :cond_9

    .line 116
    const-string v26, "http.proxyHost"

    invoke-static/range {v26 .. v26}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 121
    :cond_6
    :goto_4
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): Proxy is : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 122
    if-eqz v15, :cond_8

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSUtility;->hasText(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 123
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Landroid/net/Proxy;->getPort(Landroid/content/Context;)I

    move-result v16

    .line 124
    .local v16, "proxyPort":I
    const/16 v26, -0x1

    move/from16 v0, v16

    move/from16 v1, v26

    if-ne v0, v1, :cond_7

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/klmsagent/http/CallState;->getCurrentConnection()Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v26

    sget-object v27, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTP:Lcom/samsung/klmsagent/http/CallState$ConnectionType;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-ne v0, v1, :cond_a

    .line 127
    :try_start_5
    const-string v26, "http.proxyPort"

    invoke-static/range {v26 .. v26}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v16

    .line 141
    :cond_7
    :goto_5
    :try_start_6
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): Proxy host is : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 142
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): Proxy port is : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 143
    if-eqz v15, :cond_8

    const/16 v26, -0x1

    move/from16 v0, v16

    move/from16 v1, v26

    if-eq v0, v1, :cond_8

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSUtility;->hasText(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_8

    if-eqz v16, :cond_8

    .line 144
    new-instance v14, Lorg/apache/http/HttpHost;

    invoke-virtual/range {v24 .. v24}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v26

    move/from16 v0, v16

    move-object/from16 v1, v26

    invoke-direct {v14, v15, v0, v1}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 145
    .end local v13    # "proxy":Lorg/apache/http/HttpHost;
    .local v14, "proxy":Lorg/apache/http/HttpHost;
    :try_start_7
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ":"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual {v14}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ":"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual {v14}, Lorg/apache/http/HttpHost;->getSchemeName()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 146
    invoke-interface {v11}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v26

    const-string v27, "http.route.default-proxy"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-interface {v0, v1, v14}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-object v13, v14

    .line 150
    .end local v14    # "proxy":Lorg/apache/http/HttpHost;
    .end local v16    # "proxyPort":I
    .restart local v13    # "proxy":Lorg/apache/http/HttpHost;
    :cond_8
    :try_start_8
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): uri : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 153
    new-instance v17, Ljava/net/URI;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 154
    .local v17, "reqUri":Ljava/net/URI;
    new-instance v7, Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 157
    .local v7, "get":Lorg/apache/http/client/methods/HttpGet;
    new-instance v8, Lorg/apache/http/message/BasicHeader;

    const-string v26, "HOST"

    invoke-virtual/range {v17 .. v17}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v8, v0, v1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    .local v8, "hostHeader":Lorg/apache/http/Header;
    invoke-virtual {v7, v8}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Lorg/apache/http/Header;)V

    .line 161
    invoke-interface {v11}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v9

    .line 162
    .local v9, "httpParameters":Lorg/apache/http/params/HttpParams;
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v26

    const-string v27, "ro.csc.country_code"

    invoke-static/range {v26 .. v27}, Lcom/samsung/klmsagent/util/KLMSSystemProperties;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 163
    .local v5, "country":Ljava/lang/String;
    if-eqz v5, :cond_b

    const-string v26, "China"

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_b

    .line 164
    const-string v26, "KLMSHTTPOnPremise(): timeOutConnection for china in sec-25"

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 165
    const-string v26, "KLMSHTTPOnPremise(): timeOutSocket for china in sec-25"

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 166
    const/16 v26, 0x61a8

    move/from16 v0, v26

    invoke-static {v9, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 168
    const/16 v26, 0x61a8

    move/from16 v0, v26

    invoke-static {v9, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 176
    :goto_6
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v26, "Content-type"

    const-string v27, "application/json"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v4, v0, v1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    .local v4, "contentType":Lorg/apache/http/Header;
    new-instance v25, Lorg/apache/http/message/BasicHeader;

    const-string v26, "version"

    const-string v27, "v2"

    invoke-direct/range {v25 .. v27}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    .local v25, "versionAlg":Lorg/apache/http/Header;
    invoke-virtual {v7, v4}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Lorg/apache/http/Header;)V

    .line 180
    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Lorg/apache/http/Header;)V

    .line 182
    const/4 v12, 0x0

    .line 183
    .local v12, "httpresponse":Lorg/apache/http/HttpResponse;
    if-eqz v13, :cond_c

    .line 184
    invoke-interface {v11, v13, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v12

    .line 189
    :goto_7
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): Response return HTTP>>>>STATUS: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 190
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v21

    .line 191
    const/16 v26, 0xc8

    move/from16 v0, v21

    move/from16 v1, v26

    if-lt v0, v1, :cond_e

    const/16 v26, 0x12c

    move/from16 v0, v21

    move/from16 v1, v26

    if-gt v0, v1, :cond_e

    .line 192
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v18

    .line 193
    .local v18, "responseEntity":Lorg/apache/http/HttpEntity;
    if-eqz v18, :cond_d

    .line 194
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v20

    .line 198
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): KLMSHttpAsynchCall.httpCall(): Succeess Code: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 203
    .end local v18    # "responseEntity":Lorg/apache/http/HttpEntity;
    :goto_8
    const-string v26, "Content-type"

    move-object/from16 v0, v26

    invoke-interface {v12, v0}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v19

    .line 205
    .local v19, "returnHeaders":[Lorg/apache/http/Header;
    const/16 v26, 0x12c

    move/from16 v0, v21

    move/from16 v1, v26

    if-lt v0, v1, :cond_13

    if-eqz v19, :cond_13

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_13

    .line 206
    const-string v26, "application/json"

    const/16 v27, 0x0

    aget-object v27, v19, v27

    invoke-interface/range {v27 .. v27}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-nez v26, :cond_f

    .line 207
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Invalid content-type returned by the server, returned content-type: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const/16 v27, 0x0

    aget-object v27, v19, v27

    invoke-interface/range {v27 .. v27}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 209
    new-instance v26, Ljava/lang/Exception;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Inputstream is not valid, content-type: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const/16 v28, 0x0

    aget-object v28, v19, v28

    invoke-interface/range {v28 .. v28}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v26

    .line 234
    .end local v4    # "contentType":Lorg/apache/http/Header;
    .end local v5    # "country":Ljava/lang/String;
    .end local v7    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v8    # "hostHeader":Lorg/apache/http/Header;
    .end local v9    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .end local v12    # "httpresponse":Lorg/apache/http/HttpResponse;
    .end local v15    # "proxyHost":Ljava/lang/String;
    .end local v17    # "reqUri":Ljava/net/URI;
    .end local v19    # "returnHeaders":[Lorg/apache/http/Header;
    .end local v25    # "versionAlg":Lorg/apache/http/Header;
    :catch_1
    move-exception v6

    move-object v10, v11

    .end local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    goto/16 :goto_2

    .line 117
    .end local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v15    # "proxyHost":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/klmsagent/http/CallState;->getCurrentConnection()Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v26

    sget-object v27, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-ne v0, v1, :cond_6

    .line 118
    const-string v26, "https.proxyHost"

    invoke-static/range {v26 .. v26}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_4

    .line 128
    .restart local v16    # "proxyPort":I
    :catch_2
    move-exception v6

    .line 129
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v26, "Error in converting to port number"

    move-object/from16 v0, v26

    invoke-static {v0, v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 130
    const/16 v16, -0x1

    .line 131
    goto/16 :goto_5

    .line 132
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/klmsagent/http/CallState;->getCurrentConnection()Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v26

    sget-object v27, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-ne v0, v1, :cond_7

    .line 134
    :try_start_9
    const-string v26, "https.proxyPort"

    invoke-static/range {v26 .. v26}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result v16

    goto/16 :goto_5

    .line 135
    :catch_3
    move-exception v6

    .line 136
    .restart local v6    # "e":Ljava/lang/Exception;
    :try_start_a
    const-string v26, "Error in converting to port number"

    move-object/from16 v0, v26

    invoke-static {v0, v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 137
    const/16 v16, -0x1

    goto/16 :goto_5

    .line 170
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v16    # "proxyPort":I
    .restart local v5    # "country":Ljava/lang/String;
    .restart local v7    # "get":Lorg/apache/http/client/methods/HttpGet;
    .restart local v8    # "hostHeader":Lorg/apache/http/Header;
    .restart local v9    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .restart local v17    # "reqUri":Ljava/net/URI;
    :cond_b
    const-string v26, "KLMSHTTPOnPremise(): timeOutConnection in sec-10"

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 171
    const-string v26, "KLMSHTTPOnPremise(): timeOutSocket in sec-10"

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 172
    const/16 v26, 0x2710

    move/from16 v0, v26

    invoke-static {v9, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 173
    const/16 v26, 0x2710

    move/from16 v0, v26

    invoke-static {v9, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    goto/16 :goto_6

    .line 238
    .end local v5    # "country":Ljava/lang/String;
    .end local v7    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v8    # "hostHeader":Lorg/apache/http/Header;
    .end local v9    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .end local v15    # "proxyHost":Ljava/lang/String;
    .end local v17    # "reqUri":Ljava/net/URI;
    :catchall_1
    move-exception v26

    move-object v10, v11

    .end local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    goto/16 :goto_3

    .line 186
    .end local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v4    # "contentType":Lorg/apache/http/Header;
    .restart local v5    # "country":Ljava/lang/String;
    .restart local v7    # "get":Lorg/apache/http/client/methods/HttpGet;
    .restart local v8    # "hostHeader":Lorg/apache/http/Header;
    .restart local v9    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .restart local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v12    # "httpresponse":Lorg/apache/http/HttpResponse;
    .restart local v15    # "proxyHost":Ljava/lang/String;
    .restart local v17    # "reqUri":Ljava/net/URI;
    .restart local v25    # "versionAlg":Lorg/apache/http/Header;
    :cond_c
    invoke-interface {v11, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v12

    goto/16 :goto_7

    .line 196
    .restart local v18    # "responseEntity":Lorg/apache/http/HttpEntity;
    :cond_d
    new-instance v26, Ljava/lang/Exception;

    const-string v27, "Inputstream is not valid"

    invoke-direct/range {v26 .. v27}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v26

    .line 200
    .end local v18    # "responseEntity":Lorg/apache/http/HttpEntity;
    :cond_e
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): KLMSHttpAsynchCall.httpCall(): Fail Code: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 212
    .restart local v19    # "returnHeaders":[Lorg/apache/http/Header;
    :cond_f
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v18

    .line 213
    .restart local v18    # "responseEntity":Lorg/apache/http/HttpEntity;
    if-eqz v18, :cond_12

    .line 214
    const-string v26, "KLMSHTTPOnPremise(): response entity not equal to null"

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/klmsagent/http/CallState;->getCallType()Lcom/samsung/klmsagent/http/CallState$CallType;

    move-result-object v26

    sget-object v27, Lcom/samsung/klmsagent/http/CallState$CallType;->GSLB:Lcom/samsung/klmsagent/http/CallState$CallType;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-ne v0, v1, :cond_11

    .line 216
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v20

    .line 217
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): Response Entity not equal to null. ServerResponse:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 233
    .end local v18    # "responseEntity":Lorg/apache/http/HttpEntity;
    :cond_10
    :goto_9
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): httpCall().serverResponse: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 238
    if-eqz v11, :cond_0

    .line 239
    invoke-interface {v11}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v26

    if-eqz v26, :cond_0

    .line 240
    invoke-interface {v11}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_0

    .line 219
    .restart local v18    # "responseEntity":Lorg/apache/http/HttpEntity;
    :cond_11
    :try_start_b
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "KLMSHTTPOnPremise(): Error recived from the KLMS server : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto :goto_9

    .line 223
    :cond_12
    const-string v26, "KLMSHTTPOnPremise(): response entity is equal to null"

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 225
    new-instance v26, Ljava/lang/Exception;

    const-string v27, "Inputstream is not valid"

    invoke-direct/range {v26 .. v27}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v26

    .line 229
    .end local v18    # "responseEntity":Lorg/apache/http/HttpEntity;
    :cond_13
    const/16 v26, 0x12c

    move/from16 v0, v21

    move/from16 v1, v26

    if-lt v0, v1, :cond_10

    .line 230
    const-string v26, "Error in processing the request. It will go in retry logic"

    invoke-static/range {v26 .. v26}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 231
    new-instance v26, Ljava/lang/Exception;

    const-string v27, "Inputstream is not valid"

    invoke-direct/range {v26 .. v27}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v26
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 238
    .end local v4    # "contentType":Lorg/apache/http/Header;
    .end local v5    # "country":Ljava/lang/String;
    .end local v7    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v8    # "hostHeader":Lorg/apache/http/Header;
    .end local v9    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .end local v12    # "httpresponse":Lorg/apache/http/HttpResponse;
    .end local v13    # "proxy":Lorg/apache/http/HttpHost;
    .end local v17    # "reqUri":Ljava/net/URI;
    .end local v19    # "returnHeaders":[Lorg/apache/http/Header;
    .end local v25    # "versionAlg":Lorg/apache/http/Header;
    .restart local v14    # "proxy":Lorg/apache/http/HttpHost;
    .restart local v16    # "proxyPort":I
    :catchall_2
    move-exception v26

    move-object v10, v11

    .end local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    move-object v13, v14

    .end local v14    # "proxy":Lorg/apache/http/HttpHost;
    .restart local v13    # "proxy":Lorg/apache/http/HttpHost;
    goto/16 :goto_3

    .line 234
    .end local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v13    # "proxy":Lorg/apache/http/HttpHost;
    .restart local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v14    # "proxy":Lorg/apache/http/HttpHost;
    :catch_4
    move-exception v6

    move-object v10, v11

    .end local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    move-object v13, v14

    .end local v14    # "proxy":Lorg/apache/http/HttpHost;
    .restart local v13    # "proxy":Lorg/apache/http/HttpHost;
    goto/16 :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 40
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 248
    iget-object v1, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->reqObj:Landroid/os/Message;

    iget v1, v1, Landroid/os/Message;->arg2:I

    const v2, -0x1869f

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->reqObj:Landroid/os/Message;

    iget v1, v1, Landroid/os/Message;->arg2:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 254
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->values()[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    move-result-object v0

    .line 255
    .local v0, "requestArray":[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 257
    :try_start_0
    sget-object v1, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$KLMSRequestType:[I

    iget-object v2, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->reqObj:Landroid/os/Message;

    iget v2, v2, Landroid/os/Message;->what:I

    aget-object v2, v0, v2

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 263
    const-string v1, "KLMSHTTPOnPremise(): No mapping for this request type."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    :goto_1
    const-string v1, "KLMSHTTPOnPremise(): Processing in there respective handlers."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 259
    :pswitch_0
    :try_start_1
    const-string v1, "KLMSHTTPOnPremise(): Setting the policy file now."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 260
    iget-object v1, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->reqObj:Landroid/os/Message;

    invoke-static {p1, v1}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->postProcessResponseForPolicy(Ljava/lang/String;Landroid/os/Message;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 267
    :catchall_0
    move-exception v1

    const-string v2, "KLMSHTTPOnPremise(): Processing in there respective handlers."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    throw v1

    .line 257
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected processError(Landroid/os/Message;Ljava/lang/String;)V
    .locals 12
    .param p1, "obj"    # Landroid/os/Message;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    .line 272
    invoke-static {}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->values()[Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v7

    .line 273
    .local v7, "typeArray":[Lcom/samsung/klmsagent/http/CallState$ConnectionType;
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->values()[Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    move-result-object v1

    .line 274
    .local v1, "frequencyArray":[Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;
    sget-object v8, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$http$CallState$ConnectionType:[I

    iget-object v9, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v9}, Lcom/samsung/klmsagent/http/CallState;->getCurrentConnection()Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->ordinal()I

    move-result v9

    aget-object v9, v7, v9

    invoke-virtual {v9}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 368
    :cond_0
    :goto_0
    return-void

    .line 276
    :pswitch_0
    iget-object v8, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v8}, Lcom/samsung/klmsagent/http/CallState;->getTryCount()I

    move-result v8

    if-ge v8, v11, :cond_2

    .line 279
    iget-object v8, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v8}, Lcom/samsung/klmsagent/http/CallState;->getTryCount()I

    move-result v6

    .line 280
    .local v6, "tryCount":I
    const-wide/16 v4, 0x0

    .line 281
    .local v4, "timeDelay":J
    sget-object v8, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$HttpRetryFrequency:[I

    aget-object v9, v1, v6

    invoke-virtual {v9}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    .line 294
    :goto_1
    iput v10, p1, Landroid/os/Message;->arg2:I

    .line 296
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 297
    .local v0, "data":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 298
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "data":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 299
    .restart local v0    # "data":Landroid/os/Bundle;
    :cond_1
    const-string v8, "current_count"

    invoke-virtual {v0, v8, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 300
    const-string v8, "current_call_type"

    sget-object v9, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTP:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    invoke-virtual {v9}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->ordinal()I

    move-result v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 301
    invoke-virtual {p1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 302
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    .line 303
    .local v3, "newReq":Landroid/os/Message;
    invoke-virtual {v3, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 304
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSHTTPOnPremise(): Going to retry request after: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 305
    sget-object v8, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 283
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v3    # "newReq":Landroid/os/Message;
    :pswitch_1
    sget-object v8, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->FIVE_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSUtility;->getDelayedTimeInMillis(Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;)J

    move-result-wide v4

    .line 284
    goto :goto_1

    .line 286
    :pswitch_2
    sget-object v8, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->THREE_MIN:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSUtility;->getDelayedTimeInMillis(Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;)J

    move-result-wide v4

    .line 287
    goto :goto_1

    .line 289
    :pswitch_3
    sget-object v8, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->FIFTEEN_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSUtility;->getDelayedTimeInMillis(Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;)J

    move-result-wide v4

    .line 290
    goto :goto_1

    .line 308
    .end local v4    # "timeDelay":J
    .end local v6    # "tryCount":I
    :cond_2
    const-string v8, "Failed to even call using http, call gslb to update the host name."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 311
    iget-object v8, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v8}, Lcom/samsung/klmsagent/http/CallState;->getCallType()Lcom/samsung/klmsagent/http/CallState$CallType;

    move-result-object v8

    sget-object v9, Lcom/samsung/klmsagent/http/CallState$CallType;->GSLB:Lcom/samsung/klmsagent/http/CallState$CallType;

    if-eq v8, v9, :cond_3

    .line 312
    const/4 v8, 0x0

    const-string v9, "b2b"

    invoke-static {v8, v9}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->setKLMSServerAddr(Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 314
    :cond_3
    const-string v8, "KLMSHTTPOnPremise(): Failed to get the URL from the GSLB."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 323
    :pswitch_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSHTTPOnPremise(): Current retry Count is : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v9}, Lcom/samsung/klmsagent/http/CallState;->getTryCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Connection in use is HTTPS."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 324
    iget-object v8, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v8}, Lcom/samsung/klmsagent/http/CallState;->getTryCount()I

    move-result v8

    if-ne v8, v11, :cond_5

    .line 325
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 326
    .local v2, "msg":Landroid/os/Message;
    invoke-virtual {v2, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 328
    const-string v8, "/KnoxGSLB/lookup/klms"

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    const-string v8, "/KnoxGSLB/policy"

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    :cond_4
    invoke-virtual {v2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "onPrem"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 331
    const-string v8, "KLMSHTTPOnPremise(): No need to toggle to http as its on premise case"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 336
    .end local v2    # "msg":Landroid/os/Message;
    :cond_5
    iget-object v8, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    invoke-virtual {v8}, Lcom/samsung/klmsagent/http/CallState;->getTryCount()I

    move-result v6

    .line 337
    .restart local v6    # "tryCount":I
    const-wide/16 v4, 0x0

    .line 338
    .restart local v4    # "timeDelay":J
    sget-object v8, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$HttpRetryFrequency:[I

    aget-object v9, v1, v6

    invoke-virtual {v9}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_2

    .line 351
    :goto_2
    iput v10, p1, Landroid/os/Message;->arg2:I

    .line 353
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 354
    .restart local v0    # "data":Landroid/os/Bundle;
    if-nez v0, :cond_6

    .line 355
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "data":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 356
    .restart local v0    # "data":Landroid/os/Bundle;
    :cond_6
    const-string v8, "current_count"

    invoke-virtual {v0, v8, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 357
    const-string v8, "current_call_type"

    sget-object v9, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    invoke-virtual {v9}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->ordinal()I

    move-result v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 358
    invoke-virtual {p1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 359
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    .line 360
    .restart local v3    # "newReq":Landroid/os/Message;
    invoke-virtual {v3, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 361
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSHTTPOnPremise(): Going to retry request after: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 362
    sget-object v8, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 340
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v3    # "newReq":Landroid/os/Message;
    :pswitch_5
    sget-object v8, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->FIVE_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSUtility;->getDelayedTimeInMillis(Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;)J

    move-result-wide v4

    .line 341
    goto :goto_2

    .line 343
    :pswitch_6
    sget-object v8, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->THREE_MIN:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSUtility;->getDelayedTimeInMillis(Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;)J

    move-result-wide v4

    .line 344
    goto :goto_2

    .line 346
    :pswitch_7
    sget-object v8, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->FIFTEEN_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSUtility;->getDelayedTimeInMillis(Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;)J

    move-result-wide v4

    .line 347
    goto :goto_2

    .line 274
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 281
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 338
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public setCall(Lcom/samsung/klmsagent/http/CallState;)V
    .locals 0
    .param p1, "call"    # Lcom/samsung/klmsagent/http/CallState;

    .prologue
    .line 403
    iput-object p1, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->call:Lcom/samsung/klmsagent/http/CallState;

    .line 404
    return-void
.end method

.method public setCounter(I)V
    .locals 0
    .param p1, "counter"    # I

    .prologue
    .line 395
    iput p1, p0, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->counter:I

    .line 396
    return-void
.end method
