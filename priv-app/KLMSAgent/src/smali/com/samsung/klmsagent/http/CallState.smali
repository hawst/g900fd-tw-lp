.class public Lcom/samsung/klmsagent/http/CallState;
.super Ljava/lang/Object;
.source "CallState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/http/CallState$1;,
        Lcom/samsung/klmsagent/http/CallState$ConnectionType;,
        Lcom/samsung/klmsagent/http/CallState$CallType;
    }
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String; = "CallState(): "


# instance fields
.field private callType:Lcom/samsung/klmsagent/http/CallState$CallType;

.field private currentConnection:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

.field private tryCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->HTTPS:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    iput-object v0, p0, Lcom/samsung/klmsagent/http/CallState;->currentConnection:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    .line 40
    sget-object v0, Lcom/samsung/klmsagent/http/CallState$CallType;->KLMS:Lcom/samsung/klmsagent/http/CallState$CallType;

    iput-object v0, p0, Lcom/samsung/klmsagent/http/CallState;->callType:Lcom/samsung/klmsagent/http/CallState$CallType;

    .line 41
    return-void
.end method


# virtual methods
.method public getCallType()Lcom/samsung/klmsagent/http/CallState$CallType;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/klmsagent/http/CallState;->callType:Lcom/samsung/klmsagent/http/CallState$CallType;

    return-object v0
.end method

.method public getCurrentConnection()Lcom/samsung/klmsagent/http/CallState$ConnectionType;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/klmsagent/http/CallState;->currentConnection:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    return-object v0
.end method

.method public getTryCount()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/samsung/klmsagent/http/CallState;->tryCount:I

    return v0
.end method

.method public getUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 50
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CallState(): getUri() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " | "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 51
    invoke-static {}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->values()[Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v3

    .line 52
    .local v3, "typeArray":[Lcom/samsung/klmsagent/http/CallState$ConnectionType;
    sget-object v4, Lcom/samsung/klmsagent/http/CallState$1;->$SwitchMap$com$samsung$klmsagent$http$CallState$ConnectionType:[I

    iget-object v5, p0, Lcom/samsung/klmsagent/http/CallState;->currentConnection:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    invoke-virtual {v5}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->ordinal()I

    move-result v5

    aget-object v5, v3, v5

    invoke-virtual {v5}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 94
    const-string v4, "CallState(): protocol not supported"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 95
    const-string v4, ""

    .line 97
    :goto_0
    return-object v4

    .line 54
    :pswitch_0
    const-string v4, "CallState(): getUri(): HTTP"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    :goto_1
    move-object v4, p1

    .line 97
    goto :goto_0

    .line 57
    :pswitch_1
    const-string v4, "CallState(): getUri(): HTTPS"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 58
    iget-object v4, p0, Lcom/samsung/klmsagent/http/CallState;->callType:Lcom/samsung/klmsagent/http/CallState$CallType;

    sget-object v5, Lcom/samsung/klmsagent/http/CallState$CallType;->GSLB:Lcom/samsung/klmsagent/http/CallState$CallType;

    if-ne v4, v5, :cond_5

    .line 59
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "ro.csc.country_code"

    invoke-static {v4, v5}, Lcom/samsung/klmsagent/util/KLMSSystemProperties;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "country":Ljava/lang/String;
    if-eqz v1, :cond_2

    const-string v4, "China"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 61
    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant;->HTTP_GSLB_CHINA_REDIRECTION_SERVER_HOST_DEV:Ljava/lang/String;

    .line 62
    .local v2, "gslbHost":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 63
    const-string v4, "CallState(): [HTTPS] Getting OnPremise GSLB HOST from Shared-Pref for china."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 64
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v2

    .line 70
    :cond_0
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "https://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 83
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CallState(): [HTTPS] Calling the URL : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_1

    .line 65
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "ro.product_ship"

    invoke-static {v4, v5}, Lcom/samsung/klmsagent/util/KLMSSystemProperties;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->checkQATesting()Z

    move-result v4

    if-nez v4, :cond_0

    .line 67
    const-string v4, "CallState(): [HTTPS] Getting gslb host from constant for china."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 68
    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant;->HTTP_GSLB_CHINA_REDIRECTION_SERVER_HOST:Ljava/lang/String;

    goto :goto_2

    .line 72
    .end local v2    # "gslbHost":Ljava/lang/String;
    :cond_2
    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant;->HTTP_GSLB_GLOBAL_REDIRECTION_SERVER_HOST_DEV:Ljava/lang/String;

    .line 73
    .restart local v2    # "gslbHost":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 74
    const-string v4, "CallState(): [HTTPS] Getting OnPremise GSLB HOST from Shared-Pref for other country."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 75
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v2

    .line 81
    :cond_3
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "https://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    .line 76
    :cond_4
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "ro.product_ship"

    invoke-static {v4, v5}, Lcom/samsung/klmsagent/util/KLMSSystemProperties;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->checkQATesting()Z

    move-result v4

    if-nez v4, :cond_3

    .line 78
    const-string v4, "CallState(): [HTTPS] Getting gslb host from constant for other country."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 79
    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant;->HTTP_GSLB_GLOBAL_REDIRECTION_SERVER_HOST:Ljava/lang/String;

    goto :goto_4

    .line 85
    .end local v1    # "country":Ljava/lang/String;
    .end local v2    # "gslbHost":Ljava/lang/String;
    :cond_5
    const-string v4, "HTTPS"

    invoke-static {v4, p2}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->getKLMSServerAddr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "KLMSServerAddr":Ljava/lang/String;
    if-nez v0, :cond_6

    .line 87
    const/4 p1, 0x0

    goto/16 :goto_1

    .line 89
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "https://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_1

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setCallType(Lcom/samsung/klmsagent/http/CallState$CallType;)V
    .locals 0
    .param p1, "callType"    # Lcom/samsung/klmsagent/http/CallState$CallType;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/samsung/klmsagent/http/CallState;->callType:Lcom/samsung/klmsagent/http/CallState$CallType;

    .line 122
    return-void
.end method

.method public setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V
    .locals 0
    .param p1, "currentConnection"    # Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/klmsagent/http/CallState;->currentConnection:Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    .line 106
    return-void
.end method

.method public setTryCount(I)V
    .locals 0
    .param p1, "tryCount"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/samsung/klmsagent/http/CallState;->tryCount:I

    .line 114
    return-void
.end method
