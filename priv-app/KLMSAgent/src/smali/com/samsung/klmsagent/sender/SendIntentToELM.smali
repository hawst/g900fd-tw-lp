.class public Lcom/samsung/klmsagent/sender/SendIntentToELM;
.super Lcom/samsung/klmsagent/sender/AbstractSendIntentToELM;
.source "SendIntentToELM.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SendIntentToELM(): "

.field public static final TYPE_KLMS_ACTIVE:I = 0x64

.field public static final TYPE_KLMS_RESPONSE:I = 0x12c

.field public static final TYPE_KLMS_STATUS_CHANGED:I = 0xc8


# instance fields
.field private ELM_PACKAGENAME:Ljava/lang/String;

.field private ELM_PERMISSION:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/samsung/klmsagent/sender/AbstractSendIntentToELM;-><init>()V

    .line 16
    const-string v0, "com.sec.esdk.elm.permission.ELM_INTENT_RECEIVE"

    iput-object v0, p0, Lcom/samsung/klmsagent/sender/SendIntentToELM;->ELM_PERMISSION:Ljava/lang/String;

    .line 17
    const-string v0, "com.sec.esdk.elm"

    iput-object v0, p0, Lcom/samsung/klmsagent/sender/SendIntentToELM;->ELM_PACKAGENAME:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected GetIntent(IZI)Landroid/content/Intent;
    .locals 2
    .param p1, "IntentType"    # I
    .param p2, "KLMStatus"    # Z
    .param p3, "ErrorNum"    # I

    .prologue
    .line 44
    const/4 v0, 0x0

    .line 45
    .local v0, "intent":Landroid/content/Intent;
    sparse-switch p1, :sswitch_data_0

    .line 57
    const-string v1, "SendIntentToELM(): GetIntent(defalut): No matching case."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 61
    :goto_0
    if-eqz v0, :cond_0

    .line 62
    iget-object v1, p0, Lcom/samsung/klmsagent/sender/SendIntentToELM;->ELM_PACKAGENAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    :cond_0
    return-object v0

    .line 47
    :sswitch_0
    invoke-virtual {p0, p2, p3}, Lcom/samsung/klmsagent/sender/SendIntentToELM;->GetKLMActive(ZI)Landroid/content/Intent;

    move-result-object v0

    .line 48
    goto :goto_0

    .line 50
    :sswitch_1
    invoke-virtual {p0, p2, p3}, Lcom/samsung/klmsagent/sender/SendIntentToELM;->GetKLMStatusChanged(ZI)Landroid/content/Intent;

    move-result-object v0

    .line 51
    goto :goto_0

    .line 53
    :sswitch_2
    invoke-virtual {p0, p2, p3}, Lcom/samsung/klmsagent/sender/SendIntentToELM;->GetKLMResponse(ZI)Landroid/content/Intent;

    move-result-object v0

    .line 54
    goto :goto_0

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
    .end sparse-switch
.end method

.method public SendIntent(Landroid/content/Context;IZI)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "IntentType"    # I
    .param p3, "KLMStatus"    # Z
    .param p4, "ErrorNum"    # I

    .prologue
    .line 21
    invoke-virtual {p0, p2, p3, p4}, Lcom/samsung/klmsagent/sender/SendIntentToELM;->GetIntent(IZI)Landroid/content/Intent;

    move-result-object v0

    .line 22
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 23
    const-string v1, "SendIntentToELM(): GetIntent() is NULL."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 29
    :goto_0
    return-void

    .line 26
    :cond_0
    iget-object v1, p0, Lcom/samsung/klmsagent/sender/SendIntentToELM;->ELM_PERMISSION:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 28
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SendIntentToELM(): SendIntent().ErrorNum: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public SendIntent(Landroid/content/Context;IZILjava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "IntentType"    # I
    .param p3, "KLMStatus"    # Z
    .param p4, "ErrorNum"    # I
    .param p5, "KeyType"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/samsung/klmsagent/sender/SendIntentToELM;->GetIntent(IZILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 33
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 34
    const-string v1, "SendIntentToELM(): GetIntent() is NULL."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 40
    :goto_0
    return-void

    .line 37
    :cond_0
    iget-object v1, p0, Lcom/samsung/klmsagent/sender/SendIntentToELM;->ELM_PERMISSION:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 39
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SendIntentToELM(): SendIntent().ErrorNum: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0
.end method
