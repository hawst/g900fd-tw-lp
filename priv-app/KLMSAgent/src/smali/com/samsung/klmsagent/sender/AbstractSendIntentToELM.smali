.class public abstract Lcom/samsung/klmsagent/sender/AbstractSendIntentToELM;
.super Ljava/lang/Object;
.source "AbstractSendIntentToELM.java"


# instance fields
.field private final INTENT_KLMS_ACTIVE:Ljava/lang/String;

.field private final INTENT_KLMS_RESPONSE:Ljava/lang/String;

.field private final INTENT_KLMS_STATUS_CHANGED:Ljava/lang/String;

.field private final KLMS_ERROR:Ljava/lang/String;

.field private final KLMS_KEY_TYPE:Ljava/lang/String;

.field private final KLMS_STATUS:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string v0, "com.samsung.klmsagent.action.KLMS_RESPONSE"

    iput-object v0, p0, Lcom/samsung/klmsagent/sender/AbstractSendIntentToELM;->INTENT_KLMS_RESPONSE:Ljava/lang/String;

    .line 8
    const-string v0, "com.samsung.klmsagent.action.KLMS_STATUS_CHANGED"

    iput-object v0, p0, Lcom/samsung/klmsagent/sender/AbstractSendIntentToELM;->INTENT_KLMS_STATUS_CHANGED:Ljava/lang/String;

    .line 9
    const-string v0, "com.samsung.klmsagent.action.KLMS_ACTIVE"

    iput-object v0, p0, Lcom/samsung/klmsagent/sender/AbstractSendIntentToELM;->INTENT_KLMS_ACTIVE:Ljava/lang/String;

    .line 12
    const-string v0, "KLMS_KEY_STATUS"

    iput-object v0, p0, Lcom/samsung/klmsagent/sender/AbstractSendIntentToELM;->KLMS_STATUS:Ljava/lang/String;

    .line 13
    const-string v0, "KLMS_ERROR_NUMBER"

    iput-object v0, p0, Lcom/samsung/klmsagent/sender/AbstractSendIntentToELM;->KLMS_ERROR:Ljava/lang/String;

    .line 14
    const-string v0, "KLMS_KEY_TYPE"

    iput-object v0, p0, Lcom/samsung/klmsagent/sender/AbstractSendIntentToELM;->KLMS_KEY_TYPE:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected abstract GetIntent(IZI)Landroid/content/Intent;
.end method

.method protected GetIntent(IZILjava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p1, "IntentType"    # I
    .param p2, "KLMStatus"    # Z
    .param p3, "ErrorNum"    # I
    .param p4, "KeyType"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/klmsagent/sender/AbstractSendIntentToELM;->GetIntent(IZI)Landroid/content/Intent;

    move-result-object v0

    .line 32
    .local v0, "result":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 33
    const-string v1, "KLMS_KEY_TYPE"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    :cond_0
    return-object v0
.end method

.method protected GetKLMActive(ZI)Landroid/content/Intent;
    .locals 2
    .param p1, "KLMStatus"    # Z
    .param p2, "ErrorNum"    # I

    .prologue
    .line 77
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.klmsagent.action.KLMS_ACTIVE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "result":Landroid/content/Intent;
    const-string v1, "KLMS_KEY_STATUS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 79
    const-string v1, "KLMS_ERROR_NUMBER"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 80
    return-object v0
.end method

.method protected GetKLMResponse(ZI)Landroid/content/Intent;
    .locals 2
    .param p1, "KLMStatus"    # Z
    .param p2, "ErrorNum"    # I

    .prologue
    .line 61
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.klmsagent.action.KLMS_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 62
    .local v0, "result":Landroid/content/Intent;
    const-string v1, "KLMS_KEY_STATUS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 63
    const-string v1, "KLMS_ERROR_NUMBER"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 64
    return-object v0
.end method

.method protected GetKLMStatusChanged(ZI)Landroid/content/Intent;
    .locals 2
    .param p1, "KLMStatus"    # Z
    .param p2, "ErrorNum"    # I

    .prologue
    .line 93
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.klmsagent.action.KLMS_STATUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 94
    .local v0, "result":Landroid/content/Intent;
    const-string v1, "KLMS_KEY_STATUS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 95
    const-string v1, "KLMS_ERROR_NUMBER"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 96
    return-object v0
.end method
