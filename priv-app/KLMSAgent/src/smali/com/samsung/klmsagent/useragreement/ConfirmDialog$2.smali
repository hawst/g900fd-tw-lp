.class Lcom/samsung/klmsagent/useragreement/ConfirmDialog$2;
.super Ljava/lang/Object;
.source "ConfirmDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;


# direct methods
.method constructor <init>(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$2;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 100
    const-string v0, "ConfirmDialog(): Disagree Button Pressed."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 101
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setEula(Z)V

    .line 102
    invoke-static {}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->disagreeEula()V

    .line 103
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setEulaCancelButtonPress(Z)V

    .line 104
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$2;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    invoke-virtual {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->finish()V

    .line 105
    return-void
.end method
