.class public Lcom/samsung/klmsagent/useragreement/ConfirmDialog;
.super Landroid/app/Activity;
.source "ConfirmDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ConfirmDialog(): "

.field private static extras:Landroid/os/Bundle;

.field static packageName:Ljava/lang/String;

.field static state:Lcom/samsung/klmsagent/context/State;


# instance fields
.field private GSLBDaiclaimerServerAddr:Ljava/lang/String;

.field private GSLBDisclaimerKor:Ljava/lang/String;

.field private cb_agree:Landroid/widget/ImageView;

.field private check_confirm:Z

.field private iv_agree:Landroid/widget/ImageView;

.field licenseKey:Ljava/lang/String;

.field private mScrollView:Landroid/widget/ScrollView;

.field packageVersion:Ljava/lang/String;

.field private sViewX:I

.field private sViewY:I

.field private tr_agree:Landroid/widget/LinearLayout;

.field private tv_agree:Landroid/widget/TextView;

.field private tv_agreeView:Landroid/widget/TextView;

.field private tv_layout_agree:Landroid/widget/LinearLayout;

.field private tv_layout_disagree:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    sput-object v0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;

    .line 45
    sput-object v0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->check_confirm:Z

    .line 32
    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tr_agree:Landroid/widget/LinearLayout;

    .line 33
    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;

    .line 34
    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agreeView:Landroid/widget/TextView;

    .line 36
    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    .line 37
    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;

    .line 38
    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    .line 39
    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_disagree:Landroid/widget/LinearLayout;

    .line 40
    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->mScrollView:Landroid/widget/ScrollView;

    .line 43
    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->GSLBDaiclaimerServerAddr:Ljava/lang/String;

    .line 44
    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->GSLBDisclaimerKor:Ljava/lang/String;

    .line 46
    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->packageVersion:Ljava/lang/String;

    .line 47
    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->licenseKey:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->check_confirm:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/klmsagent/useragreement/ConfirmDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->check_confirm:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->sViewX:I

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->sViewY:I

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method public static disagreeEula()V
    .locals 6

    .prologue
    const/16 v1, 0x1e61

    .line 231
    const-string v0, "ConfirmDialog(): EULA Disagreed."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 232
    new-instance v0, Lcom/samsung/klmsagent/context/State;

    invoke-direct {v0, v1}, Lcom/samsung/klmsagent/context/State;-><init>(I)V

    sput-object v0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->state:Lcom/samsung/klmsagent/context/State;

    .line 233
    sget-object v0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->state:Lcom/samsung/klmsagent/context/State;

    invoke-virtual {v0}, Lcom/samsung/klmsagent/context/State;->takeActionGeneral()V

    .line 234
    const/16 v0, 0x1f4

    const/16 v2, 0x384

    const/16 v3, 0x320

    const-string v4, "fail"

    sget-object v5, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 237
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 212
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 213
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setEulaBackButtonPress(Z)V

    .line 214
    const-string v0, "ConfirmDialog(): BACK Button Pressed."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 215
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setEula(Z)V

    .line 216
    invoke-static {}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->disagreeEula()V

    .line 217
    invoke-virtual {p0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->finish()V

    .line 218
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 53
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const-string v1, "ConfirmDialog(): onCreate()"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 56
    const v1, 0x7f030002

    invoke-virtual {p0, v1}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->setContentView(I)V

    .line 57
    const v1, 0x7f090003

    invoke-virtual {p0, v1}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->mScrollView:Landroid/widget/ScrollView;

    .line 59
    invoke-virtual {p0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    sput-object v1, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;

    .line 60
    sget-object v1, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;

    const-string v2, "packageName"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    .line 62
    const-string v1, "https://account.samsung.com/membership/pp"

    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->GSLBDaiclaimerServerAddr:Ljava/lang/String;

    .line 63
    const-string v1, "http://eula.secb2b.com/eula/en"

    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->GSLBDisclaimerKor:Ljava/lang/String;

    .line 66
    const v1, 0x7f09000a

    invoke-virtual {p0, v1}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    .line 67
    const v1, 0x7f09000d

    invoke-virtual {p0, v1}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;

    .line 68
    const v1, 0x7f09000e

    invoke-virtual {p0, v1}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    .line 69
    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 70
    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$1;

    invoke-direct {v2, p0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$1;-><init>(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    const v1, 0x7f090008

    invoke-virtual {p0, v1}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_disagree:Landroid/widget/LinearLayout;

    .line 96
    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_disagree:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 97
    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_disagree:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$2;

    invoke-direct {v2, p0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$2;-><init>(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    const v1, 0x7f090006

    invoke-virtual {p0, v1}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agreeView:Landroid/widget/TextView;

    .line 110
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ro.csc.country_code"

    invoke-static {v1, v2}, Lcom/samsung/klmsagent/util/KLMSSystemProperties;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "country":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "Korea"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    const-string v1, "ConfirmDialog(): EULA is Displayed only for KOREAN DEVICES."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agreeView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060036

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->GSLBDisclaimerKor:Ljava/lang/String;

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->GSLBDisclaimerKor:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    :goto_0
    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agreeView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 123
    const v1, 0x7f090004

    invoke-virtual {p0, v1}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tr_agree:Landroid/widget/LinearLayout;

    .line 124
    const v1, 0x7f090005

    invoke-virtual {p0, v1}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;

    .line 125
    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tr_agree:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$3;

    invoke-direct {v2, p0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$3;-><init>(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;

    new-instance v2, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;

    invoke-direct {v2, p0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;-><init>(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    return-void

    .line 116
    :cond_0
    const-string v1, "ConfirmDialog(): EULA is Displayed for GLOBAL DEVICES."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agreeView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060034

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->GSLBDaiclaimerServerAddr:Ljava/lang/String;

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->GSLBDaiclaimerServerAddr:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f06000f

    .line 182
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 184
    const-string v0, "sViewX"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->sViewX:I

    .line 185
    const-string v0, "sViewY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->sViewY:I

    .line 186
    const-string v0, "check_confirm"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->check_confirm:Z

    .line 188
    iget-boolean v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->check_confirm:Z

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;

    const v1, 0x7f020017

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 190
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 191
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 192
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 193
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    :goto_0
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->mScrollView:Landroid/widget/ScrollView;

    new-instance v1, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$5;

    invoke-direct {v1, p0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$5;-><init>(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 208
    return-void

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;

    const v1, 0x7f020015

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 196
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 197
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;

    const v1, 0x7f02000d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 198
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    const-string v1, "#888888"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 199
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 168
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 169
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 174
    const-string v0, "sViewX"

    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 175
    const-string v0, "sViewY"

    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 176
    const-string v0, "check_confirm"

    iget-boolean v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->check_confirm:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 177
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 178
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 222
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 223
    invoke-static {}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->HomeButton()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ConfirmDialog(): HOME Button Pressed. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 225
    invoke-static {}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->disagreeEula()V

    .line 226
    invoke-virtual {p0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->finish()V

    .line 228
    :cond_0
    return-void
.end method
