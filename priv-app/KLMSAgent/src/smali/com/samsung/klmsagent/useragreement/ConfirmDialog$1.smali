.class Lcom/samsung/klmsagent/useragreement/ConfirmDialog$1;
.super Ljava/lang/Object;
.source "ConfirmDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;


# direct methods
.method constructor <init>(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$1;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v1, 0x270f

    const/4 v2, 0x1

    .line 73
    const-string v0, "ConfirmDialog(): EULA Agree Button Clicked."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 74
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setEula(Z)V

    .line 77
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSUtility;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    const-string v0, "ConfirmDialog(): Network not available before enroll device"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 80
    const/16 v0, 0xc8

    const/16 v2, 0x384

    const/16 v3, 0x320

    const-string v4, "fail"

    sget-object v5, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 83
    new-instance v6, Lcom/samsung/klmsagent/context/State;

    invoke-direct {v6, v1}, Lcom/samsung/klmsagent/context/State;-><init>(I)V

    .line 84
    .local v6, "state":Lcom/samsung/klmsagent/context/State;
    invoke-virtual {v6}, Lcom/samsung/klmsagent/context/State;->takeActionGeneral()V

    .line 85
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$1;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    invoke-virtual {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->finish()V

    .line 91
    .end local v6    # "state":Lcom/samsung/klmsagent/context/State;
    :goto_0
    return-void

    .line 88
    :cond_0
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponse(Ljava/lang/Boolean;)V

    .line 89
    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;
    invoke-static {}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$000()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "b2b"

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->setKLMSServerAddr(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$1;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    invoke-virtual {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->finish()V

    goto :goto_0
.end method
