.class Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;
.super Ljava/lang/Object;
.source "ConfirmDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;


# direct methods
.method constructor <init>(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v4, 0x7f06000f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 148
    iget-object v3, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->check_confirm:Z
    invoke-static {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$100(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    # setter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->check_confirm:Z
    invoke-static {v3, v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$102(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;Z)Z

    .line 149
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->check_confirm:Z
    invoke-static {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$100(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$200(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/ImageView;

    move-result-object v0

    const v2, 0x7f020002

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 151
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$300(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 152
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$400(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 153
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$500(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 154
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$500(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    invoke-virtual {v1, v4}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 148
    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$200(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/ImageView;

    move-result-object v0

    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 157
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$300(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 158
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$400(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02000d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 159
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$500(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "#888888"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 160
    iget-object v0, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    # getter for: Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->access$500(Lcom/samsung/klmsagent/useragreement/ConfirmDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/klmsagent/useragreement/ConfirmDialog$4;->this$0:Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    invoke-virtual {v1, v4}, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
