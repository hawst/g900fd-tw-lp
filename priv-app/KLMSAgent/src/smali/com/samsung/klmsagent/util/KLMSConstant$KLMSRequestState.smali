.class public final enum Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;
.super Ljava/lang/Enum;
.source "KLMSConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/util/KLMSConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "KLMSRequestState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

.field public static final enum ALL:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

.field public static final enum CA:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

.field public static final enum ELM:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

.field public static final enum MDM:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

.field public static final enum MDM_ELM:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 163
    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->ALL:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    const-string v1, "MDM"

    invoke-direct {v0, v1, v3}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->MDM:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    const-string v1, "ELM"

    invoke-direct {v0, v1, v4}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->ELM:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    const-string v1, "CA"

    invoke-direct {v0, v1, v5}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->CA:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    const-string v1, "MDM_ELM"

    invoke-direct {v0, v1, v6}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->MDM_ELM:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    .line 162
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->ALL:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->MDM:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->ELM:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->CA:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->MDM_ELM:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->$VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 162
    const-class v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;
    .locals 1

    .prologue
    .line 162
    sget-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->$VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    invoke-virtual {v0}, [Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;

    return-object v0
.end method
