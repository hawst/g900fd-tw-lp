.class public final enum Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;
.super Ljava/lang/Enum;
.source "KLMSConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/util/KLMSConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "KLMSValidationState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

.field public static final enum negative:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

.field public static final enum positive:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 167
    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

    const-string v1, "positive"

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;->positive:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

    const-string v1, "negative"

    invoke-direct {v0, v1, v3}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;->negative:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

    .line 166
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;->positive:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;->negative:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;->$VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 166
    const-class v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;->$VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

    invoke-virtual {v0}, [Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;

    return-object v0
.end method
