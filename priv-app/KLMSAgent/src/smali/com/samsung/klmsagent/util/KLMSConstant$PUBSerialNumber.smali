.class public final enum Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;
.super Ljava/lang/Enum;
.source "KLMSConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/util/KLMSConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PUBSerialNumber"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

.field public static final enum B2C_CONTAINER_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

.field public static final enum B2C_RP_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

.field public static final enum CLOUD_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

.field public static final enum ONPREM_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;


# instance fields
.field private alias:Ljava/lang/String;

.field private mPUBSerialNumber:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 307
    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    const-string v1, "CLOUD_SN"

    const-string v2, "7E35E09906637A0794DE040F68A0605E657A19ED7023CBE7E44DE2D36D0196F5"

    const-string v3, "klm-b2b"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->CLOUD_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    .line 308
    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    const-string v1, "ONPREM_SN"

    const-string v2, "F6EA2CEDFA8880383B4560265A6FEC55F512C248D1DEBBDB8950AA0E49608B54"

    const-string v3, "klm-onprem"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->ONPREM_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    .line 309
    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    const-string v1, "B2C_RP_SN"

    const-string v2, "B2EF5B5A76A7542C1E8316996AE8C87ABA56407C2AF293422BA567879393B8B9"

    const-string v3, "klm-b2c-rp"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->B2C_RP_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    .line 310
    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    const-string v1, "B2C_CONTAINER_SN"

    const-string v2, "3E47DFC18BDF453CE4CD356B6C6D6FC73D35FACD56154FD4761AC456EF3A859D"

    const-string v3, "klm-b2c"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->B2C_CONTAINER_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    .line 306
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->CLOUD_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->ONPREM_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->B2C_RP_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->B2C_CONTAINER_SN:Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    aput-object v1, v0, v7

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->$VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p3, "SerialNumber"    # Ljava/lang/String;
    .param p4, "mAlias"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 315
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 312
    iput-object v0, p0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->mPUBSerialNumber:Ljava/lang/String;

    .line 313
    iput-object v0, p0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->alias:Ljava/lang/String;

    .line 316
    iput-object p3, p0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->mPUBSerialNumber:Ljava/lang/String;

    .line 317
    iput-object p4, p0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->alias:Ljava/lang/String;

    .line 318
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 306
    const-class v0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    return-object v0
.end method

.method public static values()[Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;
    .locals 1

    .prologue
    .line 306
    sget-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->$VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    invoke-virtual {v0}, [Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    return-object v0
.end method


# virtual methods
.method public getAlias()Ljava/lang/String;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->alias:Ljava/lang/String;

    return-object v0
.end method

.method public getSerialNumebr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->mPUBSerialNumber:Ljava/lang/String;

    return-object v0
.end method
