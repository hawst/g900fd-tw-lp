.class public Lcom/samsung/klmsagent/util/KLMSLogger;
.super Ljava/lang/Object;
.source "KLMSLogger.java"


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSLogger;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 43
    sget-boolean v0, Lcom/samsung/klmsagent/util/KLMSConstant;->isDebugLogEnable:Z

    if-eqz v0, :cond_0

    .line 44
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSLogger;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;)V
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 31
    sget-boolean v0, Lcom/samsung/klmsagent/util/KLMSConstant;->isErrorLogEnable:Z

    if-eqz v0, :cond_0

    .line 32
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSLogger;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "value"    # Ljava/lang/String;
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 37
    sget-boolean v0, Lcom/samsung/klmsagent/util/KLMSConstant;->isErrorLogEnable:Z

    if-eqz v0, :cond_0

    .line 38
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSLogger;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 40
    :cond_0
    return-void
.end method

.method private static getTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getVersionName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KLMS-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getVersionName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSLogger;->TAG:Ljava/lang/String;

    .line 23
    :goto_0
    sget-object v0, Lcom/samsung/klmsagent/util/KLMSLogger;->TAG:Ljava/lang/String;

    return-object v0

    .line 21
    :cond_0
    const-string v0, "KLMS: "

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSLogger;->TAG:Ljava/lang/String;

    goto :goto_0
.end method

.method public static i(Ljava/lang/String;)V
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 49
    sget-boolean v0, Lcom/samsung/klmsagent/util/KLMSConstant;->isInfoLogEnable:Z

    if-eqz v0, :cond_0

    .line 50
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSLogger;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :cond_0
    return-void
.end method

.method public static popupNew(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 68
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 69
    return-void
.end method

.method public static u(Ljava/lang/String;)V
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 61
    sget-boolean v0, Lcom/samsung/klmsagent/util/KLMSConstant;->isUserBuild:Z

    if-eqz v0, :cond_0

    .line 62
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSLogger;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    :cond_0
    return-void
.end method

.method public static v(Ljava/lang/String;)V
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 55
    sget-boolean v0, Lcom/samsung/klmsagent/util/KLMSConstant;->isVerboseLogEnable:Z

    if-eqz v0, :cond_0

    .line 56
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSLogger;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_0
    return-void
.end method
