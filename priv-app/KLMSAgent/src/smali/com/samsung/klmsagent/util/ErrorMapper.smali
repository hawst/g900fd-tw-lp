.class public Lcom/samsung/klmsagent/util/ErrorMapper;
.super Ljava/lang/Object;
.source "ErrorMapper.java"


# static fields
.field public static final ERROR_LICENSE_ACTIVATION_NOT_FOUND:I = 0x2bf

.field public static final ERROR_NETWORK_GENERAL:I = 0x1f6

.field public static final ERROR_NOT_CURRENT_DATE:I = 0xcd

.field public static final ERROR_UNKNOWN:I = 0x66

.field private static MDM_ERROR_ERROR_NULL_PARAMS:I = 0x0

.field private static MDM_ERROR_INTERNAL_SERVER:I = 0x0

.field private static MDM_ERROR_INVALID_LICENSE:I = 0x0

.field private static MDM_ERROR_LICENSE_DEACTIVATED:I = 0x0

.field private static MDM_ERROR_LICENSE_EXPIRED:I = 0x0

.field private static MDM_ERROR_LICENSE_QUANTITY_EXHAUSTED:I = 0x0

.field private static MDM_ERROR_LICENSE_TERMINATED:I = 0x0

.field private static MDM_ERROR_NETWORK_DISCONNECTED:I = 0x0

.field private static MDM_ERROR_NONE:I = 0x0

.field private static MDM_USER_DISAGREE:I = 0x0

.field protected static final TAG:Ljava/lang/String; = "ErrorMapper(): "


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    sput v0, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_NONE:I

    .line 12
    const/16 v0, 0x2bc

    sput v0, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_LICENSE_DEACTIVATED:I

    .line 13
    const/16 v0, 0x2bd

    sput v0, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_LICENSE_EXPIRED:I

    .line 14
    const/16 v0, 0xc9

    sput v0, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_INVALID_LICENSE:I

    .line 15
    const/16 v0, 0xcb

    sput v0, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_LICENSE_TERMINATED:I

    .line 16
    const/16 v0, 0x2be

    sput v0, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_LICENSE_QUANTITY_EXHAUSTED:I

    .line 17
    const/16 v0, 0x1f5

    sput v0, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_NETWORK_DISCONNECTED:I

    .line 18
    const/16 v0, 0x65

    sput v0, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_ERROR_NULL_PARAMS:I

    .line 19
    const/16 v0, 0x191

    sput v0, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_INTERNAL_SERVER:I

    .line 20
    const/16 v0, 0x259

    sput v0, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_USER_DISAGREE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getErrorDescription(II)Landroid/app/enterprise/license/Error;
    .locals 3
    .param p0, "http_code"    # I
    .param p1, "err_code"    # I

    .prologue
    .line 27
    const/4 v0, 0x0

    .line 28
    .local v0, "error":Landroid/app/enterprise/license/Error;
    sparse-switch p1, :sswitch_data_0

    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorMapper(): MDM error code sent(default):"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_INTERNAL_SERVER:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 93
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    sget v1, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_INTERNAL_SERVER:I

    const-string v2, "Internal server error."

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 96
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    :goto_0
    return-object v0

    .line 32
    :sswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorMapper(): MDM error code sent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_NONE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 33
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    sget v1, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_NONE:I

    const-string v2, "Success."

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 34
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto :goto_0

    .line 36
    :sswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorMapper(): MDM error code sent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_LICENSE_EXPIRED:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 37
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    sget v1, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_LICENSE_EXPIRED:I

    const-string v2, "General license expired."

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 38
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto :goto_0

    .line 40
    :sswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorMapper(): MDM error code sent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_LICENSE_DEACTIVATED:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 41
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    sget v1, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_LICENSE_DEACTIVATED:I

    const-string v2, "General license deactivated."

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 42
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto :goto_0

    .line 44
    :sswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorMapper(): MDM error code sent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_INVALID_LICENSE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 45
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    sget v1, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_INVALID_LICENSE:I

    const-string v2, "Invalid license"

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 46
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto/16 :goto_0

    .line 48
    :sswitch_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorMapper(): MDM error code sent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_LICENSE_TERMINATED:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 49
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    sget v1, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_LICENSE_TERMINATED:I

    const-string v2, "License terminated."

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 50
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto/16 :goto_0

    .line 52
    :sswitch_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorMapper(): MDM error code sent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_LICENSE_QUANTITY_EXHAUSTED:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 53
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    sget v1, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_LICENSE_QUANTITY_EXHAUSTED:I

    const-string v2, "General license quantity exhausted."

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 55
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto/16 :goto_0

    .line 57
    :sswitch_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorMapper(): MDM error code sent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_ERROR_NULL_PARAMS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 58
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    sget v1, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_ERROR_NULL_PARAMS:I

    const-string v2, "Null parameter"

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 59
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto/16 :goto_0

    .line 61
    :sswitch_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorMapper(): MDM error code sent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_NETWORK_DISCONNECTED:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 62
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    sget v1, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_NETWORK_DISCONNECTED:I

    const-string v2, "Network disconnected."

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 63
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto/16 :goto_0

    .line 65
    :sswitch_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorMapper(): MDM error code sent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_INTERNAL_SERVER:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 66
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    sget v1, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_ERROR_INTERNAL_SERVER:I

    const-string v2, "Internal server error."

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 67
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto/16 :goto_0

    .line 69
    :sswitch_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorMapper(): MDM error code sent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_USER_DISAGREE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 70
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    sget v1, Lcom/samsung/klmsagent/util/ErrorMapper;->MDM_USER_DISAGREE:I

    const-string v2, "User Disagree."

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 71
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto/16 :goto_0

    .line 75
    :sswitch_a
    const-string v1, "ErrorMapper(): MDM error code sent : 703"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 76
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    const/16 v1, 0x2bf

    const-string v2, "License deactivation error."

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 78
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto/16 :goto_0

    .line 80
    :sswitch_b
    const-string v1, "ErrorMapper(): MDM error code sent : 205"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 81
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    const/16 v1, 0xcd

    const-string v2, "wrong date error."

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 82
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto/16 :goto_0

    .line 84
    :sswitch_c
    const-string v1, "ErrorMapper(): MDM error code sent : 102"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 85
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    const/16 v1, 0x66

    const-string v2, "Unknown Error"

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 86
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto/16 :goto_0

    .line 88
    :sswitch_d
    const-string v1, "ErrorMapper(): MDM error code sent : 502"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 89
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    const/16 v1, 0x1f6

    const-string v2, "General network error."

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 90
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    goto/16 :goto_0

    .line 28
    nop

    :sswitch_data_0
    .sparse-switch
        0x66 -> :sswitch_c
        0x1f6 -> :sswitch_d
        0x2bf -> :sswitch_a
        0x3e8 -> :sswitch_0
        0x3e9 -> :sswitch_6
        0xbb9 -> :sswitch_b
        0xc1d -> :sswitch_1
        0xc1e -> :sswitch_2
        0xc1f -> :sswitch_3
        0xc20 -> :sswitch_4
        0xc2b -> :sswitch_0
        0xc2c -> :sswitch_a
        0xc32 -> :sswitch_5
        0xc35 -> :sswitch_0
        0xc3a -> :sswitch_a
        0x1e61 -> :sswitch_9
        0x22b8 -> :sswitch_8
        0x270f -> :sswitch_7
    .end sparse-switch
.end method
