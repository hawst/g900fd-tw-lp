.class public Lcom/samsung/klmsagent/util/KLMSUtility;
.super Ljava/lang/Object;
.source "KLMSUtility.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/util/KLMSUtility$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "KLMSUtility(): "


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method public static IsPackageExistInDevice(Ljava/lang/String;)Z
    .locals 8
    .param p0, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 342
    const/4 v4, 0x0

    .line 344
    .local v4, "result":Z
    :try_start_0
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/16 v7, 0x2000

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v3

    .line 346
    .local v3, "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    if-nez v3, :cond_0

    .line 347
    const-string v6, "KLMSUtility(): IsPackageExistInDevice().mAppInfoList: Null"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    move v5, v4

    .line 363
    .end local v3    # "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v4    # "result":Z
    .local v5, "result":I
    :goto_0
    return v5

    .line 351
    .end local v5    # "result":I
    .restart local v3    # "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v4    # "result":Z
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "KLMSUtility(): IsPackageExistInDevice().mAppInfoList: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 352
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 353
    .local v0, "appInfo":Landroid/content/pm/PackageInfo;
    iget-object v6, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 354
    const/4 v4, 0x1

    .line 355
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "KLMSUtility(): IsPackageExistInDevice: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " exist."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "appInfo":Landroid/content/pm/PackageInfo;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_2
    :goto_1
    move v5, v4

    .line 363
    .restart local v5    # "result":I
    goto :goto_0

    .line 359
    .end local v5    # "result":I
    :catch_0
    move-exception v1

    .line 360
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "TAG + IsPackageExistInDevice() has Exception."

    invoke-static {v6, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 361
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private static bytesToHexString([B)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # [B

    .prologue
    .line 130
    const/4 v2, 0x2

    invoke-static {p0, v2}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    .line 131
    .local v0, "hex":[B
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    .line 132
    .local v1, "str":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KLMSUtility(): bytesToHexString() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 133
    return-object v1
.end method

.method public static checkForOnPremiseDetails(Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .param p0, "licenseKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 228
    if-nez p0, :cond_1

    .line 229
    const-string v2, "KLMSUtility(): checkForOnPremiseDetails(): Licenesekey == NULL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    move-object v0, v1

    .line 266
    :cond_0
    :goto_0
    return-object v0

    .line 233
    :cond_1
    invoke-static {p0}, Lcom/samsung/klmsagent/util/KLMSUtility;->unpackKey(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 234
    .local v0, "unPackDt":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 235
    const-string v2, "gslb"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "gslb"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 237
    const-string v1, "KLMSUtility(): This key has on premise details."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 238
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    const-string v1, "gslb"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setOnPremiseGSLBUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 241
    :cond_2
    const-string v2, "KLMSUtility(): No gslb url present."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 242
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 243
    const-string v2, "KLMSUtility(): Resetting the gslb of on premise."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 244
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setOnPremiseGSLBUrl(Ljava/lang/String;)V

    .line 245
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremisePolicy()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 246
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setOnPremisePolicy(Lorg/json/JSONObject;)V

    .line 248
    :cond_3
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseKADUrlForCA()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 249
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setOnPremiseKADUrlForCA(Ljava/lang/String;)V

    goto :goto_0

    .line 254
    :cond_4
    const-string v2, "KLMSUtility(): No details fetched from the license key."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 255
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 256
    const-string v2, "KLMSUtility(): Resetting the gslb of on premise."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 257
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setOnPremiseGSLBUrl(Ljava/lang/String;)V

    .line 258
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremisePolicy()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 259
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setOnPremisePolicy(Lorg/json/JSONObject;)V

    .line 261
    :cond_5
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseKADUrlForCA()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 262
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setOnPremiseKADUrlForCA(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static checkRightsToCallUrl(Ljava/lang/String;)Z
    .locals 6
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 270
    const-string v5, "KLMSUtility(): Checking the rights object."

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 271
    if-eqz p0, :cond_4

    .line 272
    invoke-static {}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->getUrlToPolicyMapper()Ljava/util/Map;

    move-result-object v0

    .line 273
    .local v0, "authorization":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v0, :cond_3

    .line 274
    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 276
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremisePolicy()Ljava/lang/String;

    move-result-object v3

    .line 277
    .local v3, "policy":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 279
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 280
    .local v4, "policyJson":Lorg/json/JSONObject;
    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 281
    .local v2, "hasRights":Ljava/lang/String;
    const-string v5, "on"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 282
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 302
    .end local v0    # "authorization":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "hasRights":Ljava/lang/String;
    .end local v3    # "policy":Ljava/lang/String;
    .end local v4    # "policyJson":Lorg/json/JSONObject;
    :goto_0
    return v5

    .line 284
    .restart local v0    # "authorization":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v2    # "hasRights":Ljava/lang/String;
    .restart local v3    # "policy":Ljava/lang/String;
    .restart local v4    # "policyJson":Lorg/json/JSONObject;
    :cond_0
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    goto :goto_0

    .line 285
    .end local v2    # "hasRights":Ljava/lang/String;
    .end local v4    # "policyJson":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 286
    .local v1, "e":Lorg/json/JSONException;
    const-string v5, "Json object corrupted."

    invoke-static {v5, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 288
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    goto :goto_0

    .line 290
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_1
    const-string v5, "KLMSUtility(): No policy set."

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 291
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    goto :goto_0

    .line 294
    .end local v3    # "policy":Ljava/lang/String;
    :cond_2
    const-string v5, "KLMSUtility(): url not mapped."

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 295
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    goto :goto_0

    .line 298
    :cond_3
    const-string v5, "KLMSUtility(): Url Mapper not created."

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 299
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    goto :goto_0

    .line 301
    .end local v0    # "authorization":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    const-string v5, "KLMSUtility(): Malformed url sent."

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 302
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    goto :goto_0
.end method

.method public static getAppListUsingKnoxSdk()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    const/4 v0, 0x0

    .line 188
    .local v0, "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 189
    .local v5, "mList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Landroid/app/enterprise/license/EnterpriseLicenseManager;

    move-result-object v4

    .line 193
    .local v4, "mESDKLicenseManager":Landroid/app/enterprise/license/EnterpriseLicenseManager;
    invoke-virtual {v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getAllLicenseInfo()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 194
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v9, v0

    if-ge v2, v9, :cond_0

    .line 195
    const/4 v8, 0x0

    .line 197
    .local v8, "sInstanceId":Ljava/lang/String;
    const/4 v7, 0x0

    .line 200
    .local v7, "rightObject":Landroid/app/enterprise/license/RightsObject;
    aget-object v9, v0, v2

    invoke-virtual {v9}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 201
    invoke-virtual {v4, v8}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v7

    .line 202
    if-nez v7, :cond_1

    .line 203
    const-string v9, "KLMSUtility(): rightObject is null"

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 224
    .end local v2    # "i":I
    .end local v7    # "rightObject":Landroid/app/enterprise/license/RightsObject;
    .end local v8    # "sInstanceId":Ljava/lang/String;
    :cond_0
    return-object v5

    .line 208
    .restart local v2    # "i":I
    .restart local v7    # "rightObject":Landroid/app/enterprise/license/RightsObject;
    .restart local v8    # "sInstanceId":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 209
    .local v6, "mPackageManager":Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    .line 212
    .local v3, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    aget-object v9, v0, v2

    invoke-virtual {v9}, Landroid/app/enterprise/license/LicenseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 213
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "KLMSUtility(): Fetched package name : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v3, Landroid/content/pm/ApplicationInfo;->name:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :goto_1
    if-eqz v3, :cond_2

    .line 219
    invoke-virtual {v6, v3}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    .end local v3    # "mAppInfo":Landroid/content/pm/ApplicationInfo;
    .end local v6    # "mPackageManager":Landroid/content/pm/PackageManager;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 214
    .restart local v3    # "mAppInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v6    # "mPackageManager":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v1

    .line 215
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v3, 0x0

    .line 216
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getDelayedTimeInMillis(Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;)J
    .locals 4
    .param p0, "failFrequency"    # Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    .prologue
    .line 83
    const-wide/16 v0, 0x0

    .line 84
    .local v0, "timeInMillis":J
    sget-object v2, Lcom/samsung/klmsagent/util/KLMSUtility$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$HttpRetryFrequency:[I

    invoke-virtual {p0}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 98
    const-string v2, "KLMSUtility(): No mapping for this time delay."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 101
    :goto_0
    return-wide v0

    .line 86
    :pswitch_0
    const-wide/32 v0, 0x2bf20

    .line 87
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "time delay: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :pswitch_1
    const-wide/16 v0, 0x3a98

    .line 91
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "time delay: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :pswitch_2
    const-wide/16 v0, 0x1388

    .line 95
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "time delay: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getNotificationManager()Landroid/app/NotificationManager;
    .locals 2

    .prologue
    .line 162
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    .line 163
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    return-object v0
.end method

.method public static getTransactionId()Ljava/lang/String;
    .locals 9

    .prologue
    .line 314
    :try_start_0
    const-string v5, "MD5"

    invoke-static {v5}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 316
    .local v2, "md5":Ljava/security/MessageDigest;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 318
    .local v6, "timeStamp":J
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getRandomNumber()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getDeviceId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v8, "UTF-8"

    invoke-virtual {v5, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v4

    .line 321
    .local v4, "signature":[B
    new-instance v3, Ljava/math/BigInteger;

    const/4 v5, 0x1

    invoke-direct {v3, v5, v4}, Ljava/math/BigInteger;-><init>(I[B)V

    .line 322
    .local v3, "number":Ljava/math/BigInteger;
    const/16 v5, 0x10

    invoke-virtual {v3, v5}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 323
    .local v1, "hextext":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v8, 0x20

    if-ge v5, v8, :cond_0

    .line 324
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "0"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 326
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "KLMSUtility(): getTransactionId(): "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ""

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    .end local v1    # "hextext":Ljava/lang/String;
    .end local v3    # "number":Ljava/math/BigInteger;
    .end local v4    # "signature":[B
    .end local v6    # "timeStamp":J
    :goto_1
    return-object v1

    .line 328
    :catch_0
    move-exception v0

    .line 329
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "getTransactionId() has Exception."

    invoke-static {v5, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 330
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static hasText(Ljava/lang/String;)Z
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 105
    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    const/4 v0, 0x1

    .line 108
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hashImei(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "imei"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 144
    const/4 v0, 0x0

    .line 147
    .local v0, "digest":Ljava/security/MessageDigest;
    :try_start_0
    const-string v4, "SHA-256"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 148
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 149
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSUtility;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v2

    .line 150
    .local v2, "hash":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "KLMSUtility(): hashImei() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 158
    .end local v2    # "hash":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 151
    :catch_0
    move-exception v1

    .line 152
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    move-object v2, v3

    .line 153
    goto :goto_0

    .line 154
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 155
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v2, v3

    .line 156
    goto :goto_0
.end method

.method public static isNetworkAvailable(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 47
    const-string v8, "connectivity"

    invoke-virtual {p0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 48
    .local v2, "connectivity":Landroid/net/ConnectivityManager;
    const/4 v5, 0x0

    .line 49
    .local v5, "isWifiConn":Z
    const/4 v4, 0x0

    .line 51
    .local v4, "isMobileConn":Z
    if-nez v2, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v6

    .line 56
    :cond_1
    const/4 v8, 0x1

    :try_start_0
    invoke-virtual {v2, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 57
    .local v1, "WifiInfo":Landroid/net/NetworkInfo;
    if-nez v1, :cond_3

    .line 58
    const/4 v5, 0x0

    .line 63
    :goto_1
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 64
    .local v0, "MoblieInfo":Landroid/net/NetworkInfo;
    if-nez v0, :cond_4

    .line 65
    const/4 v4, 0x0

    .line 70
    :goto_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSUtility(): isNetworkAvailable() - WIFI : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "| MOBILE : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 72
    if-nez v5, :cond_2

    if-eqz v4, :cond_0

    :cond_2
    move v6, v7

    .line 73
    goto :goto_0

    .line 60
    .end local v0    # "MoblieInfo":Landroid/net/NetworkInfo;
    :cond_3
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    goto :goto_1

    .line 67
    .restart local v0    # "MoblieInfo":Landroid/net/NetworkInfo;
    :cond_4
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    goto :goto_2

    .line 75
    .end local v0    # "MoblieInfo":Landroid/net/NetworkInfo;
    .end local v1    # "WifiInfo":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v3

    .line 76
    .local v3, "e":Ljava/lang/Exception;
    const-string v7, "TAG + isNetworkAvailable() has Exception."

    invoke-static {v7, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static isRunning(Landroid/content/Context;)Z
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 112
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 114
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 115
    .local v3, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 116
    .local v2, "task":Landroid/app/ActivityManager$RunningTaskInfo;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v2, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 119
    .end local v2    # "task":Landroid/app/ActivityManager$RunningTaskInfo;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static showNotification(Ljava/lang/String;)V
    .locals 9
    .param p0, "msgText"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 168
    const-string v5, "KLMSUtility(): showNotification().START"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 169
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getNotificationAppList()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getNotificationAppList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 171
    new-instance v3, Landroid/content/Intent;

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/samsung/klmsagent/activities/TransparentActivity;

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 172
    .local v3, "pendingIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v7, v3, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 173
    .local v4, "pi":Landroid/app/PendingIntent;
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSUtility;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v5

    sget v6, Lcom/samsung/klmsagent/util/KLMSConstant;->NOTIFICATION_STACK_ID:I

    invoke-virtual {v5, v6}, Landroid/app/NotificationManager;->cancel(I)V

    .line 174
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSUtility;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v2

    .line 175
    .local v2, "notificationManager":Landroid/app/NotificationManager;
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 177
    .local v0, "builder":Landroid/app/Notification$Builder;
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f060031

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    const v6, 0x7f020006

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 181
    new-instance v5, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v5, v0}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v5, p0}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    move-result-object v1

    .line 182
    .local v1, "notification":Landroid/app/Notification;
    sget v5, Lcom/samsung/klmsagent/util/KLMSConstant;->NOTIFICATION_STACK_ID:I

    invoke-virtual {v2, v5, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 184
    .end local v0    # "builder":Landroid/app/Notification$Builder;
    .end local v1    # "notification":Landroid/app/Notification;
    .end local v2    # "notificationManager":Landroid/app/NotificationManager;
    .end local v3    # "pendingIntent":Landroid/content/Intent;
    .end local v4    # "pi":Landroid/app/PendingIntent;
    :cond_0
    return-void
.end method

.method public static unpackKey(Ljava/lang/String;)Ljava/util/Map;
    .locals 7
    .param p0, "packedKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 374
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 376
    .local v1, "gslbLicenseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    const-string v5, "-"

    invoke-virtual {p0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 377
    .local v2, "part":[Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KLMSUtility(): unpackKey(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v6, v2, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 378
    const/4 v5, 0x0

    aget-object v5, v2, v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x35

    if-ne v5, v6, :cond_0

    .line 379
    const-string v5, "#"

    invoke-virtual {p0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 380
    .local v3, "url":[Ljava/lang/String;
    const-string v5, "gslb"

    const/4 v6, 0x1

    aget-object v6, v3, v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    const-string v5, "license"

    const/4 v6, 0x0

    aget-object v6, v3, v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KLMSUtility(): KLMS_ONPREMISE_GSLB_HOST_URL: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v3, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    .end local v1    # "gslbLicenseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "part":[Ljava/lang/String;
    .end local v3    # "url":[Ljava/lang/String;
    :goto_0
    return-object v1

    .line 385
    .restart local v1    # "gslbLicenseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 386
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "error in on premise"

    invoke-static {v5, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v4

    .line 387
    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "part":[Ljava/lang/String;
    :cond_0
    move-object v1, v4

    .line 390
    goto :goto_0
.end method
