.class public Lcom/samsung/klmsagent/util/DeviceDetailsUtility;
.super Ljava/lang/Object;
.source "DeviceDetailsUtility.java"


# static fields
.field private static mTelephonyMgr:Landroid/telephony/TelephonyManager;

.field private static mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    sput-object v0, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    .line 18
    sput-object v0, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public static getAndroidVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method public static getBuildNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    sget-object v0, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    return-object v0
.end method

.method public static getClientTimeZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCountryCode()Ljava/lang/String;
    .locals 3

    .prologue
    .line 194
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ro.csc.country_code"

    invoke-static {v1, v2}, Lcom/samsung/klmsagent/util/KLMSSystemProperties;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 196
    .local v0, "country":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 200
    .end local v0    # "country":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 199
    .restart local v0    # "country":Ljava/lang/String;
    :cond_0
    const-string v1, "getCountryCode : ro.csc.country_code is null"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 200
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getCountryISOFromCSC()Ljava/lang/String;
    .locals 5

    .prologue
    .line 181
    invoke-static {}, Lcom/samsung/klmsagent/util/CscParser;->getCustomerInstance()Lcom/samsung/klmsagent/util/CscParser;

    move-result-object v2

    .line 182
    .local v2, "cscParser":Lcom/samsung/klmsagent/util/CscParser;
    const/4 v0, 0x0

    .line 184
    .local v0, "countryISO":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/CscParser;->isLoaded()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 185
    const-string v3, "GeneralInfo.CountryISO"

    invoke-virtual {v2, v3}, Lcom/samsung/klmsagent/util/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 190
    .end local v0    # "countryISO":Ljava/lang/String;
    .local v1, "countryISO":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 188
    .end local v1    # "countryISO":Ljava/lang/String;
    .restart local v0    # "countryISO":Ljava/lang/String;
    :cond_0
    const-string v3, "getCountryFromCSC : CscParser is null or no customer.xml"

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    move-object v1, v0

    .line 190
    .end local v0    # "countryISO":Ljava/lang/String;
    .restart local v1    # "countryISO":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getDeviceCarrier()Ljava/lang/String;
    .locals 2

    .prologue
    .line 154
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 156
    .local v0, "deviceCarrier":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "samsung"

    if-ne v0, v1, :cond_1

    .line 157
    :cond_0
    const/4 v0, 0x0

    .line 159
    :cond_1
    return-object v0
.end method

.method public static getDeviceId()Ljava/lang/String;
    .locals 5

    .prologue
    .line 45
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getIMEI()Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "imei":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getDeviceSerial()Ljava/lang/String;

    move-result-object v2

    .line 47
    .local v2, "serialNum":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMAC()Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "mac":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 50
    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSUtility;->hashImei(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 54
    :goto_0
    return-object v3

    .line 51
    :cond_0
    if-eqz v2, :cond_1

    .line 52
    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSUtility;->hashImei(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 54
    :cond_1
    const-string v3, ":"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSUtility;->hashImei(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getDeviceSerial()Ljava/lang/String;
    .locals 4

    .prologue
    .line 71
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "ril.serialnumber"

    invoke-static {v2, v3}, Lcom/samsung/klmsagent/util/KLMSSystemProperties;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 73
    .local v1, "serialNum":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 74
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 75
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x30

    if-eq v2, v3, :cond_0

    .line 79
    :goto_1
    return-object v1

    .line 74
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    .end local v0    # "i":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static getIMEI()Ljava/lang/String;
    .locals 4

    .prologue
    .line 59
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 61
    .local v1, "imei":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 62
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 63
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x30

    if-eq v2, v3, :cond_0

    .line 67
    :goto_1
    return-object v1

    .line 62
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    .end local v0    # "i":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static getMAC()Ljava/lang/String;
    .locals 3

    .prologue
    .line 83
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "mac":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 86
    const-string v1, ":"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getMCC()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 101
    const/4 v2, 0x0

    .line 102
    .local v2, "mccMnc":Ljava/lang/String;
    const/4 v1, 0x0

    .line 105
    .local v1, "mcc":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "gsm.sim.operator.numeric"

    invoke-static {v3, v4}, Lcom/samsung/klmsagent/util/KLMSSystemProperties;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 106
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v5, :cond_0

    .line 107
    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 113
    :cond_0
    :goto_0
    return-object v1

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 111
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getMNC1()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 117
    const/4 v1, 0x0

    .line 118
    .local v1, "mccMnc":Ljava/lang/String;
    const/4 v2, 0x0

    .line 121
    .local v2, "mnc":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "gsm.sim.operator.numeric"

    invoke-static {v3, v4}, Lcom/samsung/klmsagent/util/KLMSSystemProperties;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v5, :cond_0

    .line 123
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 129
    :cond_0
    :goto_0
    return-object v2

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 127
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getMNC2()Ljava/lang/String;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    .line 134
    const/4 v1, 0x0

    .line 135
    .local v1, "mccMnc":Ljava/lang/String;
    const/4 v2, 0x0

    .line 138
    .local v2, "mnc":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "gsm.sim.operator.numeric2"

    invoke-static {v3, v4}, Lcom/samsung/klmsagent/util/KLMSSystemProperties;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 139
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v5, :cond_0

    .line 140
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 146
    :cond_0
    :goto_0
    return-object v2

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 144
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method public static getRandomNumber()Ljava/lang/String;
    .locals 4

    .prologue
    .line 205
    const/4 v0, 0x0

    .line 206
    .local v0, "newAndroidIdValue":Ljava/lang/String;
    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    .line 208
    .local v1, "random":Ljava/security/SecureRandom;
    if-eqz v1, :cond_0

    .line 209
    invoke-virtual {v1}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    .line 210
    :cond_0
    return-object v0
.end method

.method public static getSalesCodeFromCSC()Ljava/lang/String;
    .locals 5

    .prologue
    .line 168
    invoke-static {}, Lcom/samsung/klmsagent/util/CscParser;->getCustomerInstance()Lcom/samsung/klmsagent/util/CscParser;

    move-result-object v2

    .line 169
    .local v2, "cscParser":Lcom/samsung/klmsagent/util/CscParser;
    const/4 v0, 0x0

    .line 171
    .local v0, "country":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/CscParser;->isLoaded()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 172
    const-string v3, "GeneralInfo.SalesCode"

    invoke-virtual {v2, v3}, Lcom/samsung/klmsagent/util/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 177
    .end local v0    # "country":Ljava/lang/String;
    .local v1, "country":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 175
    .end local v1    # "country":Ljava/lang/String;
    .restart local v0    # "country":Ljava/lang/String;
    :cond_0
    const-string v3, "getCountryFromCSC : CscParser is null or no customer.xml"

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    move-object v1, v0

    .line 177
    .end local v0    # "country":Ljava/lang/String;
    .restart local v1    # "country":Ljava/lang/String;
    goto :goto_0
.end method

.method private static getTelephonyManager()Landroid/telephony/TelephonyManager;
    .locals 2

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 26
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    sput-object v0, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    .line 29
    :cond_0
    sget-object v0, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method public static getVersionCode()I
    .locals 6

    .prologue
    .line 229
    const/4 v0, -0x1

    .line 232
    .local v0, "VersionCode":I
    :try_start_0
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 234
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v0, v2, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v0

    .line 235
    :catch_0
    move-exception v1

    .line 236
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getVersionName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 215
    const/4 v0, 0x0

    .line 218
    .local v0, "VersionName":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 220
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-object v0

    .line 221
    :catch_0
    move-exception v1

    .line 222
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static getWifiManager()Landroid/net/wifi/WifiManager;
    .locals 2

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-nez v0, :cond_0

    .line 34
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    sput-object v0, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 36
    :cond_0
    sget-object v0, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object v0
.end method
