.class public final enum Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
.super Ljava/lang/Enum;
.source "KLMSConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/util/KLMSConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "KLMSRequestType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum CONTAINER_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum CONTAINER_REGISTER_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum CONTAINER_UNINSTALL:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum DEVICE_DEACTIVATION:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum DEVICE_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum DEVICE_VALIDATION:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum GSLB_REQ:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum GSLB_REQ_POLICY:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum GSLB_REQ_PROCESS:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum GSLB_REQ_PROCESS_DEVICE_REG:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum MDM_CONTAINER_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

.field public static final enum RP_MODE:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 158
    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "DEVICE_DEACTIVATION"

    invoke-direct {v0, v1, v3}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->DEVICE_DEACTIVATION:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "DEVICE_REGISTER"

    invoke-direct {v0, v1, v4}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->DEVICE_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "CONTAINER_REGISTER"

    invoke-direct {v0, v1, v5}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "DEVICE_VALIDATION"

    invoke-direct {v0, v1, v6}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->DEVICE_VALIDATION:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "CONTAINER_UNINSTALL"

    invoke-direct {v0, v1, v7}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_UNINSTALL:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "MDM_CONTAINER_REGISTER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->MDM_CONTAINER_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "GSLB_REQ"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "CONTAINER_REGISTER_B2C"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REGISTER_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "CONTAINER_REMOVED_B2C"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "GSLB_REQ_PROCESS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ_PROCESS:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "GSLB_REQ_PROCESS_DEVICE_REG"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ_PROCESS_DEVICE_REG:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "GSLB_REQ_POLICY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ_POLICY:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    const-string v1, "RP_MODE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->RP_MODE:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    .line 157
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->DEVICE_DEACTIVATION:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->DEVICE_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->DEVICE_VALIDATION:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_UNINSTALL:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->MDM_CONTAINER_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REGISTER_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ_PROCESS:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ_PROCESS_DEVICE_REG:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->GSLB_REQ_POLICY:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->RP_MODE:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->$VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 157
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 157
    const-class v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->$VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual {v0}, [Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    return-object v0
.end method
