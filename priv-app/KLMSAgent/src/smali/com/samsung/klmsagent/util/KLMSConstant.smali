.class public Lcom/samsung/klmsagent/util/KLMSConstant;
.super Ljava/lang/Object;
.source "KLMSConstant.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;,
        Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;,
        Lcom/samsung/klmsagent/util/KLMSConstant$KLMSValidationState;,
        Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;,
        Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;,
        Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;
    }
.end annotation


# static fields
.field public static final ACTION_VALIDATE_LICENSE:Ljava/lang/String; = "com.samsung.klmsclientagent.VALIDATE_LICENSE"

.field public static final ACTIVATION_ID:Ljava/lang/String; = "activation_id"

.field public static final CONNECTION_TIME_OUT:I = 0xa

.field public static final CONNECTION_TIME_OUT_CHINA:I = 0x19

.field public static final CONTAINER_ID:Ljava/lang/String; = "container_id"

.field public static final CONTAINER_PERMISSION:Ljava/lang/String; = "android.permission.sec.MDM_ENTERPRISE_CONTAINER"

.field public static final CertificatePath:Ljava/lang/String; = "certificates/"

.field public static final DEVICE_OR_SERVER_ERROR:I = 0xc8

.field public static final ELM_PACKAGE_NAME:Ljava/lang/String; = "ELM_PACKAGE_NAME"

.field public static final HTTPS_PROTOCOL:Ljava/lang/String; = "https://"

.field public static final HTTP_DATE_HEADER:Ljava/lang/String; = "client_request_time_stamp"

.field public static HTTP_GSLB_CHINA_REDIRECTION_SERVER_HOST:Ljava/lang/String; = null

.field public static HTTP_GSLB_CHINA_REDIRECTION_SERVER_HOST_DEV:Ljava/lang/String; = null

.field public static HTTP_GSLB_GLOBAL_REDIRECTION_SERVER_HOST:Ljava/lang/String; = null

.field public static HTTP_GSLB_GLOBAL_REDIRECTION_SERVER_HOST_DEV:Ljava/lang/String; = null

.field public static final INTENT_CONTAINERAGENT_FROM_KLMS_LICENSEKEY_STATUS:Ljava/lang/String; = "com.sec.knox.containeragent.klms.licensekey.status"

.field public static final INTENT_CONTAINERAGENT_FROM_KLMS_VALIDATION_INPROGRESS:Ljava/lang/String; = "com.sec.knox.containeragent.klms.licensekey.validating"

.field public static final INTENT_CONTAINERAGENT_PERMISSION:Ljava/lang/String; = "com.sec.knox.containeragent.USE_KNOX_UI"

.field public static final INTENT_CONTAINERAGENT_TO_KLMS_CREATED:Ljava/lang/String; = "com.sec.knox.containeragent.klms.created.b2b"

.field public static final INTENT_CONTAINERAGENT_TO_KLMS_CREATED_B2C:Ljava/lang/String; = "com.sec.knox.containeragent.klms.created.b2c"

.field public static final INTENT_CONTAINERAGENT_TO_KLMS_CREATED_OLD:Ljava/lang/String; = "com.sec.knox.containeragent.klms.created"

.field public static final INTENT_CONTAINERAGENT_TO_KLMS_LICENSE_KEY_CHECK:Ljava/lang/String; = "com.sec.knox.containeragent.klms.licensekey.check"

.field public static final INTENT_CONTAINERAGENT_TO_KLMS_REMOVED:Ljava/lang/String; = "com.sec.knox.containeragent.klms.removed.b2b"

.field public static final INTENT_CONTAINERAGENT_TO_KLMS_REMOVED_B2C:Ljava/lang/String; = "com.sec.knox.containeragent.klms.removed.b2c"

.field public static final INTENT_CONTAINERAGENT_TO_KLMS_REMOVED_OLD:Ljava/lang/String; = "com.sec.knox.containeragent.klms.removed"

.field public static final INTENT_ELM_TO_KLMS_CHECK_LICENSE:Ljava/lang/String; = "com.sec.esdk.elm.action.REQUEST_KLMS_STATUS"

.field public static final INTENT_ELM_TO_KLMS_NOTICE_PERMISSON_CHANGED:Ljava/lang/String; = "com.sec.esdk.elm.action.NOTICE_PERMISSON_CHANGED"

.field public static final INTENT_KAP_PERMISSION:Ljava/lang/String; = "com.sec.enterprise.knox.permission.MDM_ENTERPRISE_TIMA_NOTIFICATION"

.field public static final INTENT_KLMS_RP_NOTIFICATION_ACTION:Ljava/lang/String; = "com.samsung.action.knox.klms.KLMS_RP_NOTIFICATION"

.field public static final INTENT_KLMS_TO_LICENSETEST_RP_RESULT:Ljava/lang/String; = "com.samsung.klmsagent.action.rpmode"

.field public static final INTENT_KLM_TO_ELM_ON_PREMISE:Ljava/lang/String; = "ON_PREMISE"

.field public static final INTENT_MDM_TO_KLMS_DEACTIVATELICENSE:Ljava/lang/String; = "edm.intent.action.knox_license.deactivation.internal"

.field public static final INTENT_MDM_TO_KLMS_VALIDATELICENSE:Ljava/lang/String; = "edm.intent.action.knox_license.registration.internal"

.field public static final INTENT_MDM_TO_KLMS_VALIDATELICENSE_UMC:Ljava/lang/String; = "edm.intent.action.knox_license.registration.internal_umc"

.field public static final INTENT_RP_TO_KLMS_NOTIFICATION_ACTION:Ljava/lang/String; = "com.samsung.action.knox.kap.KAP_NOTIFICATION"

.field public static final KAP_LICENSE_NOTIFICATION_TRIGGER:Ljava/lang/String; = "KAP_LICENSE_NOTIFICATION_TRIGGER"

.field public static final KAP_LICENSE_STATUS_REQUEST:Ljava/lang/String; = "KAP_LICENSE_STATUS_REQUEST"

.field public static final KAP_RP_MODE_STATUS:Ljava/lang/String; = "KAP_RP_MODE_STATUS"

.field public static final KLMS_AGENT_PREFERENCES:Ljava/lang/String; = "klmsagent.preferences"

.field public static final KLMS_ALARM_TIME:Ljava/lang/String; = "KLMS_ALARM_TIME"

.field public static final KLMS_B2C_CONTAINER_ID:Ljava/lang/String; = "KLMS_B2C_CONTAINER_ID"

.field public static final KLMS_B2C_SERVER_ADDR:Ljava/lang/String; = "KLMS_B2C_SERVER_ADDR"

.field public static final KLMS_B2C_TRACKER_ID:Ljava/lang/String; = "KLMS_B2C_TRACKER_ID"

.field public static final KLMS_CONTAINER_EXPLICIT_LOCKED:I = 0x4

.field public static final KLMS_CONTAINER_MDM_LOCKED:I = 0x2

.field public static final KLMS_CONTAINER_MDM_UNLOCKED:I = 0x3

.field public static final KLMS_CONTAINER_PACKAGE_NAME:Ljava/lang/String; = "KLMS_CONTAINER_PACKAGE_NAME"

.field public static final KLMS_CONTAINER_SERVER_LOCKED:I = 0x0

.field public static final KLMS_CONTAINER_SERVER_UNLOCKED:I = 0x1

.field public static final KLMS_ERROR_UNKNOWN:Ljava/lang/String; = "Unknown Error"

.field public static final KLMS_EULA:Ljava/lang/String; = "KLMS_EULA"

.field public static final KLMS_EULA_BACK_BUTTON:Ljava/lang/String; = "KLMS_EULA_BACK_BUTTON"

.field public static final KLMS_EULA_CANCEL_BUTTON:Ljava/lang/String; = "KLMS_EULA_CANCEL_BUTTON"

.field public static final KLMS_EULA_DISAGREE:I = 0x28

.field public static final KLMS_GSLB_HOSTNAME:Ljava/lang/String; = "on_premise_gslb"

.field public static final KLMS_KAP_LICENSE_STATUS:Ljava/lang/String; = "KLMS_LICENSE_STATUS"

.field public static final KLMS_LICENSEKEY_ERROR:I = 0xa

.field public static final KLMS_LICENSEKEY_VAILD:I = 0x5a

.field public static final KLMS_LICENSE_STATUS:Ljava/lang/String; = "KLMS_LICENSE_STATUS"

.field public static final KLMS_LICENSKEY_INVALID:I = 0x32

.field public static final KLMS_MDM_PUSH_FROM:Ljava/lang/String; = "KLMS_MDM_PUSH_FROM"

.field public static final KLMS_MDM_PUSH_FROM_NORMAL:Ljava/lang/String; = "KLMS_MDM_PUSH_FROM_NORMAL"

.field public static final KLMS_MDM_PUSH_FROM_UMC:Ljava/lang/String; = "KLMS_MDM_PUSH_FROM_UMC"

.field public static final KLMS_MDMforUMC_STATE:Ljava/lang/String; = "KLMS_MDMforUMC_STATE"

.field public static final KLMS_NETWORK_ERROR:I = 0x14

.field public static final KLMS_NEXT_VALIDATION_INTERVAL:Ljava/lang/String; = "KLMS_NEXT_VALIDATION_INTERVAL"

.field public static final KLMS_NO_NETWORK_MDM:I = 0x270f

.field public static final KLMS_ONPREMISE_GSLB_HOST_URL:Ljava/lang/String; = "gslb"

.field public static final KLMS_ONPREMISE_KAD_URL:Ljava/lang/String; = "kad"

.field public static final KLMS_ONPREMISE_KAD_URL_ERROR_FOR_CA:Ljava/lang/String; = "kad_url_error"

.field public static final KLMS_ONPREMISE_KAD_URL_FOR_CA:Ljava/lang/String; = "kad_url"

.field public static final KLMS_ONPREMISE_KEY_IN_USE:Ljava/lang/String; = "klms_onpremise_key_in_use"

.field public static final KLMS_ONPREMISE_KLM_POLICY_KEY:Ljava/lang/String; = "klms_policy_key"

.field public static final KLMS_ONPREMISE_LICENSE:Ljava/lang/String; = "license"

.field public static final KLMS_SERVER_ADDR:Ljava/lang/String; = "KLMS_SERVER_ADDR"

.field public static final KLMS_SERVER_ERROR:I = 0x1e

.field public static final KLMS_SERVER_ERROR_MDM:I = 0x22b8

.field public static final KLMS_SMS_FORMAT_ERROR:I = 0x115c

.field public static final KLMS_STATUS:Ljava/lang/String; = "code"

.field public static final KLMS_TEMP_PACKAGE_NAME:Ljava/lang/String; = "KLMS_TEMP_PACKAGE_NAME"

.field public static final KLMS_UI_FORMAT_ERROR:I = 0x15b3

.field public static final KLMS_USER_AGREE:I = 0x1a0a

.field public static final KLMS_USER_DISAGREEMENT:I = 0x1e61

.field public static final KLMS_VALIDATE_DE_ACTIVE_MSG:Ljava/lang/String; = "General license deactivated."

.field public static final KLMS_VALIDATE_EXPIRE_MSG:Ljava/lang/String; = "General license expired."

.field public static final KLMS_VALIDATE_INPUT_PARAMS:Ljava/lang/String; = "Null parameter"

.field public static final KLMS_VALIDATE_INVALID_MSG:Ljava/lang/String; = "Invalid license"

.field public static final KLMS_VALIDATE_LICENSE_DEACTIVATE_ERROR:Ljava/lang/String; = "License deactivation error."

.field public static final KLMS_VALIDATE_LICENSE_TERMINATE:Ljava/lang/String; = "License terminated."

.field public static final KLMS_VALIDATE_LICENSE_WRONG_DATE_ERROR:Ljava/lang/String; = "wrong date error."

.field public static final KLMS_VALIDATE_NETWORK_ERROR:Ljava/lang/String; = "Network disconnected."

.field public static final KLMS_VALIDATE_QUANTITY_OVER_MSG:Ljava/lang/String; = "General license quantity exhausted."

.field public static final KLMS_VALIDATE_SERVER_ERROR:Ljava/lang/String; = "Internal server error."

.field public static final KLMS_VALIDATE_SUCCESS_MSG:Ljava/lang/String; = "Success."

.field public static final KLMS_VALIDATE_USER_DISAGREE:Ljava/lang/String; = "User Disagree."

.field public static final KLMS_WAIT_FOR_GSLB_RESPONSE:Ljava/lang/String; = "KLMS_WAIT_FOR_GSLB_RESPONSE"

.field public static final KLMS_WAIT_FOR_GSLB_RESPONSE_FOR_DEVICE_REG:Ljava/lang/String; = "KLMS_WAIT_FOR_GSLB_RESPONSE_FOR_DEVICE_REG"

.field public static final LICENSE_ACTIVATION_INITIATOR_ADMIN:I = 0x384

.field public static final LICENSE_ACTIVATION_INITIATOR_NONE:I = -0x1

.field public static final LICENSE_DEACTIVATION_INITIATOR_MDM:I = 0x3e8

.field public static final LICENSE_DEACTIVATION_INITIATOR_NONE:I = -0x1

.field public static final LICENSE_KEY:Ljava/lang/String; = "license"

.field public static final LICENSE_RESULT_TYPE_ACTIVATION:I = 0x320

.field public static final LICENSE_RESULT_TYPE_DEACTIVATION:I = 0x322

.field public static final LICENSE_RESULT_TYPE_VALIDATION:I = 0x321

.field public static final MAX_TRY:I = 0x2

.field public static final MDM_ERROR:Ljava/lang/String; = "fail"

.field public static final MDM_SUCCESS:Ljava/lang/String; = "success"

.field public static final NOTIFICATION_APP_LIST:Ljava/lang/String; = "NOTIFICATION_APP_LIST"

.field public static NOTIFICATION_STACK_ID:I = 0x0

.field public static final NOTIFICATION_STACK_VALIDATING:I = 0x2

.field public static final NO_OF_DAYS:Ljava/lang/Integer;

.field public static final ON_PREMISE_KEY:Ljava/lang/String; = "onPrem"

.field public static final ON_PREMISE_NOT_PRESENT:I = 0x2

.field public static final ON_PREMISE_PRESENT:I = 0x1

.field public static final ON_PREM_KLMS_SERVER_ADDR:Ljava/lang/String; = "ON_PREM_KLMS_SERVER_ADDR"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field public static final REDIRECTION_KLMS_ONPREMISE_POLICY_SERVICE:Ljava/lang/String; = "/KnoxGSLB/policy"

.field public static final REDIRECTION_KLMS_SERVICE:Ljava/lang/String; = "/KnoxGSLB/lookup/klms"

.field public static final REDIRECTION_KLMS_SERVICE_B2C:Ljava/lang/String; = "/KnoxGSLB/lookup/klmb2c"

.field public static final REPLACEMENT_ID:I = 0x1

.field public static final RESPONSE_TIME_OUT:I = 0xa

.field public static final RESPONSE_TIME_OUT_CHINA:I = 0x19

.field public static final RPMODE_RESULT:Ljava/lang/String; = "RPMODE_RESULT"

.field public static final RPMODE_STATUS:Ljava/lang/String; = "RPMODE_STATUS"

.field public static final SERVER_ACTIVATION_RECORD_NOT_FOUND:I = 0xc3a

.field public static final SERVER_CALL_RETRY:I = 0x1

.field public static final SERVER_CALL_RETRY_KEY:Ljava/lang/String; = "current_count"

.field public static final SERVER_CALL_TYPE_KEY:Ljava/lang/String; = "current_call_type"

.field public static final SERVER_DEVICE_NOT_REGISTERED:I = 0xc2c

.field public static final SERVER_RETURN_CODE_APP_ALREADY_INSTALLED:I = 0xc45

.field public static final SERVER_RETURN_CODE_APP_ALREADY_UNINSTALLED:I = 0xc46

.field public static final SERVER_RETURN_CODE_CONTAINER_ALREADY_REGISTERED:I = 0xc35

.field public static final SERVER_RETURN_CODE_CONTAINER_DATA_IS_NOT_PRESENT:I = 0x74

.field public static final SERVER_RETURN_CODE_CONTAINER_IS_NOT_ACTIVE:I = 0xc33

.field public static final SERVER_RETURN_CODE_CONTAINER_SUCCESS_RETURN_CODE:I = 0x0

.field public static final SERVER_RETURN_CODE_CONTAINER_TRACK_ID_NOT_PRESENT:I = 0xc59

.field public static final SERVER_RETURN_CODE_DEVICE_ALREADY_REGISTERED:I = 0xc2b

.field public static final SERVER_RETURN_CODE_DEVICE_IS_NOT_ACITVE:I = 0xc2a

.field public static final SERVER_RETURN_CODE_DEVICE_IS_NOT_REGISTERED:I = 0xc2c

.field public static final SERVER_RETURN_CODE_FAILURE_RETURN_CODE:I = 0x3ed

.field public static final SERVER_RETURN_CODE_LICENSE_EXPIRED:I = 0xc1d

.field public static final SERVER_RETURN_CODE_LICENSE_IS_NOT_ACTIVE:I = 0xc1e

.field public static final SERVER_RETURN_CODE_LICENSE_KEY_INVALID:I = 0xc1f

.field public static final SERVER_RETURN_CODE_LICENSE_KEY_TERMINATE:I = 0xc20

.field public static final SERVER_RETURN_CODE_LICENSE_QUANTITY_EXPIRED:I = 0xc32

.field public static final SERVER_RETURN_CODE_REQUIRED_INPUT_PARAMETER_ERROR_CODE:I = 0x3e9

.field public static final SERVER_RETURN_CODE_SUCCESS_RETURN_CODE:I = 0x3e8

.field public static final SERVER_RETURN_CODE_TRACKER_IS_NOT_ACTIVE:I = 0xc5b

.field public static final SERVER_RETURN_CODE_TRACKER_IS_NOT_REGISTER:I = 0xc5a

.field public static final SERVER_RETURN_CODE_WRONG_DATE_ERROR:I = 0xbb9

.field public static final TRANSACTION_ID:Ljava/lang/String; = "transaction_id"

.field public static final UNION_ID:I = 0x2

.field public static final URL_B2C_CONTAINER_REGISTER:Ljava/lang/String; = "/klm-rest/enrollContainer.do"

.field public static final URL_B2C_CONTAINER_UNINTSALL:Ljava/lang/String; = "/klm-rest/uninstallContainer.do"

.field public static final URL_CONTAINER_REGISTER:Ljava/lang/String; = "/klm-rest/v3/container/install.do"

.field public static final URL_CONTAINER_REGISTER_POLICY:Ljava/lang/String; = "enrollContainer"

.field public static final URL_CONTAINER_STATUS_NOTIFY:Ljava/lang/String; = "/klm-rest/notifyTrackKey.do"

.field public static final URL_CONTAINER_UNINTSALL:Ljava/lang/String; = "/klm-rest/v3/container/uninstall.do"

.field public static final URL_CONTAINER_UNINTSALL_POLICY:Ljava/lang/String; = "uninstallContainer"

.field public static final URL_DEVICE_REGISTER:Ljava/lang/String; = "/klm-rest/v3/device/install.do"

.field public static final URL_DEVICE_REGISTER_POLICY:Ljava/lang/String; = "enrollDevice"

.field public static final URL_RP_ATTRIBUTION:Ljava/lang/String; = "/knox-license/attribution/settings.do"

.field public static final URL_UNINSTALL_LICENSE:Ljava/lang/String; = "/klm-rest/v3/device/uninstall.do"

.field public static final URL_UNINSTALL_LICENSE_POLICY:Ljava/lang/String; = "uninstallLicense"

.field public static final URL_VALIDATE_TRACK_KEY:Ljava/lang/String; = "/klm-rest/v3/validateTrackKey.do"

.field public static final URL_VALIDATE_TRACK_KEY_POLICY:Ljava/lang/String; = "validateTrackKey"

.field public static final charset:Ljava/lang/String; = "UTF-8"

.field public static final format:Ljava/lang/String; = " method called: %s , timestamp: %s"

.field public static isDebugLogEnable:Z

.field public static isErrorLogEnable:Z

.field public static isInfoLogEnable:Z

.field public static isProxyEnabled:Z

.field public static isUserBuild:Z

.field public static isVerboseLogEnable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 41
    const-string v0, "gslb.secb2b.com"

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant;->HTTP_GSLB_GLOBAL_REDIRECTION_SERVER_HOST:Ljava/lang/String;

    .line 42
    const-string v0, "china-gslb.secb2b.com.cn"

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant;->HTTP_GSLB_CHINA_REDIRECTION_SERVER_HOST:Ljava/lang/String;

    .line 43
    const-string v0, "stage-gslb.secb2b.com"

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant;->HTTP_GSLB_GLOBAL_REDIRECTION_SERVER_HOST_DEV:Ljava/lang/String;

    .line 44
    const-string v0, "china-stage-gslb.secb2b.com.cn"

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant;->HTTP_GSLB_CHINA_REDIRECTION_SERVER_HOST_DEV:Ljava/lang/String;

    .line 249
    sput-boolean v1, Lcom/samsung/klmsagent/util/KLMSConstant;->isErrorLogEnable:Z

    .line 250
    sput-boolean v1, Lcom/samsung/klmsagent/util/KLMSConstant;->isDebugLogEnable:Z

    .line 251
    sput-boolean v1, Lcom/samsung/klmsagent/util/KLMSConstant;->isInfoLogEnable:Z

    .line 252
    sput-boolean v1, Lcom/samsung/klmsagent/util/KLMSConstant;->isVerboseLogEnable:Z

    .line 253
    sput-boolean v2, Lcom/samsung/klmsagent/util/KLMSConstant;->isUserBuild:Z

    .line 256
    sput-boolean v1, Lcom/samsung/klmsagent/util/KLMSConstant;->isProxyEnabled:Z

    .line 259
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant;->NO_OF_DAYS:Ljava/lang/Integer;

    .line 299
    const/16 v0, 0xc

    sput v0, Lcom/samsung/klmsagent/util/KLMSConstant;->NOTIFICATION_STACK_ID:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 306
    return-void
.end method
