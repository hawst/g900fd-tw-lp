.class public final enum Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;
.super Ljava/lang/Enum;
.source "KLMSConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/util/KLMSConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HttpRetryFrequency"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

.field public static final enum FIFTEEN_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

.field public static final enum FIVE_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

.field public static final enum THREE_MIN:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 103
    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    const-string v1, "FIVE_SEC"

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->FIVE_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    const-string v1, "FIFTEEN_SEC"

    invoke-direct {v0, v1, v3}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->FIFTEEN_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    const-string v1, "THREE_MIN"

    invoke-direct {v0, v1, v4}, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->THREE_MIN:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    .line 102
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->FIVE_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->FIFTEEN_SEC:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->THREE_MIN:Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->$VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 102
    const-class v0, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    return-object v0
.end method

.method public static values()[Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->$VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    invoke-virtual {v0}, [Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/klmsagent/util/KLMSConstant$HttpRetryFrequency;

    return-object v0
.end method
