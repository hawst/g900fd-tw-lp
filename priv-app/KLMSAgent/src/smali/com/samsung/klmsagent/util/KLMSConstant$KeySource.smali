.class public final enum Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;
.super Ljava/lang/Enum;
.source "KLMSConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/util/KLMSConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "KeySource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

.field public static final enum B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

.field public static final enum FOR_ALL:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

.field public static final enum MDM:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

.field public static final enum RP:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

.field public static final enum SMS:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

.field public static final enum UI:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 295
    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    const-string v1, "MDM"

    invoke-direct {v0, v1, v3}, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->MDM:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    const-string v1, "UI"

    invoke-direct {v0, v1, v4}, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->UI:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v5}, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->SMS:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    const-string v1, "FOR_ALL"

    invoke-direct {v0, v1, v6}, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->FOR_ALL:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    const-string v1, "B2C"

    invoke-direct {v0, v1, v7}, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    new-instance v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    const-string v1, "RP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->RP:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    .line 294
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->MDM:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->UI:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->SMS:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->FOR_ALL:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->RP:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->$VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 294
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 294
    const-class v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    return-object v0
.end method

.method public static values()[Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;
    .locals 1

    .prologue
    .line 294
    sget-object v0, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->$VALUES:[Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    invoke-virtual {v0}, [Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    return-object v0
.end method
