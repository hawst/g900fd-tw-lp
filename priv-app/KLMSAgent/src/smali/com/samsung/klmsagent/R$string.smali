.class public final Lcom/samsung/klmsagent/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final agree_confirm_all:I = 0x7f06000e

.field public static final agree_confirm_consent:I = 0x7f06000b

.field public static final agree_confirm_tc:I = 0x7f060008

.field public static final app_licensekey:I = 0x7f060018

.field public static final app_name:I = 0x7f060017

.field public static final bullet_point:I = 0x7f060033

.field public static final cancel:I = 0x7f060015

.field public static final confirm:I = 0x7f06000f

.field public static final consent_of_usages:I = 0x7f060011

.field public static final done:I = 0x7f060014

.field public static final enterlicense:I = 0x7f060013

.field public static final eula_comment:I = 0x7f060034

.field public static final eula_comment_kor_eng:I = 0x7f060036

.field public static final eula_consent_of_use:I = 0x7f060009

.field public static final eula_consent_text:I = 0x7f06000a

.field public static final eula_optional_Info:I = 0x7f06000d

.field public static final eula_optional_info_small_text_kor:I = 0x7f060000

.field public static final eula_optional_info_title:I = 0x7f060001

.field public static final eula_optional_title:I = 0x7f06000c

.field public static final eula_privacy_policy_kor:I = 0x7f060004

.field public static final eula_privacy_policy_small_text_kor:I = 0x7f060003

.field public static final eula_terms:I = 0x7f060035

.field public static final eula_terms_text:I = 0x7f060007

.field public static final eula_title:I = 0x7f060005

.field public static final eula_title_test:I = 0x7f060006

.field public static final klms_eula_disagree:I = 0x7f06001e

.field public static final klms_internal_server_error:I = 0x7f060021

.field public static final klms_license_deactivated:I = 0x7f060025

.field public static final klms_license_expired:I = 0x7f060024

.field public static final klms_license_invalid:I = 0x7f060023

.field public static final klms_license_quantity_over:I = 0x7f060027

.field public static final klms_license_terminated:I = 0x7f060026

.field public static final klms_network_error:I = 0x7f060022

.field public static final license_check_fail:I = 0x7f060016

.field public static final license_dialog_activate:I = 0x7f06002d

.field public static final license_dialog_cancel:I = 0x7f06002c

.field public static final license_dialog_content_empty:I = 0x7f060030

.field public static final license_dialog_content_footer:I = 0x7f06002f

.field public static final license_dialog_content_header:I = 0x7f06002e

.field public static final license_dialog_ok:I = 0x7f06002b

.field public static final license_dialog_title:I = 0x7f06002a

.field public static final license_dialog_title_text:I = 0x7f060029

.field public static final license_key_title:I = 0x7f060012

.field public static final license_key_validating:I = 0x7f060028

.field public static final license_key_validating_pop_up:I = 0x7f06001b

.field public static final license_validation_success:I = 0x7f06001d

.field public static final menu_settings:I = 0x7f060019

.field public static final network_not_available:I = 0x7f060020

.field public static final notification_content:I = 0x7f060032

.field public static final notification_title:I = 0x7f060031

.field public static final optional_info_kor:I = 0x7f060002

.field public static final t_c_complete_string:I = 0x7f060010

.field public static final trans_activity:I = 0x7f06001a

.field public static final user_input_license_key_ui_error:I = 0x7f06001c

.field public static final user_ipnut_license_key_sms_error:I = 0x7f06001f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
