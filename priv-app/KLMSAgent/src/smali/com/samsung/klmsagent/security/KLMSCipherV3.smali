.class public Lcom/samsung/klmsagent/security/KLMSCipherV3;
.super Ljava/lang/Object;
.source "KLMSCipherV3.java"


# static fields
.field private static final AES256ALGORITHM:Ljava/lang/String; = "AES/CBC/PKCS7Padding"

.field public static final InitialVectorNum:I = 0x10

.field private static final TAG:Ljava/lang/String; = "KLMSCipherV3(): "

.field private static klmsCipher:Lcom/samsung/klmsagent/security/KLMSCipherV3;

.field private static final mSynch:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/klmsagent/security/KLMSCipherV3;->mSynch:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method private decrypt256(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;[B)[B
    .locals 6
    .param p1, "inKey"    # Ljava/security/Key;
    .param p2, "ivParameterSpec"    # Ljavax/crypto/spec/IvParameterSpec;
    .param p3, "cipherData"    # [B

    .prologue
    .line 162
    const/4 v3, 0x0

    .line 163
    .local v3, "plainData":[B
    const/4 v0, 0x0

    .line 165
    .local v0, "cipher":Ljavax/crypto/Cipher;
    :try_start_0
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v4

    invoke-interface {p1}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 166
    .local v2, "ks":Ljavax/crypto/spec/SecretKeySpec;
    const-string v4, "AES/CBC/PKCS7Padding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 167
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v2, p2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 168
    invoke-virtual {v0, p3}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v3

    .line 188
    .end local v2    # "ks":Ljavax/crypto/spec/SecretKeySpec;
    :goto_0
    return-object v3

    .line 169
    :catch_0
    move-exception v1

    .line 170
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 171
    const/4 v3, 0x0

    .line 187
    goto :goto_0

    .line 172
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 173
    .local v1, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v1}, Ljava/security/InvalidKeyException;->printStackTrace()V

    .line 174
    const/4 v3, 0x0

    .line 187
    goto :goto_0

    .line 175
    .end local v1    # "e":Ljava/security/InvalidKeyException;
    :catch_2
    move-exception v1

    .line 176
    .local v1, "e":Ljava/security/InvalidAlgorithmParameterException;
    invoke-virtual {v1}, Ljava/security/InvalidAlgorithmParameterException;->printStackTrace()V

    .line 177
    const/4 v3, 0x0

    .line 187
    goto :goto_0

    .line 178
    .end local v1    # "e":Ljava/security/InvalidAlgorithmParameterException;
    :catch_3
    move-exception v1

    .line 179
    .local v1, "e":Ljavax/crypto/IllegalBlockSizeException;
    invoke-virtual {v1}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V

    .line 180
    const/4 v3, 0x0

    .line 187
    goto :goto_0

    .line 181
    .end local v1    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_4
    move-exception v1

    .line 182
    .local v1, "e":Ljavax/crypto/BadPaddingException;
    invoke-virtual {v1}, Ljavax/crypto/BadPaddingException;->printStackTrace()V

    .line 183
    const/4 v3, 0x0

    .line 187
    goto :goto_0

    .line 184
    .end local v1    # "e":Ljavax/crypto/BadPaddingException;
    :catch_5
    move-exception v1

    .line 185
    .local v1, "e":Ljavax/crypto/NoSuchPaddingException;
    invoke-virtual {v1}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V

    .line 186
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private encrypt256(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;[B)[B
    .locals 6
    .param p1, "inKey"    # Ljava/security/Key;
    .param p2, "ivParameterSpec"    # Ljavax/crypto/spec/IvParameterSpec;
    .param p3, "plainData"    # [B

    .prologue
    .line 132
    const/4 v1, 0x0

    .line 133
    .local v1, "ciphertext":[B
    const/4 v0, 0x0

    .line 135
    .local v0, "cipher":Ljavax/crypto/Cipher;
    :try_start_0
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v4

    invoke-interface {p1}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 136
    .local v3, "ks":Ljavax/crypto/spec/SecretKeySpec;
    const-string v4, "AES/CBC/PKCS7Padding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 137
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v3, p2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 138
    invoke-virtual {v0, p3}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v1

    .line 158
    .end local v3    # "ks":Ljavax/crypto/spec/SecretKeySpec;
    :goto_0
    return-object v1

    .line 139
    :catch_0
    move-exception v2

    .line 140
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 141
    const/4 v1, 0x0

    .line 157
    goto :goto_0

    .line 142
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v2

    .line 143
    .local v2, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v2}, Ljava/security/InvalidKeyException;->printStackTrace()V

    .line 144
    const/4 v1, 0x0

    .line 157
    goto :goto_0

    .line 145
    .end local v2    # "e":Ljava/security/InvalidKeyException;
    :catch_2
    move-exception v2

    .line 146
    .local v2, "e":Ljava/security/InvalidAlgorithmParameterException;
    invoke-virtual {v2}, Ljava/security/InvalidAlgorithmParameterException;->printStackTrace()V

    .line 147
    const/4 v1, 0x0

    .line 157
    goto :goto_0

    .line 148
    .end local v2    # "e":Ljava/security/InvalidAlgorithmParameterException;
    :catch_3
    move-exception v2

    .line 149
    .local v2, "e":Ljavax/crypto/IllegalBlockSizeException;
    invoke-virtual {v2}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V

    .line 150
    const/4 v1, 0x0

    .line 157
    goto :goto_0

    .line 151
    .end local v2    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_4
    move-exception v2

    .line 152
    .local v2, "e":Ljavax/crypto/BadPaddingException;
    invoke-virtual {v2}, Ljavax/crypto/BadPaddingException;->printStackTrace()V

    .line 153
    const/4 v1, 0x0

    .line 157
    goto :goto_0

    .line 154
    .end local v2    # "e":Ljavax/crypto/BadPaddingException;
    :catch_5
    move-exception v2

    .line 155
    .local v2, "e":Ljavax/crypto/NoSuchPaddingException;
    invoke-virtual {v2}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V

    .line 156
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getInstance()Lcom/samsung/klmsagent/security/KLMSCipherV3;
    .locals 2

    .prologue
    .line 38
    sget-object v0, Lcom/samsung/klmsagent/security/KLMSCipherV3;->klmsCipher:Lcom/samsung/klmsagent/security/KLMSCipherV3;

    if-nez v0, :cond_1

    .line 39
    sget-object v1, Lcom/samsung/klmsagent/security/KLMSCipherV3;->mSynch:Ljava/lang/Object;

    monitor-enter v1

    .line 40
    :try_start_0
    sget-object v0, Lcom/samsung/klmsagent/security/KLMSCipherV3;->klmsCipher:Lcom/samsung/klmsagent/security/KLMSCipherV3;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/samsung/klmsagent/security/KLMSCipherV3;

    invoke-direct {v0}, Lcom/samsung/klmsagent/security/KLMSCipherV3;-><init>()V

    sput-object v0, Lcom/samsung/klmsagent/security/KLMSCipherV3;->klmsCipher:Lcom/samsung/klmsagent/security/KLMSCipherV3;

    .line 43
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    :cond_1
    sget-object v0, Lcom/samsung/klmsagent/security/KLMSCipherV3;->klmsCipher:Lcom/samsung/klmsagent/security/KLMSCipherV3;

    return-object v0

    .line 43
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public declared-synchronized getDecryptedNetworkData(Ljava/lang/String;Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;)Ljava/lang/String;
    .locals 6
    .param p1, "cryptoData"    # Ljava/lang/String;
    .param p2, "inKey"    # Ljava/security/Key;
    .param p3, "InitVector"    # Ljavax/crypto/spec/IvParameterSpec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    const-string v4, "KLMSCipherV3(): getDecryptedNetworkData()"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    const/4 v0, 0x0

    .line 108
    .local v0, "DecryptedMessage":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v3

    .line 109
    .local v3, "receivedData":[B
    invoke-direct {p0, p2, p3, v3}, Lcom/samsung/klmsagent/security/KLMSCipherV3;->decrypt256(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;[B)[B

    move-result-object v2

    .line 111
    .local v2, "msgArr":[B
    new-instance v0, Ljava/lang/String;

    .end local v0    # "DecryptedMessage":Ljava/lang/String;
    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    .end local v2    # "msgArr":[B
    .end local v3    # "receivedData":[B
    .restart local v0    # "DecryptedMessage":Ljava/lang/String;
    :goto_0
    if-nez v0, :cond_0

    .line 118
    :try_start_2
    new-instance v4, Lorg/json/JSONException;

    const-string v5, "DecryptedMessage is Null."

    invoke-direct {v4, v5}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 103
    .end local v0    # "DecryptedMessage":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 112
    :catch_0
    move-exception v1

    .line 113
    .local v1, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    .line 114
    .restart local v0    # "DecryptedMessage":Ljava/lang/String;
    :try_start_3
    const-string v4, "KLMSCipherV3(): getDecryptedNetworkData() has Exception."

    invoke-static {v4, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 121
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    monitor-exit p0

    return-object v0
.end method

.method public getEncryptedAESSessionKey(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;)Ljava/lang/String;
    .locals 10
    .param p1, "inKey"    # Ljava/security/Key;
    .param p2, "InitialVector"    # Ljavax/crypto/spec/IvParameterSpec;
    .param p3, "mPUBSerialNumber"    # Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    .prologue
    .line 49
    const-string v8, "KLMSCipherV3(): getEncryptedAESSessionKey()"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 51
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->getKLMSCertificateUtil(Landroid/content/Context;)Lcom/samsung/klmsagent/security/KLMSCertificateUtil;

    move-result-object v4

    .line 53
    .local v4, "mKLMCertificateUtil":Lcom/samsung/klmsagent/security/KLMSCertificateUtil;
    const/4 v2, 0x0

    .line 55
    .local v2, "encryptedAESSessionKey":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v4, p3}, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->getKlmKeyDecryptPubllicKey(Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;)Ljava/security/PublicKey;

    move-result-object v6

    .line 56
    .local v6, "publicKey":Ljava/security/PublicKey;
    if-nez v6, :cond_0

    .line 57
    const-string v8, "KLMSCipherV3(): getEncryptedAESSessionKey() : publicKey is null"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 58
    const/4 v8, 0x0

    .line 77
    .end local v6    # "publicKey":Ljava/security/PublicKey;
    :goto_0
    return-object v8

    .line 61
    .restart local v6    # "publicKey":Ljava/security/PublicKey;
    :cond_0
    const-string v8, "RSA/ECB/PKCS1Padding"

    invoke-static {v8}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v7

    .line 62
    .local v7, "rsaCipher":Ljavax/crypto/Cipher;
    const/4 v8, 0x1

    invoke-virtual {v7, v8, v6}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 63
    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v8

    array-length v8, v8

    invoke-virtual {p2}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v9

    array-length v9, v9

    add-int/2addr v8, v9

    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 64
    .local v5, "plainText":Ljava/nio/ByteBuffer;
    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 65
    invoke-virtual {p2}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 67
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v3

    .line 69
    .local v3, "encryptedPassword":[B
    array-length v8, v3

    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 70
    .local v0, "bf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 71
    new-instance v2, Ljava/lang/String;

    .end local v2    # "encryptedAESSessionKey":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v8

    const/4 v9, 0x2

    invoke-static {v8, v9}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "bf":Ljava/nio/ByteBuffer;
    .end local v3    # "encryptedPassword":[B
    .end local v5    # "plainText":Ljava/nio/ByteBuffer;
    .end local v6    # "publicKey":Ljava/security/PublicKey;
    .end local v7    # "rsaCipher":Ljavax/crypto/Cipher;
    .restart local v2    # "encryptedAESSessionKey":Ljava/lang/String;
    :goto_1
    move-object v8, v2

    .line 77
    goto :goto_0

    .line 72
    .end local v2    # "encryptedAESSessionKey":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 73
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    .line 74
    .restart local v2    # "encryptedAESSessionKey":Ljava/lang/String;
    const-string v8, "KLMSCipherV3(): getEncryptedAESSessionKey() has Exception."

    invoke-static {v8, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public declared-synchronized getEncryptedNetworkData(Ljava/lang/String;Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;)Ljava/lang/String;
    .locals 5
    .param p1, "plainData"    # Ljava/lang/String;
    .param p2, "inKey"    # Ljava/security/Key;
    .param p3, "InitVector"    # Ljavax/crypto/spec/IvParameterSpec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    const-string v3, "KLMSCipherV3(): getEncryptedNetworkData()"

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    const/4 v1, 0x0

    .line 86
    .local v1, "encryptedMessage":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {p0, p2, p3, v3}, Lcom/samsung/klmsagent/security/KLMSCipherV3;->encrypt256(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;[B)[B

    move-result-object v2

    .line 88
    .local v2, "msgArr":[B
    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 94
    .end local v2    # "msgArr":[B
    :goto_0
    if-nez v1, :cond_0

    .line 95
    :try_start_2
    new-instance v3, Lorg/json/JSONException;

    const-string v4, "CompriseEncryptedMessage is Null"

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 82
    .end local v1    # "encryptedMessage":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 89
    .restart local v1    # "encryptedMessage":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .line 91
    :try_start_3
    const-string v3, "KLMSCipherV3(): getEncryptedNetworkData() has Exception."

    invoke-static {v3, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 98
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method public getInitialVector()Ljavax/crypto/spec/IvParameterSpec;
    .locals 3

    .prologue
    .line 125
    const/4 v0, 0x0

    .line 126
    .local v0, "mResult":Ljavax/crypto/spec/IvParameterSpec;
    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    .line 127
    .local v1, "mSecureRandom":Ljava/security/SecureRandom;
    new-instance v0, Ljavax/crypto/spec/IvParameterSpec;

    .end local v0    # "mResult":Ljavax/crypto/spec/IvParameterSpec;
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Ljava/security/SecureRandom;->generateSeed(I)[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 128
    .restart local v0    # "mResult":Ljavax/crypto/spec/IvParameterSpec;
    return-object v0
.end method
