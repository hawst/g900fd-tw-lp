.class public Lcom/samsung/klmsagent/security/SecureDataGenerator;
.super Ljava/lang/Object;
.source "SecureDataGenerator.java"


# static fields
.field public static final GSLB_SERVICE_NAME:Ljava/lang/String; = "GSLB"

.field private static final NUMCIPHERS:I = 0x4

.field private static final cipherSuites:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 33
    .local v0, "tmpMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/String;>;"
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const-string v2, "AES/CBC/PKCS5Padding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const-string v2, "AES/ECB/PKCS5Padding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const-string v2, "DESede/CBC/PKCS5Padding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const-string v2, "DESede/ECB/PKCS5Padding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    sput-object v1, Lcom/samsung/klmsagent/security/SecureDataGenerator;->cipherSuites:Ljava/util/Map;

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private compriseSignatureData(Ljava/lang/String;BJ)Ljava/lang/String;
    .locals 7
    .param p1, "hash"    # Ljava/lang/String;
    .param p2, "dataFormatId"    # B
    .param p3, "timestamp"    # J

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x0

    .line 274
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x18

    if-ge v0, v1, :cond_1

    .line 275
    :cond_0
    const/4 v0, 0x0

    .line 278
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s%02d%s%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/samsung/klmsagent/security/SecureDataGenerator;->xor(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getCiphersuite(Ljava/lang/String;Ljava/lang/String;)B
    .locals 4
    .param p1, "serviceName"    # Ljava/lang/String;
    .param p2, "deviceId"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 216
    const/4 v0, -0x1

    .line 218
    .local v0, "dataFormatID":B
    if-nez p1, :cond_1

    .line 219
    const-string v2, "getCiphersuite : serviceName is null"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 237
    :cond_0
    :goto_0
    return v1

    .line 223
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/klmsagent/security/SecureDataGenerator;->getDataFormatID(Ljava/lang/String;)B

    move-result v0

    .line 224
    if-ne v0, v1, :cond_2

    .line 225
    const-string v2, "getCiphersuite : dataFormatId is null"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 229
    :cond_2
    if-nez p2, :cond_3

    .line 230
    const-string v2, "getCiphersuite : deviceId is null"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 234
    :cond_3
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v2, v3

    rem-int/lit8 v2, v2, 0x4

    int-to-byte v1, v2

    .line 235
    .local v1, "num":B
    if-gez v1, :cond_0

    .line 236
    mul-int/lit8 v2, v1, -0x1

    int-to-byte v1, v2

    goto :goto_0
.end method

.method private getDataFormatID(Ljava/lang/String;)B
    .locals 3
    .param p1, "serviceName"    # Ljava/lang/String;

    .prologue
    .line 141
    const/4 v0, 0x0

    .line 143
    .local v0, "IMEI":Ljava/lang/String;
    const/4 v1, -0x1

    .line 145
    .local v1, "dataFormatID":B
    if-nez p1, :cond_0

    .line 146
    const-string v2, "getDataFormatID : serviceName is null"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 147
    const/4 v2, -0x1

    .line 159
    :goto_0
    return v2

    .line 150
    :cond_0
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getIMEI()Ljava/lang/String;

    move-result-object v0

    .line 153
    if-eqz v0, :cond_1

    .line 154
    const/4 v1, 0x2

    :goto_1
    move v2, v1

    .line 159
    goto :goto_0

    .line 156
    :cond_1
    const/4 v1, 0x3

    goto :goto_1
.end method

.method private getDeviceID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "serviceName"    # Ljava/lang/String;
    .param p2, "baseData"    # Ljava/lang/String;

    .prologue
    .line 121
    const/4 v1, 0x0

    .line 123
    .local v1, "deviceId":Ljava/lang/String;
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 124
    :cond_0
    const-string v3, "getDeviceID : serviceName or baseData is null"

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 125
    const/4 v3, 0x0

    .line 137
    :goto_0
    return-object v3

    .line 129
    :cond_1
    :try_start_0
    const-string v3, "GSLB"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 130
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 131
    .local v0, "baseJson":Lorg/json/JSONObject;
    const-string v3, "deviceid"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .end local v0    # "baseJson":Lorg/json/JSONObject;
    :cond_2
    :goto_1
    move-object v3, v1

    .line 137
    goto :goto_0

    .line 133
    :catch_0
    move-exception v2

    .line 134
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private getEnc1Data([BLjava/lang/String;[B)Ljava/lang/String;
    .locals 10
    .param p1, "data"    # [B
    .param p2, "cipher"    # Ljava/lang/String;
    .param p3, "F2Value"    # [B

    .prologue
    const/16 v9, 0x8

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/16 v7, 0x10

    .line 241
    sget-object v5, Lcom/samsung/klmsagent/security/SecureDataGenerator;->cipherSuites:Ljava/util/Map;

    invoke-interface {v5, p2}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 270
    :goto_0
    return-object v4

    .line 245
    :cond_0
    if-eqz p1, :cond_1

    if-nez p3, :cond_2

    .line 246
    :cond_1
    const-string v5, "getEnc1Data : data or F2Value is null"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 252
    :cond_2
    const-string v5, "AES"

    invoke-virtual {p2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 253
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v5, p3, v6, v7}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    const-string v6, "AES"

    invoke-direct {v3, v5, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 254
    .local v3, "sKey":Ljava/security/Key;
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5, p3, v8, v7}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-direct {v2, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 260
    .local v2, "iv":Ljavax/crypto/spec/IvParameterSpec;
    :goto_1
    :try_start_0
    invoke-static {p2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 261
    .local v0, "c":Ljavax/crypto/Cipher;
    const-string v5, "CBC"

    invoke-virtual {p2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 262
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v3, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 266
    :goto_2
    new-instance v5, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v6

    const/4 v7, 0x2

    invoke-static {v6, v7}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v6

    const-string v7, "UTF-8"

    invoke-direct {v5, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v4, v5

    goto :goto_0

    .line 256
    .end local v0    # "c":Ljavax/crypto/Cipher;
    .end local v2    # "iv":Ljavax/crypto/spec/IvParameterSpec;
    .end local v3    # "sKey":Ljava/security/Key;
    :cond_3
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const/16 v5, 0x18

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    const/16 v6, 0x18

    invoke-virtual {v5, p3, v8, v6}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    const-string v6, "DESede"

    invoke-direct {v3, v5, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 257
    .restart local v3    # "sKey":Ljava/security/Key;
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-static {v9}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5, p3, v8, v9}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-direct {v2, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .restart local v2    # "iv":Ljavax/crypto/spec/IvParameterSpec;
    goto :goto_1

    .line 264
    .restart local v0    # "c":Ljavax/crypto/Cipher;
    :cond_4
    const/4 v5, 0x1

    :try_start_1
    invoke-virtual {v0, v5, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 267
    .end local v0    # "c":Ljavax/crypto/Cipher;
    :catch_0
    move-exception v1

    .line 268
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private getF2(BLjava/lang/String;Ljava/lang/String;J)[B
    .locals 10
    .param p1, "dataFormatId"    # B
    .param p2, "IMEIorMAC"    # Ljava/lang/String;
    .param p3, "serviceName"    # Ljava/lang/String;
    .param p4, "timestamp"    # J

    .prologue
    const/4 v8, 0x0

    .line 177
    if-eqz p3, :cond_0

    const/4 v1, 0x3

    if-gt p1, v1, :cond_0

    if-eqz p2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v1, p4, v2

    if-gez v1, :cond_1

    :cond_0
    move-object v1, v8

    .line 187
    :goto_0
    return-object v1

    .line 182
    :cond_1
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const-string v1, "UTF-8"

    invoke-virtual {p3, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    const/4 v5, 0x0

    move-object v1, p0

    move v2, p1

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/samsung/klmsagent/security/SecureDataGenerator;->md5bytes(B[B[B[BJ)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v1, v8

    .line 187
    goto :goto_0
.end method

.method private getF2(BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)[B
    .locals 9
    .param p1, "dataFormatId"    # B
    .param p2, "IMEIorMAC"    # Ljava/lang/String;
    .param p3, "serviceName"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "timestamp"    # J

    .prologue
    const/4 v8, 0x0

    .line 163
    if-eqz p3, :cond_0

    const/4 v1, 0x3

    if-gt p1, v1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p4, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v1, p5, v2

    if-gez v1, :cond_1

    :cond_0
    move-object v1, v8

    .line 173
    :goto_0
    return-object v1

    .line 168
    :cond_1
    :try_start_0
    const-string v1, "UTF-8"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {p4}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    move-object v1, p0

    move v2, p1

    move-wide v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/samsung/klmsagent/security/SecureDataGenerator;->md5bytes(B[B[B[BJ)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 170
    :catch_0
    move-exception v0

    .line 171
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v1, v8

    .line 173
    goto :goto_0
.end method

.method private md5bytes(B[B[B[BJ)[B
    .locals 7
    .param p1, "b0"    # B
    .param p2, "b1"    # [B
    .param p3, "b2"    # [B
    .param p4, "b3"    # [B
    .param p5, "b4"    # J

    .prologue
    const/4 v4, 0x0

    .line 191
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-object v4

    .line 195
    :cond_1
    array-length v5, p2

    array-length v6, p3

    add-int/2addr v5, v6

    add-int/lit8 v3, v5, 0x9

    .line 196
    .local v3, "size":I
    if-eqz p4, :cond_2

    .line 197
    array-length v5, p4

    add-int/2addr v3, v5

    .line 198
    :cond_2
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 199
    .local v0, "bf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 200
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 201
    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 202
    if-eqz p4, :cond_3

    .line 203
    invoke-virtual {v0, p4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 204
    :cond_3
    invoke-virtual {v0, p5, p6}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 206
    :try_start_0
    const-string v5, "SHA-256"

    invoke-static {v5}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 207
    .local v2, "md":Ljava/security/MessageDigest;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/security/MessageDigest;->update([B)V

    .line 208
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 209
    .end local v2    # "md":Ljava/security/MessageDigest;
    :catch_0
    move-exception v1

    .line 210
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0
.end method

.method private xor(Ljava/lang/Long;)Ljava/lang/String;
    .locals 6
    .param p1, "orig"    # Ljava/lang/Long;

    .prologue
    .line 283
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide v4, -0x410efd670d2ddc6L    # -9.459999887958783E288

    xor-long v0, v2, v4

    .line 284
    .local v0, "xor":J
    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public getSignatureData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    .locals 26
    .param p1, "origData"    # Ljava/lang/String;
    .param p2, "serviceName"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "optTimestamp"    # J

    .prologue
    .line 41
    const/4 v7, 0x0

    .line 42
    .local v7, "deviceId":Ljava/lang/String;
    const/16 v21, 0x0

    .line 43
    .local v21, "enc1Data":Ljava/lang/String;
    const/16 v23, 0x0

    .line 45
    .local v23, "signatureData":Ljava/lang/String;
    const/4 v6, -0x1

    .line 47
    .local v6, "dataFormatID":B
    const/16 v19, -0x1

    .line 49
    .local v19, "ciphersuite":B
    const-wide/16 v10, -0x1

    .line 51
    .local v10, "timestamp":J
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 52
    :cond_0
    const/4 v5, 0x0

    .line 116
    :goto_0
    return-object v5

    .line 55
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/klmsagent/security/SecureDataGenerator;->getDataFormatID(Ljava/lang/String;)B

    move-result v6

    .line 56
    const/4 v5, -0x1

    if-ne v6, v5, :cond_2

    .line 57
    const-string v5, "getSignatureData : dataFormatId is null"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 58
    const/4 v5, 0x0

    goto :goto_0

    .line 61
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/security/SecureDataGenerator;->getDeviceID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 63
    if-nez v7, :cond_3

    .line 64
    const-string v5, "getSignatureData : deviceId is null"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 65
    const/4 v5, 0x0

    goto :goto_0

    .line 68
    :cond_3
    const-wide/16 v8, 0x0

    cmp-long v5, p4, v8

    if-nez v5, :cond_4

    .line 69
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 74
    :goto_1
    if-eqz p3, :cond_5

    move-object/from16 v5, p0

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    .line 75
    invoke-direct/range {v5 .. v11}, Lcom/samsung/klmsagent/security/SecureDataGenerator;->getF2(BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)[B

    move-result-object v4

    .line 80
    .local v4, "F2Value":[B
    :goto_2
    if-nez v4, :cond_6

    .line 81
    const-string v5, "getSignatureData : F2Value is null"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 82
    const/4 v5, 0x0

    goto :goto_0

    .line 71
    .end local v4    # "F2Value":[B
    :cond_4
    move-wide/from16 v10, p4

    goto :goto_1

    :cond_5
    move-object/from16 v12, p0

    move v13, v6

    move-object v14, v7

    move-object/from16 v15, p2

    move-wide/from16 v16, v10

    .line 77
    invoke-direct/range {v12 .. v17}, Lcom/samsung/klmsagent/security/SecureDataGenerator;->getF2(BLjava/lang/String;Ljava/lang/String;J)[B

    move-result-object v4

    .restart local v4    # "F2Value":[B
    goto :goto_2

    .line 85
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v7}, Lcom/samsung/klmsagent/security/SecureDataGenerator;->getCiphersuite(Ljava/lang/String;Ljava/lang/String;)B

    move-result v19

    .line 86
    if-ltz v19, :cond_7

    const/4 v5, 0x4

    move/from16 v0, v19

    if-lt v0, v5, :cond_8

    .line 87
    :cond_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSignatureData : ciphersuite("

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ") is invalid."

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 88
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 91
    :cond_8
    sget-object v5, Lcom/samsung/klmsagent/security/SecureDataGenerator;->cipherSuites:Ljava/util/Map;

    invoke-static/range {v19 .. v19}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 96
    .local v24, "strCiphersuite":Ljava/lang/String;
    :try_start_0
    const-string v5, "SHA-256"

    invoke-static {v5}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v22

    .line 98
    .local v22, "md":Ljava/security/MessageDigest;
    :try_start_1
    const-string v5, "UTF-8"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/security/MessageDigest;->update([B)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    .line 102
    :goto_3
    :try_start_2
    invoke-virtual/range {v22 .. v22}, Ljava/security/MessageDigest;->digest()[B
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v18

    .line 108
    .local v18, "MD5Data":[B
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2, v4}, Lcom/samsung/klmsagent/security/SecureDataGenerator;->getEnc1Data([BLjava/lang/String;[B)Ljava/lang/String;

    move-result-object v21

    .line 109
    if-nez v21, :cond_9

    .line 110
    const-string v5, "getSignatureData : enc1Data is null"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 111
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 99
    .end local v18    # "MD5Data":[B
    :catch_0
    move-exception v20

    .line 100
    .local v20, "e":Ljava/io/UnsupportedEncodingException;
    :try_start_3
    invoke-virtual/range {v20 .. v20}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V
    :try_end_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    .line 103
    .end local v20    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v22    # "md":Ljava/security/MessageDigest;
    :catch_1
    move-exception v20

    .line 104
    .local v20, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual/range {v20 .. v20}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 105
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 114
    .end local v20    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v18    # "MD5Data":[B
    .restart local v22    # "md":Ljava/security/MessageDigest;
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v6, v10, v11}, Lcom/samsung/klmsagent/security/SecureDataGenerator;->compriseSignatureData(Ljava/lang/String;BJ)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v5, v23

    .line 116
    goto/16 :goto_0
.end method
