.class public Lcom/samsung/klmsagent/security/KLMSCertificateUtil;
.super Ljava/lang/Object;
.source "KLMSCertificateUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "KLMSCertificateUtil(): "

.field private static certResContext:Landroid/content/Context;

.field private static klmCertificateFactory:Lcom/samsung/klmsagent/security/KLMSCertificateUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    sput-object v0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->klmCertificateFactory:Lcom/samsung/klmsagent/security/KLMSCertificateUtil;

    .line 22
    sput-object v0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->certResContext:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public static getKLMSCertificateUtil()Lcom/samsung/klmsagent/security/KLMSCertificateUtil;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->klmCertificateFactory:Lcom/samsung/klmsagent/security/KLMSCertificateUtil;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;

    invoke-direct {v0}, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;-><init>()V

    sput-object v0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->klmCertificateFactory:Lcom/samsung/klmsagent/security/KLMSCertificateUtil;

    .line 32
    :cond_0
    sget-object v0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->certResContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 33
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->certResContext:Landroid/content/Context;

    .line 35
    :cond_1
    sget-object v0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->klmCertificateFactory:Lcom/samsung/klmsagent/security/KLMSCertificateUtil;

    return-object v0
.end method

.method public static getKLMSCertificateUtil(Landroid/content/Context;)Lcom/samsung/klmsagent/security/KLMSCertificateUtil;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    sget-object v0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->klmCertificateFactory:Lcom/samsung/klmsagent/security/KLMSCertificateUtil;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;

    invoke-direct {v0}, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;-><init>()V

    sput-object v0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->klmCertificateFactory:Lcom/samsung/klmsagent/security/KLMSCertificateUtil;

    .line 43
    :cond_0
    sput-object p0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->certResContext:Landroid/content/Context;

    .line 44
    sget-object v0, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->klmCertificateFactory:Lcom/samsung/klmsagent/security/KLMSCertificateUtil;

    return-object v0
.end method


# virtual methods
.method public getKlmKeyDecryptPubllicKey(Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;)Ljava/security/PublicKey;
    .locals 11
    .param p1, "mPUBSerialNumber"    # Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;

    .prologue
    .line 48
    const-string v8, "KLMSCertificateUtil(): getKlmKeyDecryptPubllicKey().START"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 50
    const/4 v7, 0x0

    .line 52
    .local v7, "klmKeyDecryptPubllicKey":Ljava/security/PublicKey;
    const/4 v6, 0x0

    .line 53
    .local v6, "fInStream":Ljava/io/InputStream;
    const/4 v1, 0x0

    .line 55
    .local v1, "bin":Ljava/io/BufferedInputStream;
    sget-object v8, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->certResContext:Landroid/content/Context;

    if-nez v8, :cond_0

    .line 56
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v8

    sput-object v8, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->certResContext:Landroid/content/Context;

    .line 59
    :cond_0
    :try_start_0
    sget-object v8, Lcom/samsung/klmsagent/security/KLMSCertificateUtil;->certResContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 61
    .local v0, "am":Landroid/content/res/AssetManager;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "certificates/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/samsung/klmsagent/util/KLMSConstant$PUBSerialNumber;->getAlias()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v6

    .line 63
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .local v2, "bin":Ljava/io/BufferedInputStream;
    :try_start_1
    const-string v8, "X.509"

    invoke-static {v8}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v4

    .line 67
    .local v4, "cf":Ljava/security/cert/CertificateFactory;
    const/4 v3, 0x0

    .line 69
    .local v3, "cert":Ljava/security/cert/Certificate;
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->available()I

    move-result v8

    if-lez v8, :cond_1

    .line 70
    invoke-virtual {v4, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v3

    goto :goto_0

    .line 73
    :cond_1
    invoke-virtual {v3}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_d
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v7

    .line 82
    if-eqz v6, :cond_2

    .line 84
    :try_start_2
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 90
    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    .line 92
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 97
    :cond_3
    :goto_2
    const-string v8, "KLMSCertificateUtil(): getKlmKeyDecryptPubllicKey().END"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    move-object v1, v2

    .line 100
    .end local v0    # "am":Landroid/content/res/AssetManager;
    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .end local v3    # "cert":Ljava/security/cert/Certificate;
    .end local v4    # "cf":Ljava/security/cert/CertificateFactory;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    :goto_3
    return-object v7

    .line 85
    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .restart local v0    # "am":Landroid/content/res/AssetManager;
    .restart local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v3    # "cert":Ljava/security/cert/Certificate;
    .restart local v4    # "cf":Ljava/security/cert/CertificateFactory;
    :catch_0
    move-exception v5

    .line 86
    .local v5, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSCertificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 93
    .end local v5    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v5

    .line 94
    .restart local v5    # "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSCertificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_2

    .line 75
    .end local v0    # "am":Landroid/content/res/AssetManager;
    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .end local v3    # "cert":Ljava/security/cert/Certificate;
    .end local v4    # "cf":Ljava/security/cert/CertificateFactory;
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    :catch_2
    move-exception v5

    .line 76
    .local v5, "e":Ljava/io/FileNotFoundException;
    :goto_4
    :try_start_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSCertificate : FileNotFoundException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 82
    if-eqz v6, :cond_4

    .line 84
    :try_start_5
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 90
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    :cond_4
    :goto_5
    if-eqz v1, :cond_5

    .line 92
    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 97
    :cond_5
    :goto_6
    const-string v8, "KLMSCertificateUtil(): getKlmKeyDecryptPubllicKey().END"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto :goto_3

    .line 85
    .restart local v5    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v5

    .line 86
    .local v5, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSCertificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_5

    .line 93
    .end local v5    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v5

    .line 94
    .restart local v5    # "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSCertificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_6

    .line 77
    .end local v5    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v5

    .line 78
    .restart local v5    # "e":Ljava/io/IOException;
    :goto_7
    :try_start_7
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSCertificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 82
    if-eqz v6, :cond_6

    .line 84
    :try_start_8
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 90
    :cond_6
    :goto_8
    if-eqz v1, :cond_7

    .line 92
    :try_start_9
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 97
    :cond_7
    :goto_9
    const-string v8, "KLMSCertificateUtil(): getKlmKeyDecryptPubllicKey().END"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 85
    :catch_6
    move-exception v5

    .line 86
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSCertificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_8

    .line 93
    :catch_7
    move-exception v5

    .line 94
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSCertificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_9

    .line 79
    .end local v5    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v5

    .line 80
    .local v5, "e":Ljava/lang/Exception;
    :goto_a
    :try_start_a
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSCertificate : Exception"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 82
    if-eqz v6, :cond_8

    .line 84
    :try_start_b
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9

    .line 90
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_8
    :goto_b
    if-eqz v1, :cond_9

    .line 92
    :try_start_c
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_a

    .line 97
    :cond_9
    :goto_c
    const-string v8, "KLMSCertificateUtil(): getKlmKeyDecryptPubllicKey().END"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 85
    .restart local v5    # "e":Ljava/lang/Exception;
    :catch_9
    move-exception v5

    .line 86
    .local v5, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSCertificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_b

    .line 93
    .end local v5    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v5

    .line 94
    .restart local v5    # "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KLMSCertificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_c

    .line 82
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_d
    if-eqz v6, :cond_a

    .line 84
    :try_start_d
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_b

    .line 90
    :cond_a
    :goto_e
    if-eqz v1, :cond_b

    .line 92
    :try_start_e
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_c

    .line 97
    :cond_b
    :goto_f
    const-string v9, "KLMSCertificateUtil(): getKlmKeyDecryptPubllicKey().END"

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    throw v8

    .line 85
    :catch_b
    move-exception v5

    .line 86
    .restart local v5    # "e":Ljava/io/IOException;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "KLMSCertificate : IOException"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_e

    .line 93
    .end local v5    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v5

    .line 94
    .restart local v5    # "e":Ljava/io/IOException;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "KLMSCertificate : IOException"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_f

    .line 82
    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v0    # "am":Landroid/content/res/AssetManager;
    .restart local v2    # "bin":Ljava/io/BufferedInputStream;
    :catchall_1
    move-exception v8

    move-object v1, v2

    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    goto :goto_d

    .line 79
    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .restart local v2    # "bin":Ljava/io/BufferedInputStream;
    :catch_d
    move-exception v5

    move-object v1, v2

    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    goto/16 :goto_a

    .line 77
    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .restart local v2    # "bin":Ljava/io/BufferedInputStream;
    :catch_e
    move-exception v5

    move-object v1, v2

    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    goto/16 :goto_7

    .line 75
    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .restart local v2    # "bin":Ljava/io/BufferedInputStream;
    :catch_f
    move-exception v5

    move-object v1, v2

    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    goto/16 :goto_4
.end method
