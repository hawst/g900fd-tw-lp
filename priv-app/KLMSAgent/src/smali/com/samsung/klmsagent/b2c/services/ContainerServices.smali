.class public Lcom/samsung/klmsagent/b2c/services/ContainerServices;
.super Lcom/samsung/klmsagent/services/KLMSServices;
.source "ContainerServices.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/klmsagent/services/KLMSServices",
        "<",
        "Lcom/samsung/klmsagent/beans/ContainerData;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "B2C-ContainerServices(): "


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/klmsagent/services/KLMSServices;-><init>()V

    .line 30
    return-void
.end method

.method public static postProcessResponse(Ljava/lang/String;Landroid/os/Message;)V
    .locals 9
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "reqObj"    # Landroid/os/Message;

    .prologue
    .line 88
    const-string v7, "B2C-ContainerServices(): postProcessResponse().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 91
    if-eqz p0, :cond_4

    .line 92
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 93
    .local v5, "response":Lorg/json/JSONObject;
    const-string v7, "payLoad"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 94
    const-string v7, "B2C-ContainerServices(): postProcessResponse() : Result from Server is null."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    const-string v7, "B2C-ContainerServices(): postProcessResponse().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 124
    .end local v5    # "response":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 98
    .restart local v5    # "response":Lorg/json/JSONObject;
    :cond_0
    :try_start_1
    const-string v7, "payLoad"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONObject;

    .line 99
    .local v4, "payload":Lorg/json/JSONObject;
    const-string v7, "containerResponsePayInfo"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 100
    const-string v7, "B2C-ContainerServices(): Payload error code recived."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122
    const-string v7, "B2C-ContainerServices(): postProcessResponse().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :cond_1
    :try_start_2
    const-string v7, "containerResponsePayInfo"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 105
    .local v0, "containerArray":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_4

    .line 106
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 107
    .local v3, "obj":Lorg/json/JSONObject;
    const-string v7, "responseCode"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 108
    .local v6, "responseCode":I
    const-string v7, "responseCode"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    const/16 v8, 0x3e8

    if-eq v7, v8, :cond_2

    const-string v7, "responseCode"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    const/16 v8, 0xc35

    if-ne v7, v8, :cond_3

    .line 110
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2C-ContainerServices(): postProcessResponse(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "|Success"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 111
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    const-string v8, "contrainerTrackerId"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2CTrackerId(Ljava/lang/String;)V

    .line 112
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    const-string v8, "containerId"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2ContainerId(Ljava/lang/String;)V

    .line 105
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 114
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2C-ContainerServices(): postProcessResponse(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "|Fail"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 122
    const-string v7, "B2C-ContainerServices(): postProcessResponse().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v0    # "containerArray":Lorg/json/JSONArray;
    .end local v2    # "i":I
    .end local v3    # "obj":Lorg/json/JSONObject;
    .end local v4    # "payload":Lorg/json/JSONObject;
    .end local v5    # "response":Lorg/json/JSONObject;
    .end local v6    # "responseCode":I
    :cond_4
    const-string v7, "B2C-ContainerServices(): postProcessResponse().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 119
    :catch_0
    move-exception v1

    .line 120
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v7, "postProcessResponse() has Exception"

    invoke-static {v7, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 122
    const-string v7, "B2C-ContainerServices(): postProcessResponse().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    const-string v8, "B2C-ContainerServices(): postProcessResponse().START"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    throw v7
.end method

.method public static postProcessResponseRPMode(Ljava/lang/String;Landroid/os/Message;)V
    .locals 12
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "reqObj"    # Landroid/os/Message;

    .prologue
    .line 297
    const-string v7, "B2C-ContainerServices(): postProcessResponseRPMode()"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 299
    const/4 v5, 0x0

    .line 300
    .local v5, "responseCode":I
    const/4 v6, 0x0

    .line 302
    .local v6, "responseStatus":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2C-ContainerServices(): result = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 303
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2C-ContainerServices(): reqObj = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 304
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2C-ContainerServices(): reqObj.getData().toString() = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 306
    if-eqz p0, :cond_0

    :try_start_0
    const-string v7, ""

    invoke-virtual {v7, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 307
    :cond_0
    const-string v7, "B2C-ContainerServices(): Server return is null."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 339
    :goto_0
    const-string v7, "com.sec.esdk.elmagenttest"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSUtility;->IsPackageExistInDevice(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "com.sec.esdk.elmagenttest2"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSUtility;->IsPackageExistInDevice(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 341
    :cond_1
    const-string v7, "B2C-ContainerServices(): postProcessResponseRPMode().License Test apk exgist"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 342
    invoke-static {v6, v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->SendIntentToLicenseTest(Ljava/lang/String;I)V

    .line 347
    :cond_2
    :goto_1
    return-void

    .line 310
    :cond_3
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 311
    .local v4, "response":Lorg/json/JSONObject;
    const/4 v0, 0x0

    .line 313
    .local v0, "_id":Ljava/lang/String;
    const-string v7, "B2C-ContainerServices(): postProcessResponseRPMode(). Deleting Log DB."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 314
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "REQUEST_DB_ID"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 315
    if-eqz v0, :cond_4

    .line 316
    new-instance v2, Lcom/samsung/klmsagent/beans/RequestLog;

    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v2, v7, v8, v10, v11}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>(Ljava/lang/String;IJ)V

    .line 317
    .local v2, "log":Lcom/samsung/klmsagent/beans/RequestLog;
    invoke-virtual {v2, v0}, Lcom/samsung/klmsagent/beans/RequestLog;->setId(Ljava/lang/String;)V

    .line 318
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2C-ContainerServices(): postProcessResponseRPMode().Cleaning the log DB. Log to be deleted is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 320
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2C-ContainerServices(): postProcessResponseRPMode().Cleaning the log DB. ID to be deleted is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 322
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/samsung/klmsagent/data/DataSource;->deleteData(Lcom/samsung/klmsagent/beans/KLMSComponent;)I

    .line 325
    .end local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :cond_4
    const-string v7, "payLoad"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 326
    const-string v7, "B2C-ContainerServices(): postProcessResponseRPMode() : Result from Server is null."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 329
    :cond_5
    const-string v7, "payLoad"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONObject;

    .line 331
    .local v3, "payload":Lorg/json/JSONObject;
    const-string v7, "responseCode"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 332
    const-string v7, "responseMessage"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 334
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2C-ContainerServices(): postProcessResponseRPMode().responseCode : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 335
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2C-ContainerServices(): postProcessResponseRPMode().responseMessage : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 344
    .end local v0    # "_id":Ljava/lang/String;
    .end local v3    # "payload":Lorg/json/JSONObject;
    .end local v4    # "response":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 345
    .local v1, "e":Ljava/lang/Exception;
    const-string v7, "Error caught."

    invoke-static {v7, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1
.end method

.method public static postProcessResponseUninstall(Ljava/lang/String;Landroid/os/Message;)V
    .locals 10
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "reqObj"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x0

    .line 186
    const-string v7, "B2C-ContainerServices(): postProcessResponseUninstall().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 189
    if-eqz p0, :cond_3

    .line 190
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 191
    .local v5, "response":Lorg/json/JSONObject;
    const-string v7, "payLoad"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 192
    const-string v7, "B2C-ContainerServices(): postProcessResponseUninstall() : Result from Server is null."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2CKLMSServerAddr(Lorg/json/JSONArray;)V

    .line 217
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2CTrackerId(Ljava/lang/String;)V

    .line 218
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2ContainerId(Ljava/lang/String;)V

    .line 219
    const-string v7, "B2C-ContainerServices(): postProcessResponseUninstall().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 221
    .end local v5    # "response":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 196
    .restart local v5    # "response":Lorg/json/JSONObject;
    :cond_0
    :try_start_1
    const-string v7, "payLoad"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONObject;

    .line 197
    .local v4, "payload":Lorg/json/JSONObject;
    const-string v7, "containerResponsePayInfo"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 198
    const-string v7, "B2C-ContainerServices(): Payload error code recived."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2CKLMSServerAddr(Lorg/json/JSONArray;)V

    .line 217
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2CTrackerId(Ljava/lang/String;)V

    .line 218
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2ContainerId(Ljava/lang/String;)V

    .line 219
    const-string v7, "B2C-ContainerServices(): postProcessResponseUninstall().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .line 202
    :cond_1
    :try_start_2
    const-string v7, "containerResponsePayInfo"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 203
    .local v0, "containerArray":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 204
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 205
    .local v3, "obj":Lorg/json/JSONObject;
    const-string v7, "responseCode"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 206
    .local v6, "responseCode":I
    const-string v7, "responseCode"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    const/16 v8, 0x3e8

    if-ne v7, v8, :cond_2

    .line 207
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2C-ContainerServices(): postProcessResponseUninstall(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "|Success"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 203
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 209
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2C-ContainerServices(): postProcessResponseUninstall(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "|Fail"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 213
    .end local v0    # "containerArray":Lorg/json/JSONArray;
    .end local v2    # "i":I
    .end local v3    # "obj":Lorg/json/JSONObject;
    .end local v4    # "payload":Lorg/json/JSONObject;
    .end local v5    # "response":Lorg/json/JSONObject;
    .end local v6    # "responseCode":I
    :catch_0
    move-exception v1

    .line 214
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v7, "postProcessResponseUninstall() has Exception"

    invoke-static {v7, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 216
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2CKLMSServerAddr(Lorg/json/JSONArray;)V

    .line 217
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2CTrackerId(Ljava/lang/String;)V

    .line 218
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2ContainerId(Ljava/lang/String;)V

    .line 219
    const-string v7, "B2C-ContainerServices(): postProcessResponseUninstall().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 216
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2CKLMSServerAddr(Lorg/json/JSONArray;)V

    .line 217
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2CTrackerId(Ljava/lang/String;)V

    .line 218
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2ContainerId(Ljava/lang/String;)V

    .line 219
    const-string v7, "B2C-ContainerServices(): postProcessResponseUninstall().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 216
    :catchall_0
    move-exception v7

    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v8

    invoke-virtual {v8, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2CKLMSServerAddr(Lorg/json/JSONArray;)V

    .line 217
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v8

    invoke-virtual {v8, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2CTrackerId(Ljava/lang/String;)V

    .line 218
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v8

    invoke-virtual {v8, v9}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2ContainerId(Ljava/lang/String;)V

    .line 219
    const-string v8, "B2C-ContainerServices(): postProcessResponseUninstall().START"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    throw v7
.end method

.method public static uploadRKPStatus(Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;Z)V
    .locals 14
    .param p0, "var"    # Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    .param p1, "source"    # Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;
    .param p2, "RKPStatus"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 225
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "B2C-ContainerServices(): uploadRKPStatus() ,KLMSRequestType : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", KeySource : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 228
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 229
    .local v6, "request":Lorg/json/JSONObject;
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 231
    .local v8, "uploadRPPayLoad":Lorg/json/JSONObject;
    const-string v0, ""

    .line 232
    .local v0, "RPMode":Ljava/lang/String;
    if-eqz p2, :cond_4

    .line 233
    const-string v0, "on"

    .line 237
    :goto_0
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 239
    .local v5, "params":Lorg/json/JSONArray;
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 240
    .local v4, "paramObj":Lorg/json/JSONObject;
    const-string v9, "type"

    const-string v10, "rkp"

    invoke-virtual {v4, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 241
    const-string v9, "state"

    invoke-virtual {v4, v9, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 242
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 244
    const-string v9, "params"

    invoke-virtual {v8, v9, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 246
    const-string v9, "platform"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getAndroidVersion()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 247
    const-string v9, "model"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getModelName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 248
    const-string v9, "eventTime"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 249
    const-string v9, "deviceId"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getDeviceId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 251
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryISOFromCSC()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryISOFromCSC()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 253
    const-string v9, "countryCode"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryISOFromCSC()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 254
    :cond_0
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMNC1()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMNC1()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_1

    .line 255
    const-string v9, "mnc"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMNC1()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 256
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 257
    const-string v9, "mcc"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 259
    :cond_2
    const-string v9, "payLoad"

    invoke-virtual {v6, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 261
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    .line 262
    .local v3, "msg":Landroid/os/Message;
    iput-object v6, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 263
    invoke-virtual {p0}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v9

    iput v9, v3, Landroid/os/Message;->what:I

    .line 264
    invoke-virtual {p1}, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->ordinal()I

    move-result v9

    iput v9, v3, Landroid/os/Message;->arg1:I

    .line 266
    const-string v9, "B2C-ContainerServices(): uploadRKPStatus(). Doing the call only when the network connection available."

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 267
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "B2C-ContainerServices(): uploadRKPStatus().msg = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 268
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "B2C-ContainerServices(): uploadRKPStatus().msg.obj = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 269
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "B2C-ContainerServices(): uploadRKPStatus().msg.what = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v3, Landroid/os/Message;->what:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 270
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "B2C-ContainerServices(): uploadRKPStatus().msg.arg1 = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v3, Landroid/os/Message;->arg1:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 272
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSUtility;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 274
    const-string v9, "B2C-ContainerServices(): uploadRKPStatus().isNetworkAvailable  = false"

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 275
    invoke-virtual {v3}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "REQUEST_DB_ID"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_3

    .line 276
    new-instance v2, Lcom/samsung/klmsagent/beans/RequestLog;

    iget-object v9, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    iget v10, v3, Landroid/os/Message;->what:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-direct {v2, v9, v10, v12, v13}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>(Ljava/lang/String;IJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    .local v2, "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :try_start_1
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v9

    invoke-virtual {v9, v2}, Lcom/samsung/klmsagent/data/DataSource;->saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z

    .line 279
    const-string v9, "B2C-ContainerServices(): uploadRKPStatus().Save the Log in the DB."

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286
    .end local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :cond_3
    :goto_1
    :try_start_2
    new-instance v7, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    invoke-direct {v7, v3}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 287
    .local v7, "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "/knox-license/attribution/settings.do"

    aput-object v11, v9, v10

    invoke-virtual {v7, v9}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 291
    const-string v9, "uploadRKPStatus(): execution completed."

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 293
    .end local v0    # "RPMode":Ljava/lang/String;
    .end local v3    # "msg":Landroid/os/Message;
    .end local v4    # "paramObj":Lorg/json/JSONObject;
    .end local v5    # "params":Lorg/json/JSONArray;
    .end local v6    # "request":Lorg/json/JSONObject;
    .end local v7    # "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    .end local v8    # "uploadRPPayLoad":Lorg/json/JSONObject;
    :goto_2
    return-void

    .line 235
    .restart local v0    # "RPMode":Ljava/lang/String;
    .restart local v6    # "request":Lorg/json/JSONObject;
    .restart local v8    # "uploadRPPayLoad":Lorg/json/JSONObject;
    :cond_4
    :try_start_3
    const-string v0, "off"

    goto/16 :goto_0

    .line 280
    .restart local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    .restart local v3    # "msg":Landroid/os/Message;
    .restart local v4    # "paramObj":Lorg/json/JSONObject;
    .restart local v5    # "params":Lorg/json/JSONArray;
    :catch_0
    move-exception v1

    .line 281
    .local v1, "e":Ljava/lang/Exception;
    const-string v9, "uploadRKPStatus().Error in saving the data for the request log."

    invoke-static {v9, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 288
    .end local v0    # "RPMode":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    .end local v3    # "msg":Landroid/os/Message;
    .end local v4    # "paramObj":Lorg/json/JSONObject;
    .end local v5    # "params":Lorg/json/JSONArray;
    .end local v6    # "request":Lorg/json/JSONObject;
    .end local v8    # "uploadRPPayLoad":Lorg/json/JSONObject;
    :catch_1
    move-exception v1

    .line 289
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_4
    const-string v9, "uploadRKPStatus(): Exception"

    invoke-static {v9, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 291
    const-string v9, "uploadRKPStatus(): execution completed."

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto :goto_2

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    const-string v10, "uploadRKPStatus(): execution completed."

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    throw v9
.end method


# virtual methods
.method public isAlreadyRegisterd(Lcom/samsung/klmsagent/beans/ContainerData;)Z
    .locals 1
    .param p1, "obj"    # Lcom/samsung/klmsagent/beans/ContainerData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 352
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic isAlreadyRegisterd(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z
    .locals 1
    .param p1, "x0"    # Lcom/samsung/klmsagent/beans/KLMSComponent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25
    check-cast p1, Lcom/samsung/klmsagent/beans/ContainerData;

    .end local p1    # "x0":Lcom/samsung/klmsagent/beans/KLMSComponent;
    invoke-virtual {p0, p1}, Lcom/samsung/klmsagent/b2c/services/ContainerServices;->isAlreadyRegisterd(Lcom/samsung/klmsagent/beans/ContainerData;)Z

    move-result v0

    return v0
.end method

.method public onDestroy(ILjava/lang/String;)V
    .locals 10
    .param p1, "id"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 128
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2C-ContainerServices(): onDestroy().START - Container ID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 131
    :try_start_0
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    .line 132
    .local v4, "msg":Landroid/os/Message;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 133
    .local v5, "request":Lorg/json/JSONObject;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 135
    .local v2, "containerPayLoad":Lorg/json/JSONObject;
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 136
    .local v1, "containerPayInfo":Lorg/json/JSONArray;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 138
    .local v0, "container":Lorg/json/JSONObject;
    const-string v7, "level"

    const-string v8, "container"

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 139
    const-string v7, "devId"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getDeviceId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 140
    const-string v7, "platform"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getAndroidVersion()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 141
    const-string v7, "licKey"

    const-string v8, "KLMB2C"

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 142
    const-string v7, "model"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getModelName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 143
    const-string v7, "contId"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 144
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryISOFromCSC()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryISOFromCSC()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 146
    const-string v7, "countryCode"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryISOFromCSC()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 147
    :cond_0
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryCode()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryCode()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 148
    const-string v7, "countryName"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryCode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 149
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 150
    const-string v7, "mccCode"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 151
    :cond_2
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMNC1()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMNC1()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 152
    const-string v7, "mncCode"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMNC1()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 154
    :cond_3
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getB2CTrackerId()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 155
    const-string v7, "containerTrackerId"

    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getB2CTrackerId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 156
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 157
    const-string v7, "containerPayInfo"

    invoke-virtual {v2, v7, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 160
    :cond_4
    const-string v7, "payLoad"

    invoke-virtual {v5, v7, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 162
    iput-object v5, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 163
    sget-object v7, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual {v7}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v7

    iput v7, v4, Landroid/os/Message;->what:I

    .line 165
    new-instance v6, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    invoke-direct {v6, v4}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 166
    .local v6, "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "/klm-rest/uninstallContainer.do"

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    const-string v7, "B2C-ContainerServices(): onDestroy().END"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 174
    .end local v0    # "container":Lorg/json/JSONObject;
    .end local v1    # "containerPayInfo":Lorg/json/JSONArray;
    .end local v2    # "containerPayLoad":Lorg/json/JSONObject;
    .end local v4    # "msg":Landroid/os/Message;
    .end local v5    # "request":Lorg/json/JSONObject;
    .end local v6    # "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    :goto_0
    return-void

    .line 167
    :catch_0
    move-exception v3

    .line 168
    .local v3, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v7, "Json error."

    invoke-static {v7, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 172
    const-string v7, "B2C-ContainerServices(): onDestroy().END"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .line 169
    .end local v3    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v3

    .line 170
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v7, "onDestroy() has Exception."

    invoke-static {v7, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 172
    const-string v7, "B2C-ContainerServices(): onDestroy().END"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    const-string v8, "B2C-ContainerServices(): onDestroy().END"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    throw v7
.end method

.method public validateRegister(Lcom/samsung/klmsagent/beans/ContainerData;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V
    .locals 10
    .param p1, "obj"    # Lcom/samsung/klmsagent/beans/ContainerData;
    .param p2, "var"    # Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    .param p3, "source"    # Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 35
    const-string v7, "B2C-ContainerServices(): validateRegister().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 38
    :try_start_0
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    .line 39
    .local v4, "msg":Landroid/os/Message;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 40
    .local v5, "request":Lorg/json/JSONObject;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 41
    .local v2, "containerPayLoad":Lorg/json/JSONObject;
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 43
    .local v1, "containerPayInfo":Lorg/json/JSONArray;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 44
    .local v0, "container":Lorg/json/JSONObject;
    const-string v7, "containerId"

    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/ContainerData;->getContainerID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 45
    const-string v7, "containerEventTime"

    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/ContainerData;->getTimestamp()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 47
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 49
    const-string v7, "level"

    const-string v8, "container"

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 50
    const-string v7, "platform"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getAndroidVersion()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 51
    const-string v7, "operationId"

    const/4 v8, 0x1

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 52
    const-string v7, "containerPayInfo"

    invoke-virtual {v2, v7, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 53
    const-string v7, "licKey"

    const-string v8, "KLMB2C"

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 54
    const-string v7, "devId"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getDeviceId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 55
    const-string v7, "model"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getModelName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 56
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryISOFromCSC()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryISOFromCSC()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 58
    const-string v7, "countryCode"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryISOFromCSC()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 59
    :cond_0
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryCode()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryCode()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 60
    const-string v7, "countryName"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getCountryCode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 61
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 62
    const-string v7, "mccCode"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 63
    :cond_2
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMNC1()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMNC1()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 64
    const-string v7, "mncCode"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMNC1()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 66
    :cond_3
    const-string v7, "payLoad"

    invoke-virtual {v5, v7, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 68
    iput-object v5, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 69
    invoke-virtual {p2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v7

    iput v7, v4, Landroid/os/Message;->what:I

    .line 70
    invoke-virtual {p3}, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->ordinal()I

    move-result v7

    iput v7, v4, Landroid/os/Message;->arg1:I

    .line 72
    new-instance v6, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    invoke-direct {v6, v4}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 73
    .local v6, "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "/klm-rest/enrollContainer.do"

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    const-string v7, "validateRegister().END"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 79
    .end local v0    # "container":Lorg/json/JSONObject;
    .end local v1    # "containerPayInfo":Lorg/json/JSONArray;
    .end local v2    # "containerPayLoad":Lorg/json/JSONObject;
    .end local v4    # "msg":Landroid/os/Message;
    .end local v5    # "request":Lorg/json/JSONObject;
    .end local v6    # "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v3

    .line 75
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v7, "validateRegister() has Exception"

    invoke-static {v7, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    const-string v7, "validateRegister().END"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    const-string v8, "validateRegister().END"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    throw v7
.end method

.method public bridge synthetic validateRegister(Lcom/samsung/klmsagent/beans/KLMSComponent;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/klmsagent/beans/KLMSComponent;
    .param p2, "x1"    # Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    .param p3, "x2"    # Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25
    check-cast p1, Lcom/samsung/klmsagent/beans/ContainerData;

    .end local p1    # "x0":Lcom/samsung/klmsagent/beans/KLMSComponent;
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/klmsagent/b2c/services/ContainerServices;->validateRegister(Lcom/samsung/klmsagent/beans/ContainerData;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V

    return-void
.end method

.method public validateTrackKey(Lcom/samsung/klmsagent/beans/ContainerData;II)V
    .locals 0
    .param p1, "obj"    # Lcom/samsung/klmsagent/beans/ContainerData;
    .param p2, "licenseStatus"    # I
    .param p3, "activationId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 359
    return-void
.end method

.method public bridge synthetic validateTrackKey(Lcom/samsung/klmsagent/beans/KLMSComponent;II)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/klmsagent/beans/KLMSComponent;
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25
    check-cast p1, Lcom/samsung/klmsagent/beans/ContainerData;

    .end local p1    # "x0":Lcom/samsung/klmsagent/beans/KLMSComponent;
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/klmsagent/b2c/services/ContainerServices;->validateTrackKey(Lcom/samsung/klmsagent/beans/ContainerData;II)V

    return-void
.end method
