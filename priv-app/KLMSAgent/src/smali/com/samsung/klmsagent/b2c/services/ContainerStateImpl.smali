.class public Lcom/samsung/klmsagent/b2c/services/ContainerStateImpl;
.super Ljava/lang/Object;
.source "ContainerStateImpl.java"

# interfaces
.implements Lcom/samsung/klmsagent/services/ContainerStates;


# static fields
.field private static final TAG:Ljava/lang/String; = "B2C-ContainerStateImpl(): "


# instance fields
.field private context:Landroid/content/Context;

.field private mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/klmsagent/services/KLMSServices",
            "<",
            "Lcom/samsung/klmsagent/beans/ContainerData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/klmsagent/services/KLMSServices;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/klmsagent/services/KLMSServices",
            "<",
            "Lcom/samsung/klmsagent/beans/ContainerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p2, "mContainerHandler":Lcom/samsung/klmsagent/services/KLMSServices;, "Lcom/samsung/klmsagent/services/KLMSServices<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/samsung/klmsagent/b2c/services/ContainerStateImpl;->context:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/samsung/klmsagent/b2c/services/ContainerStateImpl;->mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    .line 38
    return-void
.end method

.method private checkKNOXLicenseStatus()V
    .locals 12

    .prologue
    .line 149
    const-string v9, "B2C-ContainerStateImpl(): checkKNOXLicenseStatus()."

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 151
    const/4 v7, 0x3

    .line 152
    .local v7, "notiTrigger":I
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v1

    .line 154
    .local v1, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v9, 0x0

    :try_start_0
    new-instance v10, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v10}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    invoke-virtual {v1, v9, v10}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v3

    .line 155
    .local v3, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 157
    .local v0, "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v3, :cond_5

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_5

    .line 158
    const/4 v8, 0x0

    .line 159
    .local v8, "success":Z
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 160
    .local v2, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I

    move-result v9

    if-nez v9, :cond_1

    .line 163
    const-string v9, "B2C-ContainerStateImpl(): checkKNOXLicenseStatus().FOTA case. Activation record found but status not there in DB."

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 168
    iget-object v9, p0, Lcom/samsung/klmsagent/b2c/services/ContainerStateImpl;->context:Landroid/content/Context;

    const-string v10, "klmsagent.preferences"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 170
    .local v6, "mSharedPreferences":Landroid/content/SharedPreferences;
    const-string v9, "KLMS_LICENSE_STATUS"

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-nez v9, :cond_2

    .line 172
    const-string v9, "B2C-ContainerStateImpl(): checkKNOXLicenseStatus().FOTA case. License is Active."

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 173
    const/4 v8, 0x1

    .line 174
    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v9

    sget-object v10, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual {v10}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v10

    invoke-static {v9, v10}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->upgradeData(II)V

    .line 180
    .end local v6    # "mSharedPreferences":Landroid/content/SharedPreferences;
    :cond_1
    :goto_1
    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I

    move-result v9

    sget-object v10, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual {v10}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v10

    if-ne v9, v10, :cond_0

    .line 181
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "B2C-ContainerStateImpl(): checkKNOXLicenseStatus().DeviceData.getLicenseStatus: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 183
    const/4 v8, 0x1

    goto :goto_0

    .line 176
    .restart local v6    # "mSharedPreferences":Landroid/content/SharedPreferences;
    :cond_2
    const-string v9, "B2C-ContainerStateImpl(): checkKNOXLicenseStatus().FOTA case. License is Expired."

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 177
    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v9

    sget-object v10, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->EXPIRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual {v10}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v10

    invoke-static {v9, v10}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->upgradeData(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 199
    .end local v0    # "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v2    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v3    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "mSharedPreferences":Landroid/content/SharedPreferences;
    .end local v8    # "success":Z
    :catch_0
    move-exception v4

    .line 200
    .local v4, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v9, "B2C-ContainerStateImpl(): checkKNOXLicenseStatus().Exception found"

    invoke-static {v9, v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 202
    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 204
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    .line 186
    .restart local v0    # "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v3    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v8    # "success":Z
    :cond_3
    if-eqz v8, :cond_4

    .line 187
    :try_start_2
    const-string v9, "B2C-ContainerStateImpl(): checkKNOXLicenseStatus().Activation record found and license is ACTIVE state."

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 188
    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->SendIntentToKAP(IZ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 202
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v8    # "success":Z
    :goto_3
    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_2

    .line 190
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v8    # "success":Z
    :cond_4
    :try_start_3
    const-string v9, "B2C-ContainerStateImpl(): checkKNOXLicenseStatus().ACtivation record found but license is INACTIVE state."

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 192
    const/4 v9, 0x0

    invoke-static {v7, v9}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->SendIntentToKAP(IZ)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 202
    .end local v0    # "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v3    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v8    # "success":Z
    :catchall_0
    move-exception v9

    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v9

    .line 195
    .restart local v0    # "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v3    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_5
    :try_start_4
    const-string v9, "B2C-ContainerStateImpl(): checkKNOXLicenseStatus().No activation record is found when container license check came."

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 197
    const/4 v9, 0x0

    invoke-static {v7, v9}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->SendIntentToKAP(IZ)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3
.end method


# virtual methods
.method public checkLicenseKeyStatus(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 210
    return-void
.end method

.method public createContainerListener(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 42
    const-string v4, "B2C-ContainerStateImpl(): createContainerListener().START"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 45
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 46
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_0

    const-string v4, "container_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_1

    .line 47
    :cond_0
    const-string v4, "B2C-ContainerStateImpl(): createContainerListener() : container_id == NULL."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    const-string v4, "B2C-ContainerStateImpl(): createContainerListener().END"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 76
    .end local v3    # "extras":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 51
    .restart local v3    # "extras":Landroid/os/Bundle;
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/samsung/klmsagent/b2c/services/ContainerStateImpl;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSUtility;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 52
    const-string v4, "B2C-ContainerStateImpl(): createContainerListener() : No network"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    const-string v4, "B2C-ContainerStateImpl(): createContainerListener().END"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_2
    :try_start_2
    invoke-static {}, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;->doGSLBB2CJob()V

    .line 58
    const-string v4, "container_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "containerID":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setB2ContainerId(Ljava/lang/String;)V

    .line 61
    new-instance v0, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v0}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    .line 62
    .local v0, "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerID(Ljava/lang/String;)V

    .line 63
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/samsung/klmsagent/beans/ContainerData;->setTimestamp(Ljava/lang/String;)V

    .line 64
    const-wide/16 v4, 0x320

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 65
    iget-object v4, p0, Lcom/samsung/klmsagent/b2c/services/ContainerStateImpl;->mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    sget-object v5, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REGISTER_B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    sget-object v6, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    invoke-virtual {v4, v0, v5, v6}, Lcom/samsung/klmsagent/services/KLMSServices;->validateRegister(Lcom/samsung/klmsagent/beans/KLMSComponent;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V
    :try_end_2
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 74
    const-string v4, "B2C-ContainerStateImpl(): createContainerListener().END"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 67
    .end local v0    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v1    # "containerID":Ljava/lang/String;
    .end local v3    # "extras":Landroid/os/Bundle;
    :catch_0
    move-exception v2

    .line 68
    .local v2, "e":Lorg/apache/http/MethodNotSupportedException;
    :try_start_3
    const-string v4, "MethodNotSupportedException"

    invoke-static {v4, v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 74
    const-string v4, "B2C-ContainerStateImpl(): createContainerListener().END"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 69
    .end local v2    # "e":Lorg/apache/http/MethodNotSupportedException;
    :catch_1
    move-exception v2

    .line 70
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_4
    const-string v4, "MethodNotSupportedException"

    invoke-static {v4, v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 74
    const-string v4, "B2C-ContainerStateImpl(): createContainerListener().END"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 71
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v2

    .line 72
    .local v2, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v4, "Exception"

    invoke-static {v4, v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 74
    const-string v4, "B2C-ContainerStateImpl(): createContainerListener().END"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    const-string v5, "B2C-ContainerStateImpl(): createContainerListener().END"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    throw v4
.end method

.method public removeContainerListener(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 80
    const-string v2, "B2C-ContainerStateImpl(): containerRemoved().START"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 83
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 84
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    const-string v2, "container_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 85
    :cond_0
    const-string v2, "B2C-ContainerStateImpl(): container id is null"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    const-string v2, "B2C-ContainerStateImpl(): removeContainerListener().END"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 95
    .end local v1    # "extras":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 89
    .restart local v1    # "extras":Landroid/os/Bundle;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/klmsagent/b2c/services/ContainerStateImpl;->mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    const-string v3, "container_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/klmsagent/services/KLMSServices;->onDestroy(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    const-string v2, "B2C-ContainerStateImpl(): removeContainerListener().END"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    .end local v1    # "extras":Landroid/os/Bundle;
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "Exception"

    invoke-static {v2, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 93
    const-string v2, "B2C-ContainerStateImpl(): removeContainerListener().END"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    const-string v3, "B2C-ContainerStateImpl(): removeContainerListener().END"

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    throw v2
.end method

.method public uploadRPMode(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 101
    const-string v4, "B2C-ContainerStateImpl(): uploadRPMode().Start."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 104
    .local v3, "extras":Landroid/os/Bundle;
    if-nez v3, :cond_0

    .line 105
    const-string v4, "B2C-ContainerStateImpl(): uploadRPMode() : intent.extras == NULL."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 146
    :goto_0
    return-void

    .line 109
    :cond_0
    const-string v4, "KAP_RP_MODE_STATUS"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    const-string v4, "KAP_LICENSE_STATUS_REQUEST"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    .line 111
    const-string v4, "KAP_RP_MODE_STATUS"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 112
    .local v1, "RP_MODE_STATUS":Z
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "B2C-ContainerStateImpl(): uploadRPMode().KAP_RP_MODE_STATUS : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 114
    invoke-static {}, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;->doGSLBB2CJob()V

    .line 116
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "B2C-ContainerStateImpl(): uploadRPMode().RP_MODE_STATUS() = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 117
    const-wide/16 v4, 0x320

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 118
    sget-object v4, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->RP_MODE:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    sget-object v5, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->B2C:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    invoke-static {v4, v5, v1}, Lcom/samsung/klmsagent/b2c/services/ContainerServices;->uploadRKPStatus(Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;Z)V
    :try_end_0
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 120
    :catch_0
    move-exception v2

    .line 121
    .local v2, "e":Lorg/apache/http/MethodNotSupportedException;
    const-string v4, "B2C-ContainerStateImpl(): uploadRPMode().MethodNotSupportedException"

    invoke-static {v4, v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 122
    .end local v2    # "e":Lorg/apache/http/MethodNotSupportedException;
    :catch_1
    move-exception v2

    .line 123
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v4, "B2C-ContainerStateImpl(): uploadRPMode().MethodNotSupportedException"

    invoke-static {v4, v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 126
    .end local v1    # "RP_MODE_STATUS":Z
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_1
    const-string v4, "KAP_RP_MODE_STATUS"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_3

    const-string v4, "KAP_LICENSE_STATUS_REQUEST"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 128
    const-string v4, "KAP_LICENSE_STATUS_REQUEST"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 129
    .local v0, "LICENSE_STATUS_REQUEST":Z
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "B2C-ContainerStateImpl(): uploadRPMode().LICENSE_STATUS_REQUEST : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 131
    const/4 v4, 0x1

    if-ne v0, v4, :cond_2

    .line 132
    const-string v4, "B2C-ContainerStateImpl(): uploadRPMode().checkKNOXLicenseStatus()."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 133
    invoke-direct {p0}, Lcom/samsung/klmsagent/b2c/services/ContainerStateImpl;->checkKNOXLicenseStatus()V

    goto/16 :goto_0

    .line 136
    :cond_2
    const-string v4, "B2C-ContainerStateImpl(): uploadRPMode().LICENSE_STATUS_REQUEST == false. Not defined for this extra.  "

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 141
    .end local v0    # "LICENSE_STATUS_REQUEST":Z
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "B2C-ContainerStateImpl(): uploadRPMode().No action for invalid Intent extra. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
