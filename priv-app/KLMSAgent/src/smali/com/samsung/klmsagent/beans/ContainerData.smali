.class public final Lcom/samsung/klmsagent/beans/ContainerData;
.super Lcom/samsung/klmsagent/beans/KLMSComponent;
.source "ContainerData.java"


# instance fields
.field private containerID:Ljava/lang/String;

.field private containerStatus:Ljava/lang/Integer;

.field private devId:Ljava/lang/String;

.field private deviceTracker:Ljava/lang/String;

.field private expiry:Ljava/lang/String;

.field private licKey:Ljava/lang/String;

.field private model:Ljava/lang/String;

.field private packageName:Ljava/lang/String;

.field private timestamp:Ljava/lang/String;

.field private trackerID:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/klmsagent/beans/KLMSComponent;-><init>()V

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/samsung/klmsagent/beans/KLMSComponent;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/ContainerData;->containerID:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public extractData(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 83
    if-nez p2, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    const-string v0, "CONTAINER_ID"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerID(Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :cond_2
    const-string v0, "TIME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/ContainerData;->setTimestamp(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_3
    const-string v0, "TRACKER_ID"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 91
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/ContainerData;->setTrackerID(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_4
    const-string v0, "EXPIRY"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/ContainerData;->setExpiry(Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_5
    const-string v0, "CONTAINER_STATUS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerStatus(Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 96
    :cond_6
    const-string v0, "PACKAGE_NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/ContainerData;->setPackageName(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 75
    const/4 v1, 0x6

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CONTAINER_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "TIME"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "TRACKER_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "EXPIRY"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CONTAINER_STATUS"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "PACKAGE_NAME"

    aput-object v2, v0, v1

    .line 78
    .local v0, "columns":[Ljava/lang/String;
    return-object v0
.end method

.method public getContainerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/ContainerData;->containerID:Ljava/lang/String;

    return-object v0
.end method

.method public getContainerStatus()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/ContainerData;->containerStatus:Ljava/lang/Integer;

    return-object v0
.end method

.method public getContentValue()Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 58
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 59
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "CONTAINER_ID"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/ContainerData;->containerID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v1, "TRACKER_ID"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/ContainerData;->trackerID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v1, "TIME"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/ContainerData;->timestamp:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v1, "EXPIRY"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/ContainerData;->expiry:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v1, "CONTAINER_STATUS"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/ContainerData;->containerStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 64
    const-string v1, "PACKAGE_NAME"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/ContainerData;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-object v0
.end method

.method public getDeleteQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    const-string v0, "TRACKER_ID=?"

    return-object v0
.end method

.method public getDeleteQueryArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 121
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/ContainerData;->trackerID:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getDevId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/ContainerData;->devId:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceTracker()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/ContainerData;->deviceTracker:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/ContainerData;->expiry:Ljava/lang/String;

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/klmsagent/beans/ContainerData;->containerID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLicKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/ContainerData;->licKey:Ljava/lang/String;

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/ContainerData;->model:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/ContainerData;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    const-string v0, "containerinfo"

    return-object v0
.end method

.method public getTimestamp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/ContainerData;->timestamp:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/ContainerData;->trackerID:Ljava/lang/String;

    return-object v0
.end method

.method public setContainerID(Ljava/lang/String;)V
    .locals 0
    .param p1, "containerID"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/ContainerData;->containerID:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setContainerStatus(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "containerStatus"    # Ljava/lang/Integer;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/ContainerData;->containerStatus:Ljava/lang/Integer;

    .line 146
    return-void
.end method

.method public setDevId(Ljava/lang/String;)V
    .locals 0
    .param p1, "devId"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/ContainerData;->devId:Ljava/lang/String;

    .line 162
    return-void
.end method

.method public setDeviceTracker(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceTracker"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/ContainerData;->deviceTracker:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setExpiry(Ljava/lang/String;)V
    .locals 0
    .param p1, "expiry"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/ContainerData;->expiry:Ljava/lang/String;

    .line 138
    return-void
.end method

.method public setLicKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "licKey"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/ContainerData;->licKey:Ljava/lang/String;

    .line 154
    return-void
.end method

.method public setModel(Ljava/lang/String;)V
    .locals 0
    .param p1, "model"    # Ljava/lang/String;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/ContainerData;->model:Ljava/lang/String;

    .line 170
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/ContainerData;->packageName:Ljava/lang/String;

    .line 130
    return-void
.end method

.method public setTimestamp(Ljava/lang/String;)V
    .locals 0
    .param p1, "timestamp"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/ContainerData;->timestamp:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public setTrackerID(Ljava/lang/String;)V
    .locals 0
    .param p1, "trackerID"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/ContainerData;->trackerID:Ljava/lang/String;

    .line 46
    return-void
.end method
