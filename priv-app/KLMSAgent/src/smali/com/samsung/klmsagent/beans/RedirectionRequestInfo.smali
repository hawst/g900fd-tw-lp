.class public Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;
.super Ljava/lang/Object;
.source "RedirectionRequestInfo.java"


# instance fields
.field private mCountryISO:Ljava/lang/String;

.field private mDeviceID:Ljava/lang/String;

.field private mMCC:Ljava/lang/String;

.field private mSalesCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object v0, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mDeviceID:Ljava/lang/String;

    .line 5
    iput-object v0, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mCountryISO:Ljava/lang/String;

    .line 6
    iput-object v0, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mSalesCode:Ljava/lang/String;

    .line 7
    iput-object v0, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mMCC:Ljava/lang/String;

    .line 11
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "deviceID"    # Ljava/lang/String;
    .param p2, "countryISO"    # Ljava/lang/String;
    .param p3, "salesCode"    # Ljava/lang/String;
    .param p4, "MCC"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object v0, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mDeviceID:Ljava/lang/String;

    .line 5
    iput-object v0, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mCountryISO:Ljava/lang/String;

    .line 6
    iput-object v0, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mSalesCode:Ljava/lang/String;

    .line 7
    iput-object v0, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mMCC:Ljava/lang/String;

    .line 14
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mDeviceID:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mCountryISO:Ljava/lang/String;

    .line 16
    iput-object p3, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mSalesCode:Ljava/lang/String;

    .line 17
    iput-object p4, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mMCC:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public getCountryISO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mCountryISO:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mDeviceID:Ljava/lang/String;

    return-object v0
.end method

.method public getMCC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mMCC:Ljava/lang/String;

    return-object v0
.end method

.method public getSalesCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mSalesCode:Ljava/lang/String;

    return-object v0
.end method

.method public setCountryISO(Ljava/lang/String;)V
    .locals 0
    .param p1, "countryISO"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mCountryISO:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setDeviceID(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceID"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mDeviceID:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setMCC(Ljava/lang/String;)V
    .locals 0
    .param p1, "MCC"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mMCC:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setSalesCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "salesCode"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/RedirectionRequestInfo;->mSalesCode:Ljava/lang/String;

    .line 30
    return-void
.end method
