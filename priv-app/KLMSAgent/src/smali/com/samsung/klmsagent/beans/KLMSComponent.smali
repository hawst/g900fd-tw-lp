.class public abstract Lcom/samsung/klmsagent/beans/KLMSComponent;
.super Ljava/lang/Object;
.source "KLMSComponent.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract extractData(Ljava/lang/String;Ljava/lang/Object;)V
.end method

.method public abstract getColumns()[Ljava/lang/String;
.end method

.method public abstract getContentValue()Landroid/content/ContentValues;
.end method

.method public abstract getDeleteQuery()Ljava/lang/String;
.end method

.method public abstract getDeleteQueryArgs()[Ljava/lang/String;
.end method

.method public abstract getID()Ljava/lang/String;
.end method

.method public abstract getTableName()Ljava/lang/String;
.end method
