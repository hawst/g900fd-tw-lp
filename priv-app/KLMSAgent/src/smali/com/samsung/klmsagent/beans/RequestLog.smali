.class public Lcom/samsung/klmsagent/beans/RequestLog;
.super Lcom/samsung/klmsagent/beans/KLMSComponent;
.source "RequestLog.java"


# instance fields
.field private id:Ljava/lang/String;

.field private jsonRequest:Ljava/lang/String;

.field private requestType:I

.field private time:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/samsung/klmsagent/beans/KLMSComponent;-><init>()V

    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJ)V
    .locals 1
    .param p1, "jsonRequest"    # Ljava/lang/String;
    .param p2, "requestType"    # I
    .param p3, "time"    # J

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/samsung/klmsagent/beans/KLMSComponent;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/RequestLog;->jsonRequest:Ljava/lang/String;

    .line 72
    iput p2, p0, Lcom/samsung/klmsagent/beans/RequestLog;->requestType:I

    .line 73
    iput-wide p3, p0, Lcom/samsung/klmsagent/beans/RequestLog;->time:J

    .line 74
    return-void
.end method


# virtual methods
.method public extractData(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 89
    if-nez p2, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    const-string v0, "request_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/RequestLog;->setRequestType(I)V

    goto :goto_0

    .line 94
    :cond_2
    const-string v0, "request_object"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/RequestLog;->setJsonRequest(Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :cond_3
    const-string v0, "TIME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/klmsagent/beans/RequestLog;->setTime(J)V

    goto :goto_0

    .line 98
    :cond_4
    const-string v0, "_id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/RequestLog;->setId(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 83
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "request_type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "request_object"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "TIME"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "_id"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getContentValue()Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 49
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 50
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "request_type"

    iget v2, p0, Lcom/samsung/klmsagent/beans/RequestLog;->requestType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 51
    const-string v1, "request_object"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/RequestLog;->jsonRequest:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v1, "TIME"

    iget-wide v2, p0, Lcom/samsung/klmsagent/beans/RequestLog;->time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 53
    return-object v0
.end method

.method public getDeleteQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    const-string v0, "_id=?"

    return-object v0
.end method

.method public getDeleteQueryArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 115
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/RequestLog;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/RequestLog;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getJsonRequest()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/RequestLog;->jsonRequest:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestType()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/klmsagent/beans/RequestLog;->requestType:I

    return v0
.end method

.method public getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    const-string v0, "log_db"

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/samsung/klmsagent/beans/RequestLog;->time:J

    return-wide v0
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/RequestLog;->id:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public setJsonRequest(Ljava/lang/String;)V
    .locals 0
    .param p1, "jsonRequest"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/RequestLog;->jsonRequest:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setRequestType(I)V
    .locals 0
    .param p1, "requestType"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/samsung/klmsagent/beans/RequestLog;->requestType:I

    .line 36
    return-void
.end method

.method public setTime(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/klmsagent/beans/RequestLog;->time:J

    .line 44
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RequestLog [id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/klmsagent/beans/RequestLog;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", jsonRequest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/klmsagent/beans/RequestLog;->jsonRequest:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requestType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/klmsagent/beans/RequestLog;->requestType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/klmsagent/beans/RequestLog;->time:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
