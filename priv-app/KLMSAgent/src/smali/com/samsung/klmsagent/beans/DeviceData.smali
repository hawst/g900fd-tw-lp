.class public final Lcom/samsung/klmsagent/beans/DeviceData;
.super Lcom/samsung/klmsagent/beans/KLMSComponent;
.source "DeviceData.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private _id:I

.field private alarmTime:Ljava/lang/String;

.field private expiry:Ljava/lang/String;

.field private licenseKey:Ljava/lang/String;

.field private licenseStatus:I

.field private nextValidation:Ljava/lang/String;

.field private onPremInfo:I

.field private packageName:Ljava/lang/String;

.field private time:Ljava/lang/String;

.field private trackerId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/samsung/klmsagent/beans/KLMSComponent;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/klmsagent/beans/DeviceData;->trackerId:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public extractData(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 61
    if-nez p2, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    const-string v0, "TIME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setTime(Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :cond_2
    const-string v0, "TRACKER_ID"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 67
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setTrackerId(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_3
    const-string v0, "PACKAGE_NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 69
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setPackageName(Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :cond_4
    const-string v0, "_id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->set_id(I)V

    goto :goto_0

    .line 72
    :cond_5
    const-string v0, "LICENSE_STATUS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setLicenseStatus(I)V

    goto/16 :goto_0

    .line 74
    :cond_6
    const-string v0, "LICENSE_EXPIRY"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 75
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setExpiry(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 76
    :cond_7
    const-string v0, "NEXT_VALIDATION"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 77
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setNextValidation(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 78
    :cond_8
    const-string v0, "ALARM_TIME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 79
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setAlarmTime(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 80
    :cond_9
    const-string v0, "ONPREM_INFO"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setOnPremInfo(I)V

    goto/16 :goto_0
.end method

.method public getAlarmTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/DeviceData;->alarmTime:Ljava/lang/String;

    return-object v0
.end method

.method public getColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 53
    const/16 v1, 0x9

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "TIME"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "TRACKER_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "PACKAGE_NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "LICENSE_EXPIRY"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "LICENSE_STATUS"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "NEXT_VALIDATION"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ALARM_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ONPREM_INFO"

    aput-object v2, v0, v1

    .line 57
    .local v0, "columns":[Ljava/lang/String;
    return-object v0
.end method

.method public getContentValue()Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 32
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 33
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "TRACKER_ID"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/DeviceData;->trackerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const-string v1, "TIME"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/DeviceData;->time:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string v1, "PACKAGE_NAME"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/DeviceData;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const-string v1, "LICENSE_EXPIRY"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/DeviceData;->expiry:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    const-string v1, "LICENSE_STATUS"

    iget v2, p0, Lcom/samsung/klmsagent/beans/DeviceData;->licenseStatus:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 38
    const-string v1, "NEXT_VALIDATION"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/DeviceData;->nextValidation:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const-string v1, "ALARM_TIME"

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/DeviceData;->alarmTime:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v1, "ONPREM_INFO"

    iget v2, p0, Lcom/samsung/klmsagent/beans/DeviceData;->onPremInfo:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 41
    return-object v0
.end method

.method public getDeleteQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    const-string v0, "PACKAGE_NAME=?"

    return-object v0
.end method

.method public getDeleteQueryArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 164
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/klmsagent/beans/DeviceData;->packageName:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getExpiry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/DeviceData;->expiry:Ljava/lang/String;

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/samsung/klmsagent/beans/DeviceData;->_id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLicenseKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/DeviceData;->licenseKey:Ljava/lang/String;

    return-object v0
.end method

.method public getLicenseStatus()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/samsung/klmsagent/beans/DeviceData;->licenseStatus:I

    return v0
.end method

.method public getNextValidation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/DeviceData;->nextValidation:Ljava/lang/String;

    return-object v0
.end method

.method public getOnPremInfo()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/samsung/klmsagent/beans/DeviceData;->onPremInfo:I

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/DeviceData;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    const-string v0, "deviceinfo"

    return-object v0
.end method

.method public getTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/DeviceData;->time:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/klmsagent/beans/DeviceData;->trackerId:Ljava/lang/String;

    return-object v0
.end method

.method public get_id()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/samsung/klmsagent/beans/DeviceData;->_id:I

    return v0
.end method

.method public setAlarmTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "alarmTime"    # Ljava/lang/String;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/DeviceData;->alarmTime:Ljava/lang/String;

    .line 155
    return-void
.end method

.method public setExpiry(Ljava/lang/String;)V
    .locals 0
    .param p1, "expiry"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/DeviceData;->expiry:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public setLicenseKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "licenseKey"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/DeviceData;->licenseKey:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setLicenseStatus(I)V
    .locals 0
    .param p1, "licenseStatus"    # I

    .prologue
    .line 130
    iput p1, p0, Lcom/samsung/klmsagent/beans/DeviceData;->licenseStatus:I

    .line 131
    return-void
.end method

.method public setNextValidation(Ljava/lang/String;)V
    .locals 0
    .param p1, "nextValidation"    # Ljava/lang/String;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/DeviceData;->nextValidation:Ljava/lang/String;

    .line 147
    return-void
.end method

.method public setOnPremInfo(I)V
    .locals 0
    .param p1, "onPremInfo"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/samsung/klmsagent/beans/DeviceData;->onPremInfo:I

    .line 91
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/DeviceData;->packageName:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public setTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/DeviceData;->time:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setTrackerId(Ljava/lang/String;)V
    .locals 0
    .param p1, "trackerId"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/samsung/klmsagent/beans/DeviceData;->trackerId:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public set_id(I)V
    .locals 0
    .param p1, "_id"    # I

    .prologue
    .line 172
    iput p1, p0, Lcom/samsung/klmsagent/beans/DeviceData;->_id:I

    .line 173
    return-void
.end method
