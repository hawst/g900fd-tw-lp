.class public final Lcom/samsung/klmsagent/data/DbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DbHelper.java"


# static fields
.field public static mDataSource:Lcom/samsung/klmsagent/data/DbHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/klmsagent/data/DbHelper;->mDataSource:Lcom/samsung/klmsagent/data/DbHelper;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    const-string v0, "klms.db"

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 20
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/klmsagent/data/DbHelper;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 105
    const-class v1, Lcom/samsung/klmsagent/data/DbHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/klmsagent/data/DbHelper;->mDataSource:Lcom/samsung/klmsagent/data/DbHelper;

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Lcom/samsung/klmsagent/data/DbHelper;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/samsung/klmsagent/data/DbHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/klmsagent/data/DbHelper;->mDataSource:Lcom/samsung/klmsagent/data/DbHelper;

    .line 108
    :cond_0
    sget-object v0, Lcom/samsung/klmsagent/data/DbHelper;->mDataSource:Lcom/samsung/klmsagent/data/DbHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    monitor-exit p0

    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 27
    const-string v0, "Creating the all the tables."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 28
    const-string v0, " CREATE TABLE deviceinfo (  _id INTEGER PRIMARY KEY AUTOINCREMENT,TIME TEXT NOT NULL, TRACKER_ID TEXT, PACKAGE_NAME TEXT, LICENSE_STATUS INTEGER, LICENSE_EXPIRY TEXT, NEXT_VALIDATION TEXT, ALARM_TIME TEXT, ONPREM_INFO INTEGER );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 29
    const-string v0, " CREATE TABLE containerinfo (  _id INTEGER PRIMARY KEY AUTOINCREMENT,TIME TEXT , EXPIRY TEXT , CONTAINER_ID TEXT NOT NULL, CONTAINER_STATUS INTEGER DEFAULT 11, TRACKER_ID TEXT, PACKAGE_NAME TEXT );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 30
    const-string v0, "CREATE TABLE log_db ( _id INTEGER PRIMARY KEY AUTOINCREMENT,request_type TEXT NOT NULL, request_object BLOB NOT NULL, TIME TIME NOT NULL );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 39
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Upgrading database from version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", which will destroy all old data"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 41
    const/4 v1, 0x3

    if-ne p3, v1, :cond_2

    .line 42
    if-ne p2, v3, :cond_1

    .line 43
    const-string v1, "DbHelper(): onUpgrade(): V1 ==> V3"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 45
    :try_start_0
    const-string v1, "ALTER TABLE deviceinfo ADD COLUMN PACKAGE_NAME TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 47
    const-string v1, "ALTER TABLE deviceinfo ADD COLUMN LICENSE_STATUS INTEGER"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 49
    const-string v1, "ALTER TABLE deviceinfo ADD COLUMN LICENSE_EXPIRY TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 51
    const-string v1, "ALTER TABLE deviceinfo ADD COLUMN NEXT_VALIDATION TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 53
    const-string v1, "ALTER TABLE deviceinfo ADD COLUMN ALARM_TIME TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 55
    const-string v1, "ALTER TABLE deviceinfo ADD COLUMN ONPREM_INFO INTEGER"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 58
    const-string v1, "ALTER TABLE containerinfo ADD COLUMN PACKAGE_NAME TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "column already exists. No need to alter."

    invoke-static {v1, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 63
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    if-ne p2, v4, :cond_0

    .line 64
    const-string v1, "DbHelper(): onUpgrade(): V2 ==> V3"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 66
    :try_start_1
    const-string v1, "ALTER TABLE deviceinfo ADD COLUMN LICENSE_STATUS INTEGER"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 68
    const-string v1, "ALTER TABLE deviceinfo ADD COLUMN LICENSE_EXPIRY TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 70
    const-string v1, "ALTER TABLE deviceinfo ADD COLUMN NEXT_VALIDATION TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 72
    const-string v1, "ALTER TABLE deviceinfo ADD COLUMN ALARM_TIME TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 74
    const-string v1, "ALTER TABLE deviceinfo ADD COLUMN ONPREM_INFO INTEGER "

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 77
    const-string v1, "ALTER TABLE containerinfo ADD COLUMN PACKAGE_NAME TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 79
    :catch_1
    move-exception v0

    .line 80
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "column already exists. No need to alter."

    invoke-static {v1, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 83
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    if-ne p3, v4, :cond_0

    .line 84
    if-ne p2, v3, :cond_0

    .line 86
    const-string v1, "adding the new column for 4.4"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 88
    :try_start_2
    const-string v1, "ALTER TABLE deviceinfo ADD COLUMN PACKAGE_NAME TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 90
    :catch_2
    move-exception v0

    .line 91
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "column already exists. No need to alter."

    invoke-static {v1, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
