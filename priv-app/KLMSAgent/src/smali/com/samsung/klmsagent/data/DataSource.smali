.class public final Lcom/samsung/klmsagent/data/DataSource;
.super Ljava/lang/Object;
.source "DataSource.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DataSource(): "

.field private static final mSynch:Ljava/lang/Object;


# instance fields
.field private atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/klmsagent/data/DataSource;->mSynch:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/samsung/klmsagent/data/DbHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/klmsagent/data/DbHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 30
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 33
    sget-object v1, Lcom/samsung/klmsagent/data/DataSource;->mSynch:Ljava/lang/Object;

    monitor-enter v1

    .line 34
    :try_start_0
    iget-object v0, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V

    .line 35
    monitor-exit v1

    .line 36
    return-void

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public deleteData(Lcom/samsung/klmsagent/beans/KLMSComponent;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/klmsagent/beans/KLMSComponent;",
            ">(TT;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 68
    .local p1, "data":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    iget-object v1, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getTableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getDeleteQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getDeleteQueryArgs()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 70
    .local v0, "value":I
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 71
    return v0
.end method

.method public deleteData(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/klmsagent/beans/KLMSComponent;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "appDataBatch":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/beans/KLMSComponent;

    .line 76
    .local v0, "_var":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    invoke-virtual {p0, v0}, Lcom/samsung/klmsagent/data/DataSource;->deleteData(Lcom/samsung/klmsagent/beans/KLMSComponent;)I

    goto :goto_0

    .line 78
    .end local v0    # "_var":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public deleteTables(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "whereClause"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V
    .locals 3
    .param p1, "cu"    # Landroid/database/Cursor;
    .param p3, "i"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/klmsagent/beans/KLMSComponent;",
            ">(",
            "Landroid/database/Cursor;",
            "TT;I)V"
        }
    .end annotation

    .prologue
    .line 265
    .local p2, "obj":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getType(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 300
    const-string v1, "Unknown data type can\'t handle.!!!!"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 303
    :goto_0
    return-void

    .line 272
    :pswitch_0
    const-string v1, "DataSource(): Cursor.FIELD_TYPE_BLOB : Do not use this."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 275
    :pswitch_1
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->extractData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 276
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DataSource FIELD_TYPE_FLOAT "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "----"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 279
    .local v0, "log":Ljava/lang/String;
    goto :goto_0

    .line 281
    .end local v0    # "log":Ljava/lang/String;
    :pswitch_2
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->extractData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 282
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DataSource FIELD_TYPE_INTEGER "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "----"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 285
    .restart local v0    # "log":Ljava/lang/String;
    goto :goto_0

    .line 287
    .end local v0    # "log":Ljava/lang/String;
    :pswitch_3
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->extractData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 288
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DataSource FIELD_TYPE_NULL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "----"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 292
    .restart local v0    # "log":Ljava/lang/String;
    goto/16 :goto_0

    .line 294
    .end local v0    # "log":Ljava/lang/String;
    :pswitch_4
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->extractData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DataSource FIELD_TYPE_STRING "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "----"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 298
    .restart local v0    # "log":Ljava/lang/String;
    goto/16 :goto_0

    .line 265
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;
    .locals 13
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/klmsagent/beans/KLMSComponent;",
            ">(",
            "Ljava/lang/String;",
            "TT;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 84
    .local p2, "obj":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 85
    .local v12, "objList":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const/4 v9, 0x0

    .line 87
    .local v9, "cu":Landroid/database/Cursor;
    if-nez p1, :cond_1

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getTableName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 93
    :cond_0
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 94
    instance-of v0, p2, Lcom/samsung/klmsagent/beans/DeviceData;

    if-eqz v0, :cond_4

    .line 95
    new-instance v8, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 96
    .local v8, "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ge v11, v0, :cond_2

    .line 97
    invoke-virtual {p0, v9, v8, v11}, Lcom/samsung/klmsagent/data/DataSource;->extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V

    .line 96
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 91
    .end local v8    # "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v11    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getTableName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getID()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    goto :goto_0

    .line 98
    .restart local v8    # "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v11    # "i":I
    :cond_2
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 111
    .end local v8    # "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v11    # "i":I
    :catch_0
    move-exception v10

    .line 112
    .local v10, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fetchData(String query, T obj) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    if-eqz v9, :cond_3

    .line 115
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 118
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DataSource(): Fetched Data from data base count : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 119
    return-object v12

    .line 99
    :cond_4
    :try_start_2
    instance-of v0, p2, Lcom/samsung/klmsagent/beans/ContainerData;

    if-eqz v0, :cond_7

    .line 100
    new-instance v8, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    .line 101
    .local v8, "_obj":Lcom/samsung/klmsagent/beans/ContainerData;
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_3
    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ge v11, v0, :cond_5

    .line 102
    invoke-virtual {p0, v9, v8, v11}, Lcom/samsung/klmsagent/data/DataSource;->extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V

    .line 101
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 103
    :cond_5
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 114
    .end local v8    # "_obj":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v11    # "i":I
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_6

    .line 115
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 104
    :cond_7
    :try_start_3
    instance-of v0, p2, Lcom/samsung/klmsagent/beans/RequestLog;

    if-eqz v0, :cond_0

    .line 105
    new-instance v8, Lcom/samsung/klmsagent/beans/RequestLog;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>()V

    .line 106
    .local v8, "_obj":Lcom/samsung/klmsagent/beans/RequestLog;
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_4
    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ge v11, v0, :cond_8

    .line 107
    invoke-virtual {p0, v9, v8, v11}, Lcom/samsung/klmsagent/data/DataSource;->extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V

    .line 106
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 108
    :cond_8
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 114
    .end local v8    # "_obj":Lcom/samsung/klmsagent/beans/RequestLog;
    .end local v11    # "i":I
    :cond_9
    if-eqz v9, :cond_3

    .line 115
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_2
.end method

.method public varargs fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p1, "query"    # Ljava/lang/String;
    .param p3, "sort"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/klmsagent/beans/KLMSComponent;",
            ">(",
            "Ljava/lang/String;",
            "TT;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 167
    .local p2, "obj":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 168
    .local v12, "objList":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const/4 v9, 0x0

    .line 170
    .local v9, "cu":Landroid/database/Cursor;
    if-nez p1, :cond_1

    .line 171
    :try_start_0
    iget-object v0, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getTableName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 176
    :cond_0
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 177
    instance-of v0, p2, Lcom/samsung/klmsagent/beans/DeviceData;

    if-eqz v0, :cond_4

    .line 178
    new-instance v8, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 179
    .local v8, "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ge v11, v0, :cond_2

    .line 180
    invoke-virtual {p0, v9, v8, v11}, Lcom/samsung/klmsagent/data/DataSource;->extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V

    .line 179
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 174
    .end local v8    # "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v11    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getTableName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p1

    move-object/from16 v4, p4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    goto :goto_0

    .line 181
    .restart local v8    # "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v11    # "i":I
    :cond_2
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 194
    .end local v8    # "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v11    # "i":I
    :catch_0
    move-exception v10

    .line 195
    .local v10, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fetchData(String query, T obj, String sort, String... selectionArgs) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 197
    if-eqz v9, :cond_3

    .line 198
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 201
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DataSource(): Fetched Data from data base count : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 202
    return-object v12

    .line 182
    :cond_4
    :try_start_2
    instance-of v0, p2, Lcom/samsung/klmsagent/beans/ContainerData;

    if-eqz v0, :cond_7

    .line 183
    new-instance v8, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    .line 184
    .local v8, "_obj":Lcom/samsung/klmsagent/beans/ContainerData;
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_3
    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ge v11, v0, :cond_5

    .line 185
    invoke-virtual {p0, v9, v8, v11}, Lcom/samsung/klmsagent/data/DataSource;->extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V

    .line 184
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 186
    :cond_5
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 197
    .end local v8    # "_obj":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v11    # "i":I
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_6

    .line 198
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 187
    :cond_7
    :try_start_3
    instance-of v0, p2, Lcom/samsung/klmsagent/beans/RequestLog;

    if-eqz v0, :cond_0

    .line 188
    new-instance v8, Lcom/samsung/klmsagent/beans/RequestLog;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>()V

    .line 189
    .local v8, "_obj":Lcom/samsung/klmsagent/beans/RequestLog;
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_4
    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ge v11, v0, :cond_8

    .line 190
    invoke-virtual {p0, v9, v8, v11}, Lcom/samsung/klmsagent/data/DataSource;->extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V

    .line 189
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 191
    :cond_8
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 197
    .end local v8    # "_obj":Lcom/samsung/klmsagent/beans/RequestLog;
    .end local v11    # "i":I
    :cond_9
    if-eqz v9, :cond_3

    .line 198
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_2
.end method

.method public varargs fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;[Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p1, "query"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/klmsagent/beans/KLMSComponent;",
            ">(",
            "Ljava/lang/String;",
            "TT;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 125
    .local p2, "obj":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 126
    .local v12, "objList":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const/4 v9, 0x0

    .line 128
    .local v9, "cu":Landroid/database/Cursor;
    if-nez p1, :cond_1

    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getTableName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 134
    :cond_0
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 135
    instance-of v0, p2, Lcom/samsung/klmsagent/beans/DeviceData;

    if-eqz v0, :cond_4

    .line 136
    new-instance v8, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 137
    .local v8, "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ge v11, v0, :cond_2

    .line 138
    invoke-virtual {p0, v9, v8, v11}, Lcom/samsung/klmsagent/data/DataSource;->extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V

    .line 137
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 132
    .end local v8    # "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v11    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getTableName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p1

    move-object/from16 v4, p3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    goto :goto_0

    .line 139
    .restart local v8    # "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v11    # "i":I
    :cond_2
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 152
    .end local v8    # "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v11    # "i":I
    :catch_0
    move-exception v10

    .line 153
    .local v10, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fetchData(String query, T obj, String... selectionArgs) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    if-eqz v9, :cond_3

    .line 156
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 159
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DataSource(): Fetched Data from data base count : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 160
    return-object v12

    .line 140
    :cond_4
    :try_start_2
    instance-of v0, p2, Lcom/samsung/klmsagent/beans/ContainerData;

    if-eqz v0, :cond_7

    .line 141
    new-instance v8, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    .line 142
    .local v8, "_obj":Lcom/samsung/klmsagent/beans/ContainerData;
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_3
    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ge v11, v0, :cond_5

    .line 143
    invoke-virtual {p0, v9, v8, v11}, Lcom/samsung/klmsagent/data/DataSource;->extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V

    .line 142
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 144
    :cond_5
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 155
    .end local v8    # "_obj":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v11    # "i":I
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_6

    .line 156
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 145
    :cond_7
    :try_start_3
    instance-of v0, p2, Lcom/samsung/klmsagent/beans/RequestLog;

    if-eqz v0, :cond_0

    .line 146
    new-instance v8, Lcom/samsung/klmsagent/beans/RequestLog;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>()V

    .line 147
    .local v8, "_obj":Lcom/samsung/klmsagent/beans/RequestLog;
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_4
    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ge v11, v0, :cond_8

    .line 148
    invoke-virtual {p0, v9, v8, v11}, Lcom/samsung/klmsagent/data/DataSource;->extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V

    .line 147
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 149
    :cond_8
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 155
    .end local v8    # "_obj":Lcom/samsung/klmsagent/beans/RequestLog;
    .end local v11    # "i":I
    :cond_9
    if-eqz v9, :cond_3

    .line 156
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_2
.end method

.method public getData(Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;)Lcom/samsung/klmsagent/beans/KLMSComponent;
    .locals 8
    .param p2, "column"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/klmsagent/beans/KLMSComponent;",
            ">(TT;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 231
    .local p1, "obj":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    const/4 v4, 0x0

    .line 232
    .local v4, "result":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    sget-object v6, Lcom/samsung/klmsagent/data/DataSource;->mSynch:Ljava/lang/Object;

    monitor-enter v6

    .line 233
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SELECT * FROM "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getTableName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " WHERE "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " = \'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 234
    .local v1, "countQuery":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 235
    iget-object v5, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v1, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 236
    .local v2, "cu":Landroid/database/Cursor;
    :cond_0
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 237
    instance-of v5, p1, Lcom/samsung/klmsagent/beans/DeviceData;

    if-eqz v5, :cond_2

    .line 238
    new-instance v0, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v0}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 239
    .local v0, "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v5

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 240
    invoke-virtual {p0, v2, v0, v3}, Lcom/samsung/klmsagent/data/DataSource;->extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V

    .line 239
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 241
    :cond_1
    move-object v4, v0

    .line 242
    goto :goto_0

    .end local v0    # "_obj":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v3    # "i":I
    :cond_2
    instance-of v5, p1, Lcom/samsung/klmsagent/beans/ContainerData;

    if-eqz v5, :cond_4

    .line 243
    new-instance v0, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v0}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    .line 244
    .local v0, "_obj":Lcom/samsung/klmsagent/beans/ContainerData;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v5

    array-length v5, v5

    if-ge v3, v5, :cond_3

    .line 245
    invoke-virtual {p0, v2, v0, v3}, Lcom/samsung/klmsagent/data/DataSource;->extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V

    .line 244
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 246
    :cond_3
    move-object v4, v0

    .line 247
    goto :goto_0

    .end local v0    # "_obj":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v3    # "i":I
    :cond_4
    instance-of v5, p1, Lcom/samsung/klmsagent/beans/RequestLog;

    if-eqz v5, :cond_0

    .line 248
    new-instance v0, Lcom/samsung/klmsagent/beans/RequestLog;

    invoke-direct {v0}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>()V

    .line 249
    .local v0, "_obj":Lcom/samsung/klmsagent/beans/RequestLog;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getColumns()[Ljava/lang/String;

    move-result-object v5

    array-length v5, v5

    if-ge v3, v5, :cond_5

    .line 250
    invoke-virtual {p0, v2, v0, v3}, Lcom/samsung/klmsagent/data/DataSource;->extractValue(Landroid/database/Cursor;Lcom/samsung/klmsagent/beans/KLMSComponent;I)V

    .line 249
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 251
    :cond_5
    move-object v4, v0

    .line 252
    goto :goto_0

    .line 254
    .end local v0    # "_obj":Lcom/samsung/klmsagent/beans/RequestLog;
    .end local v3    # "i":I
    :cond_6
    if-eqz v2, :cond_7

    .line 255
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 258
    :cond_7
    monitor-exit v6

    return-object v4

    .line 259
    .end local v1    # "countQuery":Ljava/lang/String;
    .end local v2    # "cu":Landroid/database/Cursor;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public getRowCount(Lcom/samsung/klmsagent/beans/KLMSComponent;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/klmsagent/beans/KLMSComponent;",
            ">(TT;)I"
        }
    .end annotation

    .prologue
    .line 206
    .local p1, "obj":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    sget-object v4, Lcom/samsung/klmsagent/data/DataSource;->mSynch:Ljava/lang/Object;

    monitor-enter v4

    .line 207
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT  * FROM "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getTableName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 208
    .local v1, "countQuery":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 209
    .local v2, "cursor":Landroid/database/Cursor;
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 210
    .local v0, "count":I
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 212
    monitor-exit v4

    return v0

    .line 213
    .end local v0    # "count":I
    .end local v1    # "countQuery":Ljava/lang/String;
    .end local v2    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/klmsagent/beans/KLMSComponent;",
            ">(TT;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "data":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    sget-object v3, Lcom/samsung/klmsagent/data/DataSource;->mSynch:Ljava/lang/Object;

    monitor-enter v3

    .line 40
    :try_start_0
    iget-object v2, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getTableName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getContentValue()Landroid/content/ContentValues;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 42
    .local v0, "value":J
    const-wide/16 v4, -0x1

    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    monitor-exit v3

    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 43
    .end local v0    # "value":J
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public saveData(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/klmsagent/beans/KLMSComponent;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "appDataBatch":Ljava/util/List;, "Ljava/util/List<TT;>;"
    sget-object v3, Lcom/samsung/klmsagent/data/DataSource;->mSynch:Ljava/lang/Object;

    monitor-enter v3

    .line 48
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/klmsagent/beans/KLMSComponent;

    .line 50
    .local v1, "t":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    invoke-virtual {p0, v1}, Lcom/samsung/klmsagent/data/DataSource;->saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z

    goto :goto_0

    .line 54
    .end local v0    # "iterator":Ljava/util/Iterator;
    .end local v1    # "t":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 52
    .restart local v0    # "iterator":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 53
    const/4 v2, 0x0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v2
.end method

.method public updateData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)I
    .locals 6
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/klmsagent/beans/KLMSComponent;",
            ">(",
            "Ljava/lang/String;",
            "TT;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 58
    .local p2, "data":Lcom/samsung/klmsagent/beans/KLMSComponent;, "TT;"
    iget-object v0, p0, Lcom/samsung/klmsagent/data/DataSource;->atsClientDbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getTableName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getContentValue()Landroid/content/ContentValues;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcom/samsung/klmsagent/beans/KLMSComponent;->getID()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public updateData(Ljava/util/List;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/klmsagent/beans/KLMSComponent;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "appDataBatch":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const/4 v0, 0x0

    return v0
.end method
