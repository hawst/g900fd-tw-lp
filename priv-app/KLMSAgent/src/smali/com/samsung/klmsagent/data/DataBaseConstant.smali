.class public Lcom/samsung/klmsagent/data/DataBaseConstant;
.super Ljava/lang/Object;
.source "DataBaseConstant.java"


# static fields
.field public static final COLUMN_ALARM_TIME:Ljava/lang/String; = "ALARM_TIME"

.field public static final COLUMN_CONTAINER_ID:Ljava/lang/String; = "CONTAINER_ID"

.field public static final COLUMN_CONTAINER_STATUS:Ljava/lang/String; = "CONTAINER_STATUS"

.field public static final COLUMN_EXPIRY:Ljava/lang/String; = "EXPIRY"

.field public static final COLUMN_LICENSE_EXPIRY:Ljava/lang/String; = "LICENSE_EXPIRY"

.field public static final COLUMN_LICENSE_STATUS:Ljava/lang/String; = "LICENSE_STATUS"

.field public static final COLUMN_NEXT_VALIDATION:Ljava/lang/String; = "NEXT_VALIDATION"

.field public static final COLUMN_ONPREM_INFO:Ljava/lang/String; = "ONPREM_INFO"

.field public static final COLUMN_PACKAGE_NAME:Ljava/lang/String; = "PACKAGE_NAME"

.field public static final COLUMN_TIME:Ljava/lang/String; = "TIME"

.field public static final COLUMN_TRACKER_ID:Ljava/lang/String; = "TRACKER_ID"

.field public static final CONTAINER_DB:Ljava/lang/String; = "containerinfo"

.field public static final CREATE_CONTAINER_DATA_QUERY:Ljava/lang/String; = " CREATE TABLE containerinfo (  _id INTEGER PRIMARY KEY AUTOINCREMENT,TIME TEXT , EXPIRY TEXT , CONTAINER_ID TEXT NOT NULL, CONTAINER_STATUS INTEGER DEFAULT 11, TRACKER_ID TEXT, PACKAGE_NAME TEXT );"

.field public static final CREATE_DEVICE_DATA_QUERY:Ljava/lang/String; = " CREATE TABLE deviceinfo (  _id INTEGER PRIMARY KEY AUTOINCREMENT,TIME TEXT NOT NULL, TRACKER_ID TEXT, PACKAGE_NAME TEXT, LICENSE_STATUS INTEGER, LICENSE_EXPIRY TEXT, NEXT_VALIDATION TEXT, ALARM_TIME TEXT, ONPREM_INFO INTEGER );"

.field public static final CREATE_LOG_DB:Ljava/lang/String; = "CREATE TABLE log_db ( _id INTEGER PRIMARY KEY AUTOINCREMENT,request_type TEXT NOT NULL, request_object BLOB NOT NULL, TIME TIME NOT NULL );"

.field public static final DATABASE_NAME:Ljava/lang/String; = "klms.db"

.field public static final DATABASE_VERSION:I = 0x3

.field public static final DATABASE_VERSION_2:I = 0x2

.field public static final DATABASE_VERSION_OLD:I = 0x1

.field public static final DELETE_CONTAINER_DATA_QUERY:Ljava/lang/String; = "DROP TABLE IF EXISTS containerinfo"

.field public static final DELETE_DEVICE_DATA_QUERY:Ljava/lang/String; = "DROP TABLE IF EXISTS deviceinfo"

.field public static final DELETE_LOG_DB:Ljava/lang/String; = "DROP TABLE IF EXISTS log_db"

.field public static final DEVICE_DB:Ljava/lang/String; = "deviceinfo"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final LOG_DB:Ljava/lang/String; = "log_db"

.field public static final REQUEST_OBJECT:Ljava/lang/String; = "request_object"

.field public static final REQUEST_TYPE:Ljava/lang/String; = "request_type"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    return-void
.end method
