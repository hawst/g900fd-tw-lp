.class public Lcom/samsung/klmsagent/data/DataSourceManager;
.super Ljava/lang/Object;
.source "DataSourceManager.java"


# static fields
.field private static dataSource:Lcom/samsung/klmsagent/data/DataSource;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDataSource()Lcom/samsung/klmsagent/data/DataSource;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/samsung/klmsagent/data/DataSourceManager;->dataSource:Lcom/samsung/klmsagent/data/DataSource;

    return-object v0
.end method

.method public static setDataSource()V
    .locals 3

    .prologue
    .line 14
    const-class v1, Lcom/samsung/klmsagent/data/DataSourceManager;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/samsung/klmsagent/data/DataSourceManager;->dataSource:Lcom/samsung/klmsagent/data/DataSource;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/samsung/klmsagent/data/DataSource;

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/samsung/klmsagent/data/DataSource;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/klmsagent/data/DataSourceManager;->dataSource:Lcom/samsung/klmsagent/data/DataSource;

    .line 18
    :cond_0
    monitor-exit v1

    .line 19
    return-void

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
