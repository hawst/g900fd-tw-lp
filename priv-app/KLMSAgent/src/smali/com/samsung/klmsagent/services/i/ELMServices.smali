.class public Lcom/samsung/klmsagent/services/i/ELMServices;
.super Ljava/lang/Object;
.source "ELMServices.java"

# interfaces
.implements Lcom/samsung/klmsagent/services/ELMStates;


# static fields
.field private static final TAG:Ljava/lang/String; = "ELMServices(): "


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static checkLicenseStatusToSendELM(I)V
    .locals 14
    .param p0, "type"    # I

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ELMServices(): checkLicenseStatusToSendELM().type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 50
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v7

    .line 53
    .local v7, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v1}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    invoke-virtual {v7, v0, v1}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v9

    .line 54
    .local v9, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .local v6, "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v9, :cond_7

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 57
    const/4 v13, 0x0

    .line 59
    .local v13, "success":Z
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 60
    .local v8, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v8}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    invoke-virtual {v8}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I

    move-result v0

    if-nez v0, :cond_1

    .line 63
    const-string v0, "ELMServices(): FOTA case. Activation record found but license status not there in DB."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "klmsagent.preferences"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v12

    .line 67
    .local v12, "mSharedPreferences":Landroid/content/SharedPreferences;
    const-string v0, "KLMS_LICENSE_STATUS"

    const/4 v1, 0x0

    invoke-interface {v12, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 69
    const-string v0, "ELMServices(): FOTA case. License is Active."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 70
    const/4 v13, 0x1

    .line 71
    invoke-virtual {v8}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v0

    sget-object v1, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->upgradeData(II)V

    .line 77
    .end local v12    # "mSharedPreferences":Landroid/content/SharedPreferences;
    :cond_1
    :goto_1
    invoke-virtual {v8}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I

    move-result v0

    sget-object v1, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ELMServices(): DeviceData.getLicenseStatus: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 79
    const/4 v13, 0x1

    goto :goto_0

    .line 73
    .restart local v12    # "mSharedPreferences":Landroid/content/SharedPreferences;
    :cond_2
    const-string v0, "ELMServices(): FOTA case. License is Expired."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 74
    invoke-virtual {v8}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v0

    sget-object v1, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->EXPIRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->upgradeData(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 113
    .end local v6    # "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v8    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v9    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v12    # "mSharedPreferences":Landroid/content/SharedPreferences;
    .end local v13    # "success":Z
    :catch_0
    move-exception v10

    .line 114
    .local v10, "e":Ljava/lang/Exception;
    const-string v0, "ELMServices():  Exception found"

    invoke-static {v0, v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 116
    .end local v10    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    .line 83
    .restart local v6    # "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v9    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v11    # "i$":Ljava/util/Iterator;
    .restart local v13    # "success":Z
    :cond_3
    if-eqz v13, :cond_5

    .line 84
    :try_start_1
    const-string v0, "ELMServices(): Activation record found and license is ATVIVE state."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 85
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 86
    new-instance v0, Lcom/samsung/klmsagent/sender/SendIntentToELM;

    invoke-direct {v0}, Lcom/samsung/klmsagent/sender/SendIntentToELM;-><init>()V

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    const/16 v3, 0x5a

    invoke-virtual {v0, v1, p0, v2, v3}, Lcom/samsung/klmsagent/sender/SendIntentToELM;->SendIntent(Landroid/content/Context;IZI)V

    .line 112
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "success":Z
    :goto_3
    invoke-virtual {v7}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_2

    .line 89
    .restart local v11    # "i$":Ljava/util/Iterator;
    .restart local v13    # "success":Z
    :cond_4
    new-instance v0, Lcom/samsung/klmsagent/sender/SendIntentToELM;

    invoke-direct {v0}, Lcom/samsung/klmsagent/sender/SendIntentToELM;-><init>()V

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x1

    const/16 v4, 0x5a

    const-string v5, "ON_PREMISE"

    move v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/klmsagent/sender/SendIntentToELM;->SendIntent(Landroid/content/Context;IZILjava/lang/String;)V

    goto :goto_3

    .line 93
    :cond_5
    const-string v0, "ELMServices(): Activation record found but license is NON ACTIVE state."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 94
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    .line 95
    new-instance v0, Lcom/samsung/klmsagent/sender/SendIntentToELM;

    invoke-direct {v0}, Lcom/samsung/klmsagent/sender/SendIntentToELM;-><init>()V

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x32

    invoke-virtual {v0, v1, p0, v2, v3}, Lcom/samsung/klmsagent/sender/SendIntentToELM;->SendIntent(Landroid/content/Context;IZI)V

    goto :goto_3

    .line 98
    :cond_6
    new-instance v0, Lcom/samsung/klmsagent/sender/SendIntentToELM;

    invoke-direct {v0}, Lcom/samsung/klmsagent/sender/SendIntentToELM;-><init>()V

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x1

    const/16 v4, 0x32

    const-string v5, "ON_PREMISE"

    move v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/klmsagent/sender/SendIntentToELM;->SendIntent(Landroid/content/Context;IZILjava/lang/String;)V

    goto :goto_3

    .line 103
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "success":Z
    :cond_7
    const-string v0, "ELMServices(): No activation record is found when elm license check came."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 104
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    .line 105
    new-instance v0, Lcom/samsung/klmsagent/sender/SendIntentToELM;

    invoke-direct {v0}, Lcom/samsung/klmsagent/sender/SendIntentToELM;-><init>()V

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0xa

    invoke-virtual {v0, v1, p0, v2, v3}, Lcom/samsung/klmsagent/sender/SendIntentToELM;->SendIntent(Landroid/content/Context;IZI)V

    goto :goto_3

    .line 108
    :cond_8
    new-instance v0, Lcom/samsung/klmsagent/sender/SendIntentToELM;

    invoke-direct {v0}, Lcom/samsung/klmsagent/sender/SendIntentToELM;-><init>()V

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x1

    const/16 v4, 0xa

    const-string v5, "ON_PREMISE"

    move v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/klmsagent/sender/SendIntentToELM;->SendIntent(Landroid/content/Context;IZILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method


# virtual methods
.method public checkLicenseStatusFromELM(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    .line 32
    const-string v0, "ELMServices(): checkLicenseStatusFromELM()"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 34
    if-nez p1, :cond_0

    .line 35
    const-string v0, "ELMServices(): No values in intent"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 40
    :goto_0
    return-void

    .line 39
    :cond_0
    const/16 v0, 0x12c

    invoke-static {v0}, Lcom/samsung/klmsagent/services/i/ELMServices;->checkLicenseStatusToSendELM(I)V

    goto :goto_0
.end method

.method public checkPermissinForPackage(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 120
    const-string v8, "ELMServices(): checkPermissinForPackage()"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 122
    if-nez p1, :cond_0

    .line 123
    const-string v8, "ELMServices(): No values in intent"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 173
    :goto_0
    return-void

    .line 127
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 128
    .local v5, "extras":Landroid/os/Bundle;
    if-nez v5, :cond_1

    .line 129
    const-string v8, "ELMServices(): intent.getExtras() == NULL"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_1
    const-string v8, "ELM_PACKAGE_NAME"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 134
    .local v6, "pkgName":Ljava/lang/String;
    if-nez v6, :cond_2

    .line 135
    const-string v8, "ELMServices(): PackageName == NULL"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .line 139
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ELMServices(): checkPermissinForPackage().pkgName: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 140
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v3

    .line 143
    .local v3, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v8, 0x1

    :try_start_0
    new-array v7, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v6, v7, v8

    .line 144
    .local v7, "whereArgs":[Ljava/lang/String;
    const-string v8, "PACKAGE_NAME=?"

    new-instance v9, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v9}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    invoke-virtual {v3, v8, v9, v7}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 146
    .local v2, "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_5

    .line 147
    const/4 v8, 0x0

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-virtual {v8}, Lcom/samsung/klmsagent/beans/ContainerData;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 148
    .local v1, "ContainerPackage":Ljava/lang/String;
    const/4 v8, 0x0

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-virtual {v8}, Lcom/samsung/klmsagent/beans/ContainerData;->getContainerID()Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "ContainerId":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ELMServices(): Container info : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 150
    const/4 v8, 0x0

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-virtual {v8}, Lcom/samsung/klmsagent/beans/ContainerData;->getContainerStatus()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const/4 v9, 0x4

    if-eq v8, v9, :cond_4

    .line 151
    invoke-static {v1}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->checkContainerPermission(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 152
    const-string v8, "ELMServices(): Send intent to Container to UNLOCK."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 153
    const/16 v8, 0x5a

    invoke-static {v8, v0}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V

    .line 154
    const/4 v8, 0x0

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/klmsagent/beans/ContainerData;

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerStatus(Ljava/lang/Integer;)V

    .line 155
    const/4 v8, 0x0

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-virtual {v8, v1}, Lcom/samsung/klmsagent/beans/ContainerData;->setPackageName(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    .end local v0    # "ContainerId":Ljava/lang/String;
    .end local v1    # "ContainerPackage":Ljava/lang/String;
    .end local v2    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .end local v7    # "whereArgs":[Ljava/lang/String;
    :goto_1
    invoke-virtual {v3}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_0

    .line 157
    .restart local v0    # "ContainerId":Ljava/lang/String;
    .restart local v1    # "ContainerPackage":Ljava/lang/String;
    .restart local v2    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .restart local v7    # "whereArgs":[Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v8, "ELMServices(): Send intent to Container to LOCK."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 158
    const/16 v8, 0x32

    invoke-static {v8, v0}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V

    .line 159
    const/4 v8, 0x0

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/klmsagent/beans/ContainerData;

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerStatus(Ljava/lang/Integer;)V

    .line 160
    const/4 v8, 0x0

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-virtual {v8, v1}, Lcom/samsung/klmsagent/beans/ContainerData;->setPackageName(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 168
    .end local v0    # "ContainerId":Ljava/lang/String;
    .end local v1    # "ContainerPackage":Ljava/lang/String;
    .end local v2    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .end local v7    # "whereArgs":[Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 169
    .local v4, "e":Ljava/lang/Exception;
    const-string v8, "ELMServices():  Exception found"

    invoke-static {v8, v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 163
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v0    # "ContainerId":Ljava/lang/String;
    .restart local v1    # "ContainerPackage":Ljava/lang/String;
    .restart local v2    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .restart local v7    # "whereArgs":[Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string v8, "ELMServices(): Container status is explicit locked."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_1

    .line 166
    .end local v0    # "ContainerId":Ljava/lang/String;
    .end local v1    # "ContainerPackage":Ljava/lang/String;
    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ELMServices(): Can\'t find pkgName in Container DB: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method
