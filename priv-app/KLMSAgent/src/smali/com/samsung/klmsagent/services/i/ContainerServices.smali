.class public Lcom/samsung/klmsagent/services/i/ContainerServices;
.super Lcom/samsung/klmsagent/services/KLMSServices;
.source "ContainerServices.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/klmsagent/services/KLMSServices",
        "<",
        "Lcom/samsung/klmsagent/beans/ContainerData;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "B2B-ContainerServices(): "

.field private static source:Lcom/samsung/klmsagent/data/DataSource;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/samsung/klmsagent/services/KLMSServices;-><init>()V

    .line 32
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    sput-object v0, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    .line 33
    return-void
.end method

.method public static postProcessResponse(Ljava/lang/String;Landroid/os/Message;)V
    .locals 12
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "reqObj"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 119
    const-string v7, "B2B-ContainerServices(): postProcessResponse().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 122
    if-eqz p0, :cond_0

    :try_start_0
    const-string v7, ""

    invoke-virtual {v7, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 123
    :cond_0
    const-string v7, "B2B-ContainerServices(): postProcessResponse() : Result from Server is null."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 124
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "REQUEST_DB_ID"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_1

    .line 125
    const-string v7, "B2B-ContainerServices(): This is a new request, needs to be saved in the db"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 126
    new-instance v4, Lcom/samsung/klmsagent/beans/RequestLog;

    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v4, v7, v8, v10, v11}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>(Ljava/lang/String;IJ)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    .local v4, "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :try_start_1
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/samsung/klmsagent/data/DataSource;->saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199
    .end local v4    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :cond_1
    :goto_0
    sget-object v7, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v7}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 201
    :goto_1
    return-void

    .line 129
    .restart local v4    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :catch_0
    move-exception v3

    .line 130
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v7, "Error in saving the data for the request log."

    invoke-static {v7, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 194
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :catch_1
    move-exception v3

    .line 195
    .local v3, "e":Lorg/json/JSONException;
    :try_start_3
    const-string v7, "common unkown exception."

    invoke-static {v7, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 199
    sget-object v7, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v7}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_1

    .line 135
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_2
    :try_start_4
    invoke-static {p0, p1}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->checkTransactionId(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 136
    const-string v7, "B2B-ContainerServices(): Container Enrollment failed due to incorrect signature"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 199
    sget-object v7, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v7}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_1

    .line 140
    :cond_3
    :try_start_5
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 141
    .local v6, "response":Lorg/json/JSONObject;
    const-string v7, "payLoad"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_4

    .line 142
    const-string v7, "B2B-ContainerServices(): postProcessResponse() : Payload from Server is null."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 199
    sget-object v7, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v7}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_1

    .line 146
    :cond_4
    :try_start_6
    const-string v7, "payLoad"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/json/JSONObject;

    .line 147
    .local v5, "payload":Lorg/json/JSONObject;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2B-ContainerServices(): Response code in enroll container:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "responseCode"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 149
    const-string v7, "responseCode"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    const/16 v8, 0xc35

    if-eq v7, v8, :cond_5

    const-string v7, "responseCode"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    const/16 v8, 0x3e8

    if-ne v7, v8, :cond_a

    .line 151
    :cond_5
    const-string v7, "B2B-ContainerServices(): Container registered. update/save data based on conatiner/mdm call."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 153
    new-instance v1, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v1}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    .line 154
    .local v1, "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    const-string v7, "containerId"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerID(Ljava/lang/String;)V

    .line 155
    const-string v7, "containerTrackerId"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/klmsagent/beans/ContainerData;->setTrackerID(Ljava/lang/String;)V

    .line 156
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/klmsagent/beans/ContainerData;->setTimestamp(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "packageName"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/klmsagent/beans/ContainerData;->setPackageName(Ljava/lang/String;)V

    .line 158
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerStatus(Ljava/lang/Integer;)V

    .line 160
    sget-object v7, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    const/4 v8, 0x0

    new-instance v9, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v9}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    invoke-virtual {v7, v8, v9}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 162
    .local v2, "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    if-eqz v2, :cond_6

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 163
    :cond_6
    sget-object v7, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v7, v1}, Lcom/samsung/klmsagent/data/DataSource;->saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z

    .line 164
    const-string v7, "B2B-ContainerServices(): Success to Save TrackID2 in DB"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 171
    :goto_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "REQUEST_DB_ID"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "_id":Ljava/lang/String;
    if-eqz v0, :cond_7

    .line 173
    new-instance v4, Lcom/samsung/klmsagent/beans/RequestLog;

    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v4, v7, v8, v10, v11}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>(Ljava/lang/String;IJ)V

    .line 174
    .restart local v4    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    invoke-virtual {v4, v0}, Lcom/samsung/klmsagent/beans/RequestLog;->setId(Ljava/lang/String;)V

    .line 175
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2B-ContainerServices(): Cleaning the log DB. Log to be deleted is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 176
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2B-ContainerServices(): Cleaning the log DB. Id to be deleted is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 178
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/samsung/klmsagent/data/DataSource;->deleteData(Lcom/samsung/klmsagent/beans/KLMSComponent;)I

    .line 180
    .end local v4    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :cond_7
    const-string v7, "B2B-ContainerServices(): Succesfully Uploaded the data to server for enroll container."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 199
    sget-object v7, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v7}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_1

    .line 166
    .end local v0    # "_id":Ljava/lang/String;
    :cond_8
    :try_start_7
    sget-object v7, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    const-string v8, "CONTAINER_ID=?"

    invoke-virtual {v7, v8, v1}, Lcom/samsung/klmsagent/data/DataSource;->updateData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)I

    move-result v7

    if-nez v7, :cond_9

    .line 167
    sget-object v7, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v7, v1}, Lcom/samsung/klmsagent/data/DataSource;->saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z

    .line 168
    :cond_9
    const-string v7, "B2B-ContainerServices(): Update to TrackID2 in DB"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2

    .line 196
    .end local v1    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v2    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .end local v5    # "payload":Lorg/json/JSONObject;
    .end local v6    # "response":Lorg/json/JSONObject;
    :catch_2
    move-exception v3

    .line 197
    .local v3, "e":Ljava/lang/Exception;
    :try_start_8
    const-string v7, "common unkown exception."

    invoke-static {v7, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 199
    sget-object v7, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v7}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_1

    .line 182
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v5    # "payload":Lorg/json/JSONObject;
    .restart local v6    # "response":Lorg/json/JSONObject;
    :cond_a
    :try_start_9
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "B2B-ContainerServices(): Container enrollment not successfull:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "responseCode"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 183
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "REQUEST_DB_ID"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_b

    .line 184
    new-instance v4, Lcom/samsung/klmsagent/beans/RequestLog;

    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v4, v7, v8, v10, v11}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>(Ljava/lang/String;IJ)V
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 186
    .restart local v4    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :try_start_a
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/samsung/klmsagent/data/DataSource;->saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 199
    .end local v4    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :cond_b
    :goto_3
    sget-object v7, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v7}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_1

    .line 187
    .restart local v4    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :catch_3
    move-exception v3

    .line 188
    .restart local v3    # "e":Ljava/lang/Exception;
    :try_start_b
    const-string v7, "Error in saving the data for the request log."

    invoke-static {v7, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_3

    .line 199
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    .end local v5    # "payload":Lorg/json/JSONObject;
    .end local v6    # "response":Lorg/json/JSONObject;
    :catchall_0
    move-exception v7

    sget-object v8, Lcom/samsung/klmsagent/services/i/ContainerServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v8}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v7
.end method

.method public static postProcessResponseUninstall(Ljava/lang/String;Landroid/os/Message;)V
    .locals 13
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "reqObj"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    const/4 v12, -0x1

    .line 300
    const-string v8, "B2B-ContainerServices(): postProcessResponseUninstall().START"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 302
    if-nez p0, :cond_2

    .line 303
    :try_start_0
    const-string v8, "B2B-ContainerServices(): postProcessResponseUninstall() : Result from Server is Null."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 304
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "REQUEST_DB_ID"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_0

    .line 305
    new-instance v2, Lcom/samsung/klmsagent/beans/RequestLog;

    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    iget v9, p1, Landroid/os/Message;->what:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v2, v8, v9, v10, v11}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>(Ljava/lang/String;IJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    .local v2, "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :try_start_1
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/samsung/klmsagent/data/DataSource;->saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z

    .line 308
    const-string v8, "B2B-ContainerServices(): Save the Log in the DB."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382
    .end local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :cond_0
    :goto_0
    const-string v8, "B2B-ContainerServices(): postProcessResponseUninstall().Completed. Check packagName for Daecativation."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 384
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "packageName"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 385
    .local v5, "pkgName":Ljava/lang/String;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 386
    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSUtility;->IsPackageExistInDevice(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 387
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "B2B-ContainerServices(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is not exist. deactivateLicense()"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 388
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->deleteNotificationAppList(Ljava/lang/String;)V

    .line 389
    new-instance v3, Lcom/samsung/klmsagent/services/i/DeviceServices;

    invoke-direct {v3}, Lcom/samsung/klmsagent/services/i/DeviceServices;-><init>()V

    .line 390
    .local v3, "mDeviceStateHandler":Lcom/samsung/klmsagent/services/i/DeviceServices;
    invoke-virtual {v3, v12, v5}, Lcom/samsung/klmsagent/services/i/DeviceServices;->onDestroy(ILjava/lang/String;)V

    .line 394
    .end local v3    # "mDeviceStateHandler":Lcom/samsung/klmsagent/services/i/DeviceServices;
    :cond_1
    :goto_1
    return-void

    .line 309
    .end local v5    # "pkgName":Ljava/lang/String;
    .restart local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :catch_0
    move-exception v1

    .line 310
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v8, "Error in saving the data for the request log."

    invoke-static {v8, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 379
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :catch_1
    move-exception v1

    .line 380
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_3
    const-string v8, "common unkown exception."

    invoke-static {v8, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 382
    const-string v8, "B2B-ContainerServices(): postProcessResponseUninstall().Completed. Check packagName for Daecativation."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 384
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "packageName"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 385
    .restart local v5    # "pkgName":Ljava/lang/String;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 386
    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSUtility;->IsPackageExistInDevice(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 387
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "B2B-ContainerServices(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is not exist. deactivateLicense()"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 388
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->deleteNotificationAppList(Ljava/lang/String;)V

    .line 389
    new-instance v3, Lcom/samsung/klmsagent/services/i/DeviceServices;

    invoke-direct {v3}, Lcom/samsung/klmsagent/services/i/DeviceServices;-><init>()V

    .line 390
    .restart local v3    # "mDeviceStateHandler":Lcom/samsung/klmsagent/services/i/DeviceServices;
    invoke-virtual {v3, v12, v5}, Lcom/samsung/klmsagent/services/i/DeviceServices;->onDestroy(ILjava/lang/String;)V

    goto :goto_1

    .line 316
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "mDeviceStateHandler":Lcom/samsung/klmsagent/services/i/DeviceServices;
    .end local v5    # "pkgName":Ljava/lang/String;
    :cond_2
    :try_start_4
    invoke-static {p0, p1}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->checkTransactionId(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 317
    const-string v8, "B2B-ContainerServices(): Container uninstall failed due to incorrect signature"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 382
    const-string v8, "B2B-ContainerServices(): postProcessResponseUninstall().Completed. Check packagName for Daecativation."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 384
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "packageName"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 385
    .restart local v5    # "pkgName":Ljava/lang/String;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 386
    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSUtility;->IsPackageExistInDevice(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 387
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "B2B-ContainerServices(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is not exist. deactivateLicense()"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 388
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->deleteNotificationAppList(Ljava/lang/String;)V

    .line 389
    new-instance v3, Lcom/samsung/klmsagent/services/i/DeviceServices;

    invoke-direct {v3}, Lcom/samsung/klmsagent/services/i/DeviceServices;-><init>()V

    .line 390
    .restart local v3    # "mDeviceStateHandler":Lcom/samsung/klmsagent/services/i/DeviceServices;
    invoke-virtual {v3, v12, v5}, Lcom/samsung/klmsagent/services/i/DeviceServices;->onDestroy(ILjava/lang/String;)V

    goto/16 :goto_1

    .line 321
    .end local v3    # "mDeviceStateHandler":Lcom/samsung/klmsagent/services/i/DeviceServices;
    .end local v5    # "pkgName":Ljava/lang/String;
    :cond_3
    :try_start_5
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 322
    .local v6, "response":Lorg/json/JSONObject;
    const-string v8, "payLoad"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_6

    .line 323
    const-string v8, "B2B-ContainerServices(): postProcessResponseUninstall() : payLoad from Server is Null."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 324
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "REQUEST_DB_ID"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_4

    .line 325
    new-instance v2, Lcom/samsung/klmsagent/beans/RequestLog;

    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    iget v9, p1, Landroid/os/Message;->what:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v2, v8, v9, v10, v11}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>(Ljava/lang/String;IJ)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 327
    .restart local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :try_start_6
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/samsung/klmsagent/data/DataSource;->saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z

    .line 328
    const-string v8, "B2B-ContainerServices(): Save the Log in the DB."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 382
    .end local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :cond_4
    :goto_2
    const-string v8, "B2B-ContainerServices(): postProcessResponseUninstall().Completed. Check packagName for Daecativation."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 384
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "packageName"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 385
    .restart local v5    # "pkgName":Ljava/lang/String;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 386
    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSUtility;->IsPackageExistInDevice(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 387
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "B2B-ContainerServices(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is not exist. deactivateLicense()"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 388
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->deleteNotificationAppList(Ljava/lang/String;)V

    .line 389
    new-instance v3, Lcom/samsung/klmsagent/services/i/DeviceServices;

    invoke-direct {v3}, Lcom/samsung/klmsagent/services/i/DeviceServices;-><init>()V

    .line 390
    .restart local v3    # "mDeviceStateHandler":Lcom/samsung/klmsagent/services/i/DeviceServices;
    invoke-virtual {v3, v12, v5}, Lcom/samsung/klmsagent/services/i/DeviceServices;->onDestroy(ILjava/lang/String;)V

    goto/16 :goto_1

    .line 329
    .end local v3    # "mDeviceStateHandler":Lcom/samsung/klmsagent/services/i/DeviceServices;
    .end local v5    # "pkgName":Ljava/lang/String;
    .restart local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :catch_2
    move-exception v1

    .line 330
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_7
    const-string v8, "Error in saving the data for the request log."

    invoke-static {v8, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2

    .line 382
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    .end local v6    # "response":Lorg/json/JSONObject;
    :catchall_0
    move-exception v8

    const-string v9, "B2B-ContainerServices(): postProcessResponseUninstall().Completed. Check packagName for Daecativation."

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 384
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "packageName"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 385
    .restart local v5    # "pkgName":Ljava/lang/String;
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_5

    .line 386
    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSUtility;->IsPackageExistInDevice(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 387
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "B2B-ContainerServices(): "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is not exist. deactivateLicense()"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 388
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->deleteNotificationAppList(Ljava/lang/String;)V

    .line 389
    new-instance v3, Lcom/samsung/klmsagent/services/i/DeviceServices;

    invoke-direct {v3}, Lcom/samsung/klmsagent/services/i/DeviceServices;-><init>()V

    .line 390
    .restart local v3    # "mDeviceStateHandler":Lcom/samsung/klmsagent/services/i/DeviceServices;
    invoke-virtual {v3, v12, v5}, Lcom/samsung/klmsagent/services/i/DeviceServices;->onDestroy(ILjava/lang/String;)V

    .line 393
    .end local v3    # "mDeviceStateHandler":Lcom/samsung/klmsagent/services/i/DeviceServices;
    :cond_5
    throw v8

    .line 335
    .end local v5    # "pkgName":Ljava/lang/String;
    .restart local v6    # "response":Lorg/json/JSONObject;
    :cond_6
    :try_start_8
    const-string v8, "payLoad"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONObject;

    .line 336
    .local v4, "payload":Lorg/json/JSONObject;
    const-string v8, "responseCode"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 337
    .local v7, "responseCode":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "B2B-ContainerServices(): postProcessResponseUninstall() : responseCode : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 338
    const/4 v0, 0x0

    .line 339
    .local v0, "_id":Ljava/lang/String;
    sparse-switch v7, :sswitch_data_0

    .line 376
    const-string v8, "B2B-ContainerServices(): Error code is not mapped."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 382
    :cond_7
    :goto_3
    const-string v8, "B2B-ContainerServices(): postProcessResponseUninstall().Completed. Check packagName for Daecativation."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 384
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "packageName"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 385
    .restart local v5    # "pkgName":Ljava/lang/String;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 386
    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSUtility;->IsPackageExistInDevice(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 387
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "B2B-ContainerServices(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is not exist. deactivateLicense()"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 388
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->deleteNotificationAppList(Ljava/lang/String;)V

    .line 389
    new-instance v3, Lcom/samsung/klmsagent/services/i/DeviceServices;

    invoke-direct {v3}, Lcom/samsung/klmsagent/services/i/DeviceServices;-><init>()V

    .line 390
    .restart local v3    # "mDeviceStateHandler":Lcom/samsung/klmsagent/services/i/DeviceServices;
    invoke-virtual {v3, v12, v5}, Lcom/samsung/klmsagent/services/i/DeviceServices;->onDestroy(ILjava/lang/String;)V

    goto/16 :goto_1

    .line 341
    .end local v3    # "mDeviceStateHandler":Lcom/samsung/klmsagent/services/i/DeviceServices;
    .end local v5    # "pkgName":Ljava/lang/String;
    :sswitch_0
    :try_start_9
    const-string v8, "B2B-ContainerServices(): Container uninsatll Success. Deleting all DB."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 342
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "REQUEST_DB_ID"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 343
    if-eqz v0, :cond_8

    .line 344
    new-instance v2, Lcom/samsung/klmsagent/beans/RequestLog;

    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    iget v9, p1, Landroid/os/Message;->what:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v2, v8, v9, v10, v11}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>(Ljava/lang/String;IJ)V

    .line 345
    .restart local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    invoke-virtual {v2, v0}, Lcom/samsung/klmsagent/beans/RequestLog;->setId(Ljava/lang/String;)V

    .line 346
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "B2B-ContainerServices(): Cleaning the log DB. Log to be deleted is : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 347
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "B2B-ContainerServices(): Cleaning the log DB. ID to be deleted is : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 349
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/samsung/klmsagent/data/DataSource;->deleteData(Lcom/samsung/klmsagent/beans/KLMSComponent;)I

    .line 351
    .end local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :cond_8
    const-string v8, "B2B-ContainerServices(): Succesfully Uploaded the data to server for uninstall."

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 354
    :sswitch_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "B2B-ContainerServices(): Remove Container Failed: responseCode : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 355
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "REQUEST_DB_ID"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_7

    .line 356
    new-instance v2, Lcom/samsung/klmsagent/beans/RequestLog;

    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    iget v9, p1, Landroid/os/Message;->what:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v2, v8, v9, v10, v11}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>(Ljava/lang/String;IJ)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 358
    .restart local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :try_start_a
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/samsung/klmsagent/data/DataSource;->saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_3

    .line 359
    :catch_3
    move-exception v1

    .line 360
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_b
    const-string v8, "Error in saving the data for the request log."

    invoke-static {v8, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 366
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    :sswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "REQUEST_DB_ID"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 367
    if-eqz v0, :cond_7

    .line 368
    new-instance v2, Lcom/samsung/klmsagent/beans/RequestLog;

    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    iget v9, p1, Landroid/os/Message;->what:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v2, v8, v9, v10, v11}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>(Ljava/lang/String;IJ)V

    .line 369
    .restart local v2    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    invoke-virtual {v2, v0}, Lcom/samsung/klmsagent/beans/RequestLog;->setId(Ljava/lang/String;)V

    .line 370
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "B2B-ContainerServices(): Cleaning the log DB. Log to be deleted is : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 372
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/samsung/klmsagent/data/DataSource;->deleteData(Lcom/samsung/klmsagent/beans/KLMSComponent;)I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_3

    .line 339
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_0
        0x3ed -> :sswitch_1
        0xc33 -> :sswitch_2
        0xc59 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public isAlreadyRegisterd(Lcom/samsung/klmsagent/beans/ContainerData;)Z
    .locals 1
    .param p1, "obj"    # Lcom/samsung/klmsagent/beans/ContainerData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 405
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic isAlreadyRegisterd(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z
    .locals 1
    .param p1, "x0"    # Lcom/samsung/klmsagent/beans/KLMSComponent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 26
    check-cast p1, Lcom/samsung/klmsagent/beans/ContainerData;

    .end local p1    # "x0":Lcom/samsung/klmsagent/beans/KLMSComponent;
    invoke-virtual {p0, p1}, Lcom/samsung/klmsagent/services/i/ContainerServices;->isAlreadyRegisterd(Lcom/samsung/klmsagent/beans/ContainerData;)Z

    move-result v0

    return v0
.end method

.method public onDestroy(ILjava/lang/String;)V
    .locals 24
    .param p1, "id"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 205
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "B2B-ContainerServices(): onDestroy().START - Container ID: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ""

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 206
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v8

    .line 209
    .local v8, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    :try_start_0
    new-instance v6, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v6}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    .line 210
    .local v6, "containerDataObj":Lcom/samsung/klmsagent/beans/ContainerData;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ""

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerID(Ljava/lang/String;)V

    .line 211
    const/4 v14, 0x0

    .line 214
    .local v14, "onPremState":I
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object p2, v20, v21

    .line 216
    .local v20, "whereArgs1":[Ljava/lang/String;
    new-instance v4, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v4}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 218
    .local v4, "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setPackageName(Ljava/lang/String;)V

    .line 220
    const-string v21, "PACKAGE_NAME=?"

    const-string v22, "ASC"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-virtual {v8, v0, v4, v1, v2}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 223
    .local v10, "deviceDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v10, :cond_0

    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_0

    .line 224
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 225
    .local v9, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v9}, Lcom/samsung/klmsagent/beans/DeviceData;->getOnPremInfo()I

    move-result v14

    .line 227
    goto :goto_0

    .line 229
    .end local v9    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v12    # "i$":Ljava/util/Iterator;
    :cond_0
    const-string v21, "CONTAINER_ID=?"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0, v6}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v7

    .line 230
    .local v7, "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    if-eqz v7, :cond_6

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_6

    .line 231
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .restart local v12    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/klmsagent/beans/ContainerData;

    .line 232
    .local v5, "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    invoke-virtual {v5}, Lcom/samsung/klmsagent/beans/ContainerData;->getContainerID()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 233
    new-instance v13, Landroid/os/Message;

    invoke-direct {v13}, Landroid/os/Message;-><init>()V

    .line 234
    .local v13, "msg":Landroid/os/Message;
    new-instance v16, Lorg/json/JSONObject;

    invoke-direct/range {v16 .. v16}, Lorg/json/JSONObject;-><init>()V

    .line 235
    .local v16, "request":Lorg/json/JSONObject;
    new-instance v15, Lorg/json/JSONObject;

    invoke-direct {v15}, Lorg/json/JSONObject;-><init>()V

    .line 237
    .local v15, "payLoad":Lorg/json/JSONObject;
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSUtility;->getTransactionId()Ljava/lang/String;

    move-result-object v17

    .line 239
    .local v17, "signature":Ljava/lang/String;
    const-string v21, "level"

    const/16 v22, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v15, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 240
    const-string v21, "trackerId"

    invoke-virtual {v5}, Lcom/samsung/klmsagent/beans/ContainerData;->getTrackerID()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v15, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 241
    const-string v21, "id"

    invoke-virtual {v5}, Lcom/samsung/klmsagent/beans/ContainerData;->getContainerID()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v15, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 242
    const-string v21, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-object/from16 v0, v21

    move-wide/from16 v1, v22

    invoke-virtual {v15, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 244
    const-string v21, "payLoad"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 245
    const-string v21, "signature"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 247
    move-object/from16 v0, v16

    iput-object v0, v13, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 248
    sget-object v21, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_UNINSTALL:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v21

    move/from16 v0, v21

    iput v0, v13, Landroid/os/Message;->what:I

    .line 250
    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v14, v0, :cond_4

    .line 251
    const-string v21, "B2B-ContainerServices(): onDestroy().ON_PREMISE"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 252
    invoke-virtual {v13}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    const-string v22, "onPrem"

    const/16 v23, 0x1

    invoke-virtual/range {v21 .. v23}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 257
    :goto_2
    if-eqz p2, :cond_2

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_2

    .line 258
    invoke-virtual {v13}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    const-string v22, "packageName"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_2
    invoke-virtual {v13}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    const-string v22, "transaction_id"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string v21, "uninstallContainer"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSUtility;->checkRightsToCallUrl(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_5

    .line 263
    const-string v21, "B2B-ContainerServices(): Not authorized to call this url."

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 268
    :goto_3
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v21, 0x0

    invoke-virtual {v5}, Lcom/samsung/klmsagent/beans/ContainerData;->getContainerID()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v19, v21

    .line 270
    .local v19, "whereArgs":[Ljava/lang/String;
    const-string v21, "containerinfo"

    const-string v22, "CONTAINER_ID=?"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v19

    invoke-virtual {v8, v0, v1, v2}, Lcom/samsung/klmsagent/data/DataSource;->deleteTables(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v21

    if-eqz v21, :cond_3

    .line 271
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "B2B-ContainerServices(): Success Deleted Container ID: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v5}, Lcom/samsung/klmsagent/beans/ContainerData;->getContainerID()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 273
    :cond_3
    invoke-virtual {v8}, Lcom/samsung/klmsagent/data/DataSource;->close()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 279
    .end local v4    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v5    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v6    # "containerDataObj":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v7    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .end local v10    # "deviceDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "msg":Landroid/os/Message;
    .end local v14    # "onPremState":I
    .end local v15    # "payLoad":Lorg/json/JSONObject;
    .end local v16    # "request":Lorg/json/JSONObject;
    .end local v17    # "signature":Ljava/lang/String;
    .end local v19    # "whereArgs":[Ljava/lang/String;
    .end local v20    # "whereArgs1":[Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 280
    .local v11, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v21, "Json error."

    move-object/from16 v0, v21

    invoke-static {v0, v11}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 284
    const-string v21, "B2B-ContainerServices(): onDestroy().execution completed."

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 285
    invoke-virtual {v8}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 287
    .end local v11    # "e":Lorg/json/JSONException;
    :goto_4
    return-void

    .line 254
    .restart local v4    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v5    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .restart local v6    # "containerDataObj":Lcom/samsung/klmsagent/beans/ContainerData;
    .restart local v7    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .restart local v10    # "deviceDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v13    # "msg":Landroid/os/Message;
    .restart local v14    # "onPremState":I
    .restart local v15    # "payLoad":Lorg/json/JSONObject;
    .restart local v16    # "request":Lorg/json/JSONObject;
    .restart local v17    # "signature":Ljava/lang/String;
    .restart local v20    # "whereArgs1":[Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string v21, "B2B-ContainerServices(): onDestroy().NORMAL"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 255
    invoke-virtual {v13}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    const-string v22, "onPrem"

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 281
    .end local v4    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v5    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v6    # "containerDataObj":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v7    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .end local v10    # "deviceDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "msg":Landroid/os/Message;
    .end local v14    # "onPremState":I
    .end local v15    # "payLoad":Lorg/json/JSONObject;
    .end local v16    # "request":Lorg/json/JSONObject;
    .end local v17    # "signature":Ljava/lang/String;
    .end local v20    # "whereArgs1":[Ljava/lang/String;
    :catch_1
    move-exception v11

    .line 282
    .local v11, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v21, "Exception."

    move-object/from16 v0, v21

    invoke-static {v0, v11}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 284
    const-string v21, "B2B-ContainerServices(): onDestroy().execution completed."

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 285
    invoke-virtual {v8}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_4

    .line 265
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v4    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v5    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .restart local v6    # "containerDataObj":Lcom/samsung/klmsagent/beans/ContainerData;
    .restart local v7    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .restart local v10    # "deviceDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v13    # "msg":Landroid/os/Message;
    .restart local v14    # "onPremState":I
    .restart local v15    # "payLoad":Lorg/json/JSONObject;
    .restart local v16    # "request":Lorg/json/JSONObject;
    .restart local v17    # "signature":Ljava/lang/String;
    .restart local v20    # "whereArgs1":[Ljava/lang/String;
    :cond_5
    :try_start_4
    new-instance v18, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    move-object/from16 v0, v18

    invoke-direct {v0, v13}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 266
    .local v18, "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/klm-rest/v3/container/uninstall.do"

    aput-object v23, v21, v22

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_3

    .line 284
    .end local v4    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v5    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v6    # "containerDataObj":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v7    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .end local v10    # "deviceDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "msg":Landroid/os/Message;
    .end local v14    # "onPremState":I
    .end local v15    # "payLoad":Lorg/json/JSONObject;
    .end local v16    # "request":Lorg/json/JSONObject;
    .end local v17    # "signature":Ljava/lang/String;
    .end local v18    # "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    .end local v20    # "whereArgs1":[Ljava/lang/String;
    :catchall_0
    move-exception v21

    const-string v22, "B2B-ContainerServices(): onDestroy().execution completed."

    invoke-static/range {v22 .. v22}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 285
    invoke-virtual {v8}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v21

    .line 277
    .restart local v4    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v6    # "containerDataObj":Lcom/samsung/klmsagent/beans/ContainerData;
    .restart local v7    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .restart local v10    # "deviceDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v14    # "onPremState":I
    .restart local v20    # "whereArgs1":[Ljava/lang/String;
    :cond_6
    :try_start_5
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "B2B-ContainerServices(): Not able to find data for Container ID:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "  hence could not delete"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 284
    :cond_7
    const-string v21, "B2B-ContainerServices(): onDestroy().execution completed."

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 285
    invoke-virtual {v8}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_4
.end method

.method public validateRegister(Lcom/samsung/klmsagent/beans/ContainerData;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V
    .locals 22
    .param p1, "obj"    # Lcom/samsung/klmsagent/beans/ContainerData;
    .param p2, "var"    # Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    .param p3, "source"    # Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 38
    const-string v18, "B2B-ContainerServices(): validateRegister().START"

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 40
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v5

    .line 43
    .local v5, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    :try_start_0
    new-instance v10, Landroid/os/Message;

    invoke-direct {v10}, Landroid/os/Message;-><init>()V

    .line 46
    .local v10, "msg":Landroid/os/Message;
    const-string v18, "enrollContainer"

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSUtility;->checkRightsToCallUrl(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 47
    const-string v18, "B2B-ContainerServices(): No authorized to call this url."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    const-string v18, "B2B-ContainerServices(): ContainerComponentRegistration.register(): execution completed."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v5}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 109
    .end local v10    # "msg":Landroid/os/Message;
    :goto_0
    return-void

    .line 51
    .restart local v10    # "msg":Landroid/os/Message;
    :cond_0
    :try_start_1
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V

    .line 52
    .local v14, "request":Lorg/json/JSONObject;
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 55
    .local v12, "payLoad":Lorg/json/JSONObject;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/klmsagent/beans/ContainerData;->getPackageName()Ljava/lang/String;

    move-result-object v13

    .line 56
    .local v13, "pkgName":Ljava/lang/String;
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v13, v17, v18

    .line 57
    .local v17, "whereArgs":[Ljava/lang/String;
    new-instance v4, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v4}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 58
    .local v4, "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v4, v13}, Lcom/samsung/klmsagent/beans/DeviceData;->setPackageName(Ljava/lang/String;)V

    .line 60
    const-string v18, "PACKAGE_NAME=?"

    const-string v19, "ASC"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-virtual {v5, v0, v4, v1, v2}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 62
    .local v7, "deviceDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    const/4 v11, 0x0

    .line 63
    .local v11, "onPremState":I
    if-eqz v7, :cond_4

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_4

    .line 64
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 65
    .local v6, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v6}, Lcom/samsung/klmsagent/beans/DeviceData;->getOnPremInfo()I

    move-result v11

    .line 66
    const-string v18, "deviceTrackId"

    invoke-virtual {v6}, Lcom/samsung/klmsagent/beans/DeviceData;->getTrackerId()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 103
    .end local v4    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v6    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v7    # "deviceDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "msg":Landroid/os/Message;
    .end local v11    # "onPremState":I
    .end local v12    # "payLoad":Lorg/json/JSONObject;
    .end local v13    # "pkgName":Ljava/lang/String;
    .end local v14    # "request":Lorg/json/JSONObject;
    .end local v17    # "whereArgs":[Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 104
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v18, "ContainerComponentRegistration.register(): Exception"

    move-object/from16 v0, v18

    invoke-static {v0, v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 106
    const-string v18, "B2B-ContainerServices(): ContainerComponentRegistration.register(): execution completed."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v5}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_0

    .line 69
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v4    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v7    # "deviceDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v10    # "msg":Landroid/os/Message;
    .restart local v11    # "onPremState":I
    .restart local v12    # "payLoad":Lorg/json/JSONObject;
    .restart local v13    # "pkgName":Ljava/lang/String;
    .restart local v14    # "request":Lorg/json/JSONObject;
    .restart local v17    # "whereArgs":[Ljava/lang/String;
    :cond_1
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v11, v0, :cond_2

    .line 70
    :try_start_3
    invoke-static {}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->checkIfOnPrem()Z

    move-result v18

    if-eqz v18, :cond_2

    .line 71
    const-string v18, "Cannot enroll Container api return. As on premise activation is present"

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 106
    const-string v18, "B2B-ContainerServices(): ContainerComponentRegistration.register(): execution completed."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v5}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_0

    .line 75
    :cond_2
    :try_start_4
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSUtility;->getTransactionId()Ljava/lang/String;

    move-result-object v15

    .line 77
    .local v15, "signature":Ljava/lang/String;
    const-string v18, "containerId"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/klmsagent/beans/ContainerData;->getContainerID()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 78
    const-string v18, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v12, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 80
    const-string v18, "payLoad"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 81
    const-string v18, "signature"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 83
    iput-object v14, v10, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 84
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v18

    move/from16 v0, v18

    iput v0, v10, Landroid/os/Message;->what:I

    .line 85
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->ordinal()I

    move-result v18

    move/from16 v0, v18

    iput v0, v10, Landroid/os/Message;->arg1:I

    .line 87
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v11, v0, :cond_3

    .line 88
    const-string v18, "B2B-ContainerServices(): validateRegister().ON_PREMISE"

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v10}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v18

    const-string v19, "onPrem"

    const/16 v20, 0x1

    invoke-virtual/range {v18 .. v20}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 94
    :goto_2
    invoke-virtual {v10}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v18

    const-string v19, "packageName"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/klmsagent/beans/ContainerData;->getPackageName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-virtual {v10}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v18

    const-string v19, "transaction_id"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    new-instance v16, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    move-object/from16 v0, v16

    invoke-direct {v0, v10}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 98
    .local v16, "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "/klm-rest/v3/container/install.do"

    aput-object v20, v18, v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 106
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v15    # "signature":Ljava/lang/String;
    .end local v16    # "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    :goto_3
    const-string v18, "B2B-ContainerServices(): ContainerComponentRegistration.register(): execution completed."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v5}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_0

    .line 91
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v15    # "signature":Ljava/lang/String;
    :cond_3
    :try_start_5
    const-string v18, "B2B-ContainerServices(): validateRegister().NORMAL"

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v10}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v18

    const-string v19, "onPrem"

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 106
    .end local v4    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v7    # "deviceDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "msg":Landroid/os/Message;
    .end local v11    # "onPremState":I
    .end local v12    # "payLoad":Lorg/json/JSONObject;
    .end local v13    # "pkgName":Ljava/lang/String;
    .end local v14    # "request":Lorg/json/JSONObject;
    .end local v15    # "signature":Ljava/lang/String;
    .end local v17    # "whereArgs":[Ljava/lang/String;
    :catchall_0
    move-exception v18

    const-string v19, "B2B-ContainerServices(): ContainerComponentRegistration.register(): execution completed."

    invoke-static/range {v19 .. v19}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v5}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v18

    .line 101
    .restart local v4    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v7    # "deviceDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v10    # "msg":Landroid/os/Message;
    .restart local v11    # "onPremState":I
    .restart local v12    # "payLoad":Lorg/json/JSONObject;
    .restart local v13    # "pkgName":Ljava/lang/String;
    .restart local v14    # "request":Lorg/json/JSONObject;
    .restart local v17    # "whereArgs":[Ljava/lang/String;
    :cond_4
    :try_start_6
    const-string v18, "B2B-ContainerServices(): No device data found, cannot enroll container"

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3
.end method

.method public bridge synthetic validateRegister(Lcom/samsung/klmsagent/beans/KLMSComponent;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/klmsagent/beans/KLMSComponent;
    .param p2, "x1"    # Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    .param p3, "x2"    # Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 26
    check-cast p1, Lcom/samsung/klmsagent/beans/ContainerData;

    .end local p1    # "x0":Lcom/samsung/klmsagent/beans/KLMSComponent;
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/klmsagent/services/i/ContainerServices;->validateRegister(Lcom/samsung/klmsagent/beans/ContainerData;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V

    return-void
.end method

.method public validateTrackKey(Lcom/samsung/klmsagent/beans/ContainerData;II)V
    .locals 0
    .param p1, "obj"    # Lcom/samsung/klmsagent/beans/ContainerData;
    .param p2, "licenseStatus"    # I
    .param p3, "activationId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 400
    return-void
.end method

.method public bridge synthetic validateTrackKey(Lcom/samsung/klmsagent/beans/KLMSComponent;II)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/klmsagent/beans/KLMSComponent;
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 26
    check-cast p1, Lcom/samsung/klmsagent/beans/ContainerData;

    .end local p1    # "x0":Lcom/samsung/klmsagent/beans/KLMSComponent;
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/klmsagent/services/i/ContainerServices;->validateTrackKey(Lcom/samsung/klmsagent/beans/ContainerData;II)V

    return-void
.end method
