.class public Lcom/samsung/klmsagent/services/i/KLMSValidator;
.super Ljava/lang/Object;
.source "KLMSValidator.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "KLMSValidator(): "

.field public static final TOGGLE_RECENTS_INTENT:Ljava/lang/String; = "com.android.systemui.TOGGLE_RECENTS"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    return-void
.end method

.method public static HomeButton()Z
    .locals 15

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 410
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->isEulaBackButtonPress()Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-nez v13, :cond_0

    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->isEulaCancelButtonPress()Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-nez v13, :cond_0

    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->isEulaAccepted()Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 413
    :cond_0
    const-string v11, "KLMSValidator(): onStop() called during BACK or CANCEL or Agree button pressed"

    invoke-static {v11}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 414
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v11

    invoke-virtual {v11, v12}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setEulaBackButtonPress(Z)V

    .line 415
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v11

    invoke-virtual {v11, v12}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setEulaCancelButtonPress(Z)V

    move v11, v12

    .line 452
    .local v0, "TopActivity":Ljava/lang/String;
    .local v1, "am":Landroid/app/ActivityManager;
    .local v2, "homeApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .local v3, "homeIntent":Landroid/content/Intent;
    .local v4, "i":I
    .local v5, "i$":Ljava/util/Iterator;
    .local v7, "pm":Landroid/content/pm/PackageManager;
    .local v8, "runningList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .local v9, "systemToggleRecents":Landroid/content/Intent;
    .local v10, "toggleApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :goto_0
    return v11

    .line 418
    .end local v0    # "TopActivity":Ljava/lang/String;
    .end local v1    # "am":Landroid/app/ActivityManager;
    .end local v2    # "homeApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v3    # "homeIntent":Landroid/content/Intent;
    .end local v4    # "i":I
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "pm":Landroid/content/pm/PackageManager;
    .end local v8    # "runningList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v9    # "systemToggleRecents":Landroid/content/Intent;
    .end local v10    # "toggleApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "activity"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 420
    .restart local v1    # "am":Landroid/app/ActivityManager;
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 423
    .restart local v7    # "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v1, v11}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v8

    .line 424
    .restart local v8    # "runningList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const-string v0, ""

    .line 425
    .restart local v0    # "TopActivity":Ljava/lang/String;
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .restart local v5    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 426
    .local v6, "info":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v13, v6, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v13}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 427
    goto :goto_1

    .line 428
    .end local v6    # "info":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_2
    new-instance v13, Landroid/content/Intent;

    const-string v14, "android.intent.action.MAIN"

    invoke-direct {v13, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v14, "android.intent.category.HOME"

    invoke-virtual {v13, v14}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 429
    .restart local v3    # "homeIntent":Landroid/content/Intent;
    new-instance v9, Landroid/content/Intent;

    const-string v13, "com.android.systemui.TOGGLE_RECENTS"

    invoke-direct {v9, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 431
    .restart local v9    # "systemToggleRecents":Landroid/content/Intent;
    invoke-virtual {v7, v3, v11}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 432
    .restart local v2    # "homeApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual {v7, v9, v11}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v10

    .line 434
    .restart local v10    # "toggleApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v13

    if-ge v4, v13, :cond_4

    .line 435
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    .line 436
    .local v6, "info":Landroid/content/pm/ResolveInfo;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "KLMSValidator(): Package Name : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v14, v14, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 437
    iget-object v13, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 438
    const-string v12, "KLMSValidator(): Home button pressed."

    invoke-static {v12}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 434
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 443
    .end local v6    # "info":Landroid/content/pm/ResolveInfo;
    :cond_4
    const/4 v4, 0x0

    :goto_3
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v13

    if-ge v4, v13, :cond_6

    .line 444
    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    .line 445
    .restart local v6    # "info":Landroid/content/pm/ResolveInfo;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "KLMSValidator(): Package Name : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v14, v14, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 446
    iget-object v13, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 447
    const-string v12, "KLMSValidator(): Long pressed Home button."

    invoke-static {v12}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 443
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .end local v6    # "info":Landroid/content/pm/ResolveInfo;
    :cond_6
    move v11, v12

    .line 452
    goto/16 :goto_0
.end method

.method public static IsPackageExistInDevice(Ljava/lang/String;)Z
    .locals 8
    .param p0, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 568
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 569
    .local v0, "dataSourceCheck":Lcom/samsung/klmsagent/data/DataSource;
    new-array v3, v4, [Ljava/lang/String;

    aput-object p0, v3, v5

    .line 572
    .local v3, "whereArgs":[Ljava/lang/String;
    :try_start_0
    const-string v6, "PACKAGE_NAME=?"

    new-instance v7, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v7}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    invoke-virtual {v0, v6, v7, v3}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 574
    .local v1, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 575
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "KLMSValidator(): IsPackageExistInDevice().exsit: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 585
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .end local v1    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :goto_0
    return v4

    .line 578
    .restart local v1    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_0
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KLMSValidator(): IsPackageExistInDevice().Not Exsit: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 585
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move v4, v5

    goto :goto_0

    .line 581
    .end local v1    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :catch_0
    move-exception v2

    .line 582
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "KLMSValidator(): IsPackageExistInDevice() has Exception."

    invoke-static {v4, v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 585
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move v4, v5

    goto :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v4
.end method

.method public static SendIntentToKAP(IZ)V
    .locals 3
    .param p0, "notiTrigger"    # I
    .param p1, "licenseStatus"    # Z

    .prologue
    .line 664
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KLMSValidator(): SendIntentToKAP().licenseStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 666
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.action.knox.klms.KLMS_RP_NOTIFICATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 667
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "KAP_LICENSE_NOTIFICATION_TRIGGER"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 668
    const-string v1, "KLMS_LICENSE_STATUS"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 670
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.enterprise.knox.permission.MDM_ENTERPRISE_TIMA_NOTIFICATION"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 671
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KLMSValidator(): SendIntentToKAP().done: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 673
    return-void
.end method

.method public static SendIntentToLicenseTest(Ljava/lang/String;I)V
    .locals 3
    .param p0, "rpmode_status"    # Ljava/lang/String;
    .param p1, "rpmode_result"    # I

    .prologue
    .line 683
    const-string v1, "KLMSValidator(): SendIntentToLicenseTest()."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 684
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KLMSValidator(): rpmode_status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", rpmode_result = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 686
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.klmsagent.action.rpmode"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 687
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "RPMODE_RESULT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 688
    const-string v1, "RPMODE_STATUS"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 689
    const-string v1, "com.sec.esdk.elmagenttest"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 691
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 692
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KLMSValidator(): SendIntentToLicenseTest().done: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 693
    return-void
.end method

.method public static checkIfOnPrem()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 607
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 610
    .local v0, "dataSourceCheck":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v7, 0x0

    :try_start_0
    new-instance v8, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    invoke-virtual {v0, v7, v8}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 611
    .local v2, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_1

    .line 612
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 613
    .local v1, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->getOnPremInfo()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-ne v7, v5, :cond_0

    .line 623
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :goto_0
    return v5

    .restart local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move v5, v6

    goto :goto_0

    .line 619
    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :catch_0
    move-exception v3

    .line 620
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v5, "KLMSValidator(): checkIfOnPrem() has Exception."

    invoke-static {v5, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move v5, v6

    goto :goto_0

    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v5
.end method

.method public static checkQATesting()Z
    .locals 2

    .prologue
    .line 336
    const-string v1, "KLMSValidator(): checkQATesting()"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 337
    const/4 v0, 0x0

    .line 338
    .local v0, "data":Z
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->checkTestApp(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 339
    const/4 v0, 0x1

    .line 343
    :goto_0
    return v0

    .line 341
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static checkTestApp(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 353
    const/4 v1, 0x0

    .line 354
    .local v1, "info":Landroid/content/pm/PackageInfo;
    const/4 v2, 0x0

    .line 355
    .local v2, "result1":Z
    const/4 v3, 0x0

    .line 358
    .local v3, "result2":Z
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.sec.esdk.elmagenttest"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 359
    if-eqz v1, :cond_0

    .line 360
    const/4 v2, 0x1

    .line 366
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.sec.esdk.elmagenttest2"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 367
    if-eqz v1, :cond_1

    .line 368
    const/4 v3, 0x1

    .line 373
    :cond_1
    :goto_1
    or-int v4, v2, v3

    return v4

    .line 361
    :catch_0
    move-exception v0

    .line 362
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0

    .line 369
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 370
    .restart local v0    # "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static checkTransactionId(Ljava/lang/String;Landroid/os/Message;)Z
    .locals 6
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 542
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "transaction_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 544
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 545
    .local v1, "jsonObject":Lorg/json/JSONObject;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "KLMSValidator(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "signature"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "transaction_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 547
    const-string v3, "signature"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "transaction_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 548
    const-string v3, "KLMSValidator(): Transaction id of KLMS Server and Agent matched"

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 549
    const/4 v2, 0x1

    .line 563
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return v2

    .line 551
    .restart local v1    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    const-string v3, "KLMSValidator(): Transaction id of KLMS Server and Agent did not match"

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 554
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 555
    .local v0, "e":Lorg/json/JSONException;
    const-string v3, "JSONException found"

    invoke-static {v3, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 557
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 558
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "KLMSValidator(): checkTransactionId() has Exception."

    invoke-static {v3, v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 562
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v3, "KLMSValidator(): No transaction id present in request."

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static convertFromPkgNameToAppName(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 590
    const/4 v2, 0x0

    .line 592
    .local v2, "appName":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 595
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const/4 v5, 0x0

    invoke-virtual {v4, p0, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 596
    .local v1, "ai":Landroid/content/pm/ApplicationInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v4, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v5

    :goto_0
    check-cast v5, Ljava/lang/String;

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 597
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KLMSValidator(): convertFromPkgNameToAppName: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " >> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object p0, v2

    .line 602
    .end local v1    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    .end local p0    # "pkgName":Ljava/lang/String;
    :goto_1
    return-object p0

    .line 596
    .restart local v1    # "ai":Landroid/content/pm/ApplicationInfo;
    .restart local v4    # "pm":Landroid/content/pm/PackageManager;
    .restart local p0    # "pkgName":Ljava/lang/String;
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 599
    .end local v1    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v3

    .line 600
    .local v3, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KLMSValidator(): convertFromPkgNameToAppName has Exception. >> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 601
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static disableAlarm(I)V
    .locals 5
    .param p0, "activationId"    # I

    .prologue
    .line 381
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "KLMSValidator(): disableAlarm().activationID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 382
    new-instance v1, Landroid/content/Intent;

    sget-object v3, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->ALARM_INTENT:Ljava/lang/String;

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 383
    .local v1, "retryIntent":Landroid/content/Intent;
    const-string v3, "_id"

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 384
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x8000000

    invoke-static {v3, p0, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 386
    .local v2, "retryPendingIntent":Landroid/app/PendingIntent;
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 387
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 388
    return-void
.end method

.method public static getAlarmTime(I)Ljava/util/Map;
    .locals 8
    .param p0, "activationId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 109
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 110
    .local v0, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 112
    .local v4, "resultMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    new-instance v1, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v1}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 113
    .local v1, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v1, p0}, Lcom/samsung/klmsagent/beans/DeviceData;->set_id(I)V

    .line 115
    const-string v5, "_id=?"

    invoke-virtual {v0, v5, v1}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 116
    .local v2, "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move-object v4, v6

    .line 133
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v4    # "resultMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    return-object v4

    .line 121
    .restart local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v4    # "resultMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v5}, Lcom/samsung/klmsagent/beans/DeviceData;->getAlarmTime()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v5}, Lcom/samsung/klmsagent/beans/DeviceData;->getAlarmTime()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 122
    :cond_2
    const-string v5, "ALARM_TIME"

    const/4 v7, 0x0

    invoke-interface {v4, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    :goto_1
    const-string v7, "NEXT_VALIDATION"

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v5}, Lcom/samsung/klmsagent/beans/DeviceData;->getNextValidation()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 129
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :catch_0
    move-exception v3

    .line 130
    .local v3, "e":Ljava/lang/Exception;
    const-string v5, "KLMSValidator(): getAlarmTime() has Exception."

    invoke-static {v5, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 132
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move-object v4, v6

    .line 133
    goto :goto_0

    .line 124
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_3
    :try_start_1
    const-string v7, "ALARM_TIME"

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v5}, Lcom/samsung/klmsagent/beans/DeviceData;->getAlarmTime()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public static getContainerStatus()I
    .locals 6

    .prologue
    const/4 v4, -0x1

    .line 316
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v1

    .line 318
    .local v1, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v3, 0x0

    :try_start_0
    new-instance v5, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v5}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    invoke-virtual {v1, v3, v5}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v0

    .line 319
    .local v0, "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v3, v4

    .line 327
    .end local v0    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    :goto_0
    return v3

    .line 322
    .restart local v0    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    :cond_1
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-virtual {v3}, Lcom/samsung/klmsagent/beans/ContainerData;->getContainerStatus()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    .line 324
    .end local v0    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    :catch_0
    move-exception v2

    .line 325
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "KLMSValidator(): getContainerStatus() has Exception."

    invoke-static {v3, v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v3, v4

    .line 327
    goto :goto_0
.end method

.method public static getLicenseStatus(I)I
    .locals 6
    .param p0, "activationId"    # I

    .prologue
    const/4 v5, 0x0

    .line 164
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 167
    .local v0, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    :try_start_0
    new-instance v1, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v1}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 168
    .local v1, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v1, p0}, Lcom/samsung/klmsagent/beans/DeviceData;->set_id(I)V

    .line 170
    const-string v4, "_id=?"

    invoke-virtual {v0, v4, v1}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 171
    .local v2, "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v4, v5

    .line 181
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :goto_0
    return v4

    .line 175
    .restart local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_1
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v4}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    goto :goto_0

    .line 177
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :catch_0
    move-exception v3

    .line 178
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "KLMSValidator(): getLicenseStatus() has Exception."

    invoke-static {v4, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 180
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move v4, v5

    .line 181
    goto :goto_0
.end method

.method public static getMDMId(I)Ljava/lang/String;
    .locals 6
    .param p0, "activationId"    # I

    .prologue
    const/4 v5, 0x0

    .line 140
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 143
    .local v0, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    :try_start_0
    new-instance v1, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v1}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 144
    .local v1, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v1, p0}, Lcom/samsung/klmsagent/beans/DeviceData;->set_id(I)V

    .line 146
    const-string v4, "_id=?"

    invoke-virtual {v0, v4, v1}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 147
    .local v2, "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move-object v4, v5

    .line 157
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :goto_0
    return-object v4

    .line 151
    .restart local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_1
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v4}, Lcom/samsung/klmsagent/beans/DeviceData;->getPackageName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 153
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :catch_0
    move-exception v3

    .line 154
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "KLMSValidator(): getMDMId() has Exception."

    invoke-static {v4, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 156
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move-object v4, v5

    .line 157
    goto :goto_0
.end method

.method public static getPackageNameUsingContainerId(I)Ljava/lang/String;
    .locals 6
    .param p0, "containerId"    # I

    .prologue
    const/4 v5, 0x0

    .line 635
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v2

    .line 638
    .local v2, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    :try_start_0
    new-instance v0, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v0}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    .line 639
    .local v0, "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerID(Ljava/lang/String;)V

    .line 641
    const-string v4, "CONTAINER_ID=?"

    invoke-virtual {v2, v4, v0}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v1

    .line 643
    .local v1, "containerDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    .line 651
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move-object v4, v5

    .line 653
    .end local v0    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v1    # "containerDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    :goto_0
    return-object v4

    .line 646
    .restart local v0    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .restart local v1    # "containerDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    :cond_1
    const/4 v4, 0x0

    :try_start_1
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-virtual {v4}, Lcom/samsung/klmsagent/beans/ContainerData;->getPackageName()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 651
    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_0

    .line 648
    .end local v0    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v1    # "containerDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    :catch_0
    move-exception v3

    .line 649
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "KLMSValidator(): getPackageNameUsingContainerId() has Exception."

    invoke-static {v4, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 651
    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move-object v4, v5

    .line 653
    goto :goto_0

    .line 651
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v4
.end method

.method public static getTrackerId1(I)Ljava/lang/String;
    .locals 6
    .param p0, "activationId"    # I

    .prologue
    const/4 v5, 0x0

    .line 49
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 52
    .local v0, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    :try_start_0
    new-instance v1, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v1}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 53
    .local v1, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v1, p0}, Lcom/samsung/klmsagent/beans/DeviceData;->set_id(I)V

    .line 55
    const-string v4, "_id=?"

    invoke-virtual {v0, v4, v1}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 56
    .local v2, "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move-object v4, v5

    .line 65
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :goto_0
    return-object v4

    .line 59
    .restart local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_1
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v4}, Lcom/samsung/klmsagent/beans/DeviceData;->getTrackerId()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 61
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :catch_0
    move-exception v3

    .line 62
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "KLMSValidator(): getTrackerId1(activationId) has Exception."

    invoke-static {v4, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 64
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move-object v4, v5

    .line 65
    goto :goto_0
.end method

.method public static getTrackerId1(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 69
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 71
    .local v0, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v4, 0x1

    new-array v3, v4, [Ljava/lang/String;

    aput-object p0, v3, v6

    .line 74
    .local v3, "whereArgs":[Ljava/lang/String;
    :try_start_0
    const-string v4, "PACKAGE_NAME=?"

    new-instance v6, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v6}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    invoke-virtual {v0, v4, v6, v3}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 75
    .local v1, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move-object v4, v5

    .line 84
    .end local v1    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :goto_0
    return-object v4

    .line 78
    .restart local v1    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_1
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v4}, Lcom/samsung/klmsagent/beans/DeviceData;->getTrackerId()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 80
    .end local v1    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :catch_0
    move-exception v2

    .line 81
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "KLMSValidator(): getTrackerId1(packageName) has Exception."

    invoke-static {v4, v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 83
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move-object v4, v5

    .line 84
    goto :goto_0
.end method

.method public static getTrackerId2()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 91
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v1

    .line 93
    .local v1, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v3, 0x0

    :try_start_0
    new-instance v5, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v5}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    invoke-virtual {v1, v3, v5}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v0

    .line 94
    .local v0, "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v3, v4

    .line 102
    .end local v0    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    :goto_0
    return-object v3

    .line 97
    .restart local v0    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    :cond_1
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-virtual {v3}, Lcom/samsung/klmsagent/beans/ContainerData;->getTrackerID()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 99
    .end local v0    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    :catch_0
    move-exception v2

    .line 100
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "KLMSValidator(): getTrackerId2() has Exception."

    invoke-static {v3, v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v3, v4

    .line 102
    goto :goto_0
.end method

.method public static isLicenseStatus(I)Z
    .locals 1
    .param p0, "licenstStatus"    # I

    .prologue
    const/4 v0, 0x1

    .line 244
    if-ne p0, v0, :cond_0

    .line 247
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isRunning(Landroid/content/Context;)Z
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 392
    const-string v4, "activity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 393
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const v4, 0x7fffffff

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 395
    .local v3, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 396
    .local v2, "task":Landroid/app/ActivityManager$RunningTaskInfo;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v2, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 397
    iget-object v4, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.samsung.klmsagent/com.samsung.klmsagent.activities.InputLicenseKeyV2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 399
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "KLMSValidator(): Top Activity="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 400
    const/4 v4, 0x1

    .line 405
    .end local v2    # "task":Landroid/app/ActivityManager$RunningTaskInfo;
    :goto_0
    return v4

    .line 404
    :cond_1
    const-string v4, "KLMSValidator(): Top activity is not input license key"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 405
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static processError(IIIILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "http_err"    # I
    .param p1, "code"    # I
    .param p2, "initiator"    # I
    .param p3, "result_type"    # I
    .param p4, "msg"    # Ljava/lang/String;
    .param p5, "packageName"    # Ljava/lang/String;

    .prologue
    .line 466
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KLMSValidator(): processError(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 470
    invoke-static {p0, p1}, Lcom/samsung/klmsagent/util/ErrorMapper;->getErrorDescription(II)Landroid/app/enterprise/license/Error;

    move-result-object v0

    .line 472
    .local v0, "error":Landroid/app/enterprise/license/Error;
    const/16 v1, 0x320

    if-ne p3, v1, :cond_2

    .line 473
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getMDMPUSHFrom()Ljava/lang/String;

    move-result-object v1

    const-string v2, "KLMS_MDM_PUSH_FROM_UMC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 475
    const-string v1, "KLMSValidator(): Sending ACTIVATION Result to UMC"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 476
    invoke-static {p5, p4, v0, p2, p3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->sendResultToMDMforUMC(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)V

    .line 496
    :cond_0
    :goto_0
    return-void

    .line 478
    :cond_1
    const-string v1, "KLMSValidator(): Sending ACTIVATION Result."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 479
    invoke-static {p5, p4, v0, p2, p3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->sendResultToMDM(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)V

    goto :goto_0

    .line 481
    :cond_2
    const/16 v1, 0x322

    if-ne p3, v1, :cond_3

    .line 482
    const-string v1, "KLMSValidator(): Sending DE-ACTIVATION Result."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 483
    invoke-static {p5, p4, v0, p2, p3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->sendResultToMDM(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)V

    .line 484
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getMDMforUMCState()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 485
    const-string v1, "KLMSValidator(): Sending DE-ACTIVATION Result to UMC."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 486
    invoke-static {p5, p4, v0, p2, p3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->sendResultToMDMforUMC(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)V

    goto :goto_0

    .line 488
    :cond_3
    const/16 v1, 0x321

    if-ne p3, v1, :cond_0

    .line 489
    const-string v1, "KLMSValidator(): Sending VALIDATION Result."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 490
    invoke-static {p5, p4, v0, p2, p3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->sendResultToMDM(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)V

    .line 491
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getMDMforUMCState()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 492
    const-string v1, "KLMSValidator(): Sending VALIDATION Result to UMC."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 493
    invoke-static {p5, p4, v0, p2, p3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->sendResultToMDMforUMC(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)V

    goto :goto_0
.end method

.method public static resetCronSettings(IJ)V
    .locals 9
    .param p0, "activationId"    # I
    .param p1, "nextValidation"    # J

    .prologue
    .line 255
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 256
    .local v0, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "KLMSValidator():  resetCronSettings(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 258
    :try_start_0
    new-instance v1, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v1}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 259
    .local v1, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v1, p0}, Lcom/samsung/klmsagent/beans/DeviceData;->set_id(I)V

    .line 261
    const-string v4, "_id=?"

    invoke-virtual {v0, v4, v1}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 262
    .local v2, "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 278
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_0
    :goto_0
    return-void

    .line 265
    .restart local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_1
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/klmsagent/beans/DeviceData;->setNextValidation(Ljava/lang/String;)V

    .line 266
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    add-long/2addr v6, p1

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/klmsagent/beans/DeviceData;->setAlarmTime(Ljava/lang/String;)V

    .line 267
    const-string v5, "_id=?"

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/klmsagent/beans/KLMSComponent;

    invoke-virtual {v0, v5, v4}, Lcom/samsung/klmsagent/data/DataSource;->updateData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)I

    move-result v4

    if-eqz v4, :cond_2

    .line 268
    const-string v4, "KLMSValidator(): successfully updated in resetCronSettings()"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :goto_1
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_0

    .line 270
    .restart local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_2
    :try_start_1
    const-string v4, "KLMSValidator(): error in updating resetCronSettings()"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 274
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :catch_0
    move-exception v3

    .line 275
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "KLMSValidator(): resetCronSettings() has Exception."

    invoke-static {v4, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static sendResultToMDM(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)V
    .locals 6
    .param p0, "pkgName"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "error"    # Landroid/app/enterprise/license/Error;
    .param p3, "initiator"    # I
    .param p4, "result_type"    # I

    .prologue
    .line 500
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    move-result-object v0

    .line 502
    .local v0, "knoxEnterpriseLicenseManager":Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;
    if-nez p0, :cond_0

    .line 503
    const-string v1, "KLMSValidator(): PackageName is NULL. Cannot Notify to MDM."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 511
    :goto_0
    return-void

    :cond_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    .line 505
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->processLicenseResponse(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 506
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KLMSValidator(): sendResultToMDM(Success) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .line 508
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KLMSValidator(): sendResultToMDM(Fail) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendResultToMDMforUMC(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)V
    .locals 6
    .param p0, "pkgName"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "error"    # Landroid/app/enterprise/license/Error;
    .param p3, "initiator"    # I
    .param p4, "result_type"    # I

    .prologue
    .line 515
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    move-result-object v0

    .line 517
    .local v0, "knoxEnterpriseLicenseManager":Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;
    if-nez p0, :cond_0

    .line 518
    const-string v1, "KLMSValidator(): PackageName is NULL. Cannot Notify to UMC."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 526
    :goto_0
    return-void

    :cond_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    .line 520
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->processLicenseResponseForUMC(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 521
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KLMSValidator(): sendResultToMDMforUMC(Success) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .line 523
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KLMSValidator(): sendResultToMDMforUMC(Fail) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setAlarmTime(IJ)V
    .locals 7
    .param p0, "activationId"    # I
    .param p1, "alarmTime"    # J

    .prologue
    .line 189
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "KLMSValidator(): setAlarmTime().activationId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 190
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 193
    .local v0, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    :try_start_0
    new-instance v1, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v1}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 194
    .local v1, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v1, p0}, Lcom/samsung/klmsagent/beans/DeviceData;->set_id(I)V

    .line 196
    const-string v4, "_id=?"

    invoke-virtual {v0, v4, v1}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 197
    .local v2, "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 208
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_0
    :goto_0
    return-void

    .line 200
    .restart local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_1
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/klmsagent/beans/DeviceData;->setAlarmTime(Ljava/lang/String;)V

    .line 201
    const-string v5, "_id=?"

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/klmsagent/beans/KLMSComponent;

    invoke-virtual {v0, v5, v4}, Lcom/samsung/klmsagent/data/DataSource;->updateData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :goto_1
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_0

    .line 204
    :catch_0
    move-exception v3

    .line 205
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "KLMSValidator(): setAlarmTime() has Exception."

    invoke-static {v4, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static setLicenseStatus(II)V
    .locals 7
    .param p0, "activationId"    # I
    .param p1, "licenseStatus"    # I

    .prologue
    .line 215
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KLMSValidator(): setLicenseStatus().activationId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 216
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v1

    .line 219
    .local v1, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    :try_start_0
    new-instance v2, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v2}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 220
    .local v2, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v2, p0}, Lcom/samsung/klmsagent/beans/DeviceData;->set_id(I)V

    .line 222
    const-string v5, "_id=?"

    invoke-virtual {v1, v5, v2}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v3

    .line 223
    .local v3, "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 241
    .end local v2    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v3    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_0
    :goto_0
    return-void

    .line 226
    .restart local v2    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v3    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_1
    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v5}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I

    move-result v0

    .line 227
    .local v0, "currentlicenseStatus":I
    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v5, p1}, Lcom/samsung/klmsagent/beans/DeviceData;->setLicenseStatus(I)V

    .line 228
    const-string v6, "_id=?"

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/klmsagent/beans/KLMSComponent;

    invoke-virtual {v1, v6, v5}, Lcom/samsung/klmsagent/data/DataSource;->updateData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)I

    move-result v5

    if-eqz v5, :cond_3

    .line 229
    const-string v5, "KLMSValidator(): successfully updated in setLicenseStatus()"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 233
    :goto_1
    invoke-static {v0}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->isLicenseStatus(I)Z

    move-result v5

    invoke-static {p1}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->isLicenseStatus(I)Z

    move-result v6

    if-eq v5, v6, :cond_2

    .line 234
    const/16 v5, 0xc8

    invoke-static {v5}, Lcom/samsung/klmsagent/services/i/ELMServices;->checkLicenseStatusToSendELM(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    .end local v0    # "currentlicenseStatus":I
    .end local v2    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v3    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_2
    :goto_2
    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_0

    .line 231
    .restart local v0    # "currentlicenseStatus":I
    .restart local v2    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v3    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_3
    :try_start_1
    const-string v5, "KLMSValidator(): error updating in setLicenseStatus()"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 237
    .end local v0    # "currentlicenseStatus":I
    .end local v2    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v3    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :catch_0
    move-exception v4

    .line 238
    .local v4, "e":Ljava/lang/Exception;
    const-string v5, "KLMSValidator(): setLicenseStatus() has Exception."

    invoke-static {v5, v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public static upgradeData(II)V
    .locals 14
    .param p0, "activationId"    # I
    .param p1, "licenseStatus"    # I

    .prologue
    .line 284
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v9

    const-string v12, "klmsagent.preferences"

    const/4 v13, 0x0

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 286
    .local v8, "mSharedPreferences":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v2

    .line 288
    .local v2, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const-string v9, "KLMS_NEXT_VALIDATION_INTERVAL"

    const-wide/16 v12, 0x0

    invoke-interface {v8, v9, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    .line 289
    .local v10, "nextValidation":J
    const-string v9, "KLMS_ALARM_TIME"

    const-wide/16 v12, 0x0

    invoke-interface {v8, v9, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 290
    .local v0, "alarmTime":J
    const-string v9, "KLMS_ALARM_TIME"

    const-wide/16 v12, 0x0

    invoke-interface {v8, v9, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 292
    .local v6, "expiry":J
    new-instance v3, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v3}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 293
    .local v3, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v3, p0}, Lcom/samsung/klmsagent/beans/DeviceData;->set_id(I)V

    .line 296
    :try_start_0
    const-string v9, "_id=?"

    invoke-virtual {v2, v9, v3}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v4

    .line 297
    .local v4, "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 313
    .end local v4    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_0
    :goto_0
    return-void

    .line 301
    .restart local v4    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_1
    const/4 v9, 0x0

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/samsung/klmsagent/beans/DeviceData;->setAlarmTime(Ljava/lang/String;)V

    .line 302
    const/4 v9, 0x0

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v9, p1}, Lcom/samsung/klmsagent/beans/DeviceData;->setLicenseStatus(I)V

    .line 303
    const/4 v9, 0x0

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/samsung/klmsagent/beans/DeviceData;->setNextValidation(Ljava/lang/String;)V

    .line 304
    const/4 v9, 0x0

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/samsung/klmsagent/beans/DeviceData;->setExpiry(Ljava/lang/String;)V

    .line 306
    const-string v12, "_id=?"

    const/4 v9, 0x0

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/klmsagent/beans/KLMSComponent;

    invoke-virtual {v2, v12, v9}, Lcom/samsung/klmsagent/data/DataSource;->updateData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 312
    .end local v4    # "deviceDataLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :goto_1
    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_0

    .line 309
    :catch_0
    move-exception v5

    .line 310
    .local v5, "e":Ljava/lang/Exception;
    const-string v9, "KLMSValidator(): upgradeData() has Exception."

    invoke-static {v9, v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
