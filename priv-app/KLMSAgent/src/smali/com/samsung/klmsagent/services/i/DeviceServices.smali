.class public Lcom/samsung/klmsagent/services/i/DeviceServices;
.super Lcom/samsung/klmsagent/services/KLMSServices;
.source "DeviceServices.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/klmsagent/services/KLMSServices",
        "<",
        "Lcom/samsung/klmsagent/beans/DeviceData;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DeviceServices(): "

.field private static source:Lcom/samsung/klmsagent/data/DataSource;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/samsung/klmsagent/services/KLMSServices;-><init>()V

    .line 40
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    sput-object v0, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    .line 41
    return-void
.end method

.method public static checkReplacementOrUnion(Ljava/lang/String;)Lcom/samsung/klmsagent/beans/DeviceData;
    .locals 8
    .param p0, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 747
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v1

    .line 748
    .local v1, "dataSourceCheck":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v5, 0x1

    new-array v4, v5, [Ljava/lang/String;

    aput-object p0, v4, v7

    .line 749
    .local v4, "whereArgs":[Ljava/lang/String;
    new-instance v0, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v0}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 750
    .local v0, "data":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v0, p0}, Lcom/samsung/klmsagent/beans/DeviceData;->setPackageName(Ljava/lang/String;)V

    .line 753
    :try_start_0
    const-string v5, "PACKAGE_NAME=?"

    const-string v7, "ASC"

    invoke-virtual {v1, v5, v0, v7, v4}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 754
    .local v2, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 755
    const-string v5, "DeviceServices(): Replacement Case."

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 756
    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/klmsagent/beans/DeviceData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 764
    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :goto_0
    return-object v5

    .line 758
    .restart local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_0
    :try_start_1
    const-string v5, "DeviceServices(): Union Case."

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 764
    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move-object v5, v6

    goto :goto_0

    .line 761
    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :catch_0
    move-exception v3

    .line 764
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    move-object v5, v6

    goto :goto_0

    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v5
.end method

.method public static deactivationProcess(ILjava/lang/String;)V
    .locals 17
    .param p0, "initiator"    # I
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 607
    const-string v14, "DeviceServices(): deactivationProcess().START"

    invoke-static {v14}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 608
    const/4 v8, 0x2

    .line 610
    .local v8, "notiTrigger":I
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v3

    .line 614
    .local v3, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v14, 0x1

    :try_start_0
    new-array v11, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    aput-object p1, v11, v14

    .line 615
    .local v11, "whereArgs":[Ljava/lang/String;
    new-instance v4, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v4}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 616
    .local v4, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setPackageName(Ljava/lang/String;)V

    .line 617
    const-string v14, "PACKAGE_NAME=?"

    const-string v15, "ASC"

    invoke-virtual {v3, v14, v4, v15, v11}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 619
    .local v5, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    const/4 v14, 0x0

    invoke-interface {v5, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v14}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v1

    .line 620
    .local v1, "activationId":I
    const/4 v14, 0x0

    invoke-interface {v5, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v14}, Lcom/samsung/klmsagent/beans/DeviceData;->getTrackerId()Ljava/lang/String;

    move-result-object v10

    .line 623
    .local v10, "trackerId":Ljava/lang/String;
    const/4 v14, 0x0

    invoke-interface {v5, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v14}, Lcom/samsung/klmsagent/beans/DeviceData;->getOnPremInfo()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_0

    .line 624
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->clearOnPremData()V

    .line 625
    invoke-static {}, Lcom/samsung/klmsagent/services/i/DeviceServices;->enableAlarm()V

    .line 628
    :cond_0
    invoke-static {v1}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->disableAlarm(I)V

    .line 631
    const/16 v14, 0x3e8

    move/from16 v0, p0

    if-ne v0, v14, :cond_1

    .line 632
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "DeviceServices(): deactivationProcess().addNotificationAppList: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 633
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->addNotificationAppList(Ljava/lang/String;)V

    .line 637
    :cond_1
    const-string v14, "deviceinfo"

    const-string v15, "PACKAGE_NAME=?"

    invoke-virtual {v3, v14, v15, v11}, Lcom/samsung/klmsagent/data/DataSource;->deleteTables(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 640
    const/4 v14, 0x1

    new-array v13, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    aput-object v10, v13, v14

    .line 641
    .local v13, "whereArgsTracker":[Ljava/lang/String;
    new-instance v4, Lcom/samsung/klmsagent/beans/DeviceData;

    .end local v4    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-direct {v4}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 642
    .restart local v4    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v4, v10}, Lcom/samsung/klmsagent/beans/DeviceData;->setTrackerId(Ljava/lang/String;)V

    .line 643
    const-string v14, "TRACKER_ID=?"

    const-string v15, "ASC"

    invoke-virtual {v3, v14, v4, v15, v13}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 645
    if-eqz v5, :cond_3

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_3

    .line 646
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 647
    .local v2, "dataDevice":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->getTrackerId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 649
    const-string v14, "DeviceServices(): Same tracker ID Found. Add Notification and Show."

    invoke-static {v14}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 650
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v14

    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->getPackageName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->addNotificationAppList(Ljava/lang/String;)V

    .line 651
    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v14

    invoke-static {v14}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->disableAlarm(I)V

    .line 653
    const/4 v14, 0x1

    new-array v12, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->getPackageName()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v12, v14

    .line 655
    .local v12, "whereArgsPkg":[Ljava/lang/String;
    const-string v14, "DeviceServices():  Activation record is found with same tracker id. Deleting the record."

    invoke-static {v14}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 656
    const-string v14, "deviceinfo"

    const-string v15, "PACKAGE_NAME=?"

    invoke-virtual {v3, v14, v15, v12}, Lcom/samsung/klmsagent/data/DataSource;->deleteTables(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 660
    const/16 v14, 0x32

    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->getPackageName()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->checkLicenseStatusToSendContainer(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 696
    .end local v1    # "activationId":I
    .end local v2    # "dataDevice":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v4    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v5    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v10    # "trackerId":Ljava/lang/String;
    .end local v11    # "whereArgs":[Ljava/lang/String;
    .end local v12    # "whereArgsPkg":[Ljava/lang/String;
    .end local v13    # "whereArgsTracker":[Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 697
    .local v6, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v14, "Common Unkown Exception."

    invoke-static {v14, v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 699
    invoke-virtual {v3}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 701
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 667
    .restart local v1    # "activationId":I
    .restart local v4    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v5    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v10    # "trackerId":Ljava/lang/String;
    .restart local v11    # "whereArgs":[Ljava/lang/String;
    .restart local v13    # "whereArgsTracker":[Ljava/lang/String;
    :cond_3
    const/4 v14, 0x0

    :try_start_2
    new-instance v15, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v15}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    invoke-virtual {v3, v14, v15}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v5

    .line 668
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v14

    if-nez v14, :cond_4

    .line 669
    const-string v14, "DeviceServices():  No activation record is found. reseting shared pref"

    invoke-static {v14}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 670
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->clear()V

    .line 671
    const/4 v14, 0x0

    new-instance v15, Lcom/samsung/klmsagent/beans/RequestLog;

    invoke-direct {v15}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>()V

    invoke-virtual {v3, v14, v15}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v9

    .line 672
    .local v9, "requestLogs":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/RequestLog;>;"
    if-eqz v9, :cond_4

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_4

    .line 673
    const-string v14, "removing all log data related to KLMS"

    invoke-static {v14}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 674
    const-string v14, "log_db"

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v3, v14, v15, v0}, Lcom/samsung/klmsagent/data/DataSource;->deleteTables(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 679
    .end local v9    # "requestLogs":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/RequestLog;>;"
    :cond_4
    const/16 v14, 0xc8

    invoke-static {v14}, Lcom/samsung/klmsagent/services/i/ELMServices;->checkLicenseStatusToSendELM(I)V

    .line 682
    const/16 v14, 0x32

    move-object/from16 v0, p1

    invoke-static {v14, v0}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->checkLicenseStatusToSendContainer(ILjava/lang/String;)V

    .line 685
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v14

    const v15, 0x7f060032

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/klmsagent/util/KLMSUtility;->showNotification(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 699
    invoke-virtual {v3}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_1

    .end local v1    # "activationId":I
    .end local v4    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v5    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v10    # "trackerId":Ljava/lang/String;
    .end local v11    # "whereArgs":[Ljava/lang/String;
    .end local v13    # "whereArgsTracker":[Ljava/lang/String;
    :catchall_0
    move-exception v14

    invoke-virtual {v3}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v14
.end method

.method public static disableAlarm()V
    .locals 7

    .prologue
    .line 795
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 798
    .local v0, "dataSourceCheck":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v5, 0x0

    :try_start_0
    new-instance v6, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v6}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    invoke-virtual {v0, v5, v6}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 799
    .local v2, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 800
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 801
    .local v1, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->getOnPremInfo()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 802
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->disableAlarm(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 806
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v3

    .line 807
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v5, "DeviceServices(): Exception found: "

    invoke-static {v5, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 809
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 811
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 809
    .restart local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_1

    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v5
.end method

.method public static enableAlarm()V
    .locals 7

    .prologue
    .line 773
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 776
    .local v0, "dataSourceCheck":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v5, 0x0

    :try_start_0
    new-instance v6, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v6}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    invoke-virtual {v0, v5, v6}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 777
    .local v2, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 778
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 779
    .local v1, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->getOnPremInfo()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 780
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/klmsagent/services/i/StateImplV2;->setUpAlarm(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 784
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v3

    .line 785
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v5, "DeviceServices(): Exception found: "

    invoke-static {v5, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 787
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 789
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 787
    .restart local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_1

    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v5
.end method

.method public static getDeviceInformation()Lcom/samsung/klmsagent/beans/DeviceData;
    .locals 4

    .prologue
    .line 332
    new-instance v0, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v0}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 334
    .local v0, "data":Lcom/samsung/klmsagent/beans/DeviceData;
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/beans/DeviceData;->setTime(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336
    const-string v1, "DeviceServices(): getDeviceInformation() gathered."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 338
    return-object v0

    .line 336
    :catchall_0
    move-exception v1

    const-string v2, "DeviceServices(): getDeviceInformation() gathered."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    throw v1
.end method

.method public static postProcessResponse(Ljava/lang/String;Landroid/os/Message;)V
    .locals 21
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 162
    const-string v2, "DeviceServices(): postProcessResponse().START"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 165
    const/4 v15, 0x1

    .line 167
    .local v15, "notiTrigger":I
    const/4 v14, 0x0

    .line 170
    .local v14, "firstActivation":Z
    :try_start_0
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v2

    sput-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    .line 173
    const/16 v2, 0x384

    const/16 v4, 0x320

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v2, v4, v1}, Lcom/samsung/klmsagent/services/i/DeviceServices;->processInvalidResponse(Ljava/lang/String;IILandroid/os/Message;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 322
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 324
    :goto_0
    return-void

    .line 176
    :cond_0
    if-eqz p0, :cond_1

    :try_start_1
    const-string v2, ""

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 177
    :cond_1
    const-string v2, "DeviceServices(): postProcessResponse() : Result from Server == NULL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 179
    new-instance v19, Lcom/samsung/klmsagent/context/State;

    const/16 v2, 0x22b8

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/samsung/klmsagent/context/State;-><init>(I)V

    .line 180
    .local v19, "state":Lcom/samsung/klmsagent/context/State;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/klmsagent/context/State;->takeActionGeneral()V

    .line 182
    const/16 v2, 0x190

    const/16 v3, 0x1f6

    const/16 v4, 0x384

    const/16 v5, 0x320

    const-string v6, "fail"

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "packageName"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 322
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_0

    .line 187
    .end local v19    # "state":Lcom/samsung/klmsagent/context/State;
    :cond_2
    :try_start_2
    new-instance v18, Lorg/json/JSONObject;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 188
    .local v18, "response":Lorg/json/JSONObject;
    invoke-static {}, Lcom/samsung/klmsagent/services/i/DeviceServices;->getDeviceInformation()Lcom/samsung/klmsagent/beans/DeviceData;

    move-result-object v10

    .line 190
    .local v10, "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    const-string v2, "payLoad"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    .line 191
    const-string v2, "DeviceServices(): postProcessResponse() : Payload from Server == NULL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 193
    new-instance v19, Lcom/samsung/klmsagent/context/State;

    const/16 v2, 0x3ed

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/samsung/klmsagent/context/State;-><init>(I)V

    .line 194
    .restart local v19    # "state":Lcom/samsung/klmsagent/context/State;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/klmsagent/context/State;->takeActionGeneral()V

    .line 196
    const/16 v2, 0x190

    const/16 v3, 0x22b8

    const/16 v4, 0x384

    const/16 v5, 0x320

    const-string v6, "fail"

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "packageName"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 322
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_0

    .line 201
    .end local v19    # "state":Lcom/samsung/klmsagent/context/State;
    :cond_3
    :try_start_3
    const-string v2, "payLoad"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/json/JSONObject;

    .line 202
    .local v17, "payload":Lorg/json/JSONObject;
    const-string v2, "responseCode"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 203
    .local v3, "statusCode":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DeviceServices(): Response code in enroll device : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 205
    const/16 v2, 0x3e8

    if-eq v3, v2, :cond_4

    const/16 v2, 0xc2b

    if-ne v3, v2, :cond_7

    .line 207
    :cond_4
    const-string v2, "DeviceServices(): Device enrolled successfully."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 208
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DeviceServices(): pkgName from server: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "mdmId"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " | pkgName on device: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "packageName"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 213
    const-string v2, "nextValidation"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/samsung/klmsagent/beans/DeviceData;->setNextValidation(Ljava/lang/String;)V

    .line 214
    const-string v2, "expiry"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/samsung/klmsagent/beans/DeviceData;->setExpiry(Ljava/lang/String;)V

    .line 215
    const-string v2, "deviceTrackerId"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/samsung/klmsagent/beans/DeviceData;->setTrackerId(Ljava/lang/String;)V

    .line 216
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "packageName"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/samsung/klmsagent/beans/DeviceData;->setPackageName(Ljava/lang/String;)V

    .line 217
    sget-object v2, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v2

    invoke-virtual {v10, v2}, Lcom/samsung/klmsagent/beans/DeviceData;->setLicenseStatus(I)V

    .line 219
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "onPrem"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 220
    const-string v2, "DeviceServices(): postProcessResponse().ON_PREMISE"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 221
    invoke-static {}, Lcom/samsung/klmsagent/services/i/DeviceServices;->disableAlarm()V

    .line 222
    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Lcom/samsung/klmsagent/beans/DeviceData;->setOnPremInfo(I)V

    .line 227
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DeviceServices(): license status: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v4, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual {v4}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 229
    const-string v2, ""

    invoke-virtual {v10, v2}, Lcom/samsung/klmsagent/beans/DeviceData;->setAlarmTime(Ljava/lang/String;)V

    .line 231
    const/4 v11, 0x0

    .line 232
    .local v11, "activation_id":I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "packageName"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 235
    .local v16, "packageName":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/4 v2, 0x0

    aput-object v16, v20, v2

    .line 236
    .local v20, "whereArgs":[Ljava/lang/String;
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    const-string v4, "PACKAGE_NAME=?"

    const-string v5, "ASC"

    move-object/from16 v0, v20

    invoke-virtual {v2, v4, v10, v5, v0}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    .line 239
    .local v12, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v12, :cond_6

    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 240
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v11

    .line 241
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DeviceServices(): Updating TrackID1 for activation id: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 243
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v2

    invoke-virtual {v10, v2}, Lcom/samsung/klmsagent/beans/DeviceData;->set_id(I)V

    .line 244
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/samsung/klmsagent/beans/DeviceData;->setTime(Ljava/lang/String;)V

    .line 246
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    const-string v4, "_id=?"

    invoke-virtual {v2, v4, v10}, Lcom/samsung/klmsagent/data/DataSource;->updateData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)I

    .line 257
    :goto_2
    new-instance v19, Lcom/samsung/klmsagent/context/State;

    const-string v2, "responseCode"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/samsung/klmsagent/context/State;-><init>(I)V

    .line 258
    .restart local v19    # "state":Lcom/samsung/klmsagent/context/State;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/klmsagent/context/State;->takeActionGeneral()V

    .line 260
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DeviceServices(): LicenseExpireDate()= "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "expiry"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " | ValidationInterval()= "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "nextValidation"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 264
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "packageName"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->deleteNotificationAppList(Ljava/lang/String;)V

    .line 266
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSUtility;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v2

    sget v4, Lcom/samsung/klmsagent/util/KLMSConstant;->NOTIFICATION_STACK_ID:I

    invoke-virtual {v2, v4}, Landroid/app/NotificationManager;->cancel(I)V

    .line 269
    invoke-static {v11}, Lcom/samsung/klmsagent/services/i/StateImplV2;->setUpAlarm(I)V

    .line 272
    const/16 v2, 0x64

    invoke-static {v2}, Lcom/samsung/klmsagent/services/i/ELMServices;->checkLicenseStatusToSendELM(I)V

    .line 275
    const/16 v2, 0xc8

    const/16 v4, 0x384

    const/16 v5, 0x320

    const-string v6, "success"

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "packageName"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 280
    const/16 v2, 0x5a

    move-object/from16 v0, v16

    invoke-static {v2, v0}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->checkLicenseStatusToSendContainer(ILjava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 322
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_0

    .line 224
    .end local v11    # "activation_id":I
    .end local v12    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v16    # "packageName":Ljava/lang/String;
    .end local v19    # "state":Lcom/samsung/klmsagent/context/State;
    .end local v20    # "whereArgs":[Ljava/lang/String;
    :cond_5
    :try_start_4
    const-string v2, "DeviceServices(): postProcessResponse().NORMAL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 225
    const/4 v2, 0x2

    invoke-virtual {v10, v2}, Lcom/samsung/klmsagent/beans/DeviceData;->setOnPremInfo(I)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 303
    .end local v3    # "statusCode":I
    .end local v10    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v17    # "payload":Lorg/json/JSONObject;
    .end local v18    # "response":Lorg/json/JSONObject;
    :catch_0
    move-exception v13

    .line 304
    .local v13, "e":Lorg/json/JSONException;
    :try_start_5
    const-string v2, "json parser exception."

    invoke-static {v2, v13}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 306
    new-instance v19, Lcom/samsung/klmsagent/context/State;

    const/16 v2, 0x22b8

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/samsung/klmsagent/context/State;-><init>(I)V

    .line 307
    .restart local v19    # "state":Lcom/samsung/klmsagent/context/State;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/klmsagent/context/State;->takeActionGeneral()V

    .line 309
    const/16 v4, 0x190

    const/16 v5, 0x22b8

    const/16 v6, 0x384

    const/16 v7, 0x320

    const-string v8, "fail"

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v9, "packageName"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v4 .. v9}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 322
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_0

    .line 249
    .end local v13    # "e":Lorg/json/JSONException;
    .end local v19    # "state":Lcom/samsung/klmsagent/context/State;
    .restart local v3    # "statusCode":I
    .restart local v10    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v11    # "activation_id":I
    .restart local v12    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v16    # "packageName":Ljava/lang/String;
    .restart local v17    # "payload":Lorg/json/JSONObject;
    .restart local v18    # "response":Lorg/json/JSONObject;
    .restart local v20    # "whereArgs":[Ljava/lang/String;
    :cond_6
    :try_start_6
    const-string v2, "DeviceServices(): Saved new activation data in database:"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 250
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v2, v10}, Lcom/samsung/klmsagent/data/DataSource;->saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z

    .line 251
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    const-string v4, "PACKAGE_NAME=?"

    const-string v5, "ASC"

    move-object/from16 v0, v20

    invoke-virtual {v2, v4, v10, v5, v0}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    .line 252
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v11

    .line 253
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DeviceServices(): activation id: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 254
    const/4 v14, 0x1

    goto/16 :goto_2

    .line 292
    .end local v11    # "activation_id":I
    .end local v12    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v16    # "packageName":Ljava/lang/String;
    .end local v20    # "whereArgs":[Ljava/lang/String;
    :cond_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DeviceServices(): Error in device enrollment. Payload response code is not valid:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "responseCode"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 295
    new-instance v19, Lcom/samsung/klmsagent/context/State;

    const-string v2, "responseCode"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/samsung/klmsagent/context/State;-><init>(I)V

    .line 296
    .restart local v19    # "state":Lcom/samsung/klmsagent/context/State;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/klmsagent/context/State;->takeActionGeneral()V

    .line 298
    const/16 v4, 0xc8

    const-string v2, "responseCode"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    const/16 v6, 0x384

    const/16 v7, 0x320

    const-string v8, "fail"

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v9, "packageName"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v4 .. v9}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 322
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_0

    .line 312
    .end local v3    # "statusCode":I
    .end local v10    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v17    # "payload":Lorg/json/JSONObject;
    .end local v18    # "response":Lorg/json/JSONObject;
    .end local v19    # "state":Lcom/samsung/klmsagent/context/State;
    :catch_1
    move-exception v13

    .line 313
    .local v13, "e":Ljava/lang/Exception;
    :try_start_7
    const-string v2, "unable to execute on db exception."

    invoke-static {v2, v13}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 315
    new-instance v19, Lcom/samsung/klmsagent/context/State;

    const/16 v2, 0x22b8

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/samsung/klmsagent/context/State;-><init>(I)V

    .line 316
    .restart local v19    # "state":Lcom/samsung/klmsagent/context/State;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/klmsagent/context/State;->takeActionGeneral()V

    .line 318
    const/16 v4, 0x190

    const/16 v5, 0x22b8

    const/16 v6, 0x384

    const/16 v7, 0x320

    const-string v8, "fail"

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v9, "packageName"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v4 .. v9}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 322
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_0

    .end local v13    # "e":Ljava/lang/Exception;
    .end local v19    # "state":Lcom/samsung/klmsagent/context/State;
    :catchall_0
    move-exception v2

    sget-object v4, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v4}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v2
.end method

.method public static postProcessResponseUninstall(Ljava/lang/String;Landroid/os/Message;)V
    .locals 17
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "reqObj"    # Landroid/os/Message;

    .prologue
    .line 529
    const-string v2, "DeviceServices(): postProcessResponseUninstall().START"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 532
    const/4 v2, -0x1

    const/16 v4, 0x322

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v2, v4, v1}, Lcom/samsung/klmsagent/services/i/DeviceServices;->processInvalidResponse(Ljava/lang/String;IILandroid/os/Message;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 604
    :cond_0
    :goto_0
    return-void

    .line 536
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "initiator"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 537
    .local v10, "initiator":I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "packageName"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 540
    .local v7, "packageName":Ljava/lang/String;
    if-nez p0, :cond_3

    .line 541
    :try_start_0
    const-string v2, "DeviceServices(): postProcessResponseUninstall() : Result from Server is Null."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 542
    const/16 v2, 0x3e8

    if-ne v10, v2, :cond_2

    .line 543
    const/16 v2, 0xc8

    const/16 v3, 0x1f6

    const/4 v4, -0x1

    const/16 v5, 0x322

    const-string v6, "fail"

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 595
    :catch_0
    move-exception v14

    .line 596
    .local v14, "e":Ljava/lang/Exception;
    const-string v2, "Common Unkown Exception."

    invoke-static {v2, v14}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 597
    const/16 v2, 0x3e8

    if-ne v10, v2, :cond_7

    .line 598
    const/16 v8, 0xc8

    const/16 v9, 0x66

    const/16 v11, 0x322

    const-string v12, "fail"

    move-object v13, v7

    invoke-static/range {v8 .. v13}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 546
    .end local v14    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v2, -0x1

    if-ne v10, v2, :cond_0

    .line 547
    :try_start_1
    invoke-static {v10, v7}, Lcom/samsung/klmsagent/services/i/DeviceServices;->deactivationProcess(ILjava/lang/String;)V

    goto :goto_0

    .line 551
    :cond_3
    new-instance v16, Lorg/json/JSONObject;

    invoke-direct/range {v16 .. v17}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 552
    .local v16, "response":Lorg/json/JSONObject;
    const-string v2, "payLoad"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_5

    .line 553
    const-string v2, "DeviceServices(): postProcessResponseUninstall() : payLoad from Server is Null."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 554
    const/16 v2, 0x3e8

    if-ne v10, v2, :cond_4

    .line 555
    const/16 v2, 0xc8

    const/16 v3, 0x22b8

    const/4 v4, -0x1

    const/16 v5, 0x322

    const-string v6, "fail"

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 558
    :cond_4
    const/4 v2, -0x1

    if-ne v10, v2, :cond_0

    .line 559
    invoke-static {v10, v7}, Lcom/samsung/klmsagent/services/i/DeviceServices;->deactivationProcess(ILjava/lang/String;)V

    goto :goto_0

    .line 563
    :cond_5
    const-string v2, "payLoad"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/json/JSONObject;

    .line 565
    .local v15, "payload":Lorg/json/JSONObject;
    const-string v2, "responseCode"

    invoke-virtual {v15, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 567
    .local v3, "responseCode":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DeviceServices(): postProcessResponseUninstall().responseCode: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 569
    packed-switch v3, :pswitch_data_0

    .line 586
    const-string v2, "DeviceServices(): postProcessResponseUninstall().Response>>Failed."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 587
    const/16 v2, 0x3e8

    if-ne v10, v2, :cond_6

    .line 588
    const/16 v2, 0xc8

    const/16 v5, 0x322

    const-string v6, "fail"

    move v4, v10

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 571
    :pswitch_0
    const-string v2, "DeviceServices(): postProcessResponseUninstall().Response>>Success."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 573
    invoke-static {v10, v7}, Lcom/samsung/klmsagent/services/i/DeviceServices;->deactivationProcess(ILjava/lang/String;)V

    .line 576
    const/16 v2, 0x3e8

    if-ne v10, v2, :cond_0

    .line 577
    const/16 v2, 0xc8

    const/16 v4, 0x3e8

    const/16 v5, 0x322

    const-string v6, "success"

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 590
    :cond_6
    const/4 v2, -0x1

    if-ne v10, v2, :cond_0

    .line 591
    invoke-static {v10, v7}, Lcom/samsung/klmsagent/services/i/DeviceServices;->deactivationProcess(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 600
    .end local v3    # "responseCode":I
    .end local v15    # "payload":Lorg/json/JSONObject;
    .end local v16    # "response":Lorg/json/JSONObject;
    .restart local v14    # "e":Ljava/lang/Exception;
    :cond_7
    const/4 v2, -0x1

    if-ne v10, v2, :cond_0

    .line 601
    invoke-static {v10, v7}, Lcom/samsung/klmsagent/services/i/DeviceServices;->deactivationProcess(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 569
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method

.method public static processInvalidResponse(Ljava/lang/String;IILandroid/os/Message;)Z
    .locals 10
    .param p0, "response"    # Ljava/lang/String;
    .param p1, "initiator"    # I
    .param p2, "type"    # I
    .param p3, "reqObj"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 706
    if-nez p0, :cond_0

    .line 707
    const-string v0, "DeviceServices(): processInvalidResponse() : Response from Server is null."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 708
    const/4 v0, 0x0

    .line 741
    :goto_0
    return v0

    .line 711
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 712
    .local v9, "stateCheck":Ljava/lang/Boolean;
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 714
    .local v7, "payLoad":Lorg/json/JSONObject;
    invoke-static {p0, p3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->checkTransactionId(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 715
    const-string v0, "DeviceServices(): processInvalidResponse() : KLMS Server Transaction id issue found. Notifying to MDM."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 717
    const/16 v0, 0xc8

    const/16 v1, 0x66

    const-string v4, "fail"

    invoke-virtual {p3}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "packageName"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move v2, p1

    move v3, p2

    invoke-static/range {v0 .. v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 719
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 733
    :cond_1
    :goto_1
    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 734
    invoke-virtual {p3}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "initiator"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 735
    invoke-virtual {p3}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "initiator"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "packageName"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/services/i/DeviceServices;->deactivationProcess(ILjava/lang/String;)V

    .line 738
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 721
    :cond_3
    const-string v0, "statusCode"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "3001"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 722
    const-string v0, "DeviceServices(): processInvalidResponse() : KLMS Server Time issue found. Notifying to MDM."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 723
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 724
    const/16 v0, 0xc8

    const/16 v1, 0xbb9

    const-string v4, "fail"

    invoke-virtual {p3}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "packageName"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move v2, p1

    move v3, p2

    invoke-static/range {v0 .. v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 727
    const/16 v0, 0x384

    if-ne p1, v0, :cond_1

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 728
    new-instance v8, Lcom/samsung/klmsagent/context/State;

    const/16 v0, 0xbb9

    invoke-direct {v8, v0}, Lcom/samsung/klmsagent/context/State;-><init>(I)V

    .line 729
    .local v8, "state":Lcom/samsung/klmsagent/context/State;
    invoke-virtual {v8}, Lcom/samsung/klmsagent/context/State;->takeActionGeneral()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 739
    .end local v7    # "payLoad":Lorg/json/JSONObject;
    .end local v8    # "state":Lcom/samsung/klmsagent/context/State;
    .end local v9    # "stateCheck":Ljava/lang/Boolean;
    :catch_0
    move-exception v6

    .line 740
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "DeviceServices(): processInvalidResponse() : Checking for Wrong Date and Time."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 741
    const/4 v0, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public isAlreadyRegisterd(Lcom/samsung/klmsagent/beans/DeviceData;)Z
    .locals 1
    .param p1, "obj"    # Lcom/samsung/klmsagent/beans/DeviceData;

    .prologue
    .line 328
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic isAlreadyRegisterd(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z
    .locals 1
    .param p1, "x0"    # Lcom/samsung/klmsagent/beans/KLMSComponent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    check-cast p1, Lcom/samsung/klmsagent/beans/DeviceData;

    .end local p1    # "x0":Lcom/samsung/klmsagent/beans/KLMSComponent;
    invoke-virtual {p0, p1}, Lcom/samsung/klmsagent/services/i/DeviceServices;->isAlreadyRegisterd(Lcom/samsung/klmsagent/beans/DeviceData;)Z

    move-result v0

    return v0
.end method

.method public onDestroy(ILjava/lang/String;)V
    .locals 22
    .param p1, "id"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 415
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DeviceServices(): onDestroy().START - initiator: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | packageName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 417
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v8

    .line 418
    .local v8, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v15, 0x0

    .line 421
    .local v15, "licenseKey":Ljava/lang/String;
    const/16 v2, 0x3e8

    move/from16 v0, p1

    if-ne v0, v2, :cond_1

    .line 422
    const-string v2, "DeviceServices(): De-activation call by MDM. Try to get the KNOX License."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 423
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    move-result-object v14

    .line 425
    .local v14, "knoxEnterpriseLicenseManager":Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;
    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getKLMLicenseKeyForDeactivation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 426
    if-eqz v15, :cond_0

    invoke-virtual {v15}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 427
    :cond_0
    const-string v2, "DeviceServices(): Could not fetch KNOX License from MDM for De-ativation."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 428
    const/16 v2, 0xc8

    const/16 v3, 0x2bf

    const/16 v5, 0x322

    const-string v6, "fail"

    move/from16 v4, p1

    move-object/from16 v7, p2

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 517
    .end local v14    # "knoxEnterpriseLicenseManager":Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;
    :goto_0
    return-void

    .line 434
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSUtility;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 435
    const/16 v2, 0x3e8

    move/from16 v0, p1

    if-ne v0, v2, :cond_2

    .line 436
    const-string v2, "DeviceServices(): No Network while trying to deactivate license. Informing MDM"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 437
    const/16 v2, 0xc8

    const/16 v3, 0x270f

    const/16 v5, 0x322

    const-string v6, "fail"

    move/from16 v4, p1

    move-object/from16 v7, p2

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 444
    :cond_2
    const/4 v2, 0x1

    :try_start_0
    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/4 v2, 0x0

    aput-object p2, v21, v2

    .line 446
    .local v21, "whereArgs":[Ljava/lang/String;
    new-instance v10, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v10}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 447
    .local v10, "deviceDataObj":Lcom/samsung/klmsagent/beans/DeviceData;
    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setPackageName(Ljava/lang/String;)V

    .line 449
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    const-string v3, "PACKAGE_NAME=?"

    const-string v4, "ASC"

    move-object/from16 v0, v21

    invoke-virtual {v2, v3, v10, v4, v0}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    .line 452
    .local v11, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v11, :cond_8

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 453
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 454
    .local v9, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v9}, Lcom/samsung/klmsagent/beans/DeviceData;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 456
    invoke-virtual {v9}, Lcom/samsung/klmsagent/beans/DeviceData;->getOnPremInfo()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 457
    invoke-static {}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->checkIfOnPrem()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 458
    const-string v2, "Cannot call de-activate api return. As on premise activation is present"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 514
    invoke-virtual {v8}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_0

    .line 462
    :cond_4
    :try_start_1
    new-instance v16, Landroid/os/Message;

    invoke-direct/range {v16 .. v16}, Landroid/os/Message;-><init>()V

    .line 463
    .local v16, "msg":Landroid/os/Message;
    new-instance v18, Lorg/json/JSONObject;

    invoke-direct/range {v18 .. v18}, Lorg/json/JSONObject;-><init>()V

    .line 464
    .local v18, "request":Lorg/json/JSONObject;
    new-instance v17, Lorg/json/JSONObject;

    invoke-direct/range {v17 .. v17}, Lorg/json/JSONObject;-><init>()V

    .line 466
    .local v17, "payLoad":Lorg/json/JSONObject;
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSUtility;->getTransactionId()Ljava/lang/String;

    move-result-object v19

    .line 468
    .local v19, "signature":Ljava/lang/String;
    const-string v2, "level"

    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 469
    const/16 v2, 0x3e8

    move/from16 v0, p1

    if-ne v0, v2, :cond_5

    .line 470
    const-string v2, "licenseKey"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 471
    :cond_5
    const-string v2, "trackerId"

    invoke-virtual {v9}, Lcom/samsung/klmsagent/beans/DeviceData;->getTrackerId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 472
    const-string v2, "mdmId"

    invoke-virtual {v9}, Lcom/samsung/klmsagent/beans/DeviceData;->getPackageName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 473
    const-string v2, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 474
    const-string v2, "id"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 476
    invoke-virtual {v9}, Lcom/samsung/klmsagent/beans/DeviceData;->getOnPremInfo()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    .line 477
    const-string v2, "DeviceServices(): onDestroy().ON_PREMISE"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 478
    invoke-virtual/range {v16 .. v16}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "onPrem"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 484
    :goto_2
    const-string v2, "payLoad"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 485
    const-string v2, "signature"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 487
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 488
    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->DEVICE_DEACTIVATION:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v2

    move-object/from16 v0, v16

    iput v2, v0, Landroid/os/Message;->what:I

    .line 490
    invoke-virtual/range {v16 .. v16}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "packageName"

    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    invoke-virtual/range {v16 .. v16}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "initiator"

    move/from16 v0, p1

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 492
    invoke-virtual/range {v16 .. v16}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "transaction_id"

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    new-instance v20, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 495
    .local v20, "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "/klm-rest/v3/device/uninstall.do"

    aput-object v4, v2, v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 497
    invoke-virtual {v8}, Lcom/samsung/klmsagent/data/DataSource;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 507
    .end local v9    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v10    # "deviceDataObj":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v11    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v16    # "msg":Landroid/os/Message;
    .end local v17    # "payLoad":Lorg/json/JSONObject;
    .end local v18    # "request":Lorg/json/JSONObject;
    .end local v19    # "signature":Ljava/lang/String;
    .end local v20    # "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    .end local v21    # "whereArgs":[Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 508
    .local v12, "e":Ljava/lang/Exception;
    const/16 v2, 0x3e8

    move/from16 v0, p1

    if-ne v0, v2, :cond_6

    .line 509
    const/16 v2, 0xc8

    const/16 v3, 0x1f6

    const/16 v5, 0x322

    :try_start_2
    const-string v6, "fail"

    move/from16 v4, p1

    move-object/from16 v7, p2

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception happened"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 514
    invoke-virtual {v8}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_0

    .line 480
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v9    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v10    # "deviceDataObj":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v11    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v13    # "i$":Ljava/util/Iterator;
    .restart local v16    # "msg":Landroid/os/Message;
    .restart local v17    # "payLoad":Lorg/json/JSONObject;
    .restart local v18    # "request":Lorg/json/JSONObject;
    .restart local v19    # "signature":Ljava/lang/String;
    .restart local v21    # "whereArgs":[Ljava/lang/String;
    :cond_7
    :try_start_3
    const-string v2, "DeviceServices(): onDestroy().NORMAL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 481
    invoke-virtual/range {v16 .. v16}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "onPrem"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 514
    .end local v9    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v10    # "deviceDataObj":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v11    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v16    # "msg":Landroid/os/Message;
    .end local v17    # "payLoad":Lorg/json/JSONObject;
    .end local v18    # "request":Lorg/json/JSONObject;
    .end local v19    # "signature":Ljava/lang/String;
    .end local v21    # "whereArgs":[Ljava/lang/String;
    :catchall_0
    move-exception v2

    invoke-virtual {v8}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v2

    .line 501
    .restart local v10    # "deviceDataObj":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v11    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v21    # "whereArgs":[Ljava/lang/String;
    :cond_8
    const/16 v2, 0x3e8

    move/from16 v0, p1

    if-ne v0, v2, :cond_9

    .line 502
    :try_start_4
    const-string v2, "DeviceServices(): sending error to MDM since no device info found on de activation"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 503
    const/16 v2, 0xc8

    const/16 v3, 0x2bf

    const/16 v5, 0x322

    const-string v6, "fail"

    move/from16 v4, p1

    move-object/from16 v7, p2

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 514
    :cond_9
    invoke-virtual {v8}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_0
.end method

.method public validateRegister(Lcom/samsung/klmsagent/beans/DeviceData;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V
    .locals 18
    .param p1, "obj"    # Lcom/samsung/klmsagent/beans/DeviceData;
    .param p2, "var"    # Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    .param p3, "sourceInput"    # Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    .prologue
    .line 44
    const-string v1, "DeviceServices(): validateRegister().START"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 46
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSUtility;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 47
    const-string v1, "DeviceServices(): validateRegiser() : No Network."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 49
    new-instance v16, Lcom/samsung/klmsagent/context/State;

    const/16 v1, 0x270f

    move-object/from16 v0, v16

    invoke-direct {v0, v1}, Lcom/samsung/klmsagent/context/State;-><init>(I)V

    .line 50
    .local v16, "state":Lcom/samsung/klmsagent/context/State;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/context/State;->takeActionGeneral()V

    .line 53
    const/16 v1, 0xc8

    const/16 v2, 0x270f

    const/16 v3, 0x384

    const/16 v4, 0x320

    const-string v5, "fail"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/klmsagent/beans/DeviceData;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 149
    .end local v16    # "state":Lcom/samsung/klmsagent/context/State;
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/klmsagent/beans/DeviceData;->getOnPremInfo()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 61
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 62
    const-string v1, "DeviceServices(): Onpremise GSLB not set, Calling GSLB to fetch KLM Server address."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 63
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v1

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponseForDeviceReg(Ljava/lang/Boolean;)V

    .line 64
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 65
    .local v11, "extras":Landroid/os/Bundle;
    const-string v1, "deviceData"

    move-object/from16 v0, p1

    invoke-virtual {v11, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 66
    const-string v1, "requestType"

    move-object/from16 v0, p2

    invoke-virtual {v11, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 67
    const-string v1, "keySource"

    move-object/from16 v0, p3

    invoke-virtual {v11, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 68
    invoke-static {v11}, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;->doGSLBJob(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    sget-object v1, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    if-eqz v1, :cond_0

    .line 147
    sget-object v1, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_0

    .line 72
    .end local v11    # "extras":Landroid/os/Bundle;
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getKLMSServerAddr()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 73
    const-string v1, "DeviceServices(): Normal GSLB not set, Calling GSLB to fetch KLM Server address."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 74
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v1

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponseForDeviceReg(Ljava/lang/Boolean;)V

    .line 75
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 76
    .restart local v11    # "extras":Landroid/os/Bundle;
    const-string v1, "deviceData"

    move-object/from16 v0, p1

    invoke-virtual {v11, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 77
    const-string v1, "requestType"

    move-object/from16 v0, p2

    invoke-virtual {v11, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 78
    const-string v1, "keySource"

    move-object/from16 v0, p3

    invoke-virtual {v11, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 79
    invoke-static {v11}, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;->doGSLBJob(Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146
    sget-object v1, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    if-eqz v1, :cond_0

    .line 147
    sget-object v1, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_0

    .line 84
    .end local v11    # "extras":Landroid/os/Bundle;
    :cond_3
    const/4 v7, 0x0

    .line 85
    .local v7, "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    :try_start_2
    new-instance v12, Landroid/os/Message;

    invoke-direct {v12}, Landroid/os/Message;-><init>()V

    .line 87
    .local v12, "msg":Landroid/os/Message;
    invoke-static {}, Lcom/samsung/klmsagent/services/i/DeviceServices;->getDeviceInformation()Lcom/samsung/klmsagent/beans/DeviceData;

    move-result-object v7

    .line 88
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSUtility;->getTransactionId()Ljava/lang/String;

    move-result-object v15

    .line 90
    .local v15, "signature":Ljava/lang/String;
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V

    .line 91
    .local v14, "request":Lorg/json/JSONObject;
    new-instance v13, Lorg/json/JSONObject;

    invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V

    .line 93
    .local v13, "payLoad":Lorg/json/JSONObject;
    const-string v1, "deviceId"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 94
    const-string v1, "model"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getModelName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 95
    const-string v1, "platform"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getAndroidVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 96
    const-string v1, "deviceEnrollTime"

    invoke-virtual {v7}, Lcom/samsung/klmsagent/beans/DeviceData;->getTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 97
    const-string v1, "licenseKey"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 98
    const-string v1, "mdmId"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/klmsagent/beans/DeviceData;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 101
    sget-object v1, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    const/4 v2, 0x0

    new-instance v3, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v3}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v9

    .line 102
    .local v9, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_6

    .line 103
    const-string v1, "DeviceServices(): installType: 3"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 104
    const-string v1, "installType"

    const/4 v2, 0x3

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 117
    :goto_1
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 118
    const-string v1, "mcc"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 119
    :cond_4
    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMNC1()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMNC1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 120
    const-string v1, "mnc"

    invoke-static {}, Lcom/samsung/klmsagent/util/DeviceDetailsUtility;->getMNC1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 122
    :cond_5
    const-string v1, "payLoad"

    invoke-virtual {v14, v1, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 123
    const-string v1, "signature"

    invoke-virtual {v14, v1, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 125
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/klmsagent/beans/DeviceData;->getOnPremInfo()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    .line 126
    const-string v1, "DeviceServices(): validateRegister().ON_PREMISE"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v12}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "onPrem"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 132
    :goto_2
    invoke-virtual {v12}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "packageName"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/klmsagent/beans/DeviceData;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-virtual {v12}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "transaction_id"

    invoke-virtual {v1, v2, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v1

    iput v1, v12, Landroid/os/Message;->what:I

    .line 136
    iput-object v14, v12, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 137
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->ordinal()I

    move-result v1

    iput v1, v12, Landroid/os/Message;->arg1:I

    .line 139
    const-string v1, "DeviceServices(): Starting enroll device"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 140
    new-instance v17, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    move-object/from16 v0, v17

    invoke-direct {v0, v12}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 141
    .local v17, "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "/klm-rest/v3/device/install.do"

    aput-object v3, v1, v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 146
    sget-object v1, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    if-eqz v1, :cond_0

    .line 147
    sget-object v1, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_0

    .line 106
    .end local v17    # "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    :cond_6
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/klmsagent/beans/DeviceData;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/services/i/DeviceServices;->checkReplacementOrUnion(Ljava/lang/String;)Lcom/samsung/klmsagent/beans/DeviceData;

    move-result-object v8

    .line 107
    .local v8, "dataCheck":Lcom/samsung/klmsagent/beans/DeviceData;
    if-eqz v8, :cond_7

    .line 108
    const-string v1, "DeviceServices(): installType: 2"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 109
    const-string v1, "installType"

    const/4 v2, 0x2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 110
    const-string v1, "oldDeviceTrackerId"

    invoke-virtual {v8}, Lcom/samsung/klmsagent/beans/DeviceData;->getTrackerId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 143
    .end local v7    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v8    # "dataCheck":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v9    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v12    # "msg":Landroid/os/Message;
    .end local v13    # "payLoad":Lorg/json/JSONObject;
    .end local v14    # "request":Lorg/json/JSONObject;
    .end local v15    # "signature":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 144
    .local v10, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v1, "DeviceComponentRegistration.register(): Exception"

    invoke-static {v1, v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 146
    sget-object v1, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    if-eqz v1, :cond_0

    .line 147
    sget-object v1, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_0

    .line 112
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v7    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v8    # "dataCheck":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v9    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v12    # "msg":Landroid/os/Message;
    .restart local v13    # "payLoad":Lorg/json/JSONObject;
    .restart local v14    # "request":Lorg/json/JSONObject;
    .restart local v15    # "signature":Ljava/lang/String;
    :cond_7
    :try_start_5
    const-string v1, "DeviceServices(): installType: 1"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 113
    const-string v1, "installType"

    const/4 v2, 0x1

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 146
    .end local v7    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v8    # "dataCheck":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v9    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v12    # "msg":Landroid/os/Message;
    .end local v13    # "payLoad":Lorg/json/JSONObject;
    .end local v14    # "request":Lorg/json/JSONObject;
    .end local v15    # "signature":Ljava/lang/String;
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    if-eqz v2, :cond_8

    .line 147
    sget-object v2, Lcom/samsung/klmsagent/services/i/DeviceServices;->source:Lcom/samsung/klmsagent/data/DataSource;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    :cond_8
    throw v1

    .line 129
    .restart local v7    # "_data":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v9    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v12    # "msg":Landroid/os/Message;
    .restart local v13    # "payLoad":Lorg/json/JSONObject;
    .restart local v14    # "request":Lorg/json/JSONObject;
    .restart local v15    # "signature":Ljava/lang/String;
    :cond_9
    :try_start_6
    const-string v1, "DeviceServices(): validateRegister().NORMAL"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 130
    invoke-virtual {v12}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "onPrem"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_2
.end method

.method public bridge synthetic validateRegister(Lcom/samsung/klmsagent/beans/KLMSComponent;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/klmsagent/beans/KLMSComponent;
    .param p2, "x1"    # Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    .param p3, "x2"    # Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    check-cast p1, Lcom/samsung/klmsagent/beans/DeviceData;

    .end local p1    # "x0":Lcom/samsung/klmsagent/beans/KLMSComponent;
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/klmsagent/services/i/DeviceServices;->validateRegister(Lcom/samsung/klmsagent/beans/DeviceData;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V

    return-void
.end method

.method public validateTrackKey(Lcom/samsung/klmsagent/beans/DeviceData;II)V
    .locals 16
    .param p1, "obj"    # Lcom/samsung/klmsagent/beans/DeviceData;
    .param p2, "licenseStatus"    # I
    .param p3, "activationId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 347
    const-string v2, "DeviceServices(): validateTrackKey().START"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 350
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DeviceServices(): validateRegiser(): licenseStatus = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 351
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSUtility;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 352
    const-string v2, "DeviceServices(): No Network. No state change"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 354
    const/16 v2, 0xc8

    const/16 v3, 0x270f

    const/4 v4, -0x1

    const/16 v5, 0x321

    const-string v6, "fail"

    invoke-static/range {p3 .. p3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->getMDMId(I)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 358
    const-wide/32 v2, 0x5265c00

    move/from16 v0, p3

    invoke-static {v0, v2, v3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->resetCronSettings(IJ)V

    .line 359
    invoke-static/range {p3 .. p3}, Lcom/samsung/klmsagent/services/i/StateImplV2;->setUpAlarm(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 409
    const-string v2, "DeviceServices(): validateTrackKey(): Completed."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    .line 411
    :goto_0
    return-void

    .line 364
    :cond_0
    :try_start_1
    const-string v2, "validateTrackKey"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSUtility;->checkRightsToCallUrl(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 365
    const-string v2, "DeviceServices(): No authorized to call validateTrackKey"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 367
    const-wide/32 v2, 0x5265c00

    move/from16 v0, p3

    invoke-static {v0, v2, v3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->resetCronSettings(IJ)V

    .line 368
    invoke-static/range {p3 .. p3}, Lcom/samsung/klmsagent/services/i/StateImplV2;->setUpAlarm(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 409
    const-string v2, "DeviceServices(): validateTrackKey(): Completed."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 373
    :cond_1
    :try_start_2
    const-string v2, "DeviceServices(): Network is available, Validating to KLMS Server."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 375
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 376
    .local v11, "request":Lorg/json/JSONObject;
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 378
    .local v10, "payLoad":Lorg/json/JSONObject;
    new-instance v14, Lorg/json/JSONArray;

    invoke-direct {v14}, Lorg/json/JSONArray;-><init>()V

    .line 379
    .local v14, "trackIds":Lorg/json/JSONArray;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/klmsagent/beans/DeviceData;->getTrackerId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 381
    const-string v2, "trackerIdList"

    invoke-virtual {v10, v2, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 382
    const-string v2, "level"

    const/4 v3, 0x0

    invoke-virtual {v10, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 384
    const-string v2, "payLoad"

    invoke-virtual {v11, v2, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 386
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSUtility;->getTransactionId()Ljava/lang/String;

    move-result-object v12

    .line 387
    .local v12, "signature":Ljava/lang/String;
    const-string v2, "signature"

    invoke-virtual {v11, v2, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 389
    new-instance v9, Landroid/os/Message;

    invoke-direct {v9}, Landroid/os/Message;-><init>()V

    .line 390
    .local v9, "message":Landroid/os/Message;
    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->DEVICE_VALIDATION:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v2

    iput v2, v9, Landroid/os/Message;->what:I

    .line 391
    iput-object v11, v9, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 393
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/klmsagent/beans/DeviceData;->getOnPremInfo()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 394
    const-string v2, "DeviceServices(): validateTrackKey().ON_PREMISE"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 395
    invoke-virtual {v9}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "onPrem"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 400
    :goto_1
    invoke-virtual {v9}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "activation_id"

    move/from16 v0, p3

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 401
    invoke-virtual {v9}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "transaction_id"

    invoke-virtual {v2, v3, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    new-instance v13, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    invoke-direct {v13, v9}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 404
    .local v13, "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    const-string v15, "/klm-rest/v3/validateTrackKey.do"

    .line 405
    .local v15, "uri":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v15, v2, v3

    invoke-virtual {v13, v2}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 409
    const-string v2, "DeviceServices(): validateTrackKey(): Completed."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 397
    .end local v13    # "task":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    .end local v15    # "uri":Ljava/lang/String;
    :cond_2
    :try_start_3
    const-string v2, "DeviceServices(): validateTrackKey().NORMAL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 398
    invoke-virtual {v9}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "onPrem"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 406
    .end local v9    # "message":Landroid/os/Message;
    .end local v10    # "payLoad":Lorg/json/JSONObject;
    .end local v11    # "request":Lorg/json/JSONObject;
    .end local v12    # "signature":Ljava/lang/String;
    .end local v14    # "trackIds":Lorg/json/JSONArray;
    :catch_0
    move-exception v8

    .line 407
    .local v8, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v2, "DeviceServices(): validateTrackKey(): Exception"

    invoke-static {v2, v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 409
    const-string v2, "DeviceServices(): validateTrackKey(): Completed."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    const-string v3, "DeviceServices(): validateTrackKey(): Completed."

    invoke-static {v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->i(Ljava/lang/String;)V

    throw v2
.end method

.method public bridge synthetic validateTrackKey(Lcom/samsung/klmsagent/beans/KLMSComponent;II)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/klmsagent/beans/KLMSComponent;
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    check-cast p1, Lcom/samsung/klmsagent/beans/DeviceData;

    .end local p1    # "x0":Lcom/samsung/klmsagent/beans/KLMSComponent;
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/klmsagent/services/i/DeviceServices;->validateTrackKey(Lcom/samsung/klmsagent/beans/DeviceData;II)V

    return-void
.end method
