.class public abstract Lcom/samsung/klmsagent/services/KLMSServices;
.super Ljava/lang/Object;
.source "KLMSServices.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/samsung/klmsagent/beans/KLMSComponent;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    .local p0, "this":Lcom/samsung/klmsagent/services/KLMSServices;, "Lcom/samsung/klmsagent/services/KLMSServices<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract isAlreadyRegisterd(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation
.end method

.method public abstract onDestroy(ILjava/lang/String;)V
.end method

.method public abstract validateRegister(Lcom/samsung/klmsagent/beans/KLMSComponent;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;",
            "Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation
.end method

.method public abstract validateTrackKey(Lcom/samsung/klmsagent/beans/KLMSComponent;II)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;II)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation
.end method
