.class public Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;
.super Ljava/lang/Object;
.source "NetworkChangeOperations.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/services/i/NetworkChangeOperations$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "NetworkChangeOperations(): "


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    return-void
.end method

.method public static doGSLBB2CJob()V
    .locals 2

    .prologue
    .line 43
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getB2CKLMSServerAddr()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 44
    const-string v0, "NetworkChangeOperations(): Get KLMS B2C Server address from GSLB."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 45
    const/4 v0, 0x0

    const-string v1, "b2c"

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->setKLMSServerAddr(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    const-string v0, "NetworkChangeOperations(): KLMS B2C Server Sddress alreay Set. No need to call GSLB."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static doGSLBJob()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;->doGSLBJob(Landroid/os/Bundle;)V

    .line 40
    return-void
.end method

.method public static doGSLBJob(Landroid/os/Bundle;)V
    .locals 1
    .param p0, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getKLMSServerAddr()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 31
    const-string v0, "NetworkChangeOperations(): Get KLMS B2B Server Address from GSLB."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 32
    const-string v0, "b2b"

    invoke-static {p0, v0}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->setKLMSServerAddr(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 36
    :goto_0
    return-void

    .line 34
    :cond_0
    const-string v0, "NetworkChangeOperations(): KLMS B2B Server Sddress alreay Set. No need to call GSLB."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static uploadRequestLog()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 73
    const-string v10, "NetworkChangeOperations(): uploadRequestLog().START "

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 77
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v1

    .line 78
    .local v1, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v9, 0x0

    .line 79
    .local v9, "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 80
    .local v3, "jsonObject":Lorg/json/JSONObject;
    const/4 v10, 0x0

    new-instance v11, Lcom/samsung/klmsagent/beans/RequestLog;

    invoke-direct {v11}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>()V

    const-string v12, "TIME ASC"

    invoke-virtual {v1, v10, v11, v12, v9}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 82
    .local v5, "lstOfRequest":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/RequestLog;>;"
    if-eqz v5, :cond_1

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1

    .line 83
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/klmsagent/beans/RequestLog;

    .line 85
    .local v4, "log":Lcom/samsung/klmsagent/beans/RequestLog;
    new-instance v6, Landroid/os/Message;

    invoke-direct {v6}, Landroid/os/Message;-><init>()V

    .line 86
    .local v6, "req":Landroid/os/Message;
    new-instance v3, Lorg/json/JSONObject;

    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    invoke-virtual {v4}, Lcom/samsung/klmsagent/beans/RequestLog;->getJsonRequest()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 87
    .restart local v3    # "jsonObject":Lorg/json/JSONObject;
    iput-object v3, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 89
    invoke-virtual {v4}, Lcom/samsung/klmsagent/beans/RequestLog;->getRequestType()I

    move-result v7

    .line 90
    .local v7, "reqType":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "NetworkChangeOperations():  req.what = log.getRequestType() = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v4}, Lcom/samsung/klmsagent/beans/RequestLog;->getRequestType()I

    move-result v10

    iput v10, v6, Landroid/os/Message;->what:I

    .line 93
    const/16 v10, 0xc

    if-eq v7, v10, :cond_0

    .line 94
    const-string v10, "NetworkChangeOperations(): If reqType is not B2C_RP, Add signature in reqObj."

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v6}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "transaction_id"

    const-string v12, "signature"

    invoke-virtual {v3, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :cond_0
    invoke-virtual {v6}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "REQUEST_DB_ID"

    invoke-virtual {v4}, Lcom/samsung/klmsagent/beans/RequestLog;->getId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    new-instance v0, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    invoke-direct {v0, v6}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 100
    .local v0, "cronTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->values()[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    move-result-object v8

    .line 103
    .local v8, "requestArray":[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    sget-object v10, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations$1;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$KLMSRequestType:[I

    iget v11, v6, Landroid/os/Message;->what:I

    aget-object v11, v8, v11

    invoke-virtual {v11}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 125
    const-string v10, "NetworkChangeOperations(): uploadRequestLog().url : No mapping for this handler."

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :pswitch_0
    const-string v10, "NetworkChangeOperations(): uploadRequestLog().url : /klm-rest/v3/container/install.do"

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 106
    new-array v10, v14, [Ljava/lang/String;

    const-string v11, "/klm-rest/v3/container/install.do"

    aput-object v11, v10, v13

    invoke-virtual {v0, v10}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 109
    :pswitch_1
    const-string v10, "NetworkChangeOperations(): uploadRequestLog().url : /klm-rest/v3/container/uninstall.do"

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 110
    new-array v10, v14, [Ljava/lang/String;

    const-string v11, "/klm-rest/v3/container/uninstall.do"

    aput-object v11, v10, v13

    invoke-virtual {v0, v10}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 113
    :pswitch_2
    const-string v10, "NetworkChangeOperations(): uploadRequestLog().url : /klm-rest/v3/device/uninstall.do"

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 114
    new-array v10, v14, [Ljava/lang/String;

    const-string v11, "/klm-rest/v3/device/uninstall.do"

    aput-object v11, v10, v13

    invoke-virtual {v0, v10}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 118
    :pswitch_3
    const-string v10, "NetworkChangeOperations(): uploadRequestLog().url : /knox-license/attribution/settings.do"

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 119
    const-string v10, "NetworkChangeOperations(): doGSLBB2CJob()"

    invoke-static {v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 120
    invoke-static {}, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;->doGSLBB2CJob()V

    .line 121
    new-array v10, v14, [Ljava/lang/String;

    const-string v11, "/knox-license/attribution/settings.do"

    aput-object v11, v10, v13

    invoke-virtual {v0, v10}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 130
    .end local v0    # "cronTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "log":Lcom/samsung/klmsagent/beans/RequestLog;
    .end local v6    # "req":Landroid/os/Message;
    .end local v7    # "reqType":I
    .end local v8    # "requestArray":[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    :cond_1
    return-void

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public doNetworkJob()V
    .locals 3

    .prologue
    .line 57
    :try_start_0
    invoke-static {}, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;->uploadRequestLog()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 63
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Lorg/json/JSONException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSONException:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 60
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 61
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
