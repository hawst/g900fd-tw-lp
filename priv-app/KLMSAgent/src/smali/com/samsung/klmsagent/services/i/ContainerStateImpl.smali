.class public Lcom/samsung/klmsagent/services/i/ContainerStateImpl;
.super Ljava/lang/Object;
.source "ContainerStateImpl.java"

# interfaces
.implements Lcom/samsung/klmsagent/services/ContainerStates;


# static fields
.field private static final TAG:Ljava/lang/String; = "ContainerStateImpl(): "


# instance fields
.field private context:Landroid/content/Context;

.field private mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/klmsagent/services/KLMSServices",
            "<",
            "Lcom/samsung/klmsagent/beans/ContainerData;",
            ">;"
        }
    .end annotation
.end field

.field mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/klmsagent/services/KLMSServices;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/klmsagent/services/KLMSServices",
            "<",
            "Lcom/samsung/klmsagent/beans/ContainerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p2, "mContainerHandler":Lcom/samsung/klmsagent/services/KLMSServices;, "Lcom/samsung/klmsagent/services/KLMSServices<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->context:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    .line 43
    return-void
.end method

.method public static SendIntentToContainer(ILjava/lang/String;)V
    .locals 3
    .param p0, "code"    # I
    .param p1, "containerID"    # Ljava/lang/String;

    .prologue
    .line 248
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContainerStateImpl(): SendIntentToContainer().code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | Container Id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 250
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.knox.containeragent.klms.licensekey.status"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 251
    .local v0, "intent":Landroid/content/Intent;
    if-nez p1, :cond_1

    .line 252
    const-string v1, "container_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 257
    :goto_0
    sparse-switch p0, :sswitch_data_0

    .line 286
    const-string v1, "code"

    const/16 v2, 0x32

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 289
    :cond_0
    :goto_1
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.knox.containeragent.USE_KNOX_UI"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 290
    return-void

    .line 254
    :cond_1
    const-string v1, "container_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0

    .line 259
    :sswitch_0
    const-string v1, "code"

    const/16 v2, 0x5a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 260
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseKADUrlForCA()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 261
    const-string v1, "ContainerStateImpl(): On premise case and KAD url available hence sending the CA kad url."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 262
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContainerStateImpl(): On premise KAD url : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseKADUrlForCA()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 264
    const-string v1, "kad_url"

    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseKADUrlForCA()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 267
    :cond_2
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseKADUrlForCA()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 269
    const-string v1, "ContainerStateImpl(): On premise case and KAD url not available hence sending the CA kad url error."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 270
    const-string v1, "kad_url_error"

    const/16 v2, 0x270f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 274
    :sswitch_1
    const-string v1, "code"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 277
    :sswitch_2
    const-string v1, "code"

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 280
    :sswitch_3
    const-string v1, "code"

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 283
    :sswitch_4
    const-string v1, "code"

    const/16 v2, 0x28

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 257
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_1
        0x14 -> :sswitch_3
        0x1e -> :sswitch_2
        0x28 -> :sswitch_4
        0x5a -> :sswitch_0
    .end sparse-switch
.end method

.method public static checkContainerPermission(Ljava/lang/String;)Z
    .locals 4
    .param p0, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 329
    const-string v2, "ContainerStateImpl(): checkContainerPermission().start "

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 331
    :try_start_0
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 332
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const-string v2, "android.permission.sec.MDM_ENTERPRISE_CONTAINER"

    invoke-virtual {v1, v2, p0}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 333
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ContainerStateImpl(): checkContainerPermission().pkgName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | CONTAINER_PERMISSION >> TRUE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    const/4 v2, 0x1

    .line 341
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v2

    .line 337
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 339
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ContainerStateImpl(): checkContainerPermission().pkgName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | CONTAINER_PERMISSION >> FALSE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 341
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static checkLicenseStatusToSendContainer(ILjava/lang/String;)V
    .locals 12
    .param p0, "code"    # I
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x4

    const/16 v10, 0x5a

    const/16 v9, 0x32

    .line 353
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ContainerStateImpl(): checkLicenseStatusToSendContainer().code: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "| pkgName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 355
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v4

    .line 358
    .local v4, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v7, 0x0

    :try_start_0
    new-instance v8, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    invoke-virtual {v4, v7, v8}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 359
    .local v2, "containerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    if-eqz v2, :cond_6

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_6

    .line 360
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/klmsagent/beans/ContainerData;

    .line 361
    .local v1, "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/ContainerData;->getContainerID()Ljava/lang/String;

    move-result-object v0

    .line 362
    .local v0, "ContainerId":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/ContainerData;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 364
    .local v3, "containerPkgName":Ljava/lang/String;
    invoke-virtual {v1, v0}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerID(Ljava/lang/String;)V

    .line 366
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ContainerStateImpl(): ContainerID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " | PkgName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 367
    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 369
    const-string v7, "ContainerStateImpl(): Action_pkgName == Container_pkgName."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 370
    if-ne p0, v10, :cond_1

    .line 371
    const-string v7, "ContainerStateImpl(): Send intent to Container to UNLOCK."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 372
    const/16 v7, 0x5a

    invoke-static {v7, v0}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V

    .line 373
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerStatus(Ljava/lang/Integer;)V

    .line 374
    invoke-virtual {v1, v3}, Lcom/samsung/klmsagent/beans/ContainerData;->setPackageName(Ljava/lang/String;)V

    .line 409
    :cond_0
    :goto_1
    const-string v7, "CONTAINER_ID=?"

    invoke-virtual {v4, v7, v1}, Lcom/samsung/klmsagent/data/DataSource;->updateData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)I

    .line 410
    const-string v7, "ContainerStateImpl(): checkLicenseStatusToSendContainer() updateData in container DB."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 415
    .end local v0    # "ContainerId":Ljava/lang/String;
    .end local v1    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v2    # "containerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .end local v3    # "containerPkgName":Ljava/lang/String;
    .end local v6    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v5

    .line 416
    .local v5, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v7, "checkLicenseStatusToSendContainer() Common Unkown Exception."

    invoke-static {v7, v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418
    invoke-virtual {v4}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 420
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    .line 375
    .restart local v0    # "ContainerId":Ljava/lang/String;
    .restart local v1    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .restart local v2    # "containerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .restart local v3    # "containerPkgName":Ljava/lang/String;
    .restart local v6    # "i$":Ljava/util/Iterator;
    :cond_1
    if-ne p0, v9, :cond_2

    .line 376
    :try_start_2
    const-string v7, "ContainerStateImpl(): Send intent to Container to LOCK."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 377
    const/16 v7, 0x32

    invoke-static {v7, v0}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V

    .line 378
    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerStatus(Ljava/lang/Integer;)V

    .line 379
    invoke-virtual {v1, v3}, Lcom/samsung/klmsagent/beans/ContainerData;->setPackageName(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 418
    .end local v0    # "ContainerId":Ljava/lang/String;
    .end local v1    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v2    # "containerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .end local v3    # "containerPkgName":Ljava/lang/String;
    .end local v6    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v7

    invoke-virtual {v4}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v7

    .line 381
    .restart local v0    # "ContainerId":Ljava/lang/String;
    .restart local v1    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .restart local v2    # "containerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .restart local v3    # "containerPkgName":Ljava/lang/String;
    .restart local v6    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ContainerStateImpl(): Not able to find action for this code: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_1

    .line 385
    :cond_3
    const-string v7, "ContainerStateImpl(): Action_pkgName != Container_pkgName."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 387
    if-ne p0, v10, :cond_4

    .line 389
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/ContainerData;->getContainerStatus()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-eq v7, v11, :cond_0

    .line 390
    const-string v7, "ContainerStateImpl(): Send intent to Container to UNLOCK."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 391
    const/16 v7, 0x5a

    invoke-static {v7, v0}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V

    .line 393
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerStatus(Ljava/lang/Integer;)V

    .line 394
    invoke-virtual {v1, v3}, Lcom/samsung/klmsagent/beans/ContainerData;->setPackageName(Ljava/lang/String;)V

    goto :goto_1

    .line 396
    :cond_4
    if-ne p0, v9, :cond_5

    .line 398
    invoke-static {v3}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->checkContainerPermission(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 399
    const-string v7, "ContainerStateImpl(): Send intent to Container to LOCK."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 400
    const/16 v7, 0x32

    invoke-static {v7, v0}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V

    .line 402
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerStatus(Ljava/lang/Integer;)V

    .line 403
    invoke-virtual {v1, v3}, Lcom/samsung/klmsagent/beans/ContainerData;->setPackageName(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 406
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ContainerStateImpl(): Not able to find action for this code: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 413
    .end local v0    # "ContainerId":Ljava/lang/String;
    .end local v1    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v3    # "containerPkgName":Ljava/lang/String;
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_6
    const-string v7, "ContainerStateImpl(): Not found Container data. Don\'t Send intent to Container."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 418
    :cond_7
    invoke-virtual {v4}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto/16 :goto_2
.end method

.method public static getAdminPackage(I)Ljava/lang/String;
    .locals 9
    .param p0, "containerId"    # I

    .prologue
    .line 301
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ContainerStateImpl(): getAdminPackage().containerId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 302
    const/4 v5, 0x0

    .line 305
    .local v5, "packageName":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "persona"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PersonaManager;

    .line 308
    .local v4, "mPm":Landroid/os/PersonaManager;
    if-eqz v4, :cond_0

    .line 309
    invoke-virtual {v4, p0}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v3

    .line 310
    .local v3, "info":Landroid/content/pm/PersonaInfo;
    invoke-virtual {v3}, Landroid/content/pm/PersonaInfo;->getAdminPackageName()Ljava/lang/String;

    move-result-object v5

    .line 311
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ContainerStateImpl(): PersonaInfo.getAdminPackage(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 314
    .end local v3    # "info":Landroid/content/pm/PersonaInfo;
    :cond_0
    const-string v7, "device_policy"

    invoke-static {v7}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Landroid/app/admin/IDevicePolicyManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/admin/IDevicePolicyManager;

    move-result-object v6

    .line 316
    .local v6, "service":Landroid/app/admin/IDevicePolicyManager;
    invoke-interface {v6, p0}, Landroid/app/admin/IDevicePolicyManager;->getActiveAdmins(I)Ljava/util/List;

    move-result-object v0

    .line 317
    .local v0, "cur":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 318
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ContainerStateImpl(): IDevicePolicyManager.getActiveAdmins(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 320
    .end local v0    # "cur":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;"
    .end local v2    # "i":I
    .end local v4    # "mPm":Landroid/os/PersonaManager;
    .end local v6    # "service":Landroid/app/admin/IDevicePolicyManager;
    :catch_0
    move-exception v1

    .line 321
    .local v1, "e":Ljava/lang/Exception;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ContainerStateImpl(): getAdminPackage() has Exception."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 324
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ContainerStateImpl(): getAdminPackage().containerId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " | packageName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 325
    return-object v5
.end method


# virtual methods
.method public checkLicenseKeyStatus(Landroid/content/Intent;)V
    .locals 21
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    .line 130
    const-string v18, "ContainerStateImpl(): checkLicenseKeyStatus().START"

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 132
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    .line 134
    .local v11, "extras":Landroid/os/Bundle;
    if-eqz v11, :cond_0

    const-string v18, "container_id"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v18

    if-nez v18, :cond_7

    .line 135
    :cond_0
    const-string v18, "ContainerStateImpl(): checkLicenseKeyStatus(): container_id == NULL."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 137
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v6

    .line 139
    .local v6, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/16 v18, 0x0

    :try_start_0
    new-instance v19, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v9

    .line 140
    .local v9, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 142
    .local v3, "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v9, :cond_6

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_6

    .line 143
    const/4 v15, 0x0

    .line 144
    .local v15, "success":Z
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 145
    .local v7, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v7}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    invoke-virtual {v7}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I

    move-result v18

    if-nez v18, :cond_2

    .line 148
    const-string v18, "ContainerStateImpl(): FOTA case. Activation record found but status not there in DB."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->context:Landroid/content/Context;

    move-object/from16 v18, v0

    const-string v19, "klmsagent.preferences"

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 153
    .local v13, "mSharedPreferences":Landroid/content/SharedPreferences;
    const-string v18, "KLMS_LICENSE_STATUS"

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v18

    if-nez v18, :cond_3

    .line 155
    const-string v18, "ContainerStateImpl(): FOTA case. License is Active."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 156
    const/4 v15, 0x1

    .line 157
    invoke-virtual {v7}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v18

    sget-object v19, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->upgradeData(II)V

    .line 163
    .end local v13    # "mSharedPreferences":Landroid/content/SharedPreferences;
    :cond_2
    :goto_1
    invoke-virtual {v7}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I

    move-result v18

    sget-object v19, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 164
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "ContainerStateImpl(): DeviceData.getLicenseStatus: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 165
    const/4 v15, 0x1

    goto/16 :goto_0

    .line 159
    .restart local v13    # "mSharedPreferences":Landroid/content/SharedPreferences;
    :cond_3
    const-string v18, "ContainerStateImpl(): FOTA case. License is Expired."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 160
    invoke-virtual {v7}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v18

    sget-object v19, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->EXPIRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->upgradeData(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 179
    .end local v3    # "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v7    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v9    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "mSharedPreferences":Landroid/content/SharedPreferences;
    .end local v15    # "success":Z
    :catch_0
    move-exception v10

    .line 180
    .local v10, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v18, "ContainerStateImpl():  Exception found"

    move-object/from16 v0, v18

    invoke-static {v0, v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 182
    invoke-virtual {v6}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 239
    .end local v10    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    .line 168
    .restart local v3    # "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v9    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v15    # "success":Z
    :cond_4
    if-eqz v15, :cond_5

    .line 169
    :try_start_2
    const-string v18, "ContainerStateImpl(): Activation record found and license is ACTIVE state."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 170
    const/16 v18, 0x5a

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 182
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v15    # "success":Z
    :goto_3
    invoke-virtual {v6}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_2

    .line 172
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v15    # "success":Z
    :cond_5
    :try_start_3
    const-string v18, "ContainerStateImpl(): ACtivation record found but license is INACTIVE state."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 173
    const/16 v18, 0x32

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 182
    .end local v3    # "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v9    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v15    # "success":Z
    :catchall_0
    move-exception v18

    invoke-virtual {v6}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v18

    .line 176
    .restart local v3    # "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v9    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_6
    :try_start_4
    const-string v18, "ContainerStateImpl(): No activation record is found when container license check came."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 177
    const/16 v18, 0xa

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 186
    .end local v3    # "activationRecord":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    .end local v9    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_7
    const-string v18, "ContainerStateImpl(): checkLicenseKeyStatus(): container_id != NULL."

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 188
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v6

    .line 191
    .restart local v6    # "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    :try_start_5
    const-string v18, "container_id"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 192
    .local v2, "ContainerId":I
    new-instance v4, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v4}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    .line 193
    .local v4, "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    .line 194
    .local v17, "whereArgs":[Ljava/lang/String;
    const-string v18, "CONTAINER_ID=?"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v6, v0, v4, v1}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 196
    .local v5, "containerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    if-eqz v5, :cond_c

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v18

    if-lez v18, :cond_c

    .line 197
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "ContainerStateImpl(): checkLicenseKeyStatus(): Found Container Record."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 198
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/klmsagent/beans/ContainerData;->getContainerStatus()Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 199
    const-string v18, "ContainerStateImpl(): KLMS_CONTAINER_EXPLICIT_LOCKED"

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 200
    const/16 v18, 0x32

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 235
    :goto_4
    invoke-virtual {v6}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 236
    const-string v18, "ContainerStateImpl(): checkLicenseKeyStatus().END"

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 202
    :cond_8
    const/16 v18, 0x0

    :try_start_6
    move/from16 v0, v18

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/klmsagent/beans/ContainerData;->getPackageName()Ljava/lang/String;

    move-result-object v14

    .line 203
    .local v14, "pkgName":Ljava/lang/String;
    new-instance v7, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v7}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 204
    .restart local v7    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v18, 0x0

    aput-object v14, v16, v18

    .line 205
    .local v16, "whereArg":[Ljava/lang/String;
    const-string v18, "PACKAGE_NAME=?"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v6, v0, v7, v1}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 208
    .local v8, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v8, :cond_a

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v18

    if-lez v18, :cond_a

    .line 209
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "ContainerStateImpl(): checkLicenseKeyStatus(): Found Activation Record."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 210
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_9

    invoke-static {v14}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->checkContainerPermission(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 211
    const/16 v18, 0x5a

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_4

    .line 232
    .end local v2    # "ContainerId":I
    .end local v4    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v5    # "containerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .end local v7    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v8    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v14    # "pkgName":Ljava/lang/String;
    .end local v16    # "whereArg":[Ljava/lang/String;
    .end local v17    # "whereArgs":[Ljava/lang/String;
    :catch_1
    move-exception v10

    .line 233
    .restart local v10    # "e":Ljava/lang/Exception;
    :try_start_7
    const-string v18, "ContainerStateImpl(): checkLicenseKeyStatus() has Exception."

    move-object/from16 v0, v18

    invoke-static {v0, v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 235
    invoke-virtual {v6}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 236
    const-string v18, "ContainerStateImpl(): checkLicenseKeyStatus().END"

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 213
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v2    # "ContainerId":I
    .restart local v4    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .restart local v5    # "containerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .restart local v7    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v8    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v14    # "pkgName":Ljava/lang/String;
    .restart local v16    # "whereArg":[Ljava/lang/String;
    .restart local v17    # "whereArgs":[Ljava/lang/String;
    :cond_9
    const/16 v18, 0x32

    :try_start_8
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_4

    .line 235
    .end local v2    # "ContainerId":I
    .end local v4    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v5    # "containerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .end local v7    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v8    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v14    # "pkgName":Ljava/lang/String;
    .end local v16    # "whereArg":[Ljava/lang/String;
    .end local v17    # "whereArgs":[Ljava/lang/String;
    :catchall_1
    move-exception v18

    invoke-virtual {v6}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 236
    const-string v19, "ContainerStateImpl(): checkLicenseKeyStatus().END"

    invoke-static/range {v19 .. v19}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    throw v18

    .line 216
    .restart local v2    # "ContainerId":I
    .restart local v4    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .restart local v5    # "containerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .restart local v7    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v8    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v14    # "pkgName":Ljava/lang/String;
    .restart local v16    # "whereArg":[Ljava/lang/String;
    .restart local v17    # "whereArgs":[Ljava/lang/String;
    :cond_a
    :try_start_9
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "ContainerStateImpl(): checkLicenseKeyStatus(): Can\'t Find Activation Record."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 217
    invoke-static {v14}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->checkContainerPermission(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 218
    const/16 v18, 0x5a

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V

    goto/16 :goto_4

    .line 220
    :cond_b
    const/16 v18, 0x32

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V

    goto/16 :goto_4

    .line 225
    .end local v7    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v8    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v14    # "pkgName":Ljava/lang/String;
    .end local v16    # "whereArg":[Ljava/lang/String;
    :cond_c
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "ContainerStateImpl(): checkLicenseKeyStatus(): Can\'t Find Container Record."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 226
    invoke-static {v2}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->getAdminPackage(I)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->checkContainerPermission(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_d

    .line 227
    const/16 v18, 0x5a

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V

    goto/16 :goto_4

    .line 229
    :cond_d
    const/16 v18, 0x32

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->SendIntentToContainer(ILjava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_4
.end method

.method public createContainerListener(Landroid/content/Intent;)V
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    .line 52
    const-string v7, "ContainerStateImpl(): createContainerListener().START"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 55
    .local v5, "extras":Landroid/os/Bundle;
    if-eqz v5, :cond_0

    const-string v7, "container_id"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_1

    .line 56
    :cond_0
    const-string v7, "ContainerStateImpl(): createContainerListener().container_id == NULL."

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 92
    :goto_0
    return-void

    .line 61
    :cond_1
    :try_start_0
    const-string v7, "container_id"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 62
    .local v1, "containerId":I
    invoke-static {v1}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->getAdminPackage(I)Ljava/lang/String;

    move-result-object v6

    .line 63
    .local v6, "pkgName":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ContainerStateImpl(): containerId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " | packageName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 66
    new-instance v0, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v0}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    .line 67
    .local v0, "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerID(Ljava/lang/String;)V

    .line 68
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/samsung/klmsagent/beans/ContainerData;->setTimestamp(Ljava/lang/String;)V

    .line 69
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/samsung/klmsagent/beans/ContainerData;->setContainerStatus(Ljava/lang/Integer;)V

    .line 70
    if-eqz v6, :cond_2

    .line 71
    invoke-virtual {v0, v6}, Lcom/samsung/klmsagent/beans/ContainerData;->setPackageName(Ljava/lang/String;)V

    .line 75
    :cond_2
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v3

    .line 76
    .local v3, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v7, 0x0

    new-instance v8, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    invoke-virtual {v3, v7, v8}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 77
    .local v2, "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 78
    :cond_3
    invoke-virtual {v3, v0}, Lcom/samsung/klmsagent/data/DataSource;->saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z

    move-result v7

    if-ne v7, v10, :cond_4

    .line 79
    const-string v7, "ContainerStateImpl(): Save Container ID and packageName in DB"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 86
    :cond_4
    :goto_1
    iget-object v7, p0, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    sget-object v8, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->CONTAINER_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    sget-object v9, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->MDM:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    invoke-virtual {v7, v0, v8, v9}, Lcom/samsung/klmsagent/services/KLMSServices;->validateRegister(Lcom/samsung/klmsagent/beans/KLMSComponent;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    const-string v7, "ContainerStateImpl(): createContainerListener().END"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 81
    :cond_5
    :try_start_1
    const-string v7, "CONTAINER_ID=?"

    invoke-virtual {v3, v7, v0}, Lcom/samsung/klmsagent/data/DataSource;->updateData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)I

    move-result v7

    if-nez v7, :cond_6

    .line 82
    invoke-virtual {v3, v0}, Lcom/samsung/klmsagent/data/DataSource;->saveData(Lcom/samsung/klmsagent/beans/KLMSComponent;)Z

    .line 83
    :cond_6
    const-string v7, "ContainerStateImpl(): Update Container ID and packageName in DB"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 87
    .end local v0    # "containerData":Lcom/samsung/klmsagent/beans/ContainerData;
    .end local v1    # "containerId":I
    .end local v2    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    .end local v3    # "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    .end local v6    # "pkgName":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 88
    .local v4, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v7, "createContainerListener() has Exception."

    invoke-static {v7, v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 90
    const-string v7, "ContainerStateImpl(): createContainerListener().END"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    const-string v8, "ContainerStateImpl(): createContainerListener().END"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    throw v7
.end method

.method public removeContainerListener(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 101
    const-string v4, "ContainerStateImpl(): removeContainerListener().START"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 104
    .local v2, "extras":Landroid/os/Bundle;
    if-eqz v2, :cond_0

    const-string v4, "container_id"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_1

    .line 105
    :cond_0
    const-string v4, "ContainerStateImpl(): removeContainerListener().container_id == NULL."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 122
    :goto_0
    return-void

    .line 109
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;->doGSLBJob()V

    .line 112
    :try_start_0
    const-string v4, "container_id"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 113
    .local v0, "containerId":I
    invoke-static {v0}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->getPackageNameUsingContainerId(I)Ljava/lang/String;

    move-result-object v3

    .line 114
    .local v3, "pkgName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ContainerStateImpl(): containerId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " | packageName: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 116
    iget-object v4, p0, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    invoke-virtual {v4, v0, v3}, Lcom/samsung/klmsagent/services/KLMSServices;->onDestroy(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    const-string v4, "ContainerStateImpl(): removeContainerListener().END"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .line 117
    .end local v0    # "containerId":I
    .end local v3    # "pkgName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 118
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v4, "removeContainerListener() has Exception."

    invoke-static {v4, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    const-string v4, "ContainerStateImpl(): removeContainerListener().END"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    const-string v5, "ContainerStateImpl(): removeContainerListener().END"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    throw v4
.end method

.method public uploadRPMode(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 426
    return-void
.end method
