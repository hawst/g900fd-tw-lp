.class public Lcom/samsung/klmsagent/services/i/StateImplV2;
.super Ljava/lang/Object;
.source "StateImplV2.java"

# interfaces
.implements Lcom/samsung/klmsagent/services/States;


# static fields
.field private static final TAG:Ljava/lang/String; = "StateImplV2(): "


# instance fields
.field private context:Landroid/content/Context;

.field private mDeviceHandler:Lcom/samsung/klmsagent/services/KLMSServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/klmsagent/services/KLMSServices",
            "<",
            "Lcom/samsung/klmsagent/beans/DeviceData;",
            ">;"
        }
    .end annotation
.end field

.field mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/klmsagent/services/KLMSServices;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/klmsagent/services/KLMSServices",
            "<",
            "Lcom/samsung/klmsagent/beans/DeviceData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p2, "mDeviceHandler":Lcom/samsung/klmsagent/services/KLMSServices;, "Lcom/samsung/klmsagent/services/KLMSServices<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/samsung/klmsagent/services/i/StateImplV2;->context:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lcom/samsung/klmsagent/services/i/StateImplV2;->mDeviceHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    .line 56
    return-void
.end method

.method public static setUpAlarm(I)V
    .locals 22
    .param p0, "activationId"    # I

    .prologue
    .line 289
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "StateImplV2(): setUpAlarm().activationId: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 290
    invoke-static/range {p0 .. p0}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->getAlarmTime(I)Ljava/util/Map;

    move-result-object v14

    .line 292
    .local v14, "resultMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v14, :cond_1

    .line 293
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "StateImplV2(): No activation record for id: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ". Not setting alaram."

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v17

    const-string v18, "alarm"

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/AlarmManager;

    .line 296
    .local v6, "am":Landroid/app/AlarmManager;
    new-instance v15, Landroid/content/Intent;

    sget-object v17, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->ALARM_INTENT:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 297
    .local v15, "retryIntent":Landroid/content/Intent;
    const-string v17, "_id"

    move-object/from16 v0, v17

    move/from16 v1, p0

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 298
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v17

    const/high16 v18, 0x8000000

    move-object/from16 v0, v17

    move/from16 v1, p0

    move/from16 v2, v18

    invoke-static {v0, v1, v15, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v16

    .line 303
    .local v16, "retryPendingIntent":Landroid/app/PendingIntent;
    const/4 v7, 0x0

    .line 304
    .local v7, "cal":Ljava/util/Calendar;
    const-string v17, "UTC"

    invoke-static/range {v17 .. v17}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v7

    .line 305
    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 306
    .local v8, "currentTime":J
    const-string v17, "NEXT_VALIDATION"

    move-object/from16 v0, v17

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 308
    .local v12, "nextValidation":J
    const-string v17, "ALARM_TIME"

    move-object/from16 v0, v17

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    if-nez v17, :cond_2

    .line 310
    const-string v17, "StateImplV2(): It\'s the first time activation."

    invoke-static/range {v17 .. v17}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 311
    add-long v18, v8, v12

    move/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->setAlarmTime(IJ)V

    .line 313
    const/16 v17, 0x0

    add-long v18, v8, v12

    move/from16 v0, v17

    move-wide/from16 v1, v18

    move-object/from16 v3, v16

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 314
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "StateImplV2(): Next Alarm : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    new-instance v18, Ljava/util/Date;

    add-long v20, v8, v12

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 316
    :cond_2
    const-string v17, "ALARM_TIME"

    move-object/from16 v0, v17

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    if-eqz v17, :cond_0

    .line 317
    const-string v17, "ALARM_TIME"

    move-object/from16 v0, v17

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 318
    .local v4, "alarmTime":J
    sub-long v18, v4, v8

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->abs(J)J

    move-result-wide v10

    .line 320
    .local v10, "duration":J
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "StateImplV2(): Checking Time Diff between Alarm Time and Current Time : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 321
    cmp-long v17, v10, v12

    if-lez v17, :cond_3

    .line 322
    const-string v17, "StateImplV2(): The duration elasped is more than nextValidation time."

    invoke-static/range {v17 .. v17}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 324
    move/from16 v0, p0

    invoke-static {v0, v8, v9}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->setAlarmTime(IJ)V

    .line 325
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v6, v0, v8, v9, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 327
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "StateImplV2(): Next Alarm : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    new-instance v18, Ljava/util/Date;

    move-object/from16 v0, v18

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 329
    :cond_3
    const-string v17, "StateImplV2(): The duration elasped is less than nextValidation time."

    invoke-static/range {v17 .. v17}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 330
    const/16 v17, 0x0

    add-long v18, v8, v10

    move/from16 v0, v17

    move-wide/from16 v1, v18

    move-object/from16 v3, v16

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 331
    add-long v18, v8, v10

    move/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->setAlarmTime(IJ)V

    .line 332
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "StateImplV2(): Next Alarm : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    new-instance v18, Ljava/util/Date;

    add-long v20, v8, v10

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public bootCompleteAction()V
    .locals 9

    .prologue
    .line 62
    const-string v6, "StateImplV2(): bootCompleteAction().STRAT"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 64
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 66
    .local v0, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v6, 0x0

    :try_start_0
    new-instance v7, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v7}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    invoke-virtual {v0, v6, v7}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 68
    .local v2, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 69
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 70
    .local v1, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "StateImplV2(): setting alarm for activation id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 71
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->getAlarmTime()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->getAlarmTime()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 72
    :cond_0
    const-string v6, "StateImplV2():  bootCompleteAction(): after Fota update. Set Up DB for multi license case."

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 75
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "klmsagent.preferences"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 77
    .local v5, "mSharedPreferences":Landroid/content/SharedPreferences;
    const-string v6, "KLMS_LICENSE_STATUS"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-nez v6, :cond_2

    .line 78
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v6

    sget-object v7, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual {v7}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v7

    invoke-static {v6, v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->upgradeData(II)V

    .line 84
    .end local v5    # "mSharedPreferences":Landroid/content/SharedPreferences;
    :cond_1
    :goto_1
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v6

    invoke-static {v6}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->disableAlarm(I)V

    .line 85
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v6

    invoke-static {v6}, Lcom/samsung/klmsagent/services/i/StateImplV2;->setUpAlarm(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 91
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v3

    .line 92
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v6, "StateImplV2(): bootCompleteAction() has Exception."

    invoke-static {v6, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 95
    const-string v6, "StateImplV2(): bootCompleteAction().END"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 97
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    .line 80
    .restart local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v5    # "mSharedPreferences":Landroid/content/SharedPreferences;
    :cond_2
    :try_start_2
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v6

    sget-object v7, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->EXPIRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual {v7}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->getCode()I

    move-result v7

    invoke-static {v6, v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->upgradeData(II)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 94
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "mSharedPreferences":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v6

    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 95
    const-string v7, "StateImplV2(): bootCompleteAction().END"

    invoke-static {v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    throw v6

    .line 90
    .restart local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :cond_3
    :try_start_3
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f060032

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSUtility;->showNotification(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 94
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 95
    const-string v6, "StateImplV2(): bootCompleteAction().END"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public dateTimeChanged()V
    .locals 12

    .prologue
    .line 249
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 250
    .local v6, "mCurrentTimeMillis":J
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "StateImplV2(): dateTimeChanged().START : "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 252
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v0

    .line 254
    .local v0, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v5, 0x0

    :try_start_0
    new-instance v8, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v8}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    invoke-virtual {v0, v5, v8}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 256
    .local v2, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 257
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 258
    .local v1, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->getAlarmTime()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->getAlarmTime()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 259
    :cond_0
    const-string v5, "StateImplV2():  dateTimeChanged(): after Fota update. Setting alarm"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 260
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->disableAlarm(I)V

    .line 261
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/klmsagent/services/i/StateImplV2;->setUpAlarm(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 275
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v3

    .line 276
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v5, "StateImplV2(): dateTimeChanged() has Exception."

    invoke-static {v5, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 278
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 279
    const-string v5, "StateImplV2(): dateTimeChanged().END"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 281
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 262
    .restart local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_2
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->getAlarmTime()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-gez v5, :cond_2

    .line 263
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "StateImplV2():  set alarm: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " | Alarm time: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v8, Ljava/util/Date;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->getAlarmTime()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " | Current Time: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 266
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->disableAlarm(I)V

    .line 267
    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/klmsagent/services/i/StateImplV2;->setUpAlarm(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 278
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 279
    const-string v8, "StateImplV2(): dateTimeChanged().END"

    invoke-static {v8}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    throw v5

    .line 269
    .restart local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "StateImplV2():  No need to set alarm: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " | Alarm time: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v8, Ljava/util/Date;

    invoke-virtual {v1}, Lcom/samsung/klmsagent/beans/DeviceData;->getAlarmTime()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " | Current Time: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 278
    .end local v1    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 279
    const-string v5, "StateImplV2(): dateTimeChanged().END"

    invoke-static {v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public deactivateLicense(ILjava/lang/String;)V
    .locals 1
    .param p1, "initiator"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 373
    const-string v0, "StateImplV2(): deactivateLicense()"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 374
    iget-object v0, p0, Lcom/samsung/klmsagent/services/i/StateImplV2;->mDeviceHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/klmsagent/services/KLMSServices;->onDestroy(ILjava/lang/String;)V

    .line 375
    return-void
.end method

.method public mdmValidateLicenseKey(Landroid/content/Intent;)V
    .locals 15
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    .line 100
    const-string v0, "StateImplV2(): mdmValidateLicenseKey().STRAT"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 103
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    .line 104
    .local v9, "extras":Landroid/os/Bundle;
    if-nez v9, :cond_0

    .line 105
    const-string v0, "StateImplV2(): mdmValidateLicenseKey(): intent.getExtras() == NULL"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 231
    :goto_0
    return-void

    .line 109
    :cond_0
    const-string v0, "packageName"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 110
    .local v5, "packageName":Ljava/lang/String;
    const-string v0, "license"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 111
    .local v11, "licenseKey":Ljava/lang/String;
    const/4 v12, 0x0

    .line 114
    .local v12, "onPremSet":Z
    iget-object v0, p0, Lcom/samsung/klmsagent/services/i/StateImplV2;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSUtility;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 115
    const-string v0, "StateImplV2(): mdmValidateLicenseKey(): No Network."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 116
    new-instance v14, Lcom/samsung/klmsagent/context/State;

    const/16 v0, 0x270f

    invoke-direct {v14, v0}, Lcom/samsung/klmsagent/context/State;-><init>(I)V

    .line 117
    .local v14, "state":Lcom/samsung/klmsagent/context/State;
    invoke-virtual {v14}, Lcom/samsung/klmsagent/context/State;->takeActionGeneral()V

    .line 118
    const/16 v0, 0xc8

    const/16 v1, 0x270f

    const/16 v2, 0x384

    const/16 v3, 0x320

    const-string v4, "fail"

    invoke-static/range {v0 .. v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 125
    .end local v14    # "state":Lcom/samsung/klmsagent/context/State;
    :cond_1
    if-eqz v11, :cond_4

    invoke-static {v11}, Lcom/samsung/klmsagent/util/KLMSUtility;->checkForOnPremiseDetails(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 126
    const-string v0, "StateImplV2(): mdmValidateLicenseKey(): OnPremise KNOX License Pushed."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 127
    const/4 v12, 0x1

    .line 139
    :goto_1
    new-instance v13, Landroid/content/Intent;

    invoke-direct {v13}, Landroid/content/Intent;-><init>()V

    .line 141
    .local v13, "originalIntent":Landroid/content/Intent;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/klmsagent/services/i/StateImplV2;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "klm_eula_shown"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    .line 142
    .local v8, "eulaForUMC":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StateImplV2(): Registering Device and validating license key. eulaForUMC: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 145
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->isEulaAccepted()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    .line 146
    if-nez v12, :cond_2

    const/4 v0, 0x1

    if-eq v8, v0, :cond_2

    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getMDMforUMCState()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 147
    :cond_2
    const-string v0, "StateImplV2(): mdmValidateLicenseKey() : EULA won\'t not displayed."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 148
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 149
    .local v10, "i":Landroid/content/Intent;
    const-string v0, "action"

    const-string v1, "mdm"

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    if-eqz v12, :cond_3

    .line 151
    const-string v0, "onPrem"

    const/4 v1, 0x1

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 153
    :cond_3
    const-string v0, "action"

    const-string v1, "mdm"

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    const-string v0, "initiator"

    const-string v1, "mdm"

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    const-string v0, "packageName"

    invoke-virtual {v10, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    const-string v0, "license"

    invoke-virtual {v10, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    const-string v0, "intentOriginal"

    invoke-virtual {v10, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 158
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponse(Ljava/lang/Boolean;)V

    .line 159
    invoke-virtual {v10}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "b2b"

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->setKLMSServerAddr(Landroid/os/Bundle;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    :goto_2
    const-string v0, "StateImplV2(): mdmValidateLicenseKey().END"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 129
    .end local v8    # "eulaForUMC":I
    .end local v10    # "i":Landroid/content/Intent;
    .end local v13    # "originalIntent":Landroid/content/Intent;
    :cond_4
    invoke-static {}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->checkIfOnPrem()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 130
    const-string v0, "Normal key came. But on prem already activated returning."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 131
    const/16 v0, 0xc8

    const/16 v1, 0xc1f

    const/16 v2, 0x384

    const/16 v3, 0x320

    const-string v4, "fail"

    invoke-static/range {v0 .. v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 136
    :cond_5
    const-string v0, "StateImplV2(): mdmValidateLicenseKey(): Normal KNOX License Pushed."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 161
    .restart local v8    # "eulaForUMC":I
    .restart local v13    # "originalIntent":Landroid/content/Intent;
    :cond_6
    :try_start_1
    const-string v0, "StateImplV2(): mdmValidateLicenseKey() : EULA will be displayed."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 162
    new-instance v10, Landroid/content/Intent;

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/samsung/klmsagent/useragreement/ConfirmDialog;

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    .restart local v10    # "i":Landroid/content/Intent;
    const-string v0, "action"

    const-string v1, "mdm"

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    const-string v0, "action"

    const-string v1, "mdm"

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    const-string v0, "initiator"

    const-string v1, "mdm"

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 166
    const-string v0, "packageName"

    invoke-virtual {v10, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    const-string v0, "license"

    invoke-virtual {v10, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 168
    const-string v0, "intentOriginal"

    invoke-virtual {v10, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 169
    const/high16 v0, 0x10000000

    invoke-virtual {v10, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 170
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 226
    .end local v8    # "eulaForUMC":I
    .end local v10    # "i":Landroid/content/Intent;
    :catch_0
    move-exception v7

    .line 227
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v0, "mdmValidateLicenseKey() has Exception"

    invoke-static {v0, v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 229
    const-string v0, "StateImplV2(): mdmValidateLicenseKey().END"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 173
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v8    # "eulaForUMC":I
    :cond_7
    if-eqz v12, :cond_8

    .line 174
    :try_start_3
    const-string v0, "StateImplV2(): mdmValidateLicenseKey() : EULA is not displayed."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 175
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 176
    .restart local v10    # "i":Landroid/content/Intent;
    const-string v0, "action"

    const-string v1, "mdm"

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    const-string v0, "onPrem"

    const/4 v1, 0x1

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 178
    const-string v0, "action"

    const-string v1, "mdm"

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    const-string v0, "initiator"

    const-string v1, "mdm"

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 180
    const-string v0, "packageName"

    invoke-virtual {v10, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    const-string v0, "license"

    invoke-virtual {v10, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    const-string v0, "intentOriginal"

    invoke-virtual {v10, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 183
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponse(Ljava/lang/Boolean;)V

    .line 184
    invoke-virtual {v10}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "b2b"

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->setKLMSServerAddr(Landroid/os/Bundle;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 229
    const-string v0, "StateImplV2(): mdmValidateLicenseKey().END"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 187
    .end local v10    # "i":Landroid/content/Intent;
    :cond_8
    :try_start_4
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->isEulaAccepted()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    .line 188
    if-eqz v12, :cond_9

    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    .line 189
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 190
    .restart local v10    # "i":Landroid/content/Intent;
    const-string v0, "action"

    const-string v1, "mdm"

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 191
    const-string v0, "onPrem"

    const/4 v1, 0x1

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 192
    const-string v0, "action"

    const-string v1, "mdm"

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    const-string v0, "initiator"

    const-string v1, "mdm"

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    const-string v0, "packageName"

    invoke-virtual {v10, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    const-string v0, "license"

    invoke-virtual {v10, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    const-string v0, "intentOriginal"

    invoke-virtual {v10, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 197
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponse(Ljava/lang/Boolean;)V

    .line 198
    invoke-virtual {v10}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "b2b"

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->setKLMSServerAddr(Landroid/os/Bundle;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 229
    const-string v0, "StateImplV2(): mdmValidateLicenseKey().END"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 200
    .end local v10    # "i":Landroid/content/Intent;
    :cond_9
    :try_start_5
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getKLMSServerAddr()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_a

    .line 201
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 202
    .restart local v10    # "i":Landroid/content/Intent;
    const-string v0, "action"

    const-string v1, "mdm"

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 203
    const-string v0, "onPrem"

    const/4 v1, 0x0

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 204
    const-string v0, "action"

    const-string v1, "mdm"

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 205
    const-string v0, "initiator"

    const-string v1, "mdm"

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    const-string v0, "packageName"

    invoke-virtual {v10, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    const-string v0, "license"

    invoke-virtual {v10, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    const-string v0, "intentOriginal"

    invoke-virtual {v10, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 209
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponse(Ljava/lang/Boolean;)V

    .line 210
    invoke-virtual {v10}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "b2b"

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->setKLMSServerAddr(Landroid/os/Bundle;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 229
    const-string v0, "StateImplV2(): mdmValidateLicenseKey().END"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 214
    .end local v10    # "i":Landroid/content/Intent;
    :cond_a
    :try_start_6
    new-instance v6, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v6}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 215
    .local v6, "_d":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v6, v11}, Lcom/samsung/klmsagent/beans/DeviceData;->setLicenseKey(Ljava/lang/String;)V

    .line 216
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setTime(Ljava/lang/String;)V

    .line 217
    invoke-virtual {v6, v5}, Lcom/samsung/klmsagent/beans/DeviceData;->setPackageName(Ljava/lang/String;)V

    .line 218
    if-eqz v12, :cond_b

    .line 219
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setOnPremInfo(I)V

    .line 223
    :goto_3
    new-instance v0, Lcom/samsung/klmsagent/services/i/DeviceServices;

    invoke-direct {v0}, Lcom/samsung/klmsagent/services/i/DeviceServices;-><init>()V

    iput-object v0, p0, Lcom/samsung/klmsagent/services/i/StateImplV2;->mDeviceHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    .line 224
    iget-object v0, p0, Lcom/samsung/klmsagent/services/i/StateImplV2;->mDeviceHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    sget-object v1, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->DEVICE_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    sget-object v2, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->MDM:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    invoke-virtual {v0, v6, v1, v2}, Lcom/samsung/klmsagent/services/KLMSServices;->validateRegister(Lcom/samsung/klmsagent/beans/KLMSComponent;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 225
    const/4 v12, 0x0

    .line 229
    const-string v0, "StateImplV2(): mdmValidateLicenseKey().END"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 221
    :cond_b
    const/4 v0, 0x2

    :try_start_7
    invoke-virtual {v6, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->setOnPremInfo(I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    .line 229
    .end local v6    # "_d":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v8    # "eulaForUMC":I
    :catchall_0
    move-exception v0

    const-string v1, "StateImplV2(): mdmValidateLicenseKey().END"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    throw v0
.end method

.method public networkChangeListener()V
    .locals 2

    .prologue
    .line 234
    iget-object v1, p0, Lcom/samsung/klmsagent/services/i/StateImplV2;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSUtility;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 235
    const-string v1, "StateImplV2(): networkChangeListener().START"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 236
    new-instance v0, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;

    invoke-direct {v0}, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;-><init>()V

    .line 237
    .local v0, "changeOperations":Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;
    invoke-virtual {v0}, Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;->doNetworkJob()V

    .line 239
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremiseGSLBUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getOnPremisePolicy()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 241
    const-string v1, "StateImplV2(): on premise policy is not set, calling on premise policy gslb."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 242
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/klmsagent/http/KLMSGSLBManager;->setKLMServerPolicy(Landroid/os/Bundle;)V

    .line 245
    .end local v0    # "changeOperations":Lcom/samsung/klmsagent/services/i/NetworkChangeOperations;
    :cond_0
    const-string v1, "StateImplV2(): networkChangeListener().END"

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 246
    return-void
.end method

.method public validateEnrollDevice(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "extBundle"    # Landroid/os/Bundle;

    .prologue
    .line 339
    const-string v6, "StateImplV2(): validateEnrollDevice()"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 342
    :try_start_0
    const-string v6, "license"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 343
    .local v4, "licenseKey":Ljava/lang/String;
    const-string v6, "initiator"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 344
    .local v3, "initiator":Ljava/lang/String;
    const-string v6, "packageName"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 345
    .local v5, "packageName":Ljava/lang/String;
    const-string v6, "action"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 347
    .local v1, "action":Ljava/lang/String;
    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    if-nez v5, :cond_1

    .line 348
    :cond_0
    const-string v6, "StateImplV2(): validateEnrollDevice(): Values are NULL."

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 369
    .end local v1    # "action":Ljava/lang/String;
    .end local v3    # "initiator":Ljava/lang/String;
    .end local v4    # "licenseKey":Ljava/lang/String;
    .end local v5    # "packageName":Ljava/lang/String;
    :goto_0
    return-void

    .line 352
    .restart local v1    # "action":Ljava/lang/String;
    .restart local v3    # "initiator":Ljava/lang/String;
    .restart local v4    # "licenseKey":Ljava/lang/String;
    .restart local v5    # "packageName":Ljava/lang/String;
    :cond_1
    new-instance v0, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v0}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 353
    .local v0, "_d":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v0, v4}, Lcom/samsung/klmsagent/beans/DeviceData;->setLicenseKey(Ljava/lang/String;)V

    .line 354
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/samsung/klmsagent/beans/DeviceData;->setTime(Ljava/lang/String;)V

    .line 355
    invoke-virtual {v0, v5}, Lcom/samsung/klmsagent/beans/DeviceData;->setPackageName(Ljava/lang/String;)V

    .line 356
    const-string v6, "onPrem"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 357
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/samsung/klmsagent/beans/DeviceData;->setOnPremInfo(I)V

    .line 362
    :goto_1
    iget-object v6, p0, Lcom/samsung/klmsagent/services/i/StateImplV2;->mDeviceHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    sget-object v7, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->DEVICE_REGISTER:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    sget-object v8, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;->MDM:Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    invoke-virtual {v6, v0, v7, v8}, Lcom/samsung/klmsagent/services/KLMSServices;->validateRegister(Lcom/samsung/klmsagent/beans/KLMSComponent;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V
    :try_end_0
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 363
    .end local v0    # "_d":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v1    # "action":Ljava/lang/String;
    .end local v3    # "initiator":Ljava/lang/String;
    .end local v4    # "licenseKey":Ljava/lang/String;
    .end local v5    # "packageName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 365
    .local v2, "e":Lorg/apache/http/MethodNotSupportedException;
    const-string v6, "StateImplV2(): MethodNotSupportedException found"

    invoke-static {v6, v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 359
    .end local v2    # "e":Lorg/apache/http/MethodNotSupportedException;
    .restart local v0    # "_d":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v1    # "action":Ljava/lang/String;
    .restart local v3    # "initiator":Ljava/lang/String;
    .restart local v4    # "licenseKey":Ljava/lang/String;
    .restart local v5    # "packageName":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x2

    :try_start_1
    invoke-virtual {v0, v6}, Lcom/samsung/klmsagent/beans/DeviceData;->setOnPremInfo(I)V
    :try_end_1
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 366
    .end local v0    # "_d":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v1    # "action":Ljava/lang/String;
    .end local v3    # "initiator":Ljava/lang/String;
    .end local v4    # "licenseKey":Ljava/lang/String;
    .end local v5    # "packageName":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 367
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "StateImplV2(): Exception found"

    invoke-static {v6, v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
