.class public Lcom/samsung/klmsagent/listner/MainReciver;
.super Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;
.source "MainReciver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/listner/MainReciver$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MainReciver(): "


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;-><init>()V

    .line 86
    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    .line 21
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 22
    .local v4, "startTime":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MainReciver(): EventNotificationReciver-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " method called: %s , timestamp: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "AppActivityListner onReceive for Intent Action : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    const/4 v9, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 27
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 28
    .local v1, "intentAction":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 29
    const-string v6, "MainReciver(): Recieved Intent is null, Can\'t process."

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    invoke-static {}, Lcom/samsung/klmsagent/listner/MainReciver;->getActionMapper()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    .line 33
    .local v0, "action":Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;
    if-nez v0, :cond_2

    .line 34
    const-string v6, "MainReciver(): No intent found"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 38
    :cond_2
    sget-object v6, Lcom/samsung/klmsagent/listner/MainReciver$1;->$SwitchMap$com$samsung$klmsagent$listner$KLMSAbstractReciever$BroadcastAction:[I

    invoke-virtual {v0}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 85
    const-string v6, "MainReciver(): Action not mapped to any Handler."

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 40
    :pswitch_0
    const-string v6, "MainReciver(): BOOT_COMPLETED"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 41
    invoke-virtual {p0}, Lcom/samsung/klmsagent/listner/MainReciver;->getStatesHandler()Lcom/samsung/klmsagent/services/States;

    move-result-object v6

    invoke-interface {v6}, Lcom/samsung/klmsagent/services/States;->bootCompleteAction()V

    goto :goto_0

    .line 45
    :pswitch_1
    const-string v6, "MainReciver(): TIME_CHANGED"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0}, Lcom/samsung/klmsagent/listner/MainReciver;->getStatesHandler()Lcom/samsung/klmsagent/services/States;

    move-result-object v6

    invoke-interface {v6}, Lcom/samsung/klmsagent/services/States;->dateTimeChanged()V

    goto :goto_0

    .line 50
    :pswitch_2
    const-string v6, "MainReciver(): NETWORK_STATE_CHANGED_ACTION"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0}, Lcom/samsung/klmsagent/listner/MainReciver;->getStatesHandler()Lcom/samsung/klmsagent/services/States;

    move-result-object v6

    invoke-interface {v6}, Lcom/samsung/klmsagent/services/States;->networkChangeListener()V

    goto :goto_0

    .line 55
    :pswitch_3
    const-string v6, "MainReciver(): PACKAGE_REMOVED"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_4

    .line 57
    :cond_3
    const-string v6, "MainReciver(): intent.getExtras() == NULL"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_4
    const-string v6, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v6, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 62
    .local v3, "replacing":Z
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    .line 64
    .local v2, "pkgName":Ljava/lang/String;
    if-nez v2, :cond_5

    .line 65
    const-string v6, "MainReciver(): PackageName == NULL"

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MainReciver(): REPLACING: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | pkgName: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 71
    if-nez v3, :cond_0

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSUtility;->IsPackageExistInDevice(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 72
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->deleteNotificationAppList(Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getNotificationAppList()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_6

    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getNotificationAppList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 75
    :cond_6
    const-string v6, "MainReciver(): Notification App List is Null. Remove Notification."

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 76
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSUtility;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v6

    sget v7, Lcom/samsung/klmsagent/util/KLMSConstant;->NOTIFICATION_STACK_ID:I

    invoke-virtual {v6, v7}, Landroid/app/NotificationManager;->cancel(I)V

    .line 78
    :cond_7
    invoke-static {v2}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->IsPackageExistInDevice(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/samsung/klmsagent/listner/MainReciver;->getStatesHandler()Lcom/samsung/klmsagent/services/States;

    move-result-object v6

    const/4 v7, -0x1

    invoke-interface {v6, v7, v2}, Lcom/samsung/klmsagent/services/States;->deactivateLicense(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 38
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
