.class public Lcom/samsung/klmsagent/listner/MDMReceiver;
.super Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;
.source "MDMReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/listner/MDMReceiver$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MDMReceiver(): "


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;-><init>()V

    .line 137
    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 24
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 25
    .local v20, "startTime":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MDMReceiver(): EventNotificationReciver-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " method called: %s , timestamp: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AppActivityListner onReceive for Intent Action : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 30
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v17

    .line 31
    .local v17, "intentAction":Ljava/lang/String;
    if-nez v17, :cond_0

    .line 32
    const-string v2, "MDMReceiver(): Recieved Intent is null, Can\'t process."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 139
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-static {}, Lcom/samsung/klmsagent/listner/MDMReceiver;->getActionMapper()Ljava/util/Map;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    .line 36
    .local v14, "action":Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;
    if-nez v14, :cond_1

    .line 37
    const-string v2, "MDMReceiver(): No intent found"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_1
    const/16 v18, 0x0

    .line 42
    .local v18, "knoxEnterpriseLicenseManager":Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;
    const/16 v19, 0x0

    .line 43
    .local v19, "licenseKey":Ljava/lang/String;
    const/4 v7, 0x0

    .line 44
    .local v7, "packageName":Ljava/lang/String;
    const/4 v15, 0x0

    .line 46
    .local v15, "extras":Landroid/os/Bundle;
    sget-object v2, Lcom/samsung/klmsagent/listner/MDMReceiver$1;->$SwitchMap$com$samsung$klmsagent$listner$KLMSAbstractReciever$BroadcastAction:[I

    invoke-virtual {v14}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 136
    const-string v2, "MDMReceiver(): Action not mapped to any Handler."

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :pswitch_0
    const-string v2, "MDMReceiver(): MDM_LICENSE_KEY"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 50
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v15

    .line 51
    if-nez v15, :cond_2

    .line 52
    const-string v2, "MDMReceiver(): intent.getExtras() == NULL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_2
    const-string v2, "edm.intent.extra.knox_license.data.pkgname"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 57
    if-nez v7, :cond_3

    .line 58
    const-string v2, "MDMReceiver(): PackageName == NULL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 59
    const/16 v2, 0xc8

    const/16 v3, 0xc1f

    const/16 v4, 0x384

    const/16 v5, 0x320

    const-string v6, "fail"

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 65
    :cond_3
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    move-result-object v18

    .line 66
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getKLMLicenseKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 67
    if-eqz v19, :cond_4

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, ""

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 68
    :cond_4
    const-string v2, "MDMReceiver(): LicenseKey == NULL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 69
    const/16 v2, 0xc8

    const/16 v3, 0xc1f

    const/16 v4, 0x384

    const/16 v5, 0x320

    const-string v6, "fail"

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 74
    :cond_5
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    const-string v3, "KLMS_MDM_PUSH_FROM_NORMAL"

    invoke-virtual {v2, v3}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setMDMPUSHFrom(Ljava/lang/String;)V

    .line 75
    const-string v2, "packageName"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    const-string v2, "license"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/klmsagent/listner/MDMReceiver;->getStatesHandler()Lcom/samsung/klmsagent/services/States;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Lcom/samsung/klmsagent/services/States;->mdmValidateLicenseKey(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 81
    :pswitch_1
    const-string v2, "MDMReceiver(): MDM_LICENSE_KEY_UMC"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 83
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v15

    .line 84
    if-nez v15, :cond_6

    .line 85
    const-string v2, "MDMReceiver(): intent.getExtras() == NULL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 89
    :cond_6
    const-string v2, "edm.intent.extra.knox_license.data.pkgname"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 90
    if-nez v7, :cond_7

    .line 91
    const-string v2, "MDMReceiver(): PackageName == NULL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 92
    const/16 v2, 0xc8

    const/16 v3, 0xc1f

    const/16 v4, 0x384

    const/16 v5, 0x320

    const-string v6, "fail"

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 98
    :cond_7
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    move-result-object v18

    .line 99
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getKLMLicenseKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 100
    if-eqz v19, :cond_8

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    const-string v2, ""

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 101
    :cond_8
    const-string v2, "MDMReceiver(): LicenseKey == NULL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 102
    const/16 v2, 0xc8

    const/16 v3, 0xc1f

    const/16 v4, 0x384

    const/16 v5, 0x320

    const-string v6, "fail"

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 107
    :cond_9
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setMDMforUMCState(Z)V

    .line 108
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v2

    const-string v3, "KLMS_MDM_PUSH_FROM_UMC"

    invoke-virtual {v2, v3}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setMDMPUSHFrom(Ljava/lang/String;)V

    .line 109
    const-string v2, "packageName"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    const-string v2, "license"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/klmsagent/listner/MDMReceiver;->getStatesHandler()Lcom/samsung/klmsagent/services/States;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Lcom/samsung/klmsagent/services/States;->mdmValidateLicenseKey(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 115
    :pswitch_2
    const-string v2, "MDMReceiver(): MDM_LICENSE_DEACTIVATE"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 116
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v16

    .line 117
    .local v16, "extrasDeactivate":Landroid/os/Bundle;
    if-nez v16, :cond_a

    .line 118
    const-string v2, "MDMReceiver(): intent.getExtras() == NULL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 122
    :cond_a
    const-string v2, "edm.intent.extra.knox_license.data.pkgname"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 124
    .local v13, "packageNameDeactivate":Ljava/lang/String;
    if-nez v13, :cond_b

    .line 125
    const-string v2, "MDMReceiver(): PackageName == NULL"

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 126
    const/16 v8, 0xc8

    const/16 v9, 0x2bf

    const/4 v10, -0x1

    const/16 v11, 0x322

    const-string v12, "fail"

    invoke-static/range {v8 .. v13}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 131
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/klmsagent/listner/MDMReceiver;->getStatesHandler()Lcom/samsung/klmsagent/services/States;

    move-result-object v2

    const/16 v3, 0x3e8

    invoke-interface {v2, v3, v13}, Lcom/samsung/klmsagent/services/States;->deactivateLicense(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
