.class public Lcom/samsung/klmsagent/listner/ContainerReceiver;
.super Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;
.source "ContainerReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/listner/ContainerReceiver$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ContainerReceiver(): "


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;-><init>()V

    .line 60
    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 18
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 19
    .local v2, "startTime":J
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ContainerReceiver(): EventNotificationReciver-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " method called: %s , timestamp: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "AppActivityListner onReceive for Intent Action : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 24
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 25
    .local v1, "intentAction":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 26
    const-string v4, "ContainerReceiver(): Recieved Intent is null, Can\'t process."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 62
    :goto_0
    return-void

    .line 29
    :cond_0
    invoke-static {}, Lcom/samsung/klmsagent/listner/ContainerReceiver;->getActionMapper()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    .line 30
    .local v0, "action":Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;
    if-nez v0, :cond_1

    .line 31
    const-string v4, "ContainerReceiver(): No intent found"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 35
    :cond_1
    sget-object v4, Lcom/samsung/klmsagent/listner/ContainerReceiver$1;->$SwitchMap$com$samsung$klmsagent$listner$KLMSAbstractReciever$BroadcastAction:[I

    invoke-virtual {v0}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 59
    const-string v4, "ContainerReceiver(): Action not mapped to any Handler."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 38
    :pswitch_0
    const-string v4, "ContainerReceiver(): CREATED_CONTAINER"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0}, Lcom/samsung/klmsagent/listner/ContainerReceiver;->getContainerState()Lcom/samsung/klmsagent/services/ContainerStates;

    move-result-object v4

    invoke-interface {v4, p2}, Lcom/samsung/klmsagent/services/ContainerStates;->createContainerListener(Landroid/content/Intent;)V

    goto :goto_0

    .line 44
    :pswitch_1
    const-string v4, "ContainerReceiver(): REMOVED_CONTAINER"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/samsung/klmsagent/listner/ContainerReceiver;->getContainerState()Lcom/samsung/klmsagent/services/ContainerStates;

    move-result-object v4

    invoke-interface {v4, p2}, Lcom/samsung/klmsagent/services/ContainerStates;->removeContainerListener(Landroid/content/Intent;)V

    goto :goto_0

    .line 49
    :pswitch_2
    const-string v4, "ContainerReceiver(): LICENSE_KEY_CHECK"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/samsung/klmsagent/listner/ContainerReceiver;->getContainerState()Lcom/samsung/klmsagent/services/ContainerStates;

    move-result-object v4

    invoke-interface {v4, p2}, Lcom/samsung/klmsagent/services/ContainerStates;->checkLicenseKeyStatus(Landroid/content/Intent;)V

    goto :goto_0

    .line 54
    :pswitch_3
    const-string v4, "ContainerReceiver(): RP_MODE_NOTIFY"

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lcom/samsung/klmsagent/listner/ContainerReceiver;->getContainerState()Lcom/samsung/klmsagent/services/ContainerStates;

    move-result-object v4

    invoke-interface {v4, p2}, Lcom/samsung/klmsagent/services/ContainerStates;->uploadRPMode(Landroid/content/Intent;)V

    goto :goto_0

    .line 35
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
