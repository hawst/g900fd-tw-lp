.class public final enum Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;
.super Ljava/lang/Enum;
.source "KLMSAbstractReciever.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "BroadcastAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum BOOT_COMPLETED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum CONTAINER_CREATED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum CONTAINER_CREATE_B2C:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum CONTAINER_LICENSE_CHECK:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum CONTAINER_REMOVED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum ELM_LICENSE_CHECK:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum ELM_PERMISSION_CHECK:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum MDM_LICENSE_DEACTIVATE:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum MDM_LICENSE_KEY:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum MDM_LICENSE_KEY_UMC:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum NETWORK_STATE_CHANGED_ACTION:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum PACKAGE_REMOVED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum RP_MODE_NOTIFY:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

.field public static final enum TIME_CHANGED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 51
    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "MDM_LICENSE_KEY"

    invoke-direct {v0, v1, v3}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->MDM_LICENSE_KEY:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "MDM_LICENSE_DEACTIVATE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->MDM_LICENSE_DEACTIVATE:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "MDM_LICENSE_KEY_UMC"

    invoke-direct {v0, v1, v5}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->MDM_LICENSE_KEY_UMC:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "CONTAINER_LICENSE_CHECK"

    invoke-direct {v0, v1, v6}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_LICENSE_CHECK:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "CONTAINER_CREATED"

    invoke-direct {v0, v1, v7}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_CREATED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "CONTAINER_REMOVED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_REMOVED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "CONTAINER_CREATE_B2C"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_CREATE_B2C:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "CONTAINER_REMOVED_B2C"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "ELM_LICENSE_CHECK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->ELM_LICENSE_CHECK:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "ELM_PERMISSION_CHECK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->ELM_PERMISSION_CHECK:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "BOOT_COMPLETED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->BOOT_COMPLETED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "NETWORK_STATE_CHANGED_ACTION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->NETWORK_STATE_CHANGED_ACTION:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "TIME_CHANGED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->TIME_CHANGED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "PACKAGE_REMOVED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->PACKAGE_REMOVED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    const-string v1, "RP_MODE_NOTIFY"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->RP_MODE_NOTIFY:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    .line 50
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    sget-object v1, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->MDM_LICENSE_KEY:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->MDM_LICENSE_DEACTIVATE:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->MDM_LICENSE_KEY_UMC:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_LICENSE_CHECK:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_CREATED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_REMOVED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_CREATE_B2C:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->ELM_LICENSE_CHECK:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->ELM_PERMISSION_CHECK:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->BOOT_COMPLETED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->NETWORK_STATE_CHANGED_ACTION:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->TIME_CHANGED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->PACKAGE_REMOVED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->RP_MODE_NOTIFY:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->$VALUES:[Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 50
    const-class v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    return-object v0
.end method

.method public static values()[Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->$VALUES:[Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-virtual {v0}, [Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    return-object v0
.end method
