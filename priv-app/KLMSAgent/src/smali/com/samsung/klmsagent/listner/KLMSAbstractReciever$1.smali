.class final Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$1;
.super Landroid/os/Handler;
.source "KLMSAbstractReciever.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 24
    .param p1, "reqMsg"    # Landroid/os/Message;

    .prologue
    .line 210
    const-string v21, "KLMSAbstractReciever(): Retrying the http/https calls."

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 213
    invoke-static/range {p1 .. p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v15

    .line 214
    .local v15, "retryMessage":Landroid/os/Message;
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$1;->removeMessages(ILjava/lang/Object;)V

    .line 216
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->values()[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    move-result-object v14

    .line 217
    .local v14, "requests":[Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    invoke-static {}, Lcom/samsung/klmsagent/http/CallState$ConnectionType;->values()[Lcom/samsung/klmsagent/http/CallState$ConnectionType;

    move-result-object v4

    .line 219
    .local v4, "connections":[Lcom/samsung/klmsagent/http/CallState$ConnectionType;
    const/16 v20, 0x0

    .line 220
    .local v20, "tryCount":I
    const/4 v3, 0x0

    .line 221
    .local v3, "callType":I
    iget v0, v15, Landroid/os/Message;->arg2:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 222
    const/16 v21, 0x0

    move/from16 v0, v21

    iput v0, v15, Landroid/os/Message;->arg2:I

    .line 224
    invoke-virtual {v15}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    if-eqz v21, :cond_0

    .line 225
    invoke-virtual {v15}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    const-string v22, "current_count"

    invoke-virtual/range {v21 .. v22}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v21

    add-int/lit8 v20, v21, 0x1

    .line 226
    invoke-virtual {v15}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    const-string v22, "current_call_type"

    invoke-virtual/range {v21 .. v22}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 227
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "KLMSAbstractReciever(): retry count is : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", for protocol type : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 231
    :cond_0
    const/16 v16, 0x0

    .line 232
    .local v16, "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    sget-object v21, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$2;->$SwitchMap$com$samsung$klmsagent$util$KLMSConstant$KLMSRequestType:[I

    iget v0, v15, Landroid/os/Message;->what:I

    move/from16 v22, v0

    aget-object v22, v14, v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;->ordinal()I

    move-result v22

    aget v21, v21, v22

    packed-switch v21, :pswitch_data_0

    .line 354
    const-string v21, "KLMSAbstractReciever(): No mapping for this handler."

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 357
    :goto_0
    return-void

    .line 234
    :pswitch_0
    const-string v21, "KLMSAbstractReciever(): calling url : /klm-rest/v3/device/install.do"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 235
    new-instance v16, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    .end local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 236
    .restart local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setTryCount(I)V

    .line 237
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    aget-object v22, v4, v3

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 238
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/klm-rest/v3/device/install.do"

    aput-object v23, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 241
    :pswitch_1
    const-string v21, "KLMSAbstractReciever(): calling url : /klm-rest/v3/validateTrackKey.do"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 242
    new-instance v16, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    .end local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 243
    .restart local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setTryCount(I)V

    .line 244
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    aget-object v22, v4, v3

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 245
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/klm-rest/v3/validateTrackKey.do"

    aput-object v23, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 248
    :pswitch_2
    const-string v21, "KLMSAbstractReciever(): calling url : /klm-rest/v3/device/uninstall.do"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 249
    new-instance v16, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    .end local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 250
    .restart local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setTryCount(I)V

    .line 251
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    aget-object v22, v4, v3

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 252
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/klm-rest/v3/device/uninstall.do"

    aput-object v23, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 256
    :pswitch_3
    const-string v21, "KLMSAbstractReciever(): calling url : /klm-rest/v3/container/install.do"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 257
    new-instance v16, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    .end local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 258
    .restart local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setTryCount(I)V

    .line 259
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    aget-object v22, v4, v3

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 260
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/klm-rest/v3/container/install.do"

    aput-object v23, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 263
    :pswitch_4
    const-string v21, "KLMSAbstractReciever(): calling url : /klm-rest/v3/container/uninstall.do"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 264
    new-instance v16, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    .end local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 265
    .restart local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setTryCount(I)V

    .line 266
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    aget-object v22, v4, v3

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 267
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/klm-rest/v3/container/uninstall.do"

    aput-object v23, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 270
    :pswitch_5
    const-string v21, "KLMSAbstractReciever(): calling url : /klm-rest/enrollContainer.do"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 271
    new-instance v16, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    .end local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 272
    .restart local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setTryCount(I)V

    .line 273
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    aget-object v22, v4, v3

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 274
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/klm-rest/enrollContainer.do"

    aput-object v23, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 277
    :pswitch_6
    const-string v21, "KLMSAbstractReciever(): calling url : /klm-rest/uninstallContainer.do"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 278
    new-instance v16, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    .end local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 279
    .restart local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setTryCount(I)V

    .line 280
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    aget-object v22, v4, v3

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 281
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/klm-rest/uninstallContainer.do"

    aput-object v23, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 284
    :pswitch_7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    const-string v22, "onPrem"

    invoke-virtual/range {v21 .. v22}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 285
    const-string v21, "KLMSAbstractReciever(): calling url : /KnoxGSLB/lookup/klms"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 286
    new-instance v16, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    .end local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 287
    .restart local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    new-instance v19, Lcom/samsung/klmsagent/http/CallState;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/klmsagent/http/CallState;-><init>()V

    .line 288
    .local v19, "state1":Lcom/samsung/klmsagent/http/CallState;
    sget-object v21, Lcom/samsung/klmsagent/http/CallState$CallType;->GSLB:Lcom/samsung/klmsagent/http/CallState$CallType;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setCallType(Lcom/samsung/klmsagent/http/CallState$CallType;)V

    .line 289
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->setCall(Lcom/samsung/klmsagent/http/CallState;)V

    .line 290
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setTryCount(I)V

    .line 291
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    aget-object v22, v4, v3

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 292
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/KnoxGSLB/lookup/klms"

    aput-object v23, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 295
    .end local v19    # "state1":Lcom/samsung/klmsagent/http/CallState;
    :cond_1
    const-string v21, "KLMSAbstractReciever(): calling gslb, special case to be handled."

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 296
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "KLMSAbstractReciever(): calling gslb, having b2c contianer uninstall ordinal."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v15}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v22

    const-string v23, "request.ordinal"

    invoke-virtual/range {v22 .. v23}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 298
    new-instance v16, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    .end local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 299
    .restart local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    new-instance v18, Lcom/samsung/klmsagent/http/CallState;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/klmsagent/http/CallState;-><init>()V

    .line 300
    .local v18, "state":Lcom/samsung/klmsagent/http/CallState;
    aget-object v21, v4, v3

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 301
    sget-object v21, Lcom/samsung/klmsagent/http/CallState$CallType;->GSLB:Lcom/samsung/klmsagent/http/CallState$CallType;

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setCallType(Lcom/samsung/klmsagent/http/CallState$CallType;)V

    .line 302
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->setCall(Lcom/samsung/klmsagent/http/CallState;)V

    .line 303
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setTryCount(I)V

    .line 304
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    aget-object v22, v4, v3

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 305
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    const-string v22, "type"

    invoke-virtual/range {v21 .. v22}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "b2c"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 306
    const-string v21, "KLMSAbstractReciever(): retrying gslb for b2c"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 307
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/KnoxGSLB/lookup/klmb2c"

    aput-object v23, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 309
    :cond_2
    const-string v21, "KLMSAbstractReciever(): retrying gslb for b2b"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 310
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/KnoxGSLB/lookup/klms"

    aput-object v23, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 314
    .end local v18    # "state":Lcom/samsung/klmsagent/http/CallState;
    :pswitch_8
    const-string v21, "KLMSAbstractReciever(): Process the validation after the GSLB is set."

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 315
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    .line 316
    .local v8, "extras":Landroid/os/Bundle;
    new-instance v11, Lcom/samsung/klmsagent/services/i/DeviceServices;

    invoke-direct {v11}, Lcom/samsung/klmsagent/services/i/DeviceServices;-><init>()V

    .line 319
    .local v11, "mDeviceHandler":Lcom/samsung/klmsagent/services/KLMSServices;, "Lcom/samsung/klmsagent/services/KLMSServices<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    new-instance v9, Lcom/samsung/klmsagent/services/i/StateImplV2;

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v9, v0, v11}, Lcom/samsung/klmsagent/services/i/StateImplV2;-><init>(Landroid/content/Context;Lcom/samsung/klmsagent/services/KLMSServices;)V

    .line 320
    .local v9, "impl":Lcom/samsung/klmsagent/services/i/StateImplV2;
    invoke-virtual {v9, v8}, Lcom/samsung/klmsagent/services/i/StateImplV2;->validateEnrollDevice(Landroid/os/Bundle;)V

    .line 321
    const-string v21, "KLMSAbstractReciever(): activity close called"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 322
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v21

    const/16 v22, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponse(Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 325
    .end local v8    # "extras":Landroid/os/Bundle;
    .end local v9    # "impl":Lcom/samsung/klmsagent/services/i/StateImplV2;
    .end local v11    # "mDeviceHandler":Lcom/samsung/klmsagent/services/KLMSServices;, "Lcom/samsung/klmsagent/services/KLMSServices<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    :pswitch_9
    const-string v21, "KLMSAbstractReciever(): Going to call device register after GSLB set."

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 326
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    .line 327
    .local v5, "devRegDt":Landroid/os/Bundle;
    new-instance v12, Lcom/samsung/klmsagent/services/i/DeviceServices;

    invoke-direct {v12}, Lcom/samsung/klmsagent/services/i/DeviceServices;-><init>()V

    .line 328
    .local v12, "mDeviceHandler1":Lcom/samsung/klmsagent/services/KLMSServices;, "Lcom/samsung/klmsagent/services/KLMSServices<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    const-string v21, "deviceData"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 329
    .local v6, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    const-string v21, "requestType"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v13

    check-cast v13, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;

    .line 330
    .local v13, "requestType":Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    const-string v21, "keySource"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v10

    check-cast v10, Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;

    .line 332
    .local v10, "keySource":Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;
    :try_start_0
    invoke-virtual {v12, v6, v13, v10}, Lcom/samsung/klmsagent/services/KLMSServices;->validateRegister(Lcom/samsung/klmsagent/beans/KLMSComponent;Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;)V
    :try_end_0
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    :goto_1
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v21

    sget-object v22, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->setWaitForGslbResponseForDeviceReg(Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 333
    :catch_0
    move-exception v7

    .line 334
    .local v7, "e":Lorg/apache/http/MethodNotSupportedException;
    const-string v21, "Not able to validate with the provided license key"

    move-object/from16 v0, v21

    invoke-static {v0, v7}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 339
    .end local v5    # "devRegDt":Landroid/os/Bundle;
    .end local v6    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v7    # "e":Lorg/apache/http/MethodNotSupportedException;
    .end local v10    # "keySource":Lcom/samsung/klmsagent/util/KLMSConstant$KeySource;
    .end local v12    # "mDeviceHandler1":Lcom/samsung/klmsagent/services/KLMSServices;, "Lcom/samsung/klmsagent/services/KLMSServices<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v13    # "requestType":Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestType;
    :pswitch_a
    const-string v21, "KLMSAbstractReciever(): Retrying the policy fetch from the device."

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 340
    new-instance v17, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;

    move-object/from16 v0, v17

    invoke-direct {v0, v15}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;-><init>(Landroid/os/Message;)V

    .line 341
    .local v17, "retryTaskPolicy":Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setTryCount(I)V

    .line 342
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    aget-object v22, v4, v3

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 343
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    sget-object v22, Lcom/samsung/klmsagent/http/CallState$CallType;->GSLB:Lcom/samsung/klmsagent/http/CallState$CallType;

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/http/CallState;->setCallType(Lcom/samsung/klmsagent/http/CallState$CallType;)V

    .line 344
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/KnoxGSLB/policy"

    aput-object v23, v21, v22

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 347
    .end local v17    # "retryTaskPolicy":Lcom/samsung/klmsagent/http/KLMSHTTPOnPremise;
    :pswitch_b
    const-string v21, "KLMSAbstractReciever(): calling url : /knox-license/attribution/settings.do"

    invoke-static/range {v21 .. v21}, Lcom/samsung/klmsagent/util/KLMSLogger;->v(Ljava/lang/String;)V

    .line 348
    new-instance v16, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;

    .end local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;-><init>(Landroid/os/Message;)V

    .line 349
    .restart local v16    # "retryTask":Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/CallState;->setTryCount(I)V

    .line 350
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->getCall()Lcom/samsung/klmsagent/http/CallState;

    move-result-object v21

    aget-object v22, v4, v3

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/klmsagent/http/CallState;->setCurrentConnection(Lcom/samsung/klmsagent/http/CallState$ConnectionType;)V

    .line 351
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "/knox-license/attribution/settings.do"

    aput-object v23, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/http/KLMSHttpAsynchCall;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 232
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
