.class public abstract Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;
.super Landroid/content/BroadcastReceiver;
.source "KLMSAbstractReciever.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$2;,
        Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "KLMSAbstractReciever(): "

.field private static actionMapper:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;",
            ">;"
        }
    .end annotation
.end field

.field public static final mHandler:Landroid/os/Handler;

.field private static urlToPolicyMapper:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/klmsagent/services/KLMSServices",
            "<",
            "Lcom/samsung/klmsagent/beans/ContainerData;",
            ">;"
        }
    .end annotation
.end field

.field private mContainerState:Lcom/samsung/klmsagent/services/ContainerStates;

.field private mDeviceHandler:Lcom/samsung/klmsagent/services/KLMSServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/klmsagent/services/KLMSServices",
            "<",
            "Lcom/samsung/klmsagent/beans/DeviceData;",
            ">;"
        }
    .end annotation
.end field

.field private mELMState:Lcom/samsung/klmsagent/services/ELMStates;

.field private state:Lcom/samsung/klmsagent/services/States;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    .line 58
    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->urlToPolicyMapper:Ljava/util/Map;

    .line 207
    new-instance v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$1;

    invoke-direct {v0}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$1;-><init>()V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 50
    return-void
.end method

.method protected static getActionMapper()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    .line 65
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "com.sec.knox.containeragent.klms.created"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_CREATED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "com.sec.knox.containeragent.klms.removed"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_REMOVED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "com.sec.knox.containeragent.klms.created.b2b"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_CREATED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "com.sec.knox.containeragent.klms.removed.b2b"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_REMOVED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "com.sec.knox.containeragent.klms.licensekey.check"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_LICENSE_CHECK:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "com.sec.knox.containeragent.klms.created.b2c"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_CREATE_B2C:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "com.sec.knox.containeragent.klms.removed.b2c"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->CONTAINER_REMOVED_B2C:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "edm.intent.action.knox_license.registration.internal"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->MDM_LICENSE_KEY:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "edm.intent.action.knox_license.registration.internal_umc"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->MDM_LICENSE_KEY_UMC:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "edm.intent.action.knox_license.deactivation.internal"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->MDM_LICENSE_DEACTIVATE:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "com.sec.esdk.elm.action.REQUEST_KLMS_STATUS"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->ELM_LICENSE_CHECK:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "com.sec.esdk.elm.action.NOTICE_PERMISSON_CHANGED"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->ELM_PERMISSION_CHECK:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->BOOT_COMPLETED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "android.intent.action.TIME_SET"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->TIME_CHANGED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->NETWORK_STATE_CHANGED_ACTION:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->PACKAGE_REMOVED:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    const-string v1, "com.samsung.action.knox.kap.KAP_NOTIFICATION"

    sget-object v2, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;->RP_MODE_NOTIFY:Lcom/samsung/klmsagent/listner/KLMSAbstractReciever$BroadcastAction;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    :cond_0
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->actionMapper:Ljava/util/Map;

    return-object v0
.end method

.method public static getUrlToPolicyMapper()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->urlToPolicyMapper:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->urlToPolicyMapper:Ljava/util/Map;

    .line 103
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->urlToPolicyMapper:Ljava/util/Map;

    const-string v1, "enrollDevice"

    const-string v2, "enrollDevice"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->urlToPolicyMapper:Ljava/util/Map;

    const-string v1, "validateTrackKey"

    const-string v2, "validateTrackKey"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->urlToPolicyMapper:Ljava/util/Map;

    const-string v1, "uninstallLicense"

    const-string v2, "uninstallLicense"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->urlToPolicyMapper:Ljava/util/Map;

    const-string v1, "uninstallContainer"

    const-string v2, "uninstallContainer"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->urlToPolicyMapper:Ljava/util/Map;

    const-string v1, "/klm-rest/uninstallContainer.do"

    const-string v2, "uninstallContainer"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->urlToPolicyMapper:Ljava/util/Map;

    const-string v1, "enrollContainer"

    const-string v2, "enrollContainer"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->urlToPolicyMapper:Ljava/util/Map;

    const-string v1, "/klm-rest/enrollContainer.do"

    const-string v2, "enrollContainer"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->urlToPolicyMapper:Ljava/util/Map;

    const-string v1, "/klm-rest/notifyTrackKey.do"

    const-string v2, "notifyTrackKey"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    :cond_0
    sget-object v0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->urlToPolicyMapper:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public abstract execute(Landroid/content/Context;Landroid/content/Intent;)V
.end method

.method public getContainerHandler()Lcom/samsung/klmsagent/services/KLMSServices;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/samsung/klmsagent/services/KLMSServices",
            "<",
            "Lcom/samsung/klmsagent/beans/ContainerData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    return-object v0
.end method

.method public getContainerState()Lcom/samsung/klmsagent/services/ContainerStates;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mContainerState:Lcom/samsung/klmsagent/services/ContainerStates;

    return-object v0
.end method

.method public getDeviceHandler()Lcom/samsung/klmsagent/services/KLMSServices;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/samsung/klmsagent/services/KLMSServices",
            "<",
            "Lcom/samsung/klmsagent/beans/DeviceData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mDeviceHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    return-object v0
.end method

.method public getELMState()Lcom/samsung/klmsagent/services/ELMStates;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mELMState:Lcom/samsung/klmsagent/services/ELMStates;

    return-object v0
.end method

.method public getStatesHandler()Lcom/samsung/klmsagent/services/States;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->state:Lcom/samsung/klmsagent/services/States;

    return-object v0
.end method

.method protected init(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 120
    if-eqz p1, :cond_0

    .line 121
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/context/KLMSContextManager;->setContext(Landroid/content/Context;)V

    .line 122
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->setDataSource()V

    .line 124
    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 154
    invoke-virtual {p0, p1}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->init(Landroid/content/Context;)V

    .line 156
    if-nez p2, :cond_0

    .line 157
    const-string v1, "KLMSAbstractReciever(): Recieved Intent is null, Can\'t process."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 201
    :goto_0
    return-void

    .line 161
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "Action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 163
    const-string v1, "KLMSAbstractReciever(): Recieved Intent Action is null, Can\'t process."

    invoke-static {v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_1
    const-string v1, "b2c"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "KAP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 169
    :cond_2
    new-instance v1, Lcom/samsung/klmsagent/b2c/services/ContainerServices;

    invoke-direct {v1}, Lcom/samsung/klmsagent/b2c/services/ContainerServices;-><init>()V

    iput-object v1, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    .line 171
    new-instance v1, Lcom/samsung/klmsagent/b2c/services/ContainerStateImpl;

    iget-object v2, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    invoke-direct {v1, p1, v2}, Lcom/samsung/klmsagent/b2c/services/ContainerStateImpl;-><init>(Landroid/content/Context;Lcom/samsung/klmsagent/services/KLMSServices;)V

    iput-object v1, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mContainerState:Lcom/samsung/klmsagent/services/ContainerStates;

    .line 179
    :goto_1
    new-instance v1, Lcom/samsung/klmsagent/services/i/DeviceServices;

    invoke-direct {v1}, Lcom/samsung/klmsagent/services/i/DeviceServices;-><init>()V

    iput-object v1, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mDeviceHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    .line 180
    new-instance v1, Lcom/samsung/klmsagent/services/i/StateImplV2;

    iget-object v2, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mDeviceHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    invoke-direct {v1, p1, v2}, Lcom/samsung/klmsagent/services/i/StateImplV2;-><init>(Landroid/content/Context;Lcom/samsung/klmsagent/services/KLMSServices;)V

    iput-object v1, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->state:Lcom/samsung/klmsagent/services/States;

    .line 181
    new-instance v1, Lcom/samsung/klmsagent/services/i/ELMServices;

    invoke-direct {v1}, Lcom/samsung/klmsagent/services/i/ELMServices;-><init>()V

    iput-object v1, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mELMState:Lcom/samsung/klmsagent/services/ELMStates;

    .line 184
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ro.product_ship"

    invoke-static {v1, v2}, Lcom/samsung/klmsagent/util/KLMSSystemProperties;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->checkQATesting()Z

    move-result v1

    if-nez v1, :cond_4

    .line 186
    sput-boolean v4, Lcom/samsung/klmsagent/util/KLMSConstant;->isErrorLogEnable:Z

    .line 187
    sput-boolean v4, Lcom/samsung/klmsagent/util/KLMSConstant;->isDebugLogEnable:Z

    .line 188
    sput-boolean v4, Lcom/samsung/klmsagent/util/KLMSConstant;->isInfoLogEnable:Z

    .line 189
    sput-boolean v4, Lcom/samsung/klmsagent/util/KLMSConstant;->isVerboseLogEnable:Z

    .line 190
    sput-boolean v3, Lcom/samsung/klmsagent/util/KLMSConstant;->isUserBuild:Z

    .line 191
    sput-boolean v4, Lcom/samsung/klmsagent/util/KLMSConstant;->isProxyEnabled:Z

    .line 200
    :goto_2
    invoke-virtual {p0, p1, p2}, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->execute(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 174
    :cond_3
    new-instance v1, Lcom/samsung/klmsagent/services/i/ContainerServices;

    invoke-direct {v1}, Lcom/samsung/klmsagent/services/i/ContainerServices;-><init>()V

    iput-object v1, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    .line 176
    new-instance v1, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;

    iget-object v2, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mContainerHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    invoke-direct {v1, p1, v2}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;-><init>(Landroid/content/Context;Lcom/samsung/klmsagent/services/KLMSServices;)V

    iput-object v1, p0, Lcom/samsung/klmsagent/listner/KLMSAbstractReciever;->mContainerState:Lcom/samsung/klmsagent/services/ContainerStates;

    goto :goto_1

    .line 193
    :cond_4
    sput-boolean v3, Lcom/samsung/klmsagent/util/KLMSConstant;->isErrorLogEnable:Z

    .line 194
    sput-boolean v3, Lcom/samsung/klmsagent/util/KLMSConstant;->isDebugLogEnable:Z

    .line 195
    sput-boolean v3, Lcom/samsung/klmsagent/util/KLMSConstant;->isInfoLogEnable:Z

    .line 196
    sput-boolean v3, Lcom/samsung/klmsagent/util/KLMSConstant;->isVerboseLogEnable:Z

    .line 197
    sput-boolean v3, Lcom/samsung/klmsagent/util/KLMSConstant;->isUserBuild:Z

    .line 198
    sput-boolean v3, Lcom/samsung/klmsagent/util/KLMSConstant;->isProxyEnabled:Z

    goto :goto_2
.end method
