.class public Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AlarmIntentReceiver.java"


# static fields
.field public static ALARM_INTENT:Ljava/lang/String;

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mDeviceStateHandler:Lcom/samsung/klmsagent/services/KLMSServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/klmsagent/services/KLMSServices",
            "<",
            "Lcom/samsung/klmsagent/beans/DeviceData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "com.samsung.klmsagent.alarm.intent"

    sput-object v0, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->ALARM_INTENT:Ljava/lang/String;

    .line 32
    const-string v0, "AlarmIntentReceiver(): "

    sput-object v0, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static postProcessResponseTrackerId(Ljava/lang/String;Landroid/os/Message;)V
    .locals 20
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "reqObj"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "activation_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 81
    .local v8, "activationId":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "postProcessResponseTrackerId().START"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 85
    if-eqz p0, :cond_0

    :try_start_0
    const-string v2, ""

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static/range {p0 .. p1}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->checkTransactionId(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 86
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "postProcessResponseTrackerId() : Result from Server is null."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 88
    const/16 v2, 0x190

    const/16 v3, 0x1f6

    const/4 v4, -0x1

    const/16 v5, 0x321

    const-string v6, "fail"

    invoke-static {v8}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->getMDMId(I)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 92
    const-wide/32 v2, 0x5265c00

    invoke-static {v8, v2, v3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->resetCronSettings(IJ)V

    .line 93
    invoke-static {v8}, Lcom/samsung/klmsagent/services/i/StateImplV2;->setUpAlarm(I)V

    .line 165
    :goto_0
    return-void

    .line 96
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Result from Server:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 97
    new-instance v16, Lorg/json/JSONObject;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 98
    .local v16, "response":Lorg/json/JSONObject;
    const-string v2, "payLoad"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/json/JSONObject;

    .line 99
    .local v15, "payload":Lorg/json/JSONObject;
    const-string v2, "responseCode"

    invoke-virtual {v15, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 100
    .local v14, "payLoadResponseCode":I
    const/4 v11, 0x0

    .line 101
    .local v11, "jsonArray":Lorg/json/JSONArray;
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 102
    .local v17, "responseLicenseStatus":Ljava/lang/Integer;
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    .line 104
    .local v13, "nextValidation":Ljava/lang/Long;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "postProcessResponseTrackerId().responseCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 106
    packed-switch v14, :pswitch_data_0

    .line 133
    new-instance v11, Lorg/json/JSONArray;

    .end local v11    # "jsonArray":Lorg/json/JSONArray;
    const-string v2, "trackIds"

    invoke-virtual {v15, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v11, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 135
    .restart local v11    # "jsonArray":Lorg/json/JSONArray;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v10, v2, :cond_3

    .line 136
    invoke-virtual {v11, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    .line 137
    .local v12, "jsonObjectArr":Lorg/json/JSONObject;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Device tracker Id from server: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "trackerId"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 138
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "License status from server: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "licenseStatus"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 140
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Device tracker Id on device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v8}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->getTrackerId1(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 142
    const-string v2, "licenseStatus"

    invoke-virtual {v12, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 135
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 110
    .end local v10    # "i":I
    .end local v12    # "jsonObjectArr":Lorg/json/JSONObject;
    :pswitch_0
    new-instance v11, Lorg/json/JSONArray;

    .end local v11    # "jsonArray":Lorg/json/JSONArray;
    const-string v2, "trackIds"

    invoke-virtual {v15, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v11, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 112
    .restart local v11    # "jsonArray":Lorg/json/JSONArray;
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_2
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v10, v2, :cond_2

    .line 113
    invoke-virtual {v11, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    .line 114
    .restart local v12    # "jsonObjectArr":Lorg/json/JSONObject;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Device tracker Id from server: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "trackerId"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 115
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "License status from server: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "licenseStatus"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 116
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Device tracker Id on device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v8}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->getTrackerId1(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 118
    const-string v2, "licenseStatus"

    invoke-virtual {v12, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 112
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 121
    .end local v12    # "jsonObjectArr":Lorg/json/JSONObject;
    :cond_2
    const-string v2, "nextValidation"

    invoke-virtual {v15, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    .line 123
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v8, v2, v3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->resetCronSettings(IJ)V

    .line 124
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v8, v2}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->setLicenseStatus(II)V

    .line 125
    invoke-static {v8}, Lcom/samsung/klmsagent/services/i/StateImplV2;->setUpAlarm(I)V

    .line 128
    new-instance v18, Lcom/samsung/klmsagent/context/State;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->fromInt(I)Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Lcom/samsung/klmsagent/context/State;-><init>(Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;)V

    .line 130
    .local v18, "state":Lcom/samsung/klmsagent/context/State;
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/samsung/klmsagent/context/State;->takeAction(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 156
    .end local v10    # "i":I
    .end local v11    # "jsonArray":Lorg/json/JSONArray;
    .end local v13    # "nextValidation":Ljava/lang/Long;
    .end local v14    # "payLoadResponseCode":I
    .end local v15    # "payload":Lorg/json/JSONObject;
    .end local v16    # "response":Lorg/json/JSONObject;
    .end local v17    # "responseLicenseStatus":Ljava/lang/Integer;
    .end local v18    # "state":Lcom/samsung/klmsagent/context/State;
    :catch_0
    move-exception v9

    .line 157
    .local v9, "e":Ljava/lang/Exception;
    const-string v2, "common UnKnown Error in parsing the response."

    invoke-static {v2, v9}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 158
    const/16 v2, 0x190

    const/16 v3, 0x66

    const/4 v4, -0x1

    const/16 v5, 0x321

    const-string v6, "fail"

    invoke-static {v8}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->getMDMId(I)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 162
    const-wide/32 v2, 0x5265c00

    invoke-static {v8, v2, v3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->resetCronSettings(IJ)V

    .line 163
    invoke-static {v8}, Lcom/samsung/klmsagent/services/i/StateImplV2;->setUpAlarm(I)V

    goto/16 :goto_0

    .line 145
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v10    # "i":I
    .restart local v11    # "jsonArray":Lorg/json/JSONArray;
    .restart local v13    # "nextValidation":Ljava/lang/Long;
    .restart local v14    # "payLoadResponseCode":I
    .restart local v15    # "payload":Lorg/json/JSONObject;
    .restart local v16    # "response":Lorg/json/JSONObject;
    .restart local v17    # "responseLicenseStatus":Ljava/lang/Integer;
    :cond_3
    :try_start_1
    const-string v2, "nextValidation"

    invoke-virtual {v15, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    .line 147
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v8, v2, v3}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->resetCronSettings(IJ)V

    .line 148
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v8, v2}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->setLicenseStatus(II)V

    .line 149
    invoke-static {v8}, Lcom/samsung/klmsagent/services/i/StateImplV2;->setUpAlarm(I)V

    .line 151
    new-instance v19, Lcom/samsung/klmsagent/context/State;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->fromInt(I)Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/samsung/klmsagent/context/State;-><init>(Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;)V

    .line 152
    .local v19, "state1":Lcom/samsung/klmsagent/context/State;
    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/samsung/klmsagent/context/State;->takeAction(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 36
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "onReceive()"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 39
    if-nez p2, :cond_0

    .line 40
    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Recieved Intent is null, Can\'t process."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V

    .line 74
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 45
    .local v4, "extra":Landroid/os/Bundle;
    if-nez v4, :cond_1

    .line 46
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Intent Extra is null, Can\'t process."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 68
    .end local v4    # "extra":Landroid/os/Bundle;
    :catch_0
    move-exception v5

    .line 69
    .local v5, "mnse":Lorg/apache/http/MethodNotSupportedException;
    const-string v6, "Error MethodNotSupportedException"

    invoke-static {v6, v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 50
    .end local v5    # "mnse":Lorg/apache/http/MethodNotSupportedException;
    .restart local v4    # "extra":Landroid/os/Bundle;
    :cond_1
    :try_start_1
    const-string v6, "_id"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 51
    .local v0, "activationId":I
    new-instance v6, Lcom/samsung/klmsagent/services/i/DeviceServices;

    invoke-direct {v6}, Lcom/samsung/klmsagent/services/i/DeviceServices;-><init>()V

    iput-object v6, p0, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->mDeviceStateHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    .line 53
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "onHandleIntent() : Starting to validate the KNOX License. Activation id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 56
    new-instance v1, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v1}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 57
    .local v1, "data":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v1, v0}, Lcom/samsung/klmsagent/beans/DeviceData;->set_id(I)V

    .line 59
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v6

    const-string v7, "_id=?"

    invoke-virtual {v6, v7, v1}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v2

    .line 60
    .local v2, "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 61
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Device Data is present : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 63
    iget-object v8, p0, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->mDeviceStateHandler:Lcom/samsung/klmsagent/services/KLMSServices;

    const/4 v6, 0x0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/klmsagent/beans/KLMSComponent;

    const/4 v7, 0x0

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v7}, Lcom/samsung/klmsagent/beans/DeviceData;->getLicenseStatus()I

    move-result v7

    invoke-virtual {v8, v6, v7, v0}, Lcom/samsung/klmsagent/services/KLMSServices;->validateTrackKey(Lcom/samsung/klmsagent/beans/KLMSComponent;II)V
    :try_end_1
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 70
    .end local v0    # "activationId":I
    .end local v1    # "data":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v4    # "extra":Landroid/os/Bundle;
    :catch_1
    move-exception v3

    .line 71
    .local v3, "e":Ljava/lang/Exception;
    const-string v6, "Error Exception"

    invoke-static {v6, v3}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 66
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v0    # "activationId":I
    .restart local v1    # "data":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v2    # "deviceLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v4    # "extra":Landroid/os/Bundle;
    :cond_2
    :try_start_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/klmsagent/cronjob/AlarmIntentReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "No Device data present, still cron job called."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0
.end method
