.class public Lcom/samsung/klmsagent/context/KLMSContextManager;
.super Ljava/lang/Object;
.source "KLMSContextManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "KLMSContextManager(): "

.field private static context:Landroid/content/Context;

.field private static klmContext:Lcom/samsung/klmsagent/context/KLMSContextManager;

.field private static mSynch:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager;->context:Landroid/content/Context;

    .line 22
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager;->mSynch:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager;->context:Landroid/content/Context;

    return-object v0
.end method

.method public static getCurrentState()Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 46
    const-string v4, "KLMSContextManager(): getCurrentState(): Checking the CA, ELM and MDM."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 47
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v1

    .line 49
    .local v1, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v4, 0x0

    :try_start_0
    new-instance v5, Lcom/samsung/klmsagent/beans/ContainerData;

    invoke-direct {v5}, Lcom/samsung/klmsagent/beans/ContainerData;-><init>()V

    invoke-virtual {v1, v4, v5}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v0

    .line 50
    .local v0, "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 51
    const-string v4, "KLMSContextManager(): getCurrentState(): All state found."

    invoke-static {v4}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 52
    sget-object v3, Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;->ALL:Lcom/samsung/klmsagent/util/KLMSConstant$KLMSRequestState;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    .end local v0    # "containerLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/ContainerData;>;"
    :cond_0
    :goto_0
    return-object v3

    .line 54
    :catch_0
    move-exception v2

    .line 55
    .local v2, "ex":Ljava/lang/Exception;
    const-string v4, "Exception happened"

    invoke-static {v4, v2}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/samsung/klmsagent/context/KLMSContextManager;
    .locals 2

    .prologue
    .line 125
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager;->klmContext:Lcom/samsung/klmsagent/context/KLMSContextManager;

    if-nez v0, :cond_1

    .line 126
    sget-object v1, Lcom/samsung/klmsagent/context/KLMSContextManager;->mSynch:Ljava/lang/Object;

    monitor-enter v1

    .line 127
    :try_start_0
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager;->klmContext:Lcom/samsung/klmsagent/context/KLMSContextManager;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager;

    invoke-direct {v0}, Lcom/samsung/klmsagent/context/KLMSContextManager;-><init>()V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager;->klmContext:Lcom/samsung/klmsagent/context/KLMSContextManager;

    .line 129
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    :cond_1
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager;->klmContext:Lcom/samsung/klmsagent/context/KLMSContextManager;

    return-object v0

    .line 129
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 34
    const-class v1, Lcom/samsung/klmsagent/context/KLMSContextManager;

    monitor-enter v1

    .line 35
    :try_start_0
    sput-object p0, Lcom/samsung/klmsagent/context/KLMSContextManager;->context:Landroid/content/Context;

    .line 36
    monitor-exit v1

    .line 38
    :cond_0
    return-void

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
