.class public Lcom/samsung/klmsagent/context/State;
.super Ljava/lang/Object;
.source "State.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/klmsagent/context/State$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "State(): "


# instance fields
.field private currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field private klmContext:Lcom/samsung/klmsagent/context/KLMSContextManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "err_code"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "State(): Check license state with Error Code : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 36
    sparse-switch p1, :sswitch_data_0

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "State(): Default+erroce code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 93
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->HTTP_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    .line 96
    :goto_0
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getInstance()Lcom/samsung/klmsagent/context/KLMSContextManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->klmContext:Lcom/samsung/klmsagent/context/KLMSContextManager;

    .line 97
    return-void

    .line 40
    :sswitch_0
    const-string v0, "State(): LicenseStatus.ACTIVE"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 41
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto :goto_0

    .line 44
    :sswitch_1
    const-string v0, "State(): LicenseStatus.EXPIRED"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 45
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->EXPIRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto :goto_0

    .line 48
    :sswitch_2
    const-string v0, "State(): LicenseStatus.DEACTIVE"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 49
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->DEACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto :goto_0

    .line 52
    :sswitch_3
    const-string v0, "State(): LicenseStatus.INVALID"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 53
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->INVALID:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto :goto_0

    .line 56
    :sswitch_4
    const-string v0, "State(): LicenseStatus.TERMINATED"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 57
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->TERMINATED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto :goto_0

    .line 60
    :sswitch_5
    const-string v0, "State(): LicenseStatus.QUANTITY_OVER"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 61
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->QUANTITY_OVER:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto :goto_0

    .line 64
    :sswitch_6
    const-string v0, "State(): LicenseStatus.EULA_NOT_AGRRED"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 65
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->EULA_NOT_AGRRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto :goto_0

    .line 68
    :sswitch_7
    const-string v0, "State(): LicenseStatus.INTERNAl_SERVER_ERROR"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 69
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->INTERNAl_SERVER_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto :goto_0

    .line 72
    :sswitch_8
    const-string v0, "State(): LicenseStatus.UI_FORMAT_ERROR"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 73
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->UI_FORMAT_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto :goto_0

    .line 76
    :sswitch_9
    const-string v0, "State(): LicenseStatus.SMS_FORMAT_ERROR"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 77
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->SMS_FORMAT_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto :goto_0

    .line 80
    :sswitch_a
    const-string v0, "State(): LicenseStatus.HTTP_ERROR"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 81
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->HTTP_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto :goto_0

    .line 84
    :sswitch_b
    const-string v0, "State(): LicenseStatus.NO_NETWORK"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 85
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->NO_NETWORK:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto :goto_0

    .line 88
    :sswitch_c
    const-string v0, "State(): LicenseStatus.DATE_ERROR"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 89
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->DATE_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    goto/16 :goto_0

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_0
        0x3ed -> :sswitch_7
        0xbb9 -> :sswitch_c
        0xc1d -> :sswitch_1
        0xc1e -> :sswitch_2
        0xc1f -> :sswitch_3
        0xc20 -> :sswitch_4
        0xc2b -> :sswitch_0
        0xc32 -> :sswitch_5
        0xc35 -> :sswitch_0
        0x115c -> :sswitch_9
        0x15b3 -> :sswitch_8
        0x1e61 -> :sswitch_6
        0x22b8 -> :sswitch_a
        0x270f -> :sswitch_b
    .end sparse-switch
.end method

.method public constructor <init>(Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;)V
    .locals 1
    .param p1, "currentState"    # Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    .line 31
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getInstance()Lcom/samsung/klmsagent/context/KLMSContextManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/klmsagent/context/State;->klmContext:Lcom/samsung/klmsagent/context/KLMSContextManager;

    .line 32
    return-void
.end method

.method public static deactivationProcess(Ljava/lang/String;)V
    .locals 14
    .param p0, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x1

    .line 201
    const-string v11, "State(): deactivationProcess().Start"

    invoke-static {v11}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 203
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v1

    .line 207
    .local v1, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v11, 0x1

    :try_start_0
    new-array v9, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object p0, v9, v11

    .line 209
    .local v9, "whereArgs":[Ljava/lang/String;
    new-instance v2, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v2}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 210
    .local v2, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v2, p0}, Lcom/samsung/klmsagent/beans/DeviceData;->setPackageName(Ljava/lang/String;)V

    .line 212
    const-string v11, "PACKAGE_NAME=?"

    const-string v12, "ASC"

    invoke-virtual {v1, v11, v2, v12, v9}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 215
    .local v3, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    const/4 v11, 0x0

    invoke-interface {v3, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v11}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v0

    .line 216
    .local v0, "activationId":I
    const/4 v11, 0x0

    invoke-interface {v3, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v11}, Lcom/samsung/klmsagent/beans/DeviceData;->getTrackerId()Ljava/lang/String;

    move-result-object v8

    .line 219
    .local v8, "trackerId":Ljava/lang/String;
    const/4 v11, 0x0

    invoke-interface {v3, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v11}, Lcom/samsung/klmsagent/beans/DeviceData;->getOnPremInfo()I

    move-result v11

    if-ne v11, v13, :cond_0

    .line 220
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->clearOnPremData()V

    .line 221
    invoke-static {}, Lcom/samsung/klmsagent/services/i/DeviceServices;->enableAlarm()V

    .line 224
    :cond_0
    invoke-static {v0}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->disableAlarm(I)V

    .line 226
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v11

    invoke-virtual {v11, p0}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->addNotificationAppList(Ljava/lang/String;)V

    .line 228
    const-string v11, "deviceinfo"

    const-string v12, "PACKAGE_NAME=?"

    invoke-virtual {v1, v11, v12, v9}, Lcom/samsung/klmsagent/data/DataSource;->deleteTables(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 231
    const/4 v11, 0x1

    new-array v10, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v8, v10, v11

    .line 232
    .local v10, "whereArgsTracker":[Ljava/lang/String;
    new-instance v2, Lcom/samsung/klmsagent/beans/DeviceData;

    .end local v2    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-direct {v2}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 233
    .restart local v2    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v2, v8}, Lcom/samsung/klmsagent/beans/DeviceData;->setTrackerId(Ljava/lang/String;)V

    .line 234
    const-string v11, "TRACKER_ID=?"

    const-string v12, "ASC"

    invoke-virtual {v1, v11, v2, v12, v10}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 236
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    .line 237
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 238
    .local v4, "devicedata":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v4}, Lcom/samsung/klmsagent/beans/DeviceData;->getTrackerId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 240
    const-string v11, "State(): Same tracker ID Found. Add Notification"

    invoke-static {v11}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 241
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v12

    const/4 v11, 0x0

    invoke-interface {v3, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v11}, Lcom/samsung/klmsagent/beans/DeviceData;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->addNotificationAppList(Ljava/lang/String;)V

    .line 242
    invoke-virtual {v2}, Lcom/samsung/klmsagent/beans/DeviceData;->get_id()I

    move-result v11

    invoke-static {v11}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->disableAlarm(I)V

    .line 244
    const-string v11, "State():  Activation record is found with same tracker id. Deleting the record."

    invoke-static {v11}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 245
    const-string v11, "deviceinfo"

    const-string v12, "TRACKER_ID=?"

    invoke-virtual {v1, v11, v12, v10}, Lcom/samsung/klmsagent/data/DataSource;->deleteTables(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 268
    .end local v0    # "activationId":I
    .end local v2    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v3    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v4    # "devicedata":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v8    # "trackerId":Ljava/lang/String;
    .end local v9    # "whereArgs":[Ljava/lang/String;
    .end local v10    # "whereArgsTracker":[Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 269
    .local v5, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v11, "Common Unkown Exception."

    invoke-static {v11, v5}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 271
    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 273
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 252
    .restart local v0    # "activationId":I
    .restart local v2    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v3    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v8    # "trackerId":Ljava/lang/String;
    .restart local v9    # "whereArgs":[Ljava/lang/String;
    .restart local v10    # "whereArgsTracker":[Ljava/lang/String;
    :cond_2
    const/4 v11, 0x0

    :try_start_2
    new-instance v12, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v12}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    invoke-virtual {v1, v11, v12}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v3

    .line 253
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v11

    if-nez v11, :cond_3

    .line 254
    const-string v11, "State():  No activation record is found. reseting shared pref"

    invoke-static {v11}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 255
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->clear()V

    .line 256
    const/4 v11, 0x0

    new-instance v12, Lcom/samsung/klmsagent/beans/RequestLog;

    invoke-direct {v12}, Lcom/samsung/klmsagent/beans/RequestLog;-><init>()V

    invoke-virtual {v1, v11, v12}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;)Ljava/util/List;

    move-result-object v7

    .line 257
    .local v7, "requestLogs":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/RequestLog;>;"
    if-eqz v7, :cond_3

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_3

    .line 258
    const-string v11, "removing all log data related to KLMS"

    invoke-static {v11}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 259
    const-string v11, "log_db"

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v1, v11, v12, v13}, Lcom/samsung/klmsagent/data/DataSource;->deleteTables(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 264
    .end local v7    # "requestLogs":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/RequestLog;>;"
    :cond_3
    const/16 v11, 0x32

    invoke-static {v11, p0}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->checkLicenseStatusToSendContainer(ILjava/lang/String;)V

    .line 267
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v11

    const v12, 0x7f060032

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/samsung/klmsagent/util/KLMSUtility;->showNotification(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 271
    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_1

    .end local v0    # "activationId":I
    .end local v2    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v3    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v8    # "trackerId":Ljava/lang/String;
    .end local v9    # "whereArgs":[Ljava/lang/String;
    .end local v10    # "whereArgsTracker":[Ljava/lang/String;
    :catchall_0
    move-exception v11

    invoke-virtual {v1}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v11
.end method


# virtual methods
.method public getCurrentState()Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/klmsagent/context/State;->currentState:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    return-object v0
.end method

.method public takeAction(I)V
    .locals 15
    .param p1, "activationId"    # I

    .prologue
    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "State(): takeAction(): getCurrentState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/klmsagent/context/State;->getCurrentState()Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 105
    invoke-static/range {p1 .. p1}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->getMDMId(I)Ljava/lang/String;

    move-result-object v5

    .line 107
    .local v5, "pkgName":Ljava/lang/String;
    sget-object v0, Lcom/samsung/klmsagent/context/State$1;->$SwitchMap$com$samsung$klmsagent$context$KLMSContextManager$LicenseStatus:[I

    invoke-virtual {p0}, Lcom/samsung/klmsagent/context/State;->getCurrentState()Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 195
    const-string v0, "State(): takeAction(defalut) : No matching case."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 198
    :goto_0
    return-void

    .line 109
    :pswitch_0
    const-string v0, "State(): takeAction() : License state is ACTIVE."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 112
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->deleteNotificationAppList(Ljava/lang/String;)V

    .line 113
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSUtility;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    sget v1, Lcom/samsung/klmsagent/util/KLMSConstant;->NOTIFICATION_STACK_ID:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 116
    const/16 v0, 0xc8

    const/16 v1, 0x3e8

    const/4 v2, -0x1

    const/16 v3, 0x321

    const-string v4, "success"

    invoke-static/range {v0 .. v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 121
    const/16 v0, 0x5a

    invoke-static {v0, v5}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->checkLicenseStatusToSendContainer(ILjava/lang/String;)V

    goto :goto_0

    .line 125
    :pswitch_1
    const-string v0, "State(): takeAction() : License state is DEACTIVE."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 127
    invoke-static/range {p1 .. p1}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->getMDMId(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/context/State;->deactivationProcess(Ljava/lang/String;)V

    .line 130
    const/16 v0, 0xc8

    const/16 v1, 0xc1e

    const/4 v2, -0x1

    const/16 v3, 0x321

    const-string v4, "fail"

    invoke-static/range {v0 .. v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 136
    :pswitch_2
    const-string v0, "State(): takeAction() : License state is EXPIRED."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 139
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->addNotificationAppList(Ljava/lang/String;)V

    .line 142
    invoke-static {}, Lcom/samsung/klmsagent/data/DataSourceManager;->getDataSource()Lcom/samsung/klmsagent/data/DataSource;

    move-result-object v6

    .line 144
    .local v6, "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    const/4 v0, 0x1

    :try_start_0
    new-array v13, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v5, v13, v0

    .line 145
    .local v13, "whereArgs":[Ljava/lang/String;
    new-instance v7, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-direct {v7}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 146
    .local v7, "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v7, v5}, Lcom/samsung/klmsagent/beans/DeviceData;->setPackageName(Ljava/lang/String;)V

    .line 148
    const-string v0, "PACKAGE_NAME=?"

    const-string v1, "ASC"

    invoke-virtual {v6, v0, v7, v1, v13}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 150
    .local v8, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/beans/DeviceData;

    invoke-virtual {v0}, Lcom/samsung/klmsagent/beans/DeviceData;->getTrackerId()Ljava/lang/String;

    move-result-object v12

    .line 153
    .local v12, "trackerId":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v14, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v12, v14, v0

    .line 154
    .local v14, "whereArgsTracker":[Ljava/lang/String;
    new-instance v7, Lcom/samsung/klmsagent/beans/DeviceData;

    .end local v7    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-direct {v7}, Lcom/samsung/klmsagent/beans/DeviceData;-><init>()V

    .line 155
    .restart local v7    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v7, v12}, Lcom/samsung/klmsagent/beans/DeviceData;->setTrackerId(Ljava/lang/String;)V

    .line 156
    const-string v0, "TRACKER_ID=?"

    const-string v1, "ASC"

    invoke-virtual {v6, v0, v7, v1, v14}, Lcom/samsung/klmsagent/data/DataSource;->fetchData(Ljava/lang/String;Lcom/samsung/klmsagent/beans/KLMSComponent;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 158
    if-eqz v8, :cond_1

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/klmsagent/beans/DeviceData;

    .line 160
    .local v9, "devicedata":Lcom/samsung/klmsagent/beans/DeviceData;
    invoke-virtual {v9}, Lcom/samsung/klmsagent/beans/DeviceData;->getTrackerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    const-string v0, "State(): Same tracker ID Found. Add Notification"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->u(Ljava/lang/String;)V

    .line 162
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->getInstance()Lcom/samsung/klmsagent/util/KLMSSharedPreferences;

    move-result-object v0

    invoke-virtual {v9}, Lcom/samsung/klmsagent/beans/DeviceData;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/klmsagent/util/KLMSSharedPreferences;->addNotificationAppList(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 166
    .end local v7    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v8    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v9    # "devicedata":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v12    # "trackerId":Ljava/lang/String;
    .end local v13    # "whereArgs":[Ljava/lang/String;
    .end local v14    # "whereArgsTracker":[Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 167
    .local v10, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "Common Unkown Exception."

    invoke-static {v0, v10}, Lcom/samsung/klmsagent/util/KLMSLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169
    invoke-virtual {v6}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    .line 173
    .end local v10    # "e":Ljava/lang/Exception;
    :goto_2
    const/16 v0, 0xc8

    const/16 v1, 0xc1d

    const/4 v2, -0x1

    const/16 v3, 0x321

    const-string v4, "fail"

    invoke-static/range {v0 .. v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    .line 178
    const/16 v0, 0x32

    invoke-static {v0, v5}, Lcom/samsung/klmsagent/services/i/ContainerStateImpl;->checkLicenseStatusToSendContainer(ILjava/lang/String;)V

    .line 180
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060032

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSUtility;->showNotification(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 169
    .restart local v7    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .restart local v8    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .restart local v12    # "trackerId":Ljava/lang/String;
    .restart local v13    # "whereArgs":[Ljava/lang/String;
    .restart local v14    # "whereArgsTracker":[Ljava/lang/String;
    :cond_1
    invoke-virtual {v6}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    goto :goto_2

    .end local v7    # "deviceData":Lcom/samsung/klmsagent/beans/DeviceData;
    .end local v8    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/klmsagent/beans/DeviceData;>;"
    .end local v12    # "trackerId":Ljava/lang/String;
    .end local v13    # "whereArgs":[Ljava/lang/String;
    .end local v14    # "whereArgsTracker":[Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Lcom/samsung/klmsagent/data/DataSource;->close()V

    throw v0

    .line 184
    .end local v6    # "dataSource":Lcom/samsung/klmsagent/data/DataSource;
    :pswitch_3
    const-string v0, "State(): takeAction() : License state is terminated"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 186
    invoke-static/range {p1 .. p1}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->getMDMId(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/klmsagent/context/State;->deactivationProcess(Ljava/lang/String;)V

    .line 189
    const/16 v0, 0xc8

    const/16 v1, 0xc20

    const/4 v2, -0x1

    const/16 v3, 0x321

    const-string v4, "fail"

    invoke-static/range {v0 .. v5}, Lcom/samsung/klmsagent/services/i/KLMSValidator;->processError(IIIILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public takeActionGeneral()V
    .locals 3

    .prologue
    const v2, 0x7f060016

    .line 276
    sget-object v0, Lcom/samsung/klmsagent/context/State$1;->$SwitchMap$com$samsung$klmsagent$context$KLMSContextManager$LicenseStatus:[I

    invoke-virtual {p0}, Lcom/samsung/klmsagent/context/State;->getCurrentState()Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 344
    const-string v0, "State(): takeActionGeneral() : License state is not active."

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 347
    :goto_0
    return-void

    .line 278
    :pswitch_0
    const-string v0, "State(): takeActionGeneral() : ACTIVE"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 279
    invoke-static {}, Lcom/samsung/klmsagent/util/KLMSUtility;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    sget v1, Lcom/samsung/klmsagent/util/KLMSConstant;->NOTIFICATION_STACK_ID:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 280
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06001d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 284
    :pswitch_1
    const-string v0, "State(): takeActionGeneral() : UI_FORMAT_ERROR"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 285
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06001c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 289
    :pswitch_2
    const-string v0, "State(): takeActionGeneral() : EULA_NOT_AGRRED"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 290
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06001e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 294
    :pswitch_3
    const-string v0, "State(): takeActionGeneral() : NO_NETWORK"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 295
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f060020

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 299
    :pswitch_4
    const-string v0, "State(): takeActionGeneral() : SMS_FORMAT_ERROR"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 300
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 304
    :pswitch_5
    const-string v0, "State(): takeActionGeneral() : INTERNAl_SERVER_ERROR"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 305
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 309
    :pswitch_6
    const-string v0, "State(): takeActionGeneral() : HTTP_ERROR"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 310
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 314
    :pswitch_7
    const-string v0, "State(): takeActionGeneral() : DATE_ERROR"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 315
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 319
    :pswitch_8
    const-string v0, "State(): takeActionGeneral() : INVALID"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 320
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 324
    :pswitch_9
    const-string v0, "State(): takeActionGeneral() : EXPIRED"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 325
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 329
    :pswitch_a
    const-string v0, "State(): takeActionGeneral() : DEACTIVE"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 330
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 334
    :pswitch_b
    const-string v0, "State(): takeActionGeneral() : TERMINATED"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 335
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 339
    :pswitch_c
    const-string v0, "State(): takeActionGeneral() : QUANTITY_OVER"

    invoke-static {v0}, Lcom/samsung/klmsagent/util/KLMSLogger;->d(Ljava/lang/String;)V

    .line 340
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/klmsagent/util/KLMSLogger;->popupNew(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 276
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_a
        :pswitch_9
        :pswitch_b
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_c
    .end packed-switch
.end method
