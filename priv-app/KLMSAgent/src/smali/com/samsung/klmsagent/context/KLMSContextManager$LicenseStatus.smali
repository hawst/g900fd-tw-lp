.class public final enum Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
.super Ljava/lang/Enum;
.source "KLMSContextManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/klmsagent/context/KLMSContextManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LicenseStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum DATE_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum DEACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum EULA_NOT_AGRRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum EXPIRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum HTTP_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum INTERNAl_SERVER_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum INVALID:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum NO_NETWORK:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum QUANTITY_OVER:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum SMS_FORMAT_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum TERMINATED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

.field public static final enum UI_FORMAT_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 68
    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "ACTIVE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "DEACTIVE"

    invoke-direct {v0, v1, v4, v5}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->DEACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5, v6}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->EXPIRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "TERMINATED"

    invoke-direct {v0, v1, v6, v7}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->TERMINATED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v7, v8}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->INVALID:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "QUANTITY_OVER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->QUANTITY_OVER:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "INTERNAl_SERVER_ERROR"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->INTERNAl_SERVER_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "EULA_NOT_AGRRED"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->EULA_NOT_AGRRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    .line 69
    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "SMS_FORMAT_ERROR"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->SMS_FORMAT_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "UI_FORMAT_ERROR"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->UI_FORMAT_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "NO_NETWORK"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->NO_NETWORK:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "HTTP_ERROR"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->HTTP_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    new-instance v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const-string v1, "DATE_ERROR"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->DATE_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    .line 67
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v2, v0, v1

    sget-object v1, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->DEACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->EXPIRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->TERMINATED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->INVALID:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->QUANTITY_OVER:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->INTERNAl_SERVER_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->EULA_NOT_AGRRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->SMS_FORMAT_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->UI_FORMAT_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->NO_NETWORK:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->HTTP_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->DATE_ERROR:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->$VALUES:[Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    iput p3, p0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->value:I

    .line 73
    return-void
.end method

.method public static fromInt(I)Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    .locals 5
    .param p0, "i"    # I

    .prologue
    .line 83
    if-lez p0, :cond_1

    const/4 v4, 0x5

    if-ge p0, v4, :cond_1

    .line 84
    invoke-static {}, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->values()[Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 85
    .local v3, "status":Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    iget v4, v3, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->value:I

    if-ne p0, v4, :cond_0

    .line 90
    .end local v0    # "arr$":[Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "status":Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    :goto_1
    return-object v3

    .line 84
    .restart local v0    # "arr$":[Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "status":Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 90
    .end local v0    # "arr$":[Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "status":Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static getCodeByValue(I)Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 102
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    .line 103
    .local v0, "code":Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    packed-switch p0, :pswitch_data_0

    .line 120
    :goto_0
    return-object v0

    .line 105
    :pswitch_0
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->ACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    .line 106
    goto :goto_0

    .line 108
    :pswitch_1
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->DEACTIVE:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    .line 109
    goto :goto_0

    .line 111
    :pswitch_2
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->EXPIRED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    .line 112
    goto :goto_0

    .line 114
    :pswitch_3
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->TERMINATED:Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    .line 115
    goto :goto_0

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 67
    const-class v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    return-object v0
.end method

.method public static values()[Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->$VALUES:[Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    invoke-virtual {v0}, [Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->value:I

    return v0
.end method

.method public setCode(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->value:I

    .line 99
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/samsung/klmsagent/context/KLMSContextManager$LicenseStatus;->value:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
