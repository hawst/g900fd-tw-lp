.class public final Lcom/google/android/gms/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static adSize:I

.field public static adUnitId:I

.field public static cameraBearing:I

.field public static cameraTargetLat:I

.field public static cameraTargetLng:I

.field public static cameraTilt:I

.field public static cameraZoom:I

.field public static mapType:I

.field public static uiCompass:I

.field public static uiRotateGestures:I

.field public static uiScrollGestures:I

.field public static uiTiltGestures:I

.field public static uiZoomControls:I

.field public static uiZoomGestures:I

.field public static useViewLifecycle:I

.field public static zOrderOnTop:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/high16 v0, 0x7f010000

    sput v0, Lcom/google/android/gms/R$attr;->adSize:I

    .line 33
    const v0, 0x7f010001

    sput v0, Lcom/google/android/gms/R$attr;->adUnitId:I

    .line 41
    const v0, 0x7f010003

    sput v0, Lcom/google/android/gms/R$attr;->cameraBearing:I

    .line 49
    const v0, 0x7f010004

    sput v0, Lcom/google/android/gms/R$attr;->cameraTargetLat:I

    .line 57
    const v0, 0x7f010005

    sput v0, Lcom/google/android/gms/R$attr;->cameraTargetLng:I

    .line 65
    const v0, 0x7f010006

    sput v0, Lcom/google/android/gms/R$attr;->cameraTilt:I

    .line 73
    const v0, 0x7f010007

    sput v0, Lcom/google/android/gms/R$attr;->cameraZoom:I

    .line 87
    const v0, 0x7f010002

    sput v0, Lcom/google/android/gms/R$attr;->mapType:I

    .line 95
    const v0, 0x7f010008

    sput v0, Lcom/google/android/gms/R$attr;->uiCompass:I

    .line 103
    const v0, 0x7f010009

    sput v0, Lcom/google/android/gms/R$attr;->uiRotateGestures:I

    .line 111
    const v0, 0x7f01000a

    sput v0, Lcom/google/android/gms/R$attr;->uiScrollGestures:I

    .line 119
    const v0, 0x7f01000b

    sput v0, Lcom/google/android/gms/R$attr;->uiTiltGestures:I

    .line 127
    const v0, 0x7f01000c

    sput v0, Lcom/google/android/gms/R$attr;->uiZoomControls:I

    .line 135
    const v0, 0x7f01000d

    sput v0, Lcom/google/android/gms/R$attr;->uiZoomGestures:I

    .line 143
    const v0, 0x7f01000e

    sput v0, Lcom/google/android/gms/R$attr;->useViewLifecycle:I

    .line 151
    const v0, 0x7f01000f

    sput v0, Lcom/google/android/gms/R$attr;->zOrderOnTop:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
