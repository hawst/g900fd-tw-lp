.class public Lcom/quramsoft/agif/QURAMWINKUTIL;
.super Ljava/lang/Object;
.source "QURAMWINKUTIL.java"


# static fields
.field public static final MAXSUPPORTAGIFSIZE:I = 0x1e00000

.field private static final TAG:Ljava/lang/String;

.field public static httpget:Lorg/apache/http/client/methods/HttpGet;


# instance fields
.field public AGIF_MODE:Z

.field public isReadyToDraw:Z

.field public mAllshare:Z

.field public mBeforBitmap:Landroid/graphics/Bitmap;

.field public mCanvas:Landroid/graphics/Canvas;

.field private mDuration:J

.field public mLoadGIF:Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;

.field private mMovie:Landroid/graphics/Movie;

.field private mMovieStart:J

.field public m_array:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/quramsoft/agif/QURAMWINKUTIL;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    .line 169
    const/4 v0, 0x0

    sput-object v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->httpget:Lorg/apache/http/client/methods/HttpGet;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->AGIF_MODE:Z

    .line 54
    iput-object v1, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mBeforBitmap:Landroid/graphics/Bitmap;

    .line 55
    iput-object v1, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mCanvas:Landroid/graphics/Canvas;

    .line 56
    iput-object v1, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    .line 57
    iput-wide v4, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 58
    iput-wide v4, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mDuration:J

    .line 60
    iput-boolean v2, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mAllshare:Z

    .line 62
    iput-object v1, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B

    .line 167
    iput-boolean v2, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->isReadyToDraw:Z

    .line 168
    iput-object v1, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mLoadGIF:Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;

    .line 71
    return-void
.end method

.method static final create()Lcom/quramsoft/agif/QURAMWINKUTIL;
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/quramsoft/agif/QURAMWINKUTIL;

    invoke-direct {v0}, Lcom/quramsoft/agif/QURAMWINKUTIL;-><init>()V

    .line 66
    .local v0, "newInstance":Lcom/quramsoft/agif/QURAMWINKUTIL;
    return-object v0
.end method

.method public static isAGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v9, 0x0

    .line 411
    if-nez p1, :cond_0

    move v5, v9

    .line 475
    :goto_0
    return v5

    .line 414
    :cond_0
    const/4 v5, 0x0

    .line 416
    .local v5, "isAGIF":Z
    monitor-enter p1

    .line 417
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getAgifMode()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 418
    const/4 v9, 0x1

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v5, v9

    goto :goto_0

    .line 421
    :cond_1
    const-wide/16 v2, 0x0

    .line 422
    .local v2, "duration":J
    const/4 v8, 0x0

    .line 425
    .local v8, "uriInputStream":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v10

    const-string v11, "gif"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 426
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v8

    .line 428
    if-nez v8, :cond_2

    .line 429
    sget-object v10, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v11, "InputStream is null!"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 472
    :try_start_2
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v5, v9

    goto :goto_0

    .line 433
    :cond_2
    :try_start_3
    invoke-virtual {v8}, Ljava/io/InputStream;->available()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v7

    .line 435
    .local v7, "streamSize":I
    if-lez v7, :cond_3

    .line 437
    :try_start_4
    invoke-static {v8}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v6

    .line 438
    .local v6, "mMovieTemp":Landroid/graphics/Movie;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/graphics/Movie;->duration()I

    move-result v9

    if-lez v9, :cond_3

    .line 439
    sget-object v9, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v10, "isAGIF Decode Agif  Success"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    invoke-virtual {v6}, Landroid/graphics/Movie;->duration()I
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v9

    int-to-long v2, v9

    .line 467
    .end local v6    # "mMovieTemp":Landroid/graphics/Movie;
    :cond_3
    :goto_1
    const-wide/16 v10, 0x0

    cmp-long v9, v2, v10

    if-lez v9, :cond_4

    .line 468
    const/4 v5, 0x1

    .line 472
    .end local v7    # "streamSize":I
    :cond_4
    :try_start_5
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 474
    :goto_2
    monitor-exit p1

    goto :goto_0

    .end local v2    # "duration":J
    .end local v8    # "uriInputStream":Ljava/io/InputStream;
    :catchall_0
    move-exception v9

    monitor-exit p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v9

    .line 443
    .restart local v2    # "duration":J
    .restart local v7    # "streamSize":I
    .restart local v8    # "uriInputStream":Ljava/io/InputStream;
    :catch_0
    move-exception v4

    .line 444
    .local v4, "e":Ljava/lang/OutOfMemoryError;
    const/high16 v9, 0x1e00000

    if-le v7, v9, :cond_5

    .line 445
    const/high16 v7, 0x1e00000

    .line 448
    :cond_5
    :try_start_6
    invoke-static {v8, v7}, Lcom/quramsoft/agif/QURAMWINKUTIL;->streamToBytes(Ljava/io/InputStream;I)[B

    move-result-object v0

    .line 450
    .local v0, "array":[B
    if-eqz v0, :cond_6

    .line 452
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 453
    .local v1, "byteInputStream":Ljava/io/InputStream;
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Ljava/io/InputStream;->mark(I)V

    .line 454
    invoke-static {v1}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v6

    .line 455
    .restart local v6    # "mMovieTemp":Landroid/graphics/Movie;
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 457
    const/4 v0, 0x0

    .line 458
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/graphics/Movie;->duration()I

    move-result v9

    if-lez v9, :cond_3

    .line 459
    sget-object v9, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v10, "isAGIF Decode Agif  Success"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    invoke-virtual {v6}, Landroid/graphics/Movie;->duration()I

    move-result v9

    int-to-long v2, v9

    goto :goto_1

    .line 463
    .end local v1    # "byteInputStream":Ljava/io/InputStream;
    .end local v6    # "mMovieTemp":Landroid/graphics/Movie;
    :cond_6
    sget-object v9, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v10, "AGIF stream is null"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 470
    .end local v0    # "array":[B
    .end local v4    # "e":Ljava/lang/OutOfMemoryError;
    .end local v7    # "streamSize":I
    :catch_1
    move-exception v9

    .line 472
    :try_start_7
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_2

    :catchall_1
    move-exception v9

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v9
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method

.method public static streamToBytes(Ljava/io/InputStream;I)[B
    .locals 4
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "streamSize"    # I

    .prologue
    .line 101
    new-array v0, p1, [B

    .line 102
    .local v0, "buffer":[B
    const/4 v2, -0x1

    .line 104
    .local v2, "readsize":I
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v3, p1}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 109
    :goto_0
    if-eq v2, p1, :cond_0

    .line 110
    const/4 v0, 0x0

    .line 111
    :cond_0
    return-object v0

    .line 106
    :catch_0
    move-exception v1

    .line 107
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static streamToBytesAllShare(Ljava/io/InputStream;I)[B
    .locals 11
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "streamSize"    # I

    .prologue
    const/4 v10, 0x0

    .line 115
    new-array v0, p1, [B

    .line 117
    .local v0, "buffer":[B
    move v4, p1

    .line 118
    .local v4, "length":I
    const/4 v7, 0x0

    .line 119
    .local v7, "read":I
    :cond_0
    if-ge v7, v4, :cond_2

    .line 122
    sub-int v8, v4, v7

    const/16 v9, 0x2000

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 123
    .local v3, "l":I
    const/4 v6, 0x0

    .line 124
    .local v6, "pos":I
    :goto_0
    if-ge v6, v3, :cond_0

    .line 125
    const/4 v1, 0x0

    .line 127
    .local v1, "c":I
    sub-int v8, v3, v6

    :try_start_0
    invoke-virtual {p0, v0, v7, v8}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 132
    :goto_1
    const/4 v8, -0x1

    if-ne v1, v8, :cond_1

    .line 133
    new-array v5, v7, [B

    .line 134
    .local v5, "newBuffer":[B
    invoke-static {v0, v10, v5, v10, v7}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 144
    .end local v1    # "c":I
    .end local v3    # "l":I
    .end local v6    # "pos":I
    :goto_2
    return-object v5

    .line 128
    .end local v5    # "newBuffer":[B
    .restart local v1    # "c":I
    .restart local v3    # "l":I
    .restart local v6    # "pos":I
    :catch_0
    move-exception v2

    .line 130
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 137
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    add-int/2addr v6, v1

    .line 138
    add-int/2addr v7, v1

    .line 139
    goto :goto_0

    .line 142
    .end local v1    # "c":I
    .end local v3    # "l":I
    .end local v6    # "pos":I
    :cond_2
    new-array v5, v7, [B

    .line 143
    .restart local v5    # "newBuffer":[B
    invoke-static {v0, v10, v5, v10, v7}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_2
.end method


# virtual methods
.method public initAGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;Z)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "isAllShare"    # Z

    .prologue
    const/4 v2, 0x0

    .line 149
    if-eqz p2, :cond_0

    .line 150
    monitor-enter p2

    .line 151
    const/4 v1, 0x0

    .line 153
    .local v1, "isGIF":Z
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/quramsoft/agif/QURAMWINKUTIL;->loadGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 158
    :try_start_1
    invoke-virtual {p2, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->resetAgifMode(Z)V

    .line 159
    monitor-exit p2

    .line 163
    .end local v1    # "isGIF":Z
    :goto_0
    return v1

    .line 154
    .restart local v1    # "isGIF":Z
    :catch_0
    move-exception v0

    .line 155
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Load agif fail"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    monitor-exit p2

    move v1, v2

    goto :goto_0

    .line 160
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 162
    .end local v1    # "isGIF":Z
    :cond_0
    sget-object v3, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v4, "AGIF item is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 163
    goto :goto_0
.end method

.method public loadGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;Z)Z
    .locals 26
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "isAllShare"    # Z

    .prologue
    .line 171
    const/16 v18, 0x0

    .line 172
    .local v18, "ret":Z
    const/4 v15, 0x0

    .line 174
    .local v15, "mis":Ljava/io/InputStream;
    if-nez p2, :cond_0

    .line 175
    const/16 v21, 0x0

    .line 320
    :goto_0
    return v21

    .line 177
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v21

    if-eqz v21, :cond_6

    .line 178
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "WINK AGIF Start Decode DRM"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v16

    .line 182
    .local v16, "pathName":Ljava/lang/String;
    new-instance v6, Landroid/drm/DrmManagerClient;

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-direct {v6, v0}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    .line 183
    .local v6, "drmclient":Landroid/drm/DrmManagerClient;
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 184
    .local v8, "fileSize":J
    const/16 v21, 0x7

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;I)I

    move-result v19

    .line 186
    .local v19, "rightStatus":I
    const/4 v11, 0x1

    .line 189
    .local v11, "isPreview":Z
    if-nez v19, :cond_5

    .line 190
    new-instance v5, Landroid/drm/DrmInfoRequest;

    const/16 v21, 0xa

    const-string v22, "application/vnd.oma.drm.content"

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v5, v0, v1}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 193
    .local v5, "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    const-string v21, "drm_path"

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v5, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 194
    const-string v21, "LENGTH"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v5, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 195
    const-string v22, "preview_option"

    if-eqz v11, :cond_1

    const-string/jumbo v21, "true"

    :goto_1
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 202
    invoke-virtual {v6, v5}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v17

    .line 204
    .local v17, "resultInfo":Landroid/drm/DrmInfo;
    if-nez v17, :cond_2

    .line 205
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "DrmInfo resultInfo is null"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    const/16 v21, 0x0

    goto :goto_0

    .line 195
    .end local v17    # "resultInfo":Landroid/drm/DrmInfo;
    :cond_1
    const-string v21, "false"

    goto :goto_1

    .line 208
    .restart local v17    # "resultInfo":Landroid/drm/DrmInfo;
    :cond_2
    const-string/jumbo v21, "status"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    .line 210
    .local v20, "status_req1":Ljava/lang/String;
    const-string/jumbo v21, "success"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 211
    invoke-virtual/range {v17 .. v17}, Landroid/drm/DrmInfo;->getData()[B

    move-result-object v21

    if-eqz v21, :cond_3

    .line 214
    invoke-virtual/range {v17 .. v17}, Landroid/drm/DrmInfo;->getData()[B

    move-result-object v4

    .line 215
    .local v4, "array":[B
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "array size = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    array-length v0, v4

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "DRM decrypt Complete!!"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/quramsoft/agif/QURAMWINKUTIL;->streamMaxBound([B)Ljava/io/InputStream;

    move-result-object v15

    .line 232
    .end local v4    # "array":[B
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/quramsoft/agif/QURAMWINKUTIL;->setGIFtoMovie(Ljava/io/InputStream;)Z

    move-result v18

    .line 236
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "WINK AGIF Decode DRM complete"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .end local v5    # "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    .end local v6    # "drmclient":Landroid/drm/DrmManagerClient;
    .end local v8    # "fileSize":J
    .end local v11    # "isPreview":Z
    .end local v16    # "pathName":Ljava/lang/String;
    .end local v17    # "resultInfo":Landroid/drm/DrmInfo;
    .end local v19    # "rightStatus":I
    .end local v20    # "status_req1":Ljava/lang/String;
    :goto_3
    move/from16 v21, v18

    .line 320
    goto/16 :goto_0

    .line 220
    .restart local v5    # "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    .restart local v6    # "drmclient":Landroid/drm/DrmManagerClient;
    .restart local v8    # "fileSize":J
    .restart local v11    # "isPreview":Z
    .restart local v16    # "pathName":Ljava/lang/String;
    .restart local v17    # "resultInfo":Landroid/drm/DrmInfo;
    .restart local v19    # "rightStatus":I
    .restart local v20    # "status_req1":Ljava/lang/String;
    :cond_3
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "DRM decrypt Faild"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    const/4 v15, 0x0

    goto :goto_2

    .line 225
    :cond_4
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "DRM decrypt Faild"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    const/4 v15, 0x0

    goto :goto_2

    .line 229
    .end local v5    # "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    .end local v17    # "resultInfo":Landroid/drm/DrmInfo;
    .end local v20    # "status_req1":Ljava/lang/String;
    :cond_5
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "QURAMWINK AGIF - RightsStatus is invalid"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 238
    .end local v6    # "drmclient":Landroid/drm/DrmManagerClient;
    .end local v8    # "fileSize":J
    .end local v11    # "isPreview":Z
    .end local v16    # "pathName":Ljava/lang/String;
    .end local v19    # "rightStatus":I
    :cond_6
    const/16 v21, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    .line 239
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 240
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mDuration:J

    .line 242
    if-eqz p3, :cond_8

    .line 244
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/quramsoft/agif/QURAMWINKUTIL;->mAllshare:Z

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mLoadGIF:Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;

    move-object/from16 v21, v0

    if-eqz v21, :cond_7

    .line 247
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mLoadGIF:Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->setTerminate()V

    .line 248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mLoadGIF:Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->interrupt()V

    .line 250
    :cond_7
    new-instance v21, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    sput-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->httpget:Lorg/apache/http/client/methods/HttpGet;

    .line 252
    new-instance v21, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;

    invoke-direct/range {v21 .. v21}, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/quramsoft/agif/QURAMWINKUTIL;->mLoadGIF:Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mLoadGIF:Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->start()V

    .line 255
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 261
    :cond_8
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/quramsoft/agif/QURAMWINKUTIL;->mAllshare:Z

    .line 263
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 264
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mDuration:J

    .line 265
    const/4 v13, 0x0

    .line 267
    .local v13, "m_is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v13

    .line 273
    const/4 v14, 0x0

    .line 275
    .local v14, "m_streamSize":I
    if-eqz v13, :cond_a

    .line 276
    :try_start_1
    invoke-virtual {v13}, Ljava/io/InputStream;->available()I

    move-result v14

    .line 278
    const/high16 v21, 0x1e00000

    move/from16 v0, v21

    if-le v14, v0, :cond_9

    .line 279
    const/high16 v14, 0x1e00000

    .line 281
    :cond_9
    invoke-static {v13, v14}, Lcom/quramsoft/agif/QURAMWINKUTIL;->streamToBytes(Ljava/io/InputStream;I)[B

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292
    :cond_a
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B

    move-object/from16 v21, v0

    if-eqz v21, :cond_d

    .line 296
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 299
    new-instance v10, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-direct {v10, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 300
    .local v10, "in":Ljava/io/InputStream;
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/io/InputStream;->mark(I)V

    .line 301
    invoke-static {v10}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v12

    .line 303
    .local v12, "mMovieTemp":Landroid/graphics/Movie;
    const/16 v21, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B

    .line 304
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    .line 305
    if-nez v12, :cond_c

    .line 306
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "Decode AGIF Failed"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    :cond_b
    :goto_4
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 318
    .end local v10    # "in":Ljava/io/InputStream;
    .end local v12    # "mMovieTemp":Landroid/graphics/Movie;
    :goto_5
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mDuration:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmp-long v21, v22, v24

    if-eqz v21, :cond_18

    const/16 v18, 0x1

    :goto_6
    goto/16 :goto_3

    .line 283
    :catch_0
    move-exception v7

    .line 284
    .local v7, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 285
    const/16 v21, 0x0

    .line 292
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 286
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    .line 287
    .local v7, "e":Ljava/lang/OutOfMemoryError;
    :try_start_3
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "low memory - force GC"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    invoke-virtual {v7}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 289
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 290
    const/16 v21, 0x0

    .line 292
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .end local v7    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v21

    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v21

    .line 307
    .restart local v10    # "in":Ljava/io/InputStream;
    .restart local v12    # "mMovieTemp":Landroid/graphics/Movie;
    :cond_c
    invoke-virtual {v12}, Landroid/graphics/Movie;->duration()I

    move-result v21

    if-lez v21, :cond_b

    .line 308
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "Decode Agif  Success"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 310
    invoke-virtual {v12}, Landroid/graphics/Movie;->duration()I

    move-result v21

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mDuration:J

    goto :goto_4

    .line 314
    .end local v10    # "in":Ljava/io/InputStream;
    .end local v12    # "mMovieTemp":Landroid/graphics/Movie;
    :cond_d
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "AGIF stream is null"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 270
    .end local v14    # "m_streamSize":I
    :catch_2
    move-exception v7

    .line 271
    .local v7, "e":Ljava/io/FileNotFoundException;
    :try_start_4
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "AGIF IO Exception cautch while opening stream"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 273
    const/4 v14, 0x0

    .line 275
    .restart local v14    # "m_streamSize":I
    if-eqz v13, :cond_f

    .line 276
    :try_start_5
    invoke-virtual {v13}, Ljava/io/InputStream;->available()I

    move-result v14

    .line 278
    const/high16 v21, 0x1e00000

    move/from16 v0, v21

    if-le v14, v0, :cond_e

    .line 279
    const/high16 v14, 0x1e00000

    .line 281
    :cond_e
    invoke-static {v13, v14}, Lcom/quramsoft/agif/QURAMWINKUTIL;->streamToBytes(Ljava/io/InputStream;I)[B

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 292
    :cond_f
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B

    move-object/from16 v21, v0

    if-eqz v21, :cond_12

    .line 296
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 299
    new-instance v10, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-direct {v10, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 300
    .restart local v10    # "in":Ljava/io/InputStream;
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/io/InputStream;->mark(I)V

    .line 301
    invoke-static {v10}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v12

    .line 303
    .restart local v12    # "mMovieTemp":Landroid/graphics/Movie;
    const/16 v21, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B

    .line 304
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    .line 305
    if-nez v12, :cond_11

    .line 306
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "Decode AGIF Failed"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    :cond_10
    :goto_7
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_5

    .line 283
    .end local v10    # "in":Ljava/io/InputStream;
    .end local v12    # "mMovieTemp":Landroid/graphics/Movie;
    :catch_3
    move-exception v7

    .line 284
    .local v7, "e":Ljava/io/IOException;
    :try_start_6
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 285
    const/16 v21, 0x0

    .line 292
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 286
    .local v7, "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v7

    .line 287
    .local v7, "e":Ljava/lang/OutOfMemoryError;
    :try_start_7
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "low memory - force GC"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    invoke-virtual {v7}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 289
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 290
    const/16 v21, 0x0

    .line 292
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .end local v7    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_1
    move-exception v21

    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v21

    .line 307
    .local v7, "e":Ljava/io/FileNotFoundException;
    .restart local v10    # "in":Ljava/io/InputStream;
    .restart local v12    # "mMovieTemp":Landroid/graphics/Movie;
    :cond_11
    invoke-virtual {v12}, Landroid/graphics/Movie;->duration()I

    move-result v21

    if-lez v21, :cond_10

    .line 308
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "Decode Agif  Success"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 310
    invoke-virtual {v12}, Landroid/graphics/Movie;->duration()I

    move-result v21

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mDuration:J

    goto :goto_7

    .line 314
    .end local v10    # "in":Ljava/io/InputStream;
    .end local v12    # "mMovieTemp":Landroid/graphics/Movie;
    :cond_12
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "AGIF stream is null"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 273
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    .end local v14    # "m_streamSize":I
    :catchall_2
    move-exception v21

    const/4 v14, 0x0

    .line 275
    .restart local v14    # "m_streamSize":I
    if-eqz v13, :cond_14

    .line 276
    :try_start_8
    invoke-virtual {v13}, Ljava/io/InputStream;->available()I

    move-result v14

    .line 278
    const/high16 v22, 0x1e00000

    move/from16 v0, v22

    if-le v14, v0, :cond_13

    .line 279
    const/high16 v14, 0x1e00000

    .line 281
    :cond_13
    invoke-static {v13, v14}, Lcom/quramsoft/agif/QURAMWINKUTIL;->streamToBytes(Ljava/io/InputStream;I)[B

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 292
    :cond_14
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B

    move-object/from16 v22, v0

    if-eqz v22, :cond_17

    .line 296
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 299
    new-instance v10, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v10, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 300
    .restart local v10    # "in":Ljava/io/InputStream;
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/io/InputStream;->mark(I)V

    .line 301
    invoke-static {v10}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v12

    .line 303
    .restart local v12    # "mMovieTemp":Landroid/graphics/Movie;
    const/16 v22, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/quramsoft/agif/QURAMWINKUTIL;->m_array:[B

    .line 304
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    .line 305
    if-nez v12, :cond_16

    .line 306
    sget-object v22, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v23, "Decode AGIF Failed"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    :cond_15
    :goto_8
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 316
    .end local v10    # "in":Ljava/io/InputStream;
    .end local v12    # "mMovieTemp":Landroid/graphics/Movie;
    :goto_9
    throw v21

    .line 283
    :catch_5
    move-exception v7

    .line 284
    .local v7, "e":Ljava/io/IOException;
    :try_start_9
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 285
    const/16 v21, 0x0

    .line 292
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 286
    .end local v7    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v7

    .line 287
    .local v7, "e":Ljava/lang/OutOfMemoryError;
    :try_start_a
    sget-object v21, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v22, "low memory - force GC"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    invoke-virtual {v7}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 289
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 290
    const/16 v21, 0x0

    .line 292
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .end local v7    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_3
    move-exception v21

    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v21

    .line 307
    .restart local v10    # "in":Ljava/io/InputStream;
    .restart local v12    # "mMovieTemp":Landroid/graphics/Movie;
    :cond_16
    invoke-virtual {v12}, Landroid/graphics/Movie;->duration()I

    move-result v22

    if-lez v22, :cond_15

    .line 308
    sget-object v22, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v23, "Decode Agif  Success"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 310
    invoke-virtual {v12}, Landroid/graphics/Movie;->duration()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->mDuration:J

    goto :goto_8

    .line 314
    .end local v10    # "in":Ljava/io/InputStream;
    .end local v12    # "mMovieTemp":Landroid/graphics/Movie;
    :cond_17
    sget-object v22, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v23, "AGIF stream is null"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 318
    :cond_18
    const/16 v18, 0x0

    goto/16 :goto_6
.end method

.method public setGIFtoMovie(Ljava/io/InputStream;)Z
    .locals 6
    .param p1, "mis"    # Ljava/io/InputStream;

    .prologue
    const-wide/16 v4, 0x0

    .line 324
    const/4 v0, 0x1

    .line 326
    .local v0, "isGIF":Z
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    .line 327
    iput-wide v4, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 328
    iput-wide v4, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mDuration:J

    .line 331
    if-eqz p1, :cond_3

    .line 332
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 334
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Ljava/io/InputStream;->mark(I)V

    .line 335
    invoke-static {p1}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v1

    .line 336
    .local v1, "mMovieTemp":Landroid/graphics/Movie;
    iput-object v1, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    .line 338
    if-nez v1, :cond_2

    .line 339
    sget-object v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v3, "Decode AGIF Failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    .end local v1    # "mMovieTemp":Landroid/graphics/Movie;
    :cond_0
    :goto_0
    iget-wide v2, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mDuration:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    .line 350
    const/4 v0, 0x0

    .line 352
    :cond_1
    return v0

    .line 340
    .restart local v1    # "mMovieTemp":Landroid/graphics/Movie;
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/Movie;->duration()I

    move-result v2

    if-lez v2, :cond_0

    .line 341
    sget-object v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v3, "Decode Agif  Success"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 343
    invoke-virtual {v1}, Landroid/graphics/Movie;->duration()I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mDuration:J

    goto :goto_0

    .line 346
    .end local v1    # "mMovieTemp":Landroid/graphics/Movie;
    :cond_3
    sget-object v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    const-string v3, "AGIF stream is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public streamMaxBound(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 4
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 86
    const/4 v1, 0x0

    .line 88
    .local v1, "length":I
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 92
    :goto_0
    if-eqz p1, :cond_0

    if-nez v1, :cond_1

    .line 93
    :cond_0
    const/4 v2, 0x0

    .line 96
    :goto_1
    return-object v2

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 94
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/high16 v2, 0x1e00000

    if-le v1, v2, :cond_2

    .line 95
    const/high16 v1, 0x1e00000

    .line 96
    :cond_2
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, p1, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    goto :goto_1
.end method

.method public streamMaxBound([B)Ljava/io/InputStream;
    .locals 3
    .param p1, "stream"    # [B

    .prologue
    .line 76
    if-eqz p1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_1

    .line 77
    :cond_0
    const/4 v1, 0x0

    .line 81
    :goto_0
    return-object v1

    .line 78
    :cond_1
    array-length v0, p1

    .line 79
    .local v0, "length":I
    const/high16 v1, 0x1e00000

    if-le v0, v1, :cond_2

    .line 80
    const/high16 v0, 0x1e00000

    .line 81
    :cond_2
    new-instance v1, Ljava/io/ByteArrayInputStream;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    goto :goto_0
.end method

.method public updateAGIF(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 18
    .param p1, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 357
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 358
    .local v10, "now":J
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-nez v13, :cond_0

    .line 359
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 361
    :cond_0
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovieStart:J

    .line 362
    .local v8, "movieStart":J
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mDuration:J

    .line 365
    .local v4, "duration":J
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mAllshare:Z

    if-eqz v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mLoadGIF:Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;

    invoke-virtual {v13}, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->isReadyToDraw()Z

    move-result v13

    if-nez v13, :cond_2

    .line 408
    :cond_1
    :goto_0
    return-void

    .line 368
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mAllshare:Z

    if-eqz v13, :cond_3

    .line 369
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mLoadGIF:Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;

    invoke-virtual {v13}, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->getMovieStart()J

    move-result-wide v8

    .line 370
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mLoadGIF:Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;

    invoke-virtual {v13}, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->getDuration()J

    move-result-wide v4

    .line 371
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mLoadGIF:Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;

    invoke-virtual {v13}, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->getMovie()Landroid/graphics/Movie;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    .line 377
    :cond_3
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    if-eqz v13, :cond_1

    const-wide/16 v14, 0x0

    cmp-long v13, v4, v14

    if-eqz v13, :cond_1

    .line 378
    sub-long v14, v10, v8

    rem-long/2addr v14, v4

    long-to-int v7, v14

    .line 380
    .local v7, "realTime":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v13, v7}, Landroid/graphics/Movie;->setTime(I)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 383
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v13}, Landroid/graphics/Movie;->width()I

    move-result v12

    .line 384
    .local v12, "width":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v13}, Landroid/graphics/Movie;->height()I

    move-result v6

    .line 385
    .local v6, "height":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mBeforBitmap:Landroid/graphics/Bitmap;

    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mCanvas:Landroid/graphics/Canvas;

    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mBeforBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    if-ne v12, v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mBeforBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    if-eq v6, v13, :cond_6

    .line 387
    :cond_4
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 388
    .local v2, "config":Landroid/graphics/Bitmap$Config;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mBeforBitmap:Landroid/graphics/Bitmap;

    if-eqz v13, :cond_5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mBeforBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v13

    if-nez v13, :cond_5

    .line 389
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mBeforBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 391
    :cond_5
    invoke-static {v12, v6, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mBeforBitmap:Landroid/graphics/Bitmap;

    .line 392
    new-instance v13, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mBeforBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v13, v14}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mCanvas:Landroid/graphics/Canvas;

    .line 394
    .end local v2    # "config":Landroid/graphics/Bitmap$Config;
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mCanvas:Landroid/graphics/Canvas;

    const/4 v14, 0x0

    sget-object v15, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v13, v14, v15}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 395
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mMovie:Landroid/graphics/Movie;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mCanvas:Landroid/graphics/Canvas;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Landroid/graphics/Movie;->draw(Landroid/graphics/Canvas;FF)V

    .line 396
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mCanvas:Landroid/graphics/Canvas;

    .line 397
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getTileImageView()Lcom/sec/android/gallery3d/ui/TileImageView;

    move-result-object v13

    new-instance v14, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/quramsoft/agif/QURAMWINKUTIL;->mBeforBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v14, v15}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v13, v14}, Lcom/sec/android/gallery3d/ui/TileImageView;->invalidateScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 404
    .end local v6    # "height":I
    .end local v7    # "realTime":I
    .end local v12    # "width":I
    :catch_0
    move-exception v3

    .line 405
    .local v3, "e":Ljava/lang/Exception;
    sget-object v13, Lcom/quramsoft/agif/QURAMWINKUTIL;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "AGIF updateAGIF fail :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
