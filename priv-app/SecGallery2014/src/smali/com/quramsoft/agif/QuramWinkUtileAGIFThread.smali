.class public Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;
.super Ljava/lang/Thread;
.source "QuramWinkUtileAGIFThread.java"


# instance fields
.field final TAG:Ljava/lang/String;

.field array:[B

.field duration:J

.field is:Ljava/io/InputStream;

.field isReadyToDraw:Z

.field mHttpClient:Landroid/net/http/AndroidHttpClient;

.field movie:Landroid/graphics/Movie;

.field movieStart:J

.field response:Lorg/apache/http/HttpResponse;

.field teminate:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 15
    const-string v0, "QuramWinkUtileAGIFThread"

    iput-object v0, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->TAG:Ljava/lang/String;

    .line 17
    iput-object v2, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->response:Lorg/apache/http/HttpResponse;

    .line 18
    const-string v0, "GIF Image"

    invoke-static {v0}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    .line 20
    iput-object v2, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->is:Ljava/io/InputStream;

    .line 21
    iput-object v2, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->array:[B

    .line 22
    iput-object v2, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->movie:Landroid/graphics/Movie;

    .line 23
    iput-wide v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->movieStart:J

    .line 24
    iput-wide v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->duration:J

    .line 26
    iput-boolean v1, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->teminate:Z

    .line 27
    iput-boolean v1, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->isReadyToDraw:Z

    .line 30
    iput-boolean v1, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->isReadyToDraw:Z

    .line 31
    iput-boolean v1, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->teminate:Z

    .line 32
    return-void
.end method


# virtual methods
.method public getDuration()J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->duration:J

    return-wide v0
.end method

.method public getMovie()Landroid/graphics/Movie;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->movie:Landroid/graphics/Movie;

    return-object v0
.end method

.method public getMovieStart()J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->movieStart:J

    return-wide v0
.end method

.method public isReadyToDraw()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->isReadyToDraw:Z

    return v0
.end method

.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 55
    iput-boolean v6, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->isReadyToDraw:Z

    .line 57
    :try_start_0
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    sget-object v5, Lcom/quramsoft/agif/QURAMWINKUTIL;->httpget:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v4, v5}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    iput-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->response:Lorg/apache/http/HttpResponse;

    .line 58
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->response:Lorg/apache/http/HttpResponse;

    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 59
    .local v2, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v2, :cond_0

    .line 61
    :try_start_1
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    iput-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->is:Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 67
    :cond_0
    :goto_0
    :try_start_2
    iget-boolean v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->teminate:Z

    if-eqz v4, :cond_2

    .line 68
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->is:Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v4, :cond_1

    .line 70
    :try_start_3
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->is:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 75
    :cond_1
    :goto_1
    :try_start_4
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 138
    .end local v2    # "entity":Lorg/apache/http/HttpEntity;
    :goto_2
    return-void

    .line 71
    .restart local v2    # "entity":Lorg/apache/http/HttpEntity;
    :catch_0
    move-exception v1

    .line 72
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 133
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "entity":Lorg/apache/http/HttpEntity;
    :catch_1
    move-exception v1

    .line 135
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 136
    iput-boolean v6, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->isReadyToDraw:Z

    goto :goto_2

    .line 79
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "entity":Lorg/apache/http/HttpEntity;
    :cond_2
    const/4 v3, 0x0

    .line 80
    .local v3, "m_streamSize":I
    const/high16 v3, 0x1e00000

    .line 83
    :try_start_5
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->is:Ljava/io/InputStream;

    invoke-static {v4, v3}, Lcom/quramsoft/agif/QURAMWINKUTIL;->streamToBytesAllShare(Ljava/io/InputStream;I)[B

    move-result-object v4

    iput-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->array:[B
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 91
    :try_start_6
    iget-boolean v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->teminate:Z

    if-eqz v4, :cond_4

    .line 92
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->is:Ljava/io/InputStream;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    if-eqz v4, :cond_3

    .line 94
    :try_start_7
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->is:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 99
    :cond_3
    :goto_3
    :try_start_8
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    goto :goto_2

    .line 84
    :catch_2
    move-exception v1

    .line 85
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    const-string v4, "QuramWinkUtileAGIFThread"

    const-string v5, "low memory - force GC"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 87
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_2

    .line 95
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :catch_3
    move-exception v1

    .line 96
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 103
    .end local v1    # "e":Ljava/io/IOException;
    :cond_4
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->array:[B

    if-eqz v4, :cond_5

    .line 104
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->movieStart:J

    .line 107
    new-instance v4, Ljava/io/ByteArrayInputStream;

    iget-object v5, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->array:[B

    invoke-direct {v4, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iput-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->is:Ljava/io/InputStream;

    .line 108
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->is:Ljava/io/InputStream;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/io/InputStream;->mark(I)V

    .line 109
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->is:Ljava/io/InputStream;

    invoke-static {v4}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v0

    .line 111
    .local v0, "MovieTemp":Landroid/graphics/Movie;
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->array:[B

    .line 112
    iput-object v0, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->movie:Landroid/graphics/Movie;

    .line 113
    if-nez v0, :cond_7

    .line 114
    const-string v4, "QuramWinkUtileAGIFThread"

    const-string v5, "Decode AGIF Failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    .end local v0    # "MovieTemp":Landroid/graphics/Movie;
    :cond_5
    :goto_4
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->is:Ljava/io/InputStream;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    if-eqz v4, :cond_6

    .line 124
    :try_start_9
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->is:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 125
    iget-object v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 131
    :cond_6
    :goto_5
    const/4 v4, 0x1

    :try_start_a
    iput-boolean v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->isReadyToDraw:Z

    goto :goto_2

    .line 115
    .restart local v0    # "MovieTemp":Landroid/graphics/Movie;
    :cond_7
    invoke-virtual {v0}, Landroid/graphics/Movie;->duration()I

    move-result v4

    if-lez v4, :cond_5

    .line 116
    const-string v4, "QuramWinkUtileAGIFThread"

    const-string v5, "Decode Agif  Success"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->movieStart:J

    .line 118
    invoke-virtual {v0}, Landroid/graphics/Movie;->duration()I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->duration:J

    goto :goto_4

    .line 126
    .end local v0    # "MovieTemp":Landroid/graphics/Movie;
    :catch_4
    move-exception v1

    .line 127
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1

    goto :goto_5

    .line 62
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "m_streamSize":I
    :catch_5
    move-exception v4

    goto/16 :goto_0
.end method

.method public setTerminate()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/agif/QuramWinkUtileAGIFThread;->teminate:Z

    .line 52
    return-void
.end method
