.class public Lcom/quramsoft/qrb/QuramBitmapRegionDecoderNative;
.super Ljava/lang/Object;
.source "QuramBitmapRegionDecoderNative.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "QuramBitmapRegionDecoderNative"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native nativeClean(I)V
.end method

.method public static nativeDecodeRegion(IIIIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "handle"    # I
    .param p1, "start_x"    # I
    .param p2, "start_y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 13
    const/4 v7, 0x0

    .line 15
    .local v7, "dst_format":I
    move-object/from16 v0, p5

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 17
    .local v10, "sampleSize":I
    if-eqz p0, :cond_0

    if-eqz p5, :cond_0

    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    if-gtz v2, :cond_2

    :cond_0
    const/4 v1, 0x0

    .line 55
    :cond_1
    :goto_0
    return-object v1

    .line 19
    :cond_2
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/16 v8, 0x8

    if-le v2, v8, :cond_3

    .line 20
    const/16 v2, 0x8

    move-object/from16 v0, p5

    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 22
    :cond_3
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v3, p1, v2

    .line 23
    .local v3, "x":I
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v4, p2, v2

    .line 24
    .local v4, "y":I
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v5, p3, v2

    .line 25
    .local v5, "w":I
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v6, p4, v2

    .line 29
    .local v6, "h":I
    if-lez v5, :cond_4

    if-gtz v6, :cond_5

    .line 30
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 33
    :cond_5
    move-object/from16 v0, p5

    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v2, v8, :cond_6

    .line 34
    const/4 v7, 0x0

    .line 44
    :goto_1
    move-object/from16 v0, p5

    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 46
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p5

    iget v8, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    move v2, p0

    invoke-static/range {v1 .. v8}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderNative;->nativeDoDecodeRegion(Landroid/graphics/Bitmap;IIIIIII)I

    .line 48
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    if-ge v2, v10, :cond_1

    .line 50
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v2, v5

    div-int/2addr v2, v10

    move-object/from16 v0, p5

    iget v8, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v8, v6

    div-int/2addr v8, v10

    const/4 v11, 0x0

    invoke-static {v1, v2, v8, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 51
    .local v9, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 52
    move-object v1, v9

    .line 53
    move-object/from16 v0, p5

    iput v10, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    goto :goto_0

    .line 35
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v9    # "bm":Landroid/graphics/Bitmap;
    :cond_6
    move-object/from16 v0, p5

    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-ne v2, v8, :cond_7

    .line 36
    const/4 v7, 0x1

    goto :goto_1

    .line 38
    :cond_7
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, p5

    iput-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 39
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public static nativeDecodeRegion(ILandroid/graphics/Bitmap;IIIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "handle"    # I
    .param p1, "output"    # Landroid/graphics/Bitmap;
    .param p2, "start_x"    # I
    .param p3, "start_y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 61
    const/4 v7, 0x0

    .line 63
    .local v7, "dst_format":I
    move-object/from16 v0, p6

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 65
    .local v10, "sampleSize":I
    if-eqz p0, :cond_0

    if-eqz p6, :cond_0

    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    if-gtz v2, :cond_2

    :cond_0
    const/4 v1, 0x0

    .line 113
    :cond_1
    :goto_0
    return-object v1

    .line 67
    :cond_2
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/16 v8, 0x8

    if-le v2, v8, :cond_3

    .line 68
    const/16 v2, 0x8

    move-object/from16 v0, p6

    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 70
    :cond_3
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v3, p2, v2

    .line 71
    .local v3, "x":I
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v4, p3, v2

    .line 72
    .local v4, "y":I
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v5, p4, v2

    .line 73
    .local v5, "w":I
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v6, p5, v2

    .line 77
    .local v6, "h":I
    if-lez v5, :cond_4

    if-gtz v6, :cond_5

    .line 78
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 81
    :cond_5
    move-object/from16 v0, p6

    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v2, v8, :cond_8

    .line 82
    const/4 v7, 0x0

    .line 92
    :goto_1
    const/4 v1, 0x0

    .line 93
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_6

    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/16 v8, 0x8

    if-gt v2, v8, :cond_6

    .line 94
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v5, v2, :cond_6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ne v6, v2, :cond_6

    .line 95
    move-object/from16 v0, p6

    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v2, v8, :cond_6

    .line 96
    move-object v1, p1

    .line 100
    :cond_6
    if-nez v1, :cond_7

    .line 101
    move-object/from16 v0, p6

    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 104
    :cond_7
    move-object/from16 v0, p6

    iget v8, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    move v2, p0

    invoke-static/range {v1 .. v8}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderNative;->nativeDoDecodeRegion(Landroid/graphics/Bitmap;IIIIIII)I

    .line 106
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    if-ge v2, v10, :cond_1

    .line 108
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v2, v5

    div-int/2addr v2, v10

    move-object/from16 v0, p6

    iget v8, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v8, v6

    div-int/2addr v8, v10

    const/4 v11, 0x0

    invoke-static {v1, v2, v8, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 109
    .local v9, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 110
    move-object v1, v9

    .line 111
    move-object/from16 v0, p6

    iput v10, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    goto/16 :goto_0

    .line 83
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v9    # "bm":Landroid/graphics/Bitmap;
    :cond_8
    move-object/from16 v0, p6

    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-ne v2, v8, :cond_9

    .line 84
    const/4 v7, 0x1

    goto :goto_1

    .line 86
    :cond_9
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, p6

    iput-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 87
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public static nativeDecodeRegionEx(ILandroid/graphics/Bitmap;IIIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "handle"    # I
    .param p1, "output"    # Landroid/graphics/Bitmap;
    .param p2, "start_x"    # I
    .param p3, "start_y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 122
    const/4 v7, 0x0

    .line 124
    .local v7, "dst_format":I
    move-object/from16 v0, p6

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 126
    .local v10, "sampleSize":I
    if-eqz p0, :cond_0

    if-eqz p6, :cond_0

    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    if-gtz v2, :cond_2

    :cond_0
    const/4 v1, 0x0

    .line 174
    :cond_1
    :goto_0
    return-object v1

    .line 128
    :cond_2
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/16 v8, 0x8

    if-le v2, v8, :cond_3

    .line 129
    const/16 v2, 0x8

    move-object/from16 v0, p6

    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 131
    :cond_3
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v3, p2, v2

    .line 132
    .local v3, "x":I
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v4, p3, v2

    .line 133
    .local v4, "y":I
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v5, p4, v2

    .line 134
    .local v5, "w":I
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v6, p5, v2

    .line 138
    .local v6, "h":I
    if-lez v5, :cond_4

    if-gtz v6, :cond_5

    .line 139
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 142
    :cond_5
    move-object/from16 v0, p6

    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v2, v8, :cond_8

    .line 143
    const/4 v7, 0x0

    .line 153
    :goto_1
    const/4 v1, 0x0

    .line 154
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_6

    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/16 v8, 0x8

    if-gt v2, v8, :cond_6

    .line 155
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v5, v2, :cond_6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ne v6, v2, :cond_6

    .line 156
    move-object/from16 v0, p6

    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v2, v8, :cond_6

    .line 157
    move-object v1, p1

    .line 161
    :cond_6
    if-nez v1, :cond_7

    .line 162
    move-object/from16 v0, p6

    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 165
    :cond_7
    move-object/from16 v0, p6

    iget v8, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    move v2, p0

    invoke-static/range {v1 .. v8}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderNative;->nativeDoDecodeRegionEx(Landroid/graphics/Bitmap;IIIIIII)I

    .line 167
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    if-ge v2, v10, :cond_1

    .line 169
    move-object/from16 v0, p6

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v2, v5

    div-int/2addr v2, v10

    move-object/from16 v0, p6

    iget v8, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v8, v6

    div-int/2addr v8, v10

    const/4 v11, 0x0

    invoke-static {v1, v2, v8, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 170
    .local v9, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 171
    move-object v1, v9

    .line 172
    move-object/from16 v0, p6

    iput v10, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    goto/16 :goto_0

    .line 144
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v9    # "bm":Landroid/graphics/Bitmap;
    :cond_8
    move-object/from16 v0, p6

    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-ne v2, v8, :cond_9

    .line 145
    const/4 v7, 0x1

    goto :goto_1

    .line 147
    :cond_9
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, p6

    iput-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 148
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public static native nativeDoDecodeRegion(Landroid/graphics/Bitmap;IIIIIII)I
.end method

.method public static native nativeDoDecodeRegionEx(Landroid/graphics/Bitmap;IIIIIII)I
.end method

.method public static native nativeGetHeight(I)I
.end method

.method public static native nativeGetWidth(I)I
.end method

.method public static native nativeNewInstance(Ljava/lang/String;ZLcom/quramsoft/qrb/QuramBitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;
.end method

.method public static native nativeNewInstanceBuffer([BIIZLcom/quramsoft/qrb/QuramBitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;
.end method

.method public static native nativeNewInstanceWithDec(Ljava/lang/String;Landroid/graphics/Bitmap;IZLcom/quramsoft/qrb/QuramBitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;
.end method

.method public static native nativeNewInstanceWithOpt(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;
.end method
