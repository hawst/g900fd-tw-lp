.class public Lcom/quramsoft/qrb/QuramBitmapFactory;
.super Ljava/lang/Object;
.source "QuramBitmapFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;,
        Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final Quram_DECODECANCEL_SUCC:I = 0x6

.field public static final Quram_FAIL:I = 0x0

.field public static final Quram_JPEG:Ljava/lang/String; = "Quram_JPEG"

.field public static final Quram_Progress:I = 0x4

.field public static final Quram_SUCC:I = 0x1

.field private static final TAG:Ljava/lang/String; = "QuramBitmapFactory"

.field protected static final USE_AUTO_BUFFERMODE:I = 0x2

.field protected static final USE_AUTO_FILEMODE:I = 0x0

.field public static final USE_FULLSIZE_BUFFER:I = 0x0

.field public static final USE_ITERSIZE_BUFFER:I = 0x1

.field public static final USE_MAKE_REGIONMAP:I = 0x2

.field protected static final USE_POWER_PROCESS:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static native AbortJPEGFromFileIter(I)I
.end method

.method public static native CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
.end method

.method public static native CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)I
.end method

.method public static native DecodeCancel(I)V
.end method

.method public static native DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I
.end method

.method static native DecodeJPEGFromFileIter(ILandroid/graphics/Bitmap;IIII)I
.end method

.method static native DecodeJPEGFromFileIter4LTN(ILandroid/graphics/Bitmap;IIII)I
.end method

.method static native DecodeJPEGFromFileIter4LTNToBuffer(ILjava/nio/Buffer;IIIII)I
.end method

.method static native DecodeJPEGFromFileIterToBuffer(ILjava/nio/Buffer;IIIII)I
.end method

.method static native DecodeJPEGFromFileMultiOutBuf(I[Landroid/graphics/Bitmap;IIII)I
.end method

.method public static native DecodeJPEGThumbnail(ILandroid/graphics/Bitmap;III)I
.end method

.method public static native DecodeJPEGThumbnailToBuffer(ILjava/nio/Buffer;II)I
.end method

.method public static native DecodeJpegFromStream(Ljava/io/InputStream;ILcom/quramsoft/qrb/QuramBitmapFactory$Options;[BILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
.end method

.method public static native DecodePNGIter(IIIILjava/nio/Buffer;III)I
.end method

.method public static native DecodePNGIterInit(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/lang/String;)I
.end method

.method public static native DestroyPNGIter(III)I
.end method

.method public static native EncodeJPEG(Landroid/graphics/Bitmap;[BIIJ)I
.end method

.method public static native GetImageInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
.end method

.method public static native GetJpegOffsetFromSRWFile(Ljava/lang/String;[I)I
.end method

.method public static native PDecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I
.end method

.method public static native PartialDecodeJPEGFromFile(ILandroid/graphics/Bitmap;IIIII)I
.end method

.method public static native PartialDecodeJPEGToBuffer(ILjava/nio/Buffer;IIIII)I
.end method

.method static native PrepareJPEGFromFileIter(III)I
.end method

.method public static native RegionMapCancel(I)V
.end method

.method public static abortFileIter(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V
    .locals 1
    .param p0, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    .line 962
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    .line 963
    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    invoke-static {v0}, Lcom/quramsoft/qrb/QuramBitmapFactory;->AbortJPEGFromFileIter(I)I

    .line 964
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 966
    :cond_0
    return-void
.end method

.method public static abortIter(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V
    .locals 1
    .param p0, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    .line 954
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    .line 955
    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    invoke-static {v0}, Lcom/quramsoft/qrb/QuramBitmapFactory;->AbortJPEGFromFileIter(I)I

    .line 956
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 958
    :cond_0
    return-void
.end method

.method public static abortPNGIter(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
    .locals 5
    .param p0, "opts"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v1, 0x0

    .line 1340
    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getStruct()I

    move-result v2

    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getReadInfo()I

    move-result v3

    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getEndInfo()I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DestroyPNGIter(III)I

    move-result v0

    .line 1341
    .local v0, "ret":I
    invoke-virtual {p0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setStruct(I)V

    .line 1342
    invoke-virtual {p0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setReadInfo(I)V

    .line 1343
    invoke-virtual {p0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setEndInfo(I)V

    .line 1345
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static cancelDecode(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V
    .locals 1
    .param p0, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    .line 1066
    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inCancelingRequested:Z

    if-eqz v0, :cond_1

    .line 1070
    :cond_0
    :goto_0
    return-void

    .line 1068
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inCancelingRequested:Z

    .line 1069
    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    invoke-static {v0}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeCancel(I)V

    goto :goto_0
.end method

.method public static cancelRegionMap(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V
    .locals 1
    .param p0, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    .line 1075
    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inCancelingRequested:Z

    if-eqz v0, :cond_1

    .line 1080
    :cond_0
    :goto_0
    return-void

    .line 1078
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inCancelingRequested:Z

    .line 1079
    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    invoke-static {v0}, Lcom/quramsoft/qrb/QuramBitmapFactory;->RegionMapCancel(I)V

    goto :goto_0
.end method

.method public static compressToByte(Landroid/graphics/Bitmap;Ljava/lang/String;[BIIJ)I
    .locals 7
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "compressFormat"    # Ljava/lang/String;
    .param p2, "out"    # [B
    .param p3, "out_bufsize"    # I
    .param p4, "quality"    # I
    .param p5, "orgID"    # J

    .prologue
    .line 1052
    const-string v0, "Quram_JPEG"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move-wide v4, p5

    .line 1055
    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->EncodeJPEG(Landroid/graphics/Bitmap;[BIIJ)I

    .line 1057
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static createDecInfo(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
    .locals 3
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v1, 0x0

    .line 720
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v0, v1

    .line 732
    :cond_1
    :goto_0
    return v0

    .line 726
    :cond_2
    const/4 v2, 0x2

    .line 723
    invoke-static {p0, p1, v2}, Lcom/quramsoft/qrb/QuramBitmapFactory;->CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)I

    move-result v0

    .line 728
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 729
    invoke-virtual {p1, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0
.end method

.method public static createDecInfo([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
    .locals 2
    .param p0, "buffer"    # [B
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .param p3, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v1, 0x0

    .line 737
    if-eqz p0, :cond_0

    if-nez p3, :cond_2

    :cond_0
    move v0, v1

    .line 746
    :cond_1
    :goto_0
    return v0

    .line 740
    :cond_2
    invoke-static {p0, p1, p2, p3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v0

    .line 742
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 743
    invoke-virtual {p3, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0
.end method

.method public static decodeByteArray(Landroid/graphics/Bitmap;[BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "output"    # Landroid/graphics/Bitmap;
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 265
    if-gez p2, :cond_1

    .line 316
    :cond_0
    :goto_0
    return-object v2

    .line 270
    :cond_1
    if-lez p3, :cond_0

    .line 275
    array-length v4, p1

    add-int v5, p3, p2

    if-lt v4, v5, :cond_0

    .line 280
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v4

    if-nez v4, :cond_0

    .line 285
    invoke-static {p1, p2, p3, p4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v0

    .line 286
    .local v0, "codecRet":I
    if-nez v0, :cond_2

    .line 287
    invoke-virtual {p4, v6}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 292
    :cond_2
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getWidth()I

    move-result v3

    .line 293
    .local v3, "width":I
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHeight()I

    move-result v1

    .line 296
    .local v1, "height":I
    if-eqz p0, :cond_4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-ne v4, v3, :cond_4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-ne v4, v1, :cond_4

    .line 297
    move-object v2, p0

    .line 307
    .local v2, "retBitmap":Landroid/graphics/Bitmap;
    :goto_1
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v4

    const/4 v5, 0x1

    invoke-static {v4, v2, v3, v1, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I

    move-result v0

    .line 310
    if-nez v0, :cond_3

    .line 312
    const/4 v2, 0x0

    .line 315
    :cond_3
    invoke-virtual {p4, v6}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 299
    .end local v2    # "retBitmap":Landroid/graphics/Bitmap;
    :cond_4
    iget v4, p4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v5, 0x7

    if-ne v4, v5, :cond_5

    .line 300
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v1, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .restart local v2    # "retBitmap":Landroid/graphics/Bitmap;
    goto :goto_1

    .line 302
    .end local v2    # "retBitmap":Landroid/graphics/Bitmap;
    :cond_5
    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v1, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .restart local v2    # "retBitmap":Landroid/graphics/Bitmap;
    goto :goto_1
.end method

.method public static decodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/16 v8, 0xbb8

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 421
    const/4 v1, 0x0

    .line 423
    .local v1, "retBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v5

    if-eqz v5, :cond_1

    .line 476
    :cond_0
    :goto_0
    return-object v4

    .line 430
    :cond_1
    const/4 v5, 0x2

    invoke-static {p0, p1, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)I

    move-result v0

    .line 431
    .local v0, "ret":I
    if-nez v0, :cond_2

    .line 433
    invoke-virtual {p1, v7}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 439
    :cond_2
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getWidth()I

    move-result v5

    int-to-float v5, v5

    iget v6, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->round(F)I

    move-result v3

    .line 440
    .local v3, "sampledWidth":I
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHeight()I

    move-result v5

    int-to-float v5, v5

    iget v6, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->round(F)I

    move-result v2

    .line 442
    .local v2, "sampledHeight":I
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    if-ge v3, v8, :cond_0

    if-ge v2, v8, :cond_0

    .line 445
    iget v5, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v6, 0x7

    if-ne v5, v6, :cond_5

    .line 446
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v2, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 450
    :goto_1
    iget v5, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v5, :cond_3

    .line 451
    const/4 v5, 0x1

    iput v5, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 453
    :cond_3
    iget v5, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v6, 0x8

    if-le v5, v6, :cond_6

    .line 455
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v5

    const/4 v6, -0x1

    invoke-static {v5, v1, v3, v2, v6}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I

    move-result v0

    .line 461
    :goto_2
    if-nez v0, :cond_7

    .line 463
    if-eqz v1, :cond_4

    .line 465
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 466
    const/4 v1, 0x0

    .line 469
    :cond_4
    invoke-virtual {p1, v7}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 448
    :cond_5
    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v2, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    .line 459
    :cond_6
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v5

    iget v6, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    invoke-static {v5, v1, v3, v2, v6}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I

    move-result v0

    goto :goto_2

    .line 475
    :cond_7
    invoke-virtual {p1, v7}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v4, v1

    .line 476
    goto :goto_0
.end method

.method public static decodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;III)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "origId"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 190
    const/4 v1, 0x0

    .line 192
    .local v1, "retBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    if-eqz v3, :cond_0

    .line 251
    :goto_0
    return-object v2

    .line 199
    :cond_0
    const/4 v3, 0x2

    invoke-static {p0, p1, v3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)I

    move-result v0

    .line 200
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 202
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 206
    :cond_1
    iget v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_4

    .line 207
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 211
    :goto_1
    iget v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v3, :cond_2

    .line 212
    const/4 v3, 0x1

    iput v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 214
    :cond_2
    iget v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v4, 0x8

    if-le v3, v4, :cond_5

    .line 216
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    const/4 v4, -0x1

    invoke-static {v3, v1, p2, p3, v4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I

    move-result v0

    .line 222
    :goto_2
    if-nez v0, :cond_6

    .line 224
    if-eqz v1, :cond_3

    .line 226
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 227
    const/4 v1, 0x0

    .line 230
    :cond_3
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 209
    :cond_4
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    .line 220
    :cond_5
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    iget v4, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    invoke-static {v3, v1, p2, p3, v4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I

    move-result v0

    goto :goto_2

    .line 236
    :cond_6
    const/4 v2, 0x6

    if-ne v0, v2, :cond_7

    .line 240
    if-eqz v1, :cond_7

    .line 242
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 243
    const/4 v1, 0x0

    .line 250
    :cond_7
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v2, v1

    .line 251
    goto :goto_0
.end method

.method public static decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "fd"    # Ljava/io/FileDescriptor;

    .prologue
    .line 1219
    new-instance v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 1220
    .local v0, "option":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "outpadding"    # Landroid/graphics/Rect;
    .param p2, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v8, 0x0

    .line 1225
    const/4 v7, 0x0

    .line 1226
    .local v7, "retBitmap":Landroid/graphics/Bitmap;
    const/16 v1, 0x4000

    new-array v3, v1, [B

    .line 1228
    .local v3, "bytearray":[B
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 1230
    .local v0, "fis":Ljava/io/FileInputStream;
    invoke-virtual {p2}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1232
    const-string v1, "QuramBitmapFactory"

    const-string v2, "option Fail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1303
    :goto_0
    return-object v8

    .line 1238
    :cond_0
    :try_start_0
    iget v1, p2, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-nez v1, :cond_2

    .line 1239
    invoke-virtual {v0}, Ljava/io/FileInputStream;->available()I

    move-result v1

    iget v4, p2, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJpegFromStream(Ljava/io/InputStream;ILcom/quramsoft/qrb/QuramBitmapFactory$Options;[BILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 1292
    :cond_1
    :goto_1
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 1294
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v8

    .line 1302
    check-cast v3, [B

    move-object v8, v7

    .line 1303
    goto :goto_0

    .line 1240
    :cond_2
    :try_start_2
    iget v1, p2, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1

    .line 1241
    invoke-virtual {v0}, Ljava/io/FileInputStream;->available()I

    move-result v1

    iget v4, p2, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJpegFromStream(Ljava/io/InputStream;ILcom/quramsoft/qrb/QuramBitmapFactory$Options;[BILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v7

    goto :goto_1

    .line 1244
    :catch_0
    move-exception v6

    .line 1246
    .local v6, "e":Ljava/lang/Exception;
    const-string v1, "QuramBitmapFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1296
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    .line 1298
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v1, "QuramBitmapFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static decodeFileFromThumbnail(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;III)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "origId"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 41
    const/4 v1, 0x0

    .line 43
    .local v1, "retBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    if-eqz v3, :cond_0

    .line 96
    :goto_0
    return-object v2

    .line 50
    :cond_0
    const/4 v3, 0x2

    invoke-static {p0, p1, v3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)I

    move-result v0

    .line 51
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 53
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 57
    :cond_1
    iget v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_4

    .line 58
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 62
    :goto_1
    iget v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v3, :cond_2

    .line 63
    iput v6, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 65
    :cond_2
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    invoke-static {v3, v1, p2, p3, v6}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJPEGThumbnail(ILandroid/graphics/Bitmap;III)I

    move-result v0

    .line 67
    if-nez v0, :cond_5

    .line 69
    if-eqz v1, :cond_3

    .line 71
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 72
    const/4 v1, 0x0

    .line 75
    :cond_3
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 60
    :cond_4
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    .line 81
    :cond_5
    const/4 v2, 0x6

    if-ne v0, v2, :cond_6

    .line 85
    if-eqz v1, :cond_6

    .line 87
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 88
    const/4 v1, 0x0

    .line 95
    :cond_6
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v2, v1

    .line 96
    goto :goto_0
.end method

.method public static decodeFileIter(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Landroid/graphics/Bitmap;IIII)I
    .locals 7
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "retBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "sampledWidth"    # I
    .param p4, "sampledHeight"    # I
    .param p5, "iterType"    # I
    .param p6, "decodeStep"    # I

    .prologue
    .line 848
    const/4 v6, 0x0

    .line 850
    .local v6, "ret":I
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    .line 851
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 852
    :cond_0
    const/4 v0, 0x0

    .line 890
    :goto_0
    return v0

    .line 855
    :cond_1
    packed-switch p5, :pswitch_data_0

    .line 875
    :goto_1
    :pswitch_0
    if-nez v6, :cond_2

    .line 877
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    .line 878
    const/4 p2, 0x0

    .line 881
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 884
    const/4 v0, 0x0

    goto :goto_0

    .line 858
    :pswitch_1
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    .line 860
    const/4 v4, 0x0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v5, p6

    .line 857
    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJPEGFromFileIter(ILandroid/graphics/Bitmap;IIII)I

    move-result v6

    .line 861
    goto :goto_1

    .line 868
    :pswitch_2
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 867
    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJPEGFromFileIter4LTN(ILandroid/graphics/Bitmap;IIII)I

    move-result v6

    goto :goto_1

    .line 886
    :cond_2
    const/4 v0, 0x1

    if-ne v6, v0, :cond_4

    .line 887
    if-eqz p5, :cond_3

    const/4 v0, 0x1

    if-ne p5, v0, :cond_4

    .line 888
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    :cond_4
    move v0, v6

    .line 890
    goto :goto_0

    .line 855
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static decodeFileIter(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/nio/Buffer;IIIII)I
    .locals 8
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "buffer"    # Ljava/nio/Buffer;
    .param p3, "bufferHeight"    # I
    .param p4, "sampledWidth"    # I
    .param p5, "sampledHeight"    # I
    .param p6, "iterType"    # I
    .param p7, "decodeStep"    # I

    .prologue
    .line 903
    const/4 v7, 0x0

    .line 905
    .local v7, "ret":I
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    .line 906
    if-eqz p2, :cond_0

    if-eqz p4, :cond_0

    if-nez p5, :cond_1

    .line 907
    :cond_0
    const/4 v0, 0x0

    .line 947
    :goto_0
    return v0

    .line 910
    :cond_1
    packed-switch p6, :pswitch_data_0

    .line 932
    :goto_1
    :pswitch_0
    if-nez v7, :cond_3

    .line 935
    const/4 p2, 0x0

    .line 938
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 941
    const/4 v0, 0x0

    goto :goto_0

    .line 913
    :pswitch_1
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    .line 915
    const/4 v5, 0x0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v6, p7

    .line 912
    invoke-static/range {v0 .. v6}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJPEGFromFileIterToBuffer(ILjava/nio/Buffer;IIIII)I

    move-result v7

    .line 916
    goto :goto_1

    .line 920
    :pswitch_2
    add-int/lit8 v0, p7, 0x2

    if-ne v0, p3, :cond_2

    .line 923
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    .line 922
    invoke-static/range {v0 .. v6}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJPEGFromFileIter4LTNToBuffer(ILjava/nio/Buffer;IIIII)I

    move-result v7

    goto :goto_1

    .line 927
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 943
    :cond_3
    const/4 v0, 0x1

    if-ne v7, v0, :cond_5

    .line 944
    if-eqz p6, :cond_4

    const/4 v0, 0x1

    if-ne p6, v0, :cond_5

    .line 945
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    :cond_5
    move v0, v7

    .line 947
    goto :goto_0

    .line 910
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static decodeFileToBuffer(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;III)Ljava/nio/ByteBuffer;
    .locals 7
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "origId"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 112
    const/4 v0, 0x0

    .line 114
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    if-eqz v3, :cond_0

    .line 175
    :goto_0
    return-object v2

    .line 121
    :cond_0
    invoke-static {p0, p1, v6}, Lcom/quramsoft/qrb/QuramBitmapFactory;->CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)I

    move-result v1

    .line 122
    .local v1, "ret":I
    if-nez v1, :cond_1

    .line 124
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 129
    :cond_1
    iget v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_3

    .line 130
    mul-int v3, p2, p3

    mul-int/lit8 v3, v3, 0x4

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 141
    :goto_1
    sget-object v3, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 143
    iget v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v3, :cond_2

    .line 144
    const/4 v3, 0x1

    iput v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 146
    :cond_2
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    invoke-static {v3, v0, p2, p3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJPEGThumbnailToBuffer(ILjava/nio/Buffer;II)I

    move-result v1

    .line 148
    if-nez v1, :cond_6

    .line 152
    const/4 v0, 0x0

    .line 155
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 131
    :cond_3
    iget v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-nez v3, :cond_4

    .line 132
    mul-int v3, p2, p3

    mul-int/lit8 v3, v3, 0x2

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_1

    .line 133
    :cond_4
    iget v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-ne v3, v6, :cond_5

    .line 134
    mul-int v3, p2, p3

    mul-int/lit8 v3, v3, 0x3

    shr-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_1

    .line 137
    :cond_5
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 161
    :cond_6
    const/4 v2, 0x6

    if-ne v1, v2, :cond_7

    .line 167
    const/4 v0, 0x0

    .line 174
    :cond_7
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v2, v0

    .line 175
    goto :goto_0
.end method

.method public static decodeIter(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Landroid/graphics/Bitmap;IIII)I
    .locals 7
    .param p0, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p1, "retBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "sampledWidth"    # I
    .param p3, "sampledHeight"    # I
    .param p4, "iterType"    # I
    .param p5, "decodeStep"    # I

    .prologue
    .line 818
    const/4 v0, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFileIter(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Landroid/graphics/Bitmap;IIII)I

    move-result v0

    return v0
.end method

.method public static decodeIter(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/nio/Buffer;IIIII)I
    .locals 8
    .param p0, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p1, "buffer"    # Ljava/nio/Buffer;
    .param p2, "bufferHeight"    # I
    .param p3, "sampledWidth"    # I
    .param p4, "sampledHeight"    # I
    .param p5, "iterType"    # I
    .param p6, "decodeStep"    # I

    .prologue
    .line 830
    const/4 v0, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFileIter(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/nio/Buffer;IIIII)I

    move-result v0

    return v0
.end method

.method public static decodePNGIterInit(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/lang/String;)I
    .locals 2
    .param p0, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 1324
    invoke-static {p0, p1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodePNGIterInit(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/lang/String;)I

    move-result v0

    .line 1325
    .local v0, "ret":I
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static decodePNGIterToBuffer(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;ILjava/nio/Buffer;III)I
    .locals 9
    .param p0, "opts"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p1, "sampleSize"    # I
    .param p2, "buffer"    # Ljava/nio/Buffer;
    .param p3, "decodeWdt"    # I
    .param p4, "decodeheight"    # I
    .param p5, "decodeStep"    # I

    .prologue
    .line 1333
    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getStruct()I

    move-result v0

    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getReadInfo()I

    move-result v1

    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getEndInfo()I

    move-result v2

    move v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    .line 1332
    invoke-static/range {v0 .. v7}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodePNGIter(IIIILjava/nio/Buffer;III)I

    move-result v8

    .line 1336
    .local v8, "ret":I
    if-nez v8, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static decodeThumbnailByteArrayToBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
    .locals 8
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "output"    # Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    .param p4, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v5, 0x0

    .line 349
    const/4 v2, 0x1

    .line 351
    .local v2, "ret":I
    if-gez p1, :cond_1

    .line 409
    :cond_0
    :goto_0
    return v5

    .line 356
    :cond_1
    if-lez p2, :cond_0

    .line 361
    array-length v6, p0

    add-int v7, p2, p1

    if-lt v6, v7, :cond_0

    .line 366
    if-eqz p3, :cond_0

    .line 371
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v6

    if-nez v6, :cond_0

    .line 376
    invoke-static {p0, p1, p2, p4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v2

    .line 377
    if-eqz v2, :cond_4

    .line 378
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getWidth()I

    move-result v4

    .line 379
    .local v4, "width":I
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHeight()I

    move-result v1

    .line 381
    .local v1, "height":I
    iget v6, p4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v7, 0x7

    if-ne v6, v7, :cond_5

    .line 382
    const/high16 v0, 0x40800000    # 4.0f

    .line 392
    .local v0, "bpp":F
    :goto_1
    iput v4, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->width:I

    .line 393
    iput v1, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->height:I

    .line 394
    mul-int v6, v4, v1

    int-to-float v6, v6

    mul-float/2addr v6, v0

    float-to-int v3, v6

    .line 396
    .local v3, "size":I
    iget-object v6, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    if-eqz v6, :cond_2

    iget-object v6, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->limit()I

    move-result v6

    if-ge v6, v3, :cond_3

    .line 397
    :cond_2
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    iput-object v6, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    .line 400
    :cond_3
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v6

    iget-object v7, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    invoke-static {v6, v7, v4, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJPEGThumbnailToBuffer(ILjava/nio/Buffer;II)I

    move-result v2

    .line 401
    if-nez v2, :cond_4

    .line 408
    .end local v0    # "bpp":F
    .end local v1    # "height":I
    .end local v3    # "size":I
    .end local v4    # "width":I
    :cond_4
    invoke-virtual {p4, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    move v5, v2

    .line 409
    goto :goto_0

    .line 383
    .restart local v1    # "height":I
    .restart local v4    # "width":I
    :cond_5
    iget v6, p4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-nez v6, :cond_6

    .line 384
    const/high16 v0, 0x40000000    # 2.0f

    .restart local v0    # "bpp":F
    goto :goto_1

    .line 385
    .end local v0    # "bpp":F
    :cond_6
    iget v6, p4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_7

    .line 386
    const/high16 v0, 0x3fc00000    # 1.5f

    .restart local v0    # "bpp":F
    goto :goto_1

    .line 388
    .end local v0    # "bpp":F
    :cond_7
    invoke-virtual {p4, v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0
.end method

.method public static getJpegOffsetFromSRWFile(Ljava/lang/String;[I)I
    .locals 1
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "size"    # [I

    .prologue
    .line 1314
    const/4 v0, 0x0

    .line 1316
    .local v0, "ret":I
    invoke-static {p0, p1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->GetJpegOffsetFromSRWFile(Ljava/lang/String;[I)I

    move-result v0

    .line 1318
    return v0
.end method

.method public static partialDecodeByteArray(Landroid/graphics/Bitmap;[BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;IIII)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "output"    # Landroid/graphics/Bitmap;
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p5, "left"    # I
    .param p6, "right"    # I
    .param p7, "top"    # I
    .param p8, "bottom"    # I

    .prologue
    .line 564
    move-object/from16 v0, p4

    iget v11, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 565
    .local v11, "sampleSize":I
    const/4 v12, 0x0

    .line 566
    .local v12, "width":I
    const/4 v9, 0x0

    .line 568
    .local v9, "height":I
    if-gez p2, :cond_0

    .line 570
    const/4 v2, 0x0

    .line 634
    :goto_0
    return-object v2

    .line 573
    :cond_0
    if-gtz p3, :cond_1

    .line 575
    const/4 v2, 0x0

    goto :goto_0

    .line 578
    :cond_1
    array-length v1, p1

    add-int v3, p3, p2

    if-ge v1, v3, :cond_2

    .line 580
    const/4 v2, 0x0

    goto :goto_0

    .line 583
    :cond_2
    invoke-virtual/range {p4 .. p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    if-eqz v1, :cond_3

    .line 585
    const/4 v2, 0x0

    goto :goto_0

    .line 588
    :cond_3
    invoke-static/range {p1 .. p4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v10

    .line 589
    .local v10, "ret":I
    if-nez v10, :cond_4

    .line 590
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 592
    const/4 v2, 0x0

    goto :goto_0

    .line 595
    :cond_4
    move-object/from16 v0, p4

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v1, :cond_7

    .line 596
    const/4 v1, 0x1

    move-object/from16 v0, p4

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 600
    :cond_5
    :goto_1
    sub-int v1, p6, p5

    move-object/from16 v0, p4

    iget v3, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v12, v1, v3

    .line 601
    sub-int v1, p8, p7

    move-object/from16 v0, p4

    iget v3, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v9, v1, v3

    .line 604
    if-eqz p0, :cond_8

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v1, v12, :cond_8

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-ne v1, v9, :cond_8

    .line 605
    move-object v2, p0

    .line 613
    .local v2, "retBitmap":Landroid/graphics/Bitmap;
    :goto_2
    invoke-virtual/range {p4 .. p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    sub-int v5, p6, p5

    sub-int v6, p8, p7

    move-object/from16 v0, p4

    iget v7, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    move/from16 v3, p5

    move/from16 v4, p7

    invoke-static/range {v1 .. v7}, Lcom/quramsoft/qrb/QuramBitmapFactory;->PartialDecodeJPEGFromFile(ILandroid/graphics/Bitmap;IIIII)I

    move-result v10

    .line 615
    if-nez v10, :cond_a

    .line 616
    if-eqz v2, :cond_6

    .line 617
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 618
    const/4 v2, 0x0

    .line 621
    :cond_6
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 623
    const/4 v2, 0x0

    goto :goto_0

    .line 597
    .end local v2    # "retBitmap":Landroid/graphics/Bitmap;
    :cond_7
    move-object/from16 v0, p4

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v3, 0x8

    if-le v1, v3, :cond_5

    .line 598
    const/16 v1, 0x8

    move-object/from16 v0, p4

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    goto :goto_1

    .line 607
    :cond_8
    move-object/from16 v0, p4

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v3, 0x7

    if-ne v1, v3, :cond_9

    .line 608
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v12, v9, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .restart local v2    # "retBitmap":Landroid/graphics/Bitmap;
    goto :goto_2

    .line 610
    .end local v2    # "retBitmap":Landroid/graphics/Bitmap;
    :cond_9
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v12, v9, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .restart local v2    # "retBitmap":Landroid/graphics/Bitmap;
    goto :goto_2

    .line 626
    :cond_a
    move-object/from16 v0, p4

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-ge v1, v11, :cond_b

    .line 627
    move-object/from16 v0, p4

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v1, v12

    div-int/2addr v1, v11

    move-object/from16 v0, p4

    iget v3, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v3, v9

    div-int/2addr v3, v11

    const/4 v4, 0x0

    invoke-static {v2, v1, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 628
    .local v8, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 629
    move-object v2, v8

    .line 630
    move-object/from16 v0, p4

    iput v11, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 633
    .end local v8    # "bm":Landroid/graphics/Bitmap;
    :cond_b
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto/16 :goto_0
.end method

.method public static partialDecodeByteArray([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;IIII)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p4, "left"    # I
    .param p5, "right"    # I
    .param p6, "top"    # I
    .param p7, "bottom"    # I

    .prologue
    .line 488
    const/4 v1, 0x0

    .line 489
    .local v1, "retBitmap":Landroid/graphics/Bitmap;
    iget v10, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 490
    .local v10, "sampleSize":I
    const/4 v11, 0x0

    .line 491
    .local v11, "width":I
    const/4 v8, 0x0

    .line 493
    .local v8, "height":I
    if-gez p1, :cond_0

    .line 495
    const/4 v0, 0x0

    .line 555
    :goto_0
    return-object v0

    .line 498
    :cond_0
    if-gtz p2, :cond_1

    .line 500
    const/4 v0, 0x0

    goto :goto_0

    .line 503
    :cond_1
    array-length v0, p0

    add-int v2, p2, p1

    if-ge v0, v2, :cond_2

    .line 505
    const/4 v0, 0x0

    goto :goto_0

    .line 508
    :cond_2
    invoke-virtual {p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_3

    .line 510
    const/4 v0, 0x0

    goto :goto_0

    .line 513
    :cond_3
    invoke-static {p0, p1, p2, p3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v9

    .line 514
    .local v9, "ret":I
    if-nez v9, :cond_4

    .line 515
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 517
    const/4 v0, 0x0

    goto :goto_0

    .line 520
    :cond_4
    iget v0, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v0, :cond_7

    .line 521
    const/4 v0, 0x1

    iput v0, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 525
    :cond_5
    :goto_1
    sub-int v0, p5, p4

    iget v2, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v11, v0, v2

    .line 526
    sub-int v0, p7, p6

    iget v2, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v8, v0, v2

    .line 528
    iget v0, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x7

    if-ne v0, v2, :cond_8

    .line 529
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v8, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 534
    :goto_2
    invoke-virtual {p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    sub-int v4, p5, p4

    sub-int v5, p7, p6

    iget v6, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    move/from16 v2, p4

    move/from16 v3, p6

    invoke-static/range {v0 .. v6}, Lcom/quramsoft/qrb/QuramBitmapFactory;->PartialDecodeJPEGFromFile(ILandroid/graphics/Bitmap;IIIII)I

    move-result v9

    .line 536
    if-nez v9, :cond_9

    .line 537
    if-eqz v1, :cond_6

    .line 538
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 539
    const/4 v1, 0x0

    .line 542
    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 544
    const/4 v0, 0x0

    goto :goto_0

    .line 522
    :cond_7
    iget v0, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v2, 0x8

    if-le v0, v2, :cond_5

    .line 523
    const/16 v0, 0x8

    iput v0, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    goto :goto_1

    .line 531
    :cond_8
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v8, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_2

    .line 547
    :cond_9
    iget v0, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-ge v0, v10, :cond_a

    .line 548
    iget v0, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v0, v11

    div-int/2addr v0, v10

    iget v2, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v2, v8

    div-int/2addr v2, v10

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 549
    .local v7, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 550
    move-object v1, v7

    .line 551
    iput v10, p3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 554
    .end local v7    # "bm":Landroid/graphics/Bitmap;
    :cond_a
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v0, v1

    .line 555
    goto/16 :goto_0
.end method

.method public static partialDecodeByteArrayToBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;IIIILcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;)I
    .locals 13
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p4, "left"    # I
    .param p5, "right"    # I
    .param p6, "top"    # I
    .param p7, "bottom"    # I
    .param p8, "output"    # Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    .prologue
    .line 648
    const/4 v10, 0x1

    .line 650
    .local v10, "ret":I
    if-gez p1, :cond_0

    .line 652
    const/4 v1, 0x0

    .line 715
    :goto_0
    return v1

    .line 655
    :cond_0
    if-gtz p2, :cond_1

    .line 657
    const/4 v1, 0x0

    goto :goto_0

    .line 660
    :cond_1
    array-length v1, p0

    add-int v2, p2, p1

    if-ge v1, v2, :cond_2

    .line 662
    const/4 v1, 0x0

    goto :goto_0

    .line 665
    :cond_2
    invoke-virtual/range {p3 .. p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    if-eqz v1, :cond_3

    .line 667
    const/4 v1, 0x0

    goto :goto_0

    .line 670
    :cond_3
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v2, 0x8

    if-gt v1, v2, :cond_4

    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-gez v1, :cond_5

    .line 672
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 675
    :cond_5
    if-eqz p8, :cond_6

    move-object/from16 v0, p8

    iget-object v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    if-nez v1, :cond_7

    .line 677
    :cond_6
    const/4 v1, 0x0

    goto :goto_0

    .line 680
    :cond_7
    invoke-static/range {p0 .. p3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v10

    .line 681
    const/4 v1, 0x1

    if-ne v10, v1, :cond_a

    .line 682
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v1, :cond_b

    .line 683
    const/4 v1, 0x1

    move-object/from16 v0, p3

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 687
    :cond_8
    :goto_1
    sub-int v1, p5, p4

    move-object/from16 v0, p3

    iget v2, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v12, v1, v2

    .line 688
    .local v12, "width":I
    sub-int v1, p7, p6

    move-object/from16 v0, p3

    iget v2, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v9, v1, v2

    .line 690
    .local v9, "height":I
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_c

    .line 691
    const/high16 v8, 0x40800000    # 4.0f

    .line 701
    .local v8, "bpp":F
    :goto_2
    move-object/from16 v0, p8

    iput v12, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->width:I

    .line 702
    move-object/from16 v0, p8

    iput v9, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->height:I

    .line 703
    mul-int v1, v12, v9

    int-to-float v1, v1

    mul-float/2addr v1, v8

    float-to-int v11, v1

    .line 705
    .local v11, "size":I
    move-object/from16 v0, p8

    iget-object v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    if-ge v1, v11, :cond_9

    .line 706
    invoke-static {v11}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    move-object/from16 v0, p8

    iput-object v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    .line 709
    :cond_9
    invoke-virtual/range {p3 .. p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    move-object/from16 v0, p8

    iget-object v2, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    sub-int v5, p5, p4

    sub-int v6, p7, p6

    move-object/from16 v0, p3

    iget v7, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    move/from16 v3, p4

    move/from16 v4, p6

    invoke-static/range {v1 .. v7}, Lcom/quramsoft/qrb/QuramBitmapFactory;->PartialDecodeJPEGToBuffer(ILjava/nio/Buffer;IIIII)I

    move-result v10

    .line 714
    .end local v8    # "bpp":F
    .end local v9    # "height":I
    .end local v11    # "size":I
    .end local v12    # "width":I
    :cond_a
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    move v1, v10

    .line 715
    goto/16 :goto_0

    .line 684
    :cond_b
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v2, 0x8

    if-le v1, v2, :cond_8

    .line 685
    const/16 v1, 0x8

    move-object/from16 v0, p3

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    goto :goto_1

    .line 692
    .restart local v9    # "height":I
    .restart local v12    # "width":I
    :cond_c
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-nez v1, :cond_d

    .line 693
    const/high16 v8, 0x40000000    # 2.0f

    .restart local v8    # "bpp":F
    goto :goto_2

    .line 694
    .end local v8    # "bpp":F
    :cond_d
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_e

    .line 695
    const/high16 v8, 0x3fc00000    # 1.5f

    .restart local v8    # "bpp":F
    goto :goto_2

    .line 697
    .end local v8    # "bpp":F
    :cond_e
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 698
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public static partialDecodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;IIII)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "left"    # I
    .param p3, "right"    # I
    .param p4, "top"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 977
    const/4 v1, 0x0

    .line 978
    .local v1, "retBitmap":Landroid/graphics/Bitmap;
    iget v10, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 979
    .local v10, "sampleSize":I
    const/4 v11, 0x0

    .line 980
    .local v11, "width":I
    const/4 v8, 0x0

    .line 982
    .local v8, "height":I
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    .line 986
    const/4 v0, 0x0

    .line 1037
    :goto_0
    return-object v0

    .line 989
    :cond_0
    const/4 v0, 0x2

    invoke-static {p0, p1, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory;->CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)I

    move-result v9

    .line 990
    .local v9, "ret":I
    if-nez v9, :cond_1

    .line 992
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 995
    const/4 v0, 0x0

    goto :goto_0

    .line 998
    :cond_1
    iget v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v0, :cond_4

    .line 999
    const/4 v0, 0x1

    iput v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 1003
    :cond_2
    :goto_1
    sub-int v0, p3, p2

    iget v2, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v11, v0, v2

    .line 1004
    sub-int v0, p5, p4

    iget v2, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v8, v0, v2

    .line 1006
    iget v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x7

    if-ne v0, v2, :cond_5

    .line 1007
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v8, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1012
    :goto_2
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    sub-int v4, p3, p2

    sub-int v5, p5, p4

    iget v6, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    move v2, p2

    move/from16 v3, p4

    invoke-static/range {v0 .. v6}, Lcom/quramsoft/qrb/QuramBitmapFactory;->PartialDecodeJPEGFromFile(ILandroid/graphics/Bitmap;IIIII)I

    move-result v9

    .line 1014
    if-nez v9, :cond_6

    .line 1016
    if-eqz v1, :cond_3

    .line 1018
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1019
    const/4 v1, 0x0

    .line 1022
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 1025
    const/4 v0, 0x0

    goto :goto_0

    .line 1000
    :cond_4
    iget v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v2, 0x8

    if-le v0, v2, :cond_2

    .line 1001
    const/16 v0, 0x8

    iput v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    goto :goto_1

    .line 1009
    :cond_5
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v8, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_2

    .line 1028
    :cond_6
    iget v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-ge v0, v10, :cond_7

    .line 1030
    iget v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v0, v11

    div-int/2addr v0, v10

    iget v2, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v2, v8

    div-int/2addr v2, v10

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1031
    .local v7, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1032
    move-object v1, v7

    .line 1033
    iput v10, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 1036
    .end local v7    # "bm":Landroid/graphics/Bitmap;
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v0, v1

    .line 1037
    goto :goto_0
.end method

.method public static prepareDecodeFileIter(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;II)I
    .locals 3
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "decWidth"    # I
    .param p3, "decHeight"    # I

    .prologue
    const/4 v1, 0x0

    .line 759
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v2

    if-eqz v2, :cond_0

    .line 760
    if-lez p2, :cond_0

    if-gtz p3, :cond_2

    :cond_0
    move v0, v1

    .line 773
    :cond_1
    :goto_0
    return v0

    .line 766
    :cond_2
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v2

    invoke-static {v2, p2, p3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->PrepareJPEGFromFileIter(III)I

    move-result v0

    .line 768
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 769
    invoke-virtual {p1, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0
.end method

.method public static prepareDecodeIter(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;II)I
    .locals 3
    .param p0, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p1, "decWidth"    # I
    .param p2, "decHeight"    # I

    .prologue
    const/4 v1, 0x0

    .line 785
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v2

    if-eqz v2, :cond_0

    .line 786
    if-lez p1, :cond_0

    if-gtz p2, :cond_2

    :cond_0
    move v0, v1

    .line 799
    :cond_1
    :goto_0
    return v0

    .line 792
    :cond_2
    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v2

    invoke-static {v2, p1, p2}, Lcom/quramsoft/qrb/QuramBitmapFactory;->PrepareJPEGFromFileIter(III)I

    move-result v0

    .line 794
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 795
    invoke-virtual {p0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0
.end method

.method public static round(F)I
    .locals 1
    .param p0, "val"    # F

    .prologue
    .line 26
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static translateBitmapFactoryOptions(Landroid/graphics/BitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .locals 4
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    const/4 v3, 0x7

    .line 1196
    new-instance v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 1197
    .local v0, "qrbOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    iget-object v1, p0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v1, v2, :cond_0

    .line 1199
    iput v3, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 1210
    :goto_0
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 1211
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->access$0(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)V

    .line 1212
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->access$1(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)V

    .line 1214
    return-object v0

    .line 1201
    :cond_0
    iget-object v1, p0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-ne v1, v2, :cond_1

    .line 1203
    const/4 v1, 0x0

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    goto :goto_0

    .line 1207
    :cond_1
    iput v3, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    goto :goto_0
.end method
