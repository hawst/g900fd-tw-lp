.class public final Lcom/quramsoft/qrb/QuramJpegCodec;
.super Ljava/lang/Object;
.source "QuramJpegCodec.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decodeJPEG_Full(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "fd"    # Ljava/io/FileDescriptor;

    .prologue
    .line 24
    invoke-static {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeJPEG_Full(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 39
    invoke-static {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->translateBitmapFactoryOptions(Landroid/graphics/BitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeJPEG_Full(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    .line 34
    invoke-static {p0, p1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeJPEG_Region(Lcom/quramsoft/qrb/QuramBitmapRegionDecoder;Landroid/graphics/BitmapFactory$Options;IIII)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "gbrd"    # Lcom/quramsoft/qrb/QuramBitmapRegionDecoder;
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "left"    # I
    .param p3, "right"    # I
    .param p4, "top"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 45
    const/4 v0, 0x0

    .line 46
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 47
    .local v1, "rect":Landroid/graphics/Rect;
    iput p2, v1, Landroid/graphics/Rect;->left:I

    .line 48
    iput p4, v1, Landroid/graphics/Rect;->top:I

    .line 49
    iput p3, v1, Landroid/graphics/Rect;->right:I

    .line 50
    iput p5, v1, Landroid/graphics/Rect;->bottom:I

    .line 52
    invoke-virtual {p0, v1, p1}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 53
    if-nez v0, :cond_0

    .line 55
    const-string v2, "QuramJpegCodec"

    const-string v3, "Error : region decoding fail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    :cond_0
    return-object v0
.end method

.method public static decodeJPEG_Thumbnail(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;III)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "thumbWidth"    # I
    .param p3, "thumbHeight"    # I
    .param p4, "origId"    # I

    .prologue
    .line 67
    invoke-static {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->translateBitmapFactoryOptions(Landroid/graphics/BitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v0

    invoke-static {p0, v0, p2, p3, p4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFileFromThumbnail(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;III)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeJPEG_Thumbnail(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;III)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "thumbWidth"    # I
    .param p3, "thumbHeight"    # I
    .param p4, "origId"    # I

    .prologue
    .line 62
    invoke-static {p0, p1, p2, p3, p4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFileFromThumbnail(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;III)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static partialDecodeJPEG(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;IIII)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "option"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "left"    # I
    .param p3, "right"    # I
    .param p4, "top"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 18
    invoke-static {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->translateBitmapFactoryOptions(Landroid/graphics/BitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v1

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->partialDecodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
