.class public Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
.super Ljava/lang/Object;
.source "QuramBitmapFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quramsoft/qrb/QuramBitmapFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImageBufferData"
.end annotation


# instance fields
.field public buffer:Ljava/nio/ByteBuffer;

.field public height:I

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333
    iput v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->width:I

    .line 334
    iput v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->height:I

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    .line 336
    return-void
.end method

.method public constructor <init>(IILjava/nio/ByteBuffer;)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 339
    iput p1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->width:I

    .line 340
    iput p2, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->height:I

    .line 341
    iput-object p3, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    .line 342
    return-void
.end method
