.class public Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
.super Ljava/lang/Object;
.source "QuramBitmapFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quramsoft/qrb/QuramBitmapFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Options"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quramsoft/qrb/QuramBitmapFactory$Options$Config;,
        Lcom/quramsoft/qrb/QuramBitmapFactory$Options$DecodeFromOption;,
        Lcom/quramsoft/qrb/QuramBitmapFactory$Options$InputType;
    }
.end annotation


# instance fields
.field public inCancelingRequested:Z

.field public inDecodeFromOption:I

.field public inDither:Z

.field public inInputType:I

.field public inPreferredConfig:I

.field public inQualityOverSpeed:I

.field public inSampleSize:I

.field private mDecodeEndInfo:I

.field private mDecodeHandle:I

.field private mDecodeReadInfo:I

.field private mDecodeStruct:I

.field private mHeight:I

.field private mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1100
    const/4 v0, 0x7

    iput v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 1102
    const/4 v0, 0x1

    iput v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 1103
    iput-boolean v1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inDither:Z

    .line 1105
    iput v1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inQualityOverSpeed:I

    .line 1106
    iput v1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inInputType:I

    .line 1107
    iput v1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inDecodeFromOption:I

    .line 1110
    iput v1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mDecodeHandle:I

    .line 1111
    iput v1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mWidth:I

    .line 1112
    iput v1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mHeight:I

    .line 1114
    iput-boolean v1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inCancelingRequested:Z

    .line 1117
    iput v1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mDecodeStruct:I

    .line 1118
    iput v1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mDecodeReadInfo:I

    .line 1119
    iput v1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mDecodeEndInfo:I

    .line 1150
    return-void
.end method

.method static synthetic access$0(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)V
    .locals 0

    .prologue
    .line 1111
    iput p1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mWidth:I

    return-void
.end method

.method static synthetic access$1(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)V
    .locals 0

    .prologue
    .line 1112
    iput p1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mHeight:I

    return-void
.end method


# virtual methods
.method protected getEndInfo()I
    .locals 1

    .prologue
    .line 1128
    iget v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mDecodeEndInfo:I

    return v0
.end method

.method protected getHandle()I
    .locals 1

    .prologue
    .line 1164
    iget v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mDecodeHandle:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 1159
    iget v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mHeight:I

    return v0
.end method

.method protected getReadInfo()I
    .locals 1

    .prologue
    .line 1125
    iget v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mDecodeReadInfo:I

    return v0
.end method

.method protected getStruct()I
    .locals 1

    .prologue
    .line 1122
    iget v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mDecodeStruct:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 1154
    iget v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mWidth:I

    return v0
.end method

.method protected setEndInfo(I)V
    .locals 0
    .param p1, "handle"    # I

    .prologue
    .line 1143
    iput p1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mDecodeEndInfo:I

    .line 1144
    return-void
.end method

.method protected setHandle(I)V
    .locals 0
    .param p1, "handle"    # I

    .prologue
    .line 1169
    iput p1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mDecodeHandle:I

    .line 1170
    return-void
.end method

.method protected setHeight(I)V
    .locals 0
    .param p1, "Height"    # I

    .prologue
    .line 1134
    iput p1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mHeight:I

    .line 1135
    return-void
.end method

.method protected setReadInfo(I)V
    .locals 0
    .param p1, "handle"    # I

    .prologue
    .line 1140
    iput p1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mDecodeReadInfo:I

    .line 1141
    return-void
.end method

.method protected setStruct(I)V
    .locals 0
    .param p1, "handle"    # I

    .prologue
    .line 1137
    iput p1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mDecodeStruct:I

    .line 1138
    return-void
.end method

.method protected setWidth(I)V
    .locals 0
    .param p1, "Width"    # I

    .prologue
    .line 1131
    iput p1, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->mWidth:I

    .line 1132
    return-void
.end method
