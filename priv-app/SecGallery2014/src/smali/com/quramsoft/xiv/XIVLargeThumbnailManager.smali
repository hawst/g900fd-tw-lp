.class public Lcom/quramsoft/xiv/XIVLargeThumbnailManager;
.super Ljava/lang/Object;
.source "XIVLargeThumbnailManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DECODE_STEP:I = 0x1fe

.field public static final LARGETHUMBNAIL_CENTER:I = 0x1

.field public static final LARGETHUMBNAIL_NEXT:I = 0x2

.field public static final LARGETHUMBNAIL_PREV:I = 0x0

.field private static final TAG:Ljava/lang/String; = "XIVLargeThumbnailManager"

.field static mTouchStart:J


# instance fields
.field mCurrentIndex:I

.field mFastLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

.field private mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

.field mToNext:Z

.field private mViewHeight:I

.field private mViewWidth:I

.field private mXiv:Lcom/quramsoft/xiv/XIV;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mTouchStart:J

    .line 26
    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    .line 117
    iput v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I

    .line 118
    iput v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewHeight:I

    .line 134
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .line 155
    iput-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mFastLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .line 290
    const/4 v0, -0x1

    iput v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mCurrentIndex:I

    .line 291
    iput-boolean v3, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mToNext:Z

    .line 112
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aput-object v2, v0, v1

    .line 113
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aput-object v2, v0, v3

    .line 114
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    const/4 v1, 0x2

    aput-object v2, v0, v1

    .line 115
    return-void
.end method

.method static synthetic access$0(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    return-object v0
.end method

.method static synthetic access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I

    return v0
.end method

.method static synthetic access$2(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewHeight:I

    return v0
.end method

.method static final create(Lcom/quramsoft/xiv/XIV;)Lcom/quramsoft/xiv/XIVLargeThumbnailManager;
    .locals 1
    .param p0, "xiv"    # Lcom/quramsoft/xiv/XIV;

    .prologue
    .line 39
    new-instance v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;-><init>()V

    .line 40
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVLargeThumbnailManager;
    iput-object p0, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    .line 41
    return-object v0
.end method

.method private drawBlended(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIFFLcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/android/gallery3d/ui/ScreenNail;FF)V
    .locals 8
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "offsetX"    # I
    .param p3, "offsetY"    # I
    .param p4, "drawWidth"    # F
    .param p5, "drawHeight"    # F
    .param p6, "largeThumbnail"    # Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    .param p7, "backUp"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p8, "alpha"    # F
    .param p9, "rotation"    # F

    .prologue
    .line 1412
    if-nez p7, :cond_1

    .line 1421
    :cond_0
    :goto_0
    return-void

    .line 1413
    :cond_1
    instance-of v0, p7, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    if-eqz v0, :cond_2

    move-object v0, p7

    check-cast v0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->isShowingPlaceholder()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1414
    :cond_2
    invoke-static {p4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-static {p5}, Ljava/lang/Math;->round(F)I

    move-result v5

    move-object v0, p7

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 1416
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->getAlpha()F

    move-result v7

    .line 1418
    .local v7, "canvasAlpha":F
    mul-float v0, v7, p8

    invoke-interface {p1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setAlpha(F)V

    move-object v0, p6

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p9

    .line 1419
    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIFFF)V
    invoke-static/range {v0 .. v6}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$63(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIFFF)V

    .line 1420
    invoke-interface {p1, v7}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setAlpha(F)V

    goto :goto_0
.end method

.method private refreshAll()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 234
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    if-eqz v0, :cond_0

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mFastLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .line 237
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getCurrentPhotoIndex()I

    move-result v0

    iput v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mCurrentIndex:I

    .line 239
    const/4 v0, 0x0

    iget v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mCurrentIndex:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshLargeThumbnail(IIZ)V

    .line 240
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mCurrentIndex:I

    invoke-direct {p0, v2, v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshLargeThumbnail(IIZ)V

    .line 241
    const/4 v0, 0x2

    iget v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mCurrentIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshLargeThumbnail(IIZ)V

    .line 243
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->decodeOnThreadIfNeeded()V
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$56(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V

    .line 247
    :cond_0
    return-void
.end method

.method private refreshLargeThumbnail(IIZ)V
    .locals 5
    .param p1, "pos"    # I
    .param p2, "index"    # I
    .param p3, "recycle"    # Z

    .prologue
    .line 206
    sget-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    if-eqz v2, :cond_1

    if-ltz p1, :cond_1

    const/4 v2, 0x3

    if-ge p1, v2, :cond_1

    .line 207
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v2, p2}, Lcom/quramsoft/xiv/XIV;->getItemFromIndex(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 208
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v2, v2, p1

    if-eqz v2, :cond_5

    .line 209
    if-eqz v0, :cond_3

    .line 210
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v2, v2, p1

    iget-object v2, v2, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eq v2, v0, :cond_2

    .line 211
    if-eqz p3, :cond_0

    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->recycle()V

    .line 212
    :cond_0
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    iget-object v3, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {p0, v3, v0, p2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->createLargeThumbnail(Lcom/quramsoft/xiv/XIV;Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-result-object v3

    aput-object v3, v2, p1

    .line 230
    .end local v0    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    :goto_0
    return-void

    .line 214
    .restart local v0    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v2, v2, p1

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFullImageCompelted:Z
    invoke-static {v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$59(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 215
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIV;->getPhotoDataAdapter()Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-result-object v1

    .line 216
    .local v1, "pda":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    if-eqz v1, :cond_1

    .line 217
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v2, v2, p1

    .line 218
    iget-object v3, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v3, v3, p1

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIndex:I
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$27(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v3

    iget-object v4, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v4, v4, p1

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-static {v4}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$23(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->excuteFullImageTask(ILcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)Z

    move-result v3

    .line 217
    invoke-static {v2, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$28(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    goto :goto_0

    .line 222
    .end local v1    # "pda":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    :cond_3
    const/4 v2, 0x1

    if-eq p1, v2, :cond_1

    .line 223
    if-eqz p3, :cond_4

    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->recycle()V

    .line 224
    :cond_4
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    const/4 v3, 0x0

    aput-object v3, v2, p1

    goto :goto_0

    .line 226
    :cond_5
    if-eqz v0, :cond_1

    .line 227
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    iget-object v3, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {p0, v3, v0, p2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->createLargeThumbnail(Lcom/quramsoft/xiv/XIV;Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-result-object v3

    aput-object v3, v2, p1

    goto :goto_0
.end method

.method private replaceLargeThumbnail(ILcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V
    .locals 1
    .param p1, "pos"    # I
    .param p2, "newLTN"    # Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    .param p3, "recycle"    # Z

    .prologue
    .line 195
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    if-eqz v0, :cond_1

    if-ltz p1, :cond_1

    const/4 v0, 0x3

    if-ge p1, v0, :cond_1

    .line 196
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->recycle()V

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aput-object p2, v0, p1

    .line 202
    :cond_1
    return-void
.end method


# virtual methods
.method public createAllOnCurrent()V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshAll()V

    .line 252
    return-void
.end method

.method public createCenterLargeThumbnail(Lcom/sec/android/gallery3d/data/MediaItem;I)V
    .locals 3
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "index"    # I

    .prologue
    const/4 v2, 0x1

    .line 157
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    if-eqz v0, :cond_1

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    iget-object v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {p0, v1, p1, p2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->createLargeThumbnail(Lcom/quramsoft/xiv/XIV;Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-result-object v1

    aput-object v1, v0, v2

    .line 163
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    invoke-static {v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$33(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 165
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->prepareToDecode()V
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$5(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V

    .line 166
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$6(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->decodeOnThreadIfNeeded()V
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$56(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V

    .line 168
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mFastLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    goto :goto_0
.end method

.method createLargeThumbnail(Lcom/quramsoft/xiv/XIV;Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    .locals 6
    .param p1, "xiv"    # Lcom/quramsoft/xiv/XIV;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "index"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 46
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/quramsoft/xiv/XIV;->getGalleryApp()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz p2, :cond_0

    if-gez p3, :cond_1

    :cond_0
    move-object v0, v1

    .line 107
    :goto_0
    return-object v0

    .line 52
    :cond_1
    new-instance v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;-><init>()V

    .line 54
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    invoke-static {v0, p1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$29(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/quramsoft/xiv/XIV;)V

    .line 55
    invoke-static {v0, p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$30(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)V

    .line 57
    iput-object p2, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 59
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$31(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Ljava/lang/String;)V

    .line 60
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$24(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 61
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$32(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 62
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$33(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 64
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$34(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 65
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$35(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 67
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$14(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 68
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$16(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 70
    const/4 v2, -0x1

    invoke-static {v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$36(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 71
    invoke-static {v0, v5}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$37(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 73
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$38(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 74
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$39(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 75
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$40(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 76
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$41(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 78
    invoke-static {v0, v4}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$42(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;F)V

    .line 79
    invoke-static {v0, v4}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$43(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;F)V

    .line 81
    invoke-static {v0, v4}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$44(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;F)V

    .line 82
    invoke-static {v0, v4}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$45(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;F)V

    .line 83
    invoke-static {v0, v4}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$46(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;F)V

    .line 84
    invoke-static {v0, v4}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$47(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;F)V

    .line 86
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$48(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 87
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$17(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 88
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$49(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 89
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$15(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 91
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$50(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 92
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$21(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 93
    invoke-static {v0, v5}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$28(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 94
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$51(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 96
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$52(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 98
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$20(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 99
    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$53(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 100
    invoke-static {v0, p3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$54(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 102
    iput-object v1, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mAlphablendingAnimation:Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    .line 104
    invoke-static {v0, v1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$22(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    goto :goto_0
.end method

.method public drawOnCenter(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIIFFLcom/sec/android/gallery3d/ui/ScreenNail;)Z
    .locals 27
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "offsetX"    # I
    .param p3, "offsetY"    # I
    .param p4, "imageWidth"    # I
    .param p5, "imageHeight"    # I
    .param p6, "rotation"    # F
    .param p7, "scale"    # F
    .param p8, "backUp"    # Lcom/sec/android/gallery3d/ui/ScreenNail;

    .prologue
    .line 1307
    sget-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    if-nez p8, :cond_1

    .line 1308
    :cond_0
    const/4 v2, 0x1

    .line 1403
    :goto_0
    return v2

    .line 1311
    :cond_1
    move/from16 v0, p4

    int-to-float v2, v0

    mul-float v2, v2, p7

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 1312
    .local v6, "drawWidth":I
    move/from16 v0, p5

    int-to-float v2, v0

    mul-float v2, v2, p7

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 1314
    .local v7, "drawHeight":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    iget-object v2, v2, Lcom/quramsoft/xiv/XIV;->mSlidingSpeedManager:Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getSlidingDurationLimit()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->isDurationUnderLimit(J)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v2, p8

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    .line 1315
    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 1316
    const/4 v2, 0x0

    goto :goto_0

    .line 1319
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIV;->getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v25

    .line 1320
    .local v25, "manager":Lcom/quramsoft/xiv/XIVLargeThumbnailManager;
    if-nez v25, :cond_3

    const/4 v2, 0x1

    goto :goto_0

    .line 1322
    :cond_3
    const/4 v2, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->getLargeThumbnail(I)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-result-object v14

    .line 1324
    .local v14, "largeThumbnail":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    if-eqz v14, :cond_12

    .line 1325
    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mParsingCompleted:Z
    invoke-static {v14}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$4(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1326
    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z
    invoke-static {v14}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$62(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1327
    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z
    invoke-static {v14}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$6(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1329
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getCenterLTNTimeLimit()I

    move-result v2

    .line 1330
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getCenterLTNTimeDelay()I

    move-result v3

    .line 1328
    move-object/from16 v0, p1

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    invoke-static {v14, v0, v2, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$57(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    move-object/from16 v2, p8

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    .line 1331
    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 1332
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    move-object/from16 v2, p8

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    .line 1334
    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 1335
    const/4 v2, 0x1

    goto :goto_0

    .line 1338
    :cond_5
    sget-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_SIDE_LARGETHUMBNAIL:Z

    if-eqz v2, :cond_8

    .line 1339
    move-object/from16 v0, v25

    iget-boolean v2, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mToNext:Z

    if-eqz v2, :cond_9

    .line 1340
    const/4 v2, 0x2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->getLargeThumbnail(I)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-result-object v24

    .line 1342
    .local v24, "first":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    :goto_1
    if-eqz v24, :cond_6

    .line 1343
    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mParsingCompleted:Z
    invoke-static/range {v24 .. v24}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$4(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1344
    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z
    invoke-static/range {v24 .. v24}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$6(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-eqz v2, :cond_6

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z
    invoke-static/range {v24 .. v24}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$62(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1346
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getSideLTNTimeLimit()I

    move-result v2

    .line 1347
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getSideLTNTimeDelay()I

    move-result v3

    .line 1345
    move-object/from16 v0, v24

    move-object/from16 v1, p1

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    invoke-static {v0, v1, v2, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$57(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1354
    :cond_6
    :goto_2
    sget-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_JUST_ONE_THREAD:Z

    if-eqz v2, :cond_7

    .line 1355
    if-eqz v24, :cond_7

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z
    invoke-static/range {v24 .. v24}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$62(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1357
    :cond_7
    move-object/from16 v0, v25

    iget-boolean v2, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mToNext:Z

    if-eqz v2, :cond_b

    .line 1358
    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->getLargeThumbnail(I)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-result-object v26

    .line 1360
    .local v26, "second":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    :goto_3
    if-eqz v26, :cond_8

    .line 1361
    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mParsingCompleted:Z
    invoke-static/range {v26 .. v26}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$4(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1362
    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z
    invoke-static/range {v26 .. v26}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$6(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-eqz v2, :cond_8

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z
    invoke-static/range {v26 .. v26}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$62(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1364
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getSideLTNTimeLimit()I

    move-result v2

    .line 1365
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getSideLTNTimeDelay()I

    move-result v3

    .line 1363
    move-object/from16 v0, v26

    move-object/from16 v1, p1

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    invoke-static {v0, v1, v2, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$57(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1374
    .end local v24    # "first":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    .end local v26    # "second":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    :cond_8
    :goto_4
    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->readyToDraw()Z
    invoke-static {v14}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$55(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1375
    iget-object v2, v14, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mAlphablendingAnimation:Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    if-eqz v2, :cond_d

    .line 1376
    iget-object v2, v14, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mAlphablendingAnimation:Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->needToAnimate()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1378
    iget-object v2, v14, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mAlphablendingAnimation:Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->getProgress()F

    move-result v16

    .line 1379
    .local v16, "alpha":F
    int-to-float v12, v6

    int-to-float v13, v7

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move/from16 v10, p2

    move/from16 v11, p3

    move-object/from16 v15, p8

    move/from16 v17, p6

    invoke-direct/range {v8 .. v17}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->drawBlended(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIFFLcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/android/gallery3d/ui/ScreenNail;FF)V

    .line 1380
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1341
    .end local v16    # "alpha":F
    :cond_9
    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->getLargeThumbnail(I)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-result-object v24

    goto/16 :goto_1

    .line 1350
    .restart local v24    # "first":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    :cond_a
    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->decodeOnThreadIfNeeded()V
    invoke-static/range {v24 .. v24}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$56(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V

    goto :goto_2

    .line 1359
    :cond_b
    const/4 v2, 0x2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->getLargeThumbnail(I)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-result-object v26

    goto :goto_3

    .line 1368
    .restart local v26    # "second":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    :cond_c
    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->decodeOnThreadIfNeeded()V
    invoke-static/range {v26 .. v26}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$56(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V

    goto :goto_4

    .line 1382
    .end local v24    # "first":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    .end local v26    # "second":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    :cond_d
    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z
    invoke-static {v14}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$62(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-nez v2, :cond_e

    if-eqz p8, :cond_e

    move-object/from16 v2, p8

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    .line 1383
    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 1384
    :cond_e
    int-to-float v0, v6

    move/from16 v21, v0

    int-to-float v0, v7

    move/from16 v22, v0

    move-object/from16 v17, v14

    move-object/from16 v18, p1

    move/from16 v19, p2

    move/from16 v20, p3

    move/from16 v23, p6

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIFFF)V
    invoke-static/range {v17 .. v23}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$63(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIFFF)V

    .line 1390
    :goto_5
    invoke-virtual/range {v25 .. v25}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->isUpdateCompeltedAll()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1391
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_f
    move-object/from16 v2, p8

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    .line 1387
    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto :goto_5

    .line 1393
    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_11
    move-object/from16 v2, p8

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    .line 1397
    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 1398
    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->decodeOnThreadIfNeeded()V
    invoke-static {v14}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$56(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V

    .line 1399
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_12
    move-object/from16 v2, p8

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    .line 1402
    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 1403
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method public drawOnScreenNail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ZLcom/sec/android/gallery3d/ui/ScreenNail;IIFFF)V
    .locals 21
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "isNext"    # Z
    .param p3, "backUp"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p4, "x"    # I
    .param p5, "y"    # I
    .param p6, "drawWidth"    # F
    .param p7, "drawHeight"    # F
    .param p8, "rotation"    # F

    .prologue
    .line 1263
    sget-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    if-eqz v2, :cond_0

    .line 1264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 1295
    :cond_0
    :goto_0
    return-void

    .line 1266
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIV;->getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v18

    .line 1267
    .local v18, "largeThumbnailManager":Lcom/quramsoft/xiv/XIVLargeThumbnailManager;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIV;->getSlidingSpeedManager()Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

    move-result-object v19

    .line 1268
    .local v19, "speedManager":Lcom/quramsoft/xiv/XIVSlidingSpeedManager;
    if-eqz v18, :cond_0

    if-eqz v19, :cond_0

    .line 1270
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getSlidingDurationLimit()I

    move-result v2

    int-to-long v2, v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->isDurationUnderLimit(J)Z

    move-result v20

    .line 1271
    .local v20, "underLimit":Z
    if-eqz v20, :cond_2

    .line 1272
    invoke-static/range {p6 .. p6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-static/range {p7 .. p7}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v2, p3

    move-object/from16 v3, p1

    move/from16 v4, p4

    move/from16 v5, p5

    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto :goto_0

    .line 1277
    :cond_2
    if-eqz p2, :cond_3

    .line 1278
    const/4 v2, 0x2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->getLargeThumbnail(I)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-result-object v8

    .line 1281
    .local v8, "largeThumbnail":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    :goto_1
    sget-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_SIDE_LARGETHUMBNAIL:Z

    if-eqz v2, :cond_6

    if-eqz v8, :cond_6

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->readyToDraw()Z
    invoke-static {v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$55(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1282
    iget-object v2, v8, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mAlphablendingAnimation:Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    if-eqz v2, :cond_4

    .line 1283
    iget-object v2, v8, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mAlphablendingAnimation:Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->needToAnimate()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1285
    iget-object v2, v8, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mAlphablendingAnimation:Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->getProgress()F

    move-result v10

    .local v10, "alpha":F
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v9, p3

    move/from16 v11, p8

    .line 1286
    invoke-direct/range {v2 .. v11}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->drawBlended(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIFFLcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/android/gallery3d/ui/ScreenNail;FF)V

    goto :goto_0

    .line 1279
    .end local v8    # "largeThumbnail":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    .end local v10    # "alpha":F
    :cond_3
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->getLargeThumbnail(I)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-result-object v8

    goto :goto_1

    .line 1288
    .restart local v8    # "largeThumbnail":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    :cond_4
    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z
    invoke-static {v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$62(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1289
    invoke-static/range {p6 .. p6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-static/range {p7 .. p7}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v2, p3

    move-object/from16 v3, p1

    move/from16 v4, p4

    move/from16 v5, p5

    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    :cond_5
    move-object v11, v8

    move-object/from16 v12, p1

    move/from16 v13, p4

    move/from16 v14, p5

    move/from16 v15, p6

    move/from16 v16, p7

    move/from16 v17, p8

    .line 1290
    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIFFF)V
    invoke-static/range {v11 .. v17}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$63(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIFFF)V

    goto/16 :goto_0

    .line 1293
    :cond_6
    invoke-static/range {p6 .. p6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-static/range {p7 .. p7}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v2, p3

    move-object/from16 v3, p1

    move/from16 v4, p4

    move/from16 v5, p5

    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto/16 :goto_0
.end method

.method getLargeThumbnail(I)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 141
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-ge p1, v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, p1

    .line 144
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMinLevel(FII)Z
    .locals 7
    .param p1, "currentScale"    # F
    .param p2, "imageWidth"    # I
    .param p3, "backUpWidth"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 1437
    iget-object v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-nez v6, :cond_1

    .line 1452
    :cond_0
    :goto_0
    return v4

    .line 1440
    :cond_1
    invoke-static {p2, p3}, Lcom/quramsoft/xiv/XIVUtils;->getLevelCount(II)I

    move-result v6

    .line 1439
    invoke-static {p1, v6}, Lcom/quramsoft/xiv/XIVUtils;->getLevel(FI)I

    move-result v0

    .line 1442
    .local v0, "curLevel":I
    iget-object v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v6, v6, v5

    if-eqz v6, :cond_2

    .line 1443
    iget-object v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v6, v6, v5

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLevel:I
    invoke-static {v6}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$64(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v1

    .line 1444
    .local v1, "level":I
    if-ltz v1, :cond_2

    if-gt v1, v0, :cond_2

    move v4, v5

    .line 1445
    goto :goto_0

    .line 1448
    .end local v1    # "level":I
    :cond_2
    iget-object v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v6}, Lcom/quramsoft/xiv/XIV;->getMinScale()F

    move-result v3

    .line 1450
    .local v3, "scaleMin":F
    invoke-static {p2, p3}, Lcom/quramsoft/xiv/XIVUtils;->getLevelCount(II)I

    move-result v6

    .line 1449
    invoke-static {v3, v6}, Lcom/quramsoft/xiv/XIVUtils;->getLevel(FI)I

    move-result v2

    .line 1452
    .local v2, "minLevel":I
    if-ne v2, v0, :cond_0

    move v4, v5

    goto :goto_0
.end method

.method isUpdateCompeltedAll()Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 369
    iget-object v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v4, v8, v7

    .line 370
    .local v4, "prev":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    iget-object v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v8, v6

    .line 371
    .local v0, "center":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    iget-object v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    const/4 v9, 0x2

    aget-object v2, v8, v9

    .line 373
    .local v2, "next":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    if-nez v4, :cond_0

    move v5, v6

    .line 374
    .local v5, "prev_ok":Z
    :goto_0
    if-nez v0, :cond_1

    move v1, v7

    .line 375
    .local v1, "center_ok":Z
    :goto_1
    if-nez v2, :cond_2

    move v3, v6

    .line 377
    .local v3, "next_ok":Z
    :goto_2
    if-eqz v5, :cond_3

    if-eqz v1, :cond_3

    if-eqz v3, :cond_3

    :goto_3
    return v6

    .line 373
    .end local v1    # "center_ok":Z
    .end local v3    # "next_ok":Z
    .end local v5    # "prev_ok":Z
    :cond_0
    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->isUpdateCompelted()Z
    invoke-static {v4}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$61(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v5

    goto :goto_0

    .line 374
    .restart local v5    # "prev_ok":Z
    :cond_1
    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->isUpdateCompelted()Z
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$61(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v1

    goto :goto_1

    .line 375
    .restart local v1    # "center_ok":Z
    :cond_2
    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->isUpdateCompelted()Z
    invoke-static {v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$61(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    goto :goto_2

    .restart local v3    # "next_ok":Z
    :cond_3
    move v6, v7

    .line 377
    goto :goto_3
.end method

.method moveTo(I)V
    .locals 4
    .param p1, "targetIndex"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 294
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-nez v0, :cond_1

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 296
    :cond_1
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mCurrentIndex:I

    sub-int v0, p1, v0

    packed-switch v0, :pswitch_data_0

    .line 337
    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mToNext:Z

    .line 338
    invoke-direct {p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshAll()V

    .line 342
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->isBeingSliding()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v1

    if-eqz v0, :cond_2

    .line 343
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v1

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->decodeOnThreadIfNeeded()V
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$56(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V

    .line 346
    :cond_2
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mCurrentIndex:I

    goto :goto_0

    .line 300
    :pswitch_1
    iput-boolean v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mToNext:Z

    .line 302
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    invoke-direct {p0, v3, v0, v1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->replaceLargeThumbnail(ILcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 303
    invoke-direct {p0, v1, p1, v1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshLargeThumbnail(IIZ)V

    .line 304
    add-int/lit8 v0, p1, -0x1

    invoke-direct {p0, v2, v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshLargeThumbnail(IIZ)V

    goto :goto_1

    .line 309
    :pswitch_2
    iput-boolean v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mToNext:Z

    .line 311
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v1

    invoke-direct {p0, v3, v0, v1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->replaceLargeThumbnail(ILcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 312
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    invoke-direct {p0, v1, v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->replaceLargeThumbnail(ILcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 313
    add-int/lit8 v0, p1, -0x1

    invoke-direct {p0, v2, v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshLargeThumbnail(IIZ)V

    goto :goto_1

    .line 320
    :pswitch_3
    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mToNext:Z

    .line 322
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v1

    invoke-direct {p0, v2, v0, v1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->replaceLargeThumbnail(ILcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 323
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v3

    invoke-direct {p0, v1, v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->replaceLargeThumbnail(ILcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 324
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v3, v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshLargeThumbnail(IIZ)V

    goto :goto_1

    .line 329
    :pswitch_4
    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mToNext:Z

    .line 331
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, v0, v1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->replaceLargeThumbnail(ILcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 332
    invoke-direct {p0, v1, p1, v1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshLargeThumbnail(IIZ)V

    .line 333
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v3, v0, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshLargeThumbnail(IIZ)V

    goto :goto_1

    .line 296
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public readyToDraw(I)Z
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 149
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, p1

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->readyToDraw()Z
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$55(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v0

    .line 152
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public refreshAllOnCurrent()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 256
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    if-eqz v0, :cond_2

    .line 257
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getCurrentPhotoIndex()I

    move-result v0

    iput v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mCurrentIndex:I

    .line 259
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->isReCreateLargeThumbnail()Z
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$60(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->recycle()V

    .line 262
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aput-object v4, v0, v2

    .line 263
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mCurrentIndex:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v2, v0, v1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshLargeThumbnail(IIZ)V

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v3

    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v3

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->isReCreateLargeThumbnail()Z
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$60(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->recycle()V

    .line 270
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aput-object v4, v0, v3

    .line 271
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mCurrentIndex:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v3, v0, v1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshLargeThumbnail(IIZ)V

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v1

    if-eqz v0, :cond_2

    .line 276
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v1

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->isReCreateLargeThumbnail()Z
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$60(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 277
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->recycle()V

    .line 278
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aput-object v4, v0, v1

    .line 279
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mCurrentIndex:I

    invoke-direct {p0, v1, v0, v1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->refreshLargeThumbnail(IIZ)V

    .line 280
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v1

    if-eqz v0, :cond_2

    .line 281
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v1

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->decodeOnThreadIfNeeded()V
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$56(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V

    .line 287
    :cond_2
    return-void
.end method

.method public releaseAll()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 351
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    if-eqz v0, :cond_2

    .line 352
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->recycle()V

    .line 354
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aput-object v4, v0, v1

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    if-eqz v0, :cond_1

    .line 357
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->recycle()V

    .line 358
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aput-object v4, v0, v2

    .line 360
    :cond_1
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v3

    if-eqz v0, :cond_2

    .line 361
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->recycle()V

    .line 362
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aput-object v4, v0, v3

    .line 365
    :cond_2
    return-void
.end method

.method public setViewSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 122
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I

    .line 123
    iput p2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewHeight:I

    .line 128
    return-void
.end method

.method public uploadCenterLargeThumbnail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z
    .locals 3
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mFastLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mFastLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .line 179
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getCenterLTNTimeLimit()I

    move-result v1

    .line 180
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getCenterLTNTimeDelay()I

    move-result v2

    .line 178
    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    invoke-static {v0, p1, v1, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$57(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 181
    const/4 v0, 0x1

    .line 184
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public useCenterLargeThumbnail()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1425
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v2, v2, v1

    if-eqz v2, :cond_0

    .line 1426
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v2, v2, v1

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z
    invoke-static {v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1428
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v2, v2, v1

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mParsingCompleted:Z
    invoke-static {v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$4(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1429
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    aget-object v2, v2, v1

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z
    invoke-static {v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$6(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1432
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1428
    goto :goto_0
.end method

.method public waitCenterLargeThumbnailDone()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mFastLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mFastLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeTask:Lcom/sec/android/gallery3d/util/Future;
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$58(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->waitDone()V

    .line 191
    :cond_0
    return-void
.end method
