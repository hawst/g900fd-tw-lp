.class public Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
.super Ljava/lang/Object;
.source "XIVLargeThumbnailManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quramsoft/xiv/XIVLargeThumbnailManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XIVLargeThumbnail"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;,
        Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    }
.end annotation


# static fields
.field static final TILE_STATE_DECODED:I = 0x1

.field static final TILE_STATE_FAILED:I = 0x3

.field static final TILE_STATE_NOT_DECODED:I = 0x0

.field static final TILE_STATE_UPDATED:I = 0x2


# instance fields
.field private mActiveEndColumn:I

.field private mActiveEndRow:I

.field private mActiveStartColumn:I

.field private mActiveStartRow:I

.field mAlphablendingAnimation:Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

.field private mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

.field private mDecodeScale:F

.field private mDecodeTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private mDecodedTileCount:I

.field private mDecodingCompleted:Z

.field private mDrawHeight:F

.field private mDrawScale:F

.field private mDrawTileHStep:F

.field private mDrawTileHeight:F

.field private mDrawTileWStep:F

.field private mDrawTileWidth:F

.field private mDrawWidth:F

.field private mFilePath:Ljava/lang/String;

.field private mFullImageCompelted:Z

.field private mImageHeight:I

.field private mImageWidth:I

.field private mIndex:I

.field private mIsClippingColumn:Z

.field private mIsRecycled:Z

.field mItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mLargeThumbnailTileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;",
            ">;"
        }
    .end annotation
.end field

.field private mLevel:I

.field private mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

.field private mMaxColumn:I

.field private mMaxRow:I

.field private mNextUploadingTileCount:I

.field private mParsingCompleted:Z

.field private mPreviewimageToSampleimageScale:F

.field private mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

.field private mSampleSize:I

.field private mSourceFirstClipEnd:I

.field private mSourceFirstClipStart:I

.field private mSourceLastClipEnd:I

.field private mSourceLastClipStart:I

.field private final mSourceRect:Landroid/graphics/RectF;

.field private mTargetClipSize:F

.field private mTargetFirstClipEnd:F

.field private mTargetFirstClipStart:F

.field private mTargetLastClipEnd:F

.field private mTargetLastClipStart:F

.field private mTargetOffset:F

.field private final mTargetRect:Landroid/graphics/RectF;

.field private mUnsampledHStep:I

.field private mUnsampledHeight:I

.field private mUnsampledWStep:I

.field private mUnsampledWidth:I

.field private mUploadingCompelted:Z

.field private mUseFastLargeThumbnail:Z

.field private mUseLargeThumbnail:Z

.field private mUsePNGLargeThumbnail:Z

.field private mXiv:Lcom/quramsoft/xiv/XIV;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeTask:Lcom/sec/android/gallery3d/util/Future;

    .line 454
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLargeThumbnailTileList:Ljava/util/ArrayList;

    .line 985
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceRect:Landroid/graphics/RectF;

    .line 986
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetRect:Landroid/graphics/RectF;

    .line 383
    return-void
.end method

.method static synthetic access$0(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/xiv/XIV;
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;

    return-object v0
.end method

.method static synthetic access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z
    .locals 1

    .prologue
    .line 448
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z

    return v0
.end method

.method static synthetic access$10(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V
    .locals 0

    .prologue
    .line 452
    iput-object p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    return-void
.end method

.method static synthetic access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    return-object v0
.end method

.method static synthetic access$12(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z
    .locals 1

    .prologue
    .line 394
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUsePNGLargeThumbnail:Z

    return v0
.end method

.method static synthetic access$13(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$14(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 399
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxRow:I

    return-void
.end method

.method static synthetic access$15(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 441
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveEndRow:I

    return-void
.end method

.method static synthetic access$16(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 400
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I

    return-void
.end method

.method static synthetic access$17(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 439
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveEndColumn:I

    return-void
.end method

.method static synthetic access$18(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I
    .locals 1

    .prologue
    .line 400
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I

    return v0
.end method

.method static synthetic access$19(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLargeThumbnailTileList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I
    .locals 1

    .prologue
    .line 456
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodedTileCount:I

    return v0
.end method

.method static synthetic access$20(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 456
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodedTileCount:I

    return-void
.end method

.method static synthetic access$21(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V
    .locals 0

    .prologue
    .line 444
    iput-boolean p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodingCompleted:Z

    return-void
.end method

.method static synthetic access$22(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V
    .locals 0

    .prologue
    .line 459
    iput-object p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    return-void
.end method

.method static synthetic access$23(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    return-object v0
.end method

.method static synthetic access$24(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V
    .locals 0

    .prologue
    .line 393
    iput-boolean p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z

    return-void
.end method

.method static synthetic access$25(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V
    .locals 0

    .prologue
    .line 802
    invoke-direct {p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->freeTiles()V

    return-void
.end method

.method static synthetic access$26(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z
    .locals 1

    .prologue
    .line 395
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseFastLargeThumbnail:Z

    return v0
.end method

.method static synthetic access$27(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I
    .locals 1

    .prologue
    .line 397
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIndex:I

    return v0
.end method

.method static synthetic access$28(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V
    .locals 0

    .prologue
    .line 445
    iput-boolean p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFullImageCompelted:Z

    return-void
.end method

.method static synthetic access$29(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/quramsoft/xiv/XIV;)V
    .locals 0

    .prologue
    .line 385
    iput-object p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;

    return-void
.end method

.method static synthetic access$3(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I
    .locals 1

    .prologue
    .line 457
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mNextUploadingTileCount:I

    return v0
.end method

.method static synthetic access$30(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    return-void
.end method

.method static synthetic access$31(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFilePath:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$32(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V
    .locals 0

    .prologue
    .line 394
    iput-boolean p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUsePNGLargeThumbnail:Z

    return-void
.end method

.method static synthetic access$33(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V
    .locals 0

    .prologue
    .line 395
    iput-boolean p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseFastLargeThumbnail:Z

    return-void
.end method

.method static synthetic access$34(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 390
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    return-void
.end method

.method static synthetic access$35(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 391
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageHeight:I

    return-void
.end method

.method static synthetic access$36(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 402
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLevel:I

    return-void
.end method

.method static synthetic access$37(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 403
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSampleSize:I

    return-void
.end method

.method static synthetic access$38(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 404
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledWStep:I

    return-void
.end method

.method static synthetic access$39(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 405
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledHStep:I

    return-void
.end method

.method static synthetic access$4(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z
    .locals 1

    .prologue
    .line 443
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mParsingCompleted:Z

    return v0
.end method

.method static synthetic access$40(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 406
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledWidth:I

    return-void
.end method

.method static synthetic access$41(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 407
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledHeight:I

    return-void
.end method

.method static synthetic access$42(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;F)V
    .locals 0

    .prologue
    .line 411
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawWidth:F

    return-void
.end method

.method static synthetic access$43(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;F)V
    .locals 0

    .prologue
    .line 412
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawHeight:F

    return-void
.end method

.method static synthetic access$44(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;F)V
    .locals 0

    .prologue
    .line 416
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileWidth:F

    return-void
.end method

.method static synthetic access$45(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;F)V
    .locals 0

    .prologue
    .line 417
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileHeight:F

    return-void
.end method

.method static synthetic access$46(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;F)V
    .locals 0

    .prologue
    .line 418
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileWStep:F

    return-void
.end method

.method static synthetic access$47(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;F)V
    .locals 0

    .prologue
    .line 419
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileHStep:F

    return-void
.end method

.method static synthetic access$48(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 438
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveStartColumn:I

    return-void
.end method

.method static synthetic access$49(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 440
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveStartRow:I

    return-void
.end method

.method static synthetic access$5(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V
    .locals 0

    .prologue
    .line 884
    invoke-direct {p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->prepareToDecode()V

    return-void
.end method

.method static synthetic access$50(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V
    .locals 0

    .prologue
    .line 443
    iput-boolean p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mParsingCompleted:Z

    return-void
.end method

.method static synthetic access$51(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V
    .locals 0

    .prologue
    .line 446
    iput-boolean p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z

    return-void
.end method

.method static synthetic access$52(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V
    .locals 0

    .prologue
    .line 448
    iput-boolean p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z

    return-void
.end method

.method static synthetic access$53(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 457
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mNextUploadingTileCount:I

    return-void
.end method

.method static synthetic access$54(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V
    .locals 0

    .prologue
    .line 397
    iput p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIndex:I

    return-void
.end method

.method static synthetic access$55(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z
    .locals 1

    .prologue
    .line 980
    invoke-direct {p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->readyToDraw()Z

    move-result v0

    return v0
.end method

.method static synthetic access$56(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V
    .locals 0

    .prologue
    .line 850
    invoke-direct {p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->decodeOnThreadIfNeeded()V

    return-void
.end method

.method static synthetic access$57(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 0

    .prologue
    .line 988
    invoke-direct {p0, p1, p2, p3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    return-void
.end method

.method static synthetic access$58(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/sec/android/gallery3d/util/Future;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeTask:Lcom/sec/android/gallery3d/util/Future;

    return-object v0
.end method

.method static synthetic access$59(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z
    .locals 1

    .prologue
    .line 445
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFullImageCompelted:Z

    return v0
.end method

.method static synthetic access$6(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z
    .locals 1

    .prologue
    .line 393
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z

    return v0
.end method

.method static synthetic access$60(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z
    .locals 1

    .prologue
    .line 867
    invoke-direct {p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->isReCreateLargeThumbnail()Z

    move-result v0

    return v0
.end method

.method static synthetic access$61(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z
    .locals 1

    .prologue
    .line 975
    invoke-direct {p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->isUpdateCompelted()Z

    move-result v0

    return v0
.end method

.method static synthetic access$62(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z
    .locals 1

    .prologue
    .line 446
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z

    return v0
.end method

.method static synthetic access$63(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIFFF)V
    .locals 0

    .prologue
    .line 1149
    invoke-direct/range {p0 .. p6}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIFFF)V

    return-void
.end method

.method static synthetic access$64(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I
    .locals 1

    .prologue
    .line 402
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLevel:I

    return v0
.end method

.method static synthetic access$7(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I
    .locals 1

    .prologue
    .line 403
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSampleSize:I

    return v0
.end method

.method static synthetic access$8(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I
    .locals 1

    .prologue
    .line 390
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    return v0
.end method

.method static synthetic access$9(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I
    .locals 1

    .prologue
    .line 391
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageHeight:I

    return v0
.end method

.method private decodeOnThreadIfNeeded()V
    .locals 5

    .prologue
    .line 852
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeTask:Lcom/sec/android/gallery3d/util/Future;

    if-nez v2, :cond_0

    .line 853
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIV;->getPhotoDataAdapter()Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-result-object v0

    .line 854
    .local v0, "pda":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIV;->getGalleryApp()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v1

    .line 855
    .local v1, "pool":Lcom/sec/android/gallery3d/util/ThreadPool;
    if-nez v1, :cond_1

    .line 865
    .end local v0    # "pda":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .end local v1    # "pool":Lcom/sec/android/gallery3d/util/ThreadPool;
    :cond_0
    :goto_0
    return-void

    .line 856
    .restart local v0    # "pda":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .restart local v1    # "pool":Lcom/sec/android/gallery3d/util/ThreadPool;
    :cond_1
    if-eqz v0, :cond_2

    .line 858
    new-instance v2, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;

    invoke-direct {v2, p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;-><init>(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V

    .line 859
    iget-object v3, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget v4, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIndex:I

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getFullImageListener(Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/sec/android/gallery3d/util/FutureListener;

    move-result-object v3

    .line 857
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v2

    iput-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeTask:Lcom/sec/android/gallery3d/util/Future;

    goto :goto_0

    .line 862
    :cond_2
    new-instance v2, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;

    invoke-direct {v2, p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;-><init>(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V

    .line 861
    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v2

    iput-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeTask:Lcom/sec/android/gallery3d/util/Future;

    goto :goto_0
.end method

.method private draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIFFF)V
    .locals 25
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "sx"    # I
    .param p3, "sy"    # I
    .param p4, "drawWidth"    # F
    .param p5, "drawHeight"    # F
    .param p6, "rotation"    # F

    .prologue
    .line 1154
    if-nez p1, :cond_1

    .line 1250
    :cond_0
    :goto_0
    return-void

    .line 1155
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z

    if-nez v4, :cond_0

    .line 1157
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawWidth:F

    cmpl-float v4, p4, v4

    if-nez v4, :cond_2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawHeight:F

    cmpl-float v4, p5, v4

    if-eqz v4, :cond_3

    .line 1158
    :cond_2
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawWidth:F

    .line 1159
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput v0, v1, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawHeight:F

    .line 1161
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageHeight:I

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-static {v0, v1, v4, v5}, Lcom/quramsoft/xiv/XIVUtils;->getScale(FFII)F

    move-result v18

    .line 1163
    .local v18, "scale":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawScale:F

    cmpl-float v4, v4, v18

    if-eqz v4, :cond_3

    .line 1164
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawScale:F

    .line 1166
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledWidth:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileWidth:F

    .line 1167
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledHeight:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileHeight:F

    .line 1168
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledWStep:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileWStep:F

    .line 1169
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledHStep:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileHStep:F

    .line 1171
    sget-boolean v4, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_CLIPPING_LARGETHUMBNAIL:Z

    if-eqz v4, :cond_3

    .line 1172
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSampleSize:I

    div-int/2addr v4, v5

    int-to-float v4, v4

    div-float v4, v4, p4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mPreviewimageToSampleimageScale:F

    .line 1177
    .end local v18    # "scale":F
    :cond_3
    sget-boolean v4, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_CLIPPING_LARGETHUMBNAIL:Z

    if-eqz v4, :cond_4

    .line 1178
    move/from16 v0, p6

    float-to-int v9, v0

    move-object/from16 v4, p0

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    invoke-virtual/range {v4 .. v9}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->calculateClipPositionLTN(IIFFI)V

    .line 1180
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLargeThumbnailTileList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    .line 1183
    .local v16, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;>;"
    move-object/from16 v0, p0

    iget v0, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveStartRow:I

    move/from16 v17, v0

    .local v17, "r":I
    :goto_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveEndRow:I

    move/from16 v0, v17

    if-ge v0, v4, :cond_0

    .line 1184
    move/from16 v0, p3

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileHStep:F

    move/from16 v0, v17

    int-to-float v6, v0

    mul-float/2addr v5, v6

    add-float v14, v4, v5

    .line 1185
    .local v14, "dy":F
    move-object/from16 v0, p0

    iget v10, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveStartColumn:I

    .local v10, "c":I
    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveEndColumn:I

    if-lt v10, v4, :cond_5

    .line 1183
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 1186
    :cond_5
    move/from16 v0, p2

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileWStep:F

    int-to-float v6, v10

    mul-float/2addr v5, v6

    add-float v13, v4, v5

    .line 1189
    .local v13, "dx":F
    :try_start_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I

    mul-int v4, v4, v17

    add-int/2addr v4, v10

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1193
    .local v24, "tile":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    if-eqz v24, :cond_0

    .line 1195
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;->texture:Lcom/quramsoft/xiv/XIVBufferReusableTexture;

    move-object/from16 v23, v0

    .line 1196
    .local v23, "texture":Lcom/quramsoft/xiv/XIVBufferReusableTexture;
    if-eqz v23, :cond_0

    move-object/from16 v0, v24

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;->state:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 1198
    invoke-virtual/range {v23 .. v23}, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->getWidth()I

    move-result v22

    .line 1199
    .local v22, "texWidth":I
    invoke-virtual/range {v23 .. v23}, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->getHeight()I

    move-result v21

    .line 1201
    .local v21, "texHeight":I
    sget-boolean v4, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_CLIPPING_LARGETHUMBNAIL:Z

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsClippingColumn:Z

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveStartColumn:I

    if-ne v10, v4, :cond_8

    if-gez p2, :cond_8

    .line 1202
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipEnd:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipStart:F

    sub-float v12, v4, v5

    .line 1203
    .local v12, "drawTileWidth":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mPreviewimageToSampleimageScale:F

    mul-float/2addr v4, v12

    float-to-int v0, v4

    move/from16 v22, v0

    .line 1204
    const/4 v13, 0x0

    .line 1206
    if-lez v10, :cond_6

    .line 1207
    add-int/lit8 v22, v22, -0x1

    .line 1224
    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxRow:I

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v17

    if-ge v0, v4, :cond_b

    .line 1225
    add-int/lit8 v21, v21, -0x1

    .line 1226
    move-object/from16 v0, p0

    iget v11, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileHeight:F

    .line 1232
    .local v11, "drawTileHeight":F
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceRect:Landroid/graphics/RectF;

    move-object/from16 v19, v0

    .line 1233
    .local v19, "source":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    .line 1235
    .local v20, "target":Landroid/graphics/RectF;
    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v0, v22

    int-to-float v6, v0

    move/from16 v0, v21

    int-to-float v7, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1236
    if-lez v10, :cond_c

    .line 1237
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    .line 1241
    :goto_5
    sget-boolean v4, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_CLIPPING_LARGETHUMBNAIL:Z

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsClippingColumn:Z

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveStartColumn:I

    if-ne v10, v4, :cond_7

    .line 1242
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceFirstClipStart:I

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    .line 1245
    :cond_7
    add-float v4, v13, v12

    add-float v5, v14, v11

    move-object/from16 v0, v20

    invoke-virtual {v0, v13, v14, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1247
    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1185
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2

    .line 1190
    .end local v11    # "drawTileHeight":F
    .end local v12    # "drawTileWidth":F
    .end local v19    # "source":Landroid/graphics/RectF;
    .end local v20    # "target":Landroid/graphics/RectF;
    .end local v21    # "texHeight":I
    .end local v22    # "texWidth":I
    .end local v23    # "texture":Lcom/quramsoft/xiv/XIVBufferReusableTexture;
    .end local v24    # "tile":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    :catch_0
    move-exception v15

    .line 1191
    .local v15, "e":Ljava/lang/IndexOutOfBoundsException;
    goto/16 :goto_0

    .line 1209
    .end local v15    # "e":Ljava/lang/IndexOutOfBoundsException;
    .restart local v21    # "texHeight":I
    .restart local v22    # "texWidth":I
    .restart local v23    # "texture":Lcom/quramsoft/xiv/XIVBufferReusableTexture;
    .restart local v24    # "tile":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    :cond_8
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I

    add-int/lit8 v4, v4, -0x1

    if-ge v10, v4, :cond_a

    .line 1210
    if-lez v10, :cond_9

    .line 1211
    add-int/lit8 v22, v22, -0x1

    .line 1212
    :cond_9
    move-object/from16 v0, p0

    iget v12, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileWidth:F

    .line 1218
    .restart local v12    # "drawTileWidth":F
    :goto_6
    sget-boolean v4, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_CLIPPING_LARGETHUMBNAIL:Z

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsClippingColumn:Z

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveEndColumn:I

    add-int/lit8 v4, v4, -0x1

    if-ne v10, v4, :cond_6

    move/from16 v0, p2

    int-to-float v4, v0

    add-float v4, v4, p4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I
    invoke-static {v5}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v5

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_6

    .line 1219
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipEnd:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipStart:F

    sub-float v12, v4, v5

    .line 1220
    move-object/from16 v0, p0

    iget v4, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mPreviewimageToSampleimageScale:F

    mul-float/2addr v4, v12

    float-to-int v0, v4

    move/from16 v22, v0

    goto/16 :goto_3

    .line 1214
    .end local v12    # "drawTileWidth":F
    :cond_a
    add-int/lit8 v22, v22, -0x1

    .line 1215
    move/from16 v0, p2

    int-to-float v4, v0

    add-float v4, v4, p4

    sub-float v12, v4, v13

    .restart local v12    # "drawTileWidth":F
    goto :goto_6

    .line 1228
    :cond_b
    add-int/lit8 v21, v21, -0x2

    .line 1229
    move/from16 v0, p3

    int-to-float v4, v0

    add-float v4, v4, p5

    sub-float v11, v4, v14

    .restart local v11    # "drawTileHeight":F
    goto/16 :goto_4

    .line 1239
    .restart local v19    # "source":Landroid/graphics/RectF;
    .restart local v20    # "target":Landroid/graphics/RectF;
    :cond_c
    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    goto/16 :goto_5
.end method

.method private freeTiles()V
    .locals 6

    .prologue
    .line 808
    iget-object v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLargeThumbnailTileList:Ljava/util/ArrayList;

    monitor-enter v5

    .line 809
    :try_start_0
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLargeThumbnailTileList:Ljava/util/ArrayList;

    .line 810
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 812
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_0

    .line 808
    monitor-exit v5

    .line 819
    return-void

    .line 813
    :cond_0
    iget-object v4, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLargeThumbnailTileList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;

    .line 814
    .local v3, "tile":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    if-eqz v3, :cond_1

    iget-object v4, v3, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;->texture:Lcom/quramsoft/xiv/XIVBufferReusableTexture;

    if-eqz v4, :cond_1

    .line 815
    iget-object v4, v3, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;->texture:Lcom/quramsoft/xiv/XIVBufferReusableTexture;

    invoke-virtual {v4}, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->recycle()V

    .line 812
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 808
    .end local v0    # "i":I
    .end local v1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;>;"
    .end local v2    # "size":I
    .end local v3    # "tile":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method private isCenterPhoto()Z
    .locals 2

    .prologue
    .line 823
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIndex:I

    iget-object v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v1}, Lcom/quramsoft/xiv/XIV;->getCurrentPhotoIndex()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isReCreateLargeThumbnail()Z
    .locals 5

    .prologue
    .line 869
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I
    invoke-static {v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 870
    iget-object v3, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewHeight:I
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$2(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 868
    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 874
    .local v1, "newScale":F
    iget v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    const/16 v3, 0x280

    invoke-static {v2, v3}, Lcom/quramsoft/xiv/XIVUtils;->getLevelCount(II)I

    move-result v2

    .line 872
    invoke-static {v1, v2}, Lcom/quramsoft/xiv/XIVUtils;->getLevel(FI)I

    move-result v0

    .line 876
    .local v0, "newLevel":I
    iget v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLevel:I

    if-eq v0, v2, :cond_0

    .line 877
    const/4 v2, 0x1

    .line 880
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isUpdateCompelted()Z
    .locals 1

    .prologue
    .line 977
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mParsingCompleted:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private prepareToDecode()V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 886
    iget-object v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLargeThumbnailTileList:Ljava/util/ArrayList;

    monitor-enter v6

    .line 887
    :try_start_0
    iget-object v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLargeThumbnailTileList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 886
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 890
    iput v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodedTileCount:I

    .line 891
    iput v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mNextUploadingTileCount:I

    .line 893
    iput-boolean v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodingCompleted:Z

    .line 894
    iput-boolean v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z

    .line 896
    iget-object v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFilePath:Ljava/lang/String;

    if-eqz v5, :cond_6

    .line 897
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 899
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v8, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 900
    iput-boolean v8, v2, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 901
    iget-object v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFilePath:Ljava/lang/String;

    invoke-static {v5, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 903
    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    .line 904
    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageHeight:I

    .line 906
    iget-object v5, v2, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    if-eqz v5, :cond_5

    iget v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    if-lez v5, :cond_5

    iget v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageHeight:I

    if-lez v5, :cond_5

    .line 908
    iget-object v5, v2, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v6, "image/jpeg"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 909
    iput-boolean v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z

    .line 910
    iput-boolean v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUsePNGLargeThumbnail:Z

    .line 921
    :goto_0
    sget-boolean v5, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LIMIT_WIDTH_OF_LTN:Z

    if-eqz v5, :cond_3

    .line 923
    iget-object v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I
    invoke-static {v5}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v5

    int-to-float v5, v5

    .line 924
    sget v6, Lcom/quramsoft/xiv/XIVDefinedValues;->LIMIT_OF_LTN_SIZE:I

    int-to-float v6, v6

    div-float/2addr v6, v9

    .line 922
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 926
    .local v4, "viewW":F
    iget-object v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewHeight:I
    invoke-static {v5}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$2(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v5

    int-to-float v5, v5

    .line 927
    sget v6, Lcom/quramsoft/xiv/XIVDefinedValues;->LIMIT_OF_LTN_SIZE:I

    int-to-float v6, v6

    div-float/2addr v6, v9

    .line 925
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 933
    .local v3, "viewH":F
    :goto_1
    sget-boolean v5, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_REMAKE_LTN:Z

    if-eqz v5, :cond_4

    .line 935
    iget v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    int-to-float v5, v5

    div-float v5, v4, v5

    .line 936
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageHeight:I

    int-to-float v6, v6

    div-float v6, v3, v6

    .line 934
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    iput v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeScale:F

    .line 939
    iget v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeScale:F

    .line 940
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    sget v7, Lcom/quramsoft/xiv/XIVCacheManager;->THUMBNAIL_SIZE:I

    invoke-static {v6, v7}, Lcom/quramsoft/xiv/XIVUtils;->getLevelCount(II)I

    move-result v6

    .line 938
    invoke-static {v5, v6}, Lcom/quramsoft/xiv/XIVUtils;->getLevel(FI)I

    move-result v5

    iput v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLevel:I

    .line 956
    :goto_2
    sget-boolean v5, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LTN_FIXED_SAMPLESIZE:Z

    if-eqz v5, :cond_0

    .line 957
    sget v5, Lcom/quramsoft/xiv/XIVDefinedValues;->FIXED_SAMPLE_LEVEL_FOR_LTN:I

    iput v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLevel:I

    .line 959
    :cond_0
    iget v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLevel:I

    shl-int v5, v8, v5

    iput v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSampleSize:I

    .line 961
    sget v5, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_W_STEP:I

    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLevel:I

    shl-int/2addr v5, v6

    iput v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledWStep:I

    .line 962
    const/16 v5, 0x1fe

    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLevel:I

    shl-int/2addr v5, v6

    iput v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledHStep:I

    .line 963
    sget v5, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_W_STEP:I

    add-int/lit8 v5, v5, 0x1

    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLevel:I

    shl-int/2addr v5, v6

    iput v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledWidth:I

    .line 964
    const/16 v5, 0x1ff

    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLevel:I

    shl-int/2addr v5, v6

    iput v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledHeight:I

    .line 972
    .end local v2    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v3    # "viewH":F
    .end local v4    # "viewW":F
    :goto_3
    iput-boolean v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mParsingCompleted:Z

    .line 973
    return-void

    .line 886
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 911
    .restart local v2    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_1
    sget-boolean v5, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_PNG_LARGETHUMBNAIL:Z

    if-eqz v5, :cond_2

    .line 912
    iget-object v5, v2, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v6, "image/png"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 914
    iput-boolean v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z

    .line 915
    iput-boolean v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUsePNGLargeThumbnail:Z

    goto/16 :goto_0

    .line 917
    :cond_2
    iput-boolean v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z

    goto/16 :goto_0

    .line 929
    :cond_3
    iget-object v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I
    invoke-static {v5}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v5

    int-to-float v4, v5

    .line 930
    .restart local v4    # "viewW":F
    iget-object v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewHeight:I
    invoke-static {v5}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$2(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v5

    int-to-float v3, v5

    .restart local v3    # "viewH":F
    goto :goto_1

    .line 944
    :cond_4
    iget v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    int-to-float v5, v5

    div-float v5, v4, v5

    .line 945
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageHeight:I

    int-to-float v6, v6

    div-float v6, v3, v6

    .line 943
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 948
    .local v0, "minScale1":F
    iget v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    int-to-float v5, v5

    div-float v5, v3, v5

    .line 949
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageHeight:I

    int-to-float v6, v6

    div-float v6, v4, v6

    .line 947
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 951
    .local v1, "minScale2":F
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeScale:F

    .line 953
    iget v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeScale:F

    .line 954
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageHeight:I

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    const/16 v7, 0x280

    invoke-static {v6, v7}, Lcom/quramsoft/xiv/XIVUtils;->getLevelCount(II)I

    move-result v6

    .line 952
    invoke-static {v5, v6}, Lcom/quramsoft/xiv/XIVUtils;->getLevel(FI)I

    move-result v5

    iput v5, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLevel:I

    goto/16 :goto_2

    .line 966
    .end local v0    # "minScale1":F
    .end local v1    # "minScale2":F
    .end local v3    # "viewH":F
    .end local v4    # "viewW":F
    :cond_5
    iput-boolean v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z

    goto :goto_3

    .line 969
    .end local v2    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_6
    iput-boolean v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z

    goto :goto_3
.end method

.method private readyToDraw()Z
    .locals 1

    .prologue
    .line 982
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "timeLimit"    # I
    .param p3, "timeDelay"    # I

    .prologue
    .line 990
    if-nez p1, :cond_1

    .line 1000
    :cond_0
    :goto_0
    return-void

    .line 991
    :cond_1
    iget-boolean v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z

    if-eqz v2, :cond_0

    .line 993
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v2, p3}, Lcom/quramsoft/xiv/XIV;->isAnimating(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 994
    invoke-direct {p0, p1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z

    goto :goto_0

    .line 996
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 997
    .local v0, "start":J
    :cond_3
    invoke-direct {p0, p1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/quramsoft/xiv/XIV;->isAnimating(I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    int-to-long v4, p2

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    goto :goto_0
.end method

.method private uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z
    .locals 9
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1004
    if-nez p1, :cond_1

    .line 1041
    :cond_0
    :goto_0
    return v6

    .line 1005
    :cond_1
    iget-boolean v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z

    if-eqz v8, :cond_0

    .line 1007
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodingCompleted:Z

    .line 1008
    .local v0, "decodeCompleted":Z
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLargeThumbnailTileList:Ljava/util/ArrayList;

    .line 1009
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1011
    .local v3, "size":I
    iget v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mNextUploadingTileCount:I

    if-ge v8, v3, :cond_0

    .line 1012
    const/4 v5, 0x0

    .line 1014
    .local v5, "tile":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    :try_start_0
    iget v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mNextUploadingTileCount:I

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "tile":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    check-cast v5, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1019
    .restart local v5    # "tile":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    if-eqz v5, :cond_3

    iget v6, v5, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;->state:I

    if-ne v6, v7, :cond_3

    .line 1020
    iget-object v4, v5, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;->texture:Lcom/quramsoft/xiv/XIVBufferReusableTexture;

    .line 1021
    .local v4, "texture":Lcom/quramsoft/xiv/XIVBufferReusableTexture;
    if-eqz v4, :cond_2

    .line 1022
    invoke-virtual {v4, p1}, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->updateContent(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 1023
    const/4 v6, 0x2

    iput v6, v5, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;->state:I

    .line 1026
    :cond_2
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mNextUploadingTileCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mNextUploadingTileCount:I

    .line 1028
    if-eqz v0, :cond_3

    iget-boolean v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z

    if-nez v6, :cond_3

    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mNextUploadingTileCount:I

    if-ne v6, v3, :cond_3

    .line 1029
    iput-boolean v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUploadingCompelted:Z

    .end local v4    # "texture":Lcom/quramsoft/xiv/XIVBufferReusableTexture;
    :cond_3
    move v6, v7

    .line 1039
    goto :goto_0

    .line 1015
    .end local v5    # "tile":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    :catch_0
    move-exception v1

    .line 1016
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    goto :goto_0
.end method


# virtual methods
.method public calculateClipPositionLTN(IIFFI)V
    .locals 9
    .param p1, "offsetX"    # I
    .param p2, "offsetY"    # I
    .param p3, "drawWidth"    # F
    .param p4, "drawHeight"    # F
    .param p5, "rotation"    # I

    .prologue
    .line 1045
    const/4 v6, 0x0

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveStartRow:I

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveStartColumn:I

    .line 1046
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxRow:I

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveEndRow:I

    .line 1047
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveEndColumn:I

    .line 1055
    div-int/lit8 v6, p5, 0x5a

    and-int/lit8 v6, v6, 0x1

    if-nez v6, :cond_3

    .line 1057
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsClippingColumn:Z

    .line 1058
    iput p3, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetClipSize:F

    .line 1060
    iget v4, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveStartColumn:I

    .line 1061
    .local v4, "ActiveStartBlock":I
    iget v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I

    iput v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveEndColumn:I

    .local v2, "ActiveEndBlock":I
    move v3, v2

    .line 1062
    .local v3, "ActiveMaxBlock":I
    sget v1, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_W_STEP:I

    .line 1063
    .local v1, "ActiveBlockStep":I
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileWStep:F

    .line 1065
    .local v0, "ActiveBlockDrawStep":F
    int-to-float v6, p1

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipStart:F

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetOffset:F

    .line 1066
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipStart:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileWStep:F

    add-float/2addr v6, v7

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipEnd:F

    .line 1068
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipStart:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I

    add-int/lit8 v7, v7, -0x1

    int-to-float v7, v7

    iget v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileWStep:F

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipStart:F

    .line 1069
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipStart:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I

    int-to-float v7, v7

    iget v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileWStep:F

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipEnd:F

    .line 1071
    const/4 v6, 0x0

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceLastClipStart:I

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceFirstClipStart:I

    .line 1072
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledWStep:I

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceLastClipEnd:I

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceFirstClipEnd:I

    .line 1095
    :goto_0
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetOffset:F

    const/4 v7, 0x0

    cmpg-float v6, v6, v7

    if-gez v6, :cond_4

    .line 1096
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetOffset:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetClipSize:F

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I
    invoke-static {v7}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v7

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-lez v6, :cond_1

    .line 1097
    iget-object v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I
    invoke-static {v6}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetOffset:F

    sub-float/2addr v6, v7

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipEnd:F

    .line 1098
    const/4 v6, 0x0

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceLastClipStart:I

    .line 1099
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipEnd:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mPreviewimageToSampleimageScale:F

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceLastClipEnd:I

    .line 1101
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceLastClipEnd:I

    div-int/2addr v6, v1

    add-int/lit8 v2, v6, 0x1

    .line 1102
    if-le v2, v3, :cond_0

    .line 1103
    move v2, v3

    .line 1105
    :cond_0
    add-int/lit8 v6, v2, -0x1

    int-to-float v6, v6

    mul-float/2addr v6, v0

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipStart:F

    .line 1106
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipEnd:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mPreviewimageToSampleimageScale:F

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceLastClipEnd:I

    .line 1109
    :cond_1
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetOffset:F

    neg-float v6, v6

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mPreviewimageToSampleimageScale:F

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceFirstClipStart:I

    .line 1110
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceFirstClipStart:I

    div-int v4, v6, v1

    .line 1111
    const/4 v6, 0x0

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipStart:F

    .line 1112
    add-int/lit8 v6, v4, 0x1

    int-to-float v6, v6

    mul-float/2addr v6, v0

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipEnd:F

    .line 1113
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipEnd:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mPreviewimageToSampleimageScale:F

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceFirstClipEnd:I

    .line 1115
    if-lez v4, :cond_2

    .line 1116
    mul-int v5, v4, v1

    .line 1117
    .local v5, "sourceOffset":I
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceFirstClipStart:I

    sub-int/2addr v6, v5

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceFirstClipStart:I

    .line 1118
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceFirstClipEnd:I

    sub-int/2addr v6, v5

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceFirstClipEnd:I

    .line 1139
    .end local v5    # "sourceOffset":I
    :cond_2
    :goto_1
    div-int/lit8 v6, p5, 0x5a

    and-int/lit8 v6, v6, 0x1

    if-nez v6, :cond_6

    .line 1140
    iput v4, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveStartColumn:I

    .line 1141
    iput v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveEndColumn:I

    .line 1147
    :goto_2
    return-void

    .line 1075
    .end local v0    # "ActiveBlockDrawStep":F
    .end local v1    # "ActiveBlockStep":I
    .end local v2    # "ActiveEndBlock":I
    .end local v3    # "ActiveMaxBlock":I
    .end local v4    # "ActiveStartBlock":I
    :cond_3
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsClippingColumn:Z

    .line 1076
    iput p4, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetClipSize:F

    .line 1078
    iget v4, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveStartRow:I

    .line 1079
    .restart local v4    # "ActiveStartBlock":I
    iget v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxRow:I

    iput v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveEndRow:I

    .restart local v2    # "ActiveEndBlock":I
    move v3, v2

    .line 1080
    .restart local v3    # "ActiveMaxBlock":I
    const/16 v1, 0x1fe

    .line 1081
    .restart local v1    # "ActiveBlockStep":I
    iget v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileHStep:F

    .line 1084
    .restart local v0    # "ActiveBlockDrawStep":F
    iget-object v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I
    invoke-static {v6}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v6

    iget-object v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewHeight:I
    invoke-static {v7}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$2(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v7

    sub-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, p2

    int-to-float v6, v6

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetOffset:F

    .line 1085
    int-to-float v6, p2

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipStart:F

    .line 1086
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipStart:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileHStep:F

    add-float/2addr v6, v7

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipEnd:F

    .line 1088
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipStart:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxRow:I

    add-int/lit8 v7, v7, -0x1

    int-to-float v7, v7

    iget v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileHStep:F

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipStart:F

    .line 1089
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipStart:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxRow:I

    int-to-float v7, v7

    iget v8, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDrawTileHStep:F

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipEnd:F

    .line 1091
    const/4 v6, 0x0

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceLastClipStart:I

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceFirstClipStart:I

    .line 1092
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUnsampledHStep:I

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceLastClipEnd:I

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceFirstClipEnd:I

    goto/16 :goto_0

    .line 1122
    :cond_4
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetOffset:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetClipSize:F

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I
    invoke-static {v7}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v7

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-lez v6, :cond_2

    .line 1123
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetFirstClipStart:F

    iget-object v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I
    invoke-static {v7}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v7

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-gtz v6, :cond_2

    .line 1124
    iget-object v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mViewWidth:I
    invoke-static {v6}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetOffset:F

    sub-float/2addr v6, v7

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipEnd:F

    .line 1125
    const/4 v6, 0x0

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceLastClipStart:I

    .line 1128
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipEnd:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mPreviewimageToSampleimageScale:F

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceLastClipEnd:I

    .line 1130
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceLastClipEnd:I

    div-int/2addr v6, v1

    add-int/lit8 v2, v6, 0x1

    .line 1131
    if-le v2, v3, :cond_5

    .line 1132
    move v2, v3

    .line 1134
    :cond_5
    add-int/lit8 v6, v2, -0x1

    int-to-float v6, v6

    mul-float/2addr v6, v0

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipStart:F

    .line 1135
    iget v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mTargetLastClipEnd:F

    iget v7, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mPreviewimageToSampleimageScale:F

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSourceLastClipEnd:I

    goto/16 :goto_1

    .line 1144
    :cond_6
    iput v4, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveStartRow:I

    .line 1145
    iput v2, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mActiveEndRow:I

    goto/16 :goto_2
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 466
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->recycle()V

    .line 467
    return-void
.end method

.method public recycle()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 828
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z

    if-eqz v0, :cond_1

    .line 848
    :cond_0
    :goto_0
    return-void

    .line 830
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z

    .line 832
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 834
    :cond_2
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodingCompleted:Z

    if-eqz v0, :cond_3

    .line 835
    invoke-direct {p0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->freeTiles()V

    .line 838
    :cond_3
    iput v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodedTileCount:I

    .line 839
    iput v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mNextUploadingTileCount:I

    .line 841
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    if-eqz v0, :cond_0

    .line 842
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->recycle()V

    goto :goto_0
.end method
