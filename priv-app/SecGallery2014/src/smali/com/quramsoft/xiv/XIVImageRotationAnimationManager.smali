.class public Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;
.super Ljava/lang/Object;
.source "XIVImageRotationAnimationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;,
        Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final STATE_ANIMATING:I = 0x2

.field public static final STATE_NONE:I = 0x0

.field public static final STATE_READY:I = 0x1

.field private static final TAG:Ljava/lang/String; = "XIVImageRotationAnimationManager"


# instance fields
.field private mXiv:Lcom/quramsoft/xiv/XIV;

.field private sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

.field private sMatrix:[F


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    .line 63
    iput-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    .line 64
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sMatrix:[F

    .line 32
    return-void
.end method

.method static final create(Lcom/quramsoft/xiv/XIV;)Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;
    .locals 1
    .param p0, "xiv"    # Lcom/quramsoft/xiv/XIV;

    .prologue
    .line 25
    new-instance v0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;-><init>()V

    .line 26
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;
    iput-object p0, v0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    .line 27
    return-object v0
.end method


# virtual methods
.method public getCurrentAnimationState()I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 141
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    if-eqz v1, :cond_1

    .line 142
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    # getter for: Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I
    invoke-static {v1}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->access$0(Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;)I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 148
    :goto_0
    return v0

    .line 145
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 148
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAnimating()Z
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    # getter for: Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->access$0(Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 129
    const/4 v0, 0x1

    .line 132
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public postRender(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)V
    .locals 2
    .param p1, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "isActive"    # Z

    .prologue
    .line 111
    if-eqz p3, :cond_1

    .line 112
    invoke-interface {p2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 113
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    .line 115
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->getState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 117
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    # getter for: Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mCallBack:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->access$2(Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;)Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    # getter for: Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mCallBack:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->access$2(Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;)Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;->run()V

    .line 120
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    .line 123
    :cond_1
    return-void
.end method

.method public preRender(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z
    .locals 13
    .param p1, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 104
    :goto_0
    return v0

    .line 67
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    if-eqz v0, :cond_5

    .line 70
    const/4 v0, -0x1

    invoke-interface {p2, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 72
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v9

    .line 74
    .local v9, "pc":Lcom/sec/android/gallery3d/ui/PositionController;
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v7

    .line 75
    .local v7, "bounds":Landroid/graphics/Rect;
    iget v0, v7, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 76
    .local v8, "left":I
    iget v0, v7, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v10

    .line 77
    .local v10, "right":I
    iget v0, v7, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v12

    .line 78
    .local v12, "top":I
    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 80
    .local v6, "bottom":I
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    # getter for: Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->access$0(Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 81
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    iget-object v1, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    sub-int v3, v6, v12

    int-to-float v3, v3

    invoke-virtual {v0, v1, v3}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->startAnimation(Lcom/quramsoft/xiv/XIV;F)V

    .line 82
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 85
    :cond_3
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    # getter for: Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->access$0(Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 86
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    iget-object v1, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0, v1}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->advanceAnimation(Lcom/quramsoft/xiv/XIV;)V

    .line 88
    add-int v0, v8, v10

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    add-int v1, v12, v6

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const/4 v3, 0x0

    invoke-interface {p2, v0, v1, v3}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FFF)V

    .line 89
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {p2, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->multiplyAlpha(F)V

    .line 91
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->getDegree()F

    move-result v2

    .line 92
    .local v2, "degree":F
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->getScale()F

    move-result v11

    .line 94
    .local v11, "scale":F
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {p2, v11, v11, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->scale(FFF)V

    .line 96
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sMatrix:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 97
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sMatrix:[F

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 99
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sMatrix:[F

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->multiplyMatrix([FI)V

    .line 100
    add-int v0, v8, v10

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    add-int v1, v12, v6

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const/4 v3, 0x0

    invoke-interface {p2, v0, v1, v3}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FFF)V

    .line 102
    .end local v2    # "degree":F
    .end local v11    # "scale":F
    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 104
    .end local v6    # "bottom":I
    .end local v7    # "bounds":Landroid/graphics/Rect;
    .end local v8    # "left":I
    .end local v9    # "pc":Lcom/sec/android/gallery3d/ui/PositionController;
    .end local v10    # "right":I
    .end local v12    # "top":I
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public prepareAnimation(Lcom/sec/android/gallery3d/ui/PhotoView;ZLcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;)V
    .locals 5
    .param p1, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "isClockwise"    # Z
    .param p3, "callback"    # Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;

    .prologue
    .line 36
    if-nez p1, :cond_1

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v1

    .line 39
    .local v1, "pc":Lcom/sec/android/gallery3d/ui/PositionController;
    if-eqz v1, :cond_0

    .line 40
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 41
    .local v0, "bounds":Landroid/graphics/Rect;
    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    int-to-float v2, v3

    .line 43
    .local v2, "width":F
    invoke-static {v2, p2, p3}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->createRotationAnimation(FZLcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;)Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    move-result-object v3

    iput-object v3, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    goto :goto_0
.end method

.method public startAnimation()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    # getter for: Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->access$0(Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;)I

    move-result v0

    if-nez v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->sCurrentRotationAnimation:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->access$1(Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;I)V

    .line 61
    :cond_0
    return-void
.end method
