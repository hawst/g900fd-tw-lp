.class public Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;
.super Ljava/lang/Object;
.source "XIVBufferReuseManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quramsoft/xiv/XIVBufferReuseManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BufferContainer"
.end annotation


# instance fields
.field private mFreeBitmapList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private mIsRecycled:Z

.field private mLimit:I

.field private mMaxLength:I

.field private mOverLimit:Z

.field private mType:I

.field final synthetic this$0:Lcom/quramsoft/xiv/XIVBufferReuseManager;


# direct methods
.method public constructor <init>(Lcom/quramsoft/xiv/XIVBufferReuseManager;)V
    .locals 1

    .prologue
    .line 77
    iput-object p1, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->this$0:Lcom/quramsoft/xiv/XIVBufferReuseManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    return-void
.end method

.method static synthetic access$0(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;I)V
    .locals 0

    .prologue
    .line 79
    iput p1, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mType:I

    return-void
.end method

.method static synthetic access$1(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;Z)V
    .locals 0

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mIsRecycled:Z

    return-void
.end method

.method static synthetic access$2(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;I)V
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mMaxLength:I

    return-void
.end method

.method static synthetic access$3(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;I)V
    .locals 0

    .prologue
    .line 86
    iput p1, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mLimit:I

    return-void
.end method

.method static synthetic access$4(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mMaxLength:I

    return v0
.end method

.method static synthetic access$5(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mOverLimit:Z

    return v0
.end method

.method static synthetic access$6(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    return-object v0
.end method


# virtual methods
.method freeBuffer(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mIsRecycled:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 131
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    iget v1, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mMaxLength:I

    if-eq v0, v1, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mLimit:I

    if-ge v0, v1, :cond_0

    .line 141
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 142
    monitor-enter p0

    .line 143
    :try_start_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 142
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method getBuffer()Ljava/nio/ByteBuffer;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 106
    iget-boolean v2, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mIsRecycled:Z

    if-eqz v2, :cond_0

    .line 125
    :goto_0
    return-object v0

    .line 109
    :cond_0
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    :try_start_0
    iget v2, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mMaxLength:I

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .local v0, "buffer":Ljava/nio/ByteBuffer;
    goto :goto_0

    .line 112
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    :catch_0
    move-exception v1

    .line 113
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    goto :goto_0

    .line 116
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :cond_1
    monitor-enter p0

    .line 117
    :try_start_1
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 116
    .restart local v0    # "buffer":Ljava/nio/ByteBuffer;
    monitor-exit p0

    goto :goto_0

    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method recycle()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mIsRecycled:Z

    .line 101
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->reset()V

    .line 102
    return-void
.end method

.method reset()V
    .locals 1

    .prologue
    .line 92
    monitor-enter p0

    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 92
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mOverLimit:Z

    .line 96
    return-void

    .line 92
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
