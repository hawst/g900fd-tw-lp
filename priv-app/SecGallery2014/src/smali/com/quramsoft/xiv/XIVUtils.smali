.class public Lcom/quramsoft/xiv/XIVUtils;
.super Ljava/lang/Object;
.source "XIVUtils.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "XIVUtils"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static alphaTransform(Ljavax/microedition/khronos/opengles/GL11;Landroid/graphics/RectF;[F)V
    .locals 4
    .param p0, "gl"    # Ljavax/microedition/khronos/opengles/GL11;
    .param p1, "target"    # Landroid/graphics/RectF;
    .param p2, "matrix"    # [F

    .prologue
    const/4 v3, 0x0

    .line 298
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    const/4 v2, 0x0

    invoke-static {p2, v3, v0, v1, v2}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 299
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {p2, v3, v0, v1, v2}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 301
    invoke-interface {p0, p2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 302
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-interface {p0, v0, v3, v1}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 303
    return-void
.end method

.method public static assertTrue(Z)V
    .locals 1
    .param p0, "cond"    # Z

    .prologue
    .line 84
    if-nez p0, :cond_0

    .line 85
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 87
    :cond_0
    return-void
.end method

.method public static varargs assertTrue(ZLjava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p0, "cond"    # Z
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 91
    if-nez p0, :cond_1

    .line 92
    new-instance v0, Ljava/lang/AssertionError;

    .line 93
    array-length v1, p2

    if-nez v1, :cond_0

    .line 92
    .end local p1    # "message":Ljava/lang/String;
    :goto_0
    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 93
    .restart local p1    # "message":Ljava/lang/String;
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 95
    :cond_1
    return-void
.end method

.method public static ceilLog2(F)I
    .locals 2
    .param p0, "value"    # F

    .prologue
    .line 205
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x1f

    if-lt v0, v1, :cond_1

    .line 209
    :cond_0
    return v0

    .line 206
    :cond_1
    const/4 v1, 0x1

    shl-int/2addr v1, v0

    int-to-float v1, v1

    cmpl-float v1, v1, p0

    if-gez v1, :cond_0

    .line 205
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static checkFileValidation(Ljava/lang/String;)Z
    .locals 3
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 358
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 362
    :cond_0
    :goto_0
    return v1

    .line 359
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 360
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 362
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static checkPositiveNumber(I)I
    .locals 1
    .param p0, "number"    # I

    .prologue
    const/4 v0, 0x1

    .line 290
    if-ge p0, v0, :cond_0

    move p0, v0

    .line 293
    .end local p0    # "number":I
    :cond_0
    return p0
.end method

.method public static checkWinkValidation(Ljava/lang/String;)Z
    .locals 2
    .param p0, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 366
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 371
    :cond_0
    :goto_0
    return v0

    .line 368
    :cond_1
    const-string v1, "image/jpeg"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "image/bmp"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 369
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static clamp(FFF)F
    .locals 1
    .param p0, "x"    # F
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 227
    cmpl-float v0, p0, p2

    if-lez v0, :cond_0

    .line 231
    .end local p2    # "max":F
    :goto_0
    return p2

    .line 229
    .restart local p2    # "max":F
    :cond_0
    cmpg-float v0, p0, p1

    if-gez v0, :cond_1

    move p2, p1

    .line 230
    goto :goto_0

    :cond_1
    move p2, p0

    .line 231
    goto :goto_0
.end method

.method public static clamp(III)I
    .locals 0
    .param p0, "x"    # I
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 218
    if-le p0, p2, :cond_0

    .line 222
    .end local p2    # "max":I
    :goto_0
    return p2

    .line 220
    .restart local p2    # "max":I
    :cond_0
    if-ge p0, p1, :cond_1

    move p2, p1

    .line 221
    goto :goto_0

    :cond_1
    move p2, p0

    .line 222
    goto :goto_0
.end method

.method public static clamp(JJJ)J
    .locals 2
    .param p0, "x"    # J
    .param p2, "min"    # J
    .param p4, "max"    # J

    .prologue
    .line 236
    cmp-long v0, p0, p4

    if-lez v0, :cond_0

    .line 240
    .end local p4    # "max":J
    :goto_0
    return-wide p4

    .line 238
    .restart local p4    # "max":J
    :cond_0
    cmp-long v0, p0, p2

    if-gez v0, :cond_1

    move-wide p4, p2

    .line 239
    goto :goto_0

    :cond_1
    move-wide p4, p0

    .line 240
    goto :goto_0
.end method

.method public static closeSilently(Ljava/io/Closeable;)V
    .locals 3
    .param p0, "c"    # Ljava/io/Closeable;

    .prologue
    .line 260
    if-nez p0, :cond_0

    .line 267
    :goto_0
    return-void

    .line 263
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "XIVUtils"

    const-string v2, "close fail"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static computeSampleSize(IIIIZ)I
    .locals 5
    .param p0, "srcW"    # I
    .param p1, "srcH"    # I
    .param p2, "dstW"    # I
    .param p3, "dstH"    # I
    .param p4, "larger"    # Z

    .prologue
    .line 39
    const/4 v2, 0x1

    .line 40
    .local v2, "sampleSize":I
    move v4, p0

    .line 41
    .local v4, "sampledW":I
    move v3, p1

    .line 45
    .local v3, "sampledH":I
    :goto_0
    div-int/lit8 v1, v4, 0x2

    .line 46
    .local v1, "nextW":I
    div-int/lit8 v0, v3, 0x2

    .line 48
    .local v0, "nextH":I
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 61
    :cond_0
    :goto_1
    return v2

    .line 50
    :cond_1
    if-eqz p4, :cond_3

    .line 51
    if-lt v1, p2, :cond_0

    if-lt v0, p3, :cond_0

    .line 56
    :cond_2
    move v4, v1

    .line 57
    move v3, v0

    .line 58
    mul-int/lit8 v2, v2, 0x2

    .line 43
    goto :goto_0

    .line 53
    :cond_3
    if-ge v1, p2, :cond_2

    if-ge v0, p3, :cond_2

    goto :goto_1
.end method

.method public static computeScale(IIIIZ)F
    .locals 3
    .param p0, "srcW"    # I
    .param p1, "srcH"    # I
    .param p2, "dstW"    # I
    .param p3, "dstH"    # I
    .param p4, "larger"    # Z

    .prologue
    .line 66
    if-eqz p4, :cond_0

    .line 67
    int-to-float v0, p2

    int-to-float v1, p0

    div-float/2addr v0, v1

    int-to-float v1, p3

    int-to-float v2, p1

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 69
    :goto_0
    return v0

    :cond_0
    int-to-float v0, p2

    int-to-float v1, p0

    div-float/2addr v0, v1

    int-to-float v1, p3

    int-to-float v2, p1

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0
.end method

.method public static cropThumbnailIfNeeded(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "cacheBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "type"    # I

    .prologue
    .line 170
    sget-boolean v1, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_WSTN:Z

    if-eqz v1, :cond_0

    .line 171
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    if-eqz p0, :cond_0

    .line 173
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 175
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 176
    .local v0, "targetSize":I
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 180
    .end local v0    # "targetSize":I
    .end local p0    # "cacheBitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-object p0
.end method

.method public static floorLog2(F)I
    .locals 2
    .param p0, "value"    # F

    .prologue
    .line 245
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x1f

    if-lt v0, v1, :cond_1

    .line 249
    :cond_0
    add-int/lit8 v1, v0, -0x1

    return v1

    .line 246
    :cond_1
    const/4 v1, 0x1

    shl-int/2addr v1, v0

    int-to-float v1, v1

    cmpl-float v1, v1, p0

    if-gtz v1, :cond_0

    .line 245
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static getConfig(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap$Config;
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 350
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    .line 351
    .local v0, "config":Landroid/graphics/Bitmap$Config;
    if-nez v0, :cond_0

    .line 352
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 354
    :cond_0
    return-object v0
.end method

.method public static getLevel(FI)I
    .locals 2
    .param p0, "scale"    # F
    .param p1, "levelCount"    # I

    .prologue
    const/4 v0, 0x0

    .line 253
    const/4 v1, 0x0

    cmpl-float v1, p0, v1

    if-eqz v1, :cond_0

    .line 254
    const/high16 v1, 0x3f800000    # 1.0f

    div-float/2addr v1, p0

    invoke-static {v1}, Lcom/quramsoft/xiv/XIVUtils;->floorLog2(F)I

    move-result v1

    invoke-static {v1, v0, p1}, Lcom/quramsoft/xiv/XIVUtils;->clamp(III)I

    move-result v0

    .line 256
    :cond_0
    return v0
.end method

.method public static getLevelCount(II)I
    .locals 3
    .param p0, "imageSize"    # I
    .param p1, "backUpSize"    # I

    .prologue
    .line 213
    const/4 v0, 0x0

    int-to-float v1, p0

    int-to-float v2, p1

    div-float/2addr v1, v2

    invoke-static {v1}, Lcom/quramsoft/xiv/XIVUtils;->ceilLog2(F)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static getScale(FFII)F
    .locals 2
    .param p0, "viewW"    # F
    .param p1, "viewH"    # F
    .param p2, "imageW"    # I
    .param p3, "imageH"    # I

    .prologue
    .line 200
    int-to-float v0, p2

    div-float v0, p0, v0

    int-to-float v1, p3

    div-float v1, p1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public static getSubImageBuffer([B[BIIIIIII)Z
    .locals 7
    .param p0, "src"    # [B
    .param p1, "dst"    # [B
    .param p2, "srcW"    # I
    .param p3, "srcH"    # I
    .param p4, "bytePerPixel"    # I
    .param p5, "left"    # I
    .param p6, "right"    # I
    .param p7, "top"    # I
    .param p8, "bottom"    # I

    .prologue
    .line 378
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    const/4 v4, 0x1

    if-ge p4, v4, :cond_1

    :cond_0
    const/4 v4, 0x0

    .line 399
    :goto_0
    return v4

    .line 379
    :cond_1
    if-ltz p5, :cond_2

    if-gt p6, p2, :cond_2

    if-ltz p7, :cond_2

    if-le p8, p3, :cond_3

    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 381
    :cond_3
    sub-int v1, p6, p5

    .line 382
    .local v1, "dstW":I
    sub-int v0, p8, p7

    .line 383
    .local v0, "dstH":I
    mul-int v4, p2, p3

    mul-int/2addr v4, p4

    array-length v5, p0

    if-le v4, v5, :cond_4

    const/4 v4, 0x0

    goto :goto_0

    .line 384
    :cond_4
    mul-int v4, v1, v0

    mul-int/2addr v4, p4

    array-length v5, p1

    if-le v4, v5, :cond_5

    const/4 v4, 0x0

    goto :goto_0

    .line 387
    :cond_5
    move v3, p7

    .local v3, "sy":I
    const/4 v2, 0x0

    .local v2, "dy":I
    :goto_1
    if-lt v3, p8, :cond_6

    .line 399
    const/4 v4, 0x1

    goto :goto_0

    .line 390
    :cond_6
    mul-int v4, p2, v3

    add-int/2addr v4, p5

    mul-int/2addr v4, p4

    .line 391
    mul-int v5, v1, v2

    mul-int/2addr v5, p4

    .line 392
    mul-int v6, v1, p4

    .line 389
    invoke-static {p0, v4, p1, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 387
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static getTextureSizeFromImageSize(I)I
    .locals 2
    .param p0, "imageSize"    # I

    .prologue
    .line 307
    invoke-static {p0}, Lcom/sec/android/gallery3d/common/Utils;->nextPowerOf2(I)I

    move-result v0

    .line 308
    .local v0, "nextPowerOf2":I
    if-ne v0, p0, :cond_0

    .line 314
    .end local p0    # "imageSize":I
    :goto_0
    return p0

    .line 311
    .restart local p0    # "imageSize":I
    :cond_0
    rem-int/lit8 v1, p0, 0x2

    if-nez v1, :cond_1

    .line 312
    add-int/lit8 p0, p0, 0x2

    goto :goto_0

    .line 314
    :cond_1
    add-int/lit8 p0, p0, 0x3

    goto :goto_0
.end method

.method public static isAlmostEquals(FF)Z
    .locals 2
    .param p0, "a"    # F
    .param p1, "b"    # F

    .prologue
    .line 73
    sub-float v0, p0, p1

    .line 74
    .local v0, "diff":F
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    neg-float v0, v0

    .end local v0    # "diff":F
    :cond_0
    const v1, 0x3ca3d70a    # 0.02f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLocalImage(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 161
    instance-of v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_0

    .line 162
    const/4 v0, 0x1

    .line 164
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLocalImageJob(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 152
    .local p0, "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<Landroid/graphics/Bitmap;>;"
    instance-of v0, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;

    if-eqz v0, :cond_0

    .line 153
    const/4 v0, 0x1

    .line 155
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMoreAlmostEquals(FF)Z
    .locals 2
    .param p0, "a"    # F
    .param p1, "b"    # F

    .prologue
    .line 78
    sub-float v0, p0, p1

    .line 79
    .local v0, "diff":F
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    neg-float v0, v0

    .end local v0    # "diff":F
    :cond_0
    const v1, 0x3b03126f    # 0.002f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSupportedRegionDecoding(Ljava/lang/String;)Z
    .locals 4
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 272
    if-eqz p0, :cond_1

    .line 273
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 274
    .local v0, "opts":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 275
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 276
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 278
    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 279
    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v3, "image/jpeg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v3, "png"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 285
    .end local v0    # "opts":Landroid/graphics/BitmapFactory$Options;
    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isVerticalCrop(Lcom/sec/android/gallery3d/data/Path;)Z
    .locals 4
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v2, 0x0

    .line 318
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->getObject()Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 319
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v0, :cond_0

    instance-of v3, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v3, :cond_0

    .line 320
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v1

    .line 321
    .local v1, "rotation":I
    const/16 v3, 0x5a

    if-eq v1, v3, :cond_1

    const/16 v3, 0x10e

    if-eq v1, v3, :cond_1

    .line 324
    .end local v1    # "rotation":I
    :cond_0
    :goto_0
    return v2

    .line 321
    .restart local v1    # "rotation":I
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static resizeBySideLength(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "maxLength"    # I
    .param p2, "recycle"    # Z

    .prologue
    .line 111
    if-nez p0, :cond_1

    const/4 p0, 0x0

    .line 118
    .end local p0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p0

    .line 113
    .restart local p0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 114
    .local v2, "srcWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 116
    .local v1, "srcHeight":I
    int-to-float v3, p1

    int-to-float v4, v2

    div-float/2addr v3, v4

    int-to-float v4, p1

    int-to-float v5, v1

    div-float/2addr v4, v5

    .line 115
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 117
    .local v0, "scale":F
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v0, v3

    if-gez v3, :cond_0

    .line 118
    invoke-static {p0, v0, p2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeBitmapByScale(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method public static resizeDownAndCropCenter(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "recycle"    # Z

    .prologue
    .line 329
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    .line 330
    .local v8, "w":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 332
    .local v1, "h":I
    iget v10, p1, Landroid/graphics/Rect;->right:I

    iget v11, p1, Landroid/graphics/Rect;->left:I

    sub-int v5, v10, v11

    .line 333
    .local v5, "rectW":I
    iget v10, p1, Landroid/graphics/Rect;->bottom:I

    iget v11, p1, Landroid/graphics/Rect;->top:I

    sub-int v4, v10, v11

    .line 335
    .local v4, "rectH":I
    int-to-float v10, v5

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    int-to-float v11, v11

    div-float/2addr v10, v11

    .line 336
    int-to-float v11, v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v11, v12

    .line 335
    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 337
    .local v6, "scale":F
    invoke-static {p0}, Lcom/quramsoft/xiv/XIVUtils;->getConfig(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap$Config;

    move-result-object v10

    invoke-static {v5, v4, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 338
    .local v7, "target":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    int-to-float v10, v10

    mul-float/2addr v10, v6

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v9

    .line 339
    .local v9, "width":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v10, v10

    mul-float/2addr v10, v6

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 340
    .local v2, "height":I
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 341
    .local v0, "canvas":Landroid/graphics/Canvas;
    sub-int v10, v5, v9

    int-to-float v10, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    sub-int v11, v4, v2

    int-to-float v11, v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    invoke-virtual {v0, v10, v11}, Landroid/graphics/Canvas;->translate(FF)V

    .line 342
    invoke-virtual {v0, v6, v6}, Landroid/graphics/Canvas;->scale(FF)V

    .line 343
    new-instance v3, Landroid/graphics/Paint;

    const/4 v10, 0x6

    invoke-direct {v3, v10}, Landroid/graphics/Paint;-><init>(I)V

    .line 344
    .local v3, "paint":Landroid/graphics/Paint;
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v0, p0, v10, v11, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 345
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 346
    :cond_0
    return-object v7
.end method

.method public static resizeDownForWSTN(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "maxW"    # I
    .param p2, "maxH"    # I
    .param p3, "recycle"    # Z

    .prologue
    .line 138
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    const/4 p0, 0x0

    .line 147
    .end local p0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    :goto_0
    return-object p0

    .line 140
    .restart local p0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 141
    .local v2, "srcWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 143
    .local v1, "srcHeight":I
    if-le v2, p1, :cond_1

    if-le v1, p2, :cond_1

    .line 146
    int-to-float v3, p1

    int-to-float v4, v2

    div-float/2addr v3, v4

    int-to-float v4, p2

    int-to-float v5, v1

    div-float/2addr v4, v5

    .line 145
    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 147
    .local v0, "scale":F
    invoke-static {p0, v0, p3}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeBitmapByScale(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method public static resizeDownForWSTN(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "maxLength"    # I
    .param p2, "recycle"    # Z

    .prologue
    .line 124
    if-nez p0, :cond_1

    const/4 p0, 0x0

    .line 132
    .end local p0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p0

    .line 126
    .restart local p0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 127
    .local v3, "srcWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 128
    .local v2, "srcHeight":I
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 129
    .local v0, "minSide":I
    if-le v0, p1, :cond_0

    .line 131
    int-to-float v4, p1

    int-to-float v5, v3

    div-float/2addr v4, v5

    int-to-float v5, p1

    int-to-float v6, v2

    div-float/2addr v5, v6

    .line 130
    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 132
    .local v1, "scale":F
    invoke-static {p0, v1, p2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeBitmapByScale(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method public static waitForAnimationOnPhotoView(Lcom/quramsoft/xiv/XIV;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;IJ)Z
    .locals 3
    .param p0, "xiv"    # Lcom/quramsoft/xiv/XIV;
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "delayTime"    # I
    .param p3, "sleepTime"    # J

    .prologue
    const/4 v0, 0x0

    .line 185
    :goto_0
    invoke-virtual {p0, p2}, Lcom/quramsoft/xiv/XIV;->needToWaitForAnimation(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 196
    const/4 v0, 0x1

    :cond_0
    return v0

    .line 187
    :cond_1
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 188
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->onPhotoView()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    :try_start_0
    invoke-static {p3, p4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 192
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static waitWithoutInterrupt(Ljava/lang/Object;)V
    .locals 4
    .param p0, "object"    # Ljava/lang/Object;

    .prologue
    .line 98
    monitor-enter p0

    .line 101
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    :goto_0
    :try_start_1
    monitor-exit p0

    .line 106
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "XIVUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "unexpected interrupt: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 98
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
