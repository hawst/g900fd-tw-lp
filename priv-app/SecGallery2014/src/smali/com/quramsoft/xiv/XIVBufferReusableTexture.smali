.class public Lcom/quramsoft/xiv/XIVBufferReusableTexture;
.super Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;
.source "XIVBufferReusableTexture.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "XIVBufferReusableTexture"


# instance fields
.field protected mContentData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

.field private mFastReuse:Z

.field private mManager:Lcom/quramsoft/xiv/XIVBufferReuseManager;

.field private mType:I


# direct methods
.method public constructor <init>(Lcom/quramsoft/xiv/XIVBufferReuseManager;Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;IZ)V
    .locals 6
    .param p1, "manager"    # Lcom/quramsoft/xiv/XIVBufferReuseManager;
    .param p2, "data"    # Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    .param p3, "type"    # I
    .param p4, "fastReuse"    # Z

    .prologue
    .line 40
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/quramsoft/xiv/XIVBufferReusableTexture;-><init>(Lcom/quramsoft/xiv/XIVBufferReuseManager;Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;IZZ)V

    .line 41
    return-void
.end method

.method protected constructor <init>(Lcom/quramsoft/xiv/XIVBufferReuseManager;Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;IZZ)V
    .locals 0
    .param p1, "manager"    # Lcom/quramsoft/xiv/XIVBufferReuseManager;
    .param p2, "data"    # Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    .param p3, "type"    # I
    .param p4, "fastReuse"    # Z
    .param p5, "hasBorder"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p5}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;-><init>(Z)V

    .line 48
    iput-object p1, p0, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->mManager:Lcom/quramsoft/xiv/XIVBufferReuseManager;

    .line 49
    iput-object p2, p0, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->mContentData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    .line 50
    iput p3, p0, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->mType:I

    .line 51
    iput-boolean p4, p0, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->mFastReuse:Z

    .line 52
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->setUseBuffer()V

    .line 53
    return-void
.end method


# virtual methods
.method protected onFreeBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 76
    return-void
.end method

.method protected onFreeBuffer(Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;)V
    .locals 3
    .param p1, "data"    # Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->mFastReuse:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 63
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->mManager:Lcom/quramsoft/xiv/XIVBufferReuseManager;

    iget v1, p0, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->mType:I

    iget-object v2, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->freeBuffer(ILjava/nio/ByteBuffer;)V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->mContentData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    .line 66
    :cond_0
    return-void
.end method

.method protected onGetBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onGetBuffer()Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->mContentData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    return-object v0
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->recycle()V

    .line 81
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->mContentData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    invoke-virtual {p0, v0}, Lcom/quramsoft/xiv/XIVBufferReusableTexture;->onFreeBuffer(Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;)V

    .line 82
    return-void
.end method
