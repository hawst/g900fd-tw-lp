.class Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;
.super Ljava/lang/Object;
.source "XIVLargeThumbnailManager.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LargeThumbnailDecodeThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;


# direct methods
.method constructor <init>(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V
    .locals 1
    .param p1, "largeThumbnail"    # Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .prologue
    .line 499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 498
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .line 501
    iput-object p1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .line 502
    return-void
.end method

.method private delay(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z
    .locals 6
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 528
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .line 529
    .local v0, "LTN":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_0

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move v1, v2

    .line 544
    :cond_1
    :goto_0
    return v1

    .line 530
    :cond_2
    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$0(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/xiv/XIV;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quramsoft/xiv/XIV;->getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v3

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->mLargeThumbnail:[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->access$0(Lcom/quramsoft/xiv/XIVLargeThumbnailManager;)[Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-result-object v3

    aget-object v3, v3, v2

    if-eq v0, v3, :cond_1

    .line 532
    :goto_1
    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodedTileCount:I
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$2(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v3

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mNextUploadingTileCount:I
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$3(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v4

    sub-int/2addr v3, v4

    const/16 v4, 0xa

    if-le v3, v4, :cond_1

    .line 534
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_3

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    move v1, v2

    goto :goto_0

    .line 539
    :cond_4
    const-wide/16 v4, 0x5

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 540
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method private suspend(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z
    .locals 4
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 507
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;
    invoke-static {v1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$0(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/xiv/XIV;

    move-result-object v0

    .line 510
    .local v0, "xiv":Lcom/quramsoft/xiv/XIV;
    :goto_0
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z
    invoke-static {v1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 518
    :goto_1
    return v1

    .line 512
    :cond_1
    iget-object v1, v0, Lcom/quramsoft/xiv/XIV;->mSlidingSpeedManager:Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getSlidingDurationLimit()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->isDurationUnderLimit(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 514
    const-wide/16 v2, 0x5

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 515
    :catch_0
    move-exception v1

    goto :goto_0

    .line 518
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 36
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 549
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    .line 556
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    .line 798
    :goto_0
    return-object v3

    .line 557
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$0(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/xiv/XIV;

    move-result-object v3

    if-nez v3, :cond_2

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 559
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$0(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/xiv/XIV;

    move-result-object v3

    iget-object v0, v3, Lcom/quramsoft/xiv/XIV;->mBufferReuseManager:Lcom/quramsoft/xiv/XIVBufferReuseManager;

    move-object/from16 v24, v0

    .line 560
    .local v24, "bufferReuseManager":Lcom/quramsoft/xiv/XIVBufferReuseManager;
    if-nez v24, :cond_3

    const/4 v3, 0x0

    goto :goto_0

    .line 562
    :cond_3
    invoke-direct/range {p0 .. p1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->suspend(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x0

    goto :goto_0

    .line 564
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mParsingCompleted:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$4(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 565
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->prepareToDecode()V
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$5(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V

    .line 567
    :cond_5
    invoke-direct/range {p0 .. p1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->suspend(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x0

    goto :goto_0

    .line 569
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$6(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 571
    const/4 v14, 0x1

    .line 572
    .local v14, "iterOpt":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mSampleSize:I
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$7(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v4

    .line 576
    .local v4, "sampleSize":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageWidth:I
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$8(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v3

    div-int v6, v3, v4

    .line 577
    .local v6, "sampledWidth":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mImageHeight:I
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$9(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v3

    div-int v7, v3, v4

    .line 579
    .local v7, "sampledHeight":I
    if-eqz v6, :cond_7

    if-nez v7, :cond_8

    :cond_7
    const/4 v3, 0x0

    goto :goto_0

    .line 581
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    new-instance v8, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v8}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    invoke-static {v3, v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$10(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V

    .line 582
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v3

    iput v4, v3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 583
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v3

    const/4 v8, 0x7

    iput v8, v3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 593
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUsePNGLargeThumbnail:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$12(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 594
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFilePath:Ljava/lang/String;
    invoke-static {v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$13(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodePNGIterInit(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/lang/String;)I

    move-result v27

    .line 595
    .local v27, "ret":I
    const/4 v3, 0x1

    move/from16 v0, v27

    if-eq v0, v3, :cond_f

    const/4 v3, 0x0

    goto/16 :goto_0

    .line 598
    .end local v27    # "ret":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$13(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Ljava/lang/String;

    move-result-object v3

    .line 599
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v8

    .line 597
    invoke-static {v3, v8}, Lcom/quramsoft/qrb/QuramBitmapFactory;->createDecInfo(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v27

    .line 601
    .restart local v27    # "ret":I
    if-nez v27, :cond_a

    const/4 v3, 0x0

    goto/16 :goto_0

    .line 603
    :cond_a
    or-int/lit8 v14, v14, 0x2

    .line 605
    :cond_b
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 613
    :cond_c
    :goto_1
    const/4 v3, 0x1

    move/from16 v0, v27

    if-eq v0, v3, :cond_f

    .line 614
    const/4 v3, 0x4

    move/from16 v0, v27

    if-ne v0, v3, :cond_d

    .line 615
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v3

    invoke-static {v3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->abortFileIter(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V

    .line 617
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    const/4 v8, 0x0

    invoke-static {v3, v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$10(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V

    .line 618
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 606
    :cond_e
    invoke-direct/range {p0 .. p1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->suspend(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 609
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$13(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v8

    .line 608
    invoke-static {v3, v8, v6, v7}, Lcom/quramsoft/qrb/QuramBitmapFactory;->prepareDecodeFileIter(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;II)I

    move-result v27

    .line 611
    const/4 v3, 0x4

    move/from16 v0, v27

    if-eq v0, v3, :cond_b

    goto :goto_1

    .line 626
    :cond_f
    sget v32, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_W_STEP:I

    .line 628
    .local v32, "tileWStep":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    add-int/lit8 v9, v7, -0x1

    div-int/lit16 v9, v9, 0x1fe

    add-int/lit8 v9, v9, 0x1

    invoke-static {v8, v9}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$14(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    invoke-static {v3, v9}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$15(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 629
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    add-int/lit8 v9, v6, -0x1

    sget v10, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_W_STEP:I

    div-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x1

    invoke-static {v8, v9}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$16(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    invoke-static {v3, v9}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$17(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 631
    const/4 v5, 0x0

    .line 632
    .local v5, "iterRowBuffer":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$18(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v3

    const/4 v8, 0x1

    if-le v3, v8, :cond_10

    .line 633
    const/4 v3, 0x5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->getBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 636
    :cond_10
    const/16 v27, 0x4

    .line 637
    const/16 v34, 0x0

    .local v34, "ty":I
    :goto_2
    move/from16 v0, v34

    if-lt v0, v7, :cond_19

    .line 717
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$18(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v3

    const/4 v8, 0x1

    if-le v3, v8, :cond_12

    .line 718
    const/4 v3, 0x5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v5}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->freeBuffer(ILjava/nio/ByteBuffer;)V

    .line 721
    :cond_12
    const/4 v3, 0x1

    move/from16 v0, v27

    if-ne v0, v3, :cond_27

    .line 722
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    const/4 v8, 0x1

    invoke-static {v3, v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$21(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 727
    sget-boolean v3, Lcom/quramsoft/xiv/XIVConfig;->XIV_MTN_TO_LTN_ALPHA_ANIM:Z

    if-eqz v3, :cond_13

    .line 728
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .line 729
    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x43480000    # 200.0f

    invoke-static {v8, v9, v10}, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->create(FFF)Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    move-result-object v8

    .line 728
    iput-object v8, v3, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mAlphablendingAnimation:Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    .line 734
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUsePNGLargeThumbnail:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$12(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-nez v3, :cond_25

    .line 735
    const/4 v3, 0x3

    if-ne v14, v3, :cond_25

    .line 738
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$13(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Ljava/lang/String;

    move-result-object v3

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v9}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v9

    .line 737
    invoke-static {v3, v8, v9}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->newInstance(Ljava/lang/String;ZLcom/quramsoft/qrb/QuramBitmapFactory$Options;)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    move-result-object v35

    .line 739
    .local v35, "xrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    if-eqz v35, :cond_14

    .line 740
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .line 741
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;
    invoke-static {v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$0(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/xiv/XIV;

    move-result-object v8

    invoke-virtual {v8}, Lcom/quramsoft/xiv/XIV;->getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-result-object v8

    move-object/from16 v0, v35

    invoke-static {v0, v8}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->newInstance(Ljava/lang/Object;Lcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v8

    .line 740
    invoke-static {v3, v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$22(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    .line 743
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$23(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v3

    if-nez v3, :cond_15

    .line 744
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v3

    invoke-static {v3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->abortFileIter(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V

    .line 768
    .end local v4    # "sampleSize":I
    .end local v5    # "iterRowBuffer":Ljava/nio/ByteBuffer;
    .end local v6    # "sampledWidth":I
    .end local v7    # "sampledHeight":I
    .end local v14    # "iterOpt":I
    .end local v27    # "ret":I
    .end local v32    # "tileWStep":I
    .end local v34    # "ty":I
    .end local v35    # "xrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    :cond_15
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-nez v3, :cond_16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$23(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v3

    if-nez v3, :cond_16

    .line 769
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$13(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_16

    .line 770
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseLargeThumbnail:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$6(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 771
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUsePNGLargeThumbnail:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$12(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-nez v3, :cond_16

    .line 773
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$13(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Ljava/lang/String;

    move-result-object v3

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 772
    invoke-static {v3, v8, v9}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->newInstance(Ljava/lang/String;ZZ)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    move-result-object v35

    .line 774
    .restart local v35    # "xrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    if-eqz v35, :cond_16

    .line 775
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .line 776
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;
    invoke-static {v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$0(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/xiv/XIV;

    move-result-object v8

    invoke-virtual {v8}, Lcom/quramsoft/xiv/XIV;->getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-result-object v8

    move-object/from16 v0, v35

    invoke-static {v0, v8}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->newInstance(Ljava/lang/Object;Lcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v8

    .line 775
    invoke-static {v3, v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$22(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    .line 781
    .end local v35    # "xrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    :cond_16
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIsRecycled:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$1(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-eqz v3, :cond_2a

    .line 782
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$23(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v3

    if-eqz v3, :cond_18

    .line 783
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$23(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->recycle()V

    .line 798
    :cond_18
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$23(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v3

    goto/16 :goto_0

    .line 639
    .restart local v4    # "sampleSize":I
    .restart local v5    # "iterRowBuffer":Ljava/nio/ByteBuffer;
    .restart local v6    # "sampledWidth":I
    .restart local v7    # "sampledHeight":I
    .restart local v14    # "iterOpt":I
    .restart local v27    # "ret":I
    .restart local v32    # "tileWStep":I
    .restart local v34    # "ty":I
    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUsePNGLargeThumbnail:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$12(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-nez v3, :cond_1a

    .line 640
    const/4 v3, 0x4

    move/from16 v0, v27

    if-ne v0, v3, :cond_11

    .line 643
    :cond_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$18(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_1b

    .line 644
    const/4 v3, 0x3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->getBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 647
    :cond_1b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUsePNGLargeThumbnail:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$12(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 649
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v3

    .line 650
    const/16 v8, 0x1fe

    .line 648
    invoke-static/range {v3 .. v8}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodePNGIterToBuffer(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;ILjava/nio/Buffer;III)I

    move-result v27

    .line 659
    :goto_5
    if-eqz v27, :cond_11

    .line 661
    move/from16 v0, v34

    add-int/lit16 v3, v0, 0x1fe

    if-lt v3, v7, :cond_1d

    sub-int v3, v7, v34

    add-int/lit8 v23, v3, 0x2

    .line 663
    .local v23, "texHeight":I
    :goto_6
    const/16 v33, 0x0

    .local v33, "tx":I
    :goto_7
    move/from16 v0, v33

    if-lt v0, v6, :cond_1e

    .line 637
    :goto_8
    move/from16 v0, v34

    add-int/lit16 v0, v0, 0x1fe

    move/from16 v34, v0

    goto/16 :goto_2

    .line 654
    .end local v23    # "texHeight":I
    .end local v33    # "tx":I
    :cond_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$13(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v9

    .line 655
    const/16 v11, 0x200

    .line 656
    const/16 v15, 0x1fe

    move-object v10, v5

    move v12, v6

    move v13, v7

    .line 653
    invoke-static/range {v8 .. v15}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFileIter(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/nio/Buffer;IIIII)I

    move-result v27

    goto :goto_5

    .line 661
    :cond_1d
    const/16 v23, 0x200

    goto :goto_6

    .line 664
    .restart local v23    # "texHeight":I
    .restart local v33    # "tx":I
    :cond_1e
    invoke-direct/range {p0 .. p1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->delay(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 665
    const/16 v27, 0x4

    .line 666
    goto :goto_8

    .line 671
    :cond_1f
    if-nez v33, :cond_21

    .line 672
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$18(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_20

    .line 673
    move/from16 v29, v6

    .line 676
    .local v29, "texWidth":I
    :goto_9
    const/16 v20, 0x0

    .line 686
    .local v20, "subx":I
    :goto_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mMaxColumn:I
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$18(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v3

    const/4 v8, 0x1

    if-le v3, v8, :cond_23

    .line 687
    const/4 v3, 0x3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->getBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v31

    .line 689
    .local v31, "tileBuffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v15

    invoke-virtual/range {v31 .. v31}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v16

    .line 690
    const/16 v18, 0x200

    const/16 v19, 0x4

    .line 691
    add-int v21, v20, v29

    const/16 v22, 0x0

    move/from16 v17, v6

    .line 688
    invoke-static/range {v15 .. v23}, Lcom/quramsoft/xiv/XIVUtils;->getSubImageBuffer([B[BIIIIIII)Z

    move-result v26

    .line 692
    .local v26, "r":Z
    if-nez v26, :cond_24

    .line 693
    const/16 v27, 0x4

    .line 694
    goto :goto_8

    .line 675
    .end local v20    # "subx":I
    .end local v26    # "r":Z
    .end local v29    # "texWidth":I
    .end local v31    # "tileBuffer":Ljava/nio/ByteBuffer;
    :cond_20
    add-int/lit8 v29, v32, 0x1

    .restart local v29    # "texWidth":I
    goto :goto_9

    .line 677
    .end local v29    # "texWidth":I
    :cond_21
    add-int v3, v33, v32

    if-lt v3, v6, :cond_22

    .line 678
    sub-int v3, v6, v33

    add-int/lit8 v29, v3, 0x1

    .line 679
    .restart local v29    # "texWidth":I
    add-int/lit8 v20, v33, -0x1

    .restart local v20    # "subx":I
    goto :goto_a

    .line 681
    .end local v20    # "subx":I
    .end local v29    # "texWidth":I
    :cond_22
    add-int/lit8 v29, v32, 0x2

    .line 682
    .restart local v29    # "texWidth":I
    add-int/lit8 v20, v33, -0x1

    .restart local v20    # "subx":I
    goto :goto_a

    .line 697
    :cond_23
    move-object/from16 v31, v5

    .line 700
    .restart local v31    # "tileBuffer":Ljava/nio/ByteBuffer;
    :cond_24
    new-instance v28, Lcom/quramsoft/xiv/XIVBufferReusableTexture;

    .line 702
    new-instance v3, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    move/from16 v0, v29

    move/from16 v1, v23

    move-object/from16 v2, v31

    invoke-direct {v3, v0, v1, v2}, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;-><init>(IILjava/nio/ByteBuffer;)V

    .line 703
    const/4 v8, 0x3

    const/4 v9, 0x1

    .line 700
    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v3, v8, v9}, Lcom/quramsoft/xiv/XIVBufferReusableTexture;-><init>(Lcom/quramsoft/xiv/XIVBufferReuseManager;Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;IZ)V

    .line 704
    .local v28, "tex":Lcom/quramsoft/xiv/XIVBufferReusableTexture;
    invoke-static/range {v28 .. v28}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;->createLargeThumbnail(Lcom/quramsoft/xiv/XIVBufferReusableTexture;)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;

    move-result-object v30

    .line 705
    .local v30, "tile":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    const/4 v3, 0x1

    move-object/from16 v0, v30

    iput v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;->state:I

    .line 707
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLargeThumbnailTileList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$19(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Ljava/util/ArrayList;

    move-result-object v8

    monitor-enter v8

    .line 708
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mLargeThumbnailTileList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$19(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 707
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 710
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodedTileCount:I
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$2(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-static {v3, v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$20(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;I)V

    .line 663
    add-int v33, v33, v32

    goto/16 :goto_7

    .line 707
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 747
    .end local v20    # "subx":I
    .end local v23    # "texHeight":I
    .end local v28    # "tex":Lcom/quramsoft/xiv/XIVBufferReusableTexture;
    .end local v29    # "texWidth":I
    .end local v30    # "tile":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    .end local v31    # "tileBuffer":Ljava/nio/ByteBuffer;
    .end local v33    # "tx":I
    :cond_25
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUsePNGLargeThumbnail:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$12(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-eqz v3, :cond_26

    .line 748
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v3

    invoke-static {v3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->abortPNGIter(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    .line 750
    :cond_26
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    const/4 v8, 0x0

    invoke-static {v3, v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$10(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V

    goto/16 :goto_3

    .line 753
    :cond_27
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUsePNGLargeThumbnail:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$12(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-nez v3, :cond_28

    .line 754
    const/4 v3, 0x4

    move/from16 v0, v27

    if-ne v0, v3, :cond_28

    .line 756
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v3

    invoke-static {v3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->abortFileIter(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V

    .line 758
    :cond_28
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUsePNGLargeThumbnail:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$12(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-eqz v3, :cond_29

    .line 759
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mDecodeOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$11(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v3

    invoke-static {v3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->abortPNGIter(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    .line 761
    :cond_29
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    const/4 v8, 0x0

    invoke-static {v3, v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$10(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V

    .line 762
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    const/4 v8, 0x0

    invoke-static {v3, v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$24(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    .line 763
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # invokes: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->freeTiles()V
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$25(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)V

    .line 764
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 786
    .end local v4    # "sampleSize":I
    .end local v5    # "iterRowBuffer":Ljava/nio/ByteBuffer;
    .end local v6    # "sampledWidth":I
    .end local v7    # "sampledHeight":I
    .end local v14    # "iterOpt":I
    .end local v27    # "ret":I
    .end local v32    # "tileWStep":I
    .end local v34    # "ty":I
    :cond_2a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mXiv:Lcom/quramsoft/xiv/XIV;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$0(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/quramsoft/xiv/XIV;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quramsoft/xiv/XIV;->getPhotoDataAdapter()Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-result-object v25

    .line 787
    .local v25, "pda":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    if-eqz v25, :cond_18

    .line 788
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mUseFastLargeThumbnail:Z
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$26(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 789
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .line 790
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIndex:I
    invoke-static {v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$27(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-static {v9}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$23(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v9

    move-object/from16 v0, v25

    invoke-virtual {v0, v8, v9}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->excuteFullImageTask(ILcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)Z

    move-result v8

    .line 789
    invoke-static {v3, v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$28(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    goto/16 :goto_4

    .line 792
    :cond_2b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    .line 793
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mIndex:I
    invoke-static {v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$27(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-static {v9}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$23(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v9

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v8, v9}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->requestFullImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;ILcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v8

    .line 792
    invoke-static {v3, v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$22(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    .line 794
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->mLargeThumbnail:Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;

    # getter for: Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-static {v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$23(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v3

    if-eqz v3, :cond_2c

    const/4 v3, 0x1

    :goto_b
    invoke-static {v8, v3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;->access$28(Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;Z)V

    goto/16 :goto_4

    :cond_2c
    const/4 v3, 0x0

    goto :goto_b
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailDecodeThread;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    return-object v0
.end method
