.class public Lcom/quramsoft/xiv/XIVCoder;
.super Ljava/lang/Object;
.source "XIVCoder.java"


# static fields
.field public static final QURAM_WINK_FAIL:I = 0x0

.field public static final QURAM_WINK_SUCCESS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "XIVCoder"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native DecodeImage(Landroid/graphics/Bitmap;[BI)I
.end method

.method public static native DecodeImageFromFile(Ljava/lang/String;Landroid/graphics/Bitmap;)I
.end method

.method public static native DecodeImageFromFileLite(Ljava/lang/String;Landroid/graphics/Bitmap;)I
.end method

.method public static native DecodeImageOffset(Landroid/graphics/Bitmap;I[BI)I
.end method

.method public static native DecodeJPEGFromFile(Ljava/lang/String;Landroid/graphics/Bitmap;III)I
.end method

.method public static native DecodeJPEGFromFileAndGetThumnail(Ljava/lang/String;Landroid/graphics/Bitmap;)I
.end method

.method public static native DecodeJPEGFromMemory([BIILandroid/graphics/Bitmap;III)I
.end method

.method public static native DecodeWSTNToBitmap(Landroid/graphics/Bitmap;I[BIZ)I
.end method

.method public static native EncodeBitmapToWSTN(Landroid/graphics/Bitmap;[B)I
.end method

.method public static native EncodeImage(Landroid/graphics/Bitmap;[B)I
.end method

.method public static native EncodeImageLite(Landroid/graphics/Bitmap;[B)I
.end method

.method public static native IsNeonDCTEnable()I
.end method

.method public static native IsNeonEnable()I
.end method

.method public static native IsNeonEncEnable()I
.end method

.method public static native IsNormalDualcoreEnable()I
.end method

.method public static native IsRegionDualcoreEnable()I
.end method

.method public static native IsStandardJPEG(Ljava/lang/String;)I
.end method

.method public static native MakeBufferFromBitmap(Landroid/graphics/Bitmap;Ljava/nio/Buffer;)I
.end method

.method public static native ParseImage([B[I[I)I
.end method

.method public static native ParseImageFromFile(Ljava/lang/String;[II)I
.end method

.method public static native ParseImageFromMemory([BII[II)I
.end method

.method public static native ParseImageOffset([BI[I[I)I
.end method

.method public static native ParseVCodecFromFile(Ljava/lang/String;[I)I
.end method

.method public static native ParseWSTN([BI[I[I)I
.end method

.method public static native getSubImageBuffer(Ljava/nio/Buffer;IIILjava/nio/Buffer;IIII)I
.end method
