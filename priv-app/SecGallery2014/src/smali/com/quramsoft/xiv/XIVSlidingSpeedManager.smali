.class public Lcom/quramsoft/xiv/XIVSlidingSpeedManager;
.super Ljava/lang/Object;
.source "XIVSlidingSpeedManager.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final MAX_FLING_TIME_QUEUE_SIZE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "XIVSlidingSpeedManager"


# instance fields
.field private mDuration:F

.field private final mFlingTimeQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mXiv:Lcom/quramsoft/xiv/XIV;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mFlingTimeQueue:Ljava/util/LinkedList;

    .line 21
    const/high16 v0, 0x43fa0000    # 500.0f

    iput v0, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mDuration:F

    .line 32
    return-void
.end method

.method static final create(Lcom/quramsoft/xiv/XIV;)Lcom/quramsoft/xiv/XIVSlidingSpeedManager;
    .locals 1
    .param p0, "xiv"    # Lcom/quramsoft/xiv/XIV;

    .prologue
    .line 25
    new-instance v0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;-><init>()V

    .line 26
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVSlidingSpeedManager;
    iput-object p0, v0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    .line 27
    return-object v0
.end method


# virtual methods
.method public checkSliding()V
    .locals 14

    .prologue
    .line 37
    iget-object v9, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mFlingTimeQueue:Ljava/util/LinkedList;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 39
    sget v9, Lcom/quramsoft/xiv/XIVDefinedValues;->mMaxSlidingAnimationDuration:I

    int-to-float v4, v9

    .line 40
    .local v4, "maxDuration":F
    iput v4, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mDuration:F

    .line 42
    iget-object v10, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mFlingTimeQueue:Ljava/util/LinkedList;

    monitor-enter v10

    .line 43
    :try_start_0
    iget-object v9, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mFlingTimeQueue:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v7

    .line 44
    .local v7, "size":I
    const/4 v9, 0x2

    if-lt v7, v9, :cond_0

    .line 45
    iget-object v9, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mFlingTimeQueue:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 46
    .local v0, "firstT":J
    iget-object v9, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mFlingTimeQueue:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 47
    .local v2, "lastT":J
    sub-long v12, v2, v0

    long-to-float v9, v12

    iput v9, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mDuration:F

    .line 49
    iget-object v9, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mFlingTimeQueue:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 52
    iget v9, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mDuration:F

    cmpl-float v9, v9, v4

    if-ltz v9, :cond_0

    .line 53
    iput v4, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mDuration:F

    .line 54
    iget-object v9, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mFlingTimeQueue:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->clear()V

    .line 58
    .end local v0    # "firstT":J
    .end local v2    # "lastT":J
    :cond_0
    iget v9, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mDuration:F

    cmpl-float v9, v9, v4

    if-nez v9, :cond_2

    .line 59
    iget-object v9, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v9}, Lcom/quramsoft/xiv/XIV;->getAnimationManager()Lcom/quramsoft/xiv/XIVAnimationManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/quramsoft/xiv/XIVAnimationManager;->getCurrentVX()F

    move-result v8

    .line 60
    .local v8, "vx":F
    sget v9, Lcom/quramsoft/xiv/XIVDefinedValues;->mMinSlidingAnimationDuration:I

    int-to-float v5, v9

    .line 62
    .local v5, "minDuration":F
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const v11, 0x469c4000    # 20000.0f

    div-float v6, v9, v11

    .line 63
    .local v6, "p":F
    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v9, v6, v9

    if-lez v9, :cond_1

    const/high16 v6, 0x3f800000    # 1.0f

    .line 65
    :cond_1
    sub-float v9, v5, v4

    mul-float/2addr v9, v6

    add-float/2addr v9, v4

    iput v9, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mDuration:F

    .line 42
    .end local v5    # "minDuration":F
    .end local v6    # "p":F
    .end local v8    # "vx":F
    :cond_2
    monitor-exit v10

    .line 72
    return-void

    .line 42
    .end local v7    # "size":I
    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9
.end method

.method public getDuration(FF)F
    .locals 1
    .param p1, "vx"    # F
    .param p2, "originalTime"    # F

    .prologue
    .line 77
    iget v0, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mDuration:F

    return v0
.end method

.method public isDurationUnderLimit(J)Z
    .locals 9
    .param p1, "limit"    # J

    .prologue
    .line 85
    iget-object v5, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mFlingTimeQueue:Ljava/util/LinkedList;

    monitor-enter v5

    .line 86
    :try_start_0
    iget-object v4, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mFlingTimeQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 87
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 88
    .local v0, "CurrentT":J
    iget-object v4, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mFlingTimeQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 90
    .local v2, "lastT":J
    iget v4, p0, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->mDuration:F

    long-to-float v6, p1

    cmpg-float v4, v4, v6

    if-gez v4, :cond_0

    sub-long v6, v0, v2

    cmp-long v4, v6, p1

    if-gez v4, :cond_0

    .line 94
    monitor-exit v5

    const/4 v4, 0x1

    .line 102
    .end local v0    # "CurrentT":J
    .end local v2    # "lastT":J
    :goto_0
    return v4

    .line 85
    :cond_0
    monitor-exit v5

    .line 102
    const/4 v4, 0x0

    goto :goto_0

    .line 85
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method
