.class public Lcom/quramsoft/xiv/XIVAnimationManager;
.super Ljava/lang/Object;
.source "XIVAnimationManager.java"


# static fields
.field private static final ANIMATION_DEFAULT_SLIDE:I = 0x0

.field private static final ANIMATION_XIV1:I = 0x1

.field private static final ANIMATION_XIV2:I = 0x2

.field private static final ANIMATION_XIV3:I = 0x3

.field private static final ANIMATION_XIV4:I = 0x4

.field private static final ANIMATION_XIV5:I = 0x5

.field static final ANIM_KIND_SCALE:I = 0x1

.field static final ANIM_KIND_SCROLL:I = 0x0

.field static final ANIM_KIND_SLIDE:I = 0x3

.field static final ANIM_KIND_SNAPBACK:I = 0x2

.field static final ANIM_KIND_ZOOM:I = 0x4

.field private static final ANIM_TIME_FLYING:F = 300.0f

.field private static final ANIM_TIME_SCALE:F = 100.0f

.field private static final ANIM_TIME_SCROLL:F = 100.0f

.field private static final ANIM_TIME_SNAPBACK:F = 600.0f

.field private static final DEBUG:Z = false

.field private static final FLYING_ANIMATION_TYPE:I = 0x4

.field private static final FLYING_PROPORTIONAL_FACTOR:F = 10.0f

.field private static final SCALE_ANIMATION_TYPE:I = 0x4

.field private static final SCROLL_ANIMATION_TYPE:I = 0x5

.field private static final SLIDE_ANIMATION_TYPE:I = 0x4

.field private static final TAG:Ljava/lang/String; = "XIVAnimation"


# instance fields
.field private mVX:F

.field private mVY:F

.field private mXiv:Lcom/quramsoft/xiv/XIV;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput v0, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVX:F

    .line 28
    iput v0, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVY:F

    .line 25
    return-void
.end method

.method static final create(Lcom/quramsoft/xiv/XIV;)Lcom/quramsoft/xiv/XIVAnimationManager;
    .locals 1
    .param p0, "xiv"    # Lcom/quramsoft/xiv/XIV;

    .prologue
    .line 18
    new-instance v0, Lcom/quramsoft/xiv/XIVAnimationManager;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIVAnimationManager;-><init>()V

    .line 19
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVAnimationManager;
    iput-object p0, v0, Lcom/quramsoft/xiv/XIVAnimationManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    .line 20
    return-object v0
.end method


# virtual methods
.method public checkFlingVelocity(FF)V
    .locals 0
    .param p1, "vX"    # F
    .param p2, "vY"    # F

    .prologue
    .line 32
    iput p1, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVX:F

    .line 33
    iput p2, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVY:F

    .line 34
    return-void
.end method

.method public getCurrentVX()F
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVX:F

    return v0
.end method

.method public getFlyingAnimationTime()F
    .locals 1

    .prologue
    .line 211
    const/high16 v0, 0x43960000    # 300.0f

    return v0
.end method

.method public getFlyingDestinationX(FF)F
    .locals 5
    .param p1, "currentX"    # F
    .param p2, "scale"    # F

    .prologue
    const/high16 v4, 0x41200000    # 10.0f

    .line 44
    const/4 v1, 0x0

    .line 45
    .local v1, "v":F
    iget v2, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVX:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 46
    iget v2, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVX:F

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    neg-float v1, v2

    .line 51
    :goto_0
    div-float v2, v1, p2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    sub-float v0, p1, v2

    .line 52
    .local v0, "dest":F
    return v0

    .line 48
    .end local v0    # "dest":F
    :cond_0
    iget v2, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVX:F

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    goto :goto_0
.end method

.method public getFlyingDestinationY(FF)F
    .locals 5
    .param p1, "currentY"    # F
    .param p2, "scale"    # F

    .prologue
    const/high16 v4, 0x41200000    # 10.0f

    .line 57
    const/4 v1, 0x0

    .line 58
    .local v1, "v":F
    iget v2, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVY:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 59
    iget v2, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVY:F

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    neg-float v1, v2

    .line 63
    :goto_0
    div-float v2, v1, p2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    sub-float v0, p1, v2

    .line 64
    .local v0, "dest":F
    return v0

    .line 61
    .end local v0    # "dest":F
    :cond_0
    iget v2, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVY:F

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    goto :goto_0
.end method

.method public getProgress(FFFI)F
    .locals 14
    .param p1, "fromX"    # F
    .param p2, "toX"    # F
    .param p3, "progress"    # F
    .param p4, "animKind"    # I

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    .local v0, "animationType":I
    packed-switch p4, :pswitch_data_0

    .line 131
    :pswitch_0
    const/4 v3, 0x0

    .line 188
    :cond_0
    :goto_0
    return v3

    .line 119
    :pswitch_1
    const/4 v0, 0x5

    .line 134
    :goto_1
    const/high16 v7, 0x3f800000    # 1.0f

    sub-float v1, v7, p3

    .line 135
    .local v1, "f":F
    move/from16 v4, p3

    .line 136
    .local v4, "p":F
    const/4 v3, 0x0

    .line 138
    .local v3, "nextProgress":F
    if-nez v0, :cond_1

    .line 140
    const/high16 v7, 0x3f800000    # 1.0f

    add-float/2addr v7, p1

    mul-float v8, v1, v1

    mul-float/2addr v8, v1

    mul-float/2addr v8, v1

    mul-float/2addr v8, v1

    sub-float v3, v7, v8

    goto :goto_0

    .line 122
    .end local v1    # "f":F
    .end local v3    # "nextProgress":F
    .end local v4    # "p":F
    :pswitch_2
    const/4 v0, 0x4

    .line 123
    goto :goto_1

    .line 126
    :pswitch_3
    const/4 v0, 0x4

    .line 129
    goto :goto_1

    .line 142
    .restart local v1    # "f":F
    .restart local v3    # "nextProgress":F
    .restart local v4    # "p":F
    :cond_1
    const/4 v7, 0x1

    if-ne v0, v7, :cond_2

    .line 144
    mul-float v7, v1, v1

    mul-float/2addr v7, v1

    mul-float/2addr v7, p1

    .line 145
    const/high16 v8, 0x40400000    # 3.0f

    mul-float/2addr v8, v1

    mul-float/2addr v8, v1

    mul-float/2addr v8, v4

    mul-float/2addr v8, p1

    .line 144
    add-float/2addr v7, v8

    .line 146
    const/high16 v8, 0x40400000    # 3.0f

    mul-float/2addr v8, v1

    mul-float/2addr v8, v4

    mul-float/2addr v8, v4

    mul-float v8, v8, p2

    .line 144
    add-float/2addr v7, v8

    .line 147
    mul-float v8, v4, v4

    mul-float/2addr v8, v4

    mul-float v8, v8, p2

    .line 144
    add-float v3, v7, v8

    goto :goto_0

    .line 149
    :cond_2
    const/4 v7, 0x2

    if-ne v0, v7, :cond_4

    .line 151
    cmpg-float v7, p1, p2

    if-gez v7, :cond_3

    .line 152
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    const/high16 v7, 0x3f800000    # 1.0f

    add-float/2addr v7, v4

    float-to-double v10, v7

    const-wide v12, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v10, v12

    double-to-float v7, v10

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    add-double/2addr v10, v12

    mul-double/2addr v8, v10

    double-to-float v7, v8

    add-float v3, p1, v7

    goto :goto_0

    .line 154
    :cond_3
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    const/high16 v7, 0x3f800000    # 1.0f

    add-float/2addr v7, v4

    float-to-double v10, v7

    const-wide v12, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v10, v12

    double-to-float v7, v10

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    add-double/2addr v10, v12

    mul-double/2addr v8, v10

    double-to-float v7, v8

    sub-float v3, p1, v7

    goto :goto_0

    .line 156
    :cond_4
    const/4 v7, 0x3

    if-ne v0, v7, :cond_6

    .line 159
    const/4 v2, 0x0

    .line 161
    .local v2, "mV":F
    neg-float v7, v4

    mul-float/2addr v7, v4

    const/high16 v8, 0x3f400000    # 0.75f

    mul-float/2addr v7, v8

    const/high16 v8, 0x3fe00000    # 1.75f

    mul-float/2addr v8, v4

    add-float v4, v7, v8

    .line 162
    const/high16 v7, 0x3f800000    # 1.0f

    sub-float v1, v7, v4

    .line 164
    cmpl-float v7, p1, p2

    if-lez v7, :cond_5

    .line 165
    iget v7, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVX:F

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    neg-float v7, v7

    const/high16 v8, 0x41000000    # 8.0f

    mul-float/2addr v7, v8

    const v8, 0x459c4000    # 5000.0f

    div-float v2, v7, v8

    .line 169
    :goto_2
    const/high16 v7, 0x3fe00000    # 1.75f

    div-float v7, v2, v7

    const/high16 v8, 0x40400000    # 3.0f

    div-float/2addr v7, v8

    add-float v5, p1, v7

    .line 170
    .local v5, "x2":F
    move/from16 v6, p2

    .line 173
    .local v6, "x3":F
    mul-float v7, v1, v1

    mul-float/2addr v7, v1

    mul-float/2addr v7, p1

    .line 174
    const/high16 v8, 0x40400000    # 3.0f

    mul-float/2addr v8, v1

    mul-float/2addr v8, v1

    mul-float/2addr v8, v4

    mul-float/2addr v8, v5

    .line 173
    add-float/2addr v7, v8

    .line 175
    const/high16 v8, 0x40400000    # 3.0f

    mul-float/2addr v8, v1

    mul-float/2addr v8, v4

    mul-float/2addr v8, v4

    mul-float/2addr v8, v6

    .line 173
    add-float/2addr v7, v8

    .line 176
    mul-float v8, v4, v4

    mul-float/2addr v8, v4

    mul-float v8, v8, p2

    .line 173
    add-float v3, v7, v8

    .line 172
    goto/16 :goto_0

    .line 167
    .end local v5    # "x2":F
    .end local v6    # "x3":F
    :cond_5
    iget v7, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVX:F

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    const/high16 v8, 0x41000000    # 8.0f

    mul-float/2addr v7, v8

    const v8, 0x459c4000    # 5000.0f

    div-float v2, v7, v8

    goto :goto_2

    .line 178
    .end local v2    # "mV":F
    :cond_6
    const/4 v7, 0x4

    if-ne v0, v7, :cond_7

    .line 180
    sub-float v7, p2, p1

    const v8, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v8, v4

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v8, v8

    mul-float/2addr v7, v8

    add-float v3, p1, v7

    goto/16 :goto_0

    .line 182
    :cond_7
    const/4 v7, 0x5

    if-ne v0, v7, :cond_0

    .line 185
    sub-float v7, p2, p1

    const/high16 v8, 0x3f000000    # 0.5f

    const v9, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v9, v4

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    float-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    double-to-float v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float v3, p1, v7

    goto/16 :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getScaleAnimationTime(F)F
    .locals 1
    .param p1, "originalTime"    # F

    .prologue
    .line 229
    const/high16 v0, 0x42c80000    # 100.0f

    return v0
.end method

.method public getScrollAnimationTime(Lcom/quramsoft/xiv/XIV;F)F
    .locals 0
    .param p1, "xiv"    # Lcom/quramsoft/xiv/XIV;
    .param p2, "originalTime"    # F

    .prologue
    .line 198
    return p2
.end method

.method public getSlideAnimationTime(Lcom/quramsoft/xiv/XIV;F)F
    .locals 2
    .param p1, "xiv"    # Lcom/quramsoft/xiv/XIV;
    .param p2, "originalTime"    # F

    .prologue
    .line 217
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/quramsoft/xiv/XIV;->getSlidingSpeedManager()Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {p1}, Lcom/quramsoft/xiv/XIV;->getSlidingSpeedManager()Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

    move-result-object v0

    iget v1, p0, Lcom/quramsoft/xiv/XIVAnimationManager;->mVX:F

    invoke-virtual {v0, v1, p2}, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->getDuration(FF)F

    move-result p2

    .line 220
    .end local p2    # "originalTime":F
    :cond_0
    return p2
.end method

.method public getSnapBackAnimationTime(F)F
    .locals 1
    .param p1, "originalTime"    # F

    .prologue
    .line 204
    const/high16 v0, 0x44160000    # 600.0f

    return v0
.end method

.method public isUseXIVAnimation(I)Z
    .locals 1
    .param p1, "kind"    # I

    .prologue
    const/4 v0, 0x1

    .line 77
    packed-switch p1, :pswitch_data_0

    .line 93
    :pswitch_0
    const/4 v0, 0x0

    :pswitch_1
    return v0

    .line 77
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
