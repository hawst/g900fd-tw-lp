.class public Lcom/quramsoft/xiv/XIVImageInfoCacheService;
.super Ljava/lang/Object;
.source "XIVImageInfoCacheService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quramsoft/xiv/XIVImageInfoCacheService$XIVImageInfoData;
    }
.end annotation


# static fields
.field private static final IMAGE_INFO_CACHE_MAX_BYTES:I = 0xc800000

.field private static final IMAGE_INFO_CACHE_MAX_ENTRIES:I = 0x1388

.field private static final IMAGE_INFO_CACHE_VERSION:I = 0x3

.field private static final TAG:Ljava/lang/String; = "XIVCacheOriginalInfoService"


# instance fields
.field private mCache:Lcom/sec/android/gallery3d/common/BlobCache;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method public static create(Landroid/content/Context;)Lcom/quramsoft/xiv/XIVImageInfoCacheService;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    return-object v0
.end method

.method private static isSameKey([B[B)Z
    .locals 5
    .param p0, "key"    # [B
    .param p1, "buffer"    # [B

    .prologue
    const/4 v2, 0x0

    .line 119
    array-length v1, p0

    .line 120
    .local v1, "n":I
    array-length v3, p1

    if-ge v3, v1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v2

    .line 123
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_2

    .line 128
    const/4 v2, 0x1

    goto :goto_0

    .line 124
    :cond_2
    aget-byte v3, p0, v0

    aget-byte v4, p1, v0

    if-ne v3, v4, :cond_0

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static makeKey(Lcom/sec/android/gallery3d/data/Path;J)[B
    .locals 3
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "dateTakenInMs"    # J

    .prologue
    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getImageInfo(Lcom/sec/android/gallery3d/data/Path;J)Lcom/quramsoft/xiv/XIVImageInfoCacheService$XIVImageInfoData;
    .locals 12
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "dateTakenInMs"    # J

    .prologue
    const/4 v9, 0x0

    .line 48
    invoke-static {p1, p2, p3}, Lcom/quramsoft/xiv/XIVImageInfoCacheService;->makeKey(Lcom/sec/android/gallery3d/data/Path;J)[B

    move-result-object v4

    .line 49
    .local v4, "key":[B
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->crc64Long([B)J

    move-result-wide v2

    .line 52
    .local v2, "cacheKey":J
    const/4 v8, 0x0

    :try_start_0
    check-cast v8, [B

    .line 53
    .local v8, "value":[B
    iget-object v11, p0, Lcom/quramsoft/xiv/XIVImageInfoCacheService;->mCache:Lcom/sec/android/gallery3d/common/BlobCache;

    monitor-enter v11
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :try_start_1
    iget-object v10, p0, Lcom/quramsoft/xiv/XIVImageInfoCacheService;->mCache:Lcom/sec/android/gallery3d/common/BlobCache;

    invoke-virtual {v10, v2, v3}, Lcom/sec/android/gallery3d/common/BlobCache;->lookup(J)[B

    move-result-object v8

    .line 53
    monitor-exit v11

    .line 56
    if-nez v8, :cond_1

    move-object v6, v9

    .line 78
    .end local v8    # "value":[B
    :goto_0
    return-object v6

    .line 53
    .restart local v8    # "value":[B
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v10

    .line 75
    .end local v8    # "value":[B
    :catch_0
    move-exception v10

    :cond_0
    move-object v6, v9

    .line 78
    goto :goto_0

    .line 57
    .restart local v8    # "value":[B
    :cond_1
    invoke-static {v4, v8}, Lcom/quramsoft/xiv/XIVImageInfoCacheService;->isSameKey([B[B)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 58
    new-instance v6, Lcom/quramsoft/xiv/XIVImageInfoCacheService$XIVImageInfoData;

    invoke-direct {v6}, Lcom/quramsoft/xiv/XIVImageInfoCacheService$XIVImageInfoData;-><init>()V

    .line 60
    .local v6, "newData":Lcom/quramsoft/xiv/XIVImageInfoCacheService$XIVImageInfoData;
    array-length v7, v4

    .line 61
    .local v7, "offset":I
    array-length v10, v8

    sub-int/2addr v10, v7

    invoke-static {v10}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 62
    .local v0, "buff":Ljava/nio/ByteBuffer;
    array-length v10, v8

    sub-int/2addr v10, v7

    invoke-static {v8, v7, v10}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    iput v10, v6, Lcom/quramsoft/xiv/XIVImageInfoCacheService$XIVImageInfoData;->mWidth:I

    .line 65
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    iput v10, v6, Lcom/quramsoft/xiv/XIVImageInfoCacheService$XIVImageInfoData;->mHeight:I

    .line 67
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    .line 68
    .local v5, "lenOfFilePath":I
    new-array v1, v5, [B

    .line 70
    .local v1, "bytesOfFilePath":[B
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 71
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v1}, Ljava/lang/String;-><init>([B)V

    iput-object v10, v6, Lcom/quramsoft/xiv/XIVImageInfoCacheService$XIVImageInfoData;->mFilePath:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method public putImageInfoIfNeeded(Lcom/sec/android/gallery3d/data/Path;JLjava/lang/String;II)V
    .locals 12
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "dateTakenInMs"    # J
    .param p4, "filePath"    # Ljava/lang/String;
    .param p5, "width"    # I
    .param p6, "height"    # I

    .prologue
    .line 83
    invoke-static {p1, p2, p3}, Lcom/quramsoft/xiv/XIVImageInfoCacheService;->makeKey(Lcom/sec/android/gallery3d/data/Path;J)[B

    move-result-object v6

    .line 84
    .local v6, "key":[B
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->crc64Long([B)J

    move-result-wide v2

    .line 86
    .local v2, "cacheKey":J
    const/4 v7, 0x0

    check-cast v7, [B

    .line 87
    .local v7, "value":[B
    iget-object v10, p0, Lcom/quramsoft/xiv/XIVImageInfoCacheService;->mCache:Lcom/sec/android/gallery3d/common/BlobCache;

    monitor-enter v10

    .line 89
    :try_start_0
    iget-object v9, p0, Lcom/quramsoft/xiv/XIVImageInfoCacheService;->mCache:Lcom/sec/android/gallery3d/common/BlobCache;

    invoke-virtual {v9, v2, v3}, Lcom/sec/android/gallery3d/common/BlobCache;->lookup(J)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 87
    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    if-eqz v7, :cond_0

    invoke-static {v6, v7}, Lcom/quramsoft/xiv/XIVImageInfoCacheService;->isSameKey([B[B)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 112
    :goto_0
    return-void

    .line 90
    :catch_0
    move-exception v4

    .line 91
    .local v4, "e":Ljava/io/IOException;
    :try_start_2
    monitor-exit v10

    goto :goto_0

    .line 87
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v9

    .line 96
    :cond_0
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    .line 97
    .local v5, "filePathToArray":[B
    array-length v9, v6

    add-int/lit8 v9, v9, 0xc

    array-length v10, v5

    add-int/2addr v9, v10

    invoke-static {v9}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 99
    .local v8, "writeBuffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v8, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 100
    move/from16 v0, p5

    invoke-virtual {v8, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 101
    move/from16 v0, p6

    invoke-virtual {v8, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 102
    array-length v9, v5

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 103
    invoke-virtual {v8, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 105
    iget-object v10, p0, Lcom/quramsoft/xiv/XIVImageInfoCacheService;->mCache:Lcom/sec/android/gallery3d/common/BlobCache;

    monitor-enter v10

    .line 107
    :try_start_3
    iget-object v9, p0, Lcom/quramsoft/xiv/XIVImageInfoCacheService;->mCache:Lcom/sec/android/gallery3d/common/BlobCache;

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v11

    invoke-virtual {v9, v2, v3, v11}, Lcom/sec/android/gallery3d/common/BlobCache;->insert(J[B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 105
    :goto_1
    :try_start_4
    monitor-exit v10

    goto :goto_0

    :catchall_1
    move-exception v9

    monitor-exit v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v9

    .line 108
    :catch_1
    move-exception v9

    goto :goto_1
.end method
