.class public Lcom/quramsoft/xiv/XIVAlphablendingAnimation;
.super Ljava/lang/Object;
.source "XIVAlphablendingAnimation.java"


# instance fields
.field private mAnimationStarted:Z

.field private mDuration:F

.field private mEnd:F

.field private mNeedToAnimate:Z

.field private mStart:F

.field private mStartTime:J


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mStartTime:J

    .line 39
    return-void
.end method

.method public static create(FFF)Lcom/quramsoft/xiv/XIVAlphablendingAnimation;
    .locals 2
    .param p0, "start"    # F
    .param p1, "end"    # F
    .param p2, "duration"    # F

    .prologue
    .line 16
    new-instance v0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;-><init>()V

    .line 18
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVAlphablendingAnimation;
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mNeedToAnimate:Z

    .line 19
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mAnimationStarted:Z

    .line 21
    iput p0, v0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mStart:F

    .line 22
    iput p1, v0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mEnd:F

    .line 23
    iput p2, v0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mDuration:F

    .line 25
    return-object v0
.end method

.method public static drawBlended(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;F)V
    .locals 2
    .param p0, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "source"    # Landroid/graphics/RectF;
    .param p3, "target"    # Landroid/graphics/RectF;
    .param p4, "alpha"    # F

    .prologue
    .line 71
    invoke-interface {p0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->getAlpha()F

    move-result v0

    .line 73
    .local v0, "canvasAlpha":F
    mul-float v1, v0, p4

    invoke-interface {p0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setAlpha(F)V

    .line 74
    invoke-interface {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 75
    invoke-interface {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setAlpha(F)V

    .line 76
    return-void
.end method


# virtual methods
.method public getProgress()F
    .locals 6

    .prologue
    .line 48
    iget-boolean v1, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mAnimationStarted:Z

    if-eqz v1, :cond_1

    .line 49
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mStartTime:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    iget v2, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mDuration:F

    div-float v0, v1, v2

    .line 50
    .local v0, "progress":F
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    .line 51
    const/high16 v0, 0x3f800000    # 1.0f

    .line 52
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mNeedToAnimate:Z

    .line 54
    :cond_0
    iget v1, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mStart:F

    iget v2, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mEnd:F

    iget v3, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mStart:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    .line 58
    .end local v0    # "progress":F
    :goto_0
    return v1

    .line 56
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mAnimationStarted:Z

    .line 57
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mStartTime:J

    .line 58
    iget v1, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mStart:F

    goto :goto_0
.end method

.method public needToAnimate()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mNeedToAnimate:Z

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mNeedToAnimate:Z

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->mAnimationStarted:Z

    .line 66
    return-void
.end method
