.class public Lcom/quramsoft/xiv/XIVDefinedValues;
.super Ljava/lang/Object;
.source "XIVDefinedValues.java"


# static fields
.field static final DEFAULT_SLIDE_ANIMATION_DURATION:I = 0x190

.field static final DEFAULT_SLIDING_DURATION:I = 0x3e8

.field static final DELAY_TIME_FOR_LTN:I = 0x64

.field static final DELAY_TIME_FOR_MTN:I = 0x1

.field static final DELAY_TIME_FOR_RD:I = 0x1

.field static final DELAY_TIME_FOR_RD_SCAN:I = 0x64

.field static FIXED_SAMPLE_LEVEL_FOR_LTN:I = 0x0

.field static final IMAGE_INFO_CACHE_FILE:Ljava/lang/String; = "imgInfoCache"

.field static IMAGE_ROTATION_ANIMATION_TIME:I = 0x0

.field static final INIT_SIZE_OF_LTN_QUEUE:I = 0x16

.field static final INIT_SIZE_OF_LTN_ROW_QUEUE:I = 0x2

.field static final INIT_SIZE_OF_MTN_QUEUE:I = 0xa

.field static final LIMIT_OF_FTN_CONTAINER:I = 0xa

.field static LIMIT_OF_LTN_CONTAINER:I = 0x0

.field static LIMIT_OF_LTN_SIZE:I = 0x0

.field static final LIMIT_OF_LTN_TILE_QUEUE:I = 0x14

.field static final LIMIT_OF_MTN_CONTAINER:I = 0xa

.field static LIMIT_OF_RD_CONTAINER:I = 0x0

.field static final LIMIT_OF_STN_CONTAINER:I = 0x64

.field static final LIMIT_OF_TEXTURE_SIZE:I = 0x200

.field static final LTN_H_STEP:I = 0x1fe

.field static LTN_MAX_SAMPLED_WIDTH:I = 0x0

.field static final LTN_TO_RD_ALPHA_ANIM_TIME:F = 200.0f

.field static LTN_W_STEP:I = 0x0

.field static final MAX_FLING_VX:F = 20000.0f

.field static final MICRO_CACHE_FILE:Ljava/lang/String; = "imgcacheMicro"

.field static final MINI_CACHE_FILE:Ljava/lang/String; = "imgcacheMini"

.field static final MTN_TO_LTN_ALPHA_ANIM_TIME:F = 200.0f

.field static final ORIGINAL_FRAME_TIME:I = 0x21

.field static final ORIGINAL_TILE_SIZE:I = 0x1fe

.field static final PI:F = 3.1415927f

.field static final SLEEP_TIME_UNIT_FOR_LTN:I = 0x10

.field static final SLEEP_TIME_UNIT_FOR_MTN:I = 0x10

.field static final SLEEP_TIME_UNIT_FOR_RD:I = 0x10

.field static final SLEEP_TIME_UNIT_FOR_RD_SCAN:I = 0x10

.field static final SLIDING_DURATION_LIMIT:I = 0x12c

.field static final XIV_CENTER_LTN_TIME_DELAY:I = 0x64

.field static final XIV_CENTER_LTN_TIME_LIMIT:I = 0xa

.field static final XIV_DOWN_SAMPLING_TILE_SIZE:I = 0x7e

.field static final XIV_FRAME_TIME:I = 0x10

.field static final XIV_LARGE_ALBUM_CACHE_SIZE:I = 0x80

.field static final XIV_MTN_CACHING_SIZE:I = 0x2

.field static XIV_PANORAMA_THUM_CROP_RATIO:F = 0.0f

.field static final XIV_SIDE_LTN_TIME_DELAY:I = 0x64

.field static final XIV_SIDE_LTN_TIME_LIMIT:I = 0x5

.field static final XIV_TILE_BORDER:I = 0x1

.field static XIV_TILE_DEC_SIZE:I

.field static XIV_TILE_SIZE:I

.field static mMaxSlidingAnimationDuration:I

.field static mMinSlidingAnimationDuration:I

.field static mSlidingAnimationInverseProportionalFactor:I


# instance fields
.field private mXiv:Lcom/quramsoft/xiv/XIV;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0x1f4

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->mMaxSlidingAnimationDuration:I

    .line 51
    const/16 v0, 0x96

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->mMinSlidingAnimationDuration:I

    .line 52
    const v0, 0x16e360

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->mSlidingAnimationInverseProportionalFactor:I

    .line 54
    const/16 v0, 0x190

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->IMAGE_ROTATION_ANIMATION_TIME:I

    .line 65
    const v0, 0x3fe38e39

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_PANORAMA_THUM_CROP_RATIO:F

    .line 68
    const/16 v0, 0x500

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_MAX_SAMPLED_WIDTH:I

    .line 72
    const/16 v0, 0x1fe

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_W_STEP:I

    .line 86
    const/4 v0, 0x1

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->FIXED_SAMPLE_LEVEL_FOR_LTN:I

    .line 93
    const/16 v0, 0x100

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_TILE_DEC_SIZE:I

    .line 94
    sget v0, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_TILE_DEC_SIZE:I

    add-int/lit8 v0, v0, -0x2

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_TILE_SIZE:I

    .line 103
    const/16 v0, 0x50

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->LIMIT_OF_RD_CONTAINER:I

    .line 104
    const/16 v0, 0x28

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->LIMIT_OF_LTN_CONTAINER:I

    .line 111
    const/16 v0, 0xbb8

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->LIMIT_OF_LTN_SIZE:I

    .line 11
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method static final create(Lcom/quramsoft/xiv/XIV;)Lcom/quramsoft/xiv/XIVDefinedValues;
    .locals 1
    .param p0, "xiv"    # Lcom/quramsoft/xiv/XIV;

    .prologue
    .line 17
    new-instance v0, Lcom/quramsoft/xiv/XIVDefinedValues;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIVDefinedValues;-><init>()V

    .line 18
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVDefinedValues;
    iput-object p0, v0, Lcom/quramsoft/xiv/XIVDefinedValues;->mXiv:Lcom/quramsoft/xiv/XIV;

    .line 19
    return-object v0
.end method

.method public static final getAlbumCacheSize()I
    .locals 1

    .prologue
    .line 163
    const/16 v0, 0x80

    return v0
.end method

.method public static final getAlphablendingAnimationTimeFromLTNToRD()F
    .locals 1

    .prologue
    .line 207
    const/high16 v0, 0x43480000    # 200.0f

    return v0
.end method

.method public static final getCenterLTNTimeDelay()I
    .locals 1

    .prologue
    .line 191
    const/16 v0, 0x64

    return v0
.end method

.method public static final getCenterLTNTimeLimit()I
    .locals 1

    .prologue
    .line 187
    const/16 v0, 0xa

    return v0
.end method

.method public static getDelayTimeForMTN()I
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    return v0
.end method

.method public static getDelayTimeForRD()I
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x1

    return v0
.end method

.method public static getDelayTimeForRDScan()I
    .locals 1

    .prologue
    .line 134
    const/16 v0, 0x64

    return v0
.end method

.method public static final getFrameTime()I
    .locals 1

    .prologue
    .line 120
    const/16 v0, 0x10

    return v0
.end method

.method public static final getImageInfoCacheFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    const-string v0, "imgInfoCache"

    return-object v0
.end method

.method public static final getMTNCachingSize()I
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x2

    return v0
.end method

.method public static final getMicroCacheFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    const-string v0, "imgcacheMicro"

    return-object v0
.end method

.method public static final getMiniCacheFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    const-string v0, "imgcacheMini"

    return-object v0
.end method

.method public static final getSideLTNTimeDelay()I
    .locals 1

    .prologue
    .line 199
    const/16 v0, 0x64

    return v0
.end method

.method public static final getSideLTNTimeLimit()I
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x5

    return v0
.end method

.method public static getSleepTimeUnitForMTN()I
    .locals 1

    .prologue
    .line 146
    const/16 v0, 0x10

    return v0
.end method

.method public static getSleepTimeUnitForRD()I
    .locals 1

    .prologue
    .line 130
    const/16 v0, 0x10

    return v0
.end method

.method public static getSleepTimeUnitForRDScan()I
    .locals 1

    .prologue
    .line 138
    const/16 v0, 0x10

    return v0
.end method

.method public static final getSlidingDurationLimit()I
    .locals 1

    .prologue
    .line 203
    const/16 v0, 0x12c

    return v0
.end method

.method public static final getTileDecSize()I
    .locals 1

    .prologue
    .line 253
    sget v0, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_TILE_DEC_SIZE:I

    return v0
.end method

.method public static final getTileSize()I
    .locals 1

    .prologue
    .line 245
    sget v0, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_TILE_SIZE:I

    return v0
.end method

.method public static setDurationOfImageRotationAnimation(I)V
    .locals 0
    .param p0, "duration"    # I

    .prologue
    .line 150
    sput p0, Lcom/quramsoft/xiv/XIVDefinedValues;->IMAGE_ROTATION_ANIMATION_TIME:I

    .line 151
    return-void
.end method

.method public static final setLimitOfLTNContainer(I)V
    .locals 0
    .param p0, "limit"    # I

    .prologue
    .line 265
    sput p0, Lcom/quramsoft/xiv/XIVDefinedValues;->LIMIT_OF_LTN_CONTAINER:I

    .line 266
    return-void
.end method

.method public static final setLimitOfLargeThumbnalSize(I)V
    .locals 1
    .param p0, "limit"    # I

    .prologue
    .line 270
    const/4 v0, 0x1

    sput-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LIMIT_WIDTH_OF_LTN:Z

    .line 271
    sput p0, Lcom/quramsoft/xiv/XIVDefinedValues;->LIMIT_OF_LTN_SIZE:I

    .line 272
    return-void
.end method

.method public static final setLimitOfRDContainer(I)V
    .locals 0
    .param p0, "limit"    # I

    .prologue
    .line 260
    sput p0, Lcom/quramsoft/xiv/XIVDefinedValues;->LIMIT_OF_RD_CONTAINER:I

    .line 261
    return-void
.end method

.method public static setSlidingAnimationFactors(III)V
    .locals 0
    .param p0, "maxDuration"    # I
    .param p1, "minDuration"    # I
    .param p2, "inverseProportionalFactor"    # I

    .prologue
    .line 155
    sput p0, Lcom/quramsoft/xiv/XIVDefinedValues;->mMaxSlidingAnimationDuration:I

    .line 156
    sput p1, Lcom/quramsoft/xiv/XIVDefinedValues;->mMinSlidingAnimationDuration:I

    .line 157
    sput p2, Lcom/quramsoft/xiv/XIVDefinedValues;->mSlidingAnimationInverseProportionalFactor:I

    .line 158
    return-void
.end method

.method public static final setTileDecSize(I)V
    .locals 1
    .param p0, "size"    # I

    .prologue
    .line 238
    sput p0, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_TILE_DEC_SIZE:I

    .line 239
    sget v0, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_TILE_DEC_SIZE:I

    add-int/lit8 v0, v0, -0x2

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_TILE_SIZE:I

    .line 240
    return-void
.end method


# virtual methods
.method public final setWidthOfView(Landroid/view/WindowManager;)V
    .locals 11
    .param p1, "manager"    # Landroid/view/WindowManager;

    .prologue
    const/16 v10, 0x1fe

    .line 211
    iget-object v7, p0, Lcom/quramsoft/xiv/XIVDefinedValues;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-eqz v7, :cond_0

    if-nez p1, :cond_1

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 217
    .local v0, "display":Landroid/view/Display;
    :try_start_0
    const-class v7, Landroid/view/Display;

    const-string v8, "getRawHeight"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 218
    .local v3, "mGetRawH":Ljava/lang/reflect/Method;
    const-class v7, Landroid/view/Display;

    const-string v8, "getRawWidth"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 219
    .local v4, "mGetRawW":Ljava/lang/reflect/Method;
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v4, v0, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 220
    .local v6, "width":I
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 228
    .end local v3    # "mGetRawH":Ljava/lang/reflect/Method;
    .end local v4    # "mGetRawW":Ljava/lang/reflect/Method;
    .local v2, "height":I
    :goto_1
    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v7

    mul-int/lit8 v7, v7, 0x2

    sput v7, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_W_STEP:I

    .line 229
    sget v7, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_W_STEP:I

    sput v7, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_MAX_SAMPLED_WIDTH:I

    .line 230
    sget v7, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_W_STEP:I

    if-le v7, v10, :cond_2

    .line 231
    sput v10, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_W_STEP:I

    .line 233
    :cond_2
    iget-object v7, p0, Lcom/quramsoft/xiv/XIVDefinedValues;->mXiv:Lcom/quramsoft/xiv/XIV;

    iget-object v7, v7, Lcom/quramsoft/xiv/XIV;->mLargeThumbnailManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    invoke-virtual {v7, v6, v2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->setViewSize(II)V

    goto :goto_0

    .line 221
    .end local v2    # "height":I
    .end local v6    # "width":I
    :catch_0
    move-exception v1

    .line 222
    .local v1, "e":Ljava/lang/Exception;
    new-instance v5, Landroid/util/DisplayMetrics;

    invoke-direct {v5}, Landroid/util/DisplayMetrics;-><init>()V

    .line 223
    .local v5, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v0, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 224
    iget v6, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 225
    .restart local v6    # "width":I
    iget v2, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    .restart local v2    # "height":I
    goto :goto_1
.end method
