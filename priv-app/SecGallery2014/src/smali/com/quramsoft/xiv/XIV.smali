.class public Lcom/quramsoft/xiv/XIV;
.super Ljava/lang/Object;
.source "XIV.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final NO_SELECTED:I = -0x1

.field private static final TAG:Ljava/lang/String; = "XIV"

.field private static mIsAnimating:Z

.field private static mLastZoomingTime:J


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/GalleryApp;

.field mAnimationManager:Lcom/quramsoft/xiv/XIVAnimationManager;

.field mBitmapReuseManager:Lcom/quramsoft/xiv/XIVBitmapReuseManager;

.field mBufferReuseManager:Lcom/quramsoft/xiv/XIVBufferReuseManager;

.field mDefinedValues:Lcom/quramsoft/xiv/XIVDefinedValues;

.field mImageRotationAnimationManager:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;

.field private mIsScaling:Z

.field private mIsTouched:Z

.field private mIsTracking:Z

.field mLargeThumbnailManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

.field private mLastAnimationTime:J

.field private mMultiViewState:Z

.field private mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

.field private mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

.field private mSelectedSlotIndex:I

.field mSlidingSpeedManager:Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

.field private mStartScale:F

.field mTileManager:Lcom/quramsoft/xiv/XIVTileManager;

.field private mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 193
    const/4 v0, 0x0

    sput-boolean v0, Lcom/quramsoft/xiv/XIV;->mIsAnimating:Z

    .line 195
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/quramsoft/xiv/XIV;->mLastZoomingTime:J

    .line 13
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object v0, p0, Lcom/quramsoft/xiv/XIV;->mActivity:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 106
    iput-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 143
    iput-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .line 164
    iput-boolean v2, p0, Lcom/quramsoft/xiv/XIV;->mMultiViewState:Z

    .line 194
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/quramsoft/xiv/XIV;->mLastAnimationTime:J

    .line 317
    iput-boolean v2, p0, Lcom/quramsoft/xiv/XIV;->mIsTouched:Z

    .line 318
    iput-boolean v2, p0, Lcom/quramsoft/xiv/XIV;->mIsTracking:Z

    .line 319
    iput-boolean v2, p0, Lcom/quramsoft/xiv/XIV;->mIsScaling:Z

    .line 320
    const/4 v0, 0x0

    iput v0, p0, Lcom/quramsoft/xiv/XIV;->mStartScale:F

    .line 405
    const/4 v0, -0x1

    iput v0, p0, Lcom/quramsoft/xiv/XIV;->mSelectedSlotIndex:I

    .line 39
    return-void
.end method

.method public static final create(Lcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/quramsoft/xiv/XIV;
    .locals 2
    .param p0, "XIVInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .prologue
    .line 23
    new-instance v0, Lcom/quramsoft/xiv/XIV;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIV;-><init>()V

    .line 24
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIV;
    iput-object p0, v0, Lcom/quramsoft/xiv/XIV;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .line 25
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVTileManager;->create(Lcom/quramsoft/xiv/XIV;)Lcom/quramsoft/xiv/XIVTileManager;

    move-result-object v1

    iput-object v1, v0, Lcom/quramsoft/xiv/XIV;->mTileManager:Lcom/quramsoft/xiv/XIVTileManager;

    .line 26
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVDefinedValues;->create(Lcom/quramsoft/xiv/XIV;)Lcom/quramsoft/xiv/XIVDefinedValues;

    move-result-object v1

    iput-object v1, v0, Lcom/quramsoft/xiv/XIV;->mDefinedValues:Lcom/quramsoft/xiv/XIVDefinedValues;

    .line 27
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVAnimationManager;->create(Lcom/quramsoft/xiv/XIV;)Lcom/quramsoft/xiv/XIVAnimationManager;

    move-result-object v1

    iput-object v1, v0, Lcom/quramsoft/xiv/XIV;->mAnimationManager:Lcom/quramsoft/xiv/XIVAnimationManager;

    .line 28
    invoke-static {}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->create()Lcom/quramsoft/xiv/XIVBitmapReuseManager;

    move-result-object v1

    iput-object v1, v0, Lcom/quramsoft/xiv/XIV;->mBitmapReuseManager:Lcom/quramsoft/xiv/XIVBitmapReuseManager;

    .line 29
    invoke-static {}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->create()Lcom/quramsoft/xiv/XIVBufferReuseManager;

    move-result-object v1

    iput-object v1, v0, Lcom/quramsoft/xiv/XIV;->mBufferReuseManager:Lcom/quramsoft/xiv/XIVBufferReuseManager;

    .line 30
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->create(Lcom/quramsoft/xiv/XIV;)Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;

    move-result-object v1

    iput-object v1, v0, Lcom/quramsoft/xiv/XIV;->mImageRotationAnimationManager:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;

    .line 31
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->create(Lcom/quramsoft/xiv/XIV;)Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

    move-result-object v1

    iput-object v1, v0, Lcom/quramsoft/xiv/XIV;->mSlidingSpeedManager:Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

    .line 32
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->create(Lcom/quramsoft/xiv/XIV;)Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v1

    iput-object v1, v0, Lcom/quramsoft/xiv/XIV;->mLargeThumbnailManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    .line 34
    return-object v0
.end method


# virtual methods
.method public checkAnimation(IZ)V
    .locals 2
    .param p1, "kind"    # I
    .param p2, "isAnimating"    # Z

    .prologue
    .line 201
    packed-switch p1, :pswitch_data_0

    .line 217
    :goto_0
    return-void

    .line 203
    :pswitch_0
    if-eqz p2, :cond_0

    .line 204
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/quramsoft/xiv/XIV;->mLastZoomingTime:J

    .line 215
    :cond_0
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/quramsoft/xiv/XIV;->checkAnimation(Z)V

    goto :goto_0

    .line 201
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public checkAnimation(Z)V
    .locals 2
    .param p1, "isAnimating"    # Z

    .prologue
    .line 231
    sget-boolean v0, Lcom/quramsoft/xiv/XIV;->mIsAnimating:Z

    if-eq v0, p1, :cond_0

    .line 233
    sput-boolean p1, Lcom/quramsoft/xiv/XIV;->mIsAnimating:Z

    .line 234
    if-nez p1, :cond_0

    .line 235
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/quramsoft/xiv/XIV;->mLastAnimationTime:J

    .line 241
    :cond_0
    return-void
.end method

.method public checkSlotSelected(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 411
    iput p1, p0, Lcom/quramsoft/xiv/XIV;->mSelectedSlotIndex:I

    .line 412
    return-void
.end method

.method public getAnimationManager()Lcom/quramsoft/xiv/XIVAnimationManager;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mAnimationManager:Lcom/quramsoft/xiv/XIVAnimationManager;

    return-object v0
.end method

.method public getBitmapReuseManager()Lcom/quramsoft/xiv/XIVBitmapReuseManager;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mBitmapReuseManager:Lcom/quramsoft/xiv/XIVBitmapReuseManager;

    return-object v0
.end method

.method public getBufferReuseManager()Lcom/quramsoft/xiv/XIVBufferReuseManager;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mBufferReuseManager:Lcom/quramsoft/xiv/XIVBufferReuseManager;

    return-object v0
.end method

.method public getCurrentPhotoIndex()I
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v0, :cond_0

    .line 179
    iget-object v1, p0, Lcom/quramsoft/xiv/XIV;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    monitor-enter v1

    .line 180
    :try_start_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v0

    monitor-exit v1

    .line 184
    :goto_0
    return v0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 184
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getDefinedValues()Lcom/quramsoft/xiv/XIVDefinedValues;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mDefinedValues:Lcom/quramsoft/xiv/XIVDefinedValues;

    return-object v0
.end method

.method public getGalleryApp()Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mActivity:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method public getImageRotationAnimationManager()Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mImageRotationAnimationManager:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;

    return-object v0
.end method

.method public getItemFromIndex(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->getPhotoDataAdapter()Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-result-object v0

    .line 285
    .local v0, "pda":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    if-eqz v0, :cond_0

    .line 286
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItemFromIndex(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 288
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mLargeThumbnailManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    return-object v0
.end method

.method public getMinScale()F
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getScaleMin()F

    move-result v0

    .line 311
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPhotoDataAdapter()Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    return-object v0
.end method

.method public getSelectedSlotIndex()I
    .locals 1

    .prologue
    .line 416
    iget v0, p0, Lcom/quramsoft/xiv/XIV;->mSelectedSlotIndex:I

    return v0
.end method

.method public getSlidingSpeedManager()Lcom/quramsoft/xiv/XIVSlidingSpeedManager;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mSlidingSpeedManager:Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

    return-object v0
.end method

.method public getTileManager()Lcom/quramsoft/xiv/XIVTileManager;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mTileManager:Lcom/quramsoft/xiv/XIVTileManager;

    return-object v0
.end method

.method getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    return-object v0
.end method

.method public isAnimating(I)Z
    .locals 8
    .param p1, "delay"    # I

    .prologue
    .line 245
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/quramsoft/xiv/XIV;->mLastAnimationTime:J

    sub-long v0, v4, v6

    .line 246
    .local v0, "diff":J
    sget-boolean v3, Lcom/quramsoft/xiv/XIV;->mIsAnimating:Z

    if-nez v3, :cond_0

    int-to-long v4, p1

    cmp-long v3, v0, v4

    if-ltz v3, :cond_0

    const/4 v2, 0x0

    .line 251
    .local v2, "ret":Z
    :goto_0
    return v2

    .line 246
    .end local v2    # "ret":Z
    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isBeingSliding()Z
    .locals 4

    .prologue
    .line 443
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->getSlidingSpeedManager()Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

    move-result-object v0

    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getSlidingDurationLimit()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->isDurationUnderLimit(J)Z

    move-result v0

    return v0
.end method

.method public isFilmMode()Z
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getFilmMode()Z

    move-result v0

    .line 140
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isMultiViewState()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIV;->mMultiViewState:Z

    return v0
.end method

.method public isScaling()Z
    .locals 1

    .prologue
    .line 393
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIV;->mIsScaling:Z

    return v0
.end method

.method public isTouched()Z
    .locals 1

    .prologue
    .line 383
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIV;->mIsTouched:Z

    return v0
.end method

.method public isTracking()Z
    .locals 1

    .prologue
    .line 388
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIV;->mIsTracking:Z

    return v0
.end method

.method public isZoomState()Z
    .locals 4

    .prologue
    .line 294
    const/4 v0, 0x0

    .line 295
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v1, :cond_0

    .line 296
    iget-object v2, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    monitor-enter v2

    .line 297
    :try_start_0
    iget-object v1, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getCurrentScale()F

    move-result v1

    iget-object v3, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getScaleMin()F

    move-result v3

    invoke-static {v1, v3}, Lcom/quramsoft/xiv/XIVUtils;->isAlmostEquals(FF)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    .line 296
    :goto_0
    monitor-exit v2

    .line 303
    :cond_0
    return v0

    .line 297
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 296
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isZooming(I)Z
    .locals 8
    .param p1, "delay"    # I

    .prologue
    .line 256
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sget-wide v6, Lcom/quramsoft/xiv/XIV;->mLastZoomingTime:J

    sub-long v0, v4, v6

    .line 257
    .local v0, "diff":J
    int-to-long v4, p1

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    const/4 v2, 0x1

    .line 262
    .local v2, "ret":Z
    :goto_0
    return v2

    .line 257
    .end local v2    # "ret":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public moveTo(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 422
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mLargeThumbnailManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    invoke-virtual {v0, p1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->moveTo(I)V

    .line 423
    return-void
.end method

.method public needToWaitForAnimation(I)Z
    .locals 3
    .param p1, "delay"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 274
    sget-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_STOP_RD_WHILE_ZOOMING:Z

    if-eqz v2, :cond_2

    .line 275
    invoke-virtual {p0, p1}, Lcom/quramsoft/xiv/XIV;->isAnimating(I)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->isTouched()Z

    move-result v2

    if-nez v2, :cond_1

    .line 277
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 275
    goto :goto_0

    .line 277
    :cond_2
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->isZoomState()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/quramsoft/xiv/XIV;->isAnimating(I)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->isTouched()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public onDoubleTapZoomBegin()V
    .locals 0

    .prologue
    .line 355
    return-void
.end method

.method public onDoubleTapZoomEnd()V
    .locals 0

    .prologue
    .line 361
    return-void
.end method

.method public final onPhotoView()Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->stopDecodingForDelayTime()V

    .line 374
    return-void
.end method

.method public onScaleBegin(F)V
    .locals 1
    .param p1, "scale"    # F

    .prologue
    .line 365
    iput p1, p0, Lcom/quramsoft/xiv/XIV;->mStartScale:F

    .line 366
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIV;->mIsScaling:Z

    .line 367
    return-void
.end method

.method public onScaleEnd()V
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIV;->mIsScaling:Z

    .line 379
    return-void
.end method

.method public onSurfaceChanged()V
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mTileManager:Lcom/quramsoft/xiv/XIVTileManager;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVTileManager;->onSurfaceChanged()V

    .line 401
    return-void
.end method

.method public processTouchEvent(I)V
    .locals 2
    .param p1, "action"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 325
    packed-switch p1, :pswitch_data_0

    .line 349
    :goto_0
    return-void

    .line 328
    :pswitch_0
    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIV;->mIsTouched:Z

    .line 329
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->stopDecodingForDelayTime()V

    goto :goto_0

    .line 335
    :pswitch_1
    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIV;->mIsTouched:Z

    .line 336
    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIV;->mIsTracking:Z

    .line 337
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->stopDecodingForDelayTime()V

    goto :goto_0

    .line 340
    :pswitch_2
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIV;->mIsTracking:Z

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mTileManager:Lcom/quramsoft/xiv/XIVTileManager;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVTileManager;->resetTimeSumOfDecodingTiles()V

    .line 342
    :cond_0
    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIV;->mIsTouched:Z

    .line 343
    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIV;->mIsTracking:Z

    goto :goto_0

    .line 325
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setGalleryApp(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 0
    .param p1, "ga"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/quramsoft/xiv/XIV;->mActivity:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 99
    return-void
.end method

.method public setMultiViewState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 168
    iput-boolean p1, p0, Lcom/quramsoft/xiv/XIV;->mMultiViewState:Z

    .line 169
    return-void
.end method

.method public setPhotoDataAdapter(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p1, "pda"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/quramsoft/xiv/XIV;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .line 147
    return-void
.end method

.method public setPhotoView(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 1
    .param p1, "pv"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 111
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mTileManager:Lcom/quramsoft/xiv/XIVTileManager;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVTileManager;->resetTimeSumOfDecodingTiles()V

    .line 112
    const/4 v0, 0x0

    sput-boolean v0, Lcom/quramsoft/xiv/XIV;->mIsAnimating:Z

    .line 114
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mBufferReuseManager:Lcom/quramsoft/xiv/XIVBufferReuseManager;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->initBufferContainers()V

    .line 115
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mBitmapReuseManager:Lcom/quramsoft/xiv/XIVBitmapReuseManager;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->initBitmapContainer()V

    .line 116
    return-void
.end method

.method public setWidthOfView(Landroid/view/WindowManager;)V
    .locals 1
    .param p1, "manager"    # Landroid/view/WindowManager;

    .prologue
    .line 439
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mDefinedValues:Lcom/quramsoft/xiv/XIVDefinedValues;

    invoke-virtual {v0, p1}, Lcom/quramsoft/xiv/XIVDefinedValues;->setWidthOfView(Landroid/view/WindowManager;)V

    .line 440
    return-void
.end method

.method public stopDecodingForDelayTime()V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v0, :cond_0

    .line 268
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/quramsoft/xiv/XIV;->mLastAnimationTime:J

    .line 270
    :cond_0
    return-void
.end method

.method public switchToNext()V
    .locals 2

    .prologue
    .line 433
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->getCurrentPhotoIndex()I

    move-result v0

    .line 434
    .local v0, "currentIndex":I
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Lcom/quramsoft/xiv/XIV;->moveTo(I)V

    .line 435
    return-void
.end method

.method public switchToPrev()V
    .locals 2

    .prologue
    .line 427
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->getCurrentPhotoIndex()I

    move-result v0

    .line 428
    .local v0, "currentIndex":I
    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Lcom/quramsoft/xiv/XIV;->moveTo(I)V

    .line 429
    return-void
.end method

.method public unsetPhotoDataAdapter()V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v0, :cond_0

    .line 157
    iget-object v1, p0, Lcom/quramsoft/xiv/XIV;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    monitor-enter v1

    .line 158
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .line 157
    monitor-exit v1

    .line 161
    :cond_0
    return-void

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unsetPhotoView()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mBufferReuseManager:Lcom/quramsoft/xiv/XIVBufferReuseManager;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->freeBufferContainers()V

    .line 124
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mBitmapReuseManager:Lcom/quramsoft/xiv/XIVBitmapReuseManager;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->freeBitmapContainer()V

    .line 126
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v0, :cond_0

    .line 127
    iget-object v1, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    monitor-enter v1

    .line 128
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/quramsoft/xiv/XIV;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 127
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mLargeThumbnailManager:Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->releaseAll()V

    .line 133
    iget-object v0, p0, Lcom/quramsoft/xiv/XIV;->mTileManager:Lcom/quramsoft/xiv/XIVTileManager;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVTileManager;->freeQrd()V

    .line 134
    return-void

    .line 127
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
