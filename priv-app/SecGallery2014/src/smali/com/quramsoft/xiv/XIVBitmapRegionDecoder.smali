.class public Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
.super Ljava/lang/Object;
.source "XIVBitmapRegionDecoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quramsoft/xiv/XIVBitmapRegionDecoder$IOHelper;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final MAX_SIZE_OF_LTN:I = 0x800

.field private static final MIN_SIZE_OF_LTN:I

.field private static final SAMPLE_SIZE_OF_LTN:I = 0x8

.field private static final TAG:Ljava/lang/String; = "XIVBitmapRegionDecoder"

.field static sQrOptsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/quramsoft/qrb/QuramBitmapFactory$Options;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field isRecycled:Z

.field private mBrd:Landroid/graphics/BitmapRegionDecoder;

.field private mHeight:I

.field mIsDoingDecoding:Z

.field private mIsPreview:Z

.field private mIsShareable:Z

.field private mIsUseQrRegionDecoder:Z

.field private mLargeThumbnail:Landroid/graphics/Bitmap;

.field private mPathName:Ljava/lang/String;

.field private mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget v0, Lcom/quramsoft/xiv/XIVCacheManager;->THUMBNAIL_SIZE:I

    sput v0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->MIN_SIZE_OF_LTN:I

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->sQrOptsList:Ljava/util/ArrayList;

    .line 31
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    .line 43
    iput-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    .line 45
    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    .line 48
    iput-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mPathName:Ljava/lang/String;

    .line 49
    iput v2, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mWidth:I

    .line 50
    iput v2, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mHeight:I

    .line 51
    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsShareable:Z

    .line 52
    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsPreview:Z

    .line 54
    iput-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mLargeThumbnail:Landroid/graphics/Bitmap;

    .line 396
    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsDoingDecoding:Z

    .line 442
    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->isRecycled:Z

    .line 31
    return-void
.end method

.method public static cancelAllNewInstances()V
    .locals 3

    .prologue
    .line 220
    sget-object v1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->sQrOptsList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 230
    .local v0, "i":I
    :goto_0
    return-void

    .line 224
    .end local v0    # "i":I
    :cond_0
    sget-object v2, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->sQrOptsList:Ljava/util/ArrayList;

    monitor-enter v2

    .line 225
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    :try_start_0
    sget-object v1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->sQrOptsList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 224
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 227
    :cond_1
    :try_start_1
    sget-object v1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->sQrOptsList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-static {v1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->cancelRegionMap(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static newInstance(Ljava/io/FileDescriptor;Z)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    .locals 9
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "isShareable"    # Z

    .prologue
    const/4 v7, 0x0

    .line 234
    new-instance v6, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    invoke-direct {v6}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;-><init>()V

    .line 236
    .local v6, "xivBrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    :try_start_0
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseFileDescriptorQRB()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 237
    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    .line 242
    :goto_0
    const/4 v8, 0x0

    iput-object v8, v6, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    .line 243
    iget-boolean v8, v6, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    if-eqz v8, :cond_0

    .line 244
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 245
    .local v2, "fis":Ljava/io/FileInputStream;
    invoke-static {v2}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder$IOHelper;->readToEnd(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 246
    .local v0, "data":[B
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 247
    const/4 v4, 0x0

    .line 248
    .local v4, "offset":I
    array-length v3, v0

    .line 250
    .local v3, "length":I
    new-instance v5, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 251
    .local v5, "qOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v0, v4, v3, p1, v5}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->newInstance([BIIZLcom/quramsoft/qrb/QuramBitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    move-result-object v8

    iput-object v8, v6, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    .line 253
    .end local v0    # "data":[B
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .end local v3    # "length":I
    .end local v4    # "offset":I
    .end local v5    # "qOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    :cond_0
    iget-object v8, v6, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    if-nez v8, :cond_1

    .line 254
    const/4 v8, 0x0

    iput-boolean v8, v6, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    .line 255
    invoke-static {p0, p1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/FileDescriptor;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v8

    iput-object v8, v6, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    .line 256
    iget-object v8, v6, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    if-nez v8, :cond_1

    move-object v6, v7

    .line 265
    .end local v6    # "xivBrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    :cond_1
    :goto_1
    return-object v6

    .line 239
    .restart local v6    # "xivBrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    :cond_2
    const/4 v8, 0x0

    iput-boolean v8, v6, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 260
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/io/IOException;
    move-object v6, v7

    .line 262
    goto :goto_1
.end method

.method public static newInstance(Ljava/io/InputStream;Z)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    .locals 8
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "isShareable"    # Z

    .prologue
    const/4 v6, 0x0

    .line 269
    new-instance v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    invoke-direct {v5}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;-><init>()V

    .line 271
    .local v5, "xivBrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    :try_start_0
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseFileDescriptorQRB()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 272
    const/4 v7, 0x1

    iput-boolean v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    .line 277
    :goto_0
    const/4 v7, 0x0

    iput-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    .line 278
    iget-boolean v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    if-eqz v7, :cond_0

    .line 279
    invoke-static {p0}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder$IOHelper;->readToEnd(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 280
    .local v0, "data":[B
    const/4 v3, 0x0

    .line 281
    .local v3, "offset":I
    array-length v2, v0

    .line 282
    .local v2, "length":I
    new-instance v4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 283
    .local v4, "qOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {v0, v3, v2, p1, v4}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->newInstance([BIIZLcom/quramsoft/qrb/QuramBitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    move-result-object v7

    iput-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    .line 285
    .end local v0    # "data":[B
    .end local v2    # "length":I
    .end local v3    # "offset":I
    .end local v4    # "qOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    :cond_0
    iget-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    if-nez v7, :cond_1

    .line 286
    const/4 v7, 0x0

    iput-boolean v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    .line 287
    invoke-static {p0, p1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v7

    iput-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    .line 288
    iget-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    if-nez v7, :cond_1

    move-object v5, v6

    .line 296
    .end local v5    # "xivBrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    :cond_1
    :goto_1
    return-object v5

    .line 274
    .restart local v5    # "xivBrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    :cond_2
    const/4 v7, 0x0

    iput-boolean v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 291
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/io/IOException;
    move-object v5, v6

    .line 293
    goto :goto_1
.end method

.method public static newInstance(Ljava/lang/String;Z)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    .locals 1
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "isShareable"    # Z

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->newInstance(Ljava/lang/String;ZZ)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;ZLcom/quramsoft/qrb/QuramBitmapFactory$Options;)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    .locals 3
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "isShareable"    # Z
    .param p2, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v1, 0x0

    .line 107
    if-nez p2, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-object v1

    .line 110
    :cond_1
    new-instance v1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    invoke-direct {v1}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;-><init>()V

    .line 111
    .local v1, "xivBrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    invoke-virtual {p2}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getWidth()I

    move-result v2

    iput v2, v1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mWidth:I

    .line 112
    invoke-virtual {p2}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHeight()I

    move-result v2

    iput v2, v1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mHeight:I

    .line 113
    iput-object p0, v1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mPathName:Ljava/lang/String;

    .line 114
    iput-boolean p1, v1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsShareable:Z

    .line 117
    const/4 v2, 0x0

    :try_start_0
    iput-object v2, v1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    .line 118
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    .line 119
    invoke-static {p2}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->newInstance(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    move-result-object v2

    iput-object v2, v1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    .line 120
    iget-object v2, v1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v2, :cond_0

    .line 121
    const/4 v1, 0x0

    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;ZZ)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    .locals 11
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "isShareable"    # Z
    .param p2, "isPreview"    # Z

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 136
    if-nez p0, :cond_1

    move-object v5, v6

    .line 215
    :cond_0
    :goto_0
    return-object v5

    .line 139
    :cond_1
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 141
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v9, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 142
    iput-boolean v9, v2, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 143
    invoke-static {p0, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 145
    const/4 v5, 0x0

    .line 148
    .local v5, "xivBrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    new-instance v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .end local v5    # "xivBrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    invoke-direct {v5}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;-><init>()V

    .line 150
    .restart local v5    # "xivBrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    iget v7, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mWidth:I

    .line 151
    iget v7, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mHeight:I

    .line 152
    iput-object p0, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mPathName:Ljava/lang/String;

    .line 153
    iput-boolean p1, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsShareable:Z

    .line 155
    iget v4, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mWidth:I

    .line 156
    .local v4, "width":I
    iget v1, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mHeight:I

    .line 161
    .local v1, "height":I
    iget-object v7, v2, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    if-eqz v7, :cond_2

    .line 162
    iget-object v7, v2, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v8, "image/jpeg"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 163
    if-lez v4, :cond_2

    .line 164
    if-lez v1, :cond_2

    .line 166
    iput-boolean v9, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    .line 172
    :goto_1
    const/4 v7, 0x0

    :try_start_0
    iput-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    .line 173
    iget-boolean v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    if-eqz v7, :cond_3

    .line 174
    new-instance v3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 181
    .local v3, "qOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {p0, p1, v3}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->newInstance(Ljava/lang/String;ZLcom/quramsoft/qrb/QuramBitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    move-result-object v7

    iput-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    .line 192
    iget-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    if-eqz v7, :cond_3

    .line 193
    iget-boolean v7, v3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inCancelingRequested:Z

    if-eqz v7, :cond_3

    .line 194
    iget-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    invoke-virtual {v7}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->recycle()V

    .line 195
    const/4 v7, 0x0

    iput-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v5, v6

    .line 196
    goto :goto_0

    .line 168
    .end local v3    # "qOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    :cond_2
    iput-boolean v10, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    goto :goto_1

    .line 201
    :cond_3
    :try_start_1
    iget-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    if-nez v7, :cond_0

    .line 202
    const/4 v7, 0x0

    iput-boolean v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    .line 203
    invoke-static {p0, p1, p2}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/lang/String;ZZ)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v7

    iput-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    .line 204
    iget-object v7, v5, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v7, :cond_0

    move-object v5, v6

    .line 205
    goto :goto_0

    .line 207
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    move-object v5, v6

    .line 209
    goto :goto_0
.end method

.method public static newInstance([BIIZ)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    .locals 8
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "isShareable"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 300
    if-eqz p0, :cond_0

    array-length v6, p0

    sub-int/2addr v6, p1

    if-gtz v6, :cond_2

    :cond_0
    move-object v4, v5

    .line 329
    :cond_1
    :goto_0
    return-object v4

    .line 302
    :cond_2
    new-instance v4, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    invoke-direct {v4}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;-><init>()V

    .line 304
    .local v4, "xivBrd":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    const/4 v6, 0x3

    :try_start_0
    new-array v1, v6, [I

    .line 306
    .local v1, "imageInfo":[I
    const/4 v6, 0x1

    invoke-static {p0, p1, p2, v1, v6}, Lcom/quramsoft/xiv/XIVCoder;->ParseImageFromMemory([BII[II)I

    move-result v3

    .line 307
    .local v3, "ret":I
    if-ne v3, v7, :cond_4

    .line 308
    const/4 v6, 0x2

    aget v6, v1, v6

    if-ne v6, v7, :cond_4

    const/4 v6, 0x0

    aget v6, v1, v6

    if-lez v6, :cond_4

    const/4 v6, 0x1

    aget v6, v1, v6

    if-lez v6, :cond_4

    .line 309
    const/4 v6, 0x1

    iput-boolean v6, v4, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    .line 314
    :goto_1
    iget-boolean v6, v4, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    if-eqz v6, :cond_3

    .line 315
    new-instance v2, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v2}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 316
    .local v2, "qOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-static {p0, p1, p2, p3, v2}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->newInstance([BIIZLcom/quramsoft/qrb/QuramBitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    move-result-object v6

    iput-object v6, v4, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    .line 318
    .end local v2    # "qOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    :cond_3
    iget-object v6, v4, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    if-nez v6, :cond_1

    .line 319
    const/4 v6, 0x0

    iput-boolean v6, v4, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    .line 320
    invoke-static {p0, p1, p2, p3}, Landroid/graphics/BitmapRegionDecoder;->newInstance([BIIZ)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v6

    iput-object v6, v4, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    .line 321
    iget-object v6, v4, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    if-nez v6, :cond_1

    move-object v4, v5

    .line 322
    goto :goto_0

    .line 311
    :cond_4
    const/4 v6, 0x0

    iput-boolean v6, v4, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 324
    .end local v1    # "imageInfo":[I
    .end local v3    # "ret":I
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    move-object v4, v5

    .line 326
    goto :goto_0
.end method


# virtual methods
.method public cancelDecoding()V
    .locals 1

    .prologue
    .line 483
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    invoke-virtual {v0}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->cancelDecoding()V

    .line 487
    :cond_0
    return-void
.end method

.method public decodeRegion(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "output"    # Landroid/graphics/Bitmap;
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 406
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsDoingDecoding:Z

    .line 408
    const/4 v0, 0x0

    .line 409
    .local v0, "region":Landroid/graphics/Bitmap;
    iget-boolean v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    if-eqz v1, :cond_1

    .line 410
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    invoke-virtual {v1}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 411
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    invoke-virtual {v1, p1, p2, p3}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->decodeRegion(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 419
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsDoingDecoding:Z

    .line 420
    return-object v0

    .line 414
    :cond_1
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v1, :cond_0

    .line 415
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v1, p2, p3}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public decodeRegionEx(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "output"    # Landroid/graphics/Bitmap;
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 425
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsDoingDecoding:Z

    .line 427
    const/4 v0, 0x0

    .line 428
    .local v0, "region":Landroid/graphics/Bitmap;
    iget-boolean v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    if-eqz v1, :cond_1

    .line 429
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    invoke-virtual {v1}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 430
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    invoke-virtual {v1, p1, p2, p3}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->decodeRegionEx(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 438
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsDoingDecoding:Z

    .line 439
    return-object v0

    .line 433
    :cond_1
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v1, :cond_0

    .line 434
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v1, p2, p3}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 491
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->isRecycled:Z

    if-nez v0, :cond_0

    .line 492
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->recycle()V

    .line 493
    :cond_0
    return-void
.end method

.method public freeLargeThumbnail()V
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mLargeThumbnail:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 343
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mLargeThumbnail:Landroid/graphics/Bitmap;

    monitor-enter v1

    .line 344
    :try_start_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mLargeThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 345
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mLargeThumbnail:Landroid/graphics/Bitmap;

    .line 343
    monitor-exit v1

    .line 348
    :cond_0
    return-void

    .line 343
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 367
    iget-boolean v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    if-eqz v1, :cond_1

    .line 368
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    if-eqz v1, :cond_0

    .line 369
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    invoke-virtual {v0}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->getHeight()I

    move-result v0

    .line 376
    :cond_0
    :goto_0
    return v0

    .line 373
    :cond_1
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v1, :cond_0

    .line 374
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getLargeThumbnail()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mLargeThumbnail:Landroid/graphics/Bitmap;

    .line 336
    .local v0, "LTN":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 352
    iget-boolean v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    if-eqz v1, :cond_1

    .line 353
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    if-eqz v1, :cond_0

    .line 354
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    invoke-virtual {v0}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->getWidth()I

    move-result v0

    .line 361
    :cond_0
    :goto_0
    return v0

    .line 358
    :cond_1
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v1, :cond_0

    .line 359
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public isDoingDecoding()Z
    .locals 1

    .prologue
    .line 400
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsDoingDecoding:Z

    return v0
.end method

.method public isPNG()Z
    .locals 2

    .prologue
    .line 474
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mPathName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mPathName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    const/4 v0, 0x1

    .line 478
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isReadyToDecode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 382
    iget-boolean v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    if-eqz v1, :cond_1

    .line 383
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    invoke-virtual {v1}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 392
    :cond_0
    :goto_0
    return v0

    .line 388
    :cond_1
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 392
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRecycled()Z
    .locals 1

    .prologue
    .line 469
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->isRecycled:Z

    return v0
.end method

.method public recycle()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 444
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->isRecycled:Z

    if-eqz v0, :cond_1

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->isRecycled:Z

    .line 448
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mIsUseQrRegionDecoder:Z

    if-eqz v0, :cond_2

    .line 449
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->freeLargeThumbnail()V

    .line 451
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    if-eqz v0, :cond_0

    .line 452
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    monitor-enter v1

    .line 454
    :try_start_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    invoke-virtual {v0}, Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;->recycle()V

    .line 455
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mQrBrd:Lcom/quramsoft/qrb/QuramBitmapRegionDecoderInternal;

    .line 452
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 459
    :cond_2
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    .line 461
    iput-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->mBrd:Landroid/graphics/BitmapRegionDecoder;

    goto :goto_0
.end method
