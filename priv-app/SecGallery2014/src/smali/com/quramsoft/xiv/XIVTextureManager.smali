.class public Lcom/quramsoft/xiv/XIVTextureManager;
.super Ljava/lang/Object;
.source "XIVTextureManager.java"


# static fields
.field static sNextId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x1

    sput v0, Lcom/quramsoft/xiv/XIVTextureManager;->sNextId:I

    .line 23
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized glDeleteBuffers(Ljavax/microedition/khronos/opengles/GL11;I[II)V
    .locals 2
    .param p0, "gl"    # Ljavax/microedition/khronos/opengles/GL11;
    .param p1, "n"    # I
    .param p2, "buffers"    # [I
    .param p3, "offset"    # I

    .prologue
    .line 43
    const-class v0, Lcom/quramsoft/xiv/XIVTextureManager;

    monitor-enter v0

    :try_start_0
    invoke-interface {p0, p1, p2, p3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    monitor-exit v0

    return-void

    .line 43
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized glDeleteFramebuffers(Ljavax/microedition/khronos/opengles/GL11ExtensionPack;I[II)V
    .locals 2
    .param p0, "gl11ep"    # Ljavax/microedition/khronos/opengles/GL11ExtensionPack;
    .param p1, "n"    # I
    .param p2, "buffers"    # [I
    .param p3, "offset"    # I

    .prologue
    .line 48
    const-class v0, Lcom/quramsoft/xiv/XIVTextureManager;

    monitor-enter v0

    :try_start_0
    invoke-interface {p0, p1, p2, p3}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glDeleteFramebuffersOES(I[II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    monitor-exit v0

    return-void

    .line 48
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized glDeleteTextures(Ljavax/microedition/khronos/opengles/GL11;I[II)V
    .locals 2
    .param p0, "gl"    # Ljavax/microedition/khronos/opengles/GL11;
    .param p1, "n"    # I
    .param p2, "textures"    # [I
    .param p3, "offset"    # I

    .prologue
    .line 39
    const-class v0, Lcom/quramsoft/xiv/XIVTextureManager;

    monitor-enter v0

    :try_start_0
    invoke-interface {p0, p1, p2, p3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteTextures(I[II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    monitor-exit v0

    return-void

    .line 39
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized glGenBuffers(I[II)V
    .locals 5
    .param p0, "n"    # I
    .param p1, "buffers"    # [I
    .param p2, "offset"    # I

    .prologue
    .line 33
    const-class v2, Lcom/quramsoft/xiv/XIVTextureManager;

    monitor-enter v2

    move v0, p0

    .end local p0    # "n":I
    .local v0, "n":I
    :goto_0
    add-int/lit8 p0, v0, -0x1

    .end local v0    # "n":I
    .restart local p0    # "n":I
    if-gtz v0, :cond_0

    .line 36
    monitor-exit v2

    return-void

    .line 34
    :cond_0
    add-int v1, p2, p0

    :try_start_0
    sget v3, Lcom/quramsoft/xiv/XIVTextureManager;->sNextId:I

    add-int/lit8 v4, v3, 0x1

    sput v4, Lcom/quramsoft/xiv/XIVTextureManager;->sNextId:I

    aput v3, p1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, p0

    .end local p0    # "n":I
    .restart local v0    # "n":I
    goto :goto_0

    .line 33
    .end local v0    # "n":I
    .restart local p0    # "n":I
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized glGenTextures(I[II)V
    .locals 5
    .param p0, "n"    # I
    .param p1, "textures"    # [I
    .param p2, "offset"    # I

    .prologue
    .line 27
    const-class v2, Lcom/quramsoft/xiv/XIVTextureManager;

    monitor-enter v2

    move v0, p0

    .end local p0    # "n":I
    .local v0, "n":I
    :goto_0
    add-int/lit8 p0, v0, -0x1

    .end local v0    # "n":I
    .restart local p0    # "n":I
    if-gtz v0, :cond_0

    .line 30
    monitor-exit v2

    return-void

    .line 28
    :cond_0
    add-int v1, p2, p0

    :try_start_0
    sget v3, Lcom/quramsoft/xiv/XIVTextureManager;->sNextId:I

    add-int/lit8 v4, v3, 0x1

    sput v4, Lcom/quramsoft/xiv/XIVTextureManager;->sNextId:I

    aput v3, p1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, p0

    .end local p0    # "n":I
    .restart local v0    # "n":I
    goto :goto_0

    .line 27
    .end local v0    # "n":I
    .restart local p0    # "n":I
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method
