.class public Lcom/quramsoft/xiv/XIVCacheManager;
.super Ljava/lang/Object;
.source "XIVCacheManager.java"


# static fields
.field static final CROP_TYPE_CENTER:I = 0x2

.field static final CROP_TYPE_FACE:I = 0x3

.field static final CROP_TYPE_NONE:I = 0x0

.field static final CROP_TYPE_SIGNATURE:I = 0x1

.field private static final DEBUG:Z = false

.field private static final IMAGE_TYPE_QURAMWINK_BMP:I = 0x2

.field private static final IMAGE_TYPE_QURAMWINK_JPEG:I = 0x1

.field static final MICROTHUMBNAIL_SIZE:I

.field static final MINI_MICROTHUMBNAIL_SIZE:I

.field private static final MIN_SIZE_FOR_MAKE_STN_OF_MTN:I

.field private static final TAG:Ljava/lang/String; = "XIVCacheManager"

.field static final THUMBNAIL_SIZE:I

.field static final USE_SKIA_CACHE_MODE:I = 0x0

.field static final USE_WINK_CACHE_MODE:I = 0x1

.field static final USE_XIV_CACHE_MODE:I = 0x2

.field private static final XIV_MSTN_ENC_MODE:I = 0x1

.field private static final XIV_MTN_ENC_MODE:I = 0x1

.field private static final XIV_STN_ENC_MODE:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/quramsoft/xiv/XIVCacheManager;->getTargetSize(I)I

    move-result v0

    sput v0, Lcom/quramsoft/xiv/XIVCacheManager;->THUMBNAIL_SIZE:I

    .line 50
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/quramsoft/xiv/XIVCacheManager;->getTargetSize(I)I

    move-result v0

    sput v0, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    .line 51
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/quramsoft/xiv/XIVCacheManager;->getTargetSize(I)I

    move-result v0

    sput v0, Lcom/quramsoft/xiv/XIVCacheManager;->MINI_MICROTHUMBNAIL_SIZE:I

    .line 66
    sget v0, Lcom/quramsoft/xiv/XIVCacheManager;->THUMBNAIL_SIZE:I

    mul-int/lit8 v0, v0, 0xa

    sput v0, Lcom/quramsoft/xiv/XIVCacheManager;->MIN_SIZE_FOR_MAKE_STN_OF_MTN:I

    .line 42
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static calcCropRect(Landroid/graphics/RectF;IIIILandroid/graphics/RectF;I)V
    .locals 16
    .param p0, "outputRect"    # Landroid/graphics/RectF;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "targetWidth"    # I
    .param p4, "targetHeight"    # I
    .param p5, "faceRect"    # Landroid/graphics/RectF;
    .param p6, "rotation"    # I

    .prologue
    .line 1243
    if-eqz p5, :cond_3

    if-eqz p6, :cond_3

    .line 1244
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/RectF;->width()F

    move-result v11

    .line 1245
    .local v11, "w":F
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/RectF;->height()F

    move-result v7

    .line 1246
    .local v7, "h":F
    move-object/from16 v0, p5

    iget v8, v0, Landroid/graphics/RectF;->left:F

    .line 1247
    .local v8, "left":F
    move-object/from16 v0, p5

    iget v10, v0, Landroid/graphics/RectF;->top:F

    .line 1249
    .local v10, "top":F
    const/16 v12, 0x5a

    move/from16 v0, p6

    if-eq v0, v12, :cond_0

    const/16 v12, 0xb4

    move/from16 v0, p6

    if-ne v0, v12, :cond_1

    .line 1250
    :cond_0
    const/high16 v12, 0x3f800000    # 1.0f

    sub-float/2addr v12, v11

    sub-float v8, v12, v8

    .line 1251
    const/high16 v12, 0x3f800000    # 1.0f

    sub-float/2addr v12, v7

    sub-float v10, v12, v10

    .line 1253
    :cond_1
    move/from16 v0, p6

    rem-int/lit16 v12, v0, 0xb4

    if-eqz v12, :cond_2

    .line 1255
    move v9, v10

    .line 1256
    .local v9, "temp":F
    move v10, v8

    .line 1257
    const/high16 v12, 0x3f800000    # 1.0f

    sub-float/2addr v12, v9

    sub-float v8, v12, v7

    .line 1259
    move v9, v11

    .line 1260
    move v11, v7

    .line 1261
    move v7, v9

    .line 1263
    .end local v9    # "temp":F
    :cond_2
    new-instance p5, Landroid/graphics/RectF;

    .end local p5    # "faceRect":Landroid/graphics/RectF;
    add-float v12, v8, v11

    add-float v13, v10, v7

    move-object/from16 v0, p5

    invoke-direct {v0, v8, v10, v12, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1266
    .end local v7    # "h":F
    .end local v8    # "left":F
    .end local v10    # "top":F
    .end local v11    # "w":F
    .restart local p5    # "faceRect":Landroid/graphics/RectF;
    :cond_3
    move/from16 v0, p6

    rem-int/lit16 v12, v0, 0xb4

    if-eqz v12, :cond_4

    .line 1268
    move/from16 v0, p3

    int-to-float v9, v0

    .line 1269
    .restart local v9    # "temp":F
    move/from16 p3, p4

    .line 1270
    float-to-int v0, v9

    move/from16 p4, v0

    .line 1276
    .end local v9    # "temp":F
    :cond_4
    mul-int v12, p3, p2

    mul-int v13, p4, p1

    if-le v12, v13, :cond_7

    .line 1278
    move/from16 v2, p1

    .line 1279
    .local v2, "cropWidth":I
    mul-int v12, p1, p4

    div-int v1, v12, p3

    .line 1280
    .local v1, "cropHeight":I
    const/4 v3, 0x0

    .line 1281
    .local v3, "cropX":I
    sub-int v12, p2, v1

    div-int/lit8 v4, v12, 0x2

    .line 1289
    .local v4, "cropY":I
    :goto_0
    if-eqz p5, :cond_6

    .line 1291
    move-object/from16 v0, p5

    iget v12, v0, Landroid/graphics/RectF;->left:F

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-ltz v12, :cond_5

    .line 1292
    move-object/from16 v0, p5

    iget v12, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p5

    iget v13, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    move/from16 v0, p1

    int-to-float v13, v0

    mul-float/2addr v12, v13

    float-to-int v5, v12

    .line 1293
    .local v5, "faceCenterH":I
    div-int/lit8 v12, v2, 0x2

    sub-int v3, v5, v12

    .line 1294
    if-gez v3, :cond_8

    .line 1295
    const/4 v3, 0x0

    .line 1300
    .end local v5    # "faceCenterH":I
    :cond_5
    :goto_1
    move-object/from16 v0, p5

    iget v12, v0, Landroid/graphics/RectF;->top:F

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-ltz v12, :cond_6

    .line 1301
    move-object/from16 v0, p5

    iget v12, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p5

    iget v13, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    move/from16 v0, p2

    int-to-float v13, v0

    mul-float/2addr v12, v13

    float-to-int v6, v12

    .line 1302
    .local v6, "faceCenterV":I
    div-int/lit8 v12, v1, 0x2

    sub-int v4, v6, v12

    .line 1303
    if-gez v4, :cond_9

    .line 1304
    const/4 v4, 0x0

    .line 1310
    .end local v6    # "faceCenterV":I
    :cond_6
    :goto_2
    int-to-float v12, v3

    int-to-float v13, v4

    add-int v14, v3, v2

    int-to-float v14, v14

    add-int v15, v4, v1

    int-to-float v15, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13, v14, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1312
    return-void

    .line 1284
    .end local v1    # "cropHeight":I
    .end local v2    # "cropWidth":I
    .end local v3    # "cropX":I
    .end local v4    # "cropY":I
    :cond_7
    move/from16 v1, p2

    .line 1285
    .restart local v1    # "cropHeight":I
    mul-int v12, p2, p3

    div-int v2, v12, p4

    .line 1286
    .restart local v2    # "cropWidth":I
    const/4 v4, 0x0

    .line 1287
    .restart local v4    # "cropY":I
    sub-int v12, p1, v2

    div-int/lit8 v3, v12, 0x2

    .restart local v3    # "cropX":I
    goto :goto_0

    .line 1296
    .restart local v5    # "faceCenterH":I
    :cond_8
    sub-int v12, p1, v2

    if-le v3, v12, :cond_5

    .line 1297
    sub-int v3, p1, v2

    goto :goto_1

    .line 1305
    .end local v5    # "faceCenterH":I
    .restart local v6    # "faceCenterV":I
    :cond_9
    sub-int v12, p2, v1

    if-le v4, v12, :cond_6

    .line 1306
    sub-int v4, p2, v1

    goto :goto_2
.end method

.method public static decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 1052
    iget-object v1, p3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 1053
    invoke-static {p3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->translateBitmapFactoryOptions(Landroid/graphics/BitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v2

    .line 1051
    invoke-static {v1, p0, p1, p2, v2}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeByteArray(Landroid/graphics/Bitmap;[BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1055
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 1058
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v0

    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    invoke-static {p0, p1, p2, p3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;IZ)Landroid/graphics/Bitmap;
    .locals 20
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "targetSize"    # I
    .param p3, "isLarger"    # Z

    .prologue
    .line 931
    if-nez p0, :cond_1

    const/4 v4, 0x0

    .line 988
    :cond_0
    :goto_0
    return-object v4

    .line 933
    :cond_1
    const/4 v4, 0x0

    .line 934
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    const/16 v17, 0x1

    .line 935
    .local v17, "useWINK":Z
    const-wide/16 v14, 0x0

    .local v14, "start":J
    const-wide/16 v6, 0x0

    .line 941
    .local v6, "elapsed":J
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v5, v0, [I

    .line 942
    .local v5, "imageInfo":[I
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v5, v1}, Lcom/quramsoft/xiv/XIVCoder;->ParseImageFromFile(Ljava/lang/String;[II)I

    move-result v9

    .line 943
    .local v9, "ret":I
    const/16 v18, 0x0

    aget v12, v5, v18

    .line 944
    .local v12, "srcWidth":I
    const/16 v18, 0x1

    aget v11, v5, v18

    .line 946
    .local v11, "srcHeight":I
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v9, v0, :cond_3

    .line 947
    const/16 v18, 0x2

    aget v18, v5, v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_2

    .line 948
    const/16 v18, 0x2

    aget v18, v5, v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    .line 949
    :cond_2
    if-lez v12, :cond_3

    if-lez v11, :cond_3

    .line 951
    move/from16 v0, p2

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v12, v11, v0, v1, v2}, Lcom/quramsoft/xiv/XIVUtils;->computeScale(IIIIZ)F

    move-result v10

    .line 955
    .local v10, "scale":F
    const/high16 v18, 0x3f800000    # 1.0f

    cmpl-float v18, v10, v18

    if-lez v18, :cond_4

    .line 956
    move/from16 v16, v12

    .line 957
    .local v16, "thumW":I
    move v13, v11

    .line 963
    .local v13, "thumH":I
    :goto_1
    new-instance v8, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v8}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 964
    .local v8, "option":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    const/16 v18, 0x7

    move/from16 v0, v18

    iput v0, v8, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 965
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v18

    invoke-static {v0, v8, v1, v13, v2}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFileFromThumbnail(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;III)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 972
    .end local v8    # "option":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .end local v10    # "scale":F
    .end local v13    # "thumH":I
    .end local v16    # "thumW":I
    :cond_3
    if-nez v4, :cond_0

    if-eqz p1, :cond_0

    .line 979
    invoke-static/range {p0 .. p1}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_0

    .line 959
    .restart local v10    # "scale":F
    :cond_4
    int-to-float v0, v12

    move/from16 v18, v0

    mul-float v18, v18, v10

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v16

    .line 960
    .restart local v16    # "thumW":I
    int-to-float v0, v11

    move/from16 v18, v0

    mul-float v18, v18, v10

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v13

    .restart local v13    # "thumH":I
    goto :goto_1
.end method

.method public static decodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)Landroid/graphics/Bitmap;
    .locals 17
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "type"    # I

    .prologue
    .line 996
    if-nez p0, :cond_1

    const/4 v2, 0x0

    .line 1047
    :cond_0
    :goto_0
    return-object v2

    .line 998
    :cond_1
    const/4 v2, 0x0

    .line 999
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    const/4 v13, 0x1

    .line 1000
    .local v13, "useWINK":Z
    const-wide/16 v10, 0x0

    .local v10, "start":J
    const-wide/16 v4, 0x0

    .line 1006
    .local v4, "elapsed":J
    const/4 v14, 0x3

    new-array v3, v14, [I

    .line 1007
    .local v3, "imageInfo":[I
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v3, v14}, Lcom/quramsoft/xiv/XIVCoder;->ParseImageFromFile(Ljava/lang/String;[II)I

    move-result v6

    .line 1008
    .local v6, "ret":I
    const/4 v14, 0x1

    if-ne v6, v14, :cond_0

    .line 1009
    const/4 v14, 0x2

    aget v14, v3, v14

    const/4 v15, 0x1

    if-eq v14, v15, :cond_2

    .line 1010
    const/4 v14, 0x2

    aget v14, v3, v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_0

    .line 1012
    :cond_2
    const/4 v7, 0x1

    .line 1013
    .local v7, "sampleSize":I
    invoke-static/range {p2 .. p2}, Lcom/quramsoft/xiv/XIVCacheManager;->getTargetSize(I)I

    move-result v12

    .line 1014
    .local v12, "targetSize":I
    sget v14, Lcom/quramsoft/xiv/XIVCacheManager;->THUMBNAIL_SIZE:I

    if-lt v12, v14, :cond_4

    .line 1015
    const/4 v14, 0x0

    aget v14, v3, v14

    const/4 v15, 0x1

    aget v15, v3, v15

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v12, v12, v0}, Lcom/quramsoft/xiv/XIVUtils;->computeSampleSize(IIIIZ)I

    move-result v7

    .line 1022
    :goto_1
    const/4 v14, 0x0

    aget v14, v3, v14

    int-to-float v14, v14

    int-to-float v15, v7

    div-float/2addr v14, v15

    invoke-static {v14}, Lcom/quramsoft/qrb/QuramBitmapFactory;->round(F)I

    move-result v9

    .line 1023
    .local v9, "sampledWidth":I
    const/4 v14, 0x1

    aget v14, v3, v14

    int-to-float v14, v14

    int-to-float v15, v7

    div-float/2addr v14, v15

    invoke-static {v14}, Lcom/quramsoft/qrb/QuramBitmapFactory;->round(F)I

    move-result v8

    .line 1026
    .local v8, "sampledHeight":I
    if-nez p1, :cond_3

    .line 1027
    new-instance p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .end local p1    # "options":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    invoke-direct/range {p1 .. p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 1030
    .restart local p1    # "options":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    :cond_3
    if-lez v9, :cond_0

    if-lez v8, :cond_0

    .line 1031
    move-object/from16 v0, p1

    iput v7, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 1032
    const/4 v14, 0x7

    move-object/from16 v0, p1

    iput v14, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 1033
    invoke-static/range {p0 .. p1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0

    .line 1017
    .end local v8    # "sampledHeight":I
    .end local v9    # "sampledWidth":I
    :cond_4
    const/4 v14, 0x0

    aget v14, v3, v14

    const/4 v15, 0x1

    aget v15, v3, v15

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-static {v14, v15, v12, v12, v0}, Lcom/quramsoft/xiv/XIVUtils;->computeSampleSize(IIIIZ)I

    move-result v7

    goto :goto_1
.end method

.method public static final encodeCacheData(Landroid/graphics/Bitmap;I)[B
    .locals 17
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "type"    # I

    .prologue
    .line 835
    if-nez p0, :cond_0

    .line 836
    const/4 v2, 0x0

    .line 922
    :goto_0
    return-object v2

    .line 838
    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/quramsoft/xiv/XIVCacheManager;->getCacheEncodingMode(I)I

    move-result v5

    .line 840
    .local v5, "cacheEncodingMode":I
    const/4 v2, 0x0

    check-cast v2, [B

    .line 841
    .local v2, "array":[B
    const/4 v10, 0x0

    .line 842
    .local v10, "is565or8888":Z
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v15

    sget-object v16, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v15

    sget-object v16, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 843
    :cond_1
    const/4 v10, 0x1

    .line 847
    :goto_1
    const-wide/16 v12, 0x0

    .local v12, "start":J
    const-wide/16 v8, 0x0

    .line 851
    .local v8, "elapsed":J
    const/4 v15, 0x2

    if-ne v5, v15, :cond_6

    if-eqz v10, :cond_6

    .line 853
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    .line 854
    .local v14, "width":I
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 855
    .local v7, "height":I
    mul-int v15, v14, v7

    mul-int/lit8 v15, v15, 0x3

    add-int/lit16 v4, v15, 0x400

    .line 856
    .local v4, "bufSize":I
    const/4 v3, 0x0

    check-cast v3, [B

    .line 859
    .local v3, "arrayBuffer":[B
    :try_start_0
    new-array v3, v4, [B
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 864
    const/4 v11, 0x0

    .line 865
    .local v11, "size":I
    sget-boolean v15, Lcom/quramsoft/xiv/XIVConfig;->XIV_PARTIAL_DECODE_WSTN:Z

    if-eqz v15, :cond_5

    .line 866
    const/4 v15, 0x2

    move/from16 v0, p1

    if-ne v0, v15, :cond_5

    .line 867
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVCoder;->EncodeBitmapToWSTN(Landroid/graphics/Bitmap;[B)I

    move-result v11

    .line 872
    :goto_2
    if-lez v11, :cond_3

    .line 873
    new-array v2, v11, [B

    .line 874
    if-le v11, v4, :cond_2

    .line 875
    const-string v15, "XIV"

    const-string v16, "ERROR: encodeCacheData alloc"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    move v11, v4

    .line 879
    :cond_2
    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v3, v15, v2, v0, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 881
    :cond_3
    const/4 v3, 0x0

    check-cast v3, [B

    goto :goto_0

    .line 845
    .end local v3    # "arrayBuffer":[B
    .end local v4    # "bufSize":I
    .end local v7    # "height":I
    .end local v8    # "elapsed":J
    .end local v11    # "size":I
    .end local v12    # "start":J
    .end local v14    # "width":I
    :cond_4
    const/4 v10, 0x0

    goto :goto_1

    .line 860
    .restart local v3    # "arrayBuffer":[B
    .restart local v4    # "bufSize":I
    .restart local v7    # "height":I
    .restart local v8    # "elapsed":J
    .restart local v12    # "start":J
    .restart local v14    # "width":I
    :catch_0
    move-exception v6

    .line 861
    .local v6, "e":Ljava/lang/OutOfMemoryError;
    const/4 v2, 0x0

    goto :goto_0

    .line 869
    .end local v6    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v11    # "size":I
    :cond_5
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/quramsoft/xiv/XIVCoder;->EncodeImage(Landroid/graphics/Bitmap;[B)I

    move-result v11

    goto :goto_2

    .line 912
    .end local v3    # "arrayBuffer":[B
    .end local v4    # "bufSize":I
    .end local v7    # "height":I
    .end local v11    # "size":I
    .end local v14    # "width":I
    :cond_6
    invoke-static/range {p0 .. p0}, Lcom/sec/android/gallery3d/common/BitmapUtils;->compressBitmap(Landroid/graphics/Bitmap;)[B

    move-result-object v2

    goto :goto_0
.end method

.method public static getCacheBitmap(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaItem;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "activity"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    const/4 v6, 0x0

    .line 360
    if-eqz p1, :cond_0

    .line 364
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 365
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v8

    .line 366
    .local v8, "filePath":Ljava/lang/String;
    if-eqz v8, :cond_1

    .line 367
    invoke-interface {p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v7

    .line 368
    .local v7, "drmUtil":Lcom/sec/android/gallery3d/util/ImageDRMUtil;
    if-eqz v7, :cond_1

    .line 369
    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isValidRights(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 391
    .end local v7    # "drmUtil":Lcom/sec/android/gallery3d/util/ImageDRMUtil;
    .end local v8    # "filePath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v6

    .line 376
    :cond_1
    invoke-interface {p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v0

    .line 377
    .local v0, "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    if-eqz v0, :cond_0

    .line 378
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->get()Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    move-result-object v5

    .line 380
    .local v5, "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v2

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ImageCacheService;->getImageData(Lcom/sec/android/gallery3d/data/Path;JILcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z

    move-result v9

    .line 382
    .local v9, "found":Z
    if-eqz v9, :cond_2

    .line 383
    const/4 v1, 0x0

    invoke-static {v1, v5, p2}, Lcom/quramsoft/xiv/XIVCacheManager;->getCacheBitmap(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 387
    .local v6, "cacheBitmap":Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    goto :goto_0

    .line 386
    .end local v6    # "cacheBitmap":Landroid/graphics/Bitmap;
    .end local v9    # "found":Z
    :catchall_0
    move-exception v1

    .line 387
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 388
    throw v1

    .line 387
    .restart local v9    # "found":Z
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    goto :goto_0
.end method

.method public static getCacheBitmap(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;I)Landroid/graphics/Bitmap;
    .locals 21
    .param p0, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p1, "buffer"    # Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    .param p2, "type"    # I

    .prologue
    .line 240
    if-nez p1, :cond_1

    .line 241
    const/4 v5, 0x0

    .line 354
    :cond_0
    :goto_0
    return-object v5

    .line 243
    :cond_1
    invoke-static/range {p2 .. p2}, Lcom/quramsoft/xiv/XIVCacheManager;->getCacheEncodingMode(I)I

    move-result v9

    .line 245
    .local v9, "cacheEncodingMode":I
    const-wide/16 v18, 0x0

    .local v18, "start":J
    const-wide/16 v10, 0x0

    .line 249
    .local v10, "elapsed":J
    const/4 v5, 0x0

    .line 250
    .local v5, "bitmap":Landroid/graphics/Bitmap;
    new-instance v14, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v14}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 251
    .local v14, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v14, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 252
    const/4 v2, 0x1

    iput-boolean v2, v14, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 254
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    if-eqz v2, :cond_0

    move-object/from16 v0, p1

    iget v2, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    if-lez v2, :cond_0

    .line 256
    const/16 v17, 0x0

    .line 257
    .local v17, "ret":I
    packed-switch v9, :pswitch_data_0

    goto :goto_0

    .line 293
    :pswitch_0
    if-eqz p0, :cond_0

    .line 295
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    move-object/from16 v0, p1

    iget v3, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3, v4, v14}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decode(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;[BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 296
    if-nez v5, :cond_0

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 297
    const-string v2, "XIVCacheManager"

    const-string v3, "decode cached failed "

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 261
    :pswitch_1
    const/4 v2, 0x3

    new-array v13, v2, [I

    .line 263
    .local v13, "imageInfo":[I
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    move-object/from16 v0, p1

    iget v3, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    const/4 v6, 0x1

    invoke-static {v2, v3, v4, v13, v6}, Lcom/quramsoft/xiv/XIVCoder;->ParseImageFromMemory([BII[II)I

    move-result v17

    .line 264
    const/4 v2, 0x0

    aget v16, v13, v2

    .line 265
    .local v16, "originalW":I
    const/4 v2, 0x1

    aget v15, v13, v2

    .line 267
    .local v15, "originalH":I
    const/4 v2, 0x1

    move/from16 v0, v17

    if-ne v0, v2, :cond_2

    .line 268
    const/4 v2, 0x2

    aget v2, v13, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    if-lez v16, :cond_2

    if-lez v15, :cond_2

    .line 270
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v16

    invoke-static {v0, v15, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 271
    if-eqz v5, :cond_2

    .line 273
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    move-object/from16 v0, p1

    iget v3, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Lcom/quramsoft/xiv/XIVCoder;->DecodeJPEGFromMemory([BIILandroid/graphics/Bitmap;III)I

    move-result v17

    .line 274
    const/4 v2, 0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_2

    .line 276
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 277
    const/4 v5, 0x0

    .line 282
    :cond_2
    if-nez v5, :cond_0

    if-eqz p0, :cond_0

    .line 284
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    move-object/from16 v0, p1

    iget v3, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3, v4, v14}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decode(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;[BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 285
    if-nez v5, :cond_0

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 286
    const-string v2, "XIVCacheManager"

    const-string v3, "decode cached failed "

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 304
    .end local v13    # "imageInfo":[I
    .end local v15    # "originalH":I
    .end local v16    # "originalW":I
    :pswitch_2
    const/4 v2, 0x1

    new-array v0, v2, [I

    move-object/from16 v20, v0

    .line 305
    .local v20, "width":[I
    const/4 v2, 0x1

    new-array v12, v2, [I

    .line 307
    .local v12, "height":[I
    sget-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_PARTIAL_DECODE_WSTN:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x2

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 309
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    move-object/from16 v0, p1

    iget v3, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    move-object/from16 v0, v20

    invoke-static {v2, v3, v0, v12}, Lcom/quramsoft/xiv/XIVCoder;->ParseWSTN([BI[I[I)I

    move-result v17

    .line 310
    const/4 v2, 0x1

    move/from16 v0, v17

    if-ne v0, v2, :cond_0

    const/4 v2, 0x0

    aget v2, v20, v2

    if-lez v2, :cond_0

    const/4 v2, 0x0

    aget v2, v12, v2

    if-lez v2, :cond_0

    .line 312
    const/4 v2, 0x0

    aget v2, v20, v2

    const/4 v3, 0x0

    aget v3, v12, v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 313
    if-eqz v5, :cond_0

    .line 316
    move-object/from16 v0, p1

    iget v2, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    move-object/from16 v0, p1

    iget v4, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    const/4 v6, 0x0

    .line 315
    invoke-static {v5, v2, v3, v4, v6}, Lcom/quramsoft/xiv/XIVCoder;->DecodeWSTNToBitmap(Landroid/graphics/Bitmap;I[BIZ)I

    move-result v17

    .line 317
    const/4 v2, 0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_0

    .line 319
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 320
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 325
    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    move-object/from16 v0, p1

    iget v3, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    move-object/from16 v0, v20

    invoke-static {v2, v3, v0, v12}, Lcom/quramsoft/xiv/XIVCoder;->ParseImageOffset([BI[I[I)I

    move-result v17

    .line 326
    const/4 v2, 0x1

    move/from16 v0, v17

    if-ne v0, v2, :cond_0

    const/4 v2, 0x0

    aget v2, v20, v2

    if-lez v2, :cond_0

    const/4 v2, 0x0

    aget v2, v12, v2

    if-lez v2, :cond_0

    .line 328
    const/4 v2, 0x0

    aget v2, v20, v2

    const/4 v3, 0x0

    aget v3, v12, v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 329
    if-eqz v5, :cond_0

    .line 332
    move-object/from16 v0, p1

    iget v2, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    move-object/from16 v0, p1

    iget v4, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    .line 331
    invoke-static {v5, v2, v3, v4}, Lcom/quramsoft/xiv/XIVCoder;->DecodeImageOffset(Landroid/graphics/Bitmap;I[BI)I

    move-result v17

    .line 333
    const/4 v2, 0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_0

    .line 335
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 336
    const/4 v5, 0x0

    .line 341
    goto/16 :goto_0

    .line 257
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getCacheBuffer(Lcom/quramsoft/xiv/XIV;Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    .locals 8
    .param p0, "xiv"    # Lcom/quramsoft/xiv/XIV;
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    const/4 v7, 0x0

    .line 155
    if-eqz p1, :cond_1

    .line 159
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->getGalleryApp()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v0

    .line 160
    .local v0, "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    if-eqz v0, :cond_1

    .line 161
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->get()Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    move-result-object v5

    .line 164
    .local v5, "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getModifiedDateInSec()J

    move-result-wide v2

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ImageCacheService;->getImageData(Lcom/sec/android/gallery3d/data/Path;JILcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z

    move-result v6

    .line 166
    .local v6, "found":Z
    if-eqz v6, :cond_0

    .line 167
    const/4 v1, 0x0

    invoke-static {p0, v1, v5, p2}, Lcom/quramsoft/xiv/XIVCacheManager;->getCacheBuffer(Lcom/quramsoft/xiv/XIV;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;I)Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 170
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 174
    .end local v0    # "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    .end local v5    # "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    .end local v6    # "found":Z
    :goto_0
    return-object v1

    .line 169
    .restart local v0    # "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    .restart local v5    # "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    :catchall_0
    move-exception v1

    .line 170
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 171
    throw v1

    .line 170
    .restart local v6    # "found":Z
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .end local v0    # "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    .end local v5    # "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    .end local v6    # "found":Z
    :cond_1
    move-object v1, v7

    .line 174
    goto :goto_0
.end method

.method public static getCacheBuffer(Lcom/quramsoft/xiv/XIV;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;I)Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    .locals 13
    .param p0, "xiv"    # Lcom/quramsoft/xiv/XIV;
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "buffer"    # Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    .param p3, "type"    # I

    .prologue
    .line 183
    if-eqz p0, :cond_0

    if-nez p2, :cond_2

    :cond_0
    const/4 v0, 0x0

    .line 232
    :cond_1
    :goto_0
    :pswitch_0
    return-object v0

    .line 185
    :cond_2
    invoke-static/range {p3 .. p3}, Lcom/quramsoft/xiv/XIVCacheManager;->getCacheEncodingMode(I)I

    move-result v1

    .line 187
    .local v1, "cacheEncodingMode":I
    const-wide/16 v8, 0x0

    .local v8, "start":J
    const-wide/16 v2, 0x0

    .line 191
    .local v2, "elapsed":J
    new-instance v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    invoke-direct {v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;-><init>()V

    .line 192
    .local v0, "bufferData":Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 193
    .local v5, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v7, v5, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 194
    const/4 v7, 0x1

    iput-boolean v7, v5, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 196
    iget-object v7, p2, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    if-eqz v7, :cond_1

    iget v7, p2, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    if-lez v7, :cond_1

    .line 198
    const/4 v6, 0x0

    .line 199
    .local v6, "ret":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 203
    :pswitch_1
    const/4 v7, 0x3

    new-array v4, v7, [I

    .line 205
    .local v4, "imageInfo":[I
    iget-object v7, p2, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    iget v10, p2, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    iget v11, p2, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    const/4 v12, 0x1

    invoke-static {v7, v10, v11, v4, v12}, Lcom/quramsoft/xiv/XIVCoder;->ParseImageFromMemory([BII[II)I

    move-result v6

    .line 206
    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    .line 207
    const/4 v7, 0x2

    aget v7, v4, v7

    const/4 v10, 0x1

    if-ne v7, v10, :cond_1

    const/4 v7, 0x0

    aget v7, v4, v7

    if-lez v7, :cond_1

    const/4 v7, 0x1

    aget v7, v4, v7

    if-lez v7, :cond_1

    .line 209
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->getBufferReuseManager()Lcom/quramsoft/xiv/XIVBufferReuseManager;

    move-result-object v7

    const/4 v10, 0x2

    invoke-virtual {v7, v10}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->getBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    iput-object v7, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    .line 211
    iget-object v7, p2, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    iget v10, p2, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    iget v11, p2, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    .line 212
    invoke-static {v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->translateBitmapFactoryOptions(Landroid/graphics/BitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v12

    .line 210
    invoke-static {v7, v10, v11, v0, v12}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeThumbnailByteArrayToBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v6

    .line 214
    goto :goto_0

    .line 199
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static final getCacheEncodingMode(I)I
    .locals 1
    .param p0, "type"    # I

    .prologue
    const/4 v0, 0x1

    .line 80
    packed-switch p0, :pswitch_data_0

    .line 89
    const/4 v0, 0x0

    :pswitch_0
    return v0

    .line 80
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getCropBitmap(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/graphics/Bitmap;
    .locals 39
    .param p0, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p1, "buffer"    # Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 1081
    new-instance v4, Landroid/graphics/RectF;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v4, v7, v8, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1082
    .local v4, "cropRect":Landroid/graphics/RectF;
    sget v37, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    .line 1083
    .local v37, "targetSize":I
    move/from16 v38, v37

    .line 1084
    .local v38, "targetW":I
    move/from16 v36, v37

    .line 1085
    .local v36, "targetH":I
    const/16 v18, 0x0

    .line 1086
    .local v18, "StartX":F
    const/16 v19, 0x0

    .line 1087
    .local v19, "StartY":F
    const/high16 v32, 0x3f800000    # 1.0f

    .line 1089
    .local v32, "scale":F
    if-nez p1, :cond_1

    .line 1090
    const/16 v20, 0x0

    .line 1237
    .end local p2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    :goto_0
    return-object v20

    .line 1092
    .restart local p2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    invoke-static/range {p2 .. p2}, Lcom/quramsoft/xiv/XIVCacheManager;->getCropType(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v22

    .line 1094
    .local v22, "cropType":I
    const-wide/16 v34, 0x0

    .local v34, "start":J
    const-wide/16 v24, 0x0

    .line 1098
    .local v24, "elapsed":J
    const/16 v20, 0x0

    .line 1099
    .local v20, "bitmap":Landroid/graphics/Bitmap;
    new-instance v27, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v27 .. v27}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1100
    .local v27, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v27

    iput-object v7, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1101
    const/4 v7, 0x1

    move-object/from16 v0, v27

    iput-boolean v7, v0, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 1103
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    if-eqz v7, :cond_0

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    array-length v7, v7

    move-object/from16 v0, p1

    iget v8, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    sub-int/2addr v7, v8

    if-lez v7, :cond_0

    .line 1104
    const/16 v31, 0x0

    .line 1105
    .local v31, "ret":I
    const/4 v7, 0x3

    new-array v0, v7, [I

    move-object/from16 v23, v0

    .line 1106
    .local v23, "imageInfo":[I
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    move-object/from16 v0, p1

    iget v8, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    .line 1107
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    array-length v10, v10

    move-object/from16 v0, p1

    iget v11, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    sub-int/2addr v10, v11

    const/4 v11, 0x1

    .line 1106
    move-object/from16 v0, v23

    invoke-static {v7, v8, v10, v0, v11}, Lcom/quramsoft/xiv/XIVCoder;->ParseImageFromMemory([BII[II)I

    move-result v31

    .line 1108
    const/4 v7, 0x0

    aget v5, v23, v7

    .line 1109
    .local v5, "originalW":I
    const/4 v7, 0x1

    aget v6, v23, v7

    .line 1111
    .local v6, "originalH":I
    const/4 v7, 0x1

    move/from16 v0, v31

    if-ne v0, v7, :cond_2

    .line 1112
    if-ltz v5, :cond_2

    if-ltz v6, :cond_2

    const/4 v7, 0x2

    aget v7, v23, v7

    const/4 v8, 0x1

    if-eq v7, v8, :cond_3

    .line 1114
    :cond_2
    const/16 v20, 0x0

    goto :goto_0

    .line 1117
    :cond_3
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v26

    .line 1118
    .local v26, "minSide":I
    if-ne v5, v6, :cond_4

    sget v7, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    move/from16 v0, v26

    if-gt v0, v7, :cond_4

    .line 1119
    const/16 v22, 0x0

    .line 1121
    :cond_4
    packed-switch v22, :pswitch_data_0

    .line 1201
    .end local p2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_1
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    .line 1202
    move-object/from16 v0, p1

    iget v11, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    array-length v7, v7

    move-object/from16 v0, p1

    iget v8, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    sub-int v12, v7, v8

    .line 1204
    invoke-static/range {v27 .. v27}, Lcom/quramsoft/qrb/QuramBitmapFactory;->translateBitmapFactoryOptions(Landroid/graphics/BitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v13

    .line 1205
    iget v7, v4, Landroid/graphics/RectF;->left:F

    float-to-int v14, v7

    iget v7, v4, Landroid/graphics/RectF;->right:F

    float-to-int v15, v7

    .line 1206
    iget v7, v4, Landroid/graphics/RectF;->top:F

    float-to-int v0, v7

    move/from16 v16, v0

    iget v7, v4, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v7

    move/from16 v17, v0

    .line 1201
    invoke-static/range {v10 .. v17}, Lcom/quramsoft/qrb/QuramBitmapFactory;->partialDecodeByteArray([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;IIII)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 1208
    const/4 v7, 0x0

    cmpg-float v7, v18, v7

    if-ltz v7, :cond_5

    const/4 v7, 0x0

    cmpg-float v7, v19, v7

    if-gez v7, :cond_0

    .line 1209
    :cond_5
    int-to-float v7, v5

    const/high16 v8, 0x40000000    # 2.0f

    mul-float v8, v8, v18

    sub-float/2addr v7, v8

    float-to-int v0, v7

    move/from16 v29, v0

    .line 1210
    .local v29, "pad_width":I
    move/from16 v0, v29

    if-ge v0, v5, :cond_6

    .line 1211
    move/from16 v29, v5

    .line 1213
    :cond_6
    int-to-float v7, v6

    const/high16 v8, 0x40000000    # 2.0f

    mul-float v8, v8, v19

    sub-float/2addr v7, v8

    float-to-int v0, v7

    move/from16 v28, v0

    .line 1214
    .local v28, "pad_height":I
    move/from16 v0, v28

    if-ge v0, v6, :cond_7

    .line 1215
    move/from16 v28, v6

    .line 1217
    :cond_7
    move-object/from16 v0, v27

    iget-object v7, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v29

    move/from16 v1, v28

    invoke-static {v0, v1, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v33

    .line 1218
    .local v33, "target":Landroid/graphics/Bitmap;
    new-instance v21, Landroid/graphics/Canvas;

    move-object/from16 v0, v21

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1219
    .local v21, "canvas":Landroid/graphics/Canvas;
    sub-int v7, v29, v5

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    sub-int v8, v28, v6

    int-to-float v8, v8

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v8, v10

    move-object/from16 v0, v21

    invoke-virtual {v0, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1220
    new-instance v30, Landroid/graphics/Paint;

    const/4 v7, 0x6

    move-object/from16 v0, v30

    invoke-direct {v0, v7}, Landroid/graphics/Paint;-><init>(I)V

    .line 1221
    .local v30, "paint":Landroid/graphics/Paint;
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v7, v8, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1223
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->recycle()V

    move-object/from16 v20, v33

    .line 1225
    goto/16 :goto_0

    .line 1123
    .end local v21    # "canvas":Landroid/graphics/Canvas;
    .end local v28    # "pad_height":I
    .end local v29    # "pad_width":I
    .end local v30    # "paint":Landroid/graphics/Paint;
    .end local v33    # "target":Landroid/graphics/Bitmap;
    .restart local p2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :pswitch_0
    sget v7, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    int-to-float v7, v7

    int-to-float v8, v5

    div-float/2addr v7, v8

    .line 1124
    sget v8, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    int-to-float v8, v8

    int-to-float v10, v6

    div-float/2addr v8, v10

    .line 1123
    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v32

    .line 1126
    int-to-float v7, v5

    mul-float v7, v7, v32

    float-to-int v0, v7

    move/from16 v38, v0

    .line 1127
    int-to-float v7, v6

    mul-float v7, v7, v32

    float-to-int v0, v7

    move/from16 v36, v0

    .line 1129
    sub-int v7, v38, v37

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float v18, v7, v8

    .line 1130
    sub-int v7, v36, v37

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float v19, v7, v8

    .line 1132
    div-float v18, v18, v32

    .line 1133
    div-float v19, v19, v32

    .line 1136
    move/from16 v0, v18

    iput v0, v4, Landroid/graphics/RectF;->left:F

    .line 1137
    int-to-float v7, v5

    iget v8, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v8

    iput v7, v4, Landroid/graphics/RectF;->right:F

    .line 1138
    move/from16 v0, v19

    iput v0, v4, Landroid/graphics/RectF;->top:F

    .line 1139
    int-to-float v7, v6

    iget v8, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v7, v8

    iput v7, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 1143
    :pswitch_1
    check-cast p2, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local p2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v7, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces(Z)Landroid/graphics/RectF;

    move-result-object v9

    .line 1145
    .local v9, "faceRect":Landroid/graphics/RectF;
    sget v7, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    int-to-float v7, v7

    int-to-float v8, v5

    div-float/2addr v7, v8

    .line 1146
    sget v8, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    int-to-float v8, v8

    int-to-float v10, v6

    div-float/2addr v8, v10

    .line 1145
    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v32

    .line 1148
    int-to-float v7, v5

    mul-float v7, v7, v32

    float-to-int v0, v7

    move/from16 v38, v0

    .line 1149
    int-to-float v7, v6

    mul-float v7, v7, v32

    float-to-int v0, v7

    move/from16 v36, v0

    .line 1151
    sub-int v7, v38, v37

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float v18, v7, v8

    .line 1152
    sub-int v7, v36, v37

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float v19, v7, v8

    .line 1154
    div-float v18, v18, v32

    .line 1155
    div-float v19, v19, v32

    .line 1157
    move/from16 v0, v18

    iput v0, v4, Landroid/graphics/RectF;->left:F

    .line 1158
    int-to-float v7, v5

    iget v8, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v8

    iput v7, v4, Landroid/graphics/RectF;->right:F

    .line 1159
    move/from16 v0, v19

    iput v0, v4, Landroid/graphics/RectF;->top:F

    .line 1160
    int-to-float v7, v6

    iget v8, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v7, v8

    iput v7, v4, Landroid/graphics/RectF;->bottom:F

    .line 1163
    sget v7, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    sget v8, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    const/4 v10, 0x0

    .line 1162
    invoke-static/range {v4 .. v10}, Lcom/quramsoft/xiv/XIVCacheManager;->calcCropRect(Landroid/graphics/RectF;IIIILandroid/graphics/RectF;I)V

    goto/16 :goto_1

    .line 1168
    .end local v9    # "faceRect":Landroid/graphics/RectF;
    .restart local p2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :pswitch_2
    sget v38, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    .line 1169
    sget v7, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    mul-int/lit8 v7, v7, 0x3

    div-int/lit8 v36, v7, 0x4

    .line 1171
    move/from16 v0, v38

    int-to-float v7, v0

    const v8, 0x3f59999a    # 0.85f

    mul-float/2addr v7, v8

    int-to-float v8, v5

    div-float/2addr v7, v8

    .line 1172
    move/from16 v0, v36

    int-to-float v8, v0

    const v10, 0x3f59999a    # 0.85f

    mul-float/2addr v8, v10

    int-to-float v10, v6

    div-float/2addr v8, v10

    .line 1171
    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v32

    .line 1174
    int-to-float v7, v5

    mul-float v7, v7, v32

    float-to-int v0, v7

    move/from16 v38, v0

    .line 1175
    int-to-float v7, v6

    mul-float v7, v7, v32

    float-to-int v0, v7

    move/from16 v36, v0

    .line 1177
    sget v7, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    sub-int v7, v38, v7

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float v18, v7, v8

    .line 1178
    sget v7, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    mul-int/lit8 v7, v7, 0x3

    div-int/lit8 v7, v7, 0x4

    sub-int v7, v36, v7

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float v19, v7, v8

    .line 1180
    div-float v18, v18, v32

    .line 1181
    div-float v19, v19, v32

    .line 1184
    move/from16 v0, v18

    iput v0, v4, Landroid/graphics/RectF;->left:F

    .line 1185
    iget v7, v4, Landroid/graphics/RectF;->left:F

    const/4 v8, 0x0

    cmpg-float v7, v7, v8

    if-gez v7, :cond_8

    .line 1186
    const/4 v7, 0x0

    iput v7, v4, Landroid/graphics/RectF;->left:F

    .line 1187
    :cond_8
    int-to-float v7, v5

    iget v8, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v8

    iput v7, v4, Landroid/graphics/RectF;->right:F

    .line 1189
    move/from16 v0, v19

    iput v0, v4, Landroid/graphics/RectF;->top:F

    .line 1190
    iget v7, v4, Landroid/graphics/RectF;->top:F

    const/4 v8, 0x0

    cmpg-float v7, v7, v8

    if-gez v7, :cond_9

    .line 1191
    const/4 v7, 0x0

    iput v7, v4, Landroid/graphics/RectF;->top:F

    .line 1192
    :cond_9
    int-to-float v7, v6

    iget v8, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v7, v8

    iput v7, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 1196
    :pswitch_3
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    move-object/from16 v0, p1

    iget v8, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    move-object/from16 v0, p1

    iget v10, v0, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-static {v0, v7, v8, v10, v1}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodeUsingPool(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;[BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 1197
    goto/16 :goto_0

    .line 1121
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static final getCropType(Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 5
    .param p0, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 95
    const/4 v2, 0x2

    .line 97
    .local v2, "ret":I
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSignatureFile(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 98
    const/4 v2, 0x1

    .line 118
    .end local p0    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    :goto_0
    return v2

    .line 100
    .restart local p0    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 101
    const/4 v2, 0x2

    goto :goto_0

    .line 104
    :cond_2
    instance-of v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v3, :cond_0

    move-object v3, p0

    .line 105
    check-cast v3, Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v3, v3, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    if-eqz v3, :cond_3

    move-object v3, p0

    .line 106
    check-cast v3, Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v3, v3, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    const-string v4, ".BestPic"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move-object v3, p0

    .line 107
    check-cast v3, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalImage;->getFaceCount()I

    move-result v0

    .line 108
    .local v0, "faceCount":I
    if-lez v0, :cond_0

    move-object v3, p0

    .line 109
    check-cast v3, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalImage;->loadRectOfAllFaces()V

    .line 110
    check-cast p0, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local p0    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces(Z)Landroid/graphics/RectF;

    move-result-object v1

    .line 111
    .local v1, "rect":Landroid/graphics/RectF;
    if-eqz v1, :cond_0

    .line 112
    const/4 v2, 0x3

    goto :goto_0
.end method

.method public static getExifBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 750
    const/4 v0, 0x0

    .line 752
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .local v2, "exif":Landroid/media/ExifInterface;
    move-object v4, v5

    .line 753
    check-cast v4, [B

    .line 754
    .local v4, "thumbnail":[B
    if-eqz p0, :cond_0

    .line 756
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    move-object v2, v3

    .line 760
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/media/ExifInterface;->hasThumbnail()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 761
    invoke-virtual {v2}, Landroid/media/ExifInterface;->getThumbnail()[B

    move-result-object v4

    .line 764
    :cond_0
    if-eqz v4, :cond_1

    .line 765
    const/4 v6, 0x0

    array-length v7, v4

    invoke-static {v4, v6, v7}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v4, v5

    .line 766
    check-cast v4, [B

    .line 769
    :cond_1
    return-object v0

    .line 757
    :catch_0
    move-exception v1

    .line 758
    .local v1, "ex":Ljava/io/IOException;
    const-string v6, "XIVCacheManager"

    const-string v7, "cannot read exif"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getImageBufferDataFromBitmap(Landroid/graphics/Bitmap;Ljava/nio/ByteBuffer;)Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    .locals 2
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 137
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 138
    new-instance v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    invoke-direct {v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;-><init>()V

    .line 139
    .local v0, "bufferData":Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->width:I

    .line 140
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->height:I

    .line 141
    iput-object p1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    .line 143
    iget-object v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    invoke-static {p0, v1}, Lcom/quramsoft/xiv/XIVCoder;->MakeBufferFromBitmap(Landroid/graphics/Bitmap;Ljava/nio/Buffer;)I

    .line 147
    .end local v0    # "bufferData":Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final getTargetSize(I)I
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 75
    invoke-static {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getTargetSize(I)I

    move-result v0

    return v0
.end method

.method public static hasCache(Lcom/quramsoft/xiv/XIV;Lcom/sec/android/gallery3d/data/MediaItem;I)Z
    .locals 7
    .param p0, "xiv"    # Lcom/quramsoft/xiv/XIV;
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIV;->getGalleryApp()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v0

    .line 125
    .local v0, "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    const/4 v6, 0x0

    .line 126
    .local v6, "found":Z
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->get()Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    move-result-object v5

    .line 128
    .local v5, "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    if-eqz v0, :cond_0

    .line 129
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v2

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ImageCacheService;->getImageData(Lcom/sec/android/gallery3d/data/Path;JILcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 131
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 133
    return v6

    .line 130
    :catchall_0
    move-exception v1

    .line 131
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 132
    throw v1
.end method

.method static makeCacheBitmap(Ljava/lang/String;ILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "type"    # I
    .param p2, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    .line 631
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 632
    .local v2, "opts":Landroid/graphics/BitmapFactory$Options;
    const/4 v11, 0x1

    iput-boolean v11, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 633
    const/4 v11, 0x1

    iput-boolean v11, v2, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 634
    invoke-static {p0, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 636
    iget-object v11, v2, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    if-eqz v11, :cond_2

    iget-object v11, v2, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v12, "image/jpeg"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v1, 0x1

    .line 638
    .local v1, "isJpeg":Z
    :goto_0
    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 639
    .local v4, "srcWidth":I
    iget v3, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 641
    .local v3, "srcHeight":I
    iget-object v11, v2, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    if-eqz v11, :cond_0

    iget-object v11, v2, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v12, "image/jpeg"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 642
    :cond_0
    const/4 v0, 0x0

    .line 726
    :cond_1
    :goto_1
    return-object v0

    .line 636
    .end local v1    # "isJpeg":Z
    .end local v3    # "srcHeight":I
    .end local v4    # "srcWidth":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 645
    .restart local v1    # "isJpeg":Z
    .restart local v3    # "srcHeight":I
    .restart local v4    # "srcWidth":I
    :cond_3
    const/4 v10, 0x0

    .line 646
    .local v10, "useMakeSTNOfMTN":Z
    const/4 v8, 0x0

    .line 659
    .local v8, "typeOfFirstDecoding":I
    move v8, p1

    .line 663
    invoke-static {v8}, Lcom/quramsoft/xiv/XIVCacheManager;->getTargetSize(I)I

    move-result v5

    .line 665
    .local v5, "targetSize":I
    const/4 v9, 0x1

    .line 667
    .local v9, "useFastResize":Z
    if-nez v1, :cond_5

    .line 668
    const/4 v9, 0x0

    .line 683
    :cond_4
    :goto_2
    const-wide/16 v6, 0x0

    .line 687
    .local v6, "start":J
    const/4 v0, 0x0

    .line 700
    .local v0, "cacheBitmap":Landroid/graphics/Bitmap;
    invoke-static {p0, p2, p1}, Lcom/quramsoft/xiv/XIVCacheManager;->decodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 702
    if-eqz v0, :cond_1

    .line 704
    const/4 v11, 0x2

    if-ne v8, v11, :cond_9

    .line 706
    sget-boolean v11, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_WSTN:Z

    if-nez v11, :cond_1

    .line 708
    const/4 v11, 0x1

    invoke-static {v0, v5, v11}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 707
    goto :goto_1

    .line 670
    .end local v0    # "cacheBitmap":Landroid/graphics/Bitmap;
    .end local v6    # "start":J
    :cond_5
    const/4 v11, 0x2

    if-ne v8, v11, :cond_7

    .line 672
    if-lt v4, v5, :cond_6

    if-ge v3, v5, :cond_4

    .line 673
    :cond_6
    const/4 v9, 0x0

    goto :goto_2

    .line 675
    :cond_7
    const/4 v11, 0x1

    if-ne v8, v11, :cond_8

    .line 677
    if-ge v4, v5, :cond_4

    if-ge v3, v5, :cond_4

    .line 678
    const/4 v9, 0x0

    goto :goto_2

    .line 681
    :cond_8
    const/4 v9, 0x0

    goto :goto_2

    .line 712
    .restart local v0    # "cacheBitmap":Landroid/graphics/Bitmap;
    .restart local v6    # "start":J
    :cond_9
    const/4 v11, 0x1

    invoke-static {v0, v5, v11}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownBySideLength(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 711
    goto :goto_1
.end method

.method public static makeCacheBitmapUsingFastResize(Ljava/lang/String;Lcom/sec/android/gallery3d/data/Path;III)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "srcWidth"    # I
    .param p3, "srcHeight"    # I
    .param p4, "type"    # I

    .prologue
    .line 779
    if-nez p0, :cond_1

    .line 780
    const/4 v1, 0x0

    .line 828
    :cond_0
    :goto_0
    return-object v1

    .line 782
    :cond_1
    invoke-static {p4}, Lcom/quramsoft/xiv/XIVCacheManager;->getTargetSize(I)I

    move-result v5

    .line 784
    .local v5, "targetSize":I
    const/4 v7, 0x0

    .line 785
    .local v7, "thumnail_width":I
    const/4 v6, 0x0

    .line 788
    .local v6, "thumnail_height":I
    const/4 v8, 0x2

    if-ne p4, v8, :cond_4

    .line 790
    invoke-static {p1}, Lcom/quramsoft/xiv/XIVUtils;->isVerticalCrop(Lcom/sec/android/gallery3d/data/Path;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 791
    const/high16 v8, 0x43160000    # 150.0f

    int-to-float v9, p2

    div-float v4, v8, v9

    .line 792
    .local v4, "scaleW":F
    const/high16 v8, 0x43480000    # 200.0f

    int-to-float v9, p3

    div-float v3, v8, v9

    .line 798
    .local v3, "scaleH":F
    :goto_1
    const/high16 v8, 0x43480000    # 200.0f

    int-to-float v9, p2

    div-float v4, v8, v9

    .line 799
    const/high16 v8, 0x43160000    # 150.0f

    int-to-float v9, p3

    div-float v3, v8, v9

    .line 800
    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 807
    .local v2, "scale":F
    :goto_2
    int-to-float v8, p2

    mul-float/2addr v8, v2

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 808
    int-to-float v8, p3

    mul-float/2addr v8, v2

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 810
    if-lez v7, :cond_2

    if-gtz v6, :cond_5

    .line 811
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 794
    .end local v2    # "scale":F
    .end local v3    # "scaleH":F
    .end local v4    # "scaleW":F
    :cond_3
    const/high16 v8, 0x43480000    # 200.0f

    int-to-float v9, p2

    div-float v4, v8, v9

    .line 795
    .restart local v4    # "scaleW":F
    const/high16 v8, 0x43160000    # 150.0f

    int-to-float v9, p3

    div-float v3, v8, v9

    .restart local v3    # "scaleH":F
    goto :goto_1

    .line 802
    .end local v3    # "scaleH":F
    .end local v4    # "scaleW":F
    :cond_4
    int-to-float v8, v5

    int-to-float v9, p2

    div-float v4, v8, v9

    .line 803
    .restart local v4    # "scaleW":F
    int-to-float v8, v5

    int-to-float v9, p3

    div-float v3, v8, v9

    .line 804
    .restart local v3    # "scaleH":F
    invoke-static {v4, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .restart local v2    # "scale":F
    goto :goto_2

    .line 814
    :cond_5
    new-instance v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 815
    .local v0, "Opts":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    const/4 v8, 0x7

    iput v8, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 818
    const/4 v8, 0x0

    .line 816
    invoke-static {p0, v0, v7, v6, v8}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFileFromThumbnail(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;III)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 821
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    const/4 v8, 0x3

    if-eq p4, v8, :cond_6

    .line 822
    sget-boolean v8, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_WSTN:Z

    if-nez v8, :cond_0

    const/4 v8, 0x2

    if-ne p4, v8, :cond_0

    if-eqz v1, :cond_0

    .line 825
    :cond_6
    const/4 v8, 0x1

    .line 824
    invoke-static {v1, v5, v8}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public static makeMTNOfLTN(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "LTN"    # Landroid/graphics/Bitmap;

    .prologue
    .line 731
    sget v0, Lcom/quramsoft/xiv/XIVCacheManager;->THUMBNAIL_SIZE:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownBySideLength(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static partialDecodeByteArray([BIILandroid/graphics/BitmapFactory$Options;IIII)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "opts"    # Landroid/graphics/BitmapFactory$Options;
    .param p4, "left"    # I
    .param p5, "right"    # I
    .param p6, "top"    # I
    .param p7, "bottom"    # I

    .prologue
    .line 1064
    if-nez p3, :cond_0

    new-instance p3, Landroid/graphics/BitmapFactory$Options;

    .end local p3    # "opts":Landroid/graphics/BitmapFactory$Options;
    invoke-direct {p3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1067
    .restart local p3    # "opts":Landroid/graphics/BitmapFactory$Options;
    :cond_0
    iget-object v0, p3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 1068
    invoke-static {p3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->translateBitmapFactoryOptions(Landroid/graphics/BitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    move-result-object v4

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    .line 1066
    invoke-static/range {v0 .. v8}, Lcom/quramsoft/qrb/QuramBitmapFactory;->partialDecodeByteArray(Landroid/graphics/Bitmap;[BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;IIII)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1071
    .local v9, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v9, :cond_1

    .line 1073
    .end local v9    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v9

    .restart local v9    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    invoke-static {p0, p1, p2, p3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v9

    goto :goto_0
.end method

.method public static writeCache(Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/data/Path;JILcom/sec/android/gallery3d/data/ImageCacheService;)V
    .locals 6
    .param p0, "cacheBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "dateTakenInMs"    # J
    .param p4, "type"    # I
    .param p5, "cacheService"    # Lcom/sec/android/gallery3d/data/ImageCacheService;

    .prologue
    .line 742
    .line 741
    invoke-static {p0, p4}, Lcom/quramsoft/xiv/XIVCacheManager;->encodeCacheData(Landroid/graphics/Bitmap;I)[B

    move-result-object v5

    .line 744
    .local v5, "array":[B
    if-eqz v5, :cond_0

    move-object v0, p5

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    .line 745
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ImageCacheService;->putImageData(Lcom/sec/android/gallery3d/data/Path;JI[B)V

    .line 746
    :cond_0
    return-void
.end method
