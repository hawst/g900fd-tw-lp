.class public Lcom/quramsoft/xiv/XIVBitmapReuseManager;
.super Ljava/lang/Object;
.source "XIVBitmapReuseManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;
    }
.end annotation


# static fields
.field public static final CONTAINER_TYPE_RD:I = 0x3

.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "XIVBitmapReuseManager"


# instance fields
.field private sRDContainer:Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method static final create()Lcom/quramsoft/xiv/XIVBitmapReuseManager;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/quramsoft/xiv/XIVBitmapReuseManager;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;-><init>()V

    .line 20
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVBitmapReuseManager;
    return-object v0
.end method


# virtual methods
.method createBitmapContainer(III)Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "limit"    # I

    .prologue
    .line 100
    new-instance v0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;

    invoke-direct {v0, p0}, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;-><init>(Lcom/quramsoft/xiv/XIVBitmapReuseManager;)V

    .line 102
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->access$0(Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;Z)V

    .line 103
    invoke-static {v0, p1}, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->access$1(Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;I)V

    .line 104
    invoke-static {v0, p2}, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->access$2(Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;I)V

    .line 105
    invoke-static {v0, p3}, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->access$3(Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;I)V

    .line 107
    return-object v0
.end method

.method public freeBitmap(ILandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 160
    invoke-virtual {p0, p1}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->getContainer(I)Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;

    move-result-object v0

    .line 161
    .local v0, "container":Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;
    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {v0, p2}, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->freeBitmap(Landroid/graphics/Bitmap;)V

    .line 163
    :cond_0
    return-void
.end method

.method freeBitmapContainer()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->sRDContainer:Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;

    if-eqz v0, :cond_0

    .line 126
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->sRDContainer:Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;

    monitor-enter v1

    .line 127
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->sRDContainer:Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;

    .line 126
    monitor-exit v1

    .line 131
    :cond_0
    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBitmap(I)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 151
    invoke-virtual {p0, p1}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->getContainer(I)Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;

    move-result-object v0

    .line 152
    .local v0, "container":Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;
    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 155
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getContainer(I)Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 140
    packed-switch p1, :pswitch_data_0

    .line 146
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 143
    :pswitch_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->sRDContainer:Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;

    goto :goto_0

    .line 140
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method initBitmapContainer()V
    .locals 3

    .prologue
    .line 115
    .line 116
    sget v0, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_TILE_DEC_SIZE:I

    .line 117
    sget v1, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_TILE_DEC_SIZE:I

    .line 118
    sget v2, Lcom/quramsoft/xiv/XIVDefinedValues;->LIMIT_OF_RD_CONTAINER:I

    .line 115
    invoke-virtual {p0, v0, v1, v2}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->createBitmapContainer(III)Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->sRDContainer:Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;

    .line 120
    return-void
.end method

.method public resetContainer(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 167
    invoke-virtual {p0, p1}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->getContainer(I)Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;

    move-result-object v0

    .line 168
    .local v0, "container":Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;
    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->reset()V

    .line 170
    :cond_0
    return-void
.end method
