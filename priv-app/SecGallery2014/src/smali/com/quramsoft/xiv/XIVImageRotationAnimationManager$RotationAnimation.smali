.class Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;
.super Ljava/lang/Object;
.source "XIVImageRotationAnimationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RotationAnimation"
.end annotation


# static fields
.field private static final ROTATION_ANIMATION:I = 0x1

.field private static final SCALE_ANIMATION:I = 0x2

.field static final STATE_ENDED:I = 0x3

.field static final STATE_INIT:I = 0x0

.field static final STATE_READY:I = 0x1

.field static final STATE_STARTED:I = 0x2


# instance fields
.field private mCallBack:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;

.field private mCurrentDegree:F

.field private mCurrentScale:F

.field private mDuration:F

.field private mEndDegree:F

.field private mEndScale:F

.field private mIsClockwise:Z

.field private mStartDegree:F

.field private mStartScale:F

.field private mStartTime:J

.field private mState:I

.field private sLastWidth:F


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    iput v2, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->sLastWidth:F

    .line 173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mIsClockwise:Z

    .line 175
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mStartTime:J

    .line 176
    iput v2, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mStartDegree:F

    .line 177
    iput v2, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mEndDegree:F

    .line 178
    iput v2, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mStartScale:F

    .line 179
    iput v2, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mEndScale:F

    .line 180
    iput v2, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mDuration:F

    .line 182
    iput v2, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mCurrentDegree:F

    .line 183
    iput v2, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mCurrentScale:F

    .line 189
    return-void
.end method

.method static synthetic access$0(Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;)I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I

    return v0
.end method

.method static synthetic access$1(Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;I)V
    .locals 0

    .prologue
    .line 170
    iput p1, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I

    return-void
.end method

.method static synthetic access$2(Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;)Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mCallBack:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;

    return-object v0
.end method

.method static createRotationAnimation(FZLcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;)Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;
    .locals 2
    .param p0, "width"    # F
    .param p1, "isClockwise"    # Z
    .param p2, "callBack"    # Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;

    .prologue
    .line 257
    new-instance v0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;-><init>()V

    .line 258
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;
    const/4 v1, 0x0

    iput v1, v0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I

    .line 259
    iput p0, v0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->sLastWidth:F

    .line 260
    iput-boolean p1, v0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mIsClockwise:Z

    .line 261
    iput-object p2, v0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mCallBack:Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;

    .line 263
    return-object v0
.end method


# virtual methods
.method advanceAnimation(Lcom/quramsoft/xiv/XIV;)V
    .locals 6
    .param p1, "xiv"    # Lcom/quramsoft/xiv/XIV;

    .prologue
    .line 220
    iget v3, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 221
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 223
    .local v0, "now":J
    iget-wide v4, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mStartTime:J

    sub-long v4, v0, v4

    long-to-float v3, v4

    iget v4, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mDuration:F

    div-float v2, v3, v4

    .line 224
    .local v2, "progress":F
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_0

    .line 225
    const/high16 v2, 0x3f800000    # 1.0f

    .line 226
    const/4 v3, 0x3

    iput v3, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I

    .line 227
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/quramsoft/xiv/XIV;->checkAnimation(Z)V

    .line 230
    :cond_0
    iget v3, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mStartDegree:F

    iget v4, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mEndDegree:F

    invoke-virtual {p0, v3, v4, v2}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->getProgress(FFF)F

    move-result v3

    iput v3, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mCurrentDegree:F

    .line 231
    iget v3, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mStartScale:F

    iget v4, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mEndScale:F

    invoke-virtual {p0, v3, v4, v2}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->getProgress(FFF)F

    move-result v3

    iput v3, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mCurrentScale:F

    .line 238
    .end local v0    # "now":J
    .end local v2    # "progress":F
    :cond_1
    return-void
.end method

.method getDegree()F
    .locals 1

    .prologue
    .line 247
    iget v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mCurrentDegree:F

    return v0
.end method

.method getProgress(FFF)F
    .locals 4
    .param p1, "start"    # F
    .param p2, "end"    # F
    .param p3, "progress"    # F

    .prologue
    .line 193
    sub-float v0, p2, p1

    const v1, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v1, p3

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v0, v1

    add-float/2addr v0, p1

    return v0
.end method

.method getScale()F
    .locals 1

    .prologue
    .line 252
    iget v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mCurrentScale:F

    return v0
.end method

.method getState()I
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I

    return v0
.end method

.method startAnimation(Lcom/quramsoft/xiv/XIV;F)V
    .locals 3
    .param p1, "xiv"    # Lcom/quramsoft/xiv/XIV;
    .param p2, "height"    # F

    .prologue
    const/4 v2, 0x1

    .line 198
    iget v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I

    if-ne v0, v2, :cond_0

    .line 199
    const/4 v0, 0x2

    iput v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mState:I

    .line 201
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mIsClockwise:Z

    if-eqz v0, :cond_1

    .line 202
    const/high16 v0, -0x3d4c0000    # -90.0f

    iput v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mStartDegree:F

    .line 206
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mEndDegree:F

    .line 208
    iget v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->sLastWidth:F

    div-float/2addr v0, p2

    iput v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mStartScale:F

    .line 209
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mEndScale:F

    .line 211
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mStartTime:J

    .line 212
    sget v0, Lcom/quramsoft/xiv/XIVDefinedValues;->IMAGE_ROTATION_ANIMATION_TIME:I

    int-to-float v0, v0

    iput v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mDuration:F

    .line 214
    invoke-virtual {p1, v2}, Lcom/quramsoft/xiv/XIV;->checkAnimation(Z)V

    .line 216
    :cond_0
    return-void

    .line 204
    :cond_1
    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationAnimation;->mStartDegree:F

    goto :goto_0
.end method
