.class public Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;
.super Ljava/lang/Object;
.source "XIVBitmapReuseManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quramsoft/xiv/XIVBitmapReuseManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BitmapContainer"
.end annotation


# instance fields
.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private mFreeBitmapList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mIsRecycled:Z

.field private mLimit:I

.field final synthetic this$0:Lcom/quramsoft/xiv/XIVBitmapReuseManager;


# direct methods
.method public constructor <init>(Lcom/quramsoft/xiv/XIVBitmapReuseManager;)V
    .locals 1

    .prologue
    .line 27
    iput-object p1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->this$0:Lcom/quramsoft/xiv/XIVBitmapReuseManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    return-void
.end method

.method static synthetic access$0(Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;Z)V
    .locals 0

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mIsRecycled:Z

    return-void
.end method

.method static synthetic access$1(Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;I)V
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mBitmapWidth:I

    return-void
.end method

.method static synthetic access$2(Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;I)V
    .locals 0

    .prologue
    .line 34
    iput p1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mBitmapHeight:I

    return-void
.end method

.method static synthetic access$3(Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;I)V
    .locals 0

    .prologue
    .line 36
    iput p1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mLimit:I

    return-void
.end method


# virtual methods
.method freeBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mIsRecycled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mBitmapWidth:I

    if-ne v0, v1, :cond_0

    .line 76
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mBitmapHeight:I

    if-ne v0, v1, :cond_0

    .line 77
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mLimit:I

    if-ge v0, v1, :cond_0

    .line 88
    monitor-enter p0

    .line 89
    :try_start_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 88
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method getBitmap()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 53
    iget-boolean v1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mIsRecycled:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .line 68
    :goto_0
    return-object v0

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 57
    iget v1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mBitmapWidth:I

    iget v2, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mBitmapHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, "bitmap":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 59
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    monitor-enter p0

    .line 60
    :try_start_0
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 59
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    monitor-exit p0

    goto :goto_0

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method recycle()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mIsRecycled:Z

    .line 48
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->reset()V

    .line 49
    return-void
.end method

.method reset()V
    .locals 2

    .prologue
    .line 40
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBitmapReuseManager$BitmapContainer;->mFreeBitmapList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 40
    monitor-exit v1

    .line 43
    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
