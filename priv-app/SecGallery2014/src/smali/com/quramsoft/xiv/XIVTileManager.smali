.class public Lcom/quramsoft/xiv/XIVTileManager;
.super Ljava/lang/Object;
.source "XIVTileManager.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final DELAY_TIME_WHEN_BUSY:I = 0x320

.field private static final DELAY_TIME_WHEN_FREE:I = 0xc8

.field private static final DELAY_TIME_WHEN_NORMAL:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "XIVTileManager"

.field private static final TILE_DECODING_STANDARD_TIME_WHEN_BUSY:I = 0xc8

.field private static final TILE_DECODING_STANDARD_TIME_WHEN_LOW:I = 0x32

.field private static final TILE_DECODING_STANDARD_TIME_WHEN_NORMAL:I = 0x64

.field private static final TILE_QUOTA_WHEN_BUSY:I = 0x1

.field private static final TILE_QUOTA_WHEN_FREE:I = 0x8

.field private static final TILE_QUOTA_WHEN_LOW:I = 0x4

.field private static final TILE_QUOTA_WHEN_NORMAL:I = 0x2

.field static mQrd:Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;


# instance fields
.field private mAllowedUpdateContent:Z

.field private mCheckDecodingComplete:Z

.field private mDelayTime:I

.field private mNeedToReset:Z

.field private mOnSurfaceChanged:Z

.field private mStartTimeOfDecodingTotalTiles:J

.field private mTimeSumOfOnlyDecodingTiles:J

.field private mUploadTileQuota:I

.field private mXiv:Lcom/quramsoft/xiv/XIV;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/quramsoft/xiv/XIVTileManager;->mQrd:Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .line 10
    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const/16 v0, 0x320

    iput v0, p0, Lcom/quramsoft/xiv/XIVTileManager;->mDelayTime:I

    .line 88
    const/4 v0, 0x1

    iput v0, p0, Lcom/quramsoft/xiv/XIVTileManager;->mUploadTileQuota:I

    .line 89
    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVTileManager;->mNeedToReset:Z

    .line 90
    iput-wide v2, p0, Lcom/quramsoft/xiv/XIVTileManager;->mTimeSumOfOnlyDecodingTiles:J

    .line 91
    iput-wide v2, p0, Lcom/quramsoft/xiv/XIVTileManager;->mStartTimeOfDecodingTotalTiles:J

    .line 93
    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVTileManager;->mOnSurfaceChanged:Z

    .line 275
    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVTileManager;->mCheckDecodingComplete:Z

    .line 281
    iput-boolean v1, p0, Lcom/quramsoft/xiv/XIVTileManager;->mAllowedUpdateContent:Z

    .line 29
    return-void
.end method

.method static final create(Lcom/quramsoft/xiv/XIV;)Lcom/quramsoft/xiv/XIVTileManager;
    .locals 1
    .param p0, "xiv"    # Lcom/quramsoft/xiv/XIV;

    .prologue
    .line 22
    new-instance v0, Lcom/quramsoft/xiv/XIVTileManager;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIVTileManager;-><init>()V

    .line 23
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVTileManager;
    iput-object p0, v0, Lcom/quramsoft/xiv/XIVTileManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    .line 24
    return-object v0
.end method


# virtual methods
.method public cancelDecoding()V
    .locals 0

    .prologue
    .line 240
    return-void
.end method

.method public checkBitmapRegionDecoderEnd()V
    .locals 0

    .prologue
    .line 187
    return-void
.end method

.method public checkBitmapRegionDecoderStart()V
    .locals 0

    .prologue
    .line 181
    return-void
.end method

.method public checkDecodingNoComplete()V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/quramsoft/xiv/XIVTileManager;->mCheckDecodingComplete:Z

    .line 279
    return-void
.end method

.method public checkDecodingTime(J)V
    .locals 5
    .param p1, "startTime"    # J

    .prologue
    .line 118
    iget-object v4, p0, Lcom/quramsoft/xiv/XIVTileManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-nez v4, :cond_0

    .line 175
    :goto_0
    return-void

    .line 120
    :cond_0
    const-wide/16 v0, 0x0

    .local v0, "endTime":J
    const-wide/16 v2, 0x0

    .line 175
    .local v2, "timeOfDecodingTile":J
    goto :goto_0
.end method

.method public drawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFII)V
    .locals 24
    .param p1, "tiv"    # Lcom/sec/android/gallery3d/ui/TileImageView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "r"    # Landroid/graphics/Rect;
    .param p4, "level"    # I
    .param p5, "size"    # I
    .param p6, "length"    # F
    .param p7, "offsetX"    # I
    .param p8, "offsetY"    # I

    .prologue
    .line 301
    sget-boolean v1, Lcom/quramsoft/xiv/XIVConfig;->XIV_SHOW_RD_AFTER_COMPLETED:Z

    if-eqz v1, :cond_0

    .line 302
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/quramsoft/xiv/XIVTileManager;->mCheckDecodingComplete:Z

    .line 307
    :cond_0
    move-object/from16 v0, p3

    iget v1, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    div-int v1, v1, p5

    add-int/lit8 v18, v1, 0x1

    .line 308
    .local v18, "maxNumOfX":I
    move-object/from16 v0, p3

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    div-int v1, v1, p5

    add-int/lit8 v19, v1, 0x1

    .line 309
    .local v19, "maxNumOfY":I
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(II)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v17, v1, 0x2

    .line 311
    .local v17, "maxNumOfRect":I
    add-int/lit8 v1, v17, -0x1

    mul-int/lit8 v1, v1, 0x2

    sub-int v9, v18, v1

    .line 312
    .local v9, "XOfStartRect":I
    add-int/lit8 v1, v17, -0x1

    mul-int/lit8 v1, v1, 0x2

    sub-int v10, v19, v1

    .line 314
    .local v10, "YOfStartRect":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    move/from16 v0, v17

    if-lt v13, v0, :cond_2

    .line 383
    sget-boolean v1, Lcom/quramsoft/xiv/XIVConfig;->XIV_SHOW_RD_AFTER_COMPLETED:Z

    if-eqz v1, :cond_1

    .line 384
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/quramsoft/xiv/XIVTileManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/quramsoft/xiv/XIVTileManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v1}, Lcom/quramsoft/xiv/XIV;->isZoomState()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 385
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/quramsoft/xiv/XIVTileManager;->mAllowedUpdateContent:Z

    .line 389
    :cond_1
    :goto_1
    return-void

    .line 316
    :cond_2
    mul-int/lit8 v1, v13, 0x2

    add-int v23, v9, v1

    .line 317
    .local v23, "widthOfRect":I
    mul-int/lit8 v1, v13, 0x2

    add-int v12, v10, v1

    .line 319
    .local v12, "heightOfRect":I
    add-int/lit8 v1, v17, -0x1

    sub-int v16, v1, v13

    .line 320
    .local v16, "leftOfRect":I
    add-int/lit8 v1, v17, -0x1

    sub-int v22, v1, v13

    .line 321
    .local v22, "topOfRect":I
    add-int v1, v16, v23

    add-int/lit8 v20, v1, -0x1

    .line 322
    .local v20, "rightOfRect":I
    add-int v1, v22, v12

    add-int/lit8 v11, v1, -0x1

    .line 324
    .local v11, "bottomOfRect":I
    move/from16 v14, v16

    .line 325
    .local v14, "iX":I
    move/from16 v15, v22

    .line 327
    .local v15, "iY":I
    const/16 v21, 0x0

    .line 328
    .local v21, "state":I
    :goto_2
    const/4 v1, 0x4

    move/from16 v0, v21

    if-lt v0, v1, :cond_3

    .line 314
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 330
    :cond_3
    packed-switch v21, :pswitch_data_0

    .line 364
    :goto_3
    move-object/from16 v0, p3

    iget v1, v0, Landroid/graphics/Rect;->left:I

    mul-int v2, p5, v14

    add-int v3, v1, v2

    .line 365
    .local v3, "tx":I
    move-object/from16 v0, p3

    iget v1, v0, Landroid/graphics/Rect;->top:I

    mul-int v2, p5, v15

    add-int v4, v1, v2

    .line 366
    .local v4, "ty":I
    move/from16 v0, p7

    int-to-float v1, v0

    int-to-float v2, v14

    mul-float v2, v2, p6

    add-float v6, v1, v2

    .line 367
    .local v6, "x":F
    move/from16 v0, p8

    int-to-float v1, v0

    int-to-float v2, v15

    mul-float v2, v2, p6

    add-float v7, v1, v2

    .local v7, "y":F
    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v5, p4

    move/from16 v8, p6

    .line 368
    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/gallery3d/ui/TileImageView;->drawTile(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIFFF)V

    goto :goto_2

    .line 333
    .end local v3    # "tx":I
    .end local v4    # "ty":I
    .end local v6    # "x":F
    .end local v7    # "y":F
    :pswitch_0
    move/from16 v0, v20

    if-ge v14, v0, :cond_4

    .line 334
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 336
    :cond_4
    add-int/lit8 v21, v21, 0x1

    .line 338
    goto :goto_3

    .line 340
    :pswitch_1
    if-ge v15, v11, :cond_5

    .line 341
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 343
    :cond_5
    add-int/lit8 v21, v21, 0x1

    .line 345
    goto :goto_3

    .line 347
    :pswitch_2
    move/from16 v0, v16

    if-le v14, v0, :cond_6

    .line 348
    add-int/lit8 v14, v14, -0x1

    goto :goto_3

    .line 350
    :cond_6
    add-int/lit8 v21, v21, 0x1

    .line 352
    goto :goto_3

    .line 354
    :pswitch_3
    move/from16 v0, v22

    if-le v15, v0, :cond_7

    .line 355
    add-int/lit8 v15, v15, -0x1

    goto :goto_3

    .line 357
    :cond_7
    add-int/lit8 v21, v21, 0x1

    .line 359
    goto :goto_3

    .line 387
    .end local v11    # "bottomOfRect":I
    .end local v12    # "heightOfRect":I
    .end local v14    # "iX":I
    .end local v15    # "iY":I
    .end local v16    # "leftOfRect":I
    .end local v20    # "rightOfRect":I
    .end local v21    # "state":I
    .end local v22    # "topOfRect":I
    .end local v23    # "widthOfRect":I
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/quramsoft/xiv/XIVTileManager;->mCheckDecodingComplete:Z

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/quramsoft/xiv/XIVTileManager;->mAllowedUpdateContent:Z

    goto/16 :goto_1

    .line 330
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public extendRange(I)I
    .locals 0
    .param p1, "wh"    # I

    .prologue
    .line 270
    return p1
.end method

.method freeQrd()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    sput-object v0, Lcom/quramsoft/xiv/XIVTileManager;->mQrd:Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .line 70
    return-void
.end method

.method public getCurrentRegionDecoder()Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/quramsoft/xiv/XIVTileManager;->mQrd:Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    return-object v0
.end method

.method public getFullsizeEnabled()Z
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x1

    return v0
.end method

.method public getUploadTileQuota()I
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x1

    return v0
.end method

.method public isAllowedUpdateContent()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 284
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVTileManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-nez v2, :cond_1

    .line 289
    :cond_0
    :goto_0
    return v0

    .line 286
    :cond_1
    sget-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_SHOW_RD_AFTER_COMPLETED:Z

    if-eqz v2, :cond_3

    .line 287
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVTileManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {p0, v2}, Lcom/quramsoft/xiv/XIVTileManager;->isNotDecodingAllowed(Lcom/quramsoft/xiv/XIV;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/quramsoft/xiv/XIVTileManager;->mAllowedUpdateContent:Z

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 289
    :cond_3
    iget-object v2, p0, Lcom/quramsoft/xiv/XIVTileManager;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {p0, v2}, Lcom/quramsoft/xiv/XIVTileManager;->isNotDecodingAllowed(Lcom/quramsoft/xiv/XIV;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public isNotDecodingAllowed(Lcom/quramsoft/xiv/XIV;)Z
    .locals 1
    .param p1, "xiv"    # Lcom/quramsoft/xiv/XIV;

    .prologue
    .line 34
    iget v0, p0, Lcom/quramsoft/xiv/XIVTileManager;->mDelayTime:I

    invoke-virtual {p1, v0}, Lcom/quramsoft/xiv/XIV;->needToWaitForAnimation(I)Z

    move-result v0

    .line 33
    return v0
.end method

.method public isReadyToDecode()Z
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/quramsoft/xiv/XIVTileManager;->mQrd:Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    if-eqz v0, :cond_0

    .line 62
    sget-object v0, Lcom/quramsoft/xiv/XIVTileManager;->mQrd:Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->isReadyToDecode()Z

    move-result v0

    .line 64
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSurfaceChanged()V
    .locals 0

    .prologue
    .line 195
    return-void
.end method

.method public resetTimeSumOfDecodingTiles()V
    .locals 0

    .prologue
    .line 248
    return-void
.end method

.method public resetUploadTileQuota(I)I
    .locals 1
    .param p1, "lastQuota"    # I

    .prologue
    .line 220
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_SHOW_RD_AFTER_COMPLETED:Z

    if-eqz v0, :cond_0

    .line 223
    .end local p1    # "lastQuota":I
    :goto_0
    return p1

    .restart local p1    # "lastQuota":I
    :cond_0
    add-int/lit8 p1, p1, -0x1

    goto :goto_0
.end method

.method public setDelayTime()V
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/quramsoft/xiv/XIVTileManager;->mQrd:Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/quramsoft/xiv/XIVTileManager;->mQrd:Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->isPNG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const/16 v0, 0x320

    iput v0, p0, Lcom/quramsoft/xiv/XIVTileManager;->mDelayTime:I

    .line 114
    :goto_0
    return-void

    .line 106
    :cond_0
    const/16 v0, 0xc8

    iput v0, p0, Lcom/quramsoft/xiv/XIVTileManager;->mDelayTime:I

    goto :goto_0
.end method

.method public setQrd(Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;)V
    .locals 1
    .param p1, "qrd"    # Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .prologue
    .line 40
    sget-object v0, Lcom/quramsoft/xiv/XIVTileManager;->mQrd:Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    if-eq p1, v0, :cond_1

    .line 41
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    if-nez v0, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/quramsoft/xiv/XIVTileManager;->freeQrd()V

    .line 45
    :cond_0
    if-eqz p1, :cond_1

    .line 47
    sput-object p1, Lcom/quramsoft/xiv/XIVTileManager;->mQrd:Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .line 50
    :cond_1
    return-void
.end method

.method public setRenderComplete(IZ)Z
    .locals 0
    .param p1, "lastQuota"    # I
    .param p2, "renderComplete"    # Z

    .prologue
    .line 228
    if-lez p1, :cond_0

    .line 229
    const/4 p2, 0x0

    .line 231
    .end local p2    # "renderComplete":Z
    :cond_0
    return p2
.end method
