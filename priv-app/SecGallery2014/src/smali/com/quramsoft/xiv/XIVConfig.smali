.class public Lcom/quramsoft/xiv/XIVConfig;
.super Ljava/lang/Object;
.source "XIVConfig.java"


# static fields
.field static final XIV_ADAPTIVE_THREAD_NUM:Z = false

.field static final XIV_ALPHABLENDING_ANIMATION:Z = true

.field static final XIV_ANIMATION:Z = true

.field static final XIV_BITMAP_REUSE_MANAGER:Z = true

.field static final XIV_BUFFER_REUSE_MANAGER:Z = true

.field static final XIV_CACHE_MAKING_LIMITER:Z = false

.field static final XIV_CANCELING_DECODING:Z = true

.field static final XIV_CENTER_FIRST_RD:Z = true

.field static final XIV_CHANGE_TILE_QUOTA:Z = false

.field static final XIV_DEBUG:Z = false

.field static final XIV_DELAY_DECODING:Z = true

.field static final XIV_DELAY_MTN_JOB:Z = true

.field static final XIV_DELAY_RD_SCAN:Z = true

.field static final XIV_DOWN_SAMPLING:Z = false

.field static final XIV_DUAL_CACHE:Z = true

.field static final XIV_DYNAMIC_SLIDE_TIME:Z = true

.field static final XIV_EXTEND_RD_RANGE:Z = false

.field static final XIV_EXTEND_STN_SCALE_RANGE:Z = true

.field static final XIV_FAST_LAUNCHING:Z = true

.field static final XIV_FAST_RD_SCAN:Z = true

.field static final XIV_FAST_RESIZE:Z = true

.field static XIV_FILLED_REUSE_POOL_MODE:Z = false

.field static final XIV_HIGH_FRAME_RATE:Z = true

.field static final XIV_IMAGE_INFO_CACHE:Z = false

.field static final XIV_IMAGE_ROTATE_ANIMATION:Z = true

.field static XIV_LTN_TO_RD_ALPHA_ANIM:Z = false

.field static final XIV_MAKE_LIST_EACH_FOLDER:Z = false

.field static final XIV_MAKE_MSTN_OF_STN:Z = true

.field static final XIV_MAKE_STN_OF_MTN:Z = false

.field static final XIV_MEMORY_REUSE:Z = true

.field static final XIV_MODE:Z = true

.field public static final XIV_MODIFIED_DATE:Ljava/lang/String; = "2014.04.28"

.field static final XIV_MTN_SAMPLESIZE_FIX:Z = true

.field static XIV_MTN_TO_LTN_ALPHA_ANIM:Z = false

.field static XIV_NO_CROP_PANORAMA_THUMNAIL:Z = false

.field static final XIV_NO_USE_SNAPBACK_ANIMATION:Z = true

.field static final XIV_PARTIAL_DECODE_WSTN:Z

.field static final XIV_PARTIAL_DECODING:Z = false

.field public static final XIV_PERFORMANCE_CHECK:Z = false

.field static final XIV_QRB:Z = true

.field static final XIV_QRB_FILE_DESCRIPTOR:Z = false

.field static final XIV_RD_INVALIDATE_FIX:Z = true

.field static final XIV_RD_SCAN_CANCELING:Z = false

.field public static final XIV_RELEASE_DATE:Ljava/lang/String; = "2014.04.28"

.field static XIV_REUSEBUFFER_SIZE_LIMIT:F = 0.0f

.field static final XIV_SCROLL_INTERPOLATION:Z = false

.field static XIV_SHOW_RD_AFTER_COMPLETED:Z = false

.field static final XIV_SLIDE_ANIMATION:Z = true

.field static XIV_STOP_RD_WHILE_ZOOMING:Z = false

.field static final XIV_TEXTURE_REUSE:Z = false

.field static final XIV_TEX_UPLOAD_TIME_CHECK:Z = false

.field static final XIV_THREAD_RD:Z = false

.field static final XIV_TILT_SCALING_FIX:Z = true

.field static final XIV_USE_BGTM:Z = false

.field static final XIV_USE_BUFFER_LTN_MODE:Z = true

.field static final XIV_USE_BUFFER_THUMBNAIL_MODE:Z = true

.field static final XIV_USE_CLIPPING_LARGETHUMBNAIL:Z

.field static final XIV_USE_FAST_LARGETHUMBNAIL:Z

.field static final XIV_USE_GALLERY_ACCEL:Z = true

.field static final XIV_USE_IMAGE_FIT_TEXTURE:Z = true

.field static final XIV_USE_JUST_ONE_THREAD:Z

.field static XIV_USE_LARGETHUMBNAIL:Z = false

.field static final XIV_USE_LARGE_ALBUM_CACHE:Z = true

.field static XIV_USE_LIMIT_WIDTH_OF_LTN:Z = false

.field static XIV_USE_LTN_FIXED_SAMPLESIZE:Z = false

.field static final XIV_USE_LTN_REUSE_MODE:Z = true

.field static final XIV_USE_MTN_REUSE_MODE:Z = true

.field static final XIV_USE_OVERRAPPED_TEXTURE:Z

.field static XIV_USE_PNG_LARGETHUMBNAIL:Z = false

.field static final XIV_USE_RD_EX:Z = true

.field static XIV_USE_RD_REUSE_MODE:Z = false

.field static XIV_USE_REMAKE_LTN:Z = false

.field static XIV_USE_SIDE_LARGETHUMBNAIL:Z = false

.field static final XIV_USE_SPEED_MANAGER:Z = true

.field static final XIV_USE_WINK:Z = true

.field static final XIV_USE_WINK_JPEG_ENCODER:Z = false

.field static XIV_USE_WSTN:Z = false

.field static final XIV_USE_XIV_TILE_SIZE:Z = true

.field public static final XIV_VERSION:Ljava/lang/String; = "4.5.11_S_AW"

.field static final XIV_WINK_BMP_DECODER:Z = true

.field static final XIV_ZOOM_ANIMATION:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 92
    sput-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_WSTN:Z

    .line 93
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_WSTN:Z

    sput-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_PARTIAL_DECODE_WSTN:Z

    .line 104
    sput-boolean v1, Lcom/quramsoft/xiv/XIVConfig;->XIV_NO_CROP_PANORAMA_THUMNAIL:Z

    .line 106
    const v0, 0x42587ae1    # 54.12f

    sput v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_REUSEBUFFER_SIZE_LIMIT:F

    .line 124
    sput-boolean v1, Lcom/quramsoft/xiv/XIVConfig;->XIV_SHOW_RD_AFTER_COMPLETED:Z

    .line 127
    sput-boolean v1, Lcom/quramsoft/xiv/XIVConfig;->XIV_STOP_RD_WHILE_ZOOMING:Z

    .line 134
    sput-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    .line 136
    sput-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_SIDE_LARGETHUMBNAIL:Z

    .line 138
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    sput-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_MTN_TO_LTN_ALPHA_ANIM:Z

    .line 139
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    sput-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_LTN_TO_RD_ALPHA_ANIM:Z

    .line 140
    sput-boolean v1, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_REMAKE_LTN:Z

    .line 143
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    sput-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_OVERRAPPED_TEXTURE:Z

    .line 145
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    sput-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_JUST_ONE_THREAD:Z

    .line 147
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    sput-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_PNG_LARGETHUMBNAIL:Z

    .line 148
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    sput-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_FAST_LARGETHUMBNAIL:Z

    .line 150
    sput-boolean v1, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LTN_FIXED_SAMPLESIZE:Z

    .line 152
    sput-boolean v1, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_CLIPPING_LARGETHUMBNAIL:Z

    .line 153
    sput-boolean v1, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LIMIT_WIDTH_OF_LTN:Z

    .line 164
    sput-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_RD_REUSE_MODE:Z

    .line 169
    sput-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_FILLED_REUSE_POOL_MODE:Z

    .line 19
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final extendSTNScaleRange()Z
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x1

    return v0
.end method

.method public static final isUseAlphablendingAnimationFromLTNToRD()Z
    .locals 1

    .prologue
    .line 273
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_LTN_TO_RD_ALPHA_ANIM:Z

    return v0
.end method

.method public static final isUseBufferReuseTexture()Z
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x1

    return v0
.end method

.method public static final isUseBufferThumbnailMode()Z
    .locals 1

    .prologue
    .line 351
    const/4 v0, 0x1

    return v0
.end method

.method public static final isUseCenterFirstRD()Z
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x1

    return v0
.end method

.method public static final isUseDelayDecoding()Z
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x1

    return v0
.end method

.method public static final isUseDelayRDScan()Z
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x1

    return v0
.end method

.method public static isUseFastLargeThumbnail()Z
    .locals 1

    .prologue
    .line 332
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_FAST_LARGETHUMBNAIL:Z

    return v0
.end method

.method public static final isUseFastLaunching()Z
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x1

    return v0
.end method

.method public static final isUseFastRDScan()Z
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x1

    return v0
.end method

.method public static final isUseFileDescriptorQRB()Z
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    return v0
.end method

.method public static final isUseImageFitTexture()Z
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x1

    return v0
.end method

.method public static final isUseImageRotationAnimation()Z
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x1

    return v0
.end method

.method public static final isUseLargeThumbnail()Z
    .locals 1

    .prologue
    .line 240
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    return v0
.end method

.method public static final isUseRDEX()Z
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x1

    return v0
.end method

.method public static isUseRemakeLargeThumbnail()Z
    .locals 1

    .prologue
    .line 336
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_REMAKE_LTN:Z

    return v0
.end method

.method public static final isUseTextureReuse()Z
    .locals 1

    .prologue
    .line 319
    const/4 v0, 0x0

    return v0
.end method

.method public static final isUseThreadRD()Z
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return v0
.end method

.method public static final isUseTileReuseMode()Z
    .locals 1

    .prologue
    .line 278
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_RD_REUSE_MODE:Z

    return v0
.end method

.method public static final isUseWSTN()Z
    .locals 1

    .prologue
    .line 200
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_WSTN:Z

    return v0
.end method

.method public static final isUseXIVDebug()Z
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    return v0
.end method

.method public static final isUseXIVZoomAnimation()Z
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    return v0
.end method

.method public static setReuseBufferSizeLimit(I)V
    .locals 1
    .param p0, "megaByte"    # I

    .prologue
    .line 328
    int-to-float v0, p0

    sput v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_REUSEBUFFER_SIZE_LIMIT:F

    .line 329
    return-void
.end method

.method public static final setUseFilledReusePoolMode(Z)V
    .locals 0
    .param p0, "use"    # Z

    .prologue
    .line 365
    sput-boolean p0, Lcom/quramsoft/xiv/XIVConfig;->XIV_FILLED_REUSE_POOL_MODE:Z

    .line 366
    return-void
.end method

.method public static final setUseLargeThumbnail(Z)V
    .locals 1
    .param p0, "use"    # Z

    .prologue
    .line 246
    sput-boolean p0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    .line 247
    if-eqz p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    sput-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_SHOW_RD_AFTER_COMPLETED:Z

    .line 248
    sput-boolean p0, Lcom/quramsoft/xiv/XIVConfig;->XIV_MTN_TO_LTN_ALPHA_ANIM:Z

    .line 249
    sput-boolean p0, Lcom/quramsoft/xiv/XIVConfig;->XIV_LTN_TO_RD_ALPHA_ANIM:Z

    .line 256
    return-void

    .line 247
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static final setUseNoCropPanoramaThumbnail(ZF)V
    .locals 2
    .param p0, "use"    # Z
    .param p1, "crop_ratio"    # F

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 298
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_WSTN:Z

    if-eqz v0, :cond_0

    .line 299
    sput-boolean p0, Lcom/quramsoft/xiv/XIVConfig;->XIV_NO_CROP_PANORAMA_THUMNAIL:Z

    .line 300
    cmpl-float v0, p1, v1

    if-ltz v0, :cond_1

    .line 301
    sput p1, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_PANORAMA_THUM_CROP_RATIO:F

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    div-float v0, v1, p1

    sput v0, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_PANORAMA_THUM_CROP_RATIO:F

    goto :goto_0
.end method

.method public static final setUseRemakeLargeThumbnail(Z)V
    .locals 0
    .param p0, "use"    # Z

    .prologue
    .line 341
    sput-boolean p0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_REMAKE_LTN:Z

    .line 342
    return-void
.end method

.method public static final setUseResizeLargeThumbnail(Z)V
    .locals 0
    .param p0, "use"    # Z

    .prologue
    .line 324
    return-void
.end method

.method public static final setUseSideLargeThumbnail(Z)V
    .locals 1
    .param p0, "use"    # Z

    .prologue
    .line 260
    sget-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LARGETHUMBNAIL:Z

    if-eqz v0, :cond_0

    .line 261
    sput-boolean p0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_SIDE_LARGETHUMBNAIL:Z

    .line 264
    :goto_0
    return-void

    .line 263
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_SIDE_LARGETHUMBNAIL:Z

    goto :goto_0
.end method

.method public static setUseStopRDWhileZooming(Z)V
    .locals 0
    .param p0, "use"    # Z

    .prologue
    .line 309
    sput-boolean p0, Lcom/quramsoft/xiv/XIVConfig;->XIV_STOP_RD_WHILE_ZOOMING:Z

    .line 310
    return-void
.end method

.method public static final setUseTileReuseMode(Z)V
    .locals 0
    .param p0, "use"    # Z

    .prologue
    .line 361
    sput-boolean p0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_RD_REUSE_MODE:Z

    .line 362
    return-void
.end method

.method public static final setUseWSTN(Z)V
    .locals 0
    .param p0, "use"    # Z

    .prologue
    .line 356
    sput-boolean p0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_WSTN:Z

    .line 357
    return-void
.end method

.method public static final useFixedSampleLevelForLargeThumbnail(I)V
    .locals 1
    .param p0, "level"    # I

    .prologue
    .line 345
    const/4 v0, 0x1

    sput-boolean v0, Lcom/quramsoft/xiv/XIVConfig;->XIV_USE_LTN_FIXED_SAMPLESIZE:Z

    .line 346
    sput p0, Lcom/quramsoft/xiv/XIVDefinedValues;->FIXED_SAMPLE_LEVEL_FOR_LTN:I

    .line 347
    return-void
.end method
