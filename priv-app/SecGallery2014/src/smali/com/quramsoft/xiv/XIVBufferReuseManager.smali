.class public Lcom/quramsoft/xiv/XIVBufferReuseManager;
.super Ljava/lang/Object;
.source "XIVBufferReuseManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quramsoft/xiv/XIVBufferReuseManager$BorderKey;,
        Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;
    }
.end annotation


# static fields
.field public static final BUFFER_TYPE_FTN:I = 0x1

.field public static final BUFFER_TYPE_LTN:I = 0x3

.field public static final BUFFER_TYPE_LTN_ROW:I = 0x5

.field public static final BUFFER_TYPE_MTN:I = 0x2

.field public static final BUFFER_TYPE_RD:I = 0x4

.field public static final BUFFER_TYPE_STN:I = 0x0

.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "XIVBufferReuseManager"


# instance fields
.field final UPLOAD_LIMIT:I

.field sBorderKey:Lcom/quramsoft/xiv/XIVBufferReuseManager$BorderKey;

.field sBorderLines:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/quramsoft/xiv/XIVBufferReuseManager$BorderKey;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field sCropRect:[F

.field private sLTNContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

.field private sLTNRowContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

.field private sMTNContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

.field private sRDContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

.field sTextureId:[I

.field sUploadedCount:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/16 v0, 0x64

    iput v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->UPLOAD_LIMIT:I

    .line 41
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sTextureId:[I

    .line 42
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sCropRect:[F

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sBorderLines:Ljava/util/HashMap;

    .line 75
    new-instance v0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BorderKey;

    invoke-direct {v0, p0}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BorderKey;-><init>(Lcom/quramsoft/xiv/XIVBufferReuseManager;)V

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sBorderKey:Lcom/quramsoft/xiv/XIVBufferReuseManager$BorderKey;

    .line 36
    return-void
.end method

.method static final create()Lcom/quramsoft/xiv/XIVBufferReuseManager;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/quramsoft/xiv/XIVBufferReuseManager;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIVBufferReuseManager;-><init>()V

    .line 31
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVBufferReuseManager;
    return-object v0
.end method

.method public static getBufferType(I)I
    .locals 1
    .param p0, "mediaItemType"    # I

    .prologue
    .line 183
    packed-switch p0, :pswitch_data_0

    .line 191
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 185
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 187
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 189
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method createBufferReuseContainer(IIII)Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;
    .locals 2
    .param p1, "type"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "limit"    # I

    .prologue
    .line 154
    new-instance v0, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    invoke-direct {v0, p0}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;-><init>(Lcom/quramsoft/xiv/XIVBufferReuseManager;)V

    .line 156
    .local v0, "newInstance":Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;
    invoke-static {v0, p1}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->access$0(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;I)V

    .line 157
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->access$1(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;Z)V

    .line 158
    mul-int v1, p2, p3

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v0, v1}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->access$2(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;I)V

    .line 159
    invoke-static {v0, p4}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->access$3(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;I)V

    .line 161
    return-object v0
.end method

.method createBufferReuseContainer(IIIII)Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;
    .locals 4
    .param p1, "type"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "limit"    # I
    .param p5, "initCount"    # I

    .prologue
    .line 166
    new-instance v1, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    invoke-direct {v1, p0}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;-><init>(Lcom/quramsoft/xiv/XIVBufferReuseManager;)V

    .line 168
    .local v1, "newInstance":Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;
    invoke-static {v1, p1}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->access$0(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;I)V

    .line 169
    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->access$1(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;Z)V

    .line 170
    mul-int v2, p2, p3

    mul-int/lit8 v2, v2, 0x4

    invoke-static {v1, v2}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->access$2(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;I)V

    .line 171
    invoke-static {v1, p4}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->access$3(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;I)V

    .line 172
    sget-boolean v2, Lcom/quramsoft/xiv/XIVConfig;->XIV_FILLED_REUSE_POOL_MODE:Z

    if-eqz v2, :cond_0

    .line 173
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p5, :cond_1

    .line 178
    .end local v0    # "i":I
    :cond_0
    return-object v1

    .line 174
    .restart local v0    # "i":I
    :cond_1
    invoke-virtual {p0, p1}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->getMaxLength(I)I

    move-result v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->freeBuffer(Ljava/nio/ByteBuffer;)V

    .line 173
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public freeBuffer(ILjava/nio/ByteBuffer;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 361
    invoke-virtual {p0, p1}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->getContainer(I)Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    move-result-object v0

    .line 362
    .local v0, "container":Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;
    if-eqz v0, :cond_0

    .line 363
    invoke-virtual {v0, p2}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->freeBuffer(Ljava/nio/ByteBuffer;)V

    .line 364
    :cond_0
    return-void
.end method

.method freeBufferContainers()V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sMTNContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    if-eqz v0, :cond_0

    .line 261
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sMTNContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    monitor-enter v1

    .line 262
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sMTNContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    .line 261
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sRDContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    if-eqz v0, :cond_1

    .line 267
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sRDContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    monitor-enter v1

    .line 268
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sRDContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    .line 267
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sLTNContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    if-eqz v0, :cond_2

    .line 273
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sLTNContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    monitor-enter v1

    .line 274
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sLTNContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    .line 273
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sLTNRowContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    if-eqz v0, :cond_3

    .line 279
    iget-object v1, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sLTNRowContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    monitor-enter v1

    .line 280
    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sLTNRowContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    .line 279
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 284
    :cond_3
    return-void

    .line 261
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 267
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 273
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 279
    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0
.end method

.method public getBuffer(I)Ljava/nio/ByteBuffer;
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 342
    invoke-virtual {p0, p1}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->getContainer(I)Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    move-result-object v0

    .line 343
    .local v0, "container":Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;
    if-eqz v0, :cond_0

    .line 344
    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 346
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->getMaxLength(I)I

    move-result v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    goto :goto_0
.end method

.method public getBuffer(II)Ljava/nio/ByteBuffer;
    .locals 3
    .param p1, "size"    # I
    .param p2, "type"    # I

    .prologue
    .line 351
    invoke-virtual {p0, p2}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->getContainer(I)Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    move-result-object v0

    .line 352
    .local v0, "container":Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;
    if-eqz v0, :cond_0

    # getter for: Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mMaxLength:I
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->access$4(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;)I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 353
    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 355
    :goto_0
    return-object v1

    :cond_0
    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    goto :goto_0
.end method

.method getContainer(I)Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 288
    packed-switch p1, :pswitch_data_0

    .line 304
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 295
    :pswitch_0
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sMTNContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    goto :goto_0

    .line 297
    :pswitch_1
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sLTNContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    goto :goto_0

    .line 299
    :pswitch_2
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sLTNRowContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    goto :goto_0

    .line 301
    :pswitch_3
    iget-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sRDContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    goto :goto_0

    .line 288
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method getMaxLength(I)I
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 308
    const/4 v1, 0x0

    .local v1, "width":I
    const/4 v0, 0x0

    .line 309
    .local v0, "height":I
    packed-switch p1, :pswitch_data_0

    .line 337
    :goto_0
    mul-int v2, v1, v0

    mul-int/lit8 v2, v2, 0x4

    return v2

    .line 312
    :pswitch_0
    sget v1, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    .line 313
    sget v0, Lcom/quramsoft/xiv/XIVCacheManager;->MICROTHUMBNAIL_SIZE:I

    .line 314
    goto :goto_0

    .line 316
    :pswitch_1
    sget v1, Lcom/quramsoft/xiv/XIVCacheManager;->MINI_MICROTHUMBNAIL_SIZE:I

    .line 317
    sget v0, Lcom/quramsoft/xiv/XIVCacheManager;->MINI_MICROTHUMBNAIL_SIZE:I

    .line 318
    goto :goto_0

    .line 320
    :pswitch_2
    sget v1, Lcom/quramsoft/xiv/XIVCacheManager;->THUMBNAIL_SIZE:I

    .line 321
    sget v0, Lcom/quramsoft/xiv/XIVCacheManager;->THUMBNAIL_SIZE:I

    .line 322
    goto :goto_0

    .line 324
    :pswitch_3
    sget v2, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_W_STEP:I

    add-int/lit8 v1, v2, 0x2

    .line 325
    const/16 v0, 0x200

    .line 326
    goto :goto_0

    .line 328
    :pswitch_4
    sget v1, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_MAX_SAMPLED_WIDTH:I

    .line 329
    const/16 v0, 0x202

    .line 330
    goto :goto_0

    .line 332
    :pswitch_5
    sget v1, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_TILE_DEC_SIZE:I

    .line 333
    sget v0, Lcom/quramsoft/xiv/XIVDefinedValues;->XIV_TILE_DEC_SIZE:I

    goto :goto_0

    .line 309
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method initBufferContainers()V
    .locals 9

    .prologue
    const/16 v4, 0xa

    const/4 v8, 0x5

    const/4 v1, 0x2

    .line 215
    .line 217
    sget v2, Lcom/quramsoft/xiv/XIVCacheManager;->THUMBNAIL_SIZE:I

    .line 218
    sget v3, Lcom/quramsoft/xiv/XIVCacheManager;->THUMBNAIL_SIZE:I

    move-object v0, p0

    move v5, v4

    .line 215
    invoke-virtual/range {v0 .. v5}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->createBufferReuseContainer(IIIII)Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sMTNContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    .line 230
    const/4 v3, 0x3

    .line 231
    sget v0, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_W_STEP:I

    add-int/lit8 v4, v0, 0x2

    .line 232
    const/16 v5, 0x200

    .line 233
    sget v6, Lcom/quramsoft/xiv/XIVDefinedValues;->LIMIT_OF_LTN_CONTAINER:I

    .line 234
    const/16 v7, 0x16

    move-object v2, p0

    .line 229
    invoke-virtual/range {v2 .. v7}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->createBufferReuseContainer(IIIII)Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sLTNContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    .line 238
    sget v4, Lcom/quramsoft/xiv/XIVDefinedValues;->LTN_MAX_SAMPLED_WIDTH:I

    .line 239
    const/16 v5, 0x202

    move-object v2, p0

    move v3, v8

    move v6, v8

    move v7, v1

    .line 236
    invoke-virtual/range {v2 .. v7}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->createBufferReuseContainer(IIIII)Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/quramsoft/xiv/XIVBufferReuseManager;->sLTNRowContainer:Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    .line 242
    return-void
.end method

.method public isContainerFull(I)Z
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 375
    invoke-virtual {p0, p1}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->getContainer(I)Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    move-result-object v0

    .line 376
    .local v0, "container":Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;
    if-eqz v0, :cond_0

    # getter for: Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mOverLimit:Z
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->access$5(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;)Z

    move-result v1

    if-eqz v1, :cond_0

    # getter for: Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->mFreeBitmapList:Ljava/util/LinkedList;
    invoke-static {v0}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->access$6(Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;)Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 377
    const/4 v1, 0x1

    .line 379
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resetContainer(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 368
    invoke-virtual {p0, p1}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->getContainer(I)Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;

    move-result-object v0

    .line 369
    .local v0, "container":Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;
    if-eqz v0, :cond_0

    .line 370
    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVBufferReuseManager$BufferContainer;->reset()V

    .line 371
    :cond_0
    return-void
.end method
