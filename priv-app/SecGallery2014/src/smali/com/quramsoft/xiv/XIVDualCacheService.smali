.class public Lcom/quramsoft/xiv/XIVDualCacheService;
.super Ljava/lang/Object;
.source "XIVDualCacheService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "XIVDualCacheService"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "maxEntries"    # I
    .param p3, "maxBytes"    # I
    .param p4, "version"    # I

    .prologue
    .line 14
    invoke-static {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/util/CacheManager;->getCache(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    return-object v0
.end method
