.class Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
.super Ljava/lang/Object;
.source "XIVLargeThumbnailManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LargeThumbnailTile"
.end annotation


# instance fields
.field state:I

.field texture:Lcom/quramsoft/xiv/XIVBufferReusableTexture;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482
    return-void
.end method

.method static createLargeThumbnail(Lcom/quramsoft/xiv/XIVBufferReusableTexture;)Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    .locals 2
    .param p0, "texture"    # Lcom/quramsoft/xiv/XIVBufferReusableTexture;

    .prologue
    .line 486
    new-instance v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;

    invoke-direct {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;-><init>()V

    .line 488
    .local v0, "newTile":Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;
    const/4 v1, 0x0

    iput v1, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;->state:I

    .line 489
    iput-object p0, v0, Lcom/quramsoft/xiv/XIVLargeThumbnailManager$XIVLargeThumbnail$LargeThumbnailTile;->texture:Lcom/quramsoft/xiv/XIVBufferReusableTexture;

    .line 491
    return-object v0
.end method
