.class public Lcom/sec/android/gallery3d/data/HiddenAlbumSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "HiddenAlbumSet.java"


# static fields
.field public static final INDEX_ALBUM_NAME:I = 0x0

.field public static final INDEX_BUCKET_ID:I = 0x3

.field public static final INDEX_HIDDEN_PATH:I = 0x1

.field public static final INDEX_SOURCE_PATH:I = 0x2

.field public static final PATH_ALL:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

.field public static final PROJECTIONS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAlbums:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field

.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->TAG:Ljava/lang/String;

    .line 25
    const-string v0, "/hidden/image"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    .line 26
    const-string v0, "/hidden/video"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    .line 27
    const-string v0, "/hidden/all"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->PATH_ALL:Lcom/sec/android/gallery3d/data/Path;

    .line 29
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "hidden_album"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "source_album"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->PROJECTIONS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 46
    invoke-static {}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mAlbums:Ljava/util/List;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mItems:Ljava/util/ArrayList;

    .line 47
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 48
    invoke-static {p2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 49
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->HIDDEN_ALBUM_URI:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 50
    return-void
.end method

.method private getHiddenAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 17
    .param p1, "manager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p2, "type"    # I
    .param p3, "parent"    # Lcom/sec/android/gallery3d/data/Path;
    .param p4, "bucketId"    # I
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "hiddenPath"    # Ljava/lang/String;
    .param p7, "sourcePath"    # Ljava/lang/String;

    .prologue
    .line 252
    invoke-virtual/range {p3 .. p4}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    .line 253
    .local v4, "path":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v13

    .line 254
    .local v13, "album":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v13, :cond_0

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v1

    move/from16 v0, p4

    if-ne v1, v0, :cond_0

    move-object v1, v13

    .line 269
    :goto_0
    return-object v1

    .line 258
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 271
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 260
    :pswitch_1
    new-instance v1, Lcom/sec/android/gallery3d/data/HiddenAlbum;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const/4 v6, 0x1

    move/from16 v5, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/gallery3d/data/HiddenAlbum;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/data/Path;IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 262
    :pswitch_2
    new-instance v1, Lcom/sec/android/gallery3d/data/HiddenAlbum;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const/4 v6, 0x0

    move/from16 v5, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/gallery3d/data/HiddenAlbum;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/data/Path;IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 264
    :pswitch_3
    const/4 v7, 0x2

    sget-object v8, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->getHiddenAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v15

    .line 266
    .local v15, "palb":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v7, 0x4

    sget-object v8, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->getHiddenAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v16

    .line 268
    .local v16, "valb":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v14, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 269
    .local v14, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v5, 0x0

    aput-object v15, v3, v5

    const/4 v5, 0x1

    aput-object v16, v3, v5

    invoke-direct {v1, v4, v14, v2, v3}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;Lcom/sec/android/gallery3d/app/GalleryApp;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 258
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getHiddenAlbumIds()[I
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 90
    const/4 v6, 0x0

    .line 93
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v1, "hidden_album_info"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "bucket_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 96
    if-eqz v6, :cond_1

    .line 97
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v9, v0, [I

    .line 98
    .local v9, "ids":[I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, v9

    if-ge v8, v0, :cond_0

    .line 99
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    aput v0, v9, v8

    .line 100
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 107
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 109
    .end local v8    # "i":I
    .end local v9    # "ids":[I
    :goto_1
    return-object v9

    .line 107
    :cond_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 109
    :goto_2
    new-array v9, v10, [I

    goto :goto_1

    .line 104
    :catch_0
    move-exception v7

    .line 105
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_2

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static loadHiddenItems(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;ZLjava/lang/String;)Ljava/util/List;
    .locals 19
    .param p0, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p1, "dbMgr"    # Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    .param p2, "isImage"    # Z
    .param p3, "whereClause"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/GalleryApp;",
            "Lcom/sec/samsung/gallery/util/LocalDatabaseManager;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 185
    .local v16, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v3, 0x0

    .line 186
    .local v3, "table":Ljava/lang/String;
    const/4 v4, 0x0

    .line 187
    .local v4, "projection":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 188
    .local v7, "orderClause":Ljava/lang/String;
    const/4 v15, 0x0

    .line 189
    .local v15, "itemPath":Lcom/sec/android/gallery3d/data/Path;
    if-eqz p2, :cond_0

    .line 190
    const-string v3, "images_hidden_album"

    .line 191
    sget-object v2, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    sget-object v5, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    array-length v5, v5

    add-int/lit8 v5, v5, 0x2

    invoke-static {v2, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "projection":[Ljava/lang/String;
    check-cast v4, [Ljava/lang/String;

    .line 192
    .restart local v4    # "projection":[Ljava/lang/String;
    array-length v2, v4

    add-int/lit8 v2, v2, -0x2

    const-string v5, "faces_rect"

    aput-object v5, v4, v2

    .line 193
    const-string v7, "datetaken DESC, _id DESC"

    .line 194
    sget-object v15, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 202
    :goto_0
    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    const-string/jumbo v5, "source_album"

    aput-object v5, v4, v2

    .line 204
    const/4 v9, 0x0

    .line 206
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v10

    .line 207
    .local v10, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v14, 0x0

    .line 208
    .local v14, "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 209
    if-nez v9, :cond_1

    .line 210
    sget-object v2, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->TAG:Ljava/lang/String;

    const-string v5, "cursor is null! loadHiddenItems"

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 236
    .end local v10    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v14    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :goto_1
    return-object v16

    .line 196
    .end local v9    # "cursor":Landroid/database/Cursor;
    :cond_0
    const-string/jumbo v3, "video_hidden_album"

    .line 197
    sget-object v2, Lcom/sec/android/gallery3d/data/LocalVideo;->PROJECTION:[Ljava/lang/String;

    sget-object v5, Lcom/sec/android/gallery3d/data/LocalVideo;->PROJECTION:[Ljava/lang/String;

    array-length v5, v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v2, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "projection":[Ljava/lang/String;
    check-cast v4, [Ljava/lang/String;

    .line 198
    .restart local v4    # "projection":[Ljava/lang/String;
    const-string v7, "datetaken DESC, _id DESC"

    .line 199
    sget-object v15, Lcom/sec/android/gallery3d/data/LocalVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0

    .line 214
    .restart local v9    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .restart local v14    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_1
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 215
    .local v13, "id":I
    invoke-virtual {v15, v13}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    .line 216
    .local v8, "childPath":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-static {v8, v9, v10, v0, v1}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/LocalMediaItem;

    move-result-object v14

    .line 217
    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 218
    .local v18, "sourcePath":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->setHiddenSourcePath(Ljava/lang/String;)V

    .line 219
    if-eqz p2, :cond_2

    .line 220
    array-length v2, v4

    add-int/lit8 v2, v2, -0x2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v17

    .line 221
    .local v17, "rawFaceInfo":[B
    if-eqz v17, :cond_2

    move-object/from16 v0, v17

    array-length v2, v0

    if-lez v2, :cond_2

    .line 222
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v12

    .line 223
    .local v12, "faceInfo":Landroid/os/Parcel;
    const/4 v2, 0x0

    move-object/from16 v0, v17

    array-length v5, v0

    move-object/from16 v0, v17

    invoke-virtual {v12, v0, v2, v5}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 224
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 225
    move-object v0, v14

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalImage;

    move-object v2, v0

    sget-object v5, Landroid/graphics/RectF;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, v12}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/data/LocalImage;->setRectOfAllFaces(Landroid/graphics/RectF;)V

    .line 226
    invoke-virtual {v12}, Landroid/os/Parcel;->recycle()V

    .line 229
    .end local v12    # "faceInfo":Landroid/os/Parcel;
    .end local v17    # "rawFaceInfo":[B
    :cond_2
    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 234
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .line 231
    .end local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v10    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v13    # "id":I
    .end local v14    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    .end local v18    # "sourcePath":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 232
    .local v11, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 234
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method private static loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/LocalMediaItem;
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p4, "isImage"    # Z

    .prologue
    .line 276
    invoke-virtual {p2, p0}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 277
    .local v0, "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    if-nez v0, :cond_1

    .line 278
    if-eqz p4, :cond_0

    .line 279
    new-instance v0, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v0    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    invoke-direct {v0, p0, p3, p1}, Lcom/sec/android/gallery3d/data/LocalImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .line 286
    .restart local v0    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :goto_0
    return-object v0

    .line 281
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/data/LocalVideo;

    .end local v0    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    invoke-direct {v0, p0, p3, p1}, Lcom/sec/android/gallery3d/data/LocalVideo;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .restart local v0    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    goto :goto_0

    .line 284
    :cond_1
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->updateContent(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method private loadSubMediaSets([IZ)Ljava/util/List;
    .locals 25
    .param p1, "bucketIds"    # [I
    .param p2, "isImage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([IZ)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 146
    .local v17, "albums":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    if-eqz p2, :cond_0

    const-string v3, "images_hidden_album"

    .line 149
    .local v3, "table":Ljava/lang/String;
    :goto_0
    const/16 v20, 0x0

    .line 151
    .local v20, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v9

    .line 152
    .local v9, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    .local v24, "whereClause":Ljava/lang/StringBuilder;
    const-string v2, "1=0"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    move-object/from16 v18, p1

    .local v18, "arr$":[I
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v23, v0

    .local v23, "len$":I
    const/16 v22, 0x0

    .local v22, "i$":I
    :goto_1
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_1

    aget v2, v18, v22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    .line 155
    .local v19, "bucketId":Ljava/lang/Integer;
    const-string v2, " OR "

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bucket_id="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 146
    .end local v3    # "table":Ljava/lang/String;
    .end local v9    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v18    # "arr$":[I
    .end local v19    # "bucketId":Ljava/lang/Integer;
    .end local v20    # "cursor":Landroid/database/Cursor;
    .end local v22    # "i$":I
    .end local v23    # "len$":I
    .end local v24    # "whereClause":Ljava/lang/StringBuilder;
    :cond_0
    const-string/jumbo v3, "video_hidden_album"

    goto :goto_0

    .line 159
    .restart local v3    # "table":Ljava/lang/String;
    .restart local v9    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .restart local v18    # "arr$":[I
    .restart local v20    # "cursor":Landroid/database/Cursor;
    .restart local v22    # "i$":I
    .restart local v23    # "len$":I
    .restart local v24    # "whereClause":Ljava/lang/StringBuilder;
    :cond_1
    :try_start_1
    const-string v7, "album_name DESC"

    .line 160
    .local v7, "orderClause":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    sget-object v4, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->PROJECTIONS:[Ljava/lang/String;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 161
    if-eqz v20, :cond_4

    .line 163
    :cond_2
    const/4 v2, 0x2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 164
    .local v15, "albumPath":Ljava/lang/String;
    if-eqz v15, :cond_3

    .line 165
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v15

    .line 167
    :cond_3
    const/4 v10, 0x6

    sget-object v11, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->PATH_ALL:Lcom/sec/android/gallery3d/data/Path;

    const/4 v2, 0x3

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v15}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->getHiddenAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v16

    .line 170
    .local v16, "album":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 176
    .end local v15    # "albumPath":Ljava/lang/String;
    .end local v16    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 178
    .end local v7    # "orderClause":Ljava/lang/String;
    .end local v9    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v18    # "arr$":[I
    .end local v22    # "i$":I
    .end local v23    # "len$":I
    .end local v24    # "whereClause":Ljava/lang/StringBuilder;
    :goto_2
    return-object v17

    .line 173
    :catch_0
    move-exception v21

    .line 174
    .local v21, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 176
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_2

    .end local v21    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 3
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int/2addr v1, p1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 71
    if-gtz p2, :cond_0

    .line 76
    :goto_0
    return-object v0

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mItems:Ljava/util/ArrayList;

    add-int v2, p1, p2

    invoke-virtual {v1, p1, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string v0, "hidden"

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mAlbums:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mAlbums:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public loadIslandHiddenItems([IZ)Ljava/util/List;
    .locals 8
    .param p1, "excludedBucketIds"    # [I
    .param p2, "isImage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([IZ)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 242
    .local v4, "whereClause":Ljava/lang/StringBuilder;
    const-string v5, "1=1"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget v5, v0, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 244
    .local v1, "bucketId":Ljava/lang/Integer;
    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bucket_id<>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 247
    .end local v1    # "bucketId":Ljava/lang/Integer;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, p2, v7}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->loadHiddenItems(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;ZLjava/lang/String;)Ljava/util/List;

    move-result-object v5

    return-object v5
.end method

.method public reload()J
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 114
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v2}, Lcom/sec/android/gallery3d/data/HiddenSource;->syncHiddenFiles(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 115
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->getHiddenAlbumIds()[I

    move-result-object v1

    .line 118
    .local v1, "bucketIds":[I
    new-instance v0, Ljava/util/TreeSet;

    new-instance v2, Lcom/sec/android/gallery3d/data/HiddenAlbumSet$1;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet$1;-><init>(Lcom/sec/android/gallery3d/data/HiddenAlbumSet;)V

    invoke-direct {v0, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 128
    .local v0, "albums":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    invoke-direct {p0, v1, v3}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->loadSubMediaSets([IZ)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 129
    invoke-direct {p0, v1, v4}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->loadSubMediaSets([IZ)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 130
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mAlbums:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 131
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mAlbums:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 133
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 134
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mItems:Ljava/util/ArrayList;

    invoke-virtual {p0, v1, v3}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->loadIslandHiddenItems([IZ)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 135
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mItems:Ljava/util/ArrayList;

    invoke-virtual {p0, v1, v4}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->loadIslandHiddenItems([IZ)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 136
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mItems:Ljava/util/ArrayList;

    sget-object v3, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 138
    invoke-static {}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->nextVersionNumber()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mDataVersion:J

    .line 140
    .end local v0    # "albums":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    .end local v1    # "bucketIds":[I
    :cond_0
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->mDataVersion:J

    return-wide v2
.end method
