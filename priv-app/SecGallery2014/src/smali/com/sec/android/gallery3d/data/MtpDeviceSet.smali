.class public Lcom/sec/android/gallery3d/data/MtpDeviceSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "MtpDeviceSet.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/MtpDeviceSet$DeviceLoadTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MtpDeviceSet"


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mDeviceLoadTask:Lcom/sec/android/gallery3d/data/MtpDeviceSet$DeviceLoadTask;

.field private mDeviceSet:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private mIsLoading:Z

.field private mLoadBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field

.field private final mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

.field private final mName:Ljava/lang/String;

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MtpContext;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "mtpContext"    # Lcom/sec/android/gallery3d/data/MtpContext;

    .prologue
    .line 51
    invoke-static {}, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDeviceSet:Ljava/util/ArrayList;

    .line 52
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 53
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    const-string v1, "mtp://mtp"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 54
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    .line 55
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0095

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mName:Ljava/lang/String;

    .line 56
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mHandler:Landroid/os/Handler;

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/MtpDeviceSet;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/MtpDeviceSet;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/MtpDeviceSet;)Lcom/sec/android/gallery3d/data/MtpContext;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/MtpDeviceSet;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/data/MtpDeviceSet;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/MtpDeviceSet;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->loadFinished(Ljava/util/ArrayList;)V

    return-void
.end method

.method public static getDeviceName(Lcom/sec/android/gallery3d/data/MtpContext;I)Ljava/lang/String;
    .locals 7
    .param p0, "mtpContext"    # Lcom/sec/android/gallery3d/data/MtpContext;
    .param p1, "deviceId"    # I

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MtpContext;->getMtpClient()Lcom/sec/android/gallery3d/data/MtpClient;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/data/MtpClient;->getDevice(I)Landroid/mtp/MtpDevice;

    move-result-object v0

    .line 107
    .local v0, "device":Landroid/mtp/MtpDevice;
    if-nez v0, :cond_0

    .line 108
    const-string v4, ""

    .line 118
    :goto_0
    return-object v4

    .line 110
    :cond_0
    invoke-virtual {v0}, Landroid/mtp/MtpDevice;->getDeviceInfo()Landroid/mtp/MtpDeviceInfo;

    move-result-object v1

    .line 111
    .local v1, "info":Landroid/mtp/MtpDeviceInfo;
    if-nez v1, :cond_1

    .line 112
    const-string v4, ""

    goto :goto_0

    .line 114
    :cond_1
    invoke-virtual {v1}, Landroid/mtp/MtpDeviceInfo;->getManufacturer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 115
    .local v2, "manufacturer":Ljava/lang/String;
    invoke-virtual {v1}, Landroid/mtp/MtpDeviceInfo;->getModel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 116
    .local v3, "model":Ljava/lang/String;
    const-string v4, "MtpDeviceSet"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "manufacturer : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const-string v4, "MtpDeviceSet"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "model : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private loadFinished(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mLoadBuffer:Ljava/util/ArrayList;

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mIsLoading:Z

    .line 95
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mLoadBuffer:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mLoadBuffer:Ljava/util/ArrayList;

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/gallery3d/data/MtpDeviceSet$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/data/MtpDeviceSet$1;-><init>(Lcom/sec/android/gallery3d/data/MtpDeviceSet;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 103
    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 123
    const-string v0, "MtpDeviceSet"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDeviceSet:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDeviceSet:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDeviceSet:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDeviceSet:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDeviceSet:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 132
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized isLoading()Z
    .locals 3

    .prologue
    .line 142
    monitor-enter p0

    :try_start_0
    const-string v0, "MtpDeviceSet"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mIsLoading : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mIsLoading:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mIsLoading:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public notifyDirty()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->notifyDirty()V

    .line 204
    return-void
.end method

.method public declared-synchronized reload()J
    .locals 3

    .prologue
    .line 148
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    const-string v0, "MtpDeviceSet"

    const-string v1, "reload start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDeviceLoadTask:Lcom/sec/android/gallery3d/data/MtpDeviceSet$DeviceLoadTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDeviceLoadTask:Lcom/sec/android/gallery3d/data/MtpDeviceSet$DeviceLoadTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MtpDeviceSet$DeviceLoadTask;->interrupt()V

    .line 151
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mIsLoading:Z

    .line 152
    new-instance v0, Lcom/sec/android/gallery3d/data/MtpDeviceSet$DeviceLoadTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/data/MtpDeviceSet$DeviceLoadTask;-><init>(Lcom/sec/android/gallery3d/data/MtpDeviceSet;Lcom/sec/android/gallery3d/data/MtpDeviceSet$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDeviceLoadTask:Lcom/sec/android/gallery3d/data/MtpDeviceSet$DeviceLoadTask;

    .line 153
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDeviceLoadTask:Lcom/sec/android/gallery3d/data/MtpDeviceSet$DeviceLoadTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MtpDeviceSet$DeviceLoadTask;->start()V

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mLoadBuffer:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 156
    const-string v0, "MtpDeviceSet"

    const-string v1, "mLoadBuffer not null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mLoadBuffer:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDeviceSet:Ljava/util/ArrayList;

    .line 158
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mLoadBuffer:Ljava/util/ArrayList;

    .line 159
    invoke-static {}, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDataVersion:J

    .line 163
    const-string v0, "MtpDeviceSet"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDeviceSet size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDeviceSet:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :cond_2
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->mDataVersion:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
