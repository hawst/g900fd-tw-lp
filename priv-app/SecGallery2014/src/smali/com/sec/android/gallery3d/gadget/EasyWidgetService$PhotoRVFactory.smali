.class Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;
.super Ljava/lang/Object;
.source "EasyWidgetService.java"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/gadget/EasyWidgetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PhotoRVFactory"
.end annotation


# static fields
.field private static mRecallCount:I


# instance fields
.field private final MAX_RECALL_COUNT:I

.field private QUEUE_COUNT_FUTURE_BMP:I

.field private final mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mAppWidgetId:I

.field private mFutureBmpMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mFuturePosition:I

.field private mFutureThread:Ljava/lang/Thread;

.field private mIsEmptyViewNow:Z

.field private mIsInit:Z

.field private mIsThreadStop:Z

.field private mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

.field private mSourceSize:I

.field private mTempContext:Landroid/content/Context;

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mRecallCount:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;II)V
    .locals 3
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "id"    # I
    .param p3, "type"    # I

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    .line 93
    iput v2, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFuturePosition:I

    .line 94
    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mTempContext:Landroid/content/Context;

    .line 95
    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    .line 96
    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 97
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->QUEUE_COUNT_FUTURE_BMP:I

    .line 98
    iput v2, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSourceSize:I

    .line 99
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsInit:Z

    .line 100
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsThreadStop:Z

    .line 203
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->MAX_RECALL_COUNT:I

    .line 103
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 104
    iput p2, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mAppWidgetId:I

    .line 105
    iput p3, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mType:I

    .line 106
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v0, :cond_0

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 109
    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFuturePosition:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsInit:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;
    .param p1, "x1"    # Z

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsInit:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mTempContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;)Lcom/sec/android/gallery3d/gadget/WidgetSource;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->QUEUE_COUNT_FUTURE_BMP:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSourceSize:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsThreadStop:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;
    .param p1, "x1"    # Ljava/lang/Thread;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    return-object p1
.end method

.method private initSource()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 119
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->close()V

    .line 122
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v0, :cond_3

    .line 123
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 125
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 126
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsThreadStop:Z

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 130
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 132
    :cond_2
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsInit:Z

    .line 135
    :cond_3
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mType:I

    packed-switch v0, :pswitch_data_0

    .line 144
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v0, :cond_4

    .line 145
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v0, p0}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->setContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 146
    :cond_4
    return-void

    .line 137
    :pswitch_1
    new-instance v0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$EmptySource;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$EmptySource;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    goto :goto_0

    .line 140
    :pswitch_2
    new-instance v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private updateEmptyView(Z)V
    .locals 4
    .param p1, "isEmpty"    # Z

    .prologue
    .line 170
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    if-ne v2, p1, :cond_0

    .line 182
    :goto_0
    return-void

    .line 174
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    .line 176
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 177
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v2, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 178
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    if-eqz p1, :cond_1

    const/4 v2, 0x0

    :goto_1
    iput v2, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 180
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-static {v2, v3, v0}, Lcom/sec/android/gallery3d/gadget/EasyPhotoAppWidgetProvider;->buildAndUpdateWidget(Landroid/content/Context;ILcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V

    goto :goto_0

    .line 178
    :cond_1
    const/4 v2, 0x3

    goto :goto_1
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 164
    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v0

    .line 165
    .local v0, "size":I
    if-gtz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->updateEmptyView(Z)V

    .line 166
    return v0

    .line 165
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 185
    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 3

    .prologue
    .line 197
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0300b5

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 198
    .local v0, "rv":Landroid/widget/RemoteViews;
    const v1, 0x7f0f01ea

    const v2, 0x7f02048b

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 199
    return-object v0
.end method

.method public getSingleFrame(I)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "position"    # I

    .prologue
    .line 206
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v10}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    .line 207
    .local v1, "context":Landroid/content/Context;
    const/4 v8, 0x0

    .line 208
    .local v8, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v0, 0x0

    .line 210
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v10, :cond_0

    .line 211
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v10, p1}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    .line 212
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v10}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v10

    iput v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSourceSize:I

    .line 216
    :cond_0
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    move-result v10

    if-gtz v10, :cond_3

    .line 217
    # getter for: Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->access$000()Ljava/lang/String;

    move-result-object v10

    const-string v11, "mFutureBitmap == null && position == 0"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v1, v8, v10, v11}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 254
    :cond_1
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsInit:Z

    const/4 v11, 0x1

    if-ne v10, v11, :cond_a

    const/4 v10, 0x0

    iput v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFuturePosition:I

    .line 257
    :goto_0
    iput-object v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mTempContext:Landroid/content/Context;

    .line 259
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    if-nez v10, :cond_2

    .line 260
    new-instance v10, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory$1;

    invoke-direct {v10, p0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory$1;-><init>(Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;)V

    iput-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    .line 290
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 291
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 293
    :cond_2
    return-object v0

    .line 221
    :cond_3
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 222
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v1, v8, v10, v11}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 234
    :cond_4
    :goto_1
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    if-nez v10, :cond_1

    iget-boolean v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsInit:Z

    if-nez v10, :cond_1

    .line 235
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 236
    .local v4, "futureIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 237
    .local v9, "removeIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    iget v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->QUEUE_COUNT_FUTURE_BMP:I

    if-ge v5, v10, :cond_7

    add-int v10, p1, v5

    iget v11, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSourceSize:I

    rem-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 223
    .end local v4    # "futureIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v5    # "i":I
    .end local v9    # "removeIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_5
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_4

    .line 224
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_6

    .line 225
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v1, v8, v10, v11}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 226
    iget-object v11, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    monitor-enter v11

    .line 227
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    monitor-exit v11

    goto :goto_1

    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 231
    :cond_6
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    iget-object v11, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 239
    .restart local v4    # "futureIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v5    # "i":I
    .restart local v9    # "removeIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_7
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 240
    .local v6, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;>;"
    :cond_8
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 241
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 242
    .local v3, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 243
    .local v7, "key":I
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 245
    .end local v3    # "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    .end local v7    # "key":I
    :cond_9
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 246
    .local v2, "count":I
    const/4 v7, 0x0

    .restart local v7    # "key":I
    :goto_4
    if-ge v7, v2, :cond_1

    .line 247
    iget-object v11, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    monitor-enter v11

    .line 248
    :try_start_1
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 249
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    monitor-exit v11

    .line 246
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 250
    :catchall_1
    move-exception v10

    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v10

    .line 255
    .end local v2    # "count":I
    .end local v4    # "futureIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v5    # "i":I
    .end local v6    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;>;"
    .end local v7    # "key":I
    .end local v9    # "removeIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_a
    add-int/lit8 v10, p1, 0x1

    iget v11, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSourceSize:I

    rem-int/2addr v10, v11

    iput v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFuturePosition:I

    goto/16 :goto_0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 14
    .param p1, "position"    # I

    .prologue
    const/4 v7, 0x0

    .line 298
    # getter for: Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->access$000()Ljava/lang/String;

    move-result-object v9

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "WidgetId : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", getViewAt : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v10

    .line 301
    .local v10, "token":J
    iget-object v9, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    .line 302
    .local v1, "context":Landroid/content/Context;
    const/4 v4, 0x0

    .line 304
    .local v4, "flipperFrame":Landroid/widget/RemoteViews;
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v9, p1}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    .line 305
    .local v6, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v0, 0x0

    .line 306
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v9, :cond_1

    .line 307
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->getSingleFrame(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 313
    :goto_0
    if-eqz v0, :cond_2

    .line 314
    iget-object v9, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v9

    iget-object v12, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v12, p1}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getContentUri(I)Landroid/net/Uri;

    move-result-object v12

    invoke-static {v9, v12}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExifOrientation(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v8

    .line 315
    .local v8, "rotation":I
    if-eqz v8, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v9

    if-nez v9, :cond_0

    .line 316
    const/4 v9, 0x1

    invoke-static {v0, v8, v9}, Lcom/sec/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 318
    :cond_0
    new-instance v5, Landroid/widget/RemoteViews;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const v12, 0x7f0300b1

    invoke-direct {v5, v9, v12}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    .end local v4    # "flipperFrame":Landroid/widget/RemoteViews;
    .local v5, "flipperFrame":Landroid/widget/RemoteViews;
    :try_start_1
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const v12, 0x7f0300b5

    invoke-direct {v3, v9, v12}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 320
    .local v3, "flipperContent":Landroid/widget/RemoteViews;
    const v9, 0x7f0f01ea

    invoke-virtual {v3, v9, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 321
    # getter for: Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->access$000()Ljava/lang/String;

    move-result-object v9

    const-string v12, "add view!"

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    const v9, 0x7f0f01e9

    invoke-virtual {v5, v9, v3}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 323
    const/4 v9, 0x0

    sput v9, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mRecallCount:I

    .line 337
    # getter for: Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->access$000()Ljava/lang/String;

    move-result-object v9

    const-string v12, "add click intent"

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 343
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    move-object v4, v5

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v3    # "flipperContent":Landroid/widget/RemoteViews;
    .end local v5    # "flipperFrame":Landroid/widget/RemoteViews;
    .end local v6    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v8    # "rotation":I
    .restart local v4    # "flipperFrame":Landroid/widget/RemoteViews;
    :goto_1
    move-object v9, v4

    .line 345
    :goto_2
    return-object v9

    .line 309
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v6    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    :try_start_2
    iget-object v9, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v9, p1}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    .line 310
    const/4 v9, 0x0

    const/4 v12, 0x0

    invoke-static {v1, v6, v9, v12}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 325
    :cond_2
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 326
    sget v9, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mRecallCount:I

    const/16 v12, 0xa

    if-ge v9, v12, :cond_4

    .line 327
    iget-object v9, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ge p1, v9, :cond_3

    add-int/lit8 v7, p1, 0x1

    .line 328
    .local v7, "newPos":I
    :cond_3
    # getter for: Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->access$000()Ljava/lang/String;

    move-result-object v9

    const-string v12, "bitmap is null so call next one."

    invoke-static {v9, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    sget v9, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mRecallCount:I

    add-int/lit8 v9, v9, 0x1

    sput v9, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mRecallCount:I

    .line 330
    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->getViewAt(I)Landroid/widget/RemoteViews;
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v9

    .line 343
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_2

    .line 332
    .end local v7    # "newPos":I
    :cond_4
    :try_start_3
    # getter for: Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->access$000()Ljava/lang/String;

    move-result-object v9

    const-string v12, "bitmap is null and recall count over so return null."

    invoke-static {v9, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    const/4 v9, 0x0

    sput v9, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mRecallCount:I
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 334
    const/4 v9, 0x0

    .line 343
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_2

    .line 338
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v6    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :catch_0
    move-exception v2

    .line 339
    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    :goto_3
    :try_start_4
    # getter for: Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->access$000()Ljava/lang/String;

    move-result-object v9

    const-string v12, "caught array index out of bound exception. data might be changed while sleep mode"

    invoke-static {v9, v12, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 343
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .line 340
    .end local v2    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catch_1
    move-exception v2

    .line 341
    .local v2, "e":Ljava/lang/NullPointerException;
    :goto_4
    :try_start_5
    # getter for: Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->access$000()Ljava/lang/String;

    move-result-object v9

    const-string v12, "caught null pointer exception. check source is deleted"

    invoke-static {v9, v12, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 343
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .end local v2    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v9

    :goto_5
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v9

    .end local v4    # "flipperFrame":Landroid/widget/RemoteViews;
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "flipperFrame":Landroid/widget/RemoteViews;
    .restart local v6    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v8    # "rotation":I
    :catchall_1
    move-exception v9

    move-object v4, v5

    .end local v5    # "flipperFrame":Landroid/widget/RemoteViews;
    .restart local v4    # "flipperFrame":Landroid/widget/RemoteViews;
    goto :goto_5

    .line 340
    .end local v4    # "flipperFrame":Landroid/widget/RemoteViews;
    .restart local v5    # "flipperFrame":Landroid/widget/RemoteViews;
    :catch_2
    move-exception v2

    move-object v4, v5

    .end local v5    # "flipperFrame":Landroid/widget/RemoteViews;
    .restart local v4    # "flipperFrame":Landroid/widget/RemoteViews;
    goto :goto_4

    .line 338
    .end local v4    # "flipperFrame":Landroid/widget/RemoteViews;
    .restart local v5    # "flipperFrame":Landroid/widget/RemoteViews;
    :catch_3
    move-exception v2

    move-object v4, v5

    .end local v5    # "flipperFrame":Landroid/widget/RemoteViews;
    .restart local v4    # "flipperFrame":Landroid/widget/RemoteViews;
    goto :goto_3
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x1

    return v0
.end method

.method public onContentDirty()V
    .locals 3

    .prologue
    .line 369
    # getter for: Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onContentDirty"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mAppWidgetId:I

    const v2, 0x7f0f016a

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 372
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    if-eqz v0, :cond_0

    .line 373
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->updateEmptyView(Z)V

    .line 374
    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 113
    # getter for: Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->initSource()V

    .line 116
    return-void
.end method

.method public onDataSetChanged()V
    .locals 6

    .prologue
    .line 350
    # getter for: Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "onDataSetChanged"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 353
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v4, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 354
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    iget v4, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    iput v4, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mType:I

    .line 355
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 357
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->initSource()V

    .line 359
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 361
    .local v2, "token":J
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v4, :cond_0

    .line 362
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->reload()V

    .line 364
    :cond_0
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 365
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 150
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->close()V

    .line 152
    iput-object v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    .line 155
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 158
    iput-object v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 161
    :cond_1
    return-void
.end method
