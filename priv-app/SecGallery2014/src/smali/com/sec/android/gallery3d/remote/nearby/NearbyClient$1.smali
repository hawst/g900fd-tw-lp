.class Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;
.super Ljava/lang/Object;
.source "NearbyClient.java"

# interfaces
.implements Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceAdded(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 5
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 258
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Device Added : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Model Name : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getModelName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Name : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Device Domain : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getDeviceDomain()Lcom/samsung/android/allshare/Device$DeviceDomain;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Device Type : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$100(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    .line 267
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$100(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 268
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$100(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    if-ne p1, v2, :cond_2

    .line 273
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refreshChangePlayerDialogRef()V

    .line 286
    :cond_1
    :goto_0
    return-void

    .line 270
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 274
    :cond_2
    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    if-ne p1, v2, :cond_1

    .line 275
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mWeakActivity:Ljava/lang/ref/WeakReference;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/lang/ref/WeakReference;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 276
    .local v0, "gallery":Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/nearby/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    .line 278
    .local v1, "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    if-eqz v1, :cond_3

    .line 279
    invoke-virtual {v1, p2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->update(Lcom/samsung/android/allshare/Device;)V

    .line 280
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->initFlatBrowseFlag()V

    .line 283
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$300(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->notifyDirty()V

    .line 284
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$300(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->notifyDirty(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDeviceRemoved(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 8
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    const/4 v7, 0x0

    .line 222
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Device Removed!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Device Type : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "  Name : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$100(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/ArrayList;

    move-result-object v5

    monitor-enter v5

    .line 227
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$100(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 228
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    sget-object v4, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    if-ne p1, v4, :cond_1

    .line 231
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->showDialogDisconnected(Ljava/lang/String;)V

    .line 232
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refreshChangePlayerDialogRef()V

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 228
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 233
    :cond_1
    sget-object v4, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    if-ne p1, v4, :cond_0

    .line 235
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mWeakActivity:Ljava/lang/ref/WeakReference;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/lang/ref/WeakReference;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 236
    .local v1, "gallery":Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/nearby/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    .line 238
    .local v3, "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    const/4 v4, 0x2

    new-array v0, v4, [Ljava/lang/String;

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v7

    const/4 v4, 0x1

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    .line 241
    .local v0, "deviceInfo":[Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 242
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "clear nearby device"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->update(Lcom/samsung/android/allshare/Device;)V

    .line 244
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->clearItems()V

    .line 247
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$300(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->notifyDirty()V

    .line 248
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$300(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v4

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->notifyDirty(Ljava/lang/String;)V

    .line 249
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mWeakActivity:Ljava/lang/ref/WeakReference;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/lang/ref/WeakReference;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    .line 251
    .local v2, "galleryFacade":Lcom/sec/samsung/gallery/core/GalleryFacade;
    const-string v4, "EXIT_SELECTION_MODE"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 252
    const-string v4, "DEVICE_DISCONNECTED"

    invoke-virtual {v2, v4, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
