.class public Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;
.super Lcom/sec/android/gallery3d/glcore/GlInterpolator;
.source "GlInterpolatorSqr.java"


# instance fields
.field mLevel:F

.field mType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlInterpolator;-><init>()V

    .line 6
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;->mLevel:F

    .line 7
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;->mType:I

    .line 10
    return-void
.end method

.method public constructor <init>(F)V
    .locals 1
    .param p1, "level"    # F

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlInterpolator;-><init>()V

    .line 6
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;->mLevel:F

    .line 7
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;->mType:I

    .line 13
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;->mLevel:F

    .line 14
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 26
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;->mType:I

    if-nez v1, :cond_0

    .line 27
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;->mLevel:F

    mul-float/2addr v1, p1

    sub-float v2, p1, v3

    mul-float/2addr v1, v2

    add-float v0, v1, p1

    .line 30
    .local v0, "retVal":F
    :goto_0
    return v0

    .line 29
    .end local v0    # "retVal":F
    :cond_0
    sub-float v1, v3, p1

    sub-float v2, v3, p1

    mul-float/2addr v1, v2

    sub-float v2, v3, p1

    mul-float/2addr v1, v2

    sub-float v0, v3, v1

    .restart local v0    # "retVal":F
    goto :goto_0
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 17
    return-void
.end method

.method public setLevel(F)V
    .locals 0
    .param p1, "level"    # F

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;->mLevel:F

    .line 35
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 20
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;->mType:I

    .line 21
    return-void
.end method
