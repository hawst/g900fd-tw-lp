.class Lcom/sec/android/gallery3d/ui/FacetagDialog;
.super Ljava/lang/Object;
.source "FacetagDialog.java"


# instance fields
.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mMotionDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FacetagDialog;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/FacetagDialog;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FacetagDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/FacetagDialog;->setFaceTagDialogOff(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/FacetagDialog;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FacetagDialog;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FacetagDialog;->mMotionDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/ui/FacetagDialog;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FacetagDialog;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FacetagDialog;->mMotionDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method private setFaceTagDialogOff(Z)V
    .locals 3
    .param p1, "faceTagDialogOff"    # Z

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FacetagDialog;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v1, "FaceTagDialogOff"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 82
    return-void
.end method


# virtual methods
.method faceTagDialogInitial()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 33
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FacetagDialog;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 34
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FacetagDialog;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 36
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f03005d

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 37
    .local v3, "layout":Landroid/view/View;
    const v5, 0x7f0f0050

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 38
    .local v4, "message1":Landroid/widget/TextView;
    const v5, 0x7f0f001d

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 39
    .local v1, "checkBox":Landroid/widget/CheckBox;
    const v5, 0x7f0e00e5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 40
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 41
    const v5, 0x7f0e00af

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 42
    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/ui/FacetagDialog;->isFaceTagDialogOff(Z)Z

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 43
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 44
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 45
    new-instance v5, Lcom/sec/android/gallery3d/ui/FacetagDialog$1;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/ui/FacetagDialog$1;-><init>(Lcom/sec/android/gallery3d/ui/FacetagDialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    new-instance v5, Lcom/sec/android/gallery3d/ui/FacetagDialog$2;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/ui/FacetagDialog$2;-><init>(Lcom/sec/android/gallery3d/ui/FacetagDialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 58
    const v5, 0x7f0e00db

    new-instance v6, Lcom/sec/android/gallery3d/ui/FacetagDialog$3;

    invoke-direct {v6, p0, v1}, Lcom/sec/android/gallery3d/ui/FacetagDialog$3;-><init>(Lcom/sec/android/gallery3d/ui/FacetagDialog;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 67
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FacetagDialog;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v6, Lcom/sec/android/gallery3d/ui/FacetagDialog$4;

    invoke-direct {v6, p0, v0}, Lcom/sec/android/gallery3d/ui/FacetagDialog$4;-><init>(Lcom/sec/android/gallery3d/ui/FacetagDialog;Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 78
    return-void
.end method

.method isFaceTagDialogOff(Z)Z
    .locals 4
    .param p1, "defValue"    # Z

    .prologue
    .line 86
    if-eqz p1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FacetagDialog;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v2, "FaceTagDialogOff"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 90
    .local v0, "faceTagDialogOff":Z
    :goto_0
    return v0

    .line 89
    .end local v0    # "faceTagDialogOff":Z
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FacetagDialog;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v2, "FaceTagDialogOff"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .restart local v0    # "faceTagDialogOff":Z
    goto :goto_0
.end method
