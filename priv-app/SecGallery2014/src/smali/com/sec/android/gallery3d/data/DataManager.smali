.class public Lcom/sec/android/gallery3d/data/DataManager;
.super Ljava/lang/Object;
.source "DataManager.java"

# interfaces
.implements Lcom/sec/android/gallery3d/app/StitchingChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/DataManager$1;,
        Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;,
        Lcom/sec/android/gallery3d/data/DataManager$DateTakenComparator;
    }
.end annotation


# static fields
.field public static final INCLUDE_ALL:I = 0x3

.field public static final INCLUDE_ALL_GET_CONTENT_WITH_CLOUD:I = 0xb

.field public static final INCLUDE_ALL_GET_FEATURED_CONTENT_WITH_CLOUD:I = 0xd

.field public static final INCLUDE_IMAGE:I = 0x1

.field public static final INCLUDE_IMAGE_EXCLUDE_SNS_ALBUM:I = 0x11

.field public static final INCLUDE_LOCAL_ALL_ONLY:I = 0x7

.field public static final INCLUDE_LOCAL_IMAGE_ONLY:I = 0x5

.field public static final INCLUDE_LOCAL_ONLY:I = 0x4

.field public static final INCLUDE_LOCAL_VIDEO_ONLY:I = 0x6

.field public static final INCLUDE_ONLY_CLOUD:I = 0x8

.field public static final INCLUDE_ONLY_CLOUD_IMAGE:I = 0x12

.field public static final INCLUDE_ONLY_CLOUD_VIDEO:I = 0x13

.field public static final INCLUDE_ONLY_FACEBOOK:I = 0x9

.field public static final INCLUDE_ONLY_HIDDEN:I = 0xe

.field public static final INCLUDE_ONLY_PICASA:I = 0xa

.field public static final INCLUDE_ONLY_PICASA_IMAGE:I = 0xf

.field public static final INCLUDE_ONLY_PICASA_VIDEO:I = 0x10

.field public static final INCLUDE_ONLY_TCLOUD:I = 0xc

.field public static final INCLUDE_VIDEO:I = 0x2

.field public static final INCLUDE_VIDEO_EXCLUDE_CLOUD_SNS:I = 0x14

.field public static final LOCK:Ljava/lang/Object;

.field private static final TAG:Ljava/lang/String;

.field private static final TOP_CLOUD_IMAGE_SET_PATH:Ljava/lang/String; = "/cloud/image"

.field private static final TOP_CLOUD_SET_PATH:Ljava/lang/String; = "/cloud/all"

.field private static final TOP_CLOUD_VIDEO_SET_PATH:Ljava/lang/String; = "/cloud/video"

.field private static final TOP_FACEBOOK_SET_PATH:Ljava/lang/String; = "/sns/all"

.field private static final TOP_HIDDEN_SET_PATH:Ljava/lang/String; = "/hidden/all"

.field private static final TOP_IMAGE_GET_CONTENT_PATH:Ljava/lang/String; = "/combo/{/mtp,/local/image}"

.field private static final TOP_IMAGE_SET_FEATURED_PATH:Ljava/lang/String; = "/combo/{/mtp,/local/image,/picasa/image,/sns/image,/cloud/image}"

.field private static final TOP_IMAGE_SET_PATH:Ljava/lang/String; = "/combo/{/mtp,/local/image,/picasa/image,/sns/image}"

.field private static final TOP_LOCAL_IMAGE_SET_PATH:Ljava/lang/String; = "/local/image"

.field private static final TOP_LOCAL_SET_PATH:Ljava/lang/String; = "/local/all"

.field private static final TOP_LOCAL_SINGLE_IMAGE_SET_PATH:Ljava/lang/String; = "/local/single/image"

.field private static final TOP_LOCAL_SINGLE_SET_PATH:Ljava/lang/String; = "/local/single/all"

.field private static final TOP_LOCAL_SINGLE_VIDEO_SET_PATH:Ljava/lang/String; = "/local/single/video"

.field private static final TOP_LOCAL_VIDEO_SET_PATH:Ljava/lang/String; = "/local/video"

.field private static final TOP_MTP_SET_PATH:Ljava/lang/String; = "/mtp"

.field private static final TOP_PICASA_IMAGE_SET_PATH:Ljava/lang/String; = "/picasa/image"

.field private static final TOP_PICASA_SET_PATH:Ljava/lang/String; = "/picasa/all"

.field private static final TOP_PICASA_VIDEO_SET_PATH:Ljava/lang/String; = "/picasa/video"

.field private static final TOP_SET_PATH:Ljava/lang/String; = "/combo/{/mtp,/local/all,/sns/all,/picasa/all}"

.field private static final TOP_SET_PATH_GET_CONTENT_WITH_CLOUD:Ljava/lang/String; = "/combo/{/mtp,/local/all,/cloud/all,/sns/all,/picasa/all}"

.field private static final TOP_SET_PATH_WITH_CLOUD:Ljava/lang/String; = "/combo/{/mtp,/local/all,/cloud/all,/sns/all,/picasa/all}"

.field private static final TOP_SET_PATH_WITH_TCLOUD:Ljava/lang/String; = "/combo/{/mtp,/local/all,/cloud/all,/sns/all,/picasa/all,/tCloud/all}"

.field private static final TOP_TCLOUD_SET_PATH:Ljava/lang/String; = "/tCloud/all"

.field private static final TOP_VIDEO_SET_PATH:Ljava/lang/String; = "/combo/{/local/video,/picasa/video,/cloud/video}"

.field private static final TOP_VIDEO_SET_PATH_WITHOUT_CLOUD_SNS:Ljava/lang/String; = "/combo/{/local/video,/picasa/video}"

.field private static mChangedNotiActive:Z

.field public static final sDateTakenComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActiveCount:I

.field private mAlbumCount:I

.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mBaseUri:Landroid/net/Uri;

.field private final mDefaultMainHandler:Landroid/os/Handler;

.field private mItemListHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private mItemPath:Lcom/sec/android/gallery3d/data/Path;

.field private mNotifierMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;",
            ">;"
        }
    .end annotation
.end field

.field private mOrderClause:Ljava/lang/String;

.field private mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mProjection:[Ljava/lang/String;

.field private mResolver:Landroid/content/ContentResolver;

.field private mSourceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/gallery3d/data/MediaSource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 100
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    .line 107
    const-class v0, Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    .line 174
    new-instance v0, Lcom/sec/android/gallery3d/data/DataManager$DateTakenComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager$DateTakenComparator;-><init>(Lcom/sec/android/gallery3d/data/DataManager$1;)V

    sput-object v0, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 190
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/data/DataManager;->mChangedNotiActive:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mActiveCount:I

    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 200
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mNotifierMap:Ljava/util/HashMap;

    .line 204
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    .line 931
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mItemListHashMap:Ljava/util/HashMap;

    .line 208
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 210
    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mResolver:Landroid/content/ContentResolver;

    .line 211
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/DataManager;->initQuery()V

    .line 213
    new-instance v0, Landroid/os/Handler;

    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mDefaultMainHandler:Landroid/os/Handler;

    .line 215
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 70
    sget-boolean v0, Lcom/sec/android/gallery3d/data/DataManager;->mChangedNotiActive:Z

    return v0
.end method

.method private addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V
    .locals 2
    .param p1, "source"    # Lcom/sec/android/gallery3d/data/MediaSource;

    .prologue
    .line 322
    if-nez p1, :cond_0

    .line 324
    :goto_0
    return-void

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSource;->getPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private checkRemotePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "topPath"    # Ljava/lang/String;

    .prologue
    .line 309
    move-object v0, p1

    .line 310
    .local v0, "path":Ljava/lang/String;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 311
    const-string v1, "/picasa/"

    const-string v2, "/picasa/merged"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 312
    const-string v1, "/sns/"

    const-string v2, "/sns/merged"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 313
    const-string v1, "/cloud/"

    const-string v2, "/cloud/merged"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 314
    const-string v1, "/tCloud/"

    const-string v2, "/tCloud/merged"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 317
    :cond_0
    return-object v0
.end method

.method public static from(Landroid/content/Context;)Lcom/sec/android/gallery3d/data/DataManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 104
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    return-object v1
.end method

.method public static getSingleAlbumSetPath(II)Ljava/lang/String;
    .locals 5
    .param p0, "bucketId"    # I
    .param p1, "typeBits"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 641
    const/4 v0, 0x0

    .line 642
    .local v0, "basePath":Ljava/lang/String;
    and-int/lit8 v4, p1, 0x1

    if-eqz v4, :cond_0

    move v1, v2

    .line 643
    .local v1, "hasImage":Z
    :goto_0
    and-int/lit8 v4, p1, 0x2

    if-eqz v4, :cond_1

    .line 644
    .local v2, "hasVideo":Z
    :goto_1
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 645
    const-string v0, "/local/single/all"

    .line 651
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .end local v1    # "hasImage":Z
    .end local v2    # "hasVideo":Z
    :cond_0
    move v1, v3

    .line 642
    goto :goto_0

    .restart local v1    # "hasImage":Z
    :cond_1
    move v2, v3

    .line 643
    goto :goto_1

    .line 646
    .restart local v2    # "hasVideo":Z
    :cond_2
    if-eqz v2, :cond_3

    .line 647
    const-string v0, "/local/single/video"

    goto :goto_2

    .line 649
    :cond_3
    const-string v0, "/local/single/image"

    goto :goto_2
.end method

.method private initQuery()V
    .locals 1

    .prologue
    .line 634
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mProjection:[Ljava/lang/String;

    .line 635
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 636
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mBaseUri:Landroid/net/Uri;

    .line 637
    const-string v0, "datetaken DESC, _id DESC"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mOrderClause:Ljava/lang/String;

    .line 638
    return-void
.end method

.method private loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 663
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 664
    .local v0, "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    if-nez v0, :cond_0

    .line 665
    new-instance v0, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v0    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    invoke-direct {v0, p1, p3, p2}, Lcom/sec/android/gallery3d/data/LocalImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .line 669
    .restart local v0    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :goto_0
    return-object v0

    .line 667
    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->updateContent(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method private releasePowerWakeLock()V
    .locals 1

    .prologue
    .line 941
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 942
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 943
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 945
    :cond_0
    return-void
.end method

.method private setPowerWakeLock()V
    .locals 3

    .prologue
    .line 919
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 921
    .local v0, "pm":Landroid/os/PowerManager;
    const/4 v1, 0x1

    const-string v2, "Gallery-Copy"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/DataManager;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 923
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DataManager;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    .line 924
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DataManager;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 925
    :cond_0
    return-void
.end method


# virtual methods
.method public cache(Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "flag"    # I

    .prologue
    .line 784
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 785
    .local v0, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v0, :cond_0

    .line 786
    sget-object v1, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cache mediaObject is null : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    :goto_0
    return-void

    .line 789
    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/data/MediaObject;->cache(I)V

    goto :goto_0
.end method

.method public copy(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "dstPath"    # Ljava/lang/String;
    .param p3, "newFileName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 742
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    .line 743
    .local v1, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v1, :cond_1

    .line 744
    sget-object v6, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "copy mediaObject is null : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 756
    :cond_0
    :goto_0
    return v3

    .line 747
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaObject;->getSupportedOperations()J

    move-result-wide v4

    .line 748
    .local v4, "supported":J
    const-wide/high16 v6, 0x20000000000000L

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_2

    const/4 v2, 0x1

    .line 749
    .local v2, "supportCopy":Z
    :goto_1
    if-eqz v2, :cond_0

    .line 751
    :try_start_0
    invoke-virtual {v1, p2, p3}, Lcom/sec/android/gallery3d/data/MediaObject;->copy(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    .end local v2    # "supportCopy":Z
    :cond_2
    move v2, v3

    .line 748
    goto :goto_1

    .line 752
    .restart local v2    # "supportCopy":Z
    :catch_0
    move-exception v0

    .line 753
    .local v0, "e":Ljava/lang/UnsupportedOperationException;
    goto :goto_0
.end method

.method public copyItem(Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p2, "dstPath"    # Ljava/lang/String;
    .param p3, "newFileName"    # Ljava/lang/String;

    .prologue
    .line 869
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 870
    :cond_0
    const/4 v1, 0x0

    .line 877
    :goto_0
    return v1

    .line 872
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 874
    .local v0, "path":Lcom/sec/android/gallery3d/data/Path;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/DataManager;->setPowerWakeLock()V

    .line 875
    invoke-virtual {p0, v0, p2, p3}, Lcom/sec/android/gallery3d/data/DataManager;->copy(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 877
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/DataManager;->releasePowerWakeLock()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/DataManager;->releasePowerWakeLock()V

    throw v1
.end method

.method public delete(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 6
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 445
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 447
    .local v0, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getSupportedOperations()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 448
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->delete()V

    .line 453
    :goto_0
    return-void

    .line 450
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e01f7

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 451
    sget-object v1, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t delete item : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public delete(Lcom/sec/android/gallery3d/data/Path;Z)V
    .locals 6
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "includeDir"    # Z

    .prologue
    .line 734
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 736
    .local v0, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getSupportedOperations()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 737
    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/data/MediaObject;->delete(Z)V

    .line 739
    :cond_0
    return-void
.end method

.method public deleteItem(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 895
    if-nez p1, :cond_0

    .line 900
    :goto_0
    return-void

    .line 898
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 899
    .local v0, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->delete(Lcom/sec/android/gallery3d/data/Path;)V

    goto :goto_0
.end method

.method public deleteItem(Lcom/sec/android/gallery3d/data/MediaObject;Z)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p2, "includeDir"    # Z

    .prologue
    .line 903
    if-nez p1, :cond_0

    .line 908
    :goto_0
    return-void

    .line 906
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 907
    .local v0, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {p0, v0, p2}, Lcom/sec/android/gallery3d/data/DataManager;->delete(Lcom/sec/android/gallery3d/data/Path;Z)V

    goto :goto_0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mItemListHashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 556
    return-void
.end method

.method public findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 483
    if-nez p1, :cond_0

    move-object v1, v3

    .line 488
    :goto_0
    return-object v1

    .line 484
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSource;

    .line 485
    .local v2, "source":Lcom/sec/android/gallery3d/data/MediaSource;
    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/data/MediaSource;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 486
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v1, :cond_1

    goto :goto_0

    .end local v1    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v2    # "source":Lcom/sec/android/gallery3d/data/MediaSource;
    :cond_2
    move-object v1, v3

    .line 488
    goto :goto_0
.end method

.method public getAddMtpTopPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "currentPath"    # Ljava/lang/String;

    .prologue
    .line 824
    const-string v5, "/mtp"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v1, p1

    .line 836
    .end local p1    # "currentPath":Ljava/lang/String;
    .local v1, "currentPath":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 825
    .end local v1    # "currentPath":Ljava/lang/String;
    .restart local p1    # "currentPath":Ljava/lang/String;
    :cond_0
    const-string v5, "/"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 826
    .local v4, "path":[Ljava/lang/String;
    array-length v3, v4

    .line 827
    .local v3, "n":I
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 828
    .local v0, "albumNumberBuffer":Ljava/lang/StringBuffer;
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_2

    .line 829
    const-string v5, "local"

    aget-object v6, v4, v2

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 830
    const-string v5, "/mtp"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ",/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    aget-object v6, v4, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 828
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 832
    :cond_1
    const-string v5, "/"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    aget-object v6, v4, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 835
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v1, p1

    .line 836
    .end local p1    # "currentPath":Ljava/lang/String;
    .restart local v1    # "currentPath":Ljava/lang/String;
    goto :goto_0
.end method

.method public getAlbumCount()I
    .locals 1

    .prologue
    .line 915
    iget v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mAlbumCount:I

    return v0
.end method

.method public getApplication()Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method public getContentUri(Lcom/sec/android/gallery3d/data/Path;)Landroid/net/Uri;
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 465
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 466
    .local v0, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v0, :cond_0

    .line 467
    sget-object v1, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getContentUri mediaObject is null : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    const/4 v1, 0x0

    .line 470
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public getDefaultSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v1, 0x0

    .line 499
    if-nez p1, :cond_1

    .line 502
    :cond_0
    :goto_0
    return-object v1

    .line 501
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSource;

    .line 502
    .local v0, "source":Lcom/sec/android/gallery3d/data/MediaSource;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/MediaSource;->getDefaultSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0
.end method

.method public getDetails(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 775
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 776
    .local v0, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v0, :cond_0

    .line 777
    sget-object v1, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDetails mediaObject is null : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    new-instance v1, Lcom/sec/android/gallery3d/data/MediaDetails;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/data/MediaDetails;-><init>()V

    .line 780
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v1

    goto :goto_0
.end method

.method public getLocalItemList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 937
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mItemListHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMediaItem(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 673
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "length(trim(_data)) > 0 AND _data like \'%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 674
    .local v3, "whereClause":Ljava/lang/String;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 675
    .local v12, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v7, 0x0

    .line 677
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DataManager;->mBaseUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/DataManager;->mProjection:[Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/DataManager;->mOrderClause:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 684
    if-nez v7, :cond_1

    .line 685
    sget-object v0, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    const-string v1, "query fail: "

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    :goto_0
    return-object v12

    .line 679
    :catch_0
    move-exception v9

    .line 680
    .local v9, "ex":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 684
    if-nez v7, :cond_1

    .line 685
    sget-object v0, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    const-string v1, "query fail: "

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 681
    .end local v9    # "ex":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 682
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 684
    if-nez v7, :cond_1

    .line 685
    sget-object v0, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    const-string v1, "query fail: "

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 684
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-nez v7, :cond_0

    .line 685
    sget-object v0, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    const-string v1, "query fail: "

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 686
    :cond_0
    throw v0

    .line 691
    :cond_1
    :goto_1
    :try_start_3
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 692
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 693
    .local v10, "id":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 694
    .local v6, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {p0, v6, v7, v0}, Lcom/sec/android/gallery3d/data/DataManager;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v11

    .line 695
    .local v11, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 698
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v10    # "id":I
    .end local v11    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :catchall_1
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    :cond_2
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public getMediaItemList(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1, "mediaSetPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 840
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 842
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_0

    .line 843
    const/4 v2, 0x0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    .line 847
    .local v0, "mediaItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :goto_0
    return-object v0

    .line 845
    .end local v0    # "mediaItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .restart local v0    # "mediaItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    goto :goto_0
.end method

.method public getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 9
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v4, 0x0

    .line 338
    sget-object v5, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 339
    if-nez p1, :cond_0

    .line 340
    :try_start_0
    sget-object v6, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    const-string v7, "Path is null"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    monitor-exit v5

    move-object v0, v4

    .line 361
    :goto_0
    return-object v0

    .line 344
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getObject()Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 345
    .local v0, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v0, :cond_1

    monitor-exit v5

    goto :goto_0

    .line 363
    .end local v0    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 347
    .restart local v0    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getPrefix()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSource;

    .line 348
    .local v2, "source":Lcom/sec/android/gallery3d/data/MediaSource;
    if-nez v2, :cond_2

    .line 349
    sget-object v6, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cannot find media source for path: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v4

    goto :goto_0

    .line 354
    :cond_2
    :try_start_2
    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/data/MediaSource;->createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    .line 355
    .local v1, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v1, :cond_3

    .line 356
    sget-object v6, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cannot create media object: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 358
    :cond_3
    :try_start_3
    monitor-exit v5

    move-object v0, v1

    goto :goto_0

    .line 359
    .end local v1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :catch_0
    move-exception v3

    .line 360
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "exception in creating media object: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 361
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v0, v4

    goto :goto_0
.end method

.method public getMediaObject(Lcom/sec/android/gallery3d/data/Path;Z)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "usePathCache"    # Z

    .prologue
    const/4 v2, 0x0

    .line 704
    if-nez p1, :cond_1

    .line 705
    sget-object v4, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    const-string v5, "path is null : "

    new-instance v6, Ljava/lang/Exception;

    invoke-direct {v6}, Ljava/lang/Exception;-><init>()V

    invoke-static {v4, v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v2

    .line 730
    :cond_0
    :goto_0
    return-object v1

    .line 709
    :cond_1
    if-eqz p2, :cond_2

    .line 710
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getObject()Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    .line 711
    .local v1, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v1, :cond_2

    instance-of v4, v1, Lcom/sec/android/gallery3d/data/UriImage;

    if-nez v4, :cond_2

    instance-of v4, v1, Lcom/sec/android/gallery3d/data/FilterTypeSet;

    if-eqz v4, :cond_0

    .line 715
    .end local v1    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getPrefix()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSource;

    .line 716
    .local v3, "source":Lcom/sec/android/gallery3d/data/MediaSource;
    if-nez v3, :cond_3

    .line 717
    sget-object v4, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    const-string v5, "cannot find media source for path: "

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 718
    goto :goto_0

    .line 721
    :cond_3
    const/4 v2, 0x0

    .line 723
    .local v2, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :try_start_0
    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/data/MediaSource;->createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    .line 724
    if-nez v2, :cond_4

    .line 725
    sget-object v4, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    const-string v5, "cannot create media object: "

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_1
    move-object v1, v2

    .line 730
    goto :goto_0

    .line 727
    :catch_0
    move-exception v0

    .line 728
    .local v0, "ie":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public getMediaObject(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 367
    invoke-static {p1}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    return-object v0
.end method

.method public getMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 371
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 375
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getMediaSetsFromString(Ljava/lang/String;)[Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 5
    .param p1, "segment"    # Ljava/lang/String;

    .prologue
    .line 379
    invoke-static {p1}, Lcom/sec/android/gallery3d/data/Path;->splitSequence(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 380
    .local v2, "seq":[Ljava/lang/String;
    array-length v1, v2

    .line 381
    .local v1, "n":I
    new-array v3, v1, [Lcom/sec/android/gallery3d/data/MediaSet;

    .line 382
    .local v3, "sets":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 383
    aget-object v4, v2, v0

    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    aput-object v4, v3, v0

    .line 382
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 385
    :cond_0
    return-object v3
.end method

.method public getMediaType(Lcom/sec/android/gallery3d/data/Path;)I
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 474
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 475
    .local v0, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v0, :cond_0

    .line 476
    sget-object v1, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMediaType mediaObject is null : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    const/4 v1, 0x1

    .line 479
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v1

    goto :goto_0
.end method

.method public getPanoramaSupport(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;)V
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "callback"    # Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;

    .prologue
    .line 438
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 439
    .local v0, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v0, :cond_0

    .line 440
    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/data/MediaObject;->getPanoramaSupport(Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;)V

    .line 442
    :cond_0
    return-void
.end method

.method public getParentSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v1, 0x0

    .line 492
    if-nez p1, :cond_1

    .line 495
    :cond_0
    :goto_0
    return-object v1

    .line 494
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSource;

    .line 495
    .local v0, "source":Lcom/sec/android/gallery3d/data/MediaSource;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/MediaSource;->getParentSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0
.end method

.method public getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 655
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSource;

    return-object v0
.end method

.method public getSupportedOperations(Lcom/sec/android/gallery3d/data/Path;)J
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 429
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 430
    .local v0, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v0, :cond_0

    .line 431
    sget-object v1, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSupportedOperations mediaObject is null : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    const-wide/16 v2, 0x0

    .line 434
    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getSupportedOperations()J

    move-result-wide v2

    goto :goto_0
.end method

.method public getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 3

    .prologue
    .line 810
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v1

    .line 811
    .local v1, "topSetPath":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 813
    .local v0, "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    return-object v0
.end method

.method public getTopMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 2
    .param p1, "setType"    # I

    .prologue
    .line 817
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v1

    .line 818
    .local v1, "topSetPath":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 820
    .local v0, "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    return-object v0
.end method

.method public getTopSetPath(I)Ljava/lang/String;
    .locals 1
    .param p1, "typeBits"    # I

    .prologue
    .line 271
    packed-switch p1, :pswitch_data_0

    .line 304
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 272
    :pswitch_1
    const-string v0, "/combo/{/mtp,/local/image,/picasa/image,/sns/image}"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 302
    :goto_0
    return-object v0

    .line 273
    :pswitch_2
    const-string v0, "/combo/{/local/video,/picasa/video,/cloud/video}"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 276
    :pswitch_3
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    if-eqz v0, :cond_1

    .line 277
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v0, :cond_0

    .line 278
    const-string v0, "/combo/{/mtp,/local/all,/cloud/all,/sns/all,/picasa/all,/tCloud/all}"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 280
    :cond_0
    const-string v0, "/combo/{/mtp,/local/all,/cloud/all,/sns/all,/picasa/all}"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 282
    :cond_1
    const-string v0, "/combo/{/mtp,/local/all,/sns/all,/picasa/all}"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 286
    :pswitch_4
    const-string v0, "/local/image"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 287
    :pswitch_5
    const-string v0, "/local/video"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 288
    :pswitch_6
    const-string v0, "/local/all"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 290
    :pswitch_7
    const-string v0, "/combo/{/mtp,/local/all,/cloud/all,/sns/all,/picasa/all}"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 291
    :pswitch_8
    const-string v0, "/combo/{/mtp,/local/image,/picasa/image,/sns/image,/cloud/image}"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 292
    :pswitch_9
    const-string v0, "/cloud/all"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 293
    :pswitch_a
    const-string v0, "/sns/all"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 294
    :pswitch_b
    const-string v0, "/picasa/image"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 295
    :pswitch_c
    const-string v0, "/picasa/video"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 296
    :pswitch_d
    const-string v0, "/picasa/all"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 297
    :pswitch_e
    const-string v0, "/tCloud/all"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 298
    :pswitch_f
    const-string v0, "/hidden/all"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 299
    :pswitch_10
    const-string v0, "/combo/{/mtp,/local/image}"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 300
    :pswitch_11
    const-string v0, "/cloud/image"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 301
    :pswitch_12
    const-string v0, "/cloud/video"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 302
    :pswitch_13
    const-string v0, "/combo/{/local/video,/picasa/video}"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DataManager;->checkRemotePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 271
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_a
        :pswitch_d
        :pswitch_7
        :pswitch_e
        :pswitch_8
        :pswitch_f
        :pswitch_b
        :pswitch_c
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public getTotalTargetCacheSize()J
    .locals 6

    .prologue
    .line 517
    const-wide/16 v2, 0x0

    .line 518
    .local v2, "sum":J
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSource;

    .line 519
    .local v1, "source":Lcom/sec/android/gallery3d/data/MediaSource;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSource;->getTotalTargetCacheSize()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 520
    goto :goto_0

    .line 521
    .end local v1    # "source":Lcom/sec/android/gallery3d/data/MediaSource;
    :cond_0
    return-wide v2
.end method

.method public getTotalUsedCacheSize()J
    .locals 6

    .prologue
    .line 507
    const-wide/16 v2, 0x0

    .line 508
    .local v2, "sum":J
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSource;

    .line 509
    .local v1, "source":Lcom/sec/android/gallery3d/data/MediaSource;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSource;->getTotalUsedCacheSize()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 510
    goto :goto_0

    .line 511
    .end local v1    # "source":Lcom/sec/android/gallery3d/data/MediaSource;
    :cond_0
    return-wide v2
.end method

.method public declared-synchronized initializeSourceMap()V
    .locals 5

    .prologue
    .line 218
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 267
    :cond_0
    monitor-exit p0

    return-void

    .line 221
    :cond_1
    :try_start_1
    new-instance v3, Lcom/sec/android/gallery3d/data/LocalSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/LocalSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 222
    new-instance v3, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 223
    new-instance v3, Lcom/sec/android/gallery3d/data/MtpSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/MtpSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 224
    new-instance v3, Lcom/sec/android/gallery3d/data/ComboSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/ComboSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 225
    new-instance v3, Lcom/sec/android/gallery3d/data/ClusterSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/ClusterSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 226
    new-instance v3, Lcom/sec/android/gallery3d/data/FilterSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/FilterSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 227
    new-instance v3, Lcom/sec/android/gallery3d/data/SecureSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/SecureSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 228
    new-instance v3, Lcom/sec/android/gallery3d/data/SnailSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/SnailSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 232
    new-instance v3, Lcom/sec/android/gallery3d/data/FaceSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/FaceSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 234
    new-instance v3, Lcom/sec/android/gallery3d/remote/sns/SNSSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/remote/sns/SNSSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 235
    new-instance v3, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 236
    new-instance v3, Lcom/sec/android/gallery3d/remote/slink/SLinkSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/remote/slink/SLinkSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 237
    new-instance v3, Lcom/sec/android/gallery3d/data/UriListSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/UriListSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 239
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    if-eqz v3, :cond_2

    .line 242
    new-instance v3, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 244
    :cond_2
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloudMandatory:Z

    if-eqz v3, :cond_3

    .line 245
    new-instance v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    .line 246
    .local v1, "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    iget-boolean v3, v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mInit:Z

    if-eqz v3, :cond_5

    .line 247
    new-instance v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 252
    .end local v1    # "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    :cond_3
    :goto_0
    new-instance v3, Lcom/sec/android/gallery3d/data/AllInOneSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/AllInOneSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 253
    new-instance v3, Lcom/sec/android/gallery3d/data/TimeAllSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/TimeAllSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 254
    new-instance v3, Lcom/sec/android/gallery3d/data/HiddenSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/HiddenSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 255
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v3, :cond_4

    .line 256
    new-instance v3, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 261
    :cond_4
    new-instance v3, Lcom/sec/android/gallery3d/data/UriSource;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/data/UriSource;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->addSource(Lcom/sec/android/gallery3d/data/MediaSource;)V

    .line 262
    iget v3, p0, Lcom/sec/android/gallery3d/data/DataManager;->mActiveCount:I

    if-lez v3, :cond_0

    .line 263
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSource;

    .line 264
    .local v2, "source":Lcom/sec/android/gallery3d/data/MediaSource;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSource;->resume()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 218
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "source":Lcom/sec/android/gallery3d/data/MediaSource;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 249
    .restart local v1    # "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    :cond_5
    const/4 v3, 0x0

    :try_start_2
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V
    .locals 12
    .param p2, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .param p3, "startIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 395
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 399
    .local v5, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 401
    .local v6, "n":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v6, :cond_2

    .line 402
    :try_start_0
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/Path;

    .line 403
    .local v7, "path":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v7, :cond_1

    .line 404
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/Path;->getPrefix()Ljava/lang/String;

    move-result-object v8

    .line 405
    .local v8, "prefix":Ljava/lang/String;
    invoke-virtual {v5, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 406
    .local v2, "group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    if-nez v2, :cond_0

    .line 407
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 408
    .restart local v2    # "group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    invoke-virtual {v5, v8, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    :cond_0
    new-instance v10, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    add-int v11, v3, p3

    invoke-direct {v10, v7, v11}, Lcom/sec/android/gallery3d/data/MediaSource$PathId;-><init>(Lcom/sec/android/gallery3d/data/Path;I)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    .end local v2    # "group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    .end local v8    # "prefix":Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 413
    .end local v7    # "path":Lcom/sec/android/gallery3d/data/Path;
    :catch_0
    move-exception v0

    .line 414
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 415
    sget-object v10, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    const-string v11, "mapMediaItems list size is changed : "

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_2
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 420
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 421
    .restart local v8    # "prefix":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {v10, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaSource;

    .line 422
    .local v9, "source":Lcom/sec/android/gallery3d/data/MediaSource;
    if-eqz v9, :cond_3

    .line 423
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/ArrayList;

    invoke-virtual {v9, v10, p2}, Lcom/sec/android/gallery3d/data/MediaSource;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    goto :goto_1

    .line 425
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;>;"
    .end local v8    # "prefix":Ljava/lang/String;
    .end local v9    # "source":Lcom/sec/android/gallery3d/data/MediaSource;
    :cond_4
    return-void
.end method

.method public move(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "dstPath"    # Ljava/lang/String;
    .param p3, "newFileName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 760
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 761
    .local v0, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v0, :cond_1

    .line 762
    sget-object v5, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "move mediaObject is null : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    :cond_0
    :goto_0
    return v4

    .line 766
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getSupportedOperations()J

    move-result-wide v2

    .line 767
    .local v2, "supported":J
    const-wide/high16 v6, 0x10000000000000L

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    const/4 v1, 0x1

    .line 768
    .local v1, "supportMove":Z
    :goto_1
    if-eqz v1, :cond_0

    .line 769
    invoke-virtual {v0, p2, p3}, Lcom/sec/android/gallery3d/data/MediaObject;->move(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    goto :goto_0

    .end local v1    # "supportMove":Z
    :cond_2
    move v1, v4

    .line 767
    goto :goto_1
.end method

.method public moveItem(Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p2, "dstPath"    # Ljava/lang/String;
    .param p3, "newFileName"    # Ljava/lang/String;

    .prologue
    .line 882
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 883
    :cond_0
    const/4 v1, 0x0

    .line 890
    :goto_0
    return v1

    .line 885
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 887
    .local v0, "path":Lcom/sec/android/gallery3d/data/Path;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/DataManager;->setPowerWakeLock()V

    .line 888
    invoke-virtual {p0, v0, p2, p3}, Lcom/sec/android/gallery3d/data/DataManager;->move(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 890
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/DataManager;->releasePowerWakeLock()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/DataManager;->releasePowerWakeLock()V

    throw v1
.end method

.method public onStitchingProgress(Landroid/net/Uri;I)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "progress"    # I

    .prologue
    .line 623
    return-void
.end method

.method public onStitchingQueued(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 607
    return-void
.end method

.method public onStitchingResult(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 611
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 612
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v1, :cond_0

    .line 613
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 614
    .local v0, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v0, :cond_0

    .line 615
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->clearCachedPanoramaSupport()V

    .line 618
    .end local v0    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 547
    iget v2, p0, Lcom/sec/android/gallery3d/data/DataManager;->mActiveCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/gallery3d/data/DataManager;->mActiveCount:I

    if-nez v2, :cond_0

    .line 548
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSource;

    .line 549
    .local v1, "source":Lcom/sec/android/gallery3d/data/MediaSource;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSource;->pause()V

    goto :goto_0

    .line 552
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "source":Lcom/sec/android/gallery3d/data/MediaSource;
    :cond_0
    return-void
.end method

.method public peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 334
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getObject()Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    return-object v0
.end method

.method public peekMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 659
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getObject()Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public registerChangeNotifier(Landroid/net/Uri;Lcom/sec/android/gallery3d/data/ChangeNotifier;)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "notifier"    # Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .prologue
    .line 525
    const/4 v1, 0x0

    .line 526
    .local v1, "broker":Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager;->mNotifierMap:Ljava/util/HashMap;

    monitor-enter v4

    .line 527
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/DataManager;->mNotifierMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;

    move-object v1, v0

    .line 528
    if-nez v1, :cond_0

    .line 529
    new-instance v2, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/DataManager;->mDefaultMainHandler:Landroid/os/Handler;

    invoke-direct {v2, v3}, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;-><init>(Landroid/os/Handler;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    .end local v1    # "broker":Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;
    .local v2, "broker":Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/DataManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v3, p1, v5, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 532
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/DataManager;->mNotifierMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v1, v2

    .line 534
    .end local v2    # "broker":Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;
    .restart local v1    # "broker":Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;
    :cond_0
    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 535
    invoke-virtual {v1, p2}, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->registerNotifier(Lcom/sec/android/gallery3d/data/ChangeNotifier;)V

    .line 536
    return-void

    .line 534
    :catchall_0
    move-exception v3

    :goto_0
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    .end local v1    # "broker":Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;
    .restart local v2    # "broker":Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "broker":Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;
    .restart local v1    # "broker":Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;
    goto :goto_0
.end method

.method public resume()V
    .locals 4

    .prologue
    .line 539
    iget v2, p0, Lcom/sec/android/gallery3d/data/DataManager;->mActiveCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/gallery3d/data/DataManager;->mActiveCount:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 540
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/DataManager;->mSourceMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSource;

    .line 541
    .local v1, "source":Lcom/sec/android/gallery3d/data/MediaSource;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSource;->resume()V

    goto :goto_0

    .line 544
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "source":Lcom/sec/android/gallery3d/data/MediaSource;
    :cond_0
    return-void
.end method

.method public rotate(Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "degrees"    # I

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 457
    .local v0, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v0, :cond_0

    .line 458
    sget-object v1, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rotate mediaObject is null : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    :goto_0
    return-void

    .line 461
    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/data/MediaObject;->rotate(I)V

    goto :goto_0
.end method

.method public setAlbumCount(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 911
    iput p1, p0, Lcom/sec/android/gallery3d/data/DataManager;->mAlbumCount:I

    .line 912
    return-void
.end method

.method public setChangeNotifierActive(Z)V
    .locals 6
    .param p1, "isActive"    # Z

    .prologue
    const/4 v5, 0x0

    .line 793
    sget-boolean v2, Lcom/sec/android/gallery3d/data/DataManager;->mChangedNotiActive:Z

    if-ne v2, p1, :cond_1

    .line 807
    :cond_0
    return-void

    .line 796
    :cond_1
    sput-boolean p1, Lcom/sec/android/gallery3d/data/DataManager;->mChangedNotiActive:Z

    .line 797
    sget-object v2, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setChangeNotifierState state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/sec/android/gallery3d/data/DataManager;->mChangedNotiActive:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/DataManager;->mNotifierMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;

    .line 799
    .local v0, "broker":Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;
    # getter for: Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiedCount:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->access$300(Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;)I

    move-result v2

    if-eqz v2, :cond_2

    .line 800
    sget-object v2, Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mNotifiedCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiedCount:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->access$300(Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    sget-boolean v2, Lcom/sec/android/gallery3d/data/DataManager;->mChangedNotiActive:Z

    if-eqz v2, :cond_3

    .line 802
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->onChange(Z)V

    .line 804
    :cond_3
    # setter for: Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiedCount:I
    invoke-static {v0, v5}, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->access$302(Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;I)I

    goto :goto_0
.end method

.method public setLocalItemList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 933
    .local p2, "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager;->mItemListHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 934
    return-void
.end method
