.class public Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "CloudAlbumSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;
    }
.end annotation


# static fields
.field private static final BUCKET_ORDER_BY:Ljava/lang/String; = "UPPER(bucket_display_name)"

.field private static final INDEX_BUCKET_ID:I = 0x0

.field private static final INDEX_BUCKET_ID_STRING:I = 0x3

.field private static final INDEX_BUCKET_MEDIA_TYPE:I = 0x2

.field private static final INDEX_BUCKET_NAME:I = 0x1

.field private static final MESSAGE_PENDING_RELOAD:I = 0x0

.field public static final PATH_ALL:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_MERGE_ALL:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_MERGE_IMAGE:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_MERGE_VIDEO:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

.field private static final PROJECTION_BUCKET:[Ljava/lang/String;

.field private static final RELOAD_TIME_GAP:I = 0x5dc

.field private static final TAG:Ljava/lang/String; = "CloudAlbumSet"

.field private static final mBaseUri:Landroid/net/Uri;

.field private static final mPendingReload:Landroid/net/Uri;

.field private static final mWatchUriImage:Landroid/net/Uri;

.field private static final mWatchUriVideo:Landroid/net/Uri;


# instance fields
.field private mAlbums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field

.field private mMainHandler:Landroid/os/Handler;

.field private final mName:Ljava/lang/String;

.field private final mNotifierImage:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private final mNotifierVideo:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private final mPendingNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mSupportShare:Z

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mBaseUri:Landroid/net/Uri;

    .line 46
    const-string v0, "/cloud/all"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_ALL:Lcom/sec/android/gallery3d/data/Path;

    .line 47
    const-string v0, "/cloud/image"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    .line 48
    const-string v0, "/cloud/video"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/cloud/mergedall/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->CLOUD_MERGESET_BUCKETID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_MERGE_ALL:Lcom/sec/android/gallery3d/data/Path;

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/cloud/mergedimage/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->CLOUD_MERGESET_BUCKETID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_MERGE_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/cloud/mergedvideo/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->CLOUD_MERGESET_BUCKETID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_MERGE_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    .line 70
    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Images;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mWatchUriImage:Landroid/net/Uri;

    .line 71
    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Videos;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mWatchUriVideo:Landroid/net/Uri;

    .line 72
    const-string v0, "content://cloudpendingreload"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mPendingReload:Landroid/net/Uri;

    .line 74
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "media_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PROJECTION_BUCKET:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mSupportShare:Z

    .line 90
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mType:I

    .line 91
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mWatchUriImage:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mNotifierImage:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 92
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mWatchUriVideo:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mNotifierVideo:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 93
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mPendingReload:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mPendingNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 94
    new-instance v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$1;-><init>(Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mMainHandler:Landroid/os/Handler;

    .line 105
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v0, :cond_1

    .line 106
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v0, :cond_0

    .line 107
    const-string v0, "Baidu"

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mName:Ljava/lang/String;

    .line 113
    :goto_0
    return-void

    .line 109
    :cond_0
    const-string v0, "Dropbox"

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mName:Ljava/lang/String;

    goto :goto_0

    .line 111
    :cond_1
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0093

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mName:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;)Lcom/sec/android/gallery3d/data/ChangeNotifier;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mPendingNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    return-object v0
.end method

.method private changeToMediaType(I)I
    .locals 1
    .param p1, "cloudMeidType"    # I

    .prologue
    .line 352
    packed-switch p1, :pswitch_data_0

    .line 358
    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 354
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 356
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 352
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getBucketIdString(Landroid/content/ContentResolver;I)Ljava/lang/String;
    .locals 7
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "bucketId"    # I

    .prologue
    .line 306
    sget-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 307
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 309
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v2, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PROJECTION_BUCKET:[Ljava/lang/String;

    const-string v3, "bucket_id = ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 314
    if-nez v6, :cond_1

    .line 315
    const-string v0, "CloudAlbumSet"

    const-string v2, "query fail: "

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    const-string v0, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    if-eqz v6, :cond_0

    .line 321
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v0

    .line 318
    :cond_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 320
    :goto_1
    if-eqz v6, :cond_0

    .line 321
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 318
    :cond_2
    :try_start_2
    const-string v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 320
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 321
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static getBucketName(Landroid/content/ContentResolver;I)Ljava/lang/String;
    .locals 7
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "bucketId"    # I

    .prologue
    .line 286
    sget-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 288
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 290
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v2, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PROJECTION_BUCKET:[Ljava/lang/String;

    const-string v3, "bucket_id = ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 294
    if-nez v6, :cond_0

    .line 295
    const-string v0, "CloudAlbumSet"

    const-string v2, "query fail: "

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    const-string v0, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_0
    return-object v0

    .line 298
    :cond_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 300
    :goto_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 298
    :cond_1
    :try_start_2
    const-string v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 300
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getCloudAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 13
    .param p1, "manager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p2, "type"    # I
    .param p3, "parent"    # Lcom/sec/android/gallery3d/data/Path;
    .param p4, "id"    # I
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "bucketIdString"    # Ljava/lang/String;

    .prologue
    .line 208
    invoke-virtual/range {p3 .. p4}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 209
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v10

    .line 210
    .local v10, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v10, :cond_0

    .line 211
    check-cast v10, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v10    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    move-object v0, v10

    .line 220
    :goto_0
    return-object v0

    .line 213
    .restart local v10    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 230
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :pswitch_1
    new-instance v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v4, 0x1

    move/from16 v3, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 217
    :pswitch_2
    new-instance v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v4, 0x0

    move/from16 v3, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 219
    :pswitch_3
    sget-object v9, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 220
    .local v9, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    const/4 v2, 0x2

    new-array v11, v2, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v12, 0x0

    const/4 v4, 0x2

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v2, :cond_1

    sget-object v5, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_MERGE_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    :goto_1
    move-object v2, p0

    move-object v3, p1

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->getCloudAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x1

    const/4 v4, 0x4

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v2, :cond_2

    sget-object v5, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_MERGE_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    :goto_2
    move-object v2, p0

    move-object v3, p1

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->getCloudAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    aput-object v2, v11, v12

    invoke-direct {v0, v1, v9, v11}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    :cond_1
    sget-object v5, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_1

    :cond_2
    sget-object v5, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_2

    .line 213
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I
    .locals 5
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "name":[Ljava/lang/String;
    array-length v2, v0

    if-ge v2, v1, :cond_0

    .line 118
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 121
    :cond_0
    const-string v2, "all"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "mergedall"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 122
    :cond_1
    const/4 v1, 0x6

    .line 128
    :cond_2
    :goto_0
    return v1

    .line 124
    :cond_3
    const-string v2, "image"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "mergedimage"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 127
    const-string/jumbo v1, "video"

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "mergedvideo"

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 128
    :cond_4
    const/4 v1, 0x4

    goto :goto_0

    .line 130
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private loadBucketEntries(Landroid/database/Cursor;)[Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;
    .locals 9
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v8, 0x2

    .line 260
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 262
    .local v3, "buffer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;>;"
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 263
    const/4 v6, 0x2

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->changeToMediaType(I)I

    move-result v5

    .line 265
    .local v5, "mediaType":I
    iget v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mType:I

    const/4 v7, 0x6

    if-eq v6, v7, :cond_1

    iget v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mType:I

    if-ne v6, v5, :cond_0

    .line 266
    :cond_1
    const/4 v6, 0x0

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 267
    .local v0, "bucketId":I
    const/4 v6, 0x1

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 269
    .local v2, "bucketName":Ljava/lang/String;
    const/4 v6, 0x3

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 270
    .local v1, "bucketIdString":Ljava/lang/String;
    if-eq v5, v8, :cond_2

    const/4 v6, 0x4

    if-ne v5, v6, :cond_0

    .line 272
    :cond_2
    new-instance v4, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;

    invoke-direct {v4, v0, v2, v1}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 273
    .local v4, "entry":Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 274
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 280
    .end local v0    # "bucketId":I
    .end local v1    # "bucketIdString":Ljava/lang/String;
    .end local v2    # "bucketName":Ljava/lang/String;
    .end local v4    # "entry":Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;
    .end local v5    # "mediaType":I
    :catchall_0
    move-exception v6

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v6

    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 282
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;

    return-object v6
.end method


# virtual methods
.method public getBucketId()I
    .locals 2

    .prologue
    .line 413
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v0, :cond_0

    .line 414
    sget v0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->CLOUD_MERGESET_BUCKETID:I

    .line 418
    :goto_0
    return v0

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getBucketId()I

    move-result v0

    goto :goto_0

    .line 418
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 12
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x4

    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 377
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v4, :cond_4

    .line 378
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 379
    .local v1, "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v3, 0x0

    .line 380
    .local v3, "totalItemCount":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 381
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    .line 382
    if-lt p1, v3, :cond_1

    .line 383
    sub-int/2addr p1, v3

    .line 384
    goto :goto_0

    .line 386
    :cond_1
    sub-int v4, v3, p1

    if-ge v4, p2, :cond_2

    .line 387
    sub-int v4, v3, p1

    invoke-virtual {v2, p1, v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 389
    sub-int v4, v3, p1

    sub-int/2addr p2, v4

    .line 390
    const/4 p1, 0x0

    .line 391
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v4

    and-long/2addr v4, v10

    cmp-long v4, v4, v8

    if-nez v4, :cond_0

    .line 392
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mSupportShare:Z

    goto :goto_0

    .line 395
    :cond_2
    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 396
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v4

    and-long/2addr v4, v10

    cmp-long v4, v4, v8

    if-nez v4, :cond_3

    .line 397
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mSupportShare:Z

    .line 404
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v3    # "totalItemCount":I
    :cond_3
    :goto_1
    return-object v1

    :cond_4
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1
.end method

.method public getMediaItemCount()I
    .locals 4

    .prologue
    .line 364
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v3, :cond_0

    .line 365
    const/4 v2, 0x0

    .line 366
    .local v2, "totalItemCount":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 367
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    add-int/2addr v2, v3

    .line 368
    goto :goto_0

    .line 371
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v2    # "totalItemCount":I
    :cond_0
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItemCount()I

    move-result v2

    :cond_1
    return v2
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 332
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v0, :cond_0

    .line 333
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->getSubMediaSetCount(Z)I

    move-result v0

    .line 335
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getSubMediaSetCount(Z)I
    .locals 1
    .param p1, "forMerge"    # Z

    .prologue
    .line 339
    if-eqz p1, :cond_0

    .line 340
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 342
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSupportedOperations()J
    .locals 6

    .prologue
    .line 149
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    const-wide/16 v2, 0x0

    .line 150
    .local v2, "supported":J
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 151
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v4

    and-long/2addr v2, v4

    .line 152
    goto :goto_1

    .line 149
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v2    # "supported":J
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 153
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "supported":J
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mSupportShare:Z

    if-nez v4, :cond_2

    .line 154
    const-wide/16 v4, -0x5

    and-long/2addr v2, v4

    .line 155
    :cond_2
    return-wide v2
.end method

.method public getTotalMediaItemCount()I
    .locals 4

    .prologue
    .line 140
    const/4 v0, 0x0

    .line 141
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 142
    .local v2, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    add-int/2addr v0, v3

    .line 143
    goto :goto_0

    .line 144
    .end local v2    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return v0
.end method

.method public isAlbumSetEmpty()Z
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 425
    const/4 v0, 0x0

    .line 426
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected loadSubMediaSets()Ljava/util/ArrayList;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    sget-object v3, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mBaseUri:Landroid/net/Uri;

    .line 177
    .local v3, "uri":Landroid/net/Uri;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 178
    .local v11, "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    const/4 v13, 0x0

    .line 180
    .local v13, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PROJECTION_BUCKET:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "UPPER(bucket_display_name)"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 183
    if-nez v13, :cond_0

    .line 184
    const-string v2, "CloudAlbumSet"

    const-string v4, "cannot open local database: "

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v11, v2

    .line 203
    .end local v11    # "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    :goto_0
    return-object v11

    .line 188
    .restart local v11    # "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->loadBucketEntries(Landroid/database/Cursor;)[Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;

    move-result-object v15

    .line 189
    .local v15, "entries":[Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v5

    .line 190
    .local v5, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    move-object v12, v15

    .local v12, "arr$":[Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;
    array-length v0, v12

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v18, 0x0

    .local v18, "i$":I
    :goto_1
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_1

    aget-object v16, v12, v18

    .line 191
    .local v16, "entry":Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mType:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v0, v16

    iget v8, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;->bucketId:I

    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;->bucketName:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v10, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;->bucketIdString:Ljava/lang/String;

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->getCloudAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 194
    .end local v16    # "entry":Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;
    :cond_1
    const/16 v17, 0x0

    .local v17, "i":I
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v20

    .local v20, "n":I
    :goto_2
    move/from16 v0, v17

    move/from16 v1, v20

    if-ge v0, v1, :cond_2

    .line 195
    move/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 201
    :cond_2
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 197
    .end local v5    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v12    # "arr$":[Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;
    .end local v15    # "entries":[Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet$BucketEntry;
    .end local v17    # "i":I
    .end local v18    # "i$":I
    .end local v19    # "len$":I
    .end local v20    # "n":I
    :catch_0
    move-exception v14

    .line 198
    .local v14, "e":Ljava/lang/SecurityException;
    :try_start_2
    const-string v2, "CloudAlbumSet"

    const-string v4, "Cloud DB Permission error"

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    invoke-virtual {v14}, Ljava/lang/SecurityException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 201
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v14    # "e":Ljava/lang/SecurityException;
    :catchall_0
    move-exception v2

    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method public reload()J
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 161
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mNotifierImage:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mNotifierVideo:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v1

    or-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 162
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->loadSubMediaSets()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    .line 164
    invoke-static {}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mDataVersion:J

    .line 172
    :cond_0
    :goto_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mDataVersion:J

    return-wide v0

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mMainHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 168
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mPendingNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->loadSubMediaSets()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    .line 170
    invoke-static {}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->mDataVersion:J

    goto :goto_0
.end method
