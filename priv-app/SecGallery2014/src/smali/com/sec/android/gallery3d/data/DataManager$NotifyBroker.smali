.class Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;
.super Landroid/database/ContentObserver;
.source "DataManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/DataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NotifyBroker"
.end annotation


# instance fields
.field private mNotifiedCount:I

.field private mNotifiers:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/sec/android/gallery3d/data/ChangeNotifier;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 566
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 559
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiers:Ljava/util/WeakHashMap;

    .line 567
    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;

    .prologue
    .line 558
    iget v0, p0, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiedCount:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;
    .param p1, "x1"    # I

    .prologue
    .line 558
    iput p1, p0, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiedCount:I

    return p1
.end method


# virtual methods
.method public declared-synchronized onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .prologue
    .line 600
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->onChange(ZLandroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 601
    monitor-exit p0

    return-void

    .line 600
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 577
    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    invoke-virtual {p2}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 578
    # getter for: Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/data/DataManager;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onChange uri = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 596
    :goto_0
    monitor-exit p0

    return-void

    .line 581
    :cond_0
    :try_start_1
    # getter for: Lcom/sec/android/gallery3d/data/DataManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/data/DataManager;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onChange = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiedCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", active = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/gallery3d/data/DataManager;->mChangedNotiActive:Z
    invoke-static {}, Lcom/sec/android/gallery3d/data/DataManager;->access$200()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", uri = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", this = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    # getter for: Lcom/sec/android/gallery3d/data/DataManager;->mChangedNotiActive:Z
    invoke-static {}, Lcom/sec/android/gallery3d/data/DataManager;->access$200()Z

    move-result v3

    if-nez v3, :cond_1

    .line 584
    iget v3, p0, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiedCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiedCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 577
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 587
    :cond_1
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiers:Ljava/util/WeakHashMap;

    monitor-enter v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 589
    :try_start_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiers:Ljava/util/WeakHashMap;

    invoke-virtual {v3}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 590
    .local v2, "notifier":Lcom/sec/android/gallery3d/data/ChangeNotifier;
    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->onChange(Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 592
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "notifier":Lcom/sec/android/gallery3d/data/ChangeNotifier;
    :catch_0
    move-exception v0

    .line 593
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 595
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    monitor-exit v4

    goto :goto_0

    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public declared-synchronized registerNotifier(Lcom/sec/android/gallery3d/data/ChangeNotifier;)V
    .locals 3
    .param p1, "notifier"    # Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .prologue
    .line 570
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiers:Ljava/util/WeakHashMap;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 571
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DataManager$NotifyBroker;->mNotifiers:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 573
    monitor-exit p0

    return-void

    .line 572
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 570
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
