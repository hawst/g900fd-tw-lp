.class public Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;
.super Ljava/lang/Object;
.source "AlbumSlidingWindow.java"

# interfaces
.implements Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;,
        Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$PanoSupportListener;,
        Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;,
        Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;
    }
.end annotation


# static fields
.field private static final JOB_LIMIT:I = 0x2

.field private static final MSG_UPDATE_ENTRY:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AlbumSlidingWindow"


# instance fields
.field private mActiveEnd:I

.field private mActiveRequestCount:I

.field private mActiveStart:I

.field private mContentEnd:I

.field private mContentStart:I

.field private final mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

.field private final mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

.field private mIsActive:Z

.field private mListener:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;

.field private mMiniThumbMode:Z

.field private mSize:I

.field private final mSource:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

.field private final mThreadPool:Lcom/sec/android/gallery3d/util/JobLimiter;

.field private final mTileUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

.field private mTiledTextureResources:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/AlbumDataLoader;I)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "source"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;
    .param p3, "cacheSize"    # I

    .prologue
    const/4 v0, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    .line 73
    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    .line 75
    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    .line 76
    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    .line 80
    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveRequestCount:I

    .line 81
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mIsActive:Z

    .line 100
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mMiniThumbMode:Z

    .line 110
    invoke-virtual {p2, p0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->setDataListener(Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;)V

    .line 111
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSource:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .line 112
    new-array v0, p3, [Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    .line 113
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    .line 115
    new-instance v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$1;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$1;-><init>(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    .line 123
    new-instance v0, Lcom/sec/android/gallery3d/util/JobLimiter;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/util/JobLimiter;-><init>(Lcom/sec/android/gallery3d/util/ThreadPool;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mThreadPool:Lcom/sec/android/gallery3d/util/JobLimiter;

    .line 124
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;-><init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mTileUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    .line 126
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mTiledTextureResources:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    .line 128
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/AlbumDataLoader;IZ)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "source"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;
    .param p3, "cacheSize"    # I
    .param p4, "miniThumbMode"    # Z

    .prologue
    .line 103
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/AlbumDataLoader;I)V

    .line 104
    iput-boolean p4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mMiniThumbMode:Z

    .line 105
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mMiniThumbMode:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/util/JobLimiter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mThreadPool:Lcom/sec/android/gallery3d/util/JobLimiter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mTiledTextureResources:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mTileUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveRequestCount:I

    return v0
.end method

.method static synthetic access$706(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveRequestCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveRequestCount:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->requestNonactiveImages()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;

    return-object v0
.end method

.method private cancelNonactiveImages()V
    .locals 5

    .prologue
    .line 272
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 274
    .local v1, "range":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 275
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    add-int/2addr v2, v0

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->cancelSlotImage(I)V

    .line 276
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->cancelSlotImage(I)V

    .line 274
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278
    :cond_0
    return-void
.end method

.method private cancelSlotImage(I)V
    .locals 3
    .param p1, "slotIndex"    # I

    .prologue
    .line 281
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    if-lt p1, v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    if-lt p1, v1, :cond_1

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    array-length v2, v2

    rem-int v2, p1, v2

    aget-object v0, v1, v2

    .line 283
    .local v0, "item":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->contentLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    if-eqz v1, :cond_0

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->contentLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->cancelLoad()V

    goto :goto_0
.end method

.method private freeSlotContent(I)V
    .locals 4
    .param p1, "slotIndex"    # I

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    .line 288
    .local v0, "data":[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    array-length v3, v0

    rem-int v2, p1, v3

    .line 289
    .local v2, "index":I
    aget-object v1, v0, v2

    .line 290
    .local v1, "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    if-nez v1, :cond_0

    .line 294
    :goto_0
    return-void

    .line 291
    :cond_0
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->contentLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v3

    if-eqz v3, :cond_1

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->contentLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->recycle()V

    .line 292
    :cond_1
    iget-object v3, v1, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->recycle()V

    .line 293
    :cond_2
    const/4 v3, 0x0

    aput-object v3, v0, v2

    goto :goto_0
.end method

.method private prepareSlotContent(I)V
    .locals 4
    .param p1, "slotIndex"    # I

    .prologue
    const/4 v3, 0x0

    .line 297
    new-instance v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;-><init>()V

    .line 298
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSource:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->get(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 299
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iput-object v1, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 300
    if-nez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput v2, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->mediaType:I

    .line 303
    if-nez v1, :cond_1

    const/4 v2, 0x0

    :goto_1
    iput-object v2, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->path:Lcom/sec/android/gallery3d/data/Path;

    .line 304
    if-nez v1, :cond_2

    move v2, v3

    :goto_2
    iput v2, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->rotation:I

    .line 305
    if-nez v1, :cond_3

    :goto_3
    iput-boolean v3, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->is3DTour:Z

    .line 306
    new-instance v2, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v2, p0, p1, v3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;-><init>(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;ILcom/sec/android/gallery3d/data/MediaItem;)V

    # setter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->contentLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->access$002(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;Lcom/sec/android/gallery3d/ui/BitmapLoader;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    .line 307
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    array-length v3, v3

    rem-int v3, p1, v3

    aput-object v0, v2, v3

    .line 308
    return-void

    .line 300
    :cond_0
    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v2

    goto :goto_0

    .line 303
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    goto :goto_1

    .line 304
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v2

    goto :goto_2

    .line 305
    :cond_3
    const-wide/16 v2, 0x400

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v3

    goto :goto_3
.end method

.method private requestNonactiveImages()V
    .locals 5

    .prologue
    .line 248
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 250
    .local v1, "range":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 251
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    add-int/2addr v2, v0

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->requestSlotImage(I)Z

    .line 252
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->requestSlotImage(I)Z

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_0
    return-void
.end method

.method private requestSlotImage(I)Z
    .locals 5
    .param p1, "slotIndex"    # I

    .prologue
    const/4 v2, 0x0

    .line 259
    :try_start_0
    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    if-lt p1, v3, :cond_0

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    if-lt p1, v3, :cond_1

    .line 267
    :cond_0
    :goto_0
    return v2

    .line 260
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    array-length v4, v4

    rem-int v4, p1, v4

    aget-object v1, v3, v4

    .line 261
    .local v1, "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    iget-object v3, v1, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->content:Lcom/sec/android/gallery3d/glrenderer/Texture;

    if-nez v3, :cond_0

    iget-object v3, v1, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v3, :cond_0

    .line 263
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->contentLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->startLoad()V

    .line 264
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->contentLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->isRequestInProgress()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 265
    .end local v1    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :catch_0
    move-exception v0

    .line 266
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private setContentWindow(II)V
    .locals 3
    .param p1, "contentStart"    # I
    .param p2, "contentEnd"    # I

    .prologue
    .line 151
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    if-ne p1, v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    if-ne p2, v2, :cond_0

    .line 186
    :goto_0
    return-void

    .line 153
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mIsActive:Z

    if-nez v2, :cond_1

    .line 154
    iput p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    .line 155
    iput p2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    .line 156
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSource:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->setActiveWindow(II)V

    goto :goto_0

    .line 160
    :cond_1
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    if-ge p1, v2, :cond_2

    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    if-lt v2, p2, :cond_4

    .line 161
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    .local v0, "i":I
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    .local v1, "n":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 162
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->freeSlotContent(I)V

    .line 161
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 164
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSource:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->setActiveWindow(II)V

    .line 165
    move v0, p1

    :goto_2
    if-ge v0, p2, :cond_8

    .line 166
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->prepareSlotContent(I)V

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 169
    .end local v0    # "i":I
    .end local v1    # "n":I
    :cond_4
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    .restart local v0    # "i":I
    :goto_3
    if-ge v0, p1, :cond_5

    .line 170
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->freeSlotContent(I)V

    .line 169
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 172
    :cond_5
    move v0, p2

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    .restart local v1    # "n":I
    :goto_4
    if-ge v0, v1, :cond_6

    .line 173
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->freeSlotContent(I)V

    .line 172
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 175
    :cond_6
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSource:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->setActiveWindow(II)V

    .line 176
    move v0, p1

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    :goto_5
    if-ge v0, v1, :cond_7

    .line 177
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->prepareSlotContent(I)V

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 179
    :cond_7
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    :goto_6
    if-ge v0, p2, :cond_8

    .line 180
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->prepareSlotContent(I)V

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 184
    :cond_8
    iput p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    .line 185
    iput p2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    goto :goto_0
.end method

.method private updateAllImageRequests()V
    .locals 3

    .prologue
    .line 311
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveRequestCount:I

    .line 312
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    .local v0, "i":I
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 313
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->requestSlotImage(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveRequestCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveRequestCount:I

    .line 312
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 315
    :cond_1
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveRequestCount:I

    if-nez v2, :cond_2

    .line 316
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->requestNonactiveImages()V

    .line 320
    :goto_1
    return-void

    .line 318
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->cancelNonactiveImages()V

    goto :goto_1
.end method

.method private updateTextureUploadQueue()V
    .locals 7

    .prologue
    .line 222
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mIsActive:Z

    if-nez v4, :cond_1

    .line 240
    :cond_0
    return-void

    .line 223
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mTileUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->clear()V

    .line 226
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    .local v1, "i":I
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_3

    .line 227
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    array-length v5, v5

    rem-int v5, v1, v5

    aget-object v0, v4, v5

    .line 228
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-eqz v4, :cond_2

    .line 229
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mTileUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    iget-object v5, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->addTexture(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)V

    .line 226
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 234
    .end local v0    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :cond_3
    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    iget v5, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    sub-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 236
    .local v3, "range":I
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    .line 237
    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    add-int/2addr v4, v1

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->uploadBgTextureInSlot(I)V

    .line 238
    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    sub-int/2addr v4, v1

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->uploadBgTextureInSlot(I)V

    .line 236
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private uploadBgTextureInSlot(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 213
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    if-ge p1, v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    if-lt p1, v1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    array-length v2, v2

    rem-int v2, p1, v2

    aget-object v0, v1, v2

    .line 215
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-eqz v1, :cond_0

    .line 216
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mTileUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->addTexture(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)V

    .line 219
    .end local v0    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :cond_0
    return-void
.end method


# virtual methods
.method public checkIfFimStripInitiated()Z
    .locals 6

    .prologue
    .line 432
    const/4 v1, 0x1

    .line 433
    .local v1, "hasBitmapData":Z
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    .line 434
    .local v0, "data":[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    .local v2, "i":I
    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    .local v4, "n":I
    :goto_0
    if-ge v2, v4, :cond_1

    .line 435
    array-length v5, v0

    rem-int v5, v2, v5

    aget-object v3, v0, v5

    .line 436
    .local v3, "item":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    if-eqz v3, :cond_0

    .line 437
    iget-object v5, v3, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v5, :cond_0

    .line 438
    const/4 v1, 0x0

    .line 434
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 441
    .end local v3    # "item":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :cond_1
    return v1
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 425
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    .local v0, "i":I
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 426
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->freeSlotContent(I)V

    .line 425
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 428
    :cond_0
    return-void
.end method

.method public get(I)Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    .locals 3
    .param p1, "slotIndex"    # I

    .prologue
    .line 135
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->isActiveSlot(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    add-int/lit8 p1, v0, -0x1

    .line 139
    const-string v0, "AlbumSlidingWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid slot: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " outsides ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    array-length v1, v1

    rem-int v1, p1, v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 381
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    return v0
.end method

.method public isActiveSlot(I)Z
    .locals 1
    .param p1, "slotIndex"    # I

    .prologue
    .line 147
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onContentChanged(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 386
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    if-ge p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mIsActive:Z

    if-eqz v0, :cond_0

    .line 387
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->freeSlotContent(I)V

    .line 388
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->prepareSlotContent(I)V

    .line 389
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->updateAllImageRequests()V

    .line 390
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->isActiveSlot(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;->onContentChanged()V

    .line 394
    :cond_0
    return-void
.end method

.method public onSizeChanged(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 372
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    if-eq v0, p1, :cond_2

    .line 373
    iput p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    .line 374
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;->onSizeChanged(I)V

    .line 375
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    .line 376
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    if-le v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    .line 378
    :cond_2
    return-void
.end method

.method public pause()V
    .locals 5

    .prologue
    .line 410
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mIsActive:Z

    .line 411
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mTileUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->clear()V

    .line 412
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    .local v1, "i":I
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_3

    .line 413
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->isActiveSlot(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 414
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->freeSlotContent(I)V

    .line 412
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 416
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    array-length v4, v4

    rem-int v4, v1, v4

    aget-object v0, v3, v4

    .line 417
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->content:Lcom/sec/android/gallery3d/glrenderer/Texture;

    if-eqz v3, :cond_2

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->isWaitDisplayed:Z

    if-nez v3, :cond_2

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 418
    :cond_2
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->freeSlotContent(I)V

    goto :goto_1

    .line 422
    .end local v0    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :cond_3
    return-void
.end method

.method public resume()V
    .locals 5

    .prologue
    .line 397
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mIsActive:Z

    .line 398
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentStart:I

    .local v1, "i":I
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mContentEnd:I

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 399
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->isActiveSlot(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 400
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->prepareSlotContent(I)V

    .line 398
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 402
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    array-length v4, v4

    rem-int v4, v1, v4

    aget-object v0, v3, v4

    .line 403
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->prepareSlotContent(I)V

    goto :goto_1

    .line 406
    .end local v0    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->updateAllImageRequests()V

    .line 407
    return-void
.end method

.method public setActiveWindow(II)V
    .locals 8
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v7, 0x0

    .line 189
    if-gt p1, p2, :cond_0

    sub-int v3, p2, p1

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    array-length v4, v4

    if-gt v3, v4, :cond_0

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    if-le p2, v3, :cond_2

    .line 191
    :cond_0
    const-string v3, "AlbumSlidingWindow"

    const-string v4, "%s, %s, %s, %s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    array-length v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget v7, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :cond_1
    :goto_0
    return-void

    .line 194
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    .line 196
    .local v2, "data":[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    iput p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveStart:I

    .line 197
    iput p2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveEnd:I

    .line 201
    if-eq p1, p2, :cond_1

    .line 204
    add-int v3, p1, p2

    div-int/lit8 v3, v3, 0x2

    array-length v4, v2

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    array-length v5, v2

    sub-int/2addr v4, v5

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v7, v4}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v1

    .line 206
    .local v1, "contentStart":I
    array-length v3, v2

    add-int/2addr v3, v1

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mSize:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 207
    .local v0, "contentEnd":I
    invoke-direct {p0, v1, v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->setContentWindow(II)V

    .line 208
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->updateTextureUploadQueue()V

    .line 209
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mIsActive:Z

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->updateAllImageRequests()V

    goto :goto_0
.end method

.method public setListener(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;

    .line 132
    return-void
.end method
