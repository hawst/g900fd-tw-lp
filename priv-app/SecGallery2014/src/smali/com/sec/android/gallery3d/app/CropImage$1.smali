.class Lcom/sec/android/gallery3d/app/CropImage$1;
.super Landroid/os/Handler;
.source "CropImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/CropImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/CropImage;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/CropImage;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v4, 0x7f0f0025

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 307
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 309
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mState:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$000(Lcom/sec/android/gallery3d/app/CropImage;)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 311
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->onSaveClicked()V
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$100(Lcom/sec/android/gallery3d/app/CropImage;)V

    goto :goto_0

    .line 316
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 317
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "is_pressed_cancel"

    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x68

    if-ne v4, v5, :cond_1

    :goto_1
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 318
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/app/CropImage;->setResult(ILandroid/content/Intent;)V

    .line 319
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/CropImage;->finish()V

    goto :goto_0

    :cond_1
    move v1, v2

    .line 317
    goto :goto_1

    .line 323
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CropImage;->access$200(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v4

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->changeFrame(ZZ)V
    invoke-static {v3, v4, v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$300(Lcom/sec/android/gallery3d/app/CropImage;ZZ)V

    .line 324
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CropImage;->access$200(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v4

    if-nez v4, :cond_2

    :goto_2
    # setter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z
    invoke-static {v3, v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$202(Lcom/sec/android/gallery3d/app/CropImage;Z)Z

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_2

    .line 328
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # setter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsSaveEnable:Z
    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$402(Lcom/sec/android/gallery3d/app/CropImage;Z)Z

    .line 329
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mMenu:Landroid/view/Menu;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$500(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/view/Menu;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 330
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mMenu:Landroid/view/Menu;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/CropImage;->access$500(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/view/Menu;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsSaveEnable:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CropImage;->access$400(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v4

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->setMenuEnable(Landroid/view/MenuItem;Z)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/app/CropImage;->access$600(Lcom/sec/android/gallery3d/app/CropImage;Landroid/view/MenuItem;Z)V

    .line 332
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mDoneActionView:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$700(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 333
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mDoneActionView:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$700(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsSaveEnable:Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/CropImage;->access$400(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 334
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mDoneActionView:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$700(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/view/View;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    .line 335
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # setter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsDoneActionViewEnabled:Z
    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$802(Lcom/sec/android/gallery3d/app/CropImage;Z)Z

    goto/16 :goto_0

    .line 340
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # setter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsSaveEnable:Z
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$402(Lcom/sec/android/gallery3d/app/CropImage;Z)Z

    .line 341
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mMenu:Landroid/view/Menu;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$500(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/view/Menu;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 342
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mMenu:Landroid/view/Menu;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/CropImage;->access$500(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/view/Menu;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsSaveEnable:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CropImage;->access$400(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v4

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->setMenuEnable(Landroid/view/MenuItem;Z)V
    invoke-static {v1, v3, v4}, Lcom/sec/android/gallery3d/app/CropImage;->access$600(Lcom/sec/android/gallery3d/app/CropImage;Landroid/view/MenuItem;Z)V

    .line 344
    :cond_4
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mDoneActionView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$700(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 345
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mDoneActionView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$700(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsSaveEnable:Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/CropImage;->access$400(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 346
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mDoneActionView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$700(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/view/View;

    move-result-object v1

    const v3, 0x3f19999a    # 0.6f

    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 347
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage$1;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # setter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsDoneActionViewEnabled:Z
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$802(Lcom/sec/android/gallery3d/app/CropImage;Z)Z

    goto/16 :goto_0

    .line 307
    nop

    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
