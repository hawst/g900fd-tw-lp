.class public Lcom/sec/android/gallery3d/data/LocationClustering;
.super Lcom/sec/android/gallery3d/data/Clustering;
.source "LocationClustering.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/LocationClustering$Point;
    }
.end annotation


# static fields
.field public static final DEFAULT_ZOOM_LEVEL:F = 1.0f

.field private static final MAX_GROUPS:I = 0x1

.field private static final MAX_ITERATIONS:I = 0x800

.field private static final MIN_GROUPS:I = 0x1

.field private static final STOP_CHANGE_RATIO:F = 0.01f


# instance fields
.field bounds:[Lcom/google/android/gms/maps/model/LatLngBounds;

.field private defaultImageDistance:D

.field public imageGroupDistance:D

.field inputPoints:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;"
        }
    .end annotation
.end field

.field private mCanceled:Z

.field private mContext:Landroid/content/Context;

.field private mGeocoder:Lcom/sec/android/gallery3d/util/ReverseGeocoder;

.field private mHandler:Landroid/os/Handler;

.field private mHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNoLocationString:Ljava/lang/String;

.field mPoints:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/LocationClustering$Point;",
            ">;"
        }
    .end annotation
.end field

.field mZoomLevel:F


# direct methods
.method public constructor <init>(Landroid/content/Context;F)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "zoomLevel"    # F

    .prologue
    const/high16 v6, 0x41200000    # 10.0f

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 75
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/Clustering;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mCanceled:Z

    .line 56
    const-wide v0, 0x415e848000000000L    # 8000000.0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->defaultImageDistance:D

    .line 57
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->defaultImageDistance:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->imageGroupDistance:D

    .line 76
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mContext:Landroid/content/Context;

    .line 77
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mNoLocationString:Ljava/lang/String;

    .line 78
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mHandler:Landroid/os/Handler;

    .line 79
    iput p2, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mZoomLevel:F

    .line 81
    const-string v0, "LocationClustering"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "zoom Level = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    float-to-double v2, p2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    cmpl-float v0, v6, p2

    if-lez v0, :cond_0

    const/high16 v0, 0x40a00000    # 5.0f

    cmpg-float v0, v0, p2

    if-gez v0, :cond_0

    .line 85
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->defaultImageDistance:D

    float-to-double v2, p2

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->imageGroupDistance:D

    .line 92
    :goto_0
    return-void

    .line 87
    :cond_0
    cmpl-float v0, p2, v6

    if-ltz v0, :cond_1

    .line 88
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->defaultImageDistance:D

    float-to-double v2, p2

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->imageGroupDistance:D

    goto :goto_0

    .line 90
    :cond_1
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->defaultImageDistance:D

    float-to-double v2, p2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->imageGroupDistance:D

    goto :goto_0
.end method

.method private static generateName(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/util/ReverseGeocoder;)Ljava/lang/String;
    .locals 10
    .param p1, "geocoder"    # Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;",
            "Lcom/sec/android/gallery3d/util/ReverseGeocoder;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 239
    .local p0, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    new-instance v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;

    invoke-direct {v7}, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;-><init>()V

    .line 241
    .local v7, "set":Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 242
    .local v6, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v8, 0x1

    if-ge v0, v8, :cond_4

    .line 243
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .line 244
    .local v1, "item":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    iget-wide v2, v1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    .line 245
    .local v2, "itemLatitude":D
    iget-wide v4, v1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    .line 247
    .local v4, "itemLongitude":D
    iget-wide v8, v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    cmpl-double v8, v8, v2

    if-lez v8, :cond_0

    .line 248
    iput-wide v2, v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    .line 249
    iput-wide v4, v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLongitude:D

    .line 251
    :cond_0
    iget-wide v8, v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    cmpg-double v8, v8, v2

    if-gez v8, :cond_1

    .line 252
    iput-wide v2, v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    .line 253
    iput-wide v4, v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLongitude:D

    .line 255
    :cond_1
    iget-wide v8, v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    cmpl-double v8, v8, v4

    if-lez v8, :cond_2

    .line 256
    iput-wide v2, v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLatitude:D

    .line 257
    iput-wide v4, v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    .line 259
    :cond_2
    iget-wide v8, v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    cmpg-double v8, v8, v4

    if-gez v8, :cond_3

    .line 260
    iput-wide v2, v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLatitude:D

    .line 261
    iput-wide v4, v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    .line 242
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 265
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .end local v2    # "itemLatitude":D
    .end local v4    # "itemLongitude":D
    :cond_4
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->computeAddress(Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method public static getNoLocationName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static kMeans([Lcom/sec/android/gallery3d/data/LocationClustering$Point;[I)[I
    .locals 34
    .param p0, "points"    # [Lcom/sec/android/gallery3d/data/LocationClustering$Point;
    .param p1, "bestK"    # [I

    .prologue
    .line 301
    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v27, v0

    .line 304
    .local v27, "n":I
    const/4 v2, 0x1

    move/from16 v0, v27

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v26

    .line 305
    .local v26, "minK":I
    const/4 v2, 0x1

    move/from16 v0, v27

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v25

    .line 307
    .local v25, "maxK":I
    move/from16 v0, v25

    new-array v14, v0, [Lcom/sec/android/gallery3d/data/LocationClustering$Point;

    .line 308
    .local v14, "center":[Lcom/sec/android/gallery3d/data/LocationClustering$Point;
    move/from16 v0, v25

    new-array v0, v0, [Lcom/sec/android/gallery3d/data/LocationClustering$Point;

    move-object/from16 v18, v0

    .line 309
    .local v18, "groupSum":[Lcom/sec/android/gallery3d/data/LocationClustering$Point;
    move/from16 v0, v25

    new-array v0, v0, [I

    move-object/from16 v17, v0

    .line 310
    .local v17, "groupCount":[I
    move/from16 v0, v27

    new-array v0, v0, [I

    move-object/from16 v19, v0

    .line 312
    .local v19, "grouping":[I
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_0
    move/from16 v0, v20

    move/from16 v1, v25

    if-ge v0, v1, :cond_0

    .line 313
    new-instance v2, Lcom/sec/android/gallery3d/data/LocationClustering$Point;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/data/LocationClustering$Point;-><init>()V

    aput-object v2, v14, v20

    .line 314
    new-instance v2, Lcom/sec/android/gallery3d/data/LocationClustering$Point;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/data/LocationClustering$Point;-><init>()V

    aput-object v2, v18, v20

    .line 312
    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    .line 319
    :cond_0
    const v13, 0x7f7fffff    # Float.MAX_VALUE

    .line 321
    .local v13, "bestScore":F
    move/from16 v0, v27

    new-array v11, v0, [I

    .line 323
    .local v11, "bestGrouping":[I
    const/4 v2, 0x0

    const/4 v3, 0x1

    aput v3, p1, v2

    .line 325
    const/16 v24, 0x0

    .line 326
    .local v24, "lastDistance":F
    const/16 v33, 0x0

    .line 328
    .local v33, "totalDistance":F
    move/from16 v23, v26

    .local v23, "k":I
    :goto_1
    move/from16 v0, v23

    move/from16 v1, v25

    if-gt v0, v1, :cond_d

    .line 330
    div-int v15, v27, v23

    .line 331
    .local v15, "delta":I
    const/16 v20, 0x0

    :goto_2
    move/from16 v0, v20

    move/from16 v1, v23

    if-ge v0, v1, :cond_1

    .line 332
    mul-int v2, v20, v15

    aget-object v28, p0, v2

    .line 333
    .local v28, "p":Lcom/sec/android/gallery3d/data/LocationClustering$Point;
    aget-object v2, v14, v20

    move-object/from16 v0, v28

    iget-wide v4, v0, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->latRad:D

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->latRad:D

    .line 334
    aget-object v2, v14, v20

    move-object/from16 v0, v28

    iget-wide v4, v0, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->lngRad:D

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->lngRad:D

    .line 331
    add-int/lit8 v20, v20, 0x1

    goto :goto_2

    .line 337
    .end local v28    # "p":Lcom/sec/android/gallery3d/data/LocationClustering$Point;
    :cond_1
    const/16 v21, 0x0

    .local v21, "iter":I
    :goto_3
    const/16 v2, 0x800

    move/from16 v0, v21

    if-ge v0, v2, :cond_9

    .line 339
    const/16 v20, 0x0

    :goto_4
    move/from16 v0, v20

    move/from16 v1, v23

    if-ge v0, v1, :cond_2

    .line 340
    aget-object v2, v18, v20

    const-wide/16 v4, 0x0

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->latRad:D

    .line 341
    aget-object v2, v18, v20

    const-wide/16 v4, 0x0

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->lngRad:D

    .line 342
    const/4 v2, 0x0

    aput v2, v17, v20

    .line 339
    add-int/lit8 v20, v20, 0x1

    goto :goto_4

    .line 344
    :cond_2
    const/16 v33, 0x0

    .line 346
    const/16 v20, 0x0

    :goto_5
    move/from16 v0, v20

    move/from16 v1, v27

    if-ge v0, v1, :cond_6

    .line 347
    aget-object v28, p0, v20

    .line 348
    .restart local v28    # "p":Lcom/sec/android/gallery3d/data/LocationClustering$Point;
    const v10, 0x7f7fffff    # Float.MAX_VALUE

    .line 349
    .local v10, "bestDistance":F
    const/4 v12, 0x0

    .line 350
    .local v12, "bestIndex":I
    const/16 v22, 0x0

    .local v22, "j":I
    :goto_6
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_5

    .line 351
    move-object/from16 v0, v28

    iget-wide v2, v0, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->latRad:D

    move-object/from16 v0, v28

    iget-wide v4, v0, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->lngRad:D

    aget-object v6, v14, v22

    iget-wide v6, v6, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->latRad:D

    aget-object v8, v14, v22

    iget-wide v8, v8, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->lngRad:D

    invoke-static/range {v2 .. v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->fastDistanceMeters(DDDD)D

    move-result-wide v2

    double-to-float v0, v2

    move/from16 v16, v0

    .line 356
    .local v16, "distance":F
    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v2, v16, v2

    if-gez v2, :cond_3

    .line 357
    const/16 v16, 0x0

    .line 359
    :cond_3
    cmpg-float v2, v16, v10

    if-gez v2, :cond_4

    .line 360
    move/from16 v10, v16

    .line 361
    move/from16 v12, v22

    .line 350
    :cond_4
    add-int/lit8 v22, v22, 0x1

    goto :goto_6

    .line 364
    .end local v16    # "distance":F
    :cond_5
    aput v12, v19, v20

    .line 365
    aget v2, v17, v12

    add-int/lit8 v2, v2, 0x1

    aput v2, v17, v12

    .line 366
    aget-object v2, v18, v12

    iget-wide v4, v2, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->latRad:D

    move-object/from16 v0, v28

    iget-wide v6, v0, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->latRad:D

    add-double/2addr v4, v6

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->latRad:D

    .line 367
    aget-object v2, v18, v12

    iget-wide v4, v2, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->lngRad:D

    move-object/from16 v0, v28

    iget-wide v6, v0, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->lngRad:D

    add-double/2addr v4, v6

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->lngRad:D

    .line 368
    add-float v33, v33, v10

    .line 346
    add-int/lit8 v20, v20, 0x1

    goto :goto_5

    .line 372
    .end local v10    # "bestDistance":F
    .end local v12    # "bestIndex":I
    .end local v22    # "j":I
    .end local v28    # "p":Lcom/sec/android/gallery3d/data/LocationClustering$Point;
    :cond_6
    const/16 v20, 0x0

    :goto_7
    move/from16 v0, v20

    move/from16 v1, v23

    if-ge v0, v1, :cond_8

    .line 373
    aget v2, v17, v20

    if-lez v2, :cond_7

    .line 374
    aget-object v2, v14, v20

    aget-object v3, v18, v20

    iget-wide v4, v3, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->latRad:D

    aget v3, v17, v20

    int-to-double v6, v3

    div-double/2addr v4, v6

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->latRad:D

    .line 375
    aget-object v2, v14, v20

    aget-object v3, v18, v20

    iget-wide v4, v3, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->lngRad:D

    aget v3, v17, v20

    int-to-double v6, v3

    div-double/2addr v4, v6

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->lngRad:D

    .line 372
    :cond_7
    add-int/lit8 v20, v20, 0x1

    goto :goto_7

    .line 379
    :cond_8
    const/4 v2, 0x0

    cmpl-float v2, v33, v2

    if-eqz v2, :cond_9

    sub-float v2, v24, v33

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float v2, v2, v33

    const v3, 0x3c23d70a    # 0.01f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_a

    .line 387
    :cond_9
    move/from16 v0, v23

    new-array v0, v0, [I

    move-object/from16 v31, v0

    .line 388
    .local v31, "reassign":[I
    const/16 v29, 0x0

    .line 389
    .local v29, "realK":I
    const/16 v20, 0x0

    move/from16 v30, v29

    .end local v29    # "realK":I
    .local v30, "realK":I
    :goto_8
    move/from16 v0, v20

    move/from16 v1, v23

    if-ge v0, v1, :cond_b

    .line 390
    aget v2, v17, v20

    if-lez v2, :cond_f

    .line 391
    add-int/lit8 v29, v30, 0x1

    .end local v30    # "realK":I
    .restart local v29    # "realK":I
    aput v30, v31, v20

    .line 389
    :goto_9
    add-int/lit8 v20, v20, 0x1

    move/from16 v30, v29

    .end local v29    # "realK":I
    .restart local v30    # "realK":I
    goto :goto_8

    .line 383
    .end local v30    # "realK":I
    .end local v31    # "reassign":[I
    :cond_a
    move/from16 v24, v33

    .line 337
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_3

    .line 396
    .restart local v30    # "realK":I
    .restart local v31    # "reassign":[I
    :cond_b
    move/from16 v0, v30

    int-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float v32, v33, v2

    .line 398
    .local v32, "score":F
    cmpg-float v2, v32, v13

    if-gez v2, :cond_e

    .line 399
    move/from16 v13, v32

    .line 400
    const/4 v2, 0x0

    aput v30, p1, v2

    .line 401
    const/16 v20, 0x0

    :goto_a
    move/from16 v0, v20

    move/from16 v1, v27

    if-ge v0, v1, :cond_c

    .line 402
    aget v2, v19, v20

    aget v2, v31, v2

    aput v2, v11, v20

    .line 401
    add-int/lit8 v20, v20, 0x1

    goto :goto_a

    .line 404
    :cond_c
    const/4 v2, 0x0

    cmpl-float v2, v32, v2

    if-nez v2, :cond_e

    .line 409
    .end local v15    # "delta":I
    .end local v21    # "iter":I
    .end local v30    # "realK":I
    .end local v31    # "reassign":[I
    .end local v32    # "score":F
    :cond_d
    return-object v11

    .line 328
    .restart local v15    # "delta":I
    .restart local v21    # "iter":I
    .restart local v30    # "realK":I
    .restart local v31    # "reassign":[I
    .restart local v32    # "score":F
    :cond_e
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_1

    .end local v32    # "score":F
    :cond_f
    move/from16 v29, v30

    .end local v30    # "realK":I
    .restart local v29    # "realK":I
    goto :goto_9
.end method


# virtual methods
.method public getCluster(I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mNames:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/LocationClustering;->getCluster(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCluster(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 279
    const/4 v3, 0x0

    .line 280
    .local v3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mHashMap:Ljava/util/HashMap;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mNoLocationString:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 281
    :cond_0
    const/4 v4, 0x0

    .line 289
    :goto_0
    return-object v4

    .line 282
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mHashMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 283
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    if-eqz v1, :cond_2

    .line 284
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 285
    .restart local v3    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_1
    if-ge v0, v2, :cond_2

    .line 286
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v2    # "n":I
    :cond_2
    move-object v4, v3

    .line 289
    goto :goto_0
.end method

.method public getClusterKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 415
    const/4 v0, 0x0

    return-object v0
.end method

.method public getClusterName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mNames:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNumberOfClusters()I
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mHashMap:Ljava/util/HashMap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mHashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    goto :goto_0
.end method

.method public onCancel()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mCanceled:Z

    .line 100
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mGeocoder:Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocationClustering;->mGeocoder:Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->onCancel()V

    .line 102
    :cond_0
    return-void
.end method

.method public run(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 26
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 117
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/data/LocationClustering;->mCanceled:Z

    .line 118
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v17

    .line 119
    .local v17, "total":I
    if-gtz v17, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 124
    .local v6, "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    move/from16 v3, v20

    invoke-virtual {v0, v1, v6, v2, v3}, Lcom/sec/android/gallery3d/data/LocationClustering;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;IZ)V

    .line 128
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v18, "withLatLong":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v19, "withoutLatLong":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 132
    .local v14, "points":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/LocationClustering$Point;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    if-ge v10, v0, :cond_4

    .line 133
    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .line 134
    .local v16, "s":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    if-nez v16, :cond_2

    .line 132
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 135
    :cond_2
    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    move-wide/from16 v20, v0

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    move-wide/from16 v22, v0

    invoke-static/range {v20 .. v23}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 136
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    new-instance v20, Lcom/sec/android/gallery3d/data/LocationClustering$Point;

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    move-wide/from16 v24, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    move-wide/from16 v3, v24

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/data/LocationClustering$Point;-><init>(DD)V

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 139
    :cond_3
    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 143
    .end local v16    # "s":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    :cond_4
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 145
    .local v8, "clusters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;>;"
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 147
    .local v12, "m":I
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mPoints:Ljava/util/ArrayList;

    .line 151
    if-lez v12, :cond_5

    .line 153
    new-array v15, v12, [Lcom/sec/android/gallery3d/data/LocationClustering$Point;

    .line 154
    .local v15, "pointsArray":[Lcom/sec/android/gallery3d/data/LocationClustering$Point;
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "pointsArray":[Lcom/sec/android/gallery3d/data/LocationClustering$Point;
    check-cast v15, [Lcom/sec/android/gallery3d/data/LocationClustering$Point;

    .line 159
    .restart local v15    # "pointsArray":[Lcom/sec/android/gallery3d/data/LocationClustering$Point;
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    const/4 v10, 0x0

    :goto_3
    if-ge v10, v12, :cond_5

    .line 163
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 203
    .end local v15    # "pointsArray":[Lcom/sec/android/gallery3d/data/LocationClustering$Point;
    :cond_5
    new-instance v20, Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-direct/range {v20 .. v21}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/data/LocationClustering;->mGeocoder:Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    .line 204
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/data/LocationClustering;->mNames:Ljava/util/ArrayList;

    .line 205
    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/data/LocationClustering;->mHashMap:Ljava/util/HashMap;

    .line 206
    const/4 v9, 0x0

    .line 208
    .local v9, "hasUnresolvedAddress":Z
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    .line 209
    .local v7, "cluster":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mCanceled:Z

    move/from16 v20, v0

    if-eqz v20, :cond_7

    .line 210
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/data/LocationClustering;->mCanceled:Z

    .line 229
    .end local v7    # "cluster":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mNames:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 231
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_0

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mHashMap:Ljava/util/HashMap;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mNoLocationString:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mNames:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mNoLocationString:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 213
    .restart local v7    # "cluster":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mGeocoder:Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v7, v0}, Lcom/sec/android/gallery3d/data/LocationClustering;->generateName(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/util/ReverseGeocoder;)Ljava/lang/String;

    move-result-object v13

    .line 215
    .local v13, "name":Ljava/lang/String;
    const-string v20, "LocationClustering"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, " Address = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    if-eqz v13, :cond_9

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mHashMap:Ljava/util/HashMap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    if-nez v20, :cond_8

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mHashMap:Ljava/util/HashMap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v13, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mNames:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 221
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/LocationClustering;->mHashMap:Ljava/util/HashMap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/ArrayList;

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    .line 225
    :cond_9
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 226
    const/4 v9, 0x1

    goto/16 :goto_4
.end method
