.class Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "WidgetPreferenceActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$1;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 49
    # getter for: Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onReceive!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    const-string v1, "appWidgetId"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 54
    .local v0, "deletedWidgetId":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$1;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v1, v1, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    if-ne v1, v0, :cond_0

    .line 55
    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$1;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->finish()V

    .line 57
    :cond_0
    return-void
.end method
