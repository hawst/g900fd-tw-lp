.class public final Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;
.super Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;
.source "PicasaContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;,
        Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;,
        Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$Database;
    }
.end annotation


# static fields
.field public static final ALBUMS_URI:Landroid/net/Uri;

.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.gallery3d.picasa.contentprovider"

.field public static final BASE_URI:Landroid/net/Uri;

.field private static final ID_EDITED_INDEX_PROJECTION:[Ljava/lang/String;

.field private static final ID_EDITED_PROJECTION:[Ljava/lang/String;

.field private static final MINIMUN_STORAGE_TO_CACHE:J = 0x40000000L

.field public static final PHOTOS_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String; = "PicasaContentProvider"

.field private static final TIMEOUT_CONNECTION:I = 0x3a98

.field private static final TIMEOUT_READ:I = 0x3a98

.field private static final WHERE_ACCOUNT:Ljava/lang/String; = "sync_account=?"

.field private static final WHERE_ALBUM_ID:Ljava/lang/String; = "album_id=?"


# instance fields
.field private mActiveAccount:Landroid/accounts/Account;

.field private final mAlbumInstance:Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

.field private mCacheDir:Ljava/lang/String;

.field private final mPhotoInstance:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

.field private volatile mStopSync:Z

.field private mSyncContext:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    const-string v0, "content://com.sec.android.gallery3d.picasa.contentprovider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->BASE_URI:Landroid/net/Uri;

    .line 49
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->BASE_URI:Landroid/net/Uri;

    const-string v1, "photos"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    .line 50
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->BASE_URI:Landroid/net/Uri;

    const-string v1, "albums"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    .line 53
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "date_edited"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->ID_EDITED_PROJECTION:[Ljava/lang/String;

    .line 54
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "date_edited"

    aput-object v1, v0, v3

    const-string v1, "display_index"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->ID_EDITED_INDEX_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;-><init>()V

    .line 61
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mPhotoInstance:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .line 62
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mAlbumInstance:Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mStopSync:Z

    .line 949
    return-void
.end method

.method private allowDownload()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->allowDownloadBySettings(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    :goto_0
    return v0

    .line 77
    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mStopSync:Z

    .line 78
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private deleteAlbum(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "albumId"    # J

    .prologue
    .line 843
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->deleteAlbumPhotos(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 846
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->deleteWithId(Landroid/database/sqlite/SQLiteDatabase;J)Z

    .line 847
    return-void
.end method

.method private deleteAlbumPhotos(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "albumId"    # J

    .prologue
    const/4 v2, 0x0

    .line 852
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v1

    .line 853
    .local v1, "photoTableName":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    .line 854
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 856
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/Entry;->ID_PROJECTION:[Ljava/lang/String;

    const-string v3, "album_id=?"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 859
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 861
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-direct {p0, p2, p3, v2, v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->deletePhotoCache(JJ)V

    .line 862
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 865
    :cond_1
    const-string v0, "album_id=?"

    invoke-virtual {p1, v1, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 869
    if-eqz v8, :cond_2

    .line 870
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 873
    :cond_2
    :goto_0
    return-void

    .line 866
    :catch_0
    move-exception v9

    .line 867
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "PicasaContentProvider"

    const-string v2, "deleteAlbumPhotos::"

    invoke-static {v0, v2, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 869
    if-eqz v8, :cond_2

    .line 870
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 869
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 870
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private deletePhoto(Landroid/database/sqlite/SQLiteDatabase;JJ)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "albumId"    # J
    .param p4, "photoId"    # J

    .prologue
    .line 876
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0, p1, p4, p5}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->deleteWithId(Landroid/database/sqlite/SQLiteDatabase;J)Z

    .line 877
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->deletePhotoCache(JJ)V

    .line 878
    return-void
.end method

.method private deletePhotoCache(JJ)V
    .locals 3
    .param p1, "albumId"    # J
    .param p3, "photoId"    # J

    .prologue
    .line 881
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getCacheFileName(JJ)Ljava/lang/String;

    move-result-object v0

    .line 883
    .local v0, "pathname":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 887
    :goto_0
    return-void

    .line 884
    :catch_0
    move-exception v1

    .line 885
    .local v1, "t":Ljava/lang/Throwable;
    const-string v2, "PicasaContentProvider"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private deleteUser(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "account"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 812
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v8

    .line 813
    .local v8, "albumTableName":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    aput-object p2, v4, v1

    .line 814
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 816
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/Entry;->ID_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v3, "sync_account=?"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 818
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 820
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 822
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->deleteAlbumPhotos(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 823
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 826
    :cond_1
    const-string/jumbo v0, "sync_account=?"

    invoke-virtual {p1, v8, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 829
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "account=?"

    invoke-virtual {p1, v0, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 830
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 834
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 835
    if-eqz v9, :cond_2

    .line 836
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 839
    :cond_2
    :goto_0
    return-void

    .line 831
    :catch_0
    move-exception v10

    .line 832
    .local v10, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "PicasaContentProvider"

    const-string v1, "deleteUser::"

    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 834
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 835
    if-eqz v9, :cond_2

    .line 836
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 834
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 835
    if-eqz v9, :cond_3

    .line 836
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private downloadPhoto(JLjava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p1, "photoId"    # J
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "pathname"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 377
    const/4 v4, 0x0

    .line 378
    .local v4, "os":Ljava/io/OutputStream;
    const/4 v3, 0x0

    .line 380
    .local v3, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    .end local v4    # "os":Ljava/io/OutputStream;
    .local v5, "os":Ljava/io/OutputStream;
    :try_start_1
    new-instance v8, Ljava/net/URL;

    invoke-direct {v8, p3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    .line 382
    .local v1, "connection":Ljava/net/URLConnection;
    const/16 v8, 0x3a98

    invoke-virtual {v1, v8}, Ljava/net/URLConnection;->setConnectTimeout(I)V

    .line 383
    const/16 v8, 0x3a98

    invoke-virtual {v1, v8}, Ljava/net/URLConnection;->setReadTimeout(I)V

    .line 384
    invoke-virtual {v1}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 386
    const/16 v8, 0x1000

    new-array v0, v8, [B

    .line 387
    .local v0, "buffer":[B
    const/4 v8, 0x0

    array-length v9, v0

    invoke-virtual {v3, v0, v8, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    .line 388
    .local v6, "rc":I
    :goto_0
    if-lez v6, :cond_1

    .line 389
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mStopSync:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v8, :cond_0

    .line 399
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 400
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v4, v5

    .end local v0    # "buffer":[B
    .end local v1    # "connection":Ljava/net/URLConnection;
    .end local v5    # "os":Ljava/io/OutputStream;
    .end local v6    # "rc":I
    .restart local v4    # "os":Ljava/io/OutputStream;
    :goto_1
    return v7

    .line 391
    .end local v4    # "os":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "connection":Ljava/net/URLConnection;
    .restart local v5    # "os":Ljava/io/OutputStream;
    .restart local v6    # "rc":I
    :cond_0
    const/4 v8, 0x0

    :try_start_2
    invoke-virtual {v5, v0, v8, v6}, Ljava/io/OutputStream;->write([BII)V

    .line 392
    const/4 v8, 0x0

    array-length v9, v0

    invoke-virtual {v3, v0, v8, v9}, Ljava/io/InputStream;->read([BII)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v6

    goto :goto_0

    .line 394
    :cond_1
    const/4 v7, 0x1

    .line 399
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 400
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v4, v5

    .end local v5    # "os":Ljava/io/OutputStream;
    .restart local v4    # "os":Ljava/io/OutputStream;
    goto :goto_1

    .line 395
    .end local v0    # "buffer":[B
    .end local v1    # "connection":Ljava/net/URLConnection;
    .end local v6    # "rc":I
    :catch_0
    move-exception v2

    .line 396
    .local v2, "ex":Ljava/io/IOException;
    :goto_2
    :try_start_3
    const-string v8, "PicasaContentProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "error during download : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 399
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 400
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .line 399
    .end local v2    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_3
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 400
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v7

    .line 399
    .end local v4    # "os":Ljava/io/OutputStream;
    .restart local v5    # "os":Ljava/io/OutputStream;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "os":Ljava/io/OutputStream;
    .restart local v4    # "os":Ljava/io/OutputStream;
    goto :goto_3

    .line 395
    .end local v4    # "os":Ljava/io/OutputStream;
    .restart local v5    # "os":Ljava/io/OutputStream;
    :catch_1
    move-exception v2

    move-object v4, v5

    .end local v5    # "os":Ljava/io/OutputStream;
    .restart local v4    # "os":Ljava/io/OutputStream;
    goto :goto_2
.end method

.method private getAvailableStorage()J
    .locals 6

    .prologue
    .line 793
    :try_start_0
    new-instance v0, Landroid/os/StatFs;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getCacheDirectory()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 794
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    .line 797
    .end local v0    # "stat":Landroid/os/StatFs;
    :goto_0
    return-wide v2

    .line 795
    :catch_0
    move-exception v1

    .line 796
    .local v1, "t":Ljava/lang/Throwable;
    const-string v2, "PicasaContentProvider"

    const-string v3, "Fail to getAvailableStorage"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 797
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method private getCacheDirName(J)Ljava/lang/String;
    .locals 3
    .param p1, "albumId"    # J

    .prologue
    .line 405
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getCacheDirectory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/picasa-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCacheDirectory()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mCacheDir:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    .line 85
    .local v0, "cacheDir":Ljava/io/File;
    if-nez v0, :cond_1

    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mCacheDir:Ljava/lang/String;

    .line 87
    .end local v0    # "cacheDir":Ljava/io/File;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mCacheDir:Ljava/lang/String;

    return-object v1

    .line 85
    .restart local v0    # "cacheDir":Ljava/io/File;
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getCacheFileName(JJ)Ljava/lang/String;
    .locals 3
    .param p1, "albumId"    # J
    .param p3, "photoId"    # J

    .prologue
    .line 409
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getCacheDirectory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/picasa-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getPhotoCount(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;J)I
    .locals 11
    .param p0, "context"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
    .param p1, "albumId"    # J

    .prologue
    const/4 v3, 0x1

    const/4 v9, 0x0

    .line 768
    const/4 v8, 0x0

    .line 769
    .local v8, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 770
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-array v2, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v2, v9

    .line 773
    .local v2, "projection":[Ljava/lang/String;
    new-array v4, v3, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v9

    .line 778
    .local v4, "albumIdArgs":[Ljava/lang/String;
    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "album_id=?"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "date_edited"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 780
    if-eqz v8, :cond_1

    .line 781
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 785
    if-eqz v8, :cond_0

    .line 786
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return v1

    .line 785
    :cond_1
    if-eqz v8, :cond_2

    .line 786
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    move v1, v9

    goto :goto_0

    .line 785
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_3

    .line 786
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public static isSyncEnabled(Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;)Z
    .locals 6
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "context"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;

    .prologue
    .line 426
    iget-object v4, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    if-nez v4, :cond_0

    .line 427
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->reloadAccounts()V

    .line 429
    :cond_0
    iget-object v1, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    .line 430
    .local v1, "accounts":[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    array-length v3, v1

    .line 431
    .local v3, "numAccounts":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 432
    aget-object v0, v1, v2

    .line 433
    .local v0, "account":Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->user:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 434
    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->account:Landroid/accounts/Account;

    const-string v5, "com.sec.android.gallery3d.picasa.contentprovider"

    invoke-static {v4, v5}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    .line 437
    .end local v0    # "account":Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    :goto_1
    return v4

    .line 431
    .restart local v0    # "account":Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 437
    .end local v0    # "account":Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    :cond_2
    const/4 v4, 0x1

    goto :goto_1
.end method

.method private syncAlbumPhotos(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;Landroid/content/SyncResult;)V
    .locals 31
    .param p1, "context"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "album"    # Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;
    .param p4, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 642
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 643
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->id:J

    move-wide/from16 v18, v0

    .line 644
    .local v18, "albumId":J
    const/4 v5, 0x1

    new-array v8, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v5

    .line 645
    .local v8, "albumIdArgs":[Ljava/lang/String;
    const/16 v23, 0x0

    .line 647
    .local v23, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->ID_EDITED_INDEX_PROJECTION:[Ljava/lang/String;

    const-string v7, "album_id=?"

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, "date_edited"

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 649
    if-nez v23, :cond_1

    .line 650
    const-string v5, "PicasaContentProvider"

    const-string/jumbo v6, "syncAlbumPhotos: failed to get cursor...."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 760
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 761
    if-eqz v23, :cond_0

    .line 762
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 765
    :cond_0
    :goto_0
    return-void

    .line 653
    :cond_1
    :try_start_1
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->getCount()I

    move-result v28

    .line 654
    .local v28, "localCount":I
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 657
    move/from16 v0, v28

    new-array v0, v0, [Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    move-object/from16 v27, v0

    .line 658
    .local v27, "local":[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    new-instance v26, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    invoke-direct/range {v26 .. v26}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;-><init>()V

    .line 659
    .local v26, "key":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    const/16 v25, 0x0

    .local v25, "i":I
    :goto_1
    move/from16 v0, v25

    move/from16 v1, v28

    if-eq v0, v1, :cond_2

    .line 660
    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 662
    new-instance v9, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    const/4 v5, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const/4 v5, 0x1

    move-object/from16 v0, v23

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    const/4 v5, 0x2

    move-object/from16 v0, v23

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    invoke-direct/range {v9 .. v14}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;-><init>(JJI)V

    aput-object v9, v27, v25

    .line 659
    add-int/lit8 v25, v25, 0x1

    goto :goto_1

    .line 665
    :cond_2
    invoke-static/range {v27 .. v27}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 668
    sget-object v15, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    .line 669
    .local v15, "photoSchema":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;
    const/4 v5, 0x1

    new-array v11, v5, [I

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput v6, v11, v5

    .line 670
    .local v11, "displayIndex":[I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v22

    .line 671
    .local v22, "accountManager":Landroid/accounts/AccountManager;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->api:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;

    new-instance v9, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;

    move-object/from16 v10, p0

    move-object/from16 v12, v26

    move-object/from16 v13, v27

    move-object/from16 v14, p2

    move-object/from16 v16, v4

    move-object/from16 v17, p4

    invoke-direct/range {v9 .. v17}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;[ILcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/SyncResult;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p4

    move-object/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2, v9}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->getAlbumPhotos(Landroid/accounts/AccountManager;Landroid/content/SyncResult;Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;)I

    move-result v30

    .line 720
    .local v30, "result":I
    packed-switch v30, :pswitch_data_0

    .line 737
    const/16 v25, 0x0

    :goto_2
    move/from16 v0, v25

    move/from16 v1, v28

    if-eq v0, v1, :cond_6

    .line 738
    aget-object v29, v27, v25

    .line 739
    .local v29, "metadata":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    move-object/from16 v0, v29

    iget-boolean v5, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->survived:Z

    if-nez v5, :cond_3

    .line 740
    move-object/from16 v0, v29

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->id:J

    move-wide/from16 v20, v0

    move-object/from16 v16, p0

    move-object/from16 v17, v4

    invoke-direct/range {v16 .. v21}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->deletePhoto(Landroid/database/sqlite/SQLiteDatabase;JJ)V

    .line 741
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v5, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v12, 0x1

    add-long/2addr v6, v12

    iput-wide v6, v5, Landroid/content/SyncStats;->numDeletes:J

    .line 737
    :cond_3
    add-int/lit8 v25, v25, 0x1

    goto :goto_2

    .line 722
    .end local v29    # "metadata":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    :pswitch_0
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v5, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v6, v12

    iput-wide v6, v5, Landroid/content/SyncStats;->numParseExceptions:J

    .line 723
    const-string v5, "PicasaContentProvider"

    const-string/jumbo v6, "syncAlbumPhotos error"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    :pswitch_1
    move-object/from16 v0, p3

    iget v5, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->valid:I

    if-nez v5, :cond_4

    .line 727
    move-object/from16 v0, p1

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getPhotoCount(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;J)I

    move-result v5

    if-lez v5, :cond_5

    const/4 v5, 0x2

    :goto_3
    move-object/from16 v0, p3

    iput v5, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->valid:I

    .line 729
    sget-object v5, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    move-object/from16 v0, p3

    invoke-virtual {v5, v4, v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/picasa/Entry;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 760
    :cond_4
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 761
    if-eqz v23, :cond_0

    .line 762
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 727
    :cond_5
    const/4 v5, 0x1

    goto :goto_3

    .line 745
    :cond_6
    const/4 v5, 0x0

    :try_start_2
    move-object/from16 v0, p3

    iput-boolean v5, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->photosDirty:Z

    .line 747
    move-object/from16 v0, p1

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getPhotoCount(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;J)I

    move-result v5

    if-lez v5, :cond_7

    const/4 v5, 0x2

    :goto_4
    move-object/from16 v0, p3

    iput v5, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->valid:I

    .line 749
    sget-object v5, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    move-object/from16 v0, p3

    invoke-virtual {v5, v4, v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/picasa/Entry;)J

    .line 754
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v6, v7, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 755
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v6, v7, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 756
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 760
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 761
    if-eqz v23, :cond_0

    .line 762
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 747
    :cond_7
    const/4 v5, 0x1

    goto :goto_4

    .line 757
    .end local v11    # "displayIndex":[I
    .end local v15    # "photoSchema":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;
    .end local v22    # "accountManager":Landroid/accounts/AccountManager;
    .end local v25    # "i":I
    .end local v26    # "key":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    .end local v27    # "local":[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    .end local v28    # "localCount":I
    .end local v30    # "result":I
    :catch_0
    move-exception v24

    .line 758
    .local v24, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v5, "PicasaContentProvider"

    const-string/jumbo v6, "syncAlbumPhotos:: Exception..."

    move-object/from16 v0, v24

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 760
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 761
    if-eqz v23, :cond_0

    .line 762
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 760
    .end local v24    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 761
    if-eqz v23, :cond_8

    .line 762
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v5

    .line 720
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private syncCacheForAlbum(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Ljava/lang/String;JLandroid/content/SyncResult;)V
    .locals 23
    .param p1, "context"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "albumId"    # J
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 274
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 275
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v4, :cond_1

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    const/16 v17, 0x2

    .line 277
    .local v17, "targetStatus":I
    const/16 v21, 0x0

    .line 278
    .local v21, "syncType":Ljava/lang/String;
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mChangePicasaSyncTypeToThumb:Z

    if-eqz v5, :cond_2

    .line 279
    const-string/jumbo v21, "thumbnail_url"

    .line 282
    :goto_1
    const/4 v5, 0x2

    new-array v6, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "_id"

    aput-object v7, v6, v5

    const/4 v5, 0x1

    aput-object v21, v6, v5

    .line 283
    .local v6, "projection":[Ljava/lang/String;
    const/4 v5, 0x2

    new-array v8, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v5

    const/4 v5, 0x1

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v5

    .line 288
    .local v8, "whereArgs":[Ljava/lang/String;
    sget-object v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v5

    const-string v7, "album_id=? AND cache_status < ?"

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 291
    .local v18, "cursor":Landroid/database/Cursor;
    if-eqz v18, :cond_0

    .line 293
    :try_start_0
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_3

    .line 331
    const-string v5, "PicasaContentProvider"

    const-string v7, "end download cache"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 281
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v8    # "whereArgs":[Ljava/lang/String;
    .end local v18    # "cursor":Landroid/database/Cursor;
    :cond_2
    const-string v21, "screennail_url"

    goto :goto_1

    .line 297
    .restart local v6    # "projection":[Ljava/lang/String;
    .restart local v8    # "whereArgs":[Ljava/lang/String;
    .restart local v18    # "cursor":Landroid/database/Cursor;
    :cond_3
    :try_start_1
    const-string v5, "PicasaContentProvider"

    const-string/jumbo v7, "start download cache"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getCacheDirName(J)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v19

    .line 300
    .local v19, "dirName":Ljava/lang/String;
    :try_start_2
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 306
    const/4 v5, 0x1

    :try_start_3
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-direct {v0, v4, v1, v2, v5}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->updateAlbumCacheStatus(Landroid/database/sqlite/SQLiteDatabase;JI)V

    .line 308
    const/16 v20, 0x0

    .line 310
    .local v20, "stopped":Z
    :cond_4
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 311
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mStopSync:Z

    if-nez v5, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->allowDownload()Z

    move-result v5

    if-nez v5, :cond_8

    .line 312
    :cond_5
    const/16 v20, 0x1

    .line 327
    :cond_6
    :goto_2
    if-nez v20, :cond_7

    .line 328
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    move/from16 v3, v17

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->updateAlbumCacheStatus(Landroid/database/sqlite/SQLiteDatabase;JI)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 331
    :cond_7
    const-string v5, "PicasaContentProvider"

    const-string v7, "end download cache"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 301
    .end local v20    # "stopped":Z
    :catch_0
    move-exception v22

    .line 302
    .local v22, "t":Ljava/lang/Throwable;
    :try_start_4
    const-string v5, "PicasaContentProvider"

    const-string v7, "cannot create cache dir: "

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 331
    const-string v5, "PicasaContentProvider"

    const-string v7, "end download cache"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 315
    .end local v22    # "t":Ljava/lang/Throwable;
    .restart local v20    # "stopped":Z
    :cond_8
    const/4 v5, 0x0

    :try_start_5
    move-object/from16 v0, v18

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 316
    .local v14, "photoId":J
    const/4 v5, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 317
    .local v16, "url":Ljava/lang/String;
    if-eqz v16, :cond_4

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-wide/from16 v12, p3

    .line 319
    invoke-direct/range {v10 .. v17}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncOnePhoto(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;JJLjava/lang/String;I)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v5

    if-nez v5, :cond_4

    .line 320
    const/16 v20, 0x1

    .line 321
    goto :goto_2

    .line 331
    .end local v14    # "photoId":J
    .end local v16    # "url":Ljava/lang/String;
    .end local v19    # "dirName":Ljava/lang/String;
    .end local v20    # "stopped":Z
    :catchall_0
    move-exception v5

    const-string v7, "PicasaContentProvider"

    const-string v9, "end download cache"

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    throw v5
.end method

.method private syncCacheForUser(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 12
    .param p1, "context"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 253
    iget-object v0, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 254
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 269
    :goto_0
    return-void

    .line 255
    :cond_0
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    aput-object p2, v4, v2

    .line 258
    .local v4, "whereArgs":[Ljava/lang/String;
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/Entry;->ID_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v3, "sync_account=?"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 261
    .local v11, "cursor":Landroid/database/Cursor;
    :goto_1
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 262
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mStopSync:Z

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->allowDownload()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    .line 267
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 263
    :cond_2
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v11, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    move-object v5, p0

    move-object v6, p1

    move-object v7, p2

    move-object v10, p3

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncCacheForAlbum(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Ljava/lang/String;JLandroid/content/SyncResult;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 267
    :catchall_0
    move-exception v1

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private syncOnePhoto(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;JJLjava/lang/String;I)Z
    .locals 14
    .param p1, "context"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
    .param p2, "albumId"    # J
    .param p4, "photoId"    # J
    .param p6, "url"    # Ljava/lang/String;
    .param p7, "targetStatus"    # I

    .prologue
    .line 349
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getAvailableStorage()J

    move-result-wide v8

    const-wide/32 v10, 0x40000000

    cmp-long v7, v8, v10

    if-gez v7, :cond_0

    .line 350
    const/4 v7, 0x0

    .line 371
    :goto_0
    return v7

    .line 353
    :cond_0
    move-wide/from16 v0, p2

    move-wide/from16 v2, p4

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getCacheFileName(JJ)Ljava/lang/String;

    move-result-object v5

    .line 354
    .local v5, "pathname":Ljava/lang/String;
    move-wide/from16 v0, p4

    move-object/from16 v2, p6

    invoke-direct {p0, v0, v1, v2, v5}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->downloadPhoto(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 355
    const/4 v7, 0x0

    goto :goto_0

    .line 359
    :cond_1
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 360
    .local v6, "values":Landroid/content/ContentValues;
    const-string v7, "cache_status"

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 361
    const-string v7, "cache_pathname"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 363
    .local v4, "file":Ljava/io/File;
    if-eqz v4, :cond_2

    .line 364
    const-string/jumbo v7, "size"

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 366
    :cond_2
    iget-object v7, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->db:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v8, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "_id=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static/range {p4 .. p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v7, v8, v6, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 370
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 371
    const/4 v7, 0x1

    goto :goto_0
.end method

.method private syncUserAlbums(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Lcom/sec/android/gallery3d/remote/picasa/UserEntry;Landroid/content/SyncResult;)V
    .locals 22
    .param p1, "context"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
    .param p2, "user"    # Lcom/sec/android/gallery3d/remote/picasa/UserEntry;
    .param p3, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 510
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 511
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v15, 0x0

    .line 514
    .local v15, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v5, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->ID_EDITED_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v7, "sync_account=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->account:Ljava/lang/String;

    aput-object v11, v8, v9

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, "date_edited"

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 517
    if-nez v15, :cond_1

    .line 518
    const-string v5, "PicasaContentProvider"

    const-string/jumbo v6, "syncUserAlbums:: failed to get cursor...."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 595
    if-eqz v15, :cond_0

    .line 596
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 599
    :cond_0
    :goto_0
    return-void

    .line 522
    :cond_1
    :try_start_1
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v18

    .line 524
    .local v18, "localCount":I
    move/from16 v0, v18

    new-array v0, v0, [Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    move-object/from16 v17, v0

    .line 525
    .local v17, "local":[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    move/from16 v0, v16

    move/from16 v1, v18

    if-eq v0, v1, :cond_2

    .line 526
    invoke-interface/range {v15 .. v16}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 528
    new-instance v5, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    const/4 v6, 0x0

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-interface {v15, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;-><init>(JJI)V

    aput-object v5, v17, v16

    .line 525
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 530
    :cond_2
    invoke-static/range {v17 .. v17}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 533
    sget-object v10, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    .line 534
    .local v10, "albumSchema":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;
    new-instance v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    invoke-direct {v7}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;-><init>()V

    .line 535
    .local v7, "key":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v14

    .line 536
    .local v14, "accountManager":Landroid/accounts/AccountManager;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->api:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;

    move-object/from16 v21, v0

    new-instance v5, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;

    move-object/from16 v6, p0

    move-object/from16 v8, v17

    move-object/from16 v9, p2

    move-object v11, v4

    move-object/from16 v12, p1

    move-object/from16 v13, p3

    invoke-direct/range {v5 .. v13}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;Lcom/sec/android/gallery3d/remote/picasa/UserEntry;Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Landroid/content/SyncResult;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-virtual {v0, v14, v1, v2, v5}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->getAlbums(Landroid/accounts/AccountManager;Landroid/content/SyncResult;Lcom/sec/android/gallery3d/remote/picasa/UserEntry;Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;)I

    move-result v20

    .line 568
    .local v20, "result":I
    packed-switch v20, :pswitch_data_0

    .line 578
    sget-object v5, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    move-object/from16 v0, p2

    invoke-virtual {v5, v4, v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/picasa/Entry;)J

    .line 581
    const/16 v16, 0x0

    :goto_2
    move/from16 v0, v16

    move/from16 v1, v18

    if-eq v0, v1, :cond_4

    .line 582
    aget-object v19, v17, v16

    .line 583
    .local v19, "metadata":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    move-object/from16 v0, v19

    iget-boolean v5, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->survived:Z

    if-nez v5, :cond_3

    .line 584
    move-object/from16 v0, v19

    iget-wide v8, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->id:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v8, v9}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->deleteAlbum(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 585
    move-object/from16 v0, p3

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v5, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v12, 0x1

    add-long/2addr v8, v12

    iput-wide v8, v5, Landroid/content/SyncStats;->numDeletes:J

    .line 581
    :cond_3
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 570
    .end local v19    # "metadata":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    :pswitch_0
    move-object/from16 v0, p3

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v5, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v8, v12

    iput-wide v8, v5, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595
    :pswitch_1
    if-eqz v15, :cond_0

    .line 596
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 591
    :cond_4
    const/4 v5, 0x1

    :try_start_2
    move-object/from16 v0, p1

    iput-boolean v5, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->albumsChanged:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 595
    if-eqz v15, :cond_0

    .line 596
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 592
    .end local v7    # "key":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    .end local v10    # "albumSchema":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;
    .end local v14    # "accountManager":Landroid/accounts/AccountManager;
    .end local v16    # "i":I
    .end local v17    # "local":[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    .end local v18    # "localCount":I
    .end local v20    # "result":I
    :catch_0
    move-exception v5

    .line 595
    if-eqz v15, :cond_0

    .line 596
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 595
    :catchall_0
    move-exception v5

    if-eqz v15, :cond_5

    .line 596
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v5

    .line 568
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private syncUserPhotos(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 12
    .param p1, "context"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 604
    iget-object v0, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 605
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v10, 0x0

    .line 607
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/Entry;->ID_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v3, "sync_account=? AND (photos_dirty=1 OR valid=0)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 609
    if-nez v10, :cond_1

    .line 610
    const-string v1, "PicasaContentProvider"

    const-string/jumbo v2, "syncUserPhotos:: failed to get cursor..."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 630
    if-eqz v10, :cond_0

    .line 631
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 634
    :cond_0
    :goto_0
    return-void

    .line 614
    :cond_1
    :try_start_1
    new-instance v8, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

    invoke-direct {v8}, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;-><init>()V

    .line 615
    .local v8, "album":Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;
    const/4 v11, 0x0

    .local v11, "i":I
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .local v9, "count":I
    :goto_1
    if-eq v11, v9, :cond_4

    .line 616
    invoke-interface {v10, v11}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 617
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3, v8}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->queryWithId(Landroid/database/sqlite/SQLiteDatabase;JLcom/sec/android/gallery3d/remote/picasa/Entry;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 618
    invoke-direct {p0, p1, p2, v8, p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncAlbumPhotos(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;Landroid/content/SyncResult;)V

    .line 622
    :cond_2
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 623
    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 624
    const-string v1, "PicasaContentProvider"

    const-string/jumbo v2, "syncUserPhotos interrupted"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 615
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 630
    :cond_4
    if-eqz v10, :cond_0

    .line 631
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 627
    .end local v8    # "album":Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;
    .end local v9    # "count":I
    .end local v11    # "i":I
    :catch_0
    move-exception v1

    .line 630
    if-eqz v10, :cond_0

    .line 631
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 630
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_5

    .line 631
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1
.end method

.method private syncUsers(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Landroid/content/SyncResult;)[Lcom/sec/android/gallery3d/remote/picasa/UserEntry;
    .locals 12
    .param p1, "context"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
    .param p2, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 442
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->reloadAccounts()V

    .line 443
    iget-object v1, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    .line 444
    .local v1, "accounts":[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    array-length v7, v1

    .line 445
    .local v7, "numUsers":I
    new-array v9, v7, [Lcom/sec/android/gallery3d/remote/picasa/UserEntry;

    .line 448
    .local v9, "users":[Lcom/sec/android/gallery3d/remote/picasa/UserEntry;
    sget-object v8, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    .line 449
    .local v8, "schema":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;
    iget-object v3, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 450
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v3, :cond_1

    .line 502
    :cond_0
    return-object v9

    .line 452
    :cond_1
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 453
    invoke-virtual {v8, v3}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->queryAll(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v2

    .line 455
    .local v2, "cursor":Landroid/database/Cursor;
    if-eqz v2, :cond_5

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 458
    :cond_2
    new-instance v5, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;

    invoke-direct {v5}, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;-><init>()V

    .line 459
    .local v5, "entry":Lcom/sec/android/gallery3d/remote/picasa/UserEntry;
    invoke-virtual {v8, v2, v5}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/picasa/Entry;)Lcom/sec/android/gallery3d/remote/picasa/Entry;

    .line 465
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-eq v6, v7, :cond_3

    .line 466
    aget-object v10, v1, v6

    iget-object v10, v10, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->user:Ljava/lang/String;

    iget-object v11, v5, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->account:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 467
    aput-object v5, v9, v6

    .line 471
    :cond_3
    if-ne v6, v7, :cond_4

    .line 473
    const/4 v10, 0x0

    iput-object v10, v5, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->albumsEtag:Ljava/lang/String;

    .line 474
    iget-object v10, v5, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->account:Ljava/lang/String;

    invoke-direct {p0, v3, v10}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->deleteUser(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 476
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-nez v10, :cond_2

    .line 478
    .end local v5    # "entry":Lcom/sec/android/gallery3d/remote/picasa/UserEntry;
    .end local v6    # "i":I
    :cond_5
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 482
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 483
    if-eqz v2, :cond_6

    .line 484
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 492
    :cond_6
    :goto_1
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_2
    if-eq v6, v7, :cond_0

    .line 493
    aget-object v5, v9, v6

    .line 494
    .restart local v5    # "entry":Lcom/sec/android/gallery3d/remote/picasa/UserEntry;
    aget-object v0, v1, v6

    .line 495
    .local v0, "account":Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    if-nez v5, :cond_7

    .line 496
    new-instance v5, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;

    .end local v5    # "entry":Lcom/sec/android/gallery3d/remote/picasa/UserEntry;
    invoke-direct {v5}, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;-><init>()V

    .line 497
    .restart local v5    # "entry":Lcom/sec/android/gallery3d/remote/picasa/UserEntry;
    iget-object v10, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->user:Ljava/lang/String;

    iput-object v10, v5, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->account:Ljava/lang/String;

    .line 498
    aput-object v5, v9, v6

    .line 492
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 465
    .end local v0    # "account":Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 479
    .end local v5    # "entry":Lcom/sec/android/gallery3d/remote/picasa/UserEntry;
    .end local v6    # "i":I
    :catch_0
    move-exception v4

    .line 480
    .local v4, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 483
    if-eqz v2, :cond_6

    .line 484
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 482
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v10

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 483
    if-eqz v2, :cond_9

    .line 484
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v10
.end method

.method private updateAlbumCacheStatus(Landroid/database/sqlite/SQLiteDatabase;JI)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "albumId"    # J
    .param p4, "status"    # I

    .prologue
    const/4 v5, 0x0

    .line 338
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 339
    .local v0, "values":Landroid/content/ContentValues;
    const-string v2, "cache_status"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 340
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 343
    .local v1, "whereArgs":[Ljava/lang/String;
    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "_id=?"

    invoke-virtual {p1, v2, v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 345
    return-void
.end method


# virtual methods
.method public attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Landroid/content/pm/ProviderInfo;

    .prologue
    .line 93
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V

    .line 94
    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$Database;

    const-string v2, "picasa.db"

    invoke-direct {v1, p1, v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$Database;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->setDatabase(Landroid/database/sqlite/SQLiteOpenHelper;)V

    .line 97
    const-string v1, "com.sec.android.gallery3d.picasa.contentprovider"

    const-string v2, "photos"

    const-string/jumbo v3, "vnd.android.gallery3d.picasa.photo"

    sget-object v4, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->addMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;)V

    .line 98
    const-string v1, "com.sec.android.gallery3d.picasa.contentprovider"

    const-string v2, "albums"

    const-string/jumbo v3, "vnd.android.gallery3d.picasa.album"

    sget-object v4, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->addMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;)V

    .line 99
    const-string v1, "com.sec.android.gallery3d.picasa.contentprovider"

    const-string/jumbo v2, "users"

    const-string/jumbo v3, "vnd.android.gallery3d.picasa.users"

    sget-object v4, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->addMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;)V

    .line 103
    :try_start_0
    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$Database;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$Database;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->setDatabase(Landroid/database/sqlite/SQLiteOpenHelper;)V

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 142
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    .line 143
    .local v9, "path":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.sec.android.gallery3d.picasa.contentprovider"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    .line 144
    :cond_0
    const/4 v0, 0x0

    .line 184
    :goto_0
    return v0

    .line 148
    :cond_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;

    .line 151
    .local v7, "context":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 152
    .local v11, "type":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 153
    .local v4, "id":J
    iget-object v1, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 154
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v0, "photos"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 156
    iget-object v10, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mPhotoInstance:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .line 157
    .local v10, "photo":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0, v1, v4, v5, v10}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->queryWithId(Landroid/database/sqlite/SQLiteDatabase;JLcom/sec/android/gallery3d/remote/picasa/Entry;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 159
    iget-object v0, v10, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->syncAccount:Ljava/lang/String;

    invoke-virtual {v7, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->login(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 160
    iget-object v0, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->api:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;

    iget-object v2, v10, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->editUri:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->deleteEntry(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 161
    iget-wide v2, v10, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->albumId:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->deletePhoto(Landroid/database/sqlite/SQLiteDatabase;JJ)V

    .line 162
    const/4 v0, 0x1

    iput-boolean v0, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->photosChanged:Z

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 164
    .local v8, "cr":Landroid/content/ContentResolver;
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v8, v0, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 165
    const/4 v0, 0x1

    goto :goto_0

    .line 169
    .end local v8    # "cr":Landroid/content/ContentResolver;
    .end local v10    # "photo":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    :cond_2
    const-string v0, "albums"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 171
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mAlbumInstance:Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

    .line 172
    .local v6, "album":Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0, v1, v4, v5, v6}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->queryWithId(Landroid/database/sqlite/SQLiteDatabase;JLcom/sec/android/gallery3d/remote/picasa/Entry;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 174
    iget-object v0, v6, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->syncAccount:Ljava/lang/String;

    invoke-virtual {v7, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->login(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 175
    iget-object v0, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->api:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;

    iget-object v2, v6, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->editUri:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->deleteEntry(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 176
    invoke-direct {p0, v1, v4, v5}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->deleteAlbum(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 177
    const/4 v0, 0x1

    iput-boolean v0, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->albumsChanged:Z

    .line 178
    const/4 v0, 0x1

    goto :goto_0

    .line 183
    .end local v6    # "album":Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;
    :cond_3
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->finish()V

    .line 184
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public deleteAccount(Ljava/lang/String;)V
    .locals 4
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 802
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->deleteUser(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 803
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 804
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 805
    return-void
.end method

.method public reloadAccounts()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->reloadAccounts()V

    .line 189
    return-void
.end method

.method public setActiveSyncAccount(Landroid/accounts/Account;)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mActiveAccount:Landroid/accounts/Account;

    .line 193
    return-void
.end method

.method public setStopCurrentSyncing(Z)V
    .locals 0
    .param p1, "sync"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mStopSync:Z

    .line 70
    return-void
.end method

.method public syncAlbumPhotos(JZLandroid/content/SyncResult;)V
    .locals 5
    .param p1, "albumId"    # J
    .param p3, "forceRefresh"    # Z
    .param p4, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 413
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;

    .line 414
    .local v1, "context":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;-><init>()V

    .line 415
    .local v0, "album":Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;
    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    iget-object v3, v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, v3, p1, p2, v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->queryWithId(Landroid/database/sqlite/SQLiteDatabase;JLcom/sec/android/gallery3d/remote/picasa/Entry;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 416
    iget-boolean v2, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->photosDirty:Z

    if-nez v2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->syncAccount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->login(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 417
    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->syncAccount:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->isSyncEnabled(Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 418
    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->syncAccount:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v0, p4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncAlbumPhotos(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;Landroid/content/SyncResult;)V

    .line 422
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->finish()V

    .line 423
    return-void
.end method

.method public syncUsers(Landroid/content/SyncResult;)V
    .locals 1
    .param p1, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;

    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncUsers(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Landroid/content/SyncResult;)[Lcom/sec/android/gallery3d/remote/picasa/UserEntry;

    .line 197
    return-void
.end method

.method public syncUsersAndAlbums(ZLandroid/content/SyncResult;)V
    .locals 12
    .param p1, "syncAlbumPhotos"    # Z
    .param p2, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 200
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;

    .line 203
    .local v1, "context":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
    invoke-direct {p0, v1, p2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncUsers(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Landroid/content/SyncResult;)[Lcom/sec/android/gallery3d/remote/picasa/UserEntry;

    move-result-object v5

    .line 206
    .local v5, "users":[Lcom/sec/android/gallery3d/remote/picasa/UserEntry;
    const/4 v0, 0x0

    .line 207
    .local v0, "activeUsername":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mActiveAccount:Landroid/accounts/Account;

    if-eqz v6, :cond_0

    .line 208
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mActiveAccount:Landroid/accounts/Account;

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->canonicalizeUsername(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 210
    :cond_0
    const/4 v2, 0x0

    .line 211
    .local v2, "didSyncActiveUserName":Z
    const/4 v3, 0x0

    .local v3, "i":I
    array-length v4, v5

    .local v4, "numUsers":I
    :goto_0
    if-eq v3, v4, :cond_3

    .line 212
    if-eqz v0, :cond_2

    iget-object v6, v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    aget-object v6, v6, v3

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->user:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 211
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 214
    :cond_2
    iget-object v6, v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    aget-object v6, v6, v3

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->account:Landroid/accounts/Account;

    const-string v7, "com.sec.android.gallery3d.picasa.contentprovider"

    invoke-static {v6, v7}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 216
    const/4 v2, 0x1

    .line 217
    iget-object v6, v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->api:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;

    iget-object v7, v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    aget-object v7, v7, v3

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->setAuth(Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;)V

    .line 218
    aget-object v6, v5, v3

    invoke-direct {p0, v1, v6, p2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncUserAlbums(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Lcom/sec/android/gallery3d/remote/picasa/UserEntry;Landroid/content/SyncResult;)V

    .line 219
    if-eqz p1, :cond_1

    .line 220
    aget-object v6, v5, v3

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->account:Ljava/lang/String;

    invoke-direct {p0, v1, v6, p2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncUserPhotos(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Ljava/lang/String;Landroid/content/SyncResult;)V

    goto :goto_1

    .line 229
    :cond_3
    if-eqz p1, :cond_4

    iget-boolean v6, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mStopSync:Z

    if-nez v6, :cond_4

    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->allowDownload()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 231
    const/4 v3, 0x0

    array-length v4, v5

    :goto_2
    if-eq v3, v4, :cond_4

    .line 232
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mStopSync:Z

    if-nez v6, :cond_4

    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->allowDownload()Z

    move-result v6

    if-nez v6, :cond_6

    .line 244
    :cond_4
    if-nez v2, :cond_5

    .line 245
    iget-object v6, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v6, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v6, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 247
    :cond_5
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->finish()V

    .line 248
    return-void

    .line 234
    :cond_6
    if-eqz v0, :cond_8

    iget-object v6, v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    aget-object v6, v6, v3

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->user:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 231
    :cond_7
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 237
    :cond_8
    iget-object v6, v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    aget-object v6, v6, v3

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->account:Landroid/accounts/Account;

    const-string v7, "com.sec.android.gallery3d.picasa.contentprovider"

    invoke-static {v6, v7}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 240
    iget-object v6, v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->api:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;

    iget-object v7, v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    aget-object v7, v7, v3

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->setAuth(Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;)V

    .line 241
    aget-object v6, v5, v3

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->account:Ljava/lang/String;

    invoke-direct {p0, v1, v6, p2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncCacheForUser(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Ljava/lang/String;Landroid/content/SyncResult;)V

    goto :goto_3
.end method
