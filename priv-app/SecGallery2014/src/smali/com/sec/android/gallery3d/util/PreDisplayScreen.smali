.class public Lcom/sec/android/gallery3d/util/PreDisplayScreen;
.super Ljava/lang/Object;
.source "PreDisplayScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;,
        Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mIntent:Landroid/content/Intent;

.field private mOrientation:I

.field private mPreTask:Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;

.field private mState:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

.field private mWaitToComplete:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mBitmap:Landroid/graphics/Bitmap;

    .line 41
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mContext:Landroid/content/Context;

    .line 42
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mIntent:Landroid/content/Intent;

    .line 43
    sget-object v1, Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;->IDLE:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    iput-object v1, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mState:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    .line 44
    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    :goto_0
    return-void

    .line 47
    :cond_0
    sget-object v1, Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;->ACTIVE:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    iput-object v1, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mState:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    .line 48
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mWaitToComplete:Z

    .line 49
    const-class v1, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "threadName":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;-><init>(Lcom/sec/android/gallery3d/util/PreDisplayScreen;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mPreTask:Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;

    .line 51
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mPreTask:Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->start()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/util/PreDisplayScreen;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/util/PreDisplayScreen;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/PreDisplayScreen;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->setPrepareScreenNailResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/util/PreDisplayScreen;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/util/PreDisplayScreen;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/PreDisplayScreen;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mOrientation:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/util/PreDisplayScreen;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/util/PreDisplayScreen;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/PreDisplayScreen;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private applyScreenNail()V
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mBitmap:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mOrientation:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousBitmap(Landroid/graphics/Bitmap;I)V

    .line 56
    return-void
.end method

.method private setPrepareScreenNailResult(Z)V
    .locals 1
    .param p1, "success"    # Z

    .prologue
    .line 90
    monitor-enter p0

    .line 91
    if-eqz p1, :cond_0

    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;->DONE:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    :goto_0
    iput-object v0, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mState:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    .line 92
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mWaitToComplete:Z

    if-nez v0, :cond_1

    .line 93
    monitor-exit p0

    .line 96
    :goto_1
    return-void

    .line 91
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;->FAILED:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    goto :goto_0

    .line 94
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 95
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public checkApplyScreenNail()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 59
    monitor-enter p0

    .line 60
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mState:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    sget-object v3, Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;->IDLE:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mState:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    sget-object v3, Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;->FAILED:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    if-ne v2, v3, :cond_1

    .line 61
    :cond_0
    monitor-exit p0

    move v0, v1

    .line 73
    :goto_0
    return v0

    .line 62
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mState:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    sget-object v3, Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;->DONE:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    if-ne v2, v3, :cond_2

    .line 63
    invoke-direct {p0}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->applyScreenNail()V

    .line 64
    monitor-exit p0

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 66
    :cond_2
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mWaitToComplete:Z

    .line 67
    invoke-static {p0}, Lcom/sec/android/gallery3d/common/Utils;->waitWithoutInterrupt(Ljava/lang/Object;)V

    .line 68
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mState:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    sget-object v3, Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;->DONE:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    if-ne v2, v3, :cond_3

    .line 70
    invoke-direct {p0}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->applyScreenNail()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 73
    goto :goto_0
.end method

.method public hideScreenNail()V
    .locals 2

    .prologue
    .line 78
    monitor-enter p0

    .line 79
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mState:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    sget-object v1, Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;->DONE:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    if-ne v0, v1, :cond_0

    .line 80
    sget-object v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;->IDLE:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mState:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    .line 82
    :cond_0
    monitor-exit p0

    .line 83
    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isScreenNailVisible()Z
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mState:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    sget-object v1, Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;->DONE:Lcom/sec/android/gallery3d/util/PreDisplayScreen$PreviewVisibility;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
