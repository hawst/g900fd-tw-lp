.class public Lcom/sec/android/gallery3d/data/ComboAlbumSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "ComboAlbumSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "ComboAlbumSet"


# instance fields
.field private final mName:Ljava/lang/String;

.field private final mSets:[Lcom/sec/android/gallery3d/data/MediaSet;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;[Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 6
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "mediaSets"    # [Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 37
    invoke-static {}, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->nextVersionNumber()J

    move-result-wide v4

    invoke-direct {p0, p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 38
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    .line 39
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 40
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 39
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 42
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0093

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mName:Ljava/lang/String;

    .line 44
    return-void
.end method

.method private getAlbumGroupingSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_5

    aget-object v5, v0, v2

    .line 144
    .local v5, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->shouldBeGrouped(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->isAlbumSetEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 146
    if-nez p1, :cond_0

    .line 166
    .end local v5    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_1
    return-object v5

    .line 149
    .restart local v5    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 143
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 154
    :cond_2
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v4

    .line 156
    .local v4, "notGroupSetSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v4, :cond_1

    .line 157
    invoke-virtual {v5, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v6

    if-eqz v6, :cond_4

    .line 158
    if-nez p1, :cond_3

    .line 159
    invoke-virtual {v5, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    goto :goto_1

    .line 161
    :cond_3
    add-int/lit8 p1, p1, -0x1

    .line 156
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 166
    .end local v1    # "i":I
    .end local v4    # "notGroupSetSize":I
    .end local v5    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_5
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private getAlbumGroupingSubMediaSetCount()I
    .locals 8

    .prologue
    .line 177
    const/4 v1, 0x0

    .line 178
    .local v1, "count":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v5, v0, v3

    .line 179
    .local v5, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->shouldBeGrouped(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->isAlbumSetEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 181
    add-int/lit8 v1, v1, 0x1

    .line 178
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 185
    :cond_1
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v6

    .line 186
    .local v6, "subMediaSetCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v6, :cond_0

    .line 187
    invoke-virtual {v5, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v7

    if-eqz v7, :cond_2

    .line 188
    add-int/lit8 v1, v1, 0x1

    .line 186
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 193
    .end local v2    # "i":I
    .end local v5    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v6    # "subMediaSetCount":I
    :cond_3
    return v1
.end method

.method private shouldBeGrouped(Lcom/sec/android/gallery3d/data/MediaSet;)Z
    .locals 1
    .param p1, "set"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 170
    instance-of v0, p1, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;

    if-eqz v0, :cond_1

    .line 171
    :cond_0
    const/4 v0, 0x1

    .line 173
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaSets()[Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 49
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAlbumGrouping:Z

    if-eqz v5, :cond_0

    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->getAlbumGroupingSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    .line 61
    :goto_0
    return-object v5

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 55
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v4

    .line 56
    .local v4, "size":I
    if-ge p1, v4, :cond_1

    .line 57
    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    goto :goto_0

    .line 59
    :cond_1
    sub-int/2addr p1, v4

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 61
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v4    # "size":I
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public getSubMediaSetCount()I
    .locals 6

    .prologue
    .line 67
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAlbumGrouping:Z

    if-eqz v5, :cond_1

    .line 68
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->getAlbumGroupingSubMediaSetCount()I

    move-result v1

    .line 76
    :cond_0
    return v1

    .line 72
    :cond_1
    const/4 v1, 0x0

    .line 73
    .local v1, "count":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 74
    .local v4, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v5

    add-int/2addr v1, v5

    .line 73
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getTotalMediaItemCount()I
    .locals 6

    .prologue
    .line 127
    const/4 v1, 0x0

    .line 128
    .local v1, "count":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 129
    .local v4, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v5, v4, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    if-eqz v5, :cond_0

    .line 130
    check-cast v4, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getTotalMediaItemCount()I

    move-result v5

    add-int/2addr v1, v5

    .line 128
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 132
    .restart local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v5

    add-int/2addr v1, v5

    goto :goto_1

    .line 135
    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    return v1
.end method

.method public isLoading()Z
    .locals 3

    .prologue
    .line 86
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v1, v2

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 87
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->isLoading()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 89
    :goto_1
    return v2

    .line 86
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->notifyContentChanged()V

    .line 110
    return-void
.end method

.method public reload()J
    .locals 8

    .prologue
    .line 94
    const/4 v0, 0x0

    .line 96
    .local v0, "changed":Z
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v2, v3

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 97
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v3, v3, v1

    if-eqz v3, :cond_0

    .line 99
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v4

    .line 100
    .local v4, "version":J
    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mDataVersion:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    const/4 v0, 0x1

    .line 96
    .end local v4    # "version":J
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    :cond_1
    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->nextVersionNumber()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mDataVersion:J

    .line 104
    :cond_2
    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mDataVersion:J

    return-wide v6
.end method

.method public requestSync(Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;)Lcom/sec/android/gallery3d/util/Future;
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;",
            ")",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->mSets:[Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->requestSyncOnMultipleSets([Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    return-object v0
.end method
