.class public interface abstract Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;
.super Ljava/lang/Object;
.source "GlPenSelectDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GlPenSelectDetectorListener"
.end annotation


# virtual methods
.method public abstract isReachedBoundary()I
.end method

.method public abstract onMove(II)Z
.end method

.method public abstract onPenEnter(II)Z
.end method

.method public abstract onPenMove(IIZ)Z
.end method

.method public abstract onPenSelect(II)Z
.end method

.method public abstract onPress(II)Z
.end method

.method public abstract onRelease(IIII)Z
.end method
