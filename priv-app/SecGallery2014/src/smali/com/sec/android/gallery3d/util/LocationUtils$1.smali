.class Lcom/sec/android/gallery3d/util/LocationUtils$1;
.super Ljava/lang/Object;
.source "LocationUtils.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/util/LocationUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/util/LocationUtils;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/util/LocationUtils;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/LocationUtils$1;->this$0:Lcom/sec/android/gallery3d/util/LocationUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 1
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/LocationUtils$1;->this$0:Lcom/sec/android/gallery3d/util/LocationUtils;

    # setter for: Lcom/sec/android/gallery3d/util/LocationUtils;->mCurLocation:Landroid/location/Location;
    invoke-static {v0, p1}, Lcom/sec/android/gallery3d/util/LocationUtils;->access$002(Lcom/sec/android/gallery3d/util/LocationUtils;Landroid/location/Location;)Landroid/location/Location;

    .line 74
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 69
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 65
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "i"    # I
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 61
    return-void
.end method
