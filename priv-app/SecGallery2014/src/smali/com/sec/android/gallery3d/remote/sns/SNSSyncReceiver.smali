.class public Lcom/sec/android/gallery3d/remote/sns/SNSSyncReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SNSSyncReceiver.java"


# static fields
.field public static final ACTION_SNS_SYNC_COMPLETE:Ljava/lang/String; = "com.sec.android.app.sns3.action.SYNC_GALLERY"

.field public static final TAG:Ljava/lang/String; = "SNSSyncReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 30
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.sec.android.app.sns3.action.SYNC_GALLERY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    const-string v1, "SNSSyncReceiver"

    const-string v2, "Receive action.SYNC_GALLERY"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->startSync(Landroid/content/Context;)V

    .line 35
    :cond_0
    return-void
.end method
