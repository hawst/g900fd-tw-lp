.class public Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;
.super Ljava/lang/Object;
.source "MtpDeviceIndex.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$1;,
        Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;,
        Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$MtpObjectTimestampComparator;,
        Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;,
        Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;,
        Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;,
        Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;
    }
.end annotation


# static fields
.field public static final FORMAT_MOV:I = 0x300d

.field public static final SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final SUPPORTED_VIDEO_FORMATS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sInstance:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

.field private static final sMtpObjectComparator:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$MtpObjectTimestampComparator;


# instance fields
.field private mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

.field private mCachedReverseBuckets:[Ljava/lang/Object;

.field private mDevice:Landroid/mtp/MtpDevice;

.field private mGeneration:I

.field private mMtpObjects:[Landroid/mtp/MtpObjectInfo;

.field private mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

.field private mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

.field private mUnifiedLookupIndex:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;

    .line 74
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;

    const/16 v1, 0x3808

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 75
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;

    const/16 v1, 0x3801

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 76
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;

    const/16 v1, 0x380b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 77
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;

    const/16 v1, 0x3807

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;

    const/16 v1, 0x3804

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 80
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_VIDEO_FORMATS:Ljava/util/Set;

    .line 81
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_VIDEO_FORMATS:Ljava/util/Set;

    const v1, 0xb984

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 82
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_VIDEO_FORMATS:Ljava/util/Set;

    const/16 v1, 0x300a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 83
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_VIDEO_FORMATS:Ljava/util/Set;

    const v1, 0xb982

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 84
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_VIDEO_FORMATS:Ljava/util/Set;

    const/16 v1, 0x300b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 122
    new-instance v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->sInstance:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    .line 123
    new-instance v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$MtpObjectTimestampComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$MtpObjectTimestampComparator;-><init>(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$1;)V

    sput-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->sMtpObjectComparator:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$MtpObjectTimestampComparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mGeneration:I

    .line 119
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Uninitialized:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    .line 131
    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;[I)[I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;
    .param p1, "x1"    # [I

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;[Landroid/mtp/MtpObjectInfo;)[Landroid/mtp/MtpObjectInfo;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;
    .param p1, "x1"    # [Landroid/mtp/MtpObjectInfo;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    return-object p1
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;)[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;
    .param p1, "x1"    # [Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->resetState()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->computeReversedBuckets()V

    return-void
.end method

.method static synthetic access$900()Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$MtpObjectTimestampComparator;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->sMtpObjectComparator:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$MtpObjectTimestampComparator;

    return-object v0
.end method

.method private computeReversedBuckets()V
    .locals 4

    .prologue
    .line 597
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    array-length v1, v1

    new-array v1, v1, [Ljava/lang/Object;

    iput-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mCachedReverseBuckets:[Ljava/lang/Object;

    .line 598
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mCachedReverseBuckets:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 599
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mCachedReverseBuckets:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v0

    aget-object v2, v2, v3

    aput-object v2, v1, v0

    .line 598
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 601
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->sInstance:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    return-object v0
.end method

.method private resetState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 418
    iget v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mGeneration:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mGeneration:I

    .line 419
    iput-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    .line 420
    iput-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    .line 421
    iput-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    .line 422
    iput-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mCachedReverseBuckets:[Ljava/lang/Object;

    .line 423
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mDevice:Landroid/mtp/MtpDevice;

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Uninitialized:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    :goto_0
    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    .line 424
    return-void

    .line 423
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Initialized:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    goto :goto_0
.end method


# virtual methods
.method public get(ILcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;)Ljava/lang/Object;
    .locals 5
    .param p1, "position"    # I
    .param p2, "order"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    .prologue
    .line 206
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    sget-object v3, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Finished:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    if-eq v2, v3, :cond_0

    const/4 v2, 0x0

    .line 221
    :goto_0
    return-object v2

    .line 207
    :cond_0
    sget-object v2, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;->Ascending:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    if-ne p2, v2, :cond_2

    .line 208
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    aget v3, v3, p1

    aget-object v0, v2, v3

    .line 209
    .local v0, "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    iget v2, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedStartIndex:I

    if-ne v2, p1, :cond_1

    .line 210
    iget-object v2, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->bucket:Lcom/sec/android/gallery3d/ingest/SimpleDate;

    goto :goto_0

    .line 212
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    iget v3, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->itemsStartIndex:I

    add-int/2addr v3, p1

    add-int/lit8 v3, v3, -0x1

    iget v4, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedStartIndex:I

    sub-int/2addr v3, v4

    aget-object v2, v2, v3

    goto :goto_0

    .line 216
    .end local v0    # "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    sub-int v1, v2, p1

    .line 217
    .local v1, "zeroIndex":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    aget v3, v3, v1

    aget-object v0, v2, v3

    .line 218
    .restart local v0    # "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    iget v2, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedEndIndex:I

    if-ne v2, v1, :cond_3

    .line 219
    iget-object v2, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->bucket:Lcom/sec/android/gallery3d/ingest/SimpleDate;

    goto :goto_0

    .line 221
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    iget v3, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->itemsStartIndex:I

    add-int/2addr v3, v1

    iget v4, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedStartIndex:I

    sub-int/2addr v3, v4

    aget-object v2, v2, v3

    goto :goto_0
.end method

.method public getBucketNumberForPosition(ILcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;)I
    .locals 3
    .param p1, "position"    # I
    .param p2, "order"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    .prologue
    .line 315
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;->Ascending:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    if-ne p2, v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    aget v0, v0, p1

    .line 318
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, p1

    aget v1, v1, v2

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public getBuckets(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;)[Ljava/lang/Object;
    .locals 1
    .param p1, "order"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    .prologue
    .line 334
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 341
    :goto_0
    return-object v0

    .line 335
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;->Ascending:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    if-ne p1, v0, :cond_1

    .line 336
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    goto :goto_0

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mCachedReverseBuckets:[Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 339
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->computeReversedBuckets()V

    .line 341
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mCachedReverseBuckets:[Ljava/lang/Object;

    goto :goto_0
.end method

.method public declared-synchronized getDevice()Landroid/mtp/MtpDevice;
    .locals 1

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mDevice:Landroid/mtp/MtpDevice;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getFirstPositionForBucketNumber(ILcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;)I
    .locals 3
    .param p1, "bucketNumber"    # I
    .param p2, "order"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    .prologue
    .line 307
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;->Ascending:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    if-ne p2, v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedStartIndex:I

    .line 310
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    array-length v0, v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, p1

    aget-object v1, v1, v2

    iget v1, v1, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedEndIndex:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public declared-synchronized getIndexRunnable()Ljava/lang/Runnable;
    .locals 2

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    sget-object v1, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Initialized:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    .line 160
    :goto_0
    monitor-exit p0

    return-object v0

    .line 159
    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Pending:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    .line 160
    new-instance v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mDevice:Landroid/mtp/MtpDevice;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;-><init>(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;Landroid/mtp/MtpDevice;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPositionFromPositionWithoutLabels(ILcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;)I
    .locals 7
    .param p1, "position"    # I
    .param p2, "order"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    .prologue
    const/4 v4, -0x1

    .line 252
    iget-object v5, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    sget-object v6, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Finished:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    if-eq v5, v6, :cond_1

    .line 278
    :cond_0
    :goto_0
    return v4

    .line 253
    :cond_1
    sget-object v5, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;->Descending:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    if-ne p2, v5, :cond_2

    .line 254
    iget-object v5, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    sub-int p1, v5, p1

    .line 256
    :cond_2
    const/4 v0, 0x0

    .line 257
    .local v0, "bucketNumber":I
    const/4 v3, 0x0

    .line 258
    .local v3, "iMin":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    array-length v5, v5

    add-int/lit8 v1, v5, -0x1

    .line 259
    .local v1, "iMax":I
    :goto_1
    if-lt v1, v3, :cond_5

    .line 260
    add-int v5, v1, v3

    div-int/lit8 v2, v5, 0x2

    .line 261
    .local v2, "iMid":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    aget-object v5, v5, v2

    iget v5, v5, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->itemsStartIndex:I

    iget-object v6, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    aget-object v6, v6, v2

    iget v6, v6, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->numItems:I

    add-int/2addr v5, v6

    if-gt v5, p1, :cond_3

    .line 262
    add-int/lit8 v3, v2, 0x1

    goto :goto_1

    .line 263
    :cond_3
    iget-object v5, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    aget-object v5, v5, v2

    iget v5, v5, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->itemsStartIndex:I

    if-le v5, p1, :cond_4

    .line 264
    add-int/lit8 v1, v2, -0x1

    goto :goto_1

    .line 266
    :cond_4
    move v0, v2

    .line 270
    .end local v2    # "iMid":I
    :cond_5
    iget-object v5, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    array-length v5, v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    array-length v5, v5

    if-eqz v5, :cond_0

    .line 273
    iget-object v5, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    aget-object v5, v5, v0

    iget v5, v5, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedStartIndex:I

    add-int/2addr v5, p1

    iget-object v6, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    aget-object v6, v6, v0

    iget v6, v6, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->itemsStartIndex:I

    sub-int v4, v5, v6

    .line 275
    .local v4, "mappedPos":I
    sget-object v5, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;->Descending:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    if-ne p2, v5, :cond_0

    .line 276
    iget-object v5, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    sub-int v4, v5, v4

    goto :goto_0
.end method

.method public getPositionWithoutLabelsFromPosition(ILcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;)I
    .locals 5
    .param p1, "position"    # I
    .param p2, "order"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    .prologue
    const/4 v2, -0x1

    .line 282
    iget-object v3, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    sget-object v4, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Finished:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    if-eq v3, v4, :cond_1

    .line 294
    :cond_0
    :goto_0
    return v2

    .line 283
    :cond_1
    sget-object v3, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;->Ascending:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    if-ne p2, v3, :cond_3

    .line 284
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    aget v3, v3, p1

    aget-object v0, v2, v3

    .line 285
    .local v0, "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    iget v2, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedStartIndex:I

    if-ne v2, p1, :cond_2

    add-int/lit8 p1, p1, 0x1

    .line 286
    :cond_2
    iget v2, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->itemsStartIndex:I

    add-int/2addr v2, p1

    add-int/lit8 v2, v2, -0x1

    iget v3, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedStartIndex:I

    sub-int/2addr v2, v3

    goto :goto_0

    .line 288
    .end local v0    # "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    sub-int v1, v3, p1

    .line 289
    .local v1, "zeroIndex":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    array-length v3, v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    array-length v3, v3

    if-eqz v3, :cond_0

    .line 292
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    aget v3, v3, v1

    aget-object v0, v2, v3

    .line 293
    .restart local v0    # "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    iget v2, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedEndIndex:I

    if-ne v2, v1, :cond_4

    add-int/lit8 v1, v1, -0x1

    .line 294
    :cond_4
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    iget v3, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->itemsStartIndex:I

    sub-int/2addr v2, v3

    sub-int/2addr v2, v1

    iget v3, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedStartIndex:I

    add-int/2addr v2, v3

    goto :goto_0
.end method

.method public declared-synchronized getProgress()Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;
    .locals 1

    .prologue
    .line 168
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getWithoutLabels(ILcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;)Landroid/mtp/MtpObjectInfo;
    .locals 2
    .param p1, "position"    # I
    .param p2, "order"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    sget-object v1, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Finished:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    .line 237
    :goto_0
    return-object v0

    .line 234
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;->Ascending:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    if-ne p2, v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    aget-object v0, v0, p1

    goto :goto_0

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, p1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 90
    const/16 v0, 0x1f

    .line 91
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 92
    .local v1, "result":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mDevice:Landroid/mtp/MtpDevice;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 93
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mGeneration:I

    add-int v1, v2, v3

    .line 94
    return v1

    .line 92
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mDevice:Landroid/mtp/MtpDevice;

    invoke-virtual {v2}, Landroid/mtp/MtpDevice;->getDeviceId()I

    move-result v2

    goto :goto_0
.end method

.method public declared-synchronized indexReady()Z
    .locals 2

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    sget-object v1, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Finished:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isFirstInBucket(ILcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;)Z
    .locals 4
    .param p1, "position"    # I
    .param p2, "order"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 323
    sget-object v2, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;->Ascending:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$SortOrder;

    if-ne p2, v2, :cond_2

    .line 324
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    aget v3, v3, p1

    aget-object v2, v2, v3

    iget v2, v2, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedStartIndex:I

    if-ne v2, p1, :cond_1

    .line 327
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 324
    goto :goto_0

    .line 326
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    sub-int p1, v2, p1

    .line 327
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    aget v3, v3, p1

    aget-object v2, v2, v3

    iget v2, v2, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedEndIndex:I

    if-eq v2, p1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public declared-synchronized setDevice(Landroid/mtp/MtpDevice;)V
    .locals 1
    .param p1, "device"    # Landroid/mtp/MtpDevice;

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mDevice:Landroid/mtp/MtpDevice;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v0, :cond_0

    .line 148
    :goto_0
    monitor-exit p0

    return-void

    .line 146
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mDevice:Landroid/mtp/MtpDevice;

    .line 147
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->resetState()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setProgressListener(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    .prologue
    .line 177
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    .line 178
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    sget-object v1, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Finished:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sizeWithoutLabels()I
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    sget-object v1, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Finished:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized unsetProgressListener(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    .prologue
    .line 187
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    if-ne v0, p1, :cond_0

    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    :cond_0
    monitor-exit p0

    return-void

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
