.class Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$1;
.super Landroid/os/Handler;
.source "WifiDisplayAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 42
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 44
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->access$000(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->access$000(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->setWifiEnabled(Z)V

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->access$100(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->access$100(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->show()V

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
