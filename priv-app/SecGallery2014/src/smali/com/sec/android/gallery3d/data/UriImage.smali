.class public Lcom/sec/android/gallery3d/data/UriImage;
.super Lcom/sec/android/gallery3d/data/MediaItem;
.source "UriImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;,
        Lcom/sec/android/gallery3d/data/UriImage$RegionDecoderJob;
    }
.end annotation


# static fields
.field static final ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final STATE_DOWNLOADED:I = 0x2

.field private static final STATE_DOWNLOADING:I = 0x1

.field private static final STATE_ERROR:I = -0x1

.field private static final STATE_INIT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "UriImage"


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mCacheEntry:Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

.field private mContentType:Ljava/lang/String;

.field private mFileDescriptor:Landroid/os/ParcelFileDescriptor;

.field private mFileSize:J

.field private mHeight:I

.field private mPanoramaMetadata:Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;

.field private mRotation:I

.field private mState:I

.field private final mUri:Landroid/net/Uri;

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-string v0, "/uri/mediaset/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/UriImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Landroid/net/Uri;)V
    .locals 4
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 80
    invoke-static {}, Lcom/sec/android/gallery3d/data/UriImage;->nextVersionNumber()J

    move-result-wide v2

    invoke-direct {p0, p2, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 67
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mState:I

    .line 71
    new-instance v1, Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;-><init>(Lcom/sec/android/gallery3d/data/MediaObject;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mPanoramaMetadata:Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;

    .line 75
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/UriImage;->mFileSize:J

    .line 81
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    .line 82
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryApp;

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 83
    const-string v1, "file"

    invoke-virtual {p3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 84
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v1

    invoke-virtual {p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isDrm(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->isdrm:Z

    .line 91
    :cond_0
    :goto_0
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/data/UriImage;->getMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mContentType:Ljava/lang/String;

    .line 92
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/UriImage;->updateAttribute()V

    .line 93
    return-void

    .line 86
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/data/UriImage;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "filePath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 88
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isDrm(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->isdrm:Z

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/UriImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/UriImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/UriImage;->prepareInputFile(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/data/UriImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/UriImage;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/data/UriImage;)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/UriImage;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/data/UriImage;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/UriImage;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mWidth:I

    return p1
.end method

.method static synthetic access$502(Lcom/sec/android/gallery3d/data/UriImage;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/UriImage;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mHeight:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/data/UriImage;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/UriImage;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method private getMimeType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 452
    const-string v3, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 453
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/data/UriImage;->isdrm:Z

    if-eqz v3, :cond_1

    .line 454
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 486
    :cond_0
    :goto_0
    return-object v2

    .line 458
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 460
    .local v0, "extension":Ljava/lang/String;
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 462
    .local v2, "type":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 465
    if-nez v2, :cond_6

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/UriImage;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 467
    .local v1, "filePath":Ljava/lang/String;
    if-eqz v1, :cond_2

    const-string v3, ".gif"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 468
    const-string v2, "image/gif"

    goto :goto_0

    .line 469
    :cond_2
    if-eqz v1, :cond_3

    const-string v3, ".golf"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 470
    const-string v2, "image/golf"

    goto :goto_0

    .line 471
    :cond_3
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, ".mpo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 472
    const-string v2, "image/mpo"

    goto :goto_0

    .line 475
    .end local v0    # "extension":Ljava/lang/String;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "type":Ljava/lang/String;
    :cond_4
    const-string v3, "http"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 476
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->parseMimetypeFromHttpUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 478
    :cond_5
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/data/UriImage;->isdrm:Z

    if-eqz v3, :cond_6

    .line 479
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/data/UriImage;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 480
    .restart local v1    # "filePath":Ljava/lang/String;
    if-eqz v1, :cond_6

    .line 481
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 486
    .end local v1    # "filePath":Ljava/lang/String;
    :cond_6
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private isSharable()Z
    .locals 2

    .prologue
    .line 334
    const-string v0, "file"

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private openFileOrDownloadTempFile(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)V
    .locals 3
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/UriImage;->openOrDownloadInner(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)I

    move-result v0

    .line 108
    .local v0, "state":I
    monitor-enter p0

    .line 109
    :try_start_0
    iput v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mState:I

    .line 110
    iget v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 111
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-eqz v1, :cond_0

    .line 112
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    .line 113
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 116
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 117
    monitor-exit p0

    .line 118
    return-void

    .line 117
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private openOrDownloadInner(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)I
    .locals 12
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x0

    const/4 v7, -0x1

    .line 121
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    .line 122
    .local v3, "scheme":Ljava/lang/String;
    const/4 v1, 0x0

    .line 123
    .local v1, "is":Ljava/io/InputStream;
    const-string v9, "content"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "android.resource"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "file"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 127
    :cond_0
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/UriImage;->mContentType:Ljava/lang/String;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isJpeg(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 128
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v9, v10}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 130
    invoke-static {v1}, Lcom/sec/android/gallery3d/data/Exif;->getOrientation(Ljava/io/InputStream;)I

    move-result v9

    iput v9, p0, Lcom/sec/android/gallery3d/data/UriImage;->mRotation:I

    .line 132
    :cond_1
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "content://mms/part/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 133
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    const-string v11, "r"

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/gallery3d/data/UriImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 136
    :cond_2
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-eqz v7, :cond_3

    .line 142
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 166
    :goto_0
    return v6

    .line 142
    :cond_3
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move v6, v8

    goto :goto_0

    .line 138
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_1
    const-string v6, "UriImage"

    const-string v8, "fail to open: "

    invoke-static {v6, v8, v0}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move v6, v7

    goto :goto_0

    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v6

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v6

    .line 146
    :cond_4
    :try_start_2
    new-instance v9, Ljava/net/URI;

    iget-object v10, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v5

    .line 147
    .local v5, "url":Ljava/net/URL;
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDownloadCache()Lcom/sec/android/gallery3d/data/DownloadCache;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-virtual {v9, p1, v5, v10, v11}, Lcom/sec/android/gallery3d/data/DownloadCache;->download(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;Ljava/lang/String;Z)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/gallery3d/data/UriImage;->mCacheEntry:Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    .line 148
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v9

    if-eqz v9, :cond_5

    .line 166
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .line 149
    :cond_5
    :try_start_3
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/UriImage;->mCacheEntry:Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    if-nez v6, :cond_6

    .line 150
    const-string v6, "UriImage"

    const-string v8, "download failed "

    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 166
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move v6, v7

    goto :goto_0

    .line 153
    :cond_6
    :try_start_4
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/UriImage;->mContentType:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isJpeg(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 154
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/UriImage;->mCacheEntry:Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    iget-object v6, v6, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    invoke-direct {v2, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 155
    .end local v1    # "is":Ljava/io/InputStream;
    .local v2, "is":Ljava/io/InputStream;
    :try_start_5
    invoke-static {v2}, Lcom/sec/android/gallery3d/data/Exif;->getOrientation(Ljava/io/InputStream;)I

    move-result v6

    iput v6, p0, Lcom/sec/android/gallery3d/data/UriImage;->mRotation:I
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-object v1, v2

    .line 158
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    :cond_7
    :try_start_6
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/UriImage;->mCacheEntry:Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    iget-object v6, v6, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    const/high16 v9, 0x10000000

    invoke-static {v6, v9}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/data/UriImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 160
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/UriImage;->mCacheEntry:Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    iget-object v6, v6, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v10

    iput-wide v10, p0, Lcom/sec/android/gallery3d/data/UriImage;->mFileSize:J
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 166
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move v6, v8

    goto/16 :goto_0

    .line 162
    .end local v5    # "url":Ljava/net/URL;
    :catch_1
    move-exception v4

    .line 163
    .local v4, "t":Ljava/lang/Throwable;
    :goto_1
    :try_start_7
    const-string v6, "UriImage"

    const-string v8, "download error"

    invoke-static {v6, v8, v4}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 166
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move v6, v7

    goto/16 :goto_0

    .end local v4    # "t":Ljava/lang/Throwable;
    :catchall_1
    move-exception v6

    :goto_2
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v6

    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    .restart local v5    # "url":Ljava/net/URL;
    :catchall_2
    move-exception v6

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_2

    .line 162
    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    :catch_2
    move-exception v4

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_1
.end method

.method private prepareInputFile(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z
    .locals 4
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 173
    new-instance v2, Lcom/sec/android/gallery3d/data/UriImage$1;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/data/UriImage$1;-><init>(Lcom/sec/android/gallery3d/data/UriImage;)V

    invoke-interface {p1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setCancelListener(Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;)V

    .line 183
    :goto_0
    monitor-enter p0

    .line 184
    :try_start_0
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    monitor-exit p0

    .line 191
    :goto_1
    return v0

    .line 185
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/data/UriImage;->mState:I

    if-nez v2, :cond_1

    .line 186
    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/gallery3d/data/UriImage;->mState:I

    .line 200
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/UriImage;->openFileOrDownloadTempFile(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)V

    goto :goto_0

    .line 188
    :cond_1
    :try_start_1
    iget v2, p0, Lcom/sec/android/gallery3d/data/UriImage;->mState:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 189
    monitor-exit p0

    goto :goto_1

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 190
    :cond_2
    :try_start_2
    iget v2, p0, Lcom/sec/android/gallery3d/data/UriImage;->mState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 191
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    goto :goto_1

    .line 194
    :cond_3
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 198
    :goto_2
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 195
    :catch_0
    move-exception v2

    goto :goto_2
.end method

.method private updateAttribute()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 426
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/UriImage;->mContentType:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->isMpoSupported(Ljava/lang/String;)Z

    move-result v1

    .line 427
    .local v1, "supported":Z
    const-wide/16 v2, 0x2

    invoke-virtual {p0, v2, v3, v1}, Lcom/sec/android/gallery3d/data/UriImage;->setAttribute(JZ)V

    .line 428
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/UriImage;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 429
    .local v0, "filePath":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 430
    const-wide/16 v2, 0x10

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundScene(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/android/gallery3d/data/UriImage;->setAttribute(JZ)V

    .line 432
    const-string v2, "file"

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 433
    invoke-static {v0}, Lcom/sec/android/secvision/sef/SEF;->isSEFFile(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 434
    const/16 v2, 0x850

    invoke-static {v0, v2}, Lcom/sec/android/secvision/sef/SEF;->hasDataType(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 435
    const-wide/16 v2, 0x400

    invoke-virtual {p0, v2, v3, v5}, Lcom/sec/android/gallery3d/data/UriImage;->setAttribute(JZ)V

    .line 441
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/UriImage;->isGolf()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 442
    const-wide/16 v2, 0x80

    invoke-virtual {p0, v2, v3, v5}, Lcom/sec/android/gallery3d/data/UriImage;->setAttribute(JZ)V

    .line 446
    :cond_1
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->check3DPanorama(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/UriImage;->is3dpanorama:Z

    .line 449
    :cond_2
    return-void

    .line 436
    :cond_3
    const/16 v2, 0x870

    invoke-static {v0, v2}, Lcom/sec/android/secvision/sef/SEF;->hasDataType(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 437
    const-wide/32 v2, 0x40000

    invoke-virtual {p0, v2, v3, v5}, Lcom/sec/android/gallery3d/data/UriImage;->setAttribute(JZ)V

    goto :goto_0
.end method


# virtual methods
.method public clearCachedPanoramaSupport()V
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mPanoramaMetadata:Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;->clearCachedValues()V

    .line 327
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 399
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 405
    return-void

    .line 403
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/16 v8, 0xd

    const/16 v7, 0xc

    .line 349
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v1

    .line 350
    .local v1, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    iget v5, p0, Lcom/sec/android/gallery3d/data/UriImage;->mWidth:I

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/sec/android/gallery3d/data/UriImage;->mHeight:I

    if-eqz v5, :cond_0

    .line 351
    iget v5, p0, Lcom/sec/android/gallery3d/data/UriImage;->mWidth:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v7, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 352
    iget v5, p0, Lcom/sec/android/gallery3d/data/UriImage;->mHeight:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v8, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 354
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/UriImage;->mContentType:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 355
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/UriImage;->mContentType:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 357
    :cond_1
    const-string v5, "file"

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 358
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 360
    .local v3, "filePath":Ljava/lang/String;
    const-string v5, "/data/data"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 361
    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 362
    .local v4, "split":[Ljava/lang/String;
    array-length v5, v4

    if-lez v5, :cond_2

    .line 363
    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    aget-object v0, v4, v5

    .line 364
    .local v0, "caption":Ljava/lang/String;
    invoke-virtual {v1, v10, v0}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 369
    .end local v0    # "caption":Ljava/lang/String;
    .end local v4    # "split":[Ljava/lang/String;
    :cond_2
    :goto_0
    invoke-static {v1, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->extractExifInfo(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)V

    .line 371
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 372
    iget v5, p0, Lcom/sec/android/gallery3d/data/UriImage;->mWidth:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v7, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->replaceDetail(ILjava/lang/Object;)V

    .line 374
    :cond_3
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v8}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 375
    iget v5, p0, Lcom/sec/android/gallery3d/data/UriImage;->mHeight:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v8, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->replaceDetail(ILjava/lang/Object;)V

    .line 377
    :cond_4
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/data/UriImage;->isdrm:Z

    if-eqz v5, :cond_6

    .line 378
    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 379
    .restart local v4    # "split":[Ljava/lang/String;
    array-length v5, v4

    if-lez v5, :cond_5

    .line 380
    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    aget-object v0, v4, v5

    .line 381
    .restart local v0    # "caption":Ljava/lang/String;
    invoke-virtual {v1, v10, v0}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 383
    .end local v0    # "caption":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v5

    invoke-virtual {v5, v1, v3}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDetails(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v1

    .end local v4    # "split":[Ljava/lang/String;
    :cond_6
    move-object v2, v1

    .line 388
    .end local v1    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    .end local v3    # "filePath":Ljava/lang/String;
    .local v2, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    :goto_1
    return-object v2

    .line 367
    .end local v2    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    .restart local v1    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    .restart local v3    # "filePath":Ljava/lang/String;
    :cond_7
    const/16 v5, 0xc8

    invoke-virtual {v1, v5, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    goto :goto_0

    .end local v3    # "filePath":Ljava/lang/String;
    :cond_8
    move-object v2, v1

    .line 388
    .end local v1    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    .restart local v2    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    goto :goto_1
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 491
    const-string v2, "file"

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 492
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 493
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 502
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    return-object v1

    .line 496
    .restart local v0    # "file":Ljava/io/File;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 498
    .end local v0    # "file":Ljava/io/File;
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/UriImage;->isdrm:Z

    if-eqz v2, :cond_0

    .line 499
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/data/UriImage;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v10, 0x1

    .line 506
    const/4 v6, 0x0

    .line 507
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 509
    .local v8, "filePath":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_data"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 512
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v10, :cond_0

    .line 513
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 514
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 520
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v9, v8

    .line 522
    .end local v8    # "filePath":Ljava/lang/String;
    .local v9, "filePath":Ljava/lang/String;
    :goto_0
    return-object v9

    .line 517
    .end local v9    # "filePath":Ljava/lang/String;
    .restart local v8    # "filePath":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 518
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "UriImage"

    const-string v1, "createFromUri: Exception"

    invoke-static {v0, v1, v7}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 520
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v9, v8

    .line 522
    .end local v8    # "filePath":Ljava/lang/String;
    .restart local v9    # "filePath":Ljava/lang/String;
    goto :goto_0

    .line 520
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v9    # "filePath":Ljava/lang/String;
    .restart local v8    # "filePath":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 527
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 414
    iget v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mHeight:I

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x2

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mContentType:Ljava/lang/String;

    return-object v0
.end method

.method public getPanoramaSupport(Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mPanoramaMetadata:Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;->getPanoramaSupport(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;)V

    .line 322
    return-void
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 419
    iget v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mRotation:I

    return v0
.end method

.method public getSupportedOperations()J
    .locals 8

    .prologue
    const-wide/16 v6, 0x4

    .line 281
    const-wide v2, 0x90000020L

    .line 283
    .local v2, "supported":J
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/UriImage;->isSharable()Z

    move-result v4

    if-eqz v4, :cond_0

    or-long/2addr v2, v6

    .line 284
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage;->mContentType:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->isSupportedByRegionDecoder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 285
    const-wide/16 v4, 0x240

    or-long/2addr v2, v4

    .line 288
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/data/UriImage;->isdrm:Z

    if-eqz v4, :cond_3

    .line 289
    const-wide/16 v2, 0x400

    .line 290
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/UriImage;->getFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 292
    .local v1, "originalMime":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/BitmapUtils;->isSupportedByRegionDecoder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 293
    const-wide/16 v4, 0x40

    or-long/2addr v2, v4

    .line 295
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/UriImage;->getFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDrmType(Ljava/lang/String;)I

    move-result v0

    .line 296
    .local v0, "drmType":I
    const/4 v4, 0x3

    if-ne v0, v4, :cond_7

    .line 297
    or-long/2addr v2, v6

    .line 303
    .end local v0    # "drmType":I
    .end local v1    # "originalMime":Ljava/lang/String;
    :cond_3
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "content://mms/part/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 304
    const-wide v2, 0x8000000020L

    .line 308
    :cond_4
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "http://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 309
    const-wide/16 v4, -0x21

    and-long/2addr v2, v4

    .line 312
    :cond_5
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "file:///data/data"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 313
    const-wide/16 v2, 0x400

    .line 316
    :cond_6
    return-wide v2

    .line 298
    .restart local v0    # "drmType":I
    .restart local v1    # "originalMime":Ljava/lang/String;
    :cond_7
    if-nez v0, :cond_3

    .line 299
    const-wide/16 v4, 0x20

    or-long/2addr v2, v4

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 409
    iget v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mWidth:I

    return v0
.end method

.method public isGolf()Z
    .locals 2

    .prologue
    .line 537
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/UriImage;->getMimeType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/UriImage;->getMimeType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "image/golf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538
    const/4 v0, 0x1

    .line 540
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    new-instance v0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;-><init>(Lcom/sec/android/gallery3d/data/UriImage;I)V

    return-object v0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    new-instance v0, Lcom/sec/android/gallery3d/data/UriImage$RegionDecoderJob;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/data/UriImage$RegionDecoderJob;-><init>(Lcom/sec/android/gallery3d/data/UriImage;Lcom/sec/android/gallery3d/data/UriImage$1;)V

    return-object v0
.end method

.method public setMimeType(Ljava/lang/String;)V
    .locals 1
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 531
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriImage;->mContentType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 532
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/UriImage;->mContentType:Ljava/lang/String;

    .line 534
    :cond_0
    return-void
.end method
