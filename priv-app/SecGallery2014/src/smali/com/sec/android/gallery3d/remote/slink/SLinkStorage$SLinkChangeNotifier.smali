.class Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;
.super Landroid/database/ContentObserver;
.source "SLinkStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SLinkChangeNotifier"
.end annotation


# instance fields
.field private mContentDirty:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 2
    .param p1, "_handler"    # Landroid/os/Handler;

    .prologue
    .line 235
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 232
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;->mContentDirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 236
    return-void
.end method


# virtual methods
.method public clearDirty()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;->mContentDirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 283
    return-void
.end method

.method public fakeChange()V
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;->onChange(Z)V

    .line 275
    return-void
.end method

.method public isDirty()Z
    .locals 3

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;->mContentDirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    return v0
.end method

.method public notifyDirty()V
    .locals 3

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;->mContentDirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    .line 279
    return-void
.end method

.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 240
    const-string v0, "SLinkChangeNotifier"

    const-string v1, "onChange()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;->mContentDirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    const-string v0, "SLinkChangeNotifier"

    const-string v1, "Call mMediaSet.notifyContentChanged()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->notifyContentChanged()V

    .line 245
    :cond_0
    return-void
.end method

.method public registerContentObserver(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Landroid/content/Context;)V
    .locals 3
    .param p1, "_set"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "_uri"    # Landroid/net/Uri;
    .param p3, "_context"    # Landroid/content/Context;

    .prologue
    .line 248
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 251
    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    .line 252
    :try_start_0
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p2, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 255
    :catch_0
    move-exception v0

    .line 256
    .local v0, "ex":Ljava/lang/Exception;
    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Fail to register ContentObserver."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public unregisterContentObserver(Landroid/content/Context;)V
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 262
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :goto_0
    return-void

    .line 264
    :catch_0
    move-exception v0

    .line 265
    .local v0, "ex":Ljava/lang/Exception;
    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Fail to unregister ContentObserver."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
