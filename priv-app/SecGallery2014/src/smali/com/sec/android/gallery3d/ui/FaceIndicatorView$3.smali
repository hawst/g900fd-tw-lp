.class Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;
.super Ljava/lang/Object;
.source "FaceIndicatorView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->createSettingPopupForMe(Lcom/sec/samsung/gallery/access/contact/ContactData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

.field final synthetic val$contactData:Lcom/sec/samsung/gallery/access/contact/ContactData;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Lcom/sec/samsung/gallery/access/contact/ContactData;)V
    .locals 0

    .prologue
    .line 692
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->val$contactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, -0x1

    const/4 v5, 0x1

    .line 696
    packed-switch p2, :pswitch_data_0

    .line 757
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1400(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 761
    :cond_0
    :goto_0
    return-void

    .line 698
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1400(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 699
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1400(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 701
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->val$contactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    if-eqz v1, :cond_0

    .line 702
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v1

    if-ltz v1, :cond_0

    .line 707
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1600(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v3

    aget-object v2, v2, v3

    # setter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFace:Lcom/sec/android/gallery3d/data/Face;
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1502(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Lcom/sec/android/gallery3d/data/Face;)Lcom/sec/android/gallery3d/data/Face;

    .line 708
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1600(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v2

    # setter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFaceId:I
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1702(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;I)I

    .line 710
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$900(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1600(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Face;->getFaceRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setCurFaceInfo(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Rect;)V

    .line 713
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->NOT_INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->assignName(Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;)V

    .line 714
    const/4 v0, 0x0

    .line 716
    .local v0, "key":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1600(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Face;->getRecommendedId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1600(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Face;->getGroupId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 721
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1800(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1800(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1/0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1800(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 724
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # setter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainFaceModified:Z
    invoke-static {v1, v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1902(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Z)Z

    .line 731
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$900(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    # invokes: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->updateFaceData(Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    invoke-static {v1, v2, v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    goto/16 :goto_0

    .line 737
    .end local v0    # "key":Ljava/lang/String;
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->val$contactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    if-eqz v1, :cond_0

    .line 738
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->val$contactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget v1, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->mAutoGroup:I

    if-ne v1, v4, :cond_3

    .line 739
    const-string v1, "FaceIndicatorView"

    const-string v2, "mAutoGroup -1"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->val$contactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget v3, v3, Lcom/sec/samsung/gallery/access/contact/ContactData;->mFaceId:I

    # invokes: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->removeFace(Landroid/content/Context;I)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Landroid/content/Context;I)V

    .line 741
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->val$contactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget v3, v3, Lcom/sec/samsung/gallery/access/contact/ContactData;->mFaceId:I

    # invokes: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setFaceUnknown(Landroid/content/Context;I)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Landroid/content/Context;I)V

    .line 748
    :goto_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # setter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v1, v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1102(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;I)I

    .line 750
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1400(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 751
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$900(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    # invokes: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->updateFaceData(Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    invoke-static {v1, v2, v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 752
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    goto/16 :goto_0

    .line 745
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;->val$contactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget v3, v3, Lcom/sec/samsung/gallery/access/contact/ContactData;->mFaceId:I

    # invokes: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setFaceUnknown(Landroid/content/Context;I)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Landroid/content/Context;I)V

    goto :goto_1

    .line 696
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
