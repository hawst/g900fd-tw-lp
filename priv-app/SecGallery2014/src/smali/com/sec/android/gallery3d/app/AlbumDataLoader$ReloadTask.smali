.class Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;
.super Lcom/sec/android/gallery3d/app/DetailReloadTask;
.source "AlbumDataLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/AlbumDataLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReloadTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AlbumDataLoader;Ljava/lang/String;)V
    .locals 2
    .param p2, "threadName"    # Ljava/lang/String;

    .prologue
    .line 414
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .line 415
    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$1700(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;
    invoke-static {p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$200(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Lcom/sec/android/gallery3d/app/AlbumReloader;

    move-result-object v1

    invoke-direct {p0, v0, p2, v1}, Lcom/sec/android/gallery3d/app/DetailReloadTask;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/gallery3d/app/AlbumReloader;)V

    .line 416
    return-void
.end method


# virtual methods
.method protected onLoadData()Z
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 425
    const/4 v3, 0x0

    .line 426
    .local v3, "updateComplete":Z
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # invokes: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->getReloadInfo()Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$1900(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;

    move-result-object v2

    .line 427
    .local v2, "reloadInfo":Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v5, :cond_0

    .line 428
    iget-wide v6, v2, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->version:J

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    .line 429
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    const/4 v6, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSize:I
    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$502(Lcom/sec/android/gallery3d/app/AlbumDataLoader;I)I

    .line 430
    const/4 v3, 0x1

    move v4, v3

    .line 458
    .end local v3    # "updateComplete":Z
    .local v4, "updateComplete":I
    :goto_0
    return v4

    .line 434
    .end local v4    # "updateComplete":I
    .restart local v3    # "updateComplete":Z
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    new-instance v6, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;

    iget-object v7, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    iget-wide v8, v2, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->version:J

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;-><init>(Lcom/sec/android/gallery3d/app/AlbumDataLoader;J)V

    # invokes: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$2000(Lcom/sec/android/gallery3d/app/AlbumDataLoader;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;

    .line 435
    .local v1, "info":Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;
    if-nez v1, :cond_1

    .line 436
    const/4 v3, 0x1

    move v4, v3

    .line 437
    .restart local v4    # "updateComplete":I
    goto :goto_0

    .line 439
    .end local v4    # "updateComplete":I
    :cond_1
    iget-wide v6, v1, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->version:J

    iget-wide v8, v2, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->version:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    .line 440
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mGroupIndex:J
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$2100(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)J

    move-result-wide v6

    cmp-long v5, v6, v10

    if-lez v5, :cond_4

    .line 441
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$2200(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    # invokes: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->loadGroupItems(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;)V
    invoke-static {v5, v6, v1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$2300(Lcom/sec/android/gallery3d/app/AlbumDataLoader;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;)V

    .line 445
    :goto_1
    iget-wide v6, v2, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->version:J

    iput-wide v6, v1, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->version:J

    .line 447
    :cond_2
    iget v5, v1, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->reloadCount:I

    if-lez v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mGroupIndex:J
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$2100(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)J

    move-result-wide v6

    cmp-long v5, v6, v10

    if-gtz v5, :cond_3

    .line 449
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$2200(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->reloadStart:I

    iget v7, v1, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->reloadCount:I

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->items:Ljava/util/ArrayList;

    .line 451
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457
    :cond_3
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    new-instance v6, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateContent;

    iget-object v7, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-direct {v6, v7, v1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateContent;-><init>(Lcom/sec/android/gallery3d/app/AlbumDataLoader;Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;)V

    # invokes: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$2000(Lcom/sec/android/gallery3d/app/AlbumDataLoader;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move v4, v3

    .line 458
    .restart local v4    # "updateComplete":I
    goto :goto_0

    .line 443
    .end local v4    # "updateComplete":I
    :cond_4
    iget v5, v2, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->size:I

    iput v5, v1, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->size:I

    goto :goto_1

    .line 451
    :catchall_0
    move-exception v5

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v5
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 452
    :catch_0
    move-exception v0

    .line 453
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v4, v3

    .line 454
    .restart local v4    # "updateComplete":I
    goto :goto_0
.end method

.method protected updateLoadingStatus(Z)V
    .locals 2
    .param p1, "loading"    # Z

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$1800(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Landroid/os/Handler;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 421
    return-void

    .line 420
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
