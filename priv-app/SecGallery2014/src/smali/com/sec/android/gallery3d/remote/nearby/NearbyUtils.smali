.class public Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;
.super Ljava/lang/Object;
.source "NearbyUtils.java"


# static fields
.field private static final KEY_NEARBY_DOWNLOAD:Ljava/lang/String; = "NearbyDownloadTo"

.field private static final MEGA_BYTE:J = 0x40000000L

.field private static final ROTATION_PARAMETER_KEY:Ljava/lang/String; = "rtt="

.field private static final TAG:Ljava/lang/String;

.field private static alreadtInitNearbyFramework:Z

.field private static smNearbySource:Lcom/sec/android/gallery3d/remote/nearby/NearbySource;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->smNearbySource:Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .line 40
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->alreadtInitNearbyFramework:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createCacheFilePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "cacheFilePrefix"    # Ljava/lang/String;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x5f

    .line 218
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 219
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 220
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 221
    .local v2, "lastModifiedDate":J
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 223
    .local v4, "length":J
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public static deletePreviousCache(Ljava/lang/String;)V
    .locals 2
    .param p0, "fileToDelete"    # Ljava/lang/String;

    .prologue
    .line 268
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 269
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return-void

    .line 270
    :catch_0
    move-exception v0

    .line 271
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static deletePreviousCaches(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 231
    sget-object v3, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    const-string v4, "clear nearby caches"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    if-nez p0, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    .line 237
    .local v0, "cacheDir":Ljava/io/File;
    if-nez v0, :cond_2

    .line 238
    sget-object v3, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    const-string v4, "external cache dir is not available"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 242
    :cond_2
    new-instance v3, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils$1;

    invoke-direct {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils$1;-><init>()V

    invoke-virtual {v0, v3}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 249
    .local v2, "previousCaches":[Ljava/io/File;
    if-nez v2, :cond_3

    .line 250
    sget-object v3, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    const-string v4, "previousCaches are not available"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 254
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 255
    aget-object v3, v2, v1

    if-eqz v3, :cond_4

    .line 256
    aget-object v3, v2, v1

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 254
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static drawChangePlayerIcon(Landroid/view/MenuItem;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 5
    .param p0, "menuItem"    # Landroid/view/MenuItem;
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 165
    if-nez p0, :cond_0

    .line 166
    sget-object v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    const-string v3, "set change player visible : false"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    const/4 v1, 0x0

    .line 191
    :goto_0
    return v1

    .line 170
    :cond_0
    const/4 v1, 0x1

    .line 172
    .local v1, "visible":Z
    if-eqz p1, :cond_4

    .line 173
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "mimetype":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 175
    const-string v2, "image/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "foo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "video/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 178
    const/4 v1, 0x0

    .line 189
    .end local v0    # "mimetype":Ljava/lang/String;
    :cond_1
    :goto_1
    sget-object v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set change player visible : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-interface {p0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 179
    .restart local v0    # "mimetype":Ljava/lang/String;
    :cond_2
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMmsItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 180
    const/4 v1, 0x0

    goto :goto_1

    .line 181
    :cond_3
    const-string/jumbo v2, "video/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 182
    const/4 v1, 0x0

    goto :goto_1

    .line 186
    .end local v0    # "mimetype":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static getAvailableStorageInByte(Ljava/lang/String;)J
    .locals 12
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 141
    const-wide/16 v0, 0x0

    .line 142
    .local v0, "availableMemory":J
    if-nez p0, :cond_0

    .line 143
    sget-object v7, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    const-string v10, "getAvailableStorageInByte: path is null "

    invoke-static {v7, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const-wide/16 v10, 0x0

    .line 161
    :goto_0
    return-wide v10

    .line 149
    :cond_0
    :try_start_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 150
    .local v5, "pathDir":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 151
    invoke-virtual {v5}, Ljava/io/File;->mkdir()Z

    .line 154
    :cond_1
    new-instance v6, Landroid/os/StatFs;

    invoke-direct {v6, p0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 155
    .local v6, "stat":Landroid/os/StatFs;
    invoke-virtual {v6}, Landroid/os/StatFs;->getBlockSize()I

    move-result v7

    int-to-long v2, v7

    .line 156
    .local v2, "blockSize":J
    invoke-virtual {v6}, Landroid/os/StatFs;->getAvailableBlocks()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    int-to-long v8, v7

    .line 157
    .local v8, "totalBlocks":J
    mul-long v0, v2, v8

    .end local v2    # "blockSize":J
    .end local v5    # "pathDir":Ljava/io/File;
    .end local v6    # "stat":Landroid/os/StatFs;
    .end local v8    # "totalBlocks":J
    :goto_1
    move-wide v10, v0

    .line 161
    goto :goto_0

    .line 158
    :catch_0
    move-exception v4

    .line 159
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getDownloadDir(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 131
    const-string v0, "NearbyDownloadTo"

    invoke-static {p0, v0}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getOrientationValue(Ljava/lang/String;)I
    .locals 5
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 289
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 290
    const/4 v1, -0x1

    .line 304
    :cond_0
    :goto_0
    return v1

    .line 293
    :cond_1
    const/4 v1, -0x1

    .line 295
    .local v1, "orientation":I
    const-string v3, "rtt="

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 296
    .local v2, "splitedString":[Ljava/lang/String;
    array-length v3, v2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 298
    const/4 v3, 0x1

    :try_start_0
    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 299
    :catch_0
    move-exception v0

    .line 300
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getStorageAlertDialog(Landroid/content/Context;J)Landroid/app/AlertDialog;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bytesShortage"    # J

    .prologue
    .line 328
    long-to-double v4, p1

    const-wide/high16 v6, 0x41d0000000000000L    # 1.073741824E9

    div-double v0, v4, v6

    .line 329
    .local v0, "megaByteShortage":D
    const v3, 0x7f0e0142

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-long v6, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 332
    .local v2, "message":Ljava/lang/String;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0e010d

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0e00db

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method public static getStorageShortageInByte(Landroid/content/Context;Ljava/util/List;)J
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 316
    .local p1, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const-wide/16 v2, 0x0

    .line 317
    .local v2, "bytesToDownload":J
    const/4 v4, 0x0

    .local v4, "i":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .local v5, "n":I
    :goto_0
    if-ge v4, v5, :cond_0

    .line 318
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->getSize()J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 317
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 321
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->getDownloadDir(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->getAvailableStorageInByte(Ljava/lang/String;)J

    move-result-wide v0

    .line 322
    .local v0, "bytesAvailable":J
    sget-object v6, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "storage available : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    sget-object v6, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "bytes to download : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    sub-long v6, v2, v0

    return-wide v6
.end method

.method public static isDeviceRemoved(Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Ljava/lang/String;)Z
    .locals 4
    .param p0, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p1, "nearbyClient"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 337
    invoke-static {p2}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 338
    .local v0, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v0, :cond_0

    instance-of v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-eqz v3, :cond_0

    move-object v1, v0

    .line 339
    check-cast v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    .line 340
    .local v1, "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/Device;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    .line 342
    .end local v1    # "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    :cond_0
    return v2
.end method

.method public static isNearbyPlayable(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 4
    .param p0, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v0

    const-wide v2, 0x80000000L

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static scanForNearbyPlayerDevices()V
    .locals 3

    .prologue
    .line 202
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->smNearbySource:Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v0

    .line 203
    .local v0, "nearbyContext":Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;
    if-nez v0, :cond_0

    .line 204
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    const-string v2, "nearby context not exists, ignore"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->showToastForScanningPlayerDevices()V

    .line 209
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refresh()V

    goto :goto_0
.end method

.method public static scanForNearbyProviderDevices(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 195
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->smNearbySource:Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyDeviceSet()Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    move-result-object v0

    .line 196
    .local v0, "nearbyDeviceSet":Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;
    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;->showToastForScanningProviderDevices(Landroid/app/Activity;)V

    .line 198
    :cond_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->smNearbySource:Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refresh()V

    .line 199
    return-void
.end method

.method public static setActivity(Landroid/app/Activity;Lcom/sec/android/gallery3d/data/DataManager;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;

    .prologue
    .line 66
    const-string v1, "nearby"

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    sput-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->smNearbySource:Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .line 67
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->smNearbySource:Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    if-eqz v1, :cond_0

    .line 68
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->smNearbySource:Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v0

    .line 69
    .local v0, "nearbyClient":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->setActivity(Landroid/app/Activity;)V

    .line 71
    .end local v0    # "nearbyClient":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    :cond_0
    return-void
.end method

.method public static showDeviceRemovedDialog(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "nearbyClient"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 346
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->showDmsDisconnectedDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    return-void
.end method

.method public static startNearbyFramework(Lcom/sec/android/gallery3d/data/DataManager;)V
    .locals 3
    .param p0, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;

    .prologue
    const/4 v2, 0x1

    .line 48
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNearby:Z

    if-nez v1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    sget-boolean v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->alreadtInitNearbyFramework:Z

    if-ne v1, v2, :cond_2

    .line 52
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    const-string v2, "already initialized Nearby framework. Skip init."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 55
    :cond_2
    sput-boolean v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->alreadtInitNearbyFramework:Z

    .line 57
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "start Nearby framework"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const-string v1, "nearby"

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    sput-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->smNearbySource:Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .line 59
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->smNearbySource:Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    if-eqz v1, :cond_0

    .line 60
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->smNearbySource:Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v0

    .line 61
    .local v0, "nearbyClient":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->startAllShareFrameworkService()V

    goto :goto_0
.end method

.method public static stopNearbyFramework(Lcom/sec/android/gallery3d/data/DataManager;)V
    .locals 11
    .param p0, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;

    .prologue
    .line 79
    sget-object v8, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "stop nearby framework"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNearby:Z

    if-nez v8, :cond_0

    .line 122
    :goto_0
    return-void

    .line 83
    :cond_0
    const-string v8, "nearby"

    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .line 84
    .local v7, "source":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    if-nez v7, :cond_1

    .line 85
    sget-object v8, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    const-string v9, "NearbySource instance is null"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 90
    :cond_1
    :try_start_0
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v4

    .line 91
    .local v4, "nearbyClient":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    if-nez v4, :cond_2

    .line 92
    sget-object v8, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    const-string v9, "NearbyClient instance is null"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 117
    .end local v4    # "nearbyClient":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    :catch_0
    move-exception v2

    .line 118
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 121
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    const/4 v8, 0x0

    sput-boolean v8, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->alreadtInitNearbyFramework:Z

    goto :goto_0

    .line 95
    .restart local v4    # "nearbyClient":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    :cond_2
    :try_start_1
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->disconnectWithPlayDevice()V

    .line 96
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->stopAllShareFrameworkService()V

    .line 98
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->getDevices()Ljava/util/ArrayList;

    move-result-object v1

    .line 99
    .local v1, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 100
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/nearby/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    .line 101
    .local v6, "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    if-eqz v6, :cond_3

    .line 102
    sget-object v8, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Clear NearbyDevice instance. device id= "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->update(Lcom/samsung/android/allshare/Device;)V

    .line 104
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->clearItems()V

    .line 107
    :cond_3
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v5

    .line 108
    .local v5, "nearbyContext":Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;
    if-nez v5, :cond_4

    .line 109
    sget-object v8, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->TAG:Ljava/lang/String;

    const-string v9, "NearbyContext instance is null"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 112
    :cond_4
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->notifyDirty()V

    .line 113
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->notifyDirty(Ljava/lang/String;)V

    goto :goto_2

    .line 115
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    .end local v5    # "nearbyContext":Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;
    .end local v6    # "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    :cond_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
