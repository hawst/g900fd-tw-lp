.class public Lcom/sec/android/gallery3d/ui/OCRPopupWindow;
.super Landroid/widget/FrameLayout;
.source "OCRPopupWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;,
        Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;
    }
.end annotation


# static fields
.field public static final EVENT_ADD_BOOKMARK:I = 0x9

.field public static final EVENT_ADD_CONTACT:I = 0x5

.field public static final EVENT_CALL:I = 0x6

.field public static final EVENT_COPY:I = 0x1

.field public static final EVENT_OPEN_URL:I = 0x8

.field public static final EVENT_SEND_MAIL:I = 0x4

.field public static final EVENT_SEND_MSG:I = 0x7

.field public static final EVENT_SHARE:I = 0x3

.field public static final EVENT_WEB_SEARCH:I = 0x2

.field private static final ITEM_ADD_BOOKMARK:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

.field private static final ITEM_ADD_CONTACT:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

.field private static final ITEM_CALL:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

.field private static final ITEM_COPY:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

.field private static final ITEM_OPEN_URL:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

.field private static final ITEM_SEND_MAIL:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

.field private static final ITEM_SEND_MSG:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

.field private static final ITEM_SHARE:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

.field private static final ITEM_WEB_SEARCH:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

.field public static final OCR_EMAIL:I = 0x1

.field public static final OCR_PHONENUMBER:I = 0x3

.field public static final OCR_TEXT:I = 0x0

.field public static final OCR_URL:I = 0x2

.field private static final TAG:Ljava/lang/String; = "OCRPopupWindow"

.field private static sItemMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "[",
            "Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBoundBottom:I

.field private mBoundLeft:I

.field private mBoundRight:I

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mItemListView:Landroid/widget/ListView;

.field private final mMetrics:Landroid/util/DisplayMetrics;

.field private mOCRTextHeight:I

.field private final mParentView:Landroid/view/ViewGroup;

.field private mPointX:I

.field private mPointY:I

.field private final mTitleView:Landroid/widget/TextView;

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 52
    new-instance v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    const v1, 0x7f020417

    const v2, 0x7f0e011a

    invoke-direct {v0, v4, v1, v2}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;-><init>(III)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_COPY:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    .line 53
    new-instance v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    const v1, 0x7f02041b

    const v2, 0x7f0e0250

    invoke-direct {v0, v5, v1, v2}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;-><init>(III)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_WEB_SEARCH:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    .line 54
    new-instance v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    const v1, 0x7f02041a

    const v2, 0x7f0e0047

    invoke-direct {v0, v6, v1, v2}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;-><init>(III)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_SHARE:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    .line 55
    new-instance v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    const v1, 0x7f020418

    const v2, 0x7f0e0251

    invoke-direct {v0, v8, v1, v2}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;-><init>(III)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_SEND_MAIL:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    .line 56
    new-instance v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    const/4 v1, 0x5

    const v2, 0x7f020414

    const v3, 0x7f0e0252

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;-><init>(III)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_ADD_CONTACT:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    .line 57
    new-instance v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    const/4 v1, 0x6

    const v2, 0x7f020416

    const v3, 0x7f0e00b8

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;-><init>(III)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_CALL:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    .line 58
    new-instance v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    const/4 v1, 0x7

    const v2, 0x7f020419

    const v3, 0x7f0e0253

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;-><init>(III)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_SEND_MSG:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    .line 59
    new-instance v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    const/16 v1, 0x8

    const v2, 0x7f02041b

    const v3, 0x7f0e0254

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;-><init>(III)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_OPEN_URL:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    .line 60
    new-instance v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    const/16 v1, 0x9

    const v2, 0x7f020415

    const v3, 0x7f0e0255

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;-><init>(III)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_ADD_BOOKMARK:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->sItemMap:Ljava/util/Map;

    .line 64
    sget-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->sItemMap:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v2, v6, [Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_COPY:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v7

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_WEB_SEARCH:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v4

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_SHARE:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v5

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->sItemMap:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v2, v8, [Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_CALL:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v7

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_SEND_MSG:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v4

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_ADD_CONTACT:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v5

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_SHARE:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v6

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->sItemMap:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v2, v6, [Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_SEND_MAIL:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v7

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_ADD_CONTACT:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v4

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_SHARE:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v5

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->sItemMap:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v2, v6, [Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_OPEN_URL:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v7

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_ADD_BOOKMARK:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v4

    sget-object v3, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->ITEM_SHARE:Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    aput-object v3, v2, v5

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v3, 0x7fffffff

    .line 90
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 76
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mMetrics:Landroid/util/DisplayMetrics;

    .line 78
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mType:I

    .line 80
    iput v3, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mPointX:I

    .line 81
    iput v3, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mPointY:I

    .line 83
    iput v3, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mBoundLeft:I

    .line 84
    iput v3, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mBoundRight:I

    .line 85
    iput v3, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mBoundBottom:I

    .line 87
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mOCRTextHeight:I

    .line 91
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mInflater:Landroid/view/LayoutInflater;

    .line 92
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f0300ac

    invoke-virtual {v2, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 94
    .local v1, "contentView":Landroid/view/ViewGroup;
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mParentView:Landroid/view/ViewGroup;

    .line 95
    const v2, 0x7f0f01e1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mTitleView:Landroid/widget/TextView;

    .line 96
    const v2, 0x7f0f01e3

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mItemListView:Landroid/widget/ListView;

    .line 97
    const v2, 0x7f0f01e2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 98
    .local v0, "cancelBtn":Landroid/widget/ImageButton;
    new-instance v2, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$1;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$1;-><init>(Lcom/sec/android/gallery3d/ui/OCRPopupWindow;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/OCRPopupWindow;Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/OCRPopupWindow;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;
    .param p2, "x2"    # Landroid/view/ViewGroup;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->createItemView(Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private addToContact(Ljava/lang/String;)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 277
    iget v2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mType:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 278
    const-string/jumbo v2, "tel"

    invoke-static {v2, p1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 284
    .local v1, "uri":Landroid/net/Uri;
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.contacts.action.SHOW_OR_CREATE_CONTACT"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 285
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 286
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    return-void

    .line 279
    :cond_1
    iget v2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 280
    const-string v2, "mailto"

    invoke-static {v2, p1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1    # "uri":Landroid/net/Uri;
    goto :goto_0
.end method

.method private callTo(Ljava/lang/String;)V
    .locals 4
    .param p1, "phone"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 290
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isUriNumber(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 291
    const-string/jumbo v2, "sip"

    invoke-static {v2, p1, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 295
    .local v1, "uri":Landroid/net/Uri;
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 296
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 297
    return-void

    .line 293
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    const-string/jumbo v2, "tel"

    invoke-static {v2, p1, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1    # "uri":Landroid/net/Uri;
    goto :goto_0
.end method

.method private copyToClipboard(Ljava/lang/String;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "clipboard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ClipboardManager;

    .line 243
    .local v1, "clipboard":Landroid/content/ClipboardManager;
    const-string v2, "label"

    invoke-static {v2, p1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 244
    .local v0, "clip":Landroid/content/ClipData;
    invoke-virtual {v1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 245
    return-void
.end method

.method private createItemView(Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "info"    # Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 181
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f0300ad

    const/4 v5, 0x0

    invoke-virtual {v3, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 182
    .local v2, "v":Landroid/view/ViewGroup;
    const v3, 0x7f0f01e4

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 183
    .local v0, "icon":Landroid/widget/ImageView;
    const v3, 0x7f0f01e5

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 185
    .local v1, "title":Landroid/widget/TextView;
    iget v3, p1, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;->drawable:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 186
    iget v3, p1, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;->text:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 188
    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 189
    invoke-virtual {v2, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    return-object v2
.end method

.method private createUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 306
    const-string v0, "https://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "http://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 307
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 309
    :cond_0
    return-object p1
.end method

.method private onItemClicked(Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;)V
    .locals 5
    .param p1, "info"    # Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    .prologue
    .line 203
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 205
    .local v1, "text":Ljava/lang/String;
    :try_start_0
    iget v2, p1, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;->event:I

    packed-switch v2, :pswitch_data_0

    .line 239
    :goto_0
    return-void

    .line 207
    :pswitch_0
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->copyToClipboard(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 236
    :catch_0
    move-exception v0

    .line 237
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "OCRPopupWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail to load execute event: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;->event:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 210
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_1
    :try_start_1
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->webSearch(Ljava/lang/String;)V

    goto :goto_0

    .line 213
    :pswitch_2
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->shareText(Ljava/lang/String;)V

    goto :goto_0

    .line 216
    :pswitch_3
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->sendEmail(Ljava/lang/String;)V

    goto :goto_0

    .line 219
    :pswitch_4
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->addToContact(Ljava/lang/String;)V

    goto :goto_0

    .line 222
    :pswitch_5
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->callTo(Ljava/lang/String;)V

    goto :goto_0

    .line 225
    :pswitch_6
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->sendMSG(Ljava/lang/String;)V

    goto :goto_0

    .line 228
    :pswitch_7
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->openUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 231
    :pswitch_8
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->saveBookmark(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 205
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private openUrl(Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 313
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->createUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 314
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 316
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    :goto_0
    return-void

    .line 317
    :catch_0
    move-exception v0

    .line 318
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "OCRPopupWindow"

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private saveBookmark(Ljava/lang/String;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 324
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT"

    const-string v2, "content://browser/bookmarks"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 325
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "url"

    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->createUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 326
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 328
    return-void
.end method

.method private sendEmail(Ljava/lang/String;)V
    .locals 4
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    .line 300
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mailto:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 302
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 303
    return-void
.end method

.method private sendMSG(Ljava/lang/String;)V
    .locals 4
    .param p1, "addr"    # Ljava/lang/String;

    .prologue
    .line 271
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "smsto:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 272
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 273
    return-void
.end method

.method private shareText(Ljava/lang/String;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 259
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 260
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 261
    const-string/jumbo v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    :goto_0
    return-void

    .line 264
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "OCRPopupWindow"

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private webSearch(Ljava/lang/String;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 248
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.WEB_SEARCH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 249
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "query"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 251
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :goto_0
    return-void

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "OCRPopupWindow"

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .prologue
    const v1, 0x7fffffff

    .line 143
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mType:I

    .line 144
    iput v1, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mPointX:I

    .line 145
    iput v1, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mPointY:I

    .line 146
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 147
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 195
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 196
    .local v0, "tag":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    if-eqz v1, :cond_0

    .line 197
    check-cast v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    .end local v0    # "tag":Ljava/lang/Object;
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->onItemClicked(Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;)V

    .line 199
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->dismiss()V

    .line 200
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const v10, 0x1fffffff

    const/4 v12, 0x0

    .line 151
    invoke-super {p0, v10, v10}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 153
    iget v8, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mPointX:I

    .line 154
    .local v8, "x":I
    iget v9, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mPointY:I

    .line 155
    .local v9, "y":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getMeasuredWidth()I

    move-result v7

    .line 156
    .local v7, "w":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getMeasuredHeight()I

    move-result v0

    .line 158
    .local v0, "h":I
    div-int/lit8 v10, v7, 0x2

    sub-int/2addr v8, v10

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getContext()Landroid/content/Context;

    move-result-object v10

    check-cast v10, Landroid/app/Activity;

    invoke-virtual {v10}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v10

    invoke-interface {v10}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v10, v11}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 161
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mMetrics:Landroid/util/DisplayMetrics;

    iget v5, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 162
    .local v5, "screenHeight":I
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mMetrics:Landroid/util/DisplayMetrics;

    iget v6, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 164
    .local v6, "screenWidth":I
    iget v10, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mBoundLeft:I

    invoke-static {v12, v10}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 165
    .local v3, "minX":I
    iget v10, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mBoundRight:I

    invoke-static {v6, v10}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 166
    .local v1, "maxX":I
    iget v10, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mBoundBottom:I

    invoke-static {v5, v10}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 168
    .local v2, "maxY":I
    sub-int v10, v1, v7

    invoke-static {v8, v3, v10}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v8

    .line 169
    add-int v10, v9, v0

    if-le v10, v2, :cond_0

    .line 170
    iget v10, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mOCRTextHeight:I

    add-int/2addr v10, v0

    sub-int/2addr v9, v10

    .line 172
    :cond_0
    sub-int v10, v2, v0

    invoke-static {v9, v12, v10}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v9

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 175
    .local v4, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v8, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 176
    iput v9, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 177
    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 178
    return-void
.end method

.method public prepareWindow(ILjava/lang/String;)V
    .locals 7
    .param p1, "type"    # I
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 107
    iget v5, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mType:I

    if-eq v5, p1, :cond_1

    sget-object v5, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->sItemMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 108
    iput p1, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mType:I

    .line 109
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v5, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;>;"
    sget-object v5, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->sItemMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 112
    .local v2, "info":Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    .end local v2    # "info":Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mItemListView:Landroid/widget/ListView;

    new-instance v6, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;

    invoke-direct {v6, p0, v4}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;-><init>(Lcom/sec/android/gallery3d/ui/OCRPopupWindow;Ljava/util/ArrayList;)V

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-nez v5, :cond_1

    .line 116
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v5, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 117
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->setVisibility(I)V

    .line 120
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;
    .end local v1    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;>;"
    :cond_1
    return-void
.end method

.method public setPopupBounds(III)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "right"    # I
    .param p3, "bottom"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mBoundLeft:I

    .line 138
    iput p2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mBoundRight:I

    .line 139
    iput p3, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mBoundBottom:I

    .line 140
    return-void
.end method

.method public showAtPosition(III)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "ocrHeight"    # I

    .prologue
    .line 123
    iput p3, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mOCRTextHeight:I

    .line 124
    sget-object v1, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->sItemMap:Ljava/util/Map;

    iget v2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mPointX:I

    if-ne v1, p1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mPointY:I

    if-eq v1, p2, :cond_1

    .line 125
    :cond_0
    iput p1, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mPointX:I

    .line 126
    iput p2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->mPointY:I

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 129
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 130
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 131
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 132
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->setVisibility(I)V

    .line 134
    .end local v0    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1
    return-void
.end method
