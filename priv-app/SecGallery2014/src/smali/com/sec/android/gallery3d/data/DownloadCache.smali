.class public Lcom/sec/android/gallery3d/data/DownloadCache;
.super Ljava/lang/Object;
.source "DownloadCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;,
        Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;,
        Lcom/sec/android/gallery3d/data/DownloadCache$Entry;,
        Lcom/sec/android/gallery3d/data/DownloadCache$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final FREESPACE_IDNEX_ID:I = 0x0

.field private static final FREESPACE_INDEX_CONTENT_SIZE:I = 0x3

.field private static final FREESPACE_INDEX_CONTENT_URL:I = 0x2

.field private static final FREESPACE_INDEX_DATA:I = 0x1

.field private static final FREESPACE_ORDER_BY:Ljava/lang/String;

.field private static final FREESPACE_PROJECTION:[Ljava/lang/String;

.field private static final ID_WHERE:Ljava/lang/String; = "_id = ?"

.field private static final LRU_CAPACITY:I = 0x4

.field private static final MAX_DELETE_COUNT:I = 0x10

.field private static final QUERY_INDEX_DATA:I = 0x1

.field private static final QUERY_INDEX_ID:I

.field private static final QUERY_PROJECTION:[Ljava/lang/String;

.field private static final SUM_INDEX_SUM:I

.field private static final SUM_PROJECTION:[Ljava/lang/String;

.field private static final TABLE_NAME:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static final WHERE_HASH_AND_URL:Ljava/lang/String;


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mAssociateMap:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Object;",
            "Lcom/sec/android/gallery3d/data/DownloadCache$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final mCapacity:J

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private final mDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private final mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/common/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/gallery3d/data/DownloadCache$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private mInitialized:Z

.field private final mRoot:Ljava/io/File;

.field private final mTaskMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalBytes:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 48
    const-class v0, Lcom/sec/android/gallery3d/data/DownloadCache;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/DownloadCache;->TAG:Ljava/lang/String;

    .line 52
    sget-object v0, Lcom/sec/android/gallery3d/data/DownloadEntry;->SCHEMA:Lcom/sec/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/DownloadCache;->TABLE_NAME:Ljava/lang/String;

    .line 54
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "_data"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/gallery3d/data/DownloadCache;->QUERY_PROJECTION:[Ljava/lang/String;

    .line 55
    const-string v0, "%s = ? AND %s = ?"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "hash_code"

    aput-object v2, v1, v4

    const-string v2, "content_url"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/DownloadCache;->WHERE_HASH_AND_URL:Ljava/lang/String;

    .line 60
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "content_url"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "_size"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/data/DownloadCache;->FREESPACE_PROJECTION:[Ljava/lang/String;

    .line 62
    const-string v0, "%s ASC"

    new-array v1, v3, [Ljava/lang/Object;

    const-string v2, "last_access"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/DownloadCache;->FREESPACE_ORDER_BY:Ljava/lang/String;

    .line 71
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "sum(%s)"

    new-array v2, v3, [Ljava/lang/Object;

    const-string v3, "_size"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/gallery3d/data/DownloadCache;->SUM_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/io/File;J)V
    .locals 3
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "root"    # Ljava/io/File;
    .param p3, "capacity"    # J

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Lcom/sec/android/gallery3d/common/LruCache;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/common/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;

    .line 84
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTotalBytes:J

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mInitialized:Z

    .line 88
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mAssociateMap:Ljava/util/WeakHashMap;

    .line 93
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mRoot:Ljava/io/File;

    .line 94
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 95
    iput-wide p3, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mCapacity:J

    .line 96
    new-instance v0, Lcom/sec/android/gallery3d/data/DownloadCache$DatabaseHelper;

    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/data/DownloadCache$DatabaseHelper;-><init>(Lcom/sec/android/gallery3d/data/DownloadCache;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DownloadCache$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 99
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 102
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/DownloadCache;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/DownloadCache;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mRoot:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/gallery3d/data/DownloadCache;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/data/DownloadCache;)Ljava/util/WeakHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/DownloadCache;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mAssociateMap:Ljava/util/WeakHashMap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/data/DownloadCache;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/DownloadCache;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/data/DownloadCache;Ljava/lang/String;Ljava/io/File;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/DownloadCache;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/io/File;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/data/DownloadCache;->insertEntry(Ljava/lang/String;Ljava/io/File;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/data/DownloadCache;)Lcom/sec/android/gallery3d/common/LruCache;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/DownloadCache;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/data/DownloadCache;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/DownloadCache;
    .param p1, "x1"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/DownloadCache;->freeSomeSpaceIfNeed(I)V

    return-void
.end method

.method private findEntryInDatabase(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .locals 20
    .param p1, "stringUrl"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-static/range {p1 .. p1}, Lcom/sec/android/gallery3d/common/Utils;->crc64Long(Ljava/lang/String;)J

    move-result-wide v16

    .line 106
    .local v16, "hash":J
    const/4 v4, 0x2

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v4

    const/4 v4, 0x1

    aput-object p1, v8, v4

    .line 107
    .local v8, "whereArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v5, Lcom/sec/android/gallery3d/data/DownloadCache;->TABLE_NAME:Ljava/lang/String;

    sget-object v6, Lcom/sec/android/gallery3d/data/DownloadCache;->QUERY_PROJECTION:[Ljava/lang/String;

    sget-object v7, Lcom/sec/android/gallery3d/data/DownloadCache;->WHERE_HASH_AND_URL:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 110
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 111
    new-instance v15, Ljava/io/File;

    const/4 v4, 0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v15, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 112
    .local v15, "file":Ljava/io/File;
    const/4 v4, 0x0

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v18, v0

    .line 113
    .local v18, "id":J
    const/4 v13, 0x0

    .line 114
    .local v13, "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 115
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/common/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-object v13, v0

    .line 116
    if-nez v13, :cond_0

    .line 117
    new-instance v14, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v14, v0, v1, v2, v15}, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;-><init>(Lcom/sec/android/gallery3d/data/DownloadCache;JLjava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118
    .end local v13    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .local v14, "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v14}, Lcom/sec/android/gallery3d/common/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v13, v14

    .line 120
    .end local v14    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .restart local v13    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :cond_0
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 124
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 126
    .end local v13    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .end local v15    # "file":Ljava/io/File;
    .end local v18    # "id":J
    :goto_0
    return-object v13

    .line 120
    .restart local v13    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .restart local v15    # "file":Ljava/io/File;
    .restart local v18    # "id":J
    :catchall_0
    move-exception v4

    :goto_1
    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 124
    .end local v13    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .end local v15    # "file":Ljava/io/File;
    .end local v18    # "id":J
    :catchall_1
    move-exception v4

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 126
    const/4 v13, 0x0

    goto :goto_0

    .line 120
    .restart local v14    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .restart local v15    # "file":Ljava/io/File;
    .restart local v18    # "id":J
    :catchall_2
    move-exception v4

    move-object v13, v14

    .end local v14    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .restart local v13    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    goto :goto_1
.end method

.method private declared-synchronized freeSomeSpaceIfNeed(I)V
    .locals 18
    .param p1, "maxDeleteFileCount"    # I

    .prologue
    .line 180
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTotalBytes:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mCapacity:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 210
    :goto_0
    monitor-exit p0

    return-void

    .line 181
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v3, Lcom/sec/android/gallery3d/data/DownloadCache;->TABLE_NAME:Ljava/lang/String;

    sget-object v4, Lcom/sec/android/gallery3d/data/DownloadCache;->FREESPACE_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/android/gallery3d/data/DownloadCache;->FREESPACE_ORDER_BY:Ljava/lang/String;

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 183
    .local v11, "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 186
    :cond_1
    :goto_1
    if-lez p1, :cond_2

    :try_start_2
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTotalBytes:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mCapacity:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 187
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 188
    .local v12, "id":J
    const/4 v2, 0x2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 189
    .local v15, "url":Ljava/lang/String;
    const/4 v2, 0x3

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 190
    .local v16, "size":J
    const/4 v2, 0x1

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 192
    .local v14, "path":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    monitor-enter v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 193
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    invoke-virtual {v2, v15}, Lcom/sec/android/gallery3d/common/LruCache;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    .line 194
    .local v10, "containsKey":Z
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 195
    if-nez v10, :cond_1

    .line 196
    add-int/lit8 p1, p1, -0x1

    .line 197
    :try_start_4
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTotalBytes:J

    sub-long v2, v2, v16

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTotalBytes:J

    .line 198
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v3, Lcom/sec/android/gallery3d/data/DownloadCache;->TABLE_NAME:Ljava/lang/String;

    const-string v4, "_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 207
    .end local v10    # "containsKey":Z
    .end local v12    # "id":J
    .end local v14    # "path":Ljava/lang/String;
    .end local v15    # "url":Ljava/lang/String;
    .end local v16    # "size":J
    :catchall_0
    move-exception v2

    :try_start_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 208
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 180
    .end local v11    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    .line 194
    .restart local v11    # "cursor":Landroid/database/Cursor;
    .restart local v12    # "id":J
    .restart local v14    # "path":Ljava/lang/String;
    .restart local v15    # "url":Ljava/lang/String;
    .restart local v16    # "size":J
    :catchall_2
    move-exception v2

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v2

    .line 205
    .end local v12    # "id":J
    .end local v14    # "path":Ljava/lang/String;
    .end local v15    # "url":Ljava/lang/String;
    .end local v16    # "size":J
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 207
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 208
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0
.end method

.method private declared-synchronized initialize()V
    .locals 9

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 245
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 228
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mInitialized:Z

    .line 229
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mRoot:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mRoot:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 230
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mRoot:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_3

    .line 231
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cannot create "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mRoot:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 234
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v1, Lcom/sec/android/gallery3d/data/DownloadCache;->TABLE_NAME:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/gallery3d/data/DownloadCache;->SUM_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 236
    .local v8, "cursor":Landroid/database/Cursor;
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTotalBytes:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 238
    :try_start_3
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 239
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTotalBytes:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 242
    :cond_4
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 244
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTotalBytes:J

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mCapacity:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/DownloadCache;->freeSomeSpaceIfNeed(I)V

    goto :goto_0

    .line 242
    :catchall_1
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method private declared-synchronized insertEntry(Ljava/lang/String;Ljava/io/File;)J
    .locals 8
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "file"    # Ljava/io/File;

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 214
    .local v2, "size":J
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTotalBytes:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTotalBytes:J

    .line 216
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 217
    .local v1, "values":Landroid/content/ContentValues;
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->crc64Long(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 218
    .local v0, "hashCode":Ljava/lang/String;
    const-string v4, "_data"

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v4, "hash_code"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string v4, "content_url"

    invoke-virtual {v1, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v4, "_size"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 222
    const-string v4, "last_updated"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 223
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v5, Lcom/sec/android/gallery3d/data/DownloadCache;->TABLE_NAME:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v4, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    monitor-exit p0

    return-wide v4

    .line 213
    .end local v0    # "hashCode":Ljava/lang/String;
    .end local v1    # "values":Landroid/content/ContentValues;
    .end local v2    # "size":J
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method private updateLastAccess(J)V
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 167
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 169
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 170
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "last_access"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 171
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v2, Lcom/sec/android/gallery3d/data/DownloadCache;->TABLE_NAME:Ljava/lang/String;

    const-string v3, "_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 173
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 177
    return-void

    .line 175
    .end local v0    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method


# virtual methods
.method public download(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .locals 8
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "url"    # Ljava/net/URL;

    .prologue
    .line 130
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mInitialized:Z

    if-nez v4, :cond_0

    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/DownloadCache;->initialize()V

    .line 132
    :cond_0
    invoke-virtual {p2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    .line 135
    .local v2, "stringUrl":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    monitor-enter v5

    .line 136
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/common/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    .line 137
    .local v0, "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    if-eqz v0, :cond_1

    .line 138
    iget-wide v6, v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->mId:J

    invoke-direct {p0, v6, v7}, Lcom/sec/android/gallery3d/data/DownloadCache;->updateLastAccess(J)V

    .line 139
    monitor-exit v5

    .line 163
    .end local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :goto_0
    return-object v0

    .line 141
    .restart local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 144
    new-instance v1, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;-><init>()V

    .line 145
    .local v1, "proxy":Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;

    monitor-enter v5

    .line 146
    :try_start_1
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/data/DownloadCache;->findEntryInDatabase(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v0

    .line 147
    if-eqz v0, :cond_2

    .line 148
    iget-wide v6, v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->mId:J

    invoke-direct {p0, v6, v7}, Lcom/sec/android/gallery3d/data/DownloadCache;->updateLastAccess(J)V

    .line 149
    monitor-exit v5

    goto :goto_0

    .line 161
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 141
    .end local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .end local v1    # "proxy":Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;
    :catchall_1
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v4

    .line 154
    .restart local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .restart local v1    # "proxy":Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;
    :cond_2
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;

    .line 155
    .local v3, "task":Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
    if-nez v3, :cond_3

    .line 156
    new-instance v3, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;

    .end local v3    # "task":Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
    invoke-direct {v3, p0, v2}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;-><init>(Lcom/sec/android/gallery3d/data/DownloadCache;Ljava/lang/String;)V

    .line 157
    .restart local v3    # "task":Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;

    invoke-virtual {v4, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v4

    invoke-virtual {v4, v3, v3}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v4

    # setter for: Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mFuture:Lcom/sec/android/gallery3d/util/Future;
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->access$002(Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;Lcom/sec/android/gallery3d/util/Future;)Lcom/sec/android/gallery3d/util/Future;

    .line 160
    :cond_3
    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->addProxy(Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;)V

    .line 161
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 163
    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;->get(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v0

    goto :goto_0
.end method

.method public download(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .locals 1
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "url"    # Ljava/net/URL;
    .param p3, "key"    # Ljava/lang/String;

    .prologue
    .line 456
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/android/gallery3d/data/DownloadCache;->download(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;Ljava/lang/String;Z)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v0

    return-object v0
.end method

.method public download(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;Ljava/lang/String;Z)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .locals 10
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "url"    # Ljava/net/URL;
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "forceDownload"    # Z

    .prologue
    const/4 v6, 0x0

    .line 460
    iget-boolean v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mInitialized:Z

    if-nez v7, :cond_0

    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/DownloadCache;->initialize()V

    .line 462
    :cond_0
    invoke-virtual {p2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v3

    .line 465
    .local v3, "stringUrl":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    monitor-enter v7

    .line 466
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/common/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    .line 467
    .local v0, "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    if-eqz v0, :cond_1

    .line 468
    iget-wide v8, v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->mId:J

    invoke-direct {p0, v8, v9}, Lcom/sec/android/gallery3d/data/DownloadCache;->updateLastAccess(J)V

    .line 469
    monitor-exit v7

    .line 516
    .end local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :goto_0
    return-object v0

    .line 471
    .restart local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :cond_1
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 473
    if-nez p4, :cond_2

    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->allowDownloadBySettings(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 474
    sget-object v7, Lcom/sec/android/gallery3d/data/DownloadCache;->TAG:Ljava/lang/String;

    const-string v8, "download is not available because wifionly is enabled."

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v6

    .line 475
    goto :goto_0

    .line 471
    .end local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 478
    .restart local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :cond_2
    new-instance v5, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;-><init>(Landroid/content/Context;)V

    .line 481
    .local v5, "wifiDisplayUtils":Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;
    new-instance v2, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;-><init>()V

    .line 482
    .local v2, "proxy":Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;

    monitor-enter v7

    .line 483
    :try_start_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/gallery3d/data/DownloadCache;->findEntryInDatabase(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v0

    .line 484
    if-eqz v0, :cond_3

    .line 485
    iget-wide v8, v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->mId:J

    invoke-direct {p0, v8, v9}, Lcom/sec/android/gallery3d/data/DownloadCache;->updateLastAccess(J)V

    .line 486
    monitor-exit v7

    goto :goto_0

    .line 514
    :catchall_1
    move-exception v6

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v6

    .line 489
    :cond_3
    :try_start_3
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v8}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 490
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v8

    if-nez v8, :cond_7

    .line 491
    :cond_4
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v8, :cond_5

    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v8, :cond_5

    .line 492
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->isP2pWorking()Z

    move-result v8

    if-nez v8, :cond_7

    .line 493
    monitor-exit v7

    move-object v0, v6

    goto :goto_0

    .line 495
    :cond_5
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWfd:Z

    if-eqz v8, :cond_6

    .line 496
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->isP2pWorking()Z

    move-result v8

    if-nez v8, :cond_7

    .line 497
    monitor-exit v7

    move-object v0, v6

    goto :goto_0

    .line 500
    :cond_6
    monitor-exit v7

    move-object v0, v6

    goto :goto_0

    .line 505
    :cond_7
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;

    .line 506
    .local v4, "task":Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
    if-nez v4, :cond_8

    .line 507
    new-instance v4, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;

    .end local v4    # "task":Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
    invoke-direct {v4, p0, v3, p3}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;-><init>(Lcom/sec/android/gallery3d/data/DownloadCache;Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    .restart local v4    # "task":Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;

    invoke-virtual {v6, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 509
    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->addProxy(Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;)V

    .line 510
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v6

    invoke-virtual {v6, v4, v4}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v6

    # setter for: Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mFuture:Lcom/sec/android/gallery3d/util/Future;
    invoke-static {v4, v6}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->access$002(Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;Lcom/sec/android/gallery3d/util/Future;)Lcom/sec/android/gallery3d/util/Future;

    .line 514
    :goto_1
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 516
    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;->get(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v0

    goto/16 :goto_0

    .line 512
    :cond_8
    :try_start_4
    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->addProxy(Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1
.end method

.method public downloadNoNetworkProxy(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .locals 8
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "url"    # Ljava/net/URL;
    .param p3, "key"    # Ljava/lang/String;

    .prologue
    .line 521
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mInitialized:Z

    if-nez v4, :cond_0

    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/DownloadCache;->initialize()V

    .line 523
    :cond_0
    invoke-virtual {p2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    .line 526
    .local v2, "stringUrl":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    monitor-enter v5

    .line 527
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/sec/android/gallery3d/common/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    .line 528
    .local v0, "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    if-eqz v0, :cond_1

    .line 529
    iget-wide v6, v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->mId:J

    invoke-direct {p0, v6, v7}, Lcom/sec/android/gallery3d/data/DownloadCache;->updateLastAccess(J)V

    .line 530
    monitor-exit v5

    .line 557
    .end local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :goto_0
    return-object v0

    .line 532
    .restart local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535
    new-instance v1, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;-><init>()V

    .line 536
    .local v1, "proxy":Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;

    monitor-enter v5

    .line 537
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/data/DownloadCache;->findEntryInDatabase(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v0

    .line 538
    if-eqz v0, :cond_2

    .line 539
    iget-wide v6, v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->mId:J

    invoke-direct {p0, v6, v7}, Lcom/sec/android/gallery3d/data/DownloadCache;->updateLastAccess(J)V

    .line 540
    monitor-exit v5

    goto :goto_0

    .line 555
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 532
    .end local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .end local v1    # "proxy":Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;
    :catchall_1
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v4

    .line 545
    .restart local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .restart local v1    # "proxy":Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;
    :cond_2
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;

    .line 546
    .local v3, "task":Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
    if-nez v3, :cond_3

    .line 547
    new-instance v3, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;

    .end local v3    # "task":Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
    invoke-direct {v3, p0, v2, p3}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;-><init>(Lcom/sec/android/gallery3d/data/DownloadCache;Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    .restart local v3    # "task":Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->setNoNetworkProxy(Z)V

    .line 549
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;

    invoke-virtual {v4, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->addProxy(Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;)V

    .line 551
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v4

    invoke-virtual {v4, v3, v3}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v4

    # setter for: Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mFuture:Lcom/sec/android/gallery3d/util/Future;
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->access$002(Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;Lcom/sec/android/gallery3d/util/Future;)Lcom/sec/android/gallery3d/util/Future;

    .line 555
    :goto_1
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 557
    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;->get(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v0

    goto :goto_0

    .line 553
    :cond_3
    :try_start_4
    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->addProxy(Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public lookup(Ljava/net/URL;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .locals 6
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 432
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mInitialized:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/DownloadCache;->initialize()V

    .line 433
    :cond_0
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    .line 436
    .local v1, "stringUrl":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    monitor-enter v3

    .line 437
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/common/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    .line 438
    .local v0, "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    if-eqz v0, :cond_1

    .line 439
    iget-wide v4, v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->mId:J

    invoke-direct {p0, v4, v5}, Lcom/sec/android/gallery3d/data/DownloadCache;->updateLastAccess(J)V

    .line 440
    monitor-exit v3

    move-object v2, v0

    .line 452
    :goto_0
    return-object v2

    .line 442
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;

    monitor-enter v3

    .line 446
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/data/DownloadCache;->findEntryInDatabase(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v0

    .line 447
    if-eqz v0, :cond_2

    .line 448
    iget-wide v4, v0, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->mId:J

    invoke-direct {p0, v4, v5}, Lcom/sec/android/gallery3d/data/DownloadCache;->updateLastAccess(J)V

    .line 449
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v2, v0

    goto :goto_0

    .line 442
    .end local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 451
    .restart local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :cond_2
    :try_start_3
    monitor-exit v3

    .line 452
    const/4 v2, 0x0

    goto :goto_0

    .line 451
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method
