.class public Lcom/sec/android/gallery3d/remote/slink/SLinkManager;
.super Ljava/lang/Object;
.source "SLinkManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;
    }
.end annotation


# static fields
.field public static final MULTIPLE_DEVICES_ID:I = -0x1

.field public static final SLINK_DOWNLOAD_FOLDER:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static volatile mInstance:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mFileTransferUtil:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

.field private mMenuAction:I

.field private mNetworkManager:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

.field private mSLinkInitStateChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mSelectedItemIds:[J

.field private mSignInUtils:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

.field private mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    const-class v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Download"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->SLINK_DOWNLOAD_FOLDER:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x0

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mSelectedItemIds:[J

    .line 67
    new-instance v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$1;-><init>(Lcom/sec/android/gallery3d/remote/slink/SLinkManager;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mSLinkInitStateChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 91
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 92
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mFileTransferUtil:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    .line 93
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mSignInUtils:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    .line 94
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mNetworkManager:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    .line 95
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mNetworkManager:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->createWakeLock(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    .line 96
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/slink/SLinkManager;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/slink/SLinkManager;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mSignInUtils:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/remote/slink/SLinkManager;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    return-object v0
.end method

.method public static final createDeviceChooserActivityIntent(Landroid/content/Context;I[J)Landroid/content/Intent;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sourceDeviceId"    # I
    .param p2, "sLinkRowIds"    # [J

    .prologue
    const/4 v5, 0x0

    .line 348
    invoke-static {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v4

    .line 349
    .local v4, "sLinkMediaSet":Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v1

    int-to-long v2, p1

    move v6, v5

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createDeviceChooserActivityIntent(JLcom/samsung/android/sdk/samsunglink/SlinkMediaSet;ZZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static final downloadFiles(Landroid/content/Context;Ljava/util/List;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 311
    .local p1, "sLinkList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 312
    :cond_0
    sget-object v6, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    const-string v7, "ignore slink download. list is empty"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :goto_0
    return-void

    .line 316
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 317
    .local v3, "n":I
    new-array v4, v3, [J

    .line 318
    .local v4, "rowIds":[J
    const/4 v0, 0x0

    .line 319
    .local v0, "i":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 320
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 321
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .line 322
    .local v5, "sLinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getSlinkObjectId()J

    move-result-wide v6

    aput-wide v6, v4, v0

    move v0, v1

    .line 323
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_1

    .line 325
    .end local v5    # "sLinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    :cond_2
    invoke-static {p0, v4}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->downloadFiles(Landroid/content/Context;[J)V

    goto :goto_0
.end method

.method public static final downloadFiles(Landroid/content/Context;[J)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sLinkRowIds"    # [J

    .prologue
    .line 329
    sget-object v10, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "download slink files : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    array-length v12, p1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    const/4 v3, 0x0

    .line 331
    .local v3, "index":I
    move-object v0, p1

    .local v0, "arr$":[J
    array-length v7, v0

    .local v7, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v6, v3

    .end local v3    # "index":I
    .local v6, "index":I
    :goto_0
    if-ge v2, v7, :cond_0

    aget-wide v4, v0, v2

    .line 332
    .local v4, "id":J
    sget-object v10, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "downloadFiles : index : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    add-int/lit8 v3, v6, 0x1

    .end local v6    # "index":I
    .restart local v3    # "index":I
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", id = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    add-int/lit8 v2, v2, 0x1

    move v6, v3

    .end local v3    # "index":I
    .restart local v6    # "index":I
    goto :goto_0

    .line 334
    .end local v4    # "id":J
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v9

    .line 335
    .local v9, "slinkMediaSet":Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    new-instance v8, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v8}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    .line 336
    .local v8, "options":Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    new-instance v1, Ljava/io/File;

    sget-object v10, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->SLINK_DOWNLOAD_FOLDER:Ljava/lang/String;

    invoke-direct {v1, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 337
    .local v1, "downloadFolder":Ljava/io/File;
    sget-object v10, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "downloadFiles : SLINK_DOWNLOAD_FOLDER = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->SLINK_DOWNLOAD_FOLDER:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    iput-object v1, v8, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->targetDirectory:Ljava/io/File;

    .line 339
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v10

    invoke-virtual {v10, v9, v8}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->downloadFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 340
    return-void
.end method

.method public static getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;
    .locals 2
    .param p0, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 76
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mInstance:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    if-nez v0, :cond_1

    .line 77
    const-class v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    monitor-enter v1

    .line 78
    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mInstance:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mInstance:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .line 81
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mInstance:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    return-object v0

    .line 81
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getModalDownloadIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;
    .locals 6
    .param p1, "sLinkMediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p2, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mFileTransferUtil:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0e049d

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e049e

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object v1, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createModalDownloadActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static final getSourceDeviceId(Landroid/content/Context;Landroid/content/Intent;)J
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 103
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getSourceDeviceIdFromViewIntent(Landroid/content/Intent;)J

    move-result-wide v0

    .line 104
    .local v0, "sourceDeviceId":J
    return-wide v0
.end method

.method public static isModalDownloadNeeded(Landroid/view/MenuItem;)Z
    .locals 2
    .param p0, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 217
    invoke-interface {p0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 218
    .local v0, "itemId":I
    const v1, 0x7f0f026c

    if-eq v0, v1, :cond_0

    const v1, 0x7f0f026d

    if-eq v0, v1, :cond_0

    const v1, 0x7f0f02a7

    if-eq v0, v1, :cond_0

    const v1, 0x7f0f029c

    if-eq v0, v1, :cond_0

    const v1, 0x7f0f029a

    if-eq v0, v1, :cond_0

    const v1, 0x7f0f02a8

    if-eq v0, v1, :cond_0

    const v1, 0x7f0f0283

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isModalDownloadNeeded(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/view/MenuItem;)Z
    .locals 1
    .param p0, "currentPhoto"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 209
    instance-of v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-eqz v0, :cond_0

    .line 210
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->isModalDownloadNeeded(Landroid/view/MenuItem;)Z

    move-result v0

    .line 213
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final isSLinkIntent(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 99
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->isSlinkViewIntent(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public static isSlinkPath(Ljava/lang/String;)Z
    .locals 1
    .param p0, "topSetPath"    # Ljava/lang/String;

    .prologue
    .line 195
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const-string v0, "/slink"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static startChooser(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/List;)V
    .locals 4
    .param p0, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 355
    .local p1, "mediaStoreUris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 356
    .local v1, "shareIntent":Landroid/content/Intent;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 370
    :goto_0
    return-void

    .line 358
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 359
    const-string v2, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 360
    const-string v2, "android.intent.extra.STREAM"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 366
    :goto_1
    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 368
    const v2, 0x7f0e0047

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 369
    .local v0, "chooser":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 362
    .end local v0    # "chooser":Landroid/content/Intent;
    :cond_1
    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 363
    const-string v3, "android.intent.extra.STREAM"

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public static final transferFiles(Landroid/content/Context;[JJ)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sLinkRowIds"    # [J
    .param p2, "sLinkDeviceId"    # J

    .prologue
    .line 343
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    .line 344
    .local v0, "sLinkMediaSet":Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p2, p3, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->transferFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;JLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 345
    return-void
.end method


# virtual methods
.method public acquireWakeLockIfNeeded(Ljava/lang/String;)V
    .locals 3
    .param p1, "topSetPath"    # Ljava/lang/String;

    .prologue
    .line 143
    new-instance v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$2;-><init>(Lcom/sec/android/gallery3d/remote/slink/SLinkManager;)V

    .line 170
    .local v0, "run":Ljava/lang/Runnable;
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->isSlinkPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 171
    :cond_0
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 172
    .local v1, "thread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 174
    .end local v1    # "thread":Ljava/lang/Thread;
    :cond_1
    return-void
.end method

.method public enqueueMenuItemAfterModalDownload(I)V
    .locals 0
    .param p1, "menuAction"    # I

    .prologue
    .line 199
    iput p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mMenuAction:I

    .line 200
    return-void
.end method

.method public getInitializedStatus()Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;
    .locals 4

    .prologue
    .line 450
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;->DEACTIVATED:Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;

    .line 452
    .local v1, "status":Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mNetworkManager:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 453
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;->ACTIVATED:Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 460
    :cond_0
    :goto_0
    return-object v1

    .line 455
    :catch_0
    move-exception v0

    .line 456
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    const-string v3, "getInitializedStatus : exception has occured"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;->ERROR:Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;

    .line 458
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getModalDownloadIntent(Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/content/Intent;
    .locals 6
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 225
    const/4 v3, 0x1

    new-array v0, v3, [J

    .line 226
    .local v0, "id":[J
    instance-of v3, p1, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-eqz v3, :cond_0

    move-object v2, p1

    .line 227
    check-cast v2, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .line 228
    .local v2, "slinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    const/4 v3, 0x0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getSlinkObjectId()J

    move-result-wide v4

    aput-wide v4, v0, v3

    .line 230
    .end local v2    # "slinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    :cond_0
    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v1

    .line 231
    .local v1, "sLinkMediaSet":Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getModalDownloadIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v3

    return-object v3
.end method

.method public getModalDownloadIntent(Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 11
    .param p1, "dstAlbumPath"    # Ljava/lang/String;
    .param p2, "deleteAfterDownload"    # Z

    .prologue
    .line 256
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    .line 258
    .local v3, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v4

    .line 259
    .local v4, "n":I
    new-array v1, v4, [J

    .line 260
    .local v1, "ids":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_1

    .line 261
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 263
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v8, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-eqz v8, :cond_0

    move-object v7, v2

    .line 264
    check-cast v7, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .line 265
    .local v7, "slinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getSlinkObjectId()J

    move-result-wide v8

    aput-wide v8, v1, v0

    .line 260
    .end local v7    # "slinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 269
    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    new-instance v5, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    .line 270
    .local v5, "options":Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 271
    sget-object v8, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getModalDownloadIntent : dstAlbumPath = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v8, v5, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->targetDirectory:Ljava/io/File;

    .line 274
    :cond_2
    iput-boolean p2, v5, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->deleteSourceFilesWhenTransferIsComplete:Z

    .line 276
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v6

    .line 277
    .local v6, "sLinkMediaSet":Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    invoke-direct {p0, v6, v5}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getModalDownloadIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v8

    return-object v8
.end method

.method public getModalDownloadIntent(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v2, v6, [J

    .line 237
    .local v2, "ids":[J
    const/4 v0, 0x0

    .line 238
    .local v0, "count":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 239
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v6, v3, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-eqz v6, :cond_0

    move-object v5, v3

    .line 240
    check-cast v5, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .line 241
    .local v5, "slinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getSlinkObjectId()J

    move-result-wide v6

    aput-wide v6, v2, v0

    .line 242
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 245
    .end local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v5    # "slinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    :cond_1
    invoke-static {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v4

    .line 246
    .local v4, "sLinkMediaSet":Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    const/4 v6, 0x0

    invoke-direct {p0, v4, v6}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getModalDownloadIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v6

    return-object v6
.end method

.method public getSamsungAccountStatus()Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;
    .locals 4

    .prologue
    .line 464
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;->DEACTIVATED:Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;

    .line 466
    .local v1, "status":Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mSignInUtils:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->samsungAccountExists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 467
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;->ACTIVATED:Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    :cond_0
    :goto_0
    return-object v1

    .line 469
    :catch_0
    move-exception v0

    .line 470
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    const-string v3, "getSamsungAccountStatus : exception has occured"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;->ERROR:Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;

    .line 472
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getSelectedItemIds()[J
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mSelectedItemIds:[J

    return-object v0
.end method

.method public getSignedInStatus()Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;
    .locals 4

    .prologue
    .line 429
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;->DEACTIVATED:Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;

    .line 431
    .local v1, "status":Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mSignInUtils:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 432
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;->ACTIVATED:Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 439
    :cond_0
    :goto_0
    return-object v1

    .line 434
    :catch_0
    move-exception v0

    .line 435
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    const-string v3, "getSignedInStatus : exception has occured"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;->ERROR:Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;

    .line 437
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public isActivateSignedStatus()Z
    .locals 2

    .prologue
    .line 443
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getSignedInStatus()Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;->ACTIVATED:Lcom/sec/android/gallery3d/remote/slink/SLinkManager$StatusType;

    if-ne v0, v1, :cond_0

    .line 444
    const/4 v0, 0x1

    .line 446
    :goto_0
    return v0

    .line 445
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "signed status is not Activated "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pollItemAfterModalDownload()I
    .locals 2

    .prologue
    .line 203
    iget v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mMenuAction:I

    .line 204
    .local v0, "result":I
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mMenuAction:I

    .line 205
    return v0
.end method

.method public refresh(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 4
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 373
    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    if-eqz v1, :cond_0

    .line 374
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    const-string v2, "refresh"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p1

    .line 375
    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    .line 376
    .local v0, "slinkStorage":Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->getStorageId()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->requestRefresh(J)V

    .line 378
    .end local v0    # "slinkStorage":Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;
    :cond_0
    return-void
.end method

.method public registerInitStateChangedReceiver()V
    .locals 3

    .prologue
    .line 414
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    const-string v2, "registerInitStateChangedReceiver : REGISTER InitStateChangedReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.BROADCAST_INITIALIZING_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 416
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mSLinkInitStateChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 417
    return-void
.end method

.method public releaseInstance()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mInstance:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .line 88
    return-void
.end method

.method public releaseWakeLockIfNeeded(Ljava/lang/String;)V
    .locals 3
    .param p1, "topSetPath"    # Ljava/lang/String;

    .prologue
    .line 177
    new-instance v0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$3;-><init>(Lcom/sec/android/gallery3d/remote/slink/SLinkManager;)V

    .line 188
    .local v0, "run":Ljava/lang/Runnable;
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->isSlinkPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 189
    :cond_0
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 190
    .local v1, "thread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 192
    .end local v1    # "thread":Ljava/lang/Thread;
    :cond_1
    return-void
.end method

.method public setSelectedItemIds(Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 8
    .param p1, "selectionManager"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 381
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaItemArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 382
    .local v3, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 383
    .local v4, "n":I
    new-array v1, v4, [J

    .line 384
    .local v1, "ids":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_1

    .line 385
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 386
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v6, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-eqz v6, :cond_0

    move-object v5, v2

    .line 387
    check-cast v5, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .line 388
    .local v5, "slinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getSlinkObjectId()J

    move-result-wide v6

    aput-wide v6, v1, v0

    .line 384
    .end local v5    # "slinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 391
    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mSelectedItemIds:[J

    .line 392
    return-void
.end method

.method public setSyncPriority()V
    .locals 2

    .prologue
    .line 478
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->setSyncMediaPriority(I)V

    .line 480
    return-void
.end method

.method public unregisterInitStateChangedReceiver()V
    .locals 3

    .prologue
    .line 420
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "unregisterInitStateChangedReceiver : UNREGISTER InitStateChangedReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mSLinkInitStateChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 426
    :goto_0
    return-void

    .line 423
    :catch_0
    move-exception v0

    .line 424
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public uploadFiles(Ljava/lang/String;Landroid/net/Uri;ZZ)Z
    .locals 10
    .param p1, "mediaSetPath"    # Ljava/lang/String;
    .param p2, "mediaStoreUri"    # Landroid/net/Uri;
    .param p3, "skipIfDuplicate"    # Z
    .param p4, "deleteSourceAfterUpload"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 284
    if-nez p2, :cond_0

    .line 285
    sget-object v6, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    const-string v7, "no mediastore"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    :goto_0
    return v5

    .line 288
    :cond_0
    new-array v0, v6, [J

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    aput-wide v8, v0, v5

    .line 292
    .local v0, "ids":[J
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v7

    invoke-static {p1}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 293
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v7, v1, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    if-nez v7, :cond_1

    .line 294
    sget-object v6, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    const-string v7, "not slink storage"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 298
    :cond_1
    new-instance v2, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    .line 299
    .local v2, "options":Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    iput-boolean p3, v2, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->skipIfDuplicate:Z

    .line 300
    iput-boolean p4, v2, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->deleteSourceFilesWhenTransferIsComplete:Z

    .line 302
    sget-object v5, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "uploadFiles, skip if duplicate : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", delete : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v3

    .local v3, "sLinkMediaSet":Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    move-object v4, v1

    .line 305
    check-cast v4, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    .line 306
    .local v4, "slinkStorage":Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v5

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->getStorageId()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v5, v3, v8, v9, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->uploadMediaStoreFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;JLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    move v5, v6

    .line 307
    goto :goto_0
.end method
