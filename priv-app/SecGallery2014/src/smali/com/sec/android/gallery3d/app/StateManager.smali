.class public Lcom/sec/android/gallery3d/app/StateManager;
.super Ljava/lang/Object;
.source "StateManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/StateManager$StateEntry;
    }
.end annotation


# static fields
.field private static final FILTER_TYPE:Ljava/lang/String; = "filter_type"

.field private static final KEY_APP_BRIDGE:Ljava/lang/String; = "app-bridge"

.field private static final KEY_BUNDLE:Ljava/lang/String; = "status_proxy"

.field private static final KEY_CLASS:Ljava/lang/String; = "class"

.field private static final KEY_DATA:Ljava/lang/String; = "data"

.field private static final KEY_IS_ALLVIEW:Ljava/lang/String; = "is_allview"

.field private static final KEY_MAIN:Ljava/lang/String; = "activity-state"

.field private static final KEY_STATE:Ljava/lang/String; = "bundle"

.field private static final LAUNCH_MODE:Ljava/lang/String; = "launch_mode"

.field private static final TAB_TYPE:Ljava/lang/String; = "tab_type"

.field private static final TAG:Ljava/lang/String; = "StateManager"

.field private static final UPBUTTON_VISIBILITY:Ljava/lang/String; = "upbutton_visibility"

.field private static final VIEW_BY_TYPE:Ljava/lang/String; = "view_by_type"


# instance fields
.field private isMultiFormatShareActive:Z

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mCurrenViewByType:I

.field private mCurrentLaunchModeType:Lcom/sec/samsung/gallery/core/LaunchModeType;

.field private mCurrentMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

.field private mCurrentTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

.field private mCurrentViewModeType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;"
        }
    .end annotation
.end field

.field private mIsDownloadMode:Z

.field private mIsHelpMode:Z

.field private mIsNeedMenuExpand:Z

.field private mIsResumed:Z

.field private mIsSearchMode:Z

.field private mIsStartIdle:Z

.field private mIsUpButtonVisible:Z

.field private mLastQueryText:Ljava/lang/String;

.field private mLaunchFromSetupWidzard:Z

.field private mNewMP4FilePath:Ljava/lang/String;

.field private mPreviousActivityState:Lcom/sec/android/gallery3d/app/ActivityState;

.field private mPreviousBitmap:Landroid/graphics/Bitmap;

.field private mPreviousInfo:[Ljava/lang/Object;

.field private mPreviousRotation:I

.field private mPreviousViewState:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;"
        }
    .end annotation
.end field

.field private mReloadOnResume:Z

.field private mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

.field private mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

.field private mSearchText:Ljava/lang/String;

.field private mStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/sec/android/gallery3d/app/StateManager$StateEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 4
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    .line 73
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    .line 626
    sget-object v0, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE_AND_VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .line 639
    sget-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrenViewByType:I

    .line 663
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 704
    const-class v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentViewModeType:Ljava/lang/Class;

    .line 717
    sget-object v0, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentLaunchModeType:Lcom/sec/samsung/gallery/core/LaunchModeType;

    .line 730
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mReloadOnResume:Z

    .line 756
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsSearchMode:Z

    .line 769
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsDownloadMode:Z

    .line 782
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsNeedMenuExpand:Z

    .line 795
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsUpButtonVisible:Z

    .line 808
    iput-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mSearchText:Ljava/lang/String;

    .line 818
    iput-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mLastQueryText:Ljava/lang/String;

    .line 831
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mLaunchFromSetupWidzard:Z

    .line 844
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsHelpMode:Z

    .line 881
    const-class v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousViewState:Ljava/lang/Class;

    .line 916
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousInfo:[Ljava/lang/Object;

    .line 929
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsStartIdle:Z

    .line 77
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 80
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "viewByFilterTypes"

    sget-object v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrenViewByType:I

    .line 82
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlexibleCloudAlbumFormat:Z

    if-eqz v0, :cond_0

    .line 83
    sget-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUDALBUM_FORMAT:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    invoke-static {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->changeCloudAlbumFormat(Z)V

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->changeCloudAlbumFormat(Z)V

    goto :goto_0
.end method

.method private resetSortByFilterType(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    const/4 v3, 0x0

    .line 609
    const-class v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eq p1, v2, :cond_3

    const-class v2, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    if-eq p1, v2, :cond_3

    const/4 v0, 0x1

    .line 610
    .local v0, "needResetSortType":Z
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 611
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 612
    .local v1, "previousViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v2, v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-nez v2, :cond_0

    instance-of v2, v1, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    if-eqz v2, :cond_1

    :cond_0
    const-class v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-ne p1, v2, :cond_1

    .line 614
    const/4 v0, 0x0

    .line 617
    .end local v1    # "previousViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_1
    if-eqz v0, :cond_2

    .line 618
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2, v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->setSortByType(I)V

    .line 620
    :cond_2
    return-void

    .end local v0    # "needResetSortType":Z
    :cond_3
    move v0, v3

    .line 609
    goto :goto_0
.end method

.method private saveLatestViewState()V
    .locals 2

    .prologue
    .line 595
    new-instance v0, Lcom/sec/samsung/gallery/view/ViewStateHistory;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/ViewStateHistory;-><init>(Landroid/content/Context;)V

    .line 596
    .local v0, "viewStateHistory":Lcom/sec/samsung/gallery/view/ViewStateHistory;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ViewStateHistory;->saveLatestViewState()V

    .line 597
    return-void
.end method


# virtual methods
.method public clearActivityResult()V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/ActivityState;->clearStateResult()V

    .line 217
    :cond_0
    return-void
.end method

.method public clearTasks()V
    .locals 2

    .prologue
    .line 262
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 263
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/ActivityState;->onDestroy()V

    goto :goto_0

    .line 265
    :cond_0
    return-void
.end method

.method public createOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x0

    .line 167
    :goto_0
    return v0

    .line 166
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 167
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 373
    const-string v0, "StateManager"

    const-string v1, "destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/ActivityState;->onDestroy()V

    goto :goto_0

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 378
    return-void
.end method

.method public finishAllViewState()V
    .locals 2

    .prologue
    .line 600
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 601
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 602
    .local v0, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    if-eqz v0, :cond_0

    .line 603
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    goto :goto_0

    .line 606
    .end local v0    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_1
    return-void
.end method

.method public finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V
    .locals 1
    .param p1, "state"    # Lcom/sec/android/gallery3d/app/ActivityState;

    .prologue
    .line 251
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;Z)V

    .line 252
    return-void
.end method

.method finishState(Lcom/sec/android/gallery3d/app/ActivityState;Z)V
    .locals 7
    .param p1, "state"    # Lcom/sec/android/gallery3d/app/ActivityState;
    .param p2, "fireOnPause"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 270
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    if-ne v2, v6, :cond_2

    .line 271
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 272
    .local v0, "activity":Landroid/app/Activity;
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    if-eqz v2, :cond_0

    .line 273
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    iget v2, v2, Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;->resultCode:I

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    iget-object v4, v4, Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;->resultData:Landroid/content/Intent;

    invoke-virtual {v0, v2, v4}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 275
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 276
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_1

    .line 277
    const-string v2, "StateManager"

    const-string v3, "finish is rejected, keep the last state"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    .end local v0    # "activity":Landroid/app/Activity;
    :goto_0
    return-void

    .line 280
    .restart local v0    # "activity":Landroid/app/Activity;
    :cond_1
    const-string v2, "StateManager"

    const-string v4, "no more state, finish activity"

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_2
    if-nez p1, :cond_3

    .line 285
    const-string v2, "StateManager"

    const-string/jumbo v3, "state is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 289
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 290
    const-string v2, "StateManager"

    const-string v3, "There is no state to be finished in the stack"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 295
    :cond_4
    const-string v2, "StateManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "finishState "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    if-eq p1, v2, :cond_6

    .line 297
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/ActivityState;->isDestroyed()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 298
    const-string v2, "StateManager"

    const-string v3, "The state is already destroyed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 301
    :cond_5
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The stateview to be finished is not at the top of the stack: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 308
    :cond_6
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 309
    iput-boolean v6, p1, Lcom/sec/android/gallery3d/app/ActivityState;->mIsFinishing:Z

    .line 310
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    iget-object v1, v2, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    .line 311
    .local v1, "top":Lcom/sec/android/gallery3d/app/ActivityState;
    :goto_1
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    if-eqz v2, :cond_8

    if-eqz p2, :cond_8

    .line 312
    if-eqz v1, :cond_7

    .line 313
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    sget-object v5, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;->Outgoing:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    invoke-virtual {p1, v2, v4, v5}, Lcom/sec/android/gallery3d/app/ActivityState;->transitionOnNextPause(Ljava/lang/Class;Ljava/lang/Class;Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;)V

    .line 316
    :cond_7
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onPause()V

    .line 318
    :cond_8
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v2

    invoke-interface {v2, v3}, Lcom/sec/android/gallery3d/ui/GLRoot;->setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 319
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onDestroy()V

    .line 321
    if-eqz v1, :cond_9

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/ActivityState;->resume()V

    .line 322
    :cond_9
    if-eqz v1, :cond_a

    .line 323
    const-string v2, "Gallery"

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/UsageStatistics;->onContentViewChanged(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :cond_a
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 329
    .restart local v0    # "activity":Landroid/app/Activity;
    new-instance v2, Lcom/sec/android/gallery3d/app/StateManager$2;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/gallery3d/app/StateManager$2;-><init>(Lcom/sec/android/gallery3d/app/StateManager;Landroid/app/Activity;)V

    invoke-virtual {v0, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .end local v0    # "activity":Landroid/app/Activity;
    .end local v1    # "top":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_b
    move-object v1, v3

    .line 310
    goto :goto_1
.end method

.method public galleryEventReceived(Lcom/sec/android/gallery3d/app/GalleryEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/sec/android/gallery3d/app/GalleryEvent;

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 523
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onGalleryEventReceived(Lcom/sec/android/gallery3d/app/GalleryEvent;)V

    .line 525
    :cond_0
    return-void
.end method

.method public getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentLaunchModeType:Lcom/sec/samsung/gallery/core/LaunchModeType;

    return-object v0
.end method

.method public getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    return-object v0
.end method

.method public getCurrentSearchAlbum()Lcom/sec/android/gallery3d/data/SearchAlbum;
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    return-object v0
.end method

.method public getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 4

    .prologue
    .line 668
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPickMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiPickMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->isUpButtonVisible()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentLaunchModeType:Lcom/sec/samsung/gallery/core/LaunchModeType;

    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v1, v2, :cond_0

    .line 669
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDefaultAlbumView:Z

    if-eqz v1, :cond_1

    .line 670
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "lastViewType"

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/TabTagType;->from(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 674
    .local v0, "tabTag":I
    :goto_0
    invoke-static {v0}, Lcom/sec/samsung/gallery/core/TabTagType;->from(I)Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 676
    .end local v0    # "tabTag":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    return-object v1

    .line 672
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "lastViewType"

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/TabTagType;->from(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .restart local v0    # "tabTag":I
    goto :goto_0
.end method

.method public getCurrentTabTagType(Z)Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 4
    .param p1, "fromPreference"    # Z

    .prologue
    .line 681
    if-eqz p1, :cond_0

    .line 682
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "lastViewType"

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/TabTagType;->from(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 683
    .local v0, "tabTag":I
    invoke-static {v0}, Lcom/sec/samsung/gallery/core/TabTagType;->from(I)Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 685
    .end local v0    # "tabTag":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    return-object v1
.end method

.method public getCurrentViewByType()I
    .locals 1

    .prologue
    .line 642
    iget v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrenViewByType:I

    return v0
.end method

.method public getCurrentViewMode()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 707
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentViewModeType:Ljava/lang/Class;

    return-object v0
.end method

.method public getLastQueryText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 825
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mLastQueryText:Ljava/lang/String;

    return-object v0
.end method

.method public getNewMP4FilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mNewMP4FilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getPreviousActivityState()Lcom/sec/android/gallery3d/app/ActivityState;
    .locals 1

    .prologue
    .line 894
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousActivityState:Lcom/sec/android/gallery3d/app/ActivityState;

    return-object v0
.end method

.method public getPreviousBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 909
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getPreviousRotation()I
    .locals 1

    .prologue
    .line 913
    iget v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousRotation:I

    return v0
.end method

.method public getPreviousState()Lcom/sec/android/gallery3d/app/ActivityState;
    .locals 3

    .prologue
    .line 487
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_1

    .line 488
    :cond_0
    const/4 v1, 0x0

    .line 491
    :goto_0
    return-object v1

    .line 490
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v0

    .line 491
    .local v0, "size":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    add-int/lit8 v2, v0, -0x2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    goto :goto_0
.end method

.method public getPreviousViewState()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 884
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousViewState:Ljava/lang/Class;

    return-object v0
.end method

.method public getReloadRequiredOnResume()Z
    .locals 1

    .prologue
    .line 737
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mReloadOnResume:Z

    return v0
.end method

.method public getSearchText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 815
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mSearchText:Ljava/lang/String;

    return-object v0
.end method

.method public getStateCount()I
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    return v0
.end method

.method public getTopState()Lcom/sec/android/gallery3d/app/ActivityState;
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481
    const/4 v0, 0x0

    .line 483
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    goto :goto_0
.end method

.method public hasStateClass(Ljava/lang/Class;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 470
    .local p1, "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    .line 471
    .local v0, "entry":Lcom/sec/android/gallery3d/app/StateManager$StateEntry;
    iget-object v2, v0, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    invoke-virtual {p1, v2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 472
    const/4 v2, 0x1

    .line 475
    .end local v0    # "entry":Lcom/sec/android/gallery3d/app/StateManager$StateEntry;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isActiveMultiFormatShare()Z
    .locals 1

    .prologue
    .line 871
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->isMultiFormatShareActive:Z

    return v0
.end method

.method public isDownloadMode()Z
    .locals 1

    .prologue
    .line 776
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsDownloadMode:Z

    return v0
.end method

.method public isHelpMode()Z
    .locals 1

    .prologue
    .line 851
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsHelpMode:Z

    return v0
.end method

.method public isLaunchFromSetupWidzard()Z
    .locals 1

    .prologue
    .line 838
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mLaunchFromSetupWidzard:Z

    return v0
.end method

.method public isNeedMenuExpand()Z
    .locals 1

    .prologue
    .line 789
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsNeedMenuExpand:Z

    return v0
.end method

.method public isSearchMode()Z
    .locals 1

    .prologue
    .line 763
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsSearchMode:Z

    return v0
.end method

.method public isStartIdleState()Z
    .locals 1

    .prologue
    .line 936
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsStartIdle:Z

    return v0
.end method

.method public isUpButtonVisible()Z
    .locals 1

    .prologue
    .line 802
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsUpButtonVisible:Z

    return v0
.end method

.method public itemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 224
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    .line 226
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 227
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/ActivityState;->onBackPressed()V

    .line 236
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/gallery3d/app/ActivityState;->onStateResult(IILandroid/content/Intent;)V

    .line 211
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/StateManager;->saveLatestViewState()V

    .line 243
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/ActivityState;->onBackPressed()V

    .line 246
    :cond_0
    return-void
.end method

.method public onConfigurationChange(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 182
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    .line 183
    .local v0, "entry":Lcom/sec/android/gallery3d/app/StateManager$StateEntry;
    iget-object v2, v0, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0

    .line 185
    .end local v0    # "entry":Lcom/sec/android/gallery3d/app/StateManager$StateEntry;
    :cond_0
    return-void
.end method

.method public onMWLayoutChanged()V
    .locals 3

    .prologue
    .line 528
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 529
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 530
    .local v0, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    if-nez v0, :cond_0

    .line 531
    new-instance v1, Ljava/lang/AssertionError;

    const-string v2, "getTopState() returns null"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 533
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/ActivityState;->onMWLayoutChanged()V

    .line 535
    .end local v0    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_1
    return-void
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onPostCreate(Landroid/os/Bundle;)V

    .line 258
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 173
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 175
    .local v0, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 176
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 179
    .end local v0    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 202
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    if-nez v0, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    .line 204
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/ActivityState;->pause()V

    goto :goto_0
.end method

.method public popPreviousInfo(I)Ljava/lang/Object;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 923
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousInfo:[Ljava/lang/Object;

    aget-object v0, v1, p1

    .line 925
    .local v0, "retObj":Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousInfo:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v1, p1

    .line 926
    return-object v0
.end method

.method public prepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 507
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 508
    const/4 v0, 0x0

    .line 511
    :goto_0
    return v0

    .line 510
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onPrepareOptionMenu(Landroid/view/Menu;)V

    .line 511
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public pushPreviousInfo(ILjava/lang/Object;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "info"    # Ljava/lang/Object;

    .prologue
    .line 919
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousInfo:[Ljava/lang/Object;

    aput-object p2, v0, p1

    .line 920
    return-void
.end method

.method public pushStateAtBottom(Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 5
    .param p2, "bundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 571
    .local p1, "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    const-string v3, "StateManager"

    const-string v4, "pushStateAtBottom"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    new-instance v2, Ljava/util/Stack;

    invoke-direct {v2}, Ljava/util/Stack;-><init>()V

    .line 575
    .local v2, "tempStack":Ljava/util/Stack;, "Ljava/util/Stack<Lcom/sec/android/gallery3d/app/StateManager$StateEntry;>;"
    :goto_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 576
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 579
    :cond_0
    const/4 v1, 0x0

    .line 581
    .local v1, "state":Lcom/sec/android/gallery3d/app/ActivityState;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    check-cast v1, Lcom/sec/android/gallery3d/app/ActivityState;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 585
    .restart local v1    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v3, p2}, Lcom/sec/android/gallery3d/app/ActivityState;->initialize(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/os/Bundle;)V

    .line 586
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    new-instance v4, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    invoke-direct {v4, p2, v1}, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;-><init>(Landroid/os/Bundle;Lcom/sec/android/gallery3d/app/ActivityState;)V

    invoke-virtual {v3, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 587
    const/4 v3, 0x0

    invoke-virtual {v1, p2, v3}, Lcom/sec/android/gallery3d/app/ActivityState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 589
    :goto_1
    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 590
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 582
    .end local v1    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    :catch_0
    move-exception v0

    .line 583
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 592
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_1
    return-void
.end method

.method public restoreFromState(Landroid/os/Bundle;)V
    .locals 22
    .param p1, "inState"    # Landroid/os/Bundle;

    .prologue
    .line 382
    const-string v19, "StateManager"

    const-string v20, "restoreFromState"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    const-string v19, "is_allview"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v19

    const-string v20, "START_GALLERY_VIEW"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 389
    :cond_1
    const-string/jumbo v19, "status_proxy"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 390
    .local v4, "bundle":Landroid/os/Bundle;
    if-eqz v4, :cond_6

    .line 391
    const-string/jumbo v19, "tab_type"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v15

    check-cast v15, Lcom/sec/samsung/gallery/core/TabTagType;

    .line 392
    .local v15, "tag":Lcom/sec/samsung/gallery/core/TabTagType;
    const-string v19, "launch_mode"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/core/LaunchModeType;

    .line 393
    .local v5, "currentLaunchMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    const-string/jumbo v19, "view_by_type"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    .line 394
    .local v18, "viewBy":Ljava/lang/Integer;
    const-string/jumbo v19, "upbutton_visibility"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v17

    check-cast v17, Ljava/lang/Boolean;

    .line 395
    .local v17, "upButtonVisibility":Ljava/lang/Boolean;
    const-string v19, "filter_type"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .line 396
    .local v6, "currentMediaFilterType":Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    if-eqz v15, :cond_2

    .line 397
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 398
    :cond_2
    if-eqz v5, :cond_3

    .line 399
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentLaunchMode(Lcom/sec/samsung/gallery/core/LaunchModeType;)V

    .line 400
    :cond_3
    if-eqz v18, :cond_4

    .line 401
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentViewByType(I)V

    .line 402
    :cond_4
    if-eqz v17, :cond_5

    .line 403
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/app/StateManager;->mIsUpButtonVisible:Z

    .line 404
    :cond_5
    if-eqz v6, :cond_6

    .line 405
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentMediaFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V

    .line 409
    .end local v5    # "currentLaunchMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    .end local v6    # "currentMediaFilterType":Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .end local v15    # "tag":Lcom/sec/samsung/gallery/core/TabTagType;
    .end local v17    # "upButtonVisibility":Ljava/lang/Boolean;
    .end local v18    # "viewBy":Ljava/lang/Integer;
    :cond_6
    const-string v19, "activity-state"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v12

    .line 410
    .local v12, "list":[Landroid/os/Parcelable;
    if-eqz v12, :cond_0

    .line 412
    const/16 v16, 0x0

    .line 413
    .local v16, "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    move-object v3, v12

    .local v3, "arr$":[Landroid/os/Parcelable;
    array-length v11, v3

    .local v11, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_1
    if-ge v9, v11, :cond_7

    aget-object v13, v3, v9

    .local v13, "parcelable":Landroid/os/Parcelable;
    move-object v4, v13

    .line 414
    check-cast v4, Landroid/os/Bundle;

    .line 415
    const-string v19, "class"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v10

    check-cast v10, Ljava/lang/Class;

    .line 418
    .local v10, "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    const-string v19, "data"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    .line 419
    .local v7, "data":Landroid/os/Bundle;
    const-string v19, "bundle"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v14

    .line 423
    .local v14, "state":Landroid/os/Bundle;
    :try_start_0
    const-string v19, "StateManager"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "restoreFromState "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    invoke-virtual {v10}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/ActivityState;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 428
    .local v2, "activityState":Lcom/sec/android/gallery3d/app/ActivityState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v7}, Lcom/sec/android/gallery3d/app/ActivityState;->initialize(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/os/Bundle;)V

    .line 429
    invoke-virtual {v2, v7, v14}, Lcom/sec/android/gallery3d/app/ActivityState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 430
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    move-object/from16 v19, v0

    new-instance v20, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    move-object/from16 v0, v20

    invoke-direct {v0, v7, v2}, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;-><init>(Landroid/os/Bundle;Lcom/sec/android/gallery3d/app/ActivityState;)V

    invoke-virtual/range {v19 .. v20}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    move-object/from16 v16, v2

    .line 413
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 425
    .end local v2    # "activityState":Lcom/sec/android/gallery3d/app/ActivityState;
    :catch_0
    move-exception v8

    .line 426
    .local v8, "e":Ljava/lang/Exception;
    new-instance v19, Ljava/lang/AssertionError;

    move-object/from16 v0, v19

    invoke-direct {v0, v8}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v19

    .line 433
    .end local v7    # "data":Landroid/os/Bundle;
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v10    # "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    .end local v13    # "parcelable":Landroid/os/Parcelable;
    .end local v14    # "state":Landroid/os/Bundle;
    :cond_7
    if-eqz v16, :cond_0

    .line 434
    const-string v19, "Gallery"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/gallery3d/util/UsageStatistics;->onContentViewChanged(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    if-eqz v0, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    .line 190
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/ActivityState;->resume()V

    .line 195
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    goto :goto_0
.end method

.method public saveState(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 440
    const-string v7, "StateManager"

    const-string v8, "saveState"

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 443
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v7, "tab_type"

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 444
    const-string v7, "launch_mode"

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 445
    const-string/jumbo v7, "view_by_type"

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 446
    const-string/jumbo v7, "upbutton_visibility"

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsUpButtonVisible:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 447
    const-string v7, "filter_type"

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 448
    const-string/jumbo v7, "status_proxy"

    invoke-virtual {p1, v7, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 451
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v7}, Ljava/util/Stack;->size()I

    move-result v7

    new-array v5, v7, [Landroid/os/Parcelable;

    .line 452
    .local v5, "list":[Landroid/os/Parcelable;
    const/4 v2, 0x0

    .line 453
    .local v2, "i":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v7}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    .line 454
    .local v1, "entry":Lcom/sec/android/gallery3d/app/StateManager$StateEntry;
    iget-object v7, v1, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.samsung.gallery.view.allview.AllViewState"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 455
    const-string v7, "is_allview"

    const/4 v8, 0x1

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 457
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "bundle":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 458
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-string v7, "class"

    iget-object v8, v1, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 459
    iget-object v7, v1, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    iget-object v8, v1, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->data:Landroid/os/Bundle;

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/app/ActivityState;->onSaveState(Landroid/os/Bundle;)V

    .line 460
    const-string v7, "data"

    iget-object v8, v1, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->data:Landroid/os/Bundle;

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 461
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 462
    .local v6, "state":Landroid/os/Bundle;
    const-string v7, "bundle"

    invoke-virtual {v0, v7, v6}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 463
    const-string v7, "StateManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "saveState "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v1, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    aput-object v0, v5, v2

    move v2, v3

    .line 465
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 466
    .end local v1    # "entry":Lcom/sec/android/gallery3d/app/StateManager$StateEntry;
    .end local v6    # "state":Landroid/os/Bundle;
    :cond_1
    const-string v7, "activity-state"

    invoke-virtual {p1, v7, v5}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 467
    return-void
.end method

.method public setCurrentLaunchMode(Lcom/sec/samsung/gallery/core/LaunchModeType;)V
    .locals 0
    .param p1, "type"    # Lcom/sec/samsung/gallery/core/LaunchModeType;

    .prologue
    .line 720
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentLaunchModeType:Lcom/sec/samsung/gallery/core/LaunchModeType;

    .line 721
    return-void
.end method

.method public setCurrentMediaFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V
    .locals 0
    .param p1, "currentMediaFilterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    .line 633
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .line 634
    return-void
.end method

.method public setCurrentSearchAlbum(Lcom/sec/android/gallery3d/data/SearchAlbum;)V
    .locals 0
    .param p1, "searAlbum"    # Lcom/sec/android/gallery3d/data/SearchAlbum;

    .prologue
    .line 746
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    .line 747
    return-void
.end method

.method public setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 1
    .param p1, "currentTabTagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 689
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;Z)V

    .line 690
    return-void
.end method

.method public setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;Z)V
    .locals 4
    .param p1, "currentTabTagType"    # Lcom/sec/samsung/gallery/core/TabTagType;
    .param p2, "isPick"    # Z

    .prologue
    .line 693
    invoke-static {p1}, Lcom/sec/samsung/gallery/core/TabTagType;->from(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v0

    .line 694
    .local v0, "tabTag":I
    if-nez p2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->isUpButtonVisible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 695
    const-string v1, "StateManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setCurrentTabTagType saveState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 696
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "lastViewType"

    invoke-static {v1, v2, v0}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 698
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 699
    return-void
.end method

.method public setCurrentViewByType(I)V
    .locals 1
    .param p1, "currentViewByType"    # I

    .prologue
    .line 646
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentViewByType(IZ)V

    .line 647
    return-void
.end method

.method public setCurrentViewByType(IZ)V
    .locals 3
    .param p1, "currentViewByType"    # I
    .param p2, "isPickerMode"    # Z

    .prologue
    .line 650
    iput p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrenViewByType:I

    .line 652
    if-nez p2, :cond_0

    .line 653
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "viewByFilterTypes"

    iget v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrenViewByType:I

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 658
    :cond_0
    return-void
.end method

.method public setCurrentViewMode(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 711
    .local p1, "currentViewModeType":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mCurrentViewModeType:Ljava/lang/Class;

    .line 712
    return-void
.end method

.method public setDownloadMode(Z)V
    .locals 0
    .param p1, "isDownloadCloudMode"    # Z

    .prologue
    .line 772
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsDownloadMode:Z

    .line 773
    return-void
.end method

.method public setHelpMode(Z)V
    .locals 0
    .param p1, "isHelpMode"    # Z

    .prologue
    .line 847
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsHelpMode:Z

    .line 848
    return-void
.end method

.method public setLastQueryText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 821
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mLastQueryText:Ljava/lang/String;

    .line 822
    return-void
.end method

.method public setLaunchFromSetupWidzard(Z)V
    .locals 0
    .param p1, "lanuchFromSetupWidzard"    # Z

    .prologue
    .line 834
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mLaunchFromSetupWidzard:Z

    .line 835
    return-void
.end method

.method public setMultiFormatShareActive(ZLjava/lang/String;)V
    .locals 0
    .param p1, "isActive"    # Z
    .param p2, "newMP4FilePath"    # Ljava/lang/String;

    .prologue
    .line 866
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->isMultiFormatShareActive:Z

    .line 867
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mNewMP4FilePath:Ljava/lang/String;

    .line 868
    return-void
.end method

.method public setNeedMenuExpand(Z)V
    .locals 0
    .param p1, "bool"    # Z

    .prologue
    .line 785
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsNeedMenuExpand:Z

    .line 786
    return-void
.end method

.method public setPreviousActivityState(Lcom/sec/android/gallery3d/app/ActivityState;)V
    .locals 0
    .param p1, "previousActivityState"    # Lcom/sec/android/gallery3d/app/ActivityState;

    .prologue
    .line 898
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousActivityState:Lcom/sec/android/gallery3d/app/ActivityState;

    .line 899
    return-void
.end method

.method public setPreviousBitmap(Landroid/graphics/Bitmap;I)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "rotation"    # I

    .prologue
    .line 904
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousBitmap:Landroid/graphics/Bitmap;

    .line 905
    iput p2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousRotation:I

    .line 906
    return-void
.end method

.method public setPreviousViewState(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 888
    .local p1, "previousViewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mPreviousViewState:Ljava/lang/Class;

    .line 889
    return-void
.end method

.method public setReloadRequiredOnResume(Z)V
    .locals 0
    .param p1, "newState"    # Z

    .prologue
    .line 733
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mReloadOnResume:Z

    .line 734
    return-void
.end method

.method public setSearchMode(Z)V
    .locals 0
    .param p1, "isSearchMode"    # Z

    .prologue
    .line 759
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsSearchMode:Z

    .line 760
    return-void
.end method

.method public setSearchText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 811
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mSearchText:Ljava/lang/String;

    .line 812
    return-void
.end method

.method public setStartIdleState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 932
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsStartIdle:Z

    .line 933
    return-void
.end method

.method public setUpButtonVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 798
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsUpButtonVisible:Z

    .line 799
    return-void
.end method

.method public startState(Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 7
    .param p2, "data"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    const-string v4, "StateManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "startState "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const/4 v2, 0x0

    .line 97
    .local v2, "state":Lcom/sec/android/gallery3d/app/ActivityState;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    check-cast v2, Lcom/sec/android/gallery3d/app/ActivityState;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    .restart local v2    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v3

    .line 103
    .local v3, "top":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    sget-object v5, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;->Incoming:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    invoke-virtual {v3, v4, p1, v5}, Lcom/sec/android/gallery3d/app/ActivityState;->transitionOnNextPause(Ljava/lang/Class;Ljava/lang/Class;Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;)V

    .line 106
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/ActivityState;->pause()V

    .line 110
    .end local v3    # "top":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_0
    const-string v4, "Gallery"

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/util/UsageStatistics;->onContentViewChanged(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v4, p2}, Lcom/sec/android/gallery3d/app/ActivityState;->initialize(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/os/Bundle;)V

    .line 115
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    new-instance v5, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    invoke-direct {v5, p2, v2}, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;-><init>(Landroid/os/Bundle;Lcom/sec/android/gallery3d/app/ActivityState;)V

    invoke-virtual {v4, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    const/4 v4, 0x0

    invoke-virtual {v2, p2, v4}, Lcom/sec/android/gallery3d/app/ActivityState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 117
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/ActivityState;->resume()V

    .line 120
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 121
    .local v0, "activity":Landroid/app/Activity;
    new-instance v4, Lcom/sec/android/gallery3d/app/StateManager$1;

    invoke-direct {v4, p0, v0}, Lcom/sec/android/gallery3d/app/StateManager$1;-><init>(Lcom/sec/android/gallery3d/app/StateManager;Landroid/app/Activity;)V

    invoke-virtual {v0, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 128
    return-void

    .line 98
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v2    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    :catch_0
    move-exception v1

    .line 99
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4
.end method

.method public startStateForResult(Ljava/lang/Class;ILandroid/os/Bundle;)V
    .locals 7
    .param p2, "requestCode"    # I
    .param p3, "data"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;I",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    const-string v4, "StateManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "startStateForResult "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const/4 v3, 0x0

    .line 135
    .local v3, "state":Lcom/sec/android/gallery3d/app/ActivityState;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/sec/android/gallery3d/app/ActivityState;

    move-object v3, v0

    .line 137
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->resetSortByFilterType(Ljava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3, v4, p3}, Lcom/sec/android/gallery3d/app/ActivityState;->initialize(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/os/Bundle;)V

    .line 143
    new-instance v4, Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;-><init>()V

    iput-object v4, v3, Lcom/sec/android/gallery3d/app/ActivityState;->mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    .line 144
    iget-object v4, v3, Lcom/sec/android/gallery3d/app/ActivityState;->mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    iput p2, v4, Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;->requestCode:I

    .line 146
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 148
    .local v1, "as":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    sget-object v5, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;->Incoming:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    invoke-virtual {v1, v4, p1, v5}, Lcom/sec/android/gallery3d/app/ActivityState;->transitionOnNextPause(Ljava/lang/Class;Ljava/lang/Class;Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;)V

    .line 150
    iget-object v4, v3, Lcom/sec/android/gallery3d/app/ActivityState;->mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    iput-object v4, v1, Lcom/sec/android/gallery3d/app/ActivityState;->mReceivedResults:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    .line 151
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/ActivityState;->onPause()V

    .line 155
    .end local v1    # "as":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_0
    :goto_0
    const-string v4, "Gallery"

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/util/UsageStatistics;->onContentViewChanged(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    new-instance v5, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    invoke-direct {v5, p3, v3}, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;-><init>(Landroid/os/Bundle;Lcom/sec/android/gallery3d/app/ActivityState;)V

    invoke-virtual {v4, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    const/4 v4, 0x0

    invoke-virtual {v3, p3, v4}, Lcom/sec/android/gallery3d/app/ActivityState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 159
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/ActivityState;->resume()V

    .line 160
    :cond_1
    return-void

    .line 139
    :catch_0
    move-exception v2

    .line 140
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 153
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    iget-object v4, v3, Lcom/sec/android/gallery3d/app/ActivityState;->mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    iput-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    goto :goto_0
.end method

.method public switchState(Lcom/sec/android/gallery3d/app/ActivityState;Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "oldState"    # Lcom/sec/android/gallery3d/app/ActivityState;
    .param p3, "data"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 341
    .local p2, "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    const-string v2, "StateManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "switchState "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    if-eq p1, v2, :cond_0

    .line 343
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The stateview to be finished is not at the top of the stack: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 348
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 349
    const-string v2, "app-bridge"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 351
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    sget-object v3, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;->Incoming:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    invoke-virtual {p1, v2, p2, v3}, Lcom/sec/android/gallery3d/app/ActivityState;->transitionOnNextPause(Ljava/lang/Class;Ljava/lang/Class;Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;)V

    .line 354
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onPause()V

    .line 355
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onDestroy()V

    .line 358
    const/4 v1, 0x0

    .line 360
    .local v1, "state":Lcom/sec/android/gallery3d/app/ActivityState;
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    check-cast v1, Lcom/sec/android/gallery3d/app/ActivityState;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    .restart local v1    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v2, p3}, Lcom/sec/android/gallery3d/app/ActivityState;->initialize(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/os/Bundle;)V

    .line 365
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    new-instance v3, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    invoke-direct {v3, p3, v1}, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;-><init>(Landroid/os/Bundle;Lcom/sec/android/gallery3d/app/ActivityState;)V

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    const/4 v2, 0x0

    invoke-virtual {v1, p3, v2}, Lcom/sec/android/gallery3d/app/ActivityState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 367
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/ActivityState;->resume()V

    .line 368
    :cond_3
    const-string v2, "Gallery"

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/UsageStatistics;->onContentViewChanged(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    return-void

    .line 361
    .end local v1    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    :catch_0
    move-exception v0

    .line 362
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public switchState(Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 7
    .param p2, "bundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 538
    .local p1, "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    const-string v4, "StateManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "switchState to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    iget-object v2, v4, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;->activityState:Lcom/sec/android/gallery3d/app/ActivityState;

    .line 544
    .local v2, "oldState":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    if-eqz v4, :cond_0

    .line 545
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/ActivityState;->onPause()V

    .line 546
    :cond_0
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/ActivityState;->onDestroy()V
    :try_end_0
    .catch Ljava/util/EmptyStackException; {:try_start_0 .. :try_end_0} :catch_0

    .line 554
    const/4 v3, 0x0

    .line 556
    .local v3, "state":Lcom/sec/android/gallery3d/app/ActivityState;
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/sec/android/gallery3d/app/ActivityState;

    move-object v3, v0

    .line 557
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->resetSortByFilterType(Ljava/lang/Class;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 561
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3, v4, p2}, Lcom/sec/android/gallery3d/app/ActivityState;->initialize(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/os/Bundle;)V

    .line 562
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    new-instance v5, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;

    invoke-direct {v5, p2, v3}, Lcom/sec/android/gallery3d/app/StateManager$StateEntry;-><init>(Landroid/os/Bundle;Lcom/sec/android/gallery3d/app/ActivityState;)V

    invoke-virtual {v4, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    const/4 v4, 0x0

    invoke-virtual {v3, p2, v4}, Lcom/sec/android/gallery3d/app/ActivityState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 564
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mIsResumed:Z

    if-eqz v4, :cond_1

    .line 565
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/ActivityState;->resume()V

    .line 567
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 568
    .end local v2    # "oldState":Lcom/sec/android/gallery3d/app/ActivityState;
    .end local v3    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    :goto_0
    return-void

    .line 547
    :catch_0
    move-exception v1

    .line 548
    .local v1, "e":Ljava/util/EmptyStackException;
    const-string v4, "StateManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "activity.isFinishing : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/StateManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isFinishing()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    invoke-virtual {v1}, Ljava/util/EmptyStackException;->printStackTrace()V

    goto :goto_0

    .line 558
    .end local v1    # "e":Ljava/util/EmptyStackException;
    .restart local v2    # "oldState":Lcom/sec/android/gallery3d/app/ActivityState;
    .restart local v3    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    :catch_1
    move-exception v1

    .line 559
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4
.end method

.method public windowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 516
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/StateManager;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 517
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onWindowFocusChanged(Z)V

    .line 519
    :cond_0
    return-void
.end method
