.class public final Lcom/sec/android/gallery3d/remote/picasa/PicasaSyncAdapter$AccountChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PicasaSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/picasa/PicasaSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AccountChangeReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 94
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 95
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 96
    const-string v5, "operation"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 97
    .local v3, "operation":Ljava/lang/String;
    if-eqz v3, :cond_0

    const-string v5, "remove"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 98
    const-string v5, "account"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 99
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    iget-object v5, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v6, "com.google"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 100
    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v6, "@"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 101
    .local v2, "name":[Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->getContentProvider(Landroid/content/Context;)Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;

    move-result-object v4

    .line 102
    .local v4, "provider":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;
    if-eqz v4, :cond_0

    .line 103
    const/4 v5, 0x0

    aget-object v5, v2, v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->deleteAccount(Ljava/lang/String;)V

    .line 108
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v2    # "name":[Ljava/lang/String;
    .end local v3    # "operation":Ljava/lang/String;
    .end local v4    # "provider":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;
    :cond_0
    return-void
.end method
