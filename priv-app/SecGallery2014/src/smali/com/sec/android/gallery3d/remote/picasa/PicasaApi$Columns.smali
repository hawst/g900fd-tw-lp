.class public interface abstract Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$Columns;
.super Ljava/lang/Object;
.source "PicasaApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Columns"
.end annotation


# static fields
.field public static final DATE_EDITED:Ljava/lang/String; = "date_edited"

.field public static final DATE_PUBLISHED:Ljava/lang/String; = "date_published"

.field public static final DATE_UPDATED:Ljava/lang/String; = "date_updated"

.field public static final EDIT_URI:Ljava/lang/String; = "edit_uri"

.field public static final HTML_PAGE_URL:Ljava/lang/String; = "html_page_url"

.field public static final SUMMARY:Ljava/lang/String; = "summary"

.field public static final SYNC_ACCOUNT:Ljava/lang/String; = "sync_account"

.field public static final THUMBNAIL_URL:Ljava/lang/String; = "thumbnail_url"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final _ID:Ljava/lang/String; = "_id"
