.class Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$1;
.super Landroid/database/ContentObserver;
.source "MagazineWidgetService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .prologue
    .line 90
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;

    # invokes: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->updatePersonImage()V
    invoke-static {v1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$000(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 95
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "can\'t update person images: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
