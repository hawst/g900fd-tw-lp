.class public Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlAlphaAnimation.java"


# instance fields
.field private mCount:I

.field private mCurrentA:F

.field private mFromA:F

.field private mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mObjectSet:[Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mToA:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 16
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mFromA:F

    .line 17
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mToA:F

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mCount:I

    .line 19
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlObject;FF)V
    .locals 1
    .param p1, "obj"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "fromA"    # F
    .param p3, "toA"    # F

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 23
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mFromA:F

    .line 24
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mToA:F

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mCount:I

    .line 26
    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 43
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mFromA:F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mToA:F

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mFromA:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mCurrentA:F

    .line 45
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v1, :cond_1

    .line 46
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mCurrentA:F

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    .line 52
    :cond_0
    return-void

    .line 47
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mObjectSet:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v1, :cond_0

    .line 48
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mCount:I

    if-ge v0, v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mObjectSet:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mCurrentA:F

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setParam(FF)V
    .locals 1
    .param p1, "fromA"    # F
    .param p2, "toA"    # F

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mFromA:F

    .line 30
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mToA:F

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mCount:I

    .line 32
    return-void
.end method

.method public setParam([Lcom/sec/android/gallery3d/glcore/GlObject;IFF)V
    .locals 0
    .param p1, "obj"    # [Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "count"    # I
    .param p3, "fromA"    # F
    .param p4, "toA"    # F

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mObjectSet:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 36
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mFromA:F

    .line 37
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mToA:F

    .line 38
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->mCount:I

    .line 39
    return-void
.end method
