.class public Lcom/sec/android/gallery3d/data/AllInOneSource;
.super Lcom/sec/android/gallery3d/data/MediaSource;
.source "AllInOneSource.java"


# static fields
.field private static final ALLINONE_BY_TYPE:I


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 12
    const-string v0, "allinone"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    .line 13
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/AllInOneSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 14
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 18
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/allinone/*"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 19
    return-void
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 24
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/AllInOneSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 28
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/AllInOneSource;->getPrefix()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 30
    .local v2, "subPath":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/AllInOneSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 31
    .local v1, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 32
    .local v0, "baseSet":Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v3, Lcom/sec/android/gallery3d/data/AllInOneAlbum;

    invoke-direct {v3, v0, p1}, Lcom/sec/android/gallery3d/data/AllInOneAlbum;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/Path;)V

    .end local v0    # "baseSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v1    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v2    # "subPath":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 26
    :pswitch_0
    new-instance v3, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/AllInOneSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/AllInOneSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v5

    invoke-direct {v3, v4, p1, v5}, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;I)V

    goto :goto_0

    .line 24
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
