.class public Lcom/sec/android/gallery3d/homesync/HomeSyncUtil;
.super Ljava/lang/Object;
.source "HomeSyncUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fadePointer(Landroid/content/Context;Z)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isFade"    # Z

    .prologue
    .line 12
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v4, :cond_0

    .line 25
    :goto_0
    return-void

    .line 15
    :cond_0
    const-string v0, "com.sec.android.spc.eventcontrol.ACTION_FADE_POINTER"

    .line 16
    .local v0, "ACTION_FADE_POINTER":Ljava/lang/String;
    const-string v1, "fade"

    .line 18
    .local v1, "FADE":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 19
    .local v3, "intent":Landroid/content/Intent;
    const-string v4, "com.sec.android.spc.eventcontrol.ACTION_FADE_POINTER"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 20
    new-instance v2, Landroid/content/ComponentName;

    const-string v4, "com.sec.android.spc.eventcontrol"

    const-string v5, "com.sec.android.spc.eventcontrol.SpcEventControlReceiver"

    invoke-direct {v2, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .local v2, "component":Landroid/content/ComponentName;
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 23
    const-string v4, "fade"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 24
    invoke-virtual {p0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method
