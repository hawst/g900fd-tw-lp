.class Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;
.super Ljava/lang/Object;
.source "MtpDeviceIndex.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IndexRunnable"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable$IndexingException;
    }
.end annotation


# instance fields
.field private mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

.field private mBucketsTemp:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/gallery3d/ingest/SimpleDate;",
            "Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;",
            ">;"
        }
    .end annotation
.end field

.field private mDateInstance:Lcom/sec/android/gallery3d/ingest/SimpleDate;

.field private mDevice:Landroid/mtp/MtpDevice;

.field private mMtpObjects:[Landroid/mtp/MtpObjectInfo;

.field private mNumObjects:I

.field private mUnifiedLookupIndex:[I

.field final synthetic this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;Landroid/mtp/MtpDevice;)V
    .locals 1
    .param p2, "device"    # Landroid/mtp/MtpDevice;

    .prologue
    .line 437
    iput-object p1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 433
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mNumObjects:I

    .line 567
    new-instance v0, Lcom/sec/android/gallery3d/ingest/SimpleDate;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ingest/SimpleDate;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDateInstance:Lcom/sec/android/gallery3d/ingest/SimpleDate;

    .line 438
    iput-object p2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDevice:Landroid/mtp/MtpDevice;

    .line 439
    return-void
.end method

.method private addObject(Landroid/mtp/MtpObjectInfo;)V
    .locals 4
    .param p1, "objectInfo"    # Landroid/mtp/MtpObjectInfo;

    .prologue
    .line 570
    iget v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mNumObjects:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mNumObjects:I

    .line 571
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDateInstance:Lcom/sec/android/gallery3d/ingest/SimpleDate;

    invoke-virtual {p1}, Landroid/mtp/MtpObjectInfo;->getDateCreated()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/ingest/SimpleDate;->setTimestamp(J)V

    .line 572
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mBucketsTemp:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDateInstance:Lcom/sec/android/gallery3d/ingest/SimpleDate;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    .line 573
    .local v0, "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    if-nez v0, :cond_1

    .line 574
    new-instance v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    .end local v0    # "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDateInstance:Lcom/sec/android/gallery3d/ingest/SimpleDate;

    invoke-direct {v0, v1, v2, p1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;-><init>(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;Lcom/sec/android/gallery3d/ingest/SimpleDate;Landroid/mtp/MtpObjectInfo;)V

    .line 575
    .restart local v0    # "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mBucketsTemp:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDateInstance:Lcom/sec/android/gallery3d/ingest/SimpleDate;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    new-instance v1, Lcom/sec/android/gallery3d/ingest/SimpleDate;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/ingest/SimpleDate;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDateInstance:Lcom/sec/android/gallery3d/ingest/SimpleDate;

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 580
    :cond_1
    iget-object v1, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->tempElementsList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 582
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    # getter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$500(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 583
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    # getter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$500(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mNumObjects:I

    invoke-interface {v1, p1, v2}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;->onObjectIndexed(Landroid/mtp/MtpObjectInfo;I)V

    goto :goto_0
.end method

.method private buildLookupIndex()V
    .locals 9

    .prologue
    .line 461
    iget-object v7, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    array-length v6, v7

    .line 462
    .local v6, "numBuckets":I
    iget v7, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mNumObjects:I

    add-int/2addr v7, v6

    new-array v7, v7, [I

    iput-object v7, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mUnifiedLookupIndex:[I

    .line 463
    const/4 v2, 0x0

    .line 466
    .local v2, "currentUnifiedIndexEntry":I
    iget v7, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mNumObjects:I

    new-array v7, v7, [Landroid/mtp/MtpObjectInfo;

    iput-object v7, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    .line 467
    const/4 v1, 0x0

    .line 468
    .local v1, "currentItemsEntry":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v6, :cond_1

    .line 469
    iget-object v7, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    aget-object v0, v7, v3

    .line 470
    .local v0, "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    iget-object v7, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->tempElementsList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/2addr v7, v2

    add-int/lit8 v5, v7, 0x1

    .line 471
    .local v5, "nextUnifiedEntry":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mUnifiedLookupIndex:[I

    invoke-static {v7, v2, v5, v3}, Ljava/util/Arrays;->fill([IIII)V

    .line 472
    iput v2, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedStartIndex:I

    .line 473
    add-int/lit8 v7, v5, -0x1

    iput v7, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->unifiedEndIndex:I

    .line 474
    move v2, v5

    .line 476
    iput v1, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->itemsStartIndex:I

    .line 477
    iget-object v7, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->tempElementsList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    iput v7, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->numItems:I

    .line 478
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    iget v7, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->numItems:I

    if-ge v4, v7, :cond_0

    .line 479
    iget-object v8, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    iget-object v7, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->tempElementsList:Ljava/util/List;

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/mtp/MtpObjectInfo;

    aput-object v7, v8, v1

    .line 480
    add-int/lit8 v1, v1, 0x1

    .line 478
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 482
    :cond_0
    const/4 v7, 0x0

    iput-object v7, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->tempElementsList:Ljava/util/List;

    .line 468
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 484
    .end local v0    # "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    .end local v4    # "j":I
    .end local v5    # "nextUnifiedEntry":I
    :cond_1
    return-void
.end method

.method private copyResults()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 487
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mUnifiedLookupIndex:[I

    # setter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mUnifiedLookupIndex:[I
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$102(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;[I)[I

    .line 488
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    # setter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$202(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;[Landroid/mtp/MtpObjectInfo;)[Landroid/mtp/MtpObjectInfo;

    .line 489
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    # setter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$302(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;)[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    .line 490
    iput-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mUnifiedLookupIndex:[I

    .line 491
    iput-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mMtpObjects:[Landroid/mtp/MtpObjectInfo;

    .line 492
    iput-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    .line 493
    return-void
.end method

.method private indexDevice()V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable$IndexingException;
        }
    .end annotation

    .prologue
    .line 510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    move-object/from16 v16, v0

    monitor-enter v16

    .line 511
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    sget-object v17, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Started:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    move-object/from16 v0, v17

    # setter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;
    invoke-static {v15, v0}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$602(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    .line 512
    monitor-exit v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 513
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mBucketsTemp:Ljava/util/Map;

    .line 514
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDevice:Landroid/mtp/MtpDevice;

    invoke-virtual {v15}, Landroid/mtp/MtpDevice;->getStorageIds()[I

    move-result-object v2

    .local v2, "arr$":[I
    array-length v8, v2

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    move v7, v6

    .end local v2    # "arr$":[I
    .end local v6    # "i$":I
    .end local v8    # "len$":I
    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_8

    aget v13, v2, v7

    .line 515
    .local v13, "storageId":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDevice:Landroid/mtp/MtpDevice;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->getDevice()Landroid/mtp/MtpDevice;

    move-result-object v16

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_0

    new-instance v15, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable$IndexingException;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable$IndexingException;-><init>(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$1;)V

    throw v15

    .line 512
    .end local v7    # "i$":I
    .end local v13    # "storageId":I
    :catchall_0
    move-exception v15

    :try_start_1
    monitor-exit v16
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v15

    .line 516
    .restart local v7    # "i$":I
    .restart local v13    # "storageId":I
    :cond_0
    new-instance v12, Ljava/util/Stack;

    invoke-direct {v12}, Ljava/util/Stack;-><init>()V

    .line 517
    .local v12, "pendingDirectories":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    const/4 v15, -0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 518
    .end local v7    # "i$":I
    :cond_1
    invoke-virtual {v12}, Ljava/util/Stack;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_7

    .line 519
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDevice:Landroid/mtp/MtpDevice;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->getDevice()Landroid/mtp/MtpDevice;

    move-result-object v16

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_2

    new-instance v15, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable$IndexingException;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable$IndexingException;-><init>(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$1;)V

    throw v15

    .line 520
    :cond_2
    invoke-virtual {v12}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 521
    .local v4, "dirHandle":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDevice:Landroid/mtp/MtpDevice;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v15, v13, v0, v4}, Landroid/mtp/MtpDevice;->getObjectHandles(III)[I

    move-result-object v3

    .local v3, "arr$":[I
    array-length v9, v3

    .local v9, "len$":I
    const/4 v6, 0x0

    .restart local v6    # "i$":I
    :goto_1
    if-ge v6, v9, :cond_1

    aget v10, v3, v6

    .line 522
    .local v10, "objectHandle":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDevice:Landroid/mtp/MtpDevice;

    invoke-virtual {v15, v10}, Landroid/mtp/MtpDevice;->getObjectInfo(I)Landroid/mtp/MtpObjectInfo;

    move-result-object v11

    .line 523
    .local v11, "objectInfo":Landroid/mtp/MtpObjectInfo;
    if-nez v11, :cond_3

    new-instance v15, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable$IndexingException;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable$IndexingException;-><init>(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$1;)V

    throw v15

    .line 524
    :cond_3
    invoke-virtual {v11}, Landroid/mtp/MtpObjectInfo;->getFormat()I

    move-result v5

    .line 525
    .local v5, "format":I
    const/16 v15, 0x3001

    if-ne v5, v15, :cond_5

    .line 526
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 521
    :cond_4
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 527
    :cond_5
    sget-object v15, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_6

    sget-object v15, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_VIDEO_FORMATS:Ljava/util/Set;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 529
    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->addObject(Landroid/mtp/MtpObjectInfo;)V

    goto :goto_2

    .line 514
    .end local v3    # "arr$":[I
    .end local v4    # "dirHandle":I
    .end local v5    # "format":I
    .end local v6    # "i$":I
    .end local v9    # "len$":I
    .end local v10    # "objectHandle":I
    .end local v11    # "objectInfo":Landroid/mtp/MtpObjectInfo;
    :cond_7
    add-int/lit8 v6, v7, 0x1

    .restart local v6    # "i$":I
    move v7, v6

    .end local v6    # "i$":I
    .restart local v7    # "i$":I
    goto/16 :goto_0

    .line 534
    .end local v12    # "pendingDirectories":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    .end local v13    # "storageId":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mBucketsTemp:Ljava/util/Map;

    invoke-interface {v15}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v14

    .line 535
    .local v14, "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;>;"
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mBucketsTemp:Ljava/util/Map;

    .line 536
    invoke-interface {v14}, Ljava/util/Collection;->size()I

    move-result v15

    new-array v15, v15, [Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    invoke-interface {v14, v15}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    .line 537
    const/4 v14, 0x0

    .line 538
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    move-object/from16 v16, v0

    monitor-enter v16

    .line 539
    :try_start_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    sget-object v17, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Sorting:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    move-object/from16 v0, v17

    # setter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;
    invoke-static {v15, v0}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$602(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    .line 540
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    # getter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;
    invoke-static {v15}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$500(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    move-result-object v15

    if-eqz v15, :cond_9

    .line 541
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    # getter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;
    invoke-static {v15}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$500(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;->onSorting()V

    .line 543
    :cond_9
    monitor-exit v16
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 544
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->sortAll()V

    .line 545
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->buildLookupIndex()V

    .line 546
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    move-object/from16 v16, v0

    monitor-enter v16

    .line 547
    :try_start_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mDevice:Landroid/mtp/MtpDevice;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->getDevice()Landroid/mtp/MtpDevice;

    move-result-object v17

    move-object/from16 v0, v17

    if-eq v15, v0, :cond_a

    new-instance v15, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable$IndexingException;

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v15, v0, v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable$IndexingException;-><init>(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$1;)V

    throw v15

    .line 564
    :catchall_1
    move-exception v15

    monitor-exit v16
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v15

    .line 543
    :catchall_2
    move-exception v15

    :try_start_4
    monitor-exit v16
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v15

    .line 548
    :cond_a
    :try_start_5
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->copyResults()V

    .line 558
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    # invokes: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->computeReversedBuckets()V
    invoke-static {v15}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$800(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)V

    .line 560
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    sget-object v17, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;->Finished:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    move-object/from16 v0, v17

    # setter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgress:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;
    invoke-static {v15, v0}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$602(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$Progress;

    .line 561
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    # getter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;
    invoke-static {v15}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$500(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    move-result-object v15

    if-eqz v15, :cond_b

    .line 562
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    # getter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;
    invoke-static {v15}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$500(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;->onIndexFinish()V

    .line 564
    :cond_b
    monitor-exit v16
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 565
    return-void
.end method

.method private sortAll()V
    .locals 5

    .prologue
    .line 588
    iget-object v4, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    invoke-static {v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 589
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->mBuckets:[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 590
    .local v1, "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    # getter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->sMtpObjectComparator:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$MtpObjectTimestampComparator;
    invoke-static {}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$900()Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$MtpObjectTimestampComparator;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;->sortElements(Ljava/util/Comparator;)V

    .line 589
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 592
    .end local v1    # "bucket":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$DateBucket;
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 498
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->indexDevice()V
    :try_end_0
    .catch Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable$IndexingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 507
    :goto_0
    return-void

    .line 499
    :catch_0
    move-exception v0

    .line 500
    .local v0, "e":Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable$IndexingException;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    monitor-enter v2

    .line 501
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    # invokes: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->resetState()V
    invoke-static {v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$400(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)V

    .line 502
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    # getter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$500(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 503
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$IndexRunnable;->this$0:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;

    # getter for: Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->mProgressListener:Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->access$500(Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;)Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex$ProgressListener;->onIndexFinish()V

    .line 505
    :cond_0
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
