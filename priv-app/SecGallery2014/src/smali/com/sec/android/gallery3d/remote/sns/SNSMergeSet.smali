.class public Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "SNSMergeSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# static fields
.field public static SNS_MERGESET_BUCKETID:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SNSMergeSet"


# instance fields
.field private final mName:Ljava/lang/String;

.field private mRootSet:Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, "/sns/mergedall"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sput v0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->SNS_MERGESET_BUCKETID:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 39
    new-instance v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->getBucketId()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;

    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 41
    const-string v0, "Facebook"

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->mName:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public static find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 45
    new-instance v0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    return-object v0
.end method


# virtual methods
.method public getBucketId()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    if-nez v0, :cond_0

    .line 97
    const/4 v0, -0x1

    .line 98
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItemCount()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;

    return-object v0
.end method

.method public getSubMediaSetCount()I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 55
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->getSubMediaSetCount(Z)I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 65
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getSupportedOperations()J

    move-result-wide v0

    return-wide v0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->notifyContentChanged()V

    .line 104
    return-void
.end method

.method public reload()J
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->reload()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->mDataVersion:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 71
    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->mDataVersion:J

    .line 73
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->mDataVersion:J

    return-wide v0
.end method
