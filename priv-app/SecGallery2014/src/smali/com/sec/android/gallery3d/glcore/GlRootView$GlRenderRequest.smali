.class public Lcom/sec/android/gallery3d/glcore/GlRootView$GlRenderRequest;
.super Ljava/lang/Object;
.source "GlRootView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlRootView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GlRenderRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/sec/android/gallery3d/glcore/GlObject;",
        ">;"
    }
.end annotation


# instance fields
.field mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 0
    .param p2, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 2670
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlRenderRequest;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2671
    iput-object p2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlRenderRequest;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 2672
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 1
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 2676
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlRenderRequest;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->procAsyncRender()V

    .line 2677
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlRenderRequest;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    return-object v0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 2667
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView$GlRenderRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v0

    return-object v0
.end method
