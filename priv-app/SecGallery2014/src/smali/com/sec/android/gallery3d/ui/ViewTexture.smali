.class public Lcom/sec/android/gallery3d/ui/ViewTexture;
.super Lcom/sec/android/gallery3d/glrenderer/CanvasTexture;
.source "ViewTexture.java"


# instance fields
.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;II)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 12
    invoke-direct {p0, p2, p3}, Lcom/sec/android/gallery3d/glrenderer/CanvasTexture;-><init>(II)V

    .line 13
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mView:Landroid/view/View;

    .line 14
    return-void
.end method


# virtual methods
.method public forceLayout()V
    .locals 6

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 34
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mWidth:I

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 35
    .local v1, "widthSpec":I
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mHeight:I

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 36
    .local v0, "heightSpec":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mView:Landroid/view/View;

    invoke-virtual {v2, v1, v0}, Landroid/view/View;->measure(II)V

    .line 37
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mView:Landroid/view/View;

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mWidth:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mHeight:I

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 38
    return-void
.end method

.method public getContentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mView:Landroid/view/View;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "backing"    # Landroid/graphics/Bitmap;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 19
    return-void
.end method

.method public resetSize(II)V
    .locals 6
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 26
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/glrenderer/CanvasTexture;->setSize(II)V

    .line 27
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mWidth:I

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 28
    .local v1, "widthSpec":I
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mHeight:I

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 29
    .local v0, "heightSpec":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mView:Landroid/view/View;

    invoke-virtual {v2, v1, v0}, Landroid/view/View;->measure(II)V

    .line 30
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mView:Landroid/view/View;

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mWidth:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/ViewTexture;->mHeight:I

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 31
    return-void
.end method
