.class Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
.super Ljava/lang/Object;
.source "FaceIndicatorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FaceInfo"
.end annotation


# instance fields
.field public mChannel:Ljava/lang/String;

.field public mChannelIndex:I

.field public mChannelType:I

.field public mFaceRectScreen:Landroid/graphics/Rect;

.field public mFaceState:I

.field public mName:Ljava/lang/String;

.field public mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

.field public mPopupFocusIndex:I

.field public mPopupItemsRect:[Landroid/graphics/Rect;

.field public mPopupTagRect:Landroid/graphics/Rect;

.field public mPopupVis:Z

.field public mTagPosition:I

.field public mUnnamedCandidates:[I

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Landroid/graphics/Rect;Z)V
    .locals 2
    .param p2, "rt"    # Landroid/graphics/Rect;
    .param p3, "vis"    # Z

    .prologue
    const/4 v1, 0x0

    .line 148
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupFocusIndex:I

    .line 142
    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    .line 146
    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mChannelType:I

    .line 149
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    .line 150
    iput-boolean p3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    .line 152
    return-void
.end method


# virtual methods
.method public setPopupVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 154
    if-nez p1, :cond_0

    .line 155
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupFocusIndex:I

    .line 157
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    .line 158
    return-void
.end method
