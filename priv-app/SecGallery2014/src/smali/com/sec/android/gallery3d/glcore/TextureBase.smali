.class public Lcom/sec/android/gallery3d/glcore/TextureBase;
.super Ljava/lang/Object;
.source "TextureBase.java"


# static fields
.field protected static final STATE_ERROR:I = -0x1

.field protected static final STATE_LOADED:I = 0x1

.field protected static final STATE_UNLOADED:I = 0x0

.field protected static final UNSPECIFIED:I = -0x1


# instance fields
.field protected mBitmap:Landroid/graphics/Bitmap;

.field public mContentValid:Z

.field protected mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field public mHeight:I

.field protected mState:I

.field protected mTextureHeight:I

.field public mTextureId:I

.field protected mTextureWidth:I

.field protected mUploadFull:Z

.field public mWidth:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 2
    .param p1, "root"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mWidth:I

    .line 31
    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mHeight:I

    .line 32
    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureWidth:I

    .line 33
    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureHeight:I

    .line 34
    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureId:I

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mState:I

    .line 37
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mUploadFull:Z

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mContentValid:Z

    .line 42
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 43
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLUtilInstance()Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/TUtils;->getAvailable()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureId:I

    .line 44
    return-void
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/TextureBase;->onGetBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    .line 60
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    .line 72
    :goto_0
    return-object v0

    .line 62
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mWidth:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/glcore/TextureBase;->setSize(II)V

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 64
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mWidth:I

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mHeight:I

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 66
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cannot change size: this = %s, orig = %sx%s, new = %sx%s"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mWidth:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mHeight:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mWidth:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/TextureBase;->getBitmap()Landroid/graphics/Bitmap;

    .line 82
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mHeight:I

    return v0
.end method

.method public getTextureHeight()I
    .locals 2

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mWidth:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/TextureBase;->getBitmap()Landroid/graphics/Bitmap;

    .line 94
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureHeight:I

    return v0
.end method

.method public getTextureWidth()I
    .locals 2

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mWidth:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/TextureBase;->getBitmap()Landroid/graphics/Bitmap;

    .line 88
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureWidth:I

    return v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mWidth:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/TextureBase;->getBitmap()Landroid/graphics/Bitmap;

    .line 77
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mWidth:I

    return v0
.end method

.method public invalidateContent()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 98
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mContentValid:Z

    .line 99
    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mState:I

    .line 100
    return-void
.end method

.method protected onClearBitmap()V
    .locals 0

    .prologue
    .line 179
    return-void
.end method

.method protected onFreeBitmap()V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method protected onGetBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    return-object v0
.end method

.method public prepare(Z)V
    .locals 2
    .param p1, "bForce"    # Z

    .prologue
    const/4 v1, 0x1

    .line 103
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mState:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mContentValid:Z

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    .line 105
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/TextureBase;->onGetBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    .line 108
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/TextureBase;->onClearBitmap()V

    .line 109
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mContentValid:Z

    .line 110
    return-void
.end method

.method public recycle()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 189
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureId:I

    if-ne v0, v2, :cond_0

    .line 198
    :goto_0
    return-void

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLUtilInstance()Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/TUtils;->freeId(I)V

    .line 193
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureId:I

    .line 194
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mWidth:I

    .line 195
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mHeight:I

    .line 196
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mState:I

    goto :goto_0
.end method

.method public requestFullUpload()V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mUploadFull:Z

    .line 186
    return-void
.end method

.method protected setSize(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mWidth:I

    .line 48
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mHeight:I

    .line 50
    :try_start_0
    invoke-static {p1}, Lcom/sec/android/gallery3d/glcore/TUtils;->nextPowerOf2(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureWidth:I

    .line 51
    invoke-static {p1}, Lcom/sec/android/gallery3d/glcore/TUtils;->nextPowerOf2(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureHeight:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :goto_0
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureHeight:I

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureWidth:I

    goto :goto_0
.end method

.method public setTexture(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 9
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    const/4 v8, 0x1

    const v7, 0x47012f00    # 33071.0f

    const v3, 0x46180400    # 9729.0f

    const/4 v1, 0x0

    const/16 v0, 0xde1

    .line 112
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    .line 113
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mState:I

    if-eq v2, v8, :cond_2

    if-eqz v4, :cond_2

    .line 114
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureId:I

    invoke-interface {p1, v0, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 115
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mUploadFull:Z

    if-eqz v2, :cond_3

    .line 116
    const/16 v2, 0x2802

    invoke-interface {p1, v0, v2, v7}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 117
    const/16 v2, 0x2803

    invoke-interface {p1, v0, v2, v7}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 118
    const/16 v2, 0x2801

    invoke-interface {p1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 119
    const/16 v2, 0x2800

    invoke-interface {p1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 121
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 122
    invoke-static {v0, v1, v4, v1}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 124
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mUploadFull:Z

    .line 130
    :cond_1
    :goto_0
    iput v8, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mState:I

    .line 132
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/TextureBase;->onFreeBitmap()V

    .line 133
    return-void

    .line 125
    :cond_3
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 126
    invoke-static {v4}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v5

    .line 127
    .local v5, "format":I
    invoke-static {v4}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v6

    .local v6, "type":I
    move v2, v1

    move v3, v1

    .line 128
    invoke-static/range {v0 .. v6}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;II)V

    goto :goto_0
.end method

.method public setTextureMixed(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 10
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    const v9, 0x46180400    # 9729.0f

    const v8, 0x44408000    # 770.0f

    const/4 v1, 0x0

    const/16 v7, 0x2300

    const/16 v0, 0xde1

    .line 136
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mBitmap:Landroid/graphics/Bitmap;

    .line 137
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mState:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 138
    const v2, 0x84c1

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 139
    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 140
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mTextureId:I

    invoke-interface {p1, v0, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 141
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mUploadFull:Z

    if-eqz v2, :cond_1

    .line 143
    const/16 v2, 0x2200

    const v3, 0x47057000    # 34160.0f

    invoke-interface {p1, v7, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 144
    const v2, 0x8571

    const v3, 0x47057500    # 34165.0f

    invoke-interface {p1, v7, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 145
    const v2, 0x8572

    const v3, 0x47057500    # 34165.0f

    invoke-interface {p1, v7, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 148
    const v2, 0x8582

    const v3, 0x47057600    # 34166.0f

    invoke-interface {p1, v7, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 149
    const v2, 0x8592

    invoke-interface {p1, v7, v2, v8}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 152
    const v2, 0x858a

    const v3, 0x47057600    # 34166.0f

    invoke-interface {p1, v7, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 153
    const v2, 0x859a

    invoke-interface {p1, v7, v2, v8}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 155
    const/16 v2, 0x2802

    const v3, 0x47012f00    # 33071.0f

    invoke-interface {p1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 156
    const/16 v2, 0x2803

    const v3, 0x47012f00    # 33071.0f

    invoke-interface {p1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 157
    const/16 v2, 0x2801

    invoke-interface {p1, v0, v2, v9}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 158
    const/16 v2, 0x2800

    invoke-interface {p1, v0, v2, v9}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 160
    invoke-static {v0, v1, v4, v1}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 161
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mUploadFull:Z

    .line 168
    :goto_0
    const v0, 0x84c0

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 169
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TextureBase;->mState:I

    .line 171
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/TextureBase;->onFreeBitmap()V

    .line 172
    return-void

    .line 163
    :cond_1
    invoke-static {v4}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v5

    .line 164
    .local v5, "format":I
    invoke-static {v4}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v6

    .local v6, "type":I
    move v2, v1

    move v3, v1

    .line 166
    invoke-static/range {v0 .. v6}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;II)V

    goto :goto_0
.end method
