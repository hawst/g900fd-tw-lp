.class public Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;
.super Ljava/lang/Object;
.source "GlHoverGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;,
        Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;,
        Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$SimpleOnGestureListener;,
        Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;
    }
.end annotation


# static fields
.field public static final HOVER_POINT:I = 0x0

.field private static final HOVER_SCROLL_DETECT_AREA:I = 0x3c

.field public static final HOVER_SCROLL_DOWN:I = 0x2

.field public static final HOVER_SCROLL_LEFT:I = 0x3

.field public static final HOVER_SCROLL_RIGHT:I = 0x4

.field public static final HOVER_SCROLL_UP:I = 0x1

.field private static final NSEC:J = 0x64L

.field private static final SCROLL_LIMITATION:I = 0x258

.field private static final SCROLL_MOVE:I = 0x1

.field private static final SCROLL_SPEED:I = 0x14

.field private static final TAG:Ljava/lang/String; = "GlHoverGestureDetector"


# instance fields
.field private final INVALID_TIME:I

.field private ScrollType:[Ljava/lang/String;

.field private hoverEventType:[Ljava/lang/String;

.field private mAreaOfListScroll:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHoverRelease:Z

.field private mIsListScroll:Z

.field private mListScrollListner:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;

.field private mRecongnizedStartTime:I

.field private mScrollDirection:I

.field private mTotalDx:I

.field private mTotalDy:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 156
    invoke-direct {p0, p1, v0, v0}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;Landroid/os/Handler;)V

    .line 157
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;Landroid/os/Handler;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->INVALID_TIME:I

    .line 31
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHoverRelease:Z

    .line 72
    const/16 v0, 0x258

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDx:I

    .line 73
    const/16 v0, 0x258

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDy:I

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mListScrollListner:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;

    .line 170
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;-><init>(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mAreaOfListScroll:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;

    .line 171
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mRecongnizedStartTime:I

    .line 172
    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mScrollDirection:I

    .line 194
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "HOVER_POINT"

    aput-object v1, v0, v3

    const-string v1, "HOVER_SCROLL_UP"

    aput-object v1, v0, v4

    const-string v1, "HOVER_SCROLL_DOWN"

    aput-object v1, v0, v5

    const-string v1, "HOVER_SCROLL_LEFT"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "HOVER_SCROLL_RIGHT"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->ScrollType:[Ljava/lang/String;

    .line 195
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "0"

    aput-object v1, v0, v3

    const-string v1, "1"

    aput-object v1, v0, v4

    const-string v1, "2"

    aput-object v1, v0, v5

    const-string v1, "3"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "4"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "5"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "6"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ACTION_HOVER_MOVE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "8"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "ACTION_HOVER_ENTER "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "ACTION_HOVER_EXIT"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->hoverEventType:[Ljava/lang/String;

    .line 219
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mIsListScroll:Z

    .line 160
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mContext:Landroid/content/Context;

    .line 161
    if-eqz p3, :cond_0

    .line 162
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;

    invoke-direct {v0, p0, p3}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;-><init>(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHandler:Landroid/os/Handler;

    .line 166
    :goto_0
    return-void

    .line 164
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;-><init>(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHandler:Landroid/os/Handler;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mRecongnizedStartTime:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mScrollDirection:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->scrollBy(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mIsListScroll:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getType(II)I
    .locals 10
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/high16 v9, 0x42700000    # 60.0f

    const/high16 v8, 0x3f800000    # 1.0f

    .line 307
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v4

    .line 308
    .local v4, "point":Landroid/graphics/Point;
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v7

    int-to-float v7, v7

    mul-float v1, v7, v8

    .line 309
    .local v1, "height":F
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v7

    int-to-float v7, v7

    mul-float v6, v7, v8

    .line 310
    .local v6, "width":F
    iget v7, v4, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    div-float/2addr v7, v6

    mul-float/2addr v7, v9

    float-to-int v2, v7

    .line 311
    .local v2, "horizontalArea":I
    iget v7, v4, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    div-float/2addr v7, v1

    mul-float/2addr v7, v9

    float-to-int v5, v7

    .line 312
    .local v5, "verticalArea":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mContext:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ActionBar;->getHeight()I

    move-result v0

    .line 314
    .local v0, "actionbarHeight":I
    const/4 v3, 0x0

    .line 316
    .local v3, "hoverType":I
    if-le p2, v0, :cond_1

    add-int v7, v0, v5

    if-ge p2, v7, :cond_1

    .line 317
    const/4 v3, 0x1

    .line 325
    :cond_0
    :goto_0
    return v3

    .line 318
    :cond_1
    iget v7, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v7, v5

    if-le p2, v7, :cond_2

    .line 319
    const/4 v3, 0x2

    goto :goto_0

    .line 320
    :cond_2
    if-ge p1, v2, :cond_3

    .line 321
    const/4 v3, 0x3

    goto :goto_0

    .line 322
    :cond_3
    iget v7, v4, Landroid/graphics/Point;->x:I

    sub-int/2addr v7, v2

    if-le p1, v7, :cond_0

    .line 323
    const/4 v3, 0x4

    goto :goto_0
.end method

.method private isListScroll()Z
    .locals 1

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mIsListScroll:Z

    return v0
.end method

.method private processInfoPreviewEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 276
    const/4 v0, 0x0

    .line 277
    .local v0, "wasConsumed":Z
    const/4 v1, 0x1

    return v1
.end method

.method private processListScrollEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v9, 0x9

    const/4 v7, 0x7

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 225
    const/4 v2, 0x0

    .line 227
    .local v2, "wasConsumed":Z
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHoverRelease:Z

    if-eqz v6, :cond_0

    .line 272
    :goto_0
    return v5

    .line 229
    :cond_0
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mIsListScroll:Z

    if-nez v6, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-ne v6, v7, :cond_1

    .line 230
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->setAction(I)V

    .line 233
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 235
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v3, v6

    .line 236
    .local v3, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v4, v6

    .line 238
    .local v4, "y":I
    if-ne v0, v9, :cond_4

    .line 239
    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 240
    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 241
    :cond_2
    iput-boolean v8, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mIsListScroll:Z

    .line 242
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-int v6, v6

    iput v6, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mRecongnizedStartTime:I

    .line 243
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDx:I

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDy:I

    .line 244
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mListScrollListner:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;

    invoke-interface {v5, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;->onPress(II)Z

    .line 245
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 246
    .local v1, "msg":Landroid/os/Message;
    iput v8, v1, Landroid/os/Message;->what:I

    .line 247
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 249
    const/4 v2, 0x1

    .end local v1    # "msg":Landroid/os/Message;
    :cond_3
    :goto_1
    move v5, v2

    .line 272
    goto :goto_0

    .line 250
    :cond_4
    if-ne v0, v7, :cond_6

    .line 252
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mIsListScroll:Z

    if-eqz v5, :cond_3

    .line 253
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mScrollDirection:I

    if-nez v5, :cond_5

    .line 254
    invoke-direct {p0, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->getType(II)I

    move-result v5

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mScrollDirection:I

    .line 255
    :cond_5
    const/4 v2, 0x1

    goto :goto_1

    .line 257
    :cond_6
    const/16 v6, 0xa

    if-ne v0, v6, :cond_3

    .line 259
    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 260
    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 261
    :cond_7
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mIsListScroll:Z

    .line 262
    const/4 v6, -0x1

    iput v6, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mRecongnizedStartTime:I

    .line 263
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mScrollDirection:I

    .line 264
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDx:I

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDy:I

    .line 265
    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mListScrollListner:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;

    invoke-interface {v6, v5, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;->onRelease(IIII)Z

    .line 266
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private scrollBy(II)V
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHoverRelease:Z

    if-eqz v0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDx:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDx:I

    .line 82
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDy:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDy:I

    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mListScrollListner:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mScrollDirection:I

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;->onMove(III)Z

    .line 86
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDx:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x258

    if-le v0, v1, :cond_0

    .line 87
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDx:I

    if-lez v0, :cond_2

    .line 88
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mListScrollListner:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;->onPrevious()Z

    .line 91
    :goto_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDx:I

    goto :goto_0

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mListScrollListner:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;->onNext()Z

    goto :goto_1
.end method


# virtual methods
.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 198
    const/4 v1, 0x0

    .line 200
    .local v1, "wasConsumed":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .line 201
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 203
    .local v3, "y":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mAreaOfListScroll:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 204
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->processListScrollEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 216
    :goto_0
    return v1

    .line 206
    :cond_0
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mIsListScroll:Z

    if-eqz v4, :cond_1

    .line 207
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 208
    .local v0, "cancel":Landroid/view/MotionEvent;
    const/16 v4, 0xa

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->setAction(I)V

    .line 209
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->processListScrollEvent(Landroid/view/MotionEvent;)Z

    .line 210
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 213
    .end local v0    # "cancel":Landroid/view/MotionEvent;
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->processInfoPreviewEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public releaseHoverListenerAndEvent()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 180
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHoverRelease:Z

    .line 181
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 183
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mIsListScroll:Z

    .line 184
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mRecongnizedStartTime:I

    .line 185
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mScrollDirection:I

    .line 186
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDx:I

    .line 187
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mTotalDy:I

    .line 188
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mListScrollListner:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;

    if-eqz v0, :cond_1

    .line 189
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mListScrollListner:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;

    invoke-interface {v0, v1, v1, v1, v1}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;->onRelease(IIII)Z

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mListScrollListner:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;

    .line 192
    :cond_1
    return-void
.end method

.method public setListener(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mListScrollListner:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;

    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHoverRelease:Z

    .line 177
    return-void
.end method
