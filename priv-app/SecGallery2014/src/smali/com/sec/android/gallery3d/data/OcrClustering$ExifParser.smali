.class Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;
.super Ljava/lang/Object;
.source "OcrClustering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/OcrClustering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ExifParser"
.end annotation


# static fields
.field private static final APP0_MARKER:B = -0x20t

.field private static final APP1_MARKER:B = -0x1ft

.field private static final APP_MARKER_COUNT:I = 0x40

.field private static final EOI_MARKER:B = -0x27t

.field private static final EXIF_HEADER:I = 0x6

.field private static final FILE_SIZE:I = 0x200000

.field private static final MARKER:I = 0x2

.field private static final MARKER_LEN:I = 0x2

.field private static final MARKER_PREFIX:B = -0x1t

.field private static final SOI_MARKER:B = -0x28t

.field private static final TAG:Ljava/lang/String; = "ExifParser"

.field private static final TIFF_HEADER:I = 0x8

.field private static final TIFF_HEADER_OFFSET:I = 0xc


# instance fields
.field private mApp1DataOffset:I

.field private mApp1Length:I

.field private mApp5DataOffset:I

.field private mApp5Length:I

.field private mByteBuffer:Ljava/nio/ByteBuffer;

.field private mEndian:Ljava/nio/ByteOrder;

.field private mFile:Ljava/io/RandomAccessFile;

.field private mFilePath:Ljava/lang/String;

.field private mLittleEndian:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mEndian:Ljava/nio/ByteOrder;

    .line 219
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    .line 223
    return-void
.end method

.method private IsExifFormat()Z
    .locals 12

    .prologue
    const/16 v11, 0x49

    const/16 v10, 0x2a

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 326
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    if-nez v6, :cond_1

    .line 399
    :cond_0
    :goto_0
    return v4

    .line 329
    :cond_1
    const/16 v6, 0x14

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 330
    .local v1, "byteBuffer":Ljava/nio/ByteBuffer;
    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 331
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 332
    .local v0, "buffer":[B
    const/16 v6, 0x8

    new-array v3, v6, [B

    .line 335
    .local v3, "temp":[B
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const-wide/16 v8, 0x2

    invoke-virtual {v6, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 336
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v7, 0x0

    array-length v8, v0

    invoke-virtual {v6, v0, v7, v8}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    .line 339
    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-virtual {v1, v3, v6, v7}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 342
    const/16 v6, -0x1f

    invoke-direct {p0, v3, v6}, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->checkMarker([BB)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 346
    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-virtual {v1, v3, v6, v7}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 347
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    mul-int/lit16 v6, v6, 0x100

    const/4 v7, 0x1

    aget-byte v7, v3, v7

    and-int/lit16 v7, v7, 0xff

    add-int/2addr v6, v7

    iput v6, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp1Length:I

    .line 349
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp1DataOffset:I

    .line 352
    const/4 v6, 0x0

    const/4 v7, 0x6

    invoke-virtual {v1, v3, v6, v7}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 354
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x45

    if-ne v6, v7, :cond_3

    const/4 v6, 0x1

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x78

    if-ne v6, v7, :cond_3

    const/4 v6, 0x2

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x69

    if-ne v6, v7, :cond_3

    const/4 v6, 0x3

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x66

    if-ne v6, v7, :cond_3

    move v6, v5

    :goto_1
    if-eqz v6, :cond_0

    .line 361
    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-virtual {v1, v3, v6, v7}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 364
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    if-ne v6, v11, :cond_4

    const/4 v6, 0x1

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    if-ne v6, v11, :cond_4

    .line 366
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mLittleEndian:Z

    .line 367
    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    iput-object v6, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mEndian:Ljava/nio/ByteOrder;

    .line 368
    const-string v6, "ExifParser"

    const-string v7, "Little endian"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    :goto_2
    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-virtual {v1, v3, v6, v7}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 381
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    if-ne v6, v10, :cond_6

    const/4 v6, 0x1

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    if-nez v6, :cond_6

    :cond_2
    :goto_3
    move v4, v5

    .line 399
    goto/16 :goto_0

    :cond_3
    move v6, v4

    .line 354
    goto :goto_1

    .line 369
    :cond_4
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x4d

    if-ne v6, v7, :cond_5

    const/4 v6, 0x1

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x4d

    if-ne v6, v7, :cond_5

    .line 371
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mLittleEndian:Z

    .line 372
    sget-object v6, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    iput-object v6, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mEndian:Ljava/nio/ByteOrder;

    .line 373
    const-string v6, "ExifParser"

    const-string v7, "Big endian"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 394
    :catch_0
    move-exception v2

    .line 396
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 375
    .end local v2    # "e":Ljava/io/IOException;
    :cond_5
    :try_start_1
    const-string v6, "ExifParser"

    const-string v7, "Endian error"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 384
    :cond_6
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    if-nez v6, :cond_7

    const/4 v6, 0x1

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    if-eq v6, v10, :cond_2

    .line 388
    :cond_7
    const-string v6, "ExifParser"

    const-string v7, "TIFF mark - error"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private IsJpegFormat()Z
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 284
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    if-nez v3, :cond_1

    .line 312
    :cond_0
    :goto_0
    return v2

    .line 287
    :cond_1
    const/4 v2, 0x0

    .line 288
    .local v2, "isJpeg":Z
    new-array v0, v4, [B

    .line 291
    .local v0, "buffer":[B
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-virtual {v3, v0, v4, v5}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 293
    const/16 v3, -0x28

    invoke-direct {p0, v0, v3}, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->checkMarker([BB)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 304
    const/4 v2, 0x1

    goto :goto_0

    .line 307
    :catch_0
    move-exception v1

    .line 309
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkMarker([BB)Z
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "marker"    # B

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 316
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 322
    :cond_1
    :goto_0
    return v0

    .line 319
    :cond_2
    aget-byte v2, p1, v1

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    aget-byte v2, p1, v0

    if-eq v2, p2, :cond_1

    :cond_3
    move v0, v1

    .line 322
    goto :goto_0
.end method

.method private reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 226
    iput-object v1, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    .line 227
    iput-object v1, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFilePath:Ljava/lang/String;

    .line 229
    iput v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp1Length:I

    .line 230
    iput v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp1DataOffset:I

    .line 232
    iput v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5Length:I

    .line 233
    iput v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5DataOffset:I

    .line 235
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mLittleEndian:Z

    .line 236
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mEndian:Ljava/nio/ByteOrder;

    .line 237
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 273
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v1, :cond_0

    .line 274
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->reset()V

    .line 281
    return-void

    .line 275
    :catch_0
    move-exception v0

    .line 277
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public getApp5Segment()Ljava/lang/String;
    .locals 14

    .prologue
    .line 567
    const-string v8, ""

    .line 569
    .local v8, "text":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    if-nez v10, :cond_0

    move-object v9, v8

    .line 680
    .end local v8    # "text":Ljava/lang/String;
    .local v9, "text":Ljava/lang/String;
    :goto_0
    return-object v9

    .line 576
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    .line 578
    .local v3, "hasApp5":Z
    :try_start_0
    iget v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp1DataOffset:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp1Length:I

    add-int/2addr v10, v11

    add-int/lit8 v6, v10, -0x2

    .line 581
    .local v6, "markerOffset":I
    const/4 v10, 0x2

    new-array v1, v10, [B

    .line 582
    .local v1, "buffer":[B
    const/4 v7, 0x0

    .line 584
    .local v7, "seekPos":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    const/16 v10, 0x40

    if-ge v4, v10, :cond_1

    .line 585
    const/4 v10, 0x1

    if-lt v4, v10, :cond_2

    iget v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp1DataOffset:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp1Length:I

    add-int/2addr v10, v11

    add-int/lit8 v10, v10, -0x2

    if-ne v6, v10, :cond_2

    .line 625
    :cond_1
    :goto_2
    if-nez v3, :cond_5

    move-object v9, v8

    .line 626
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto :goto_0

    .line 588
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_2
    if-ltz v6, :cond_1

    int-to-long v10, v6

    iget-object v12, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-gez v10, :cond_1

    .line 591
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v12, v6

    invoke-virtual {v10, v12, v13}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 592
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v11, 0x0

    const/4 v12, 0x2

    invoke-virtual {v10, v1, v11, v12}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_1

    .line 595
    const/4 v10, 0x0

    aget-byte v10, v1, v10

    const/4 v11, -0x1

    if-ne v10, v11, :cond_4

    const/4 v10, 0x1

    aget-byte v10, v1, v10

    const/16 v11, -0x1f

    if-lt v10, v11, :cond_4

    const/4 v10, 0x1

    aget-byte v10, v1, v10

    const/16 v11, -0x11

    if-gt v10, v11, :cond_4

    .line 600
    const/4 v10, 0x1

    aget-byte v10, v1, v10

    const/16 v11, -0x1b

    if-ne v10, v11, :cond_3

    .line 601
    const/4 v3, 0x1

    .line 602
    goto :goto_2

    .line 605
    :cond_3
    add-int/lit8 v7, v6, 0x2

    .line 607
    if-ltz v7, :cond_1

    int-to-long v10, v7

    iget-object v12, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-gez v10, :cond_1

    .line 610
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v12, v7

    invoke-virtual {v10, v12, v13}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 614
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v11, 0x0

    const/4 v12, 0x2

    invoke-virtual {v10, v1, v11, v12}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_1

    .line 617
    const/4 v10, 0x0

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    mul-int/lit16 v10, v10, 0x100

    const/4 v11, 0x1

    aget-byte v11, v1, v11

    and-int/lit16 v11, v11, 0xff

    add-int v5, v10, v11

    .line 620
    .local v5, "markerLength":I
    add-int/lit8 v10, v5, 0x2

    add-int/2addr v6, v10

    .line 584
    .end local v5    # "markerLength":I
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 628
    :cond_5
    add-int/lit8 v10, v6, 0x2

    add-int/lit8 v10, v10, 0x2

    iput v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5DataOffset:I

    .line 630
    iget v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5DataOffset:I

    if-lez v10, :cond_6

    iget v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5DataOffset:I

    int-to-long v10, v10

    iget-object v12, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-ltz v10, :cond_7

    :cond_6
    move-object v9, v8

    .line 631
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 633
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_7
    add-int/lit8 v7, v6, 0x2

    .line 635
    if-ltz v7, :cond_8

    int-to-long v10, v7

    iget-object v12, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-ltz v10, :cond_9

    :cond_8
    move-object v9, v8

    .line 636
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 638
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_9
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v12, v7

    invoke-virtual {v10, v12, v13}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 641
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v11, 0x0

    const/4 v12, 0x2

    invoke-virtual {v10, v1, v11, v12}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v10

    const/4 v11, -0x1

    if-ne v10, v11, :cond_a

    move-object v9, v8

    .line 642
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 644
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_a
    const/4 v10, 0x0

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    mul-int/lit16 v10, v10, 0x100

    const/4 v11, 0x1

    aget-byte v11, v1, v11

    and-int/lit16 v11, v11, 0xff

    add-int v5, v10, v11

    .line 649
    .restart local v5    # "markerLength":I
    iput v5, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5Length:I

    .line 651
    iget v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5Length:I

    if-nez v10, :cond_b

    move-object v9, v8

    .line 652
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 655
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_b
    const/4 v0, 0x0

    .line 657
    .local v0, "attribute":[B
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    if-nez v10, :cond_c

    .line 658
    const/high16 v10, 0x200000

    invoke-static {v10}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    .line 661
    :cond_c
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    if-eqz v10, :cond_d

    .line 662
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 664
    :cond_d
    if-eqz v0, :cond_10

    .line 665
    iget v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5DataOffset:I

    if-ltz v10, :cond_e

    iget v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5DataOffset:I

    int-to-long v10, v10

    iget-object v12, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-ltz v10, :cond_f

    :cond_e
    move-object v9, v8

    .line 666
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 668
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_f
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    iget v11, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5DataOffset:I

    int-to-long v12, v11

    invoke-virtual {v10, v12, v13}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 670
    array-length v10, v0

    iget v11, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5Length:I

    add-int/lit8 v11, v11, -0x2

    if-lt v10, v11, :cond_10

    iget-object v10, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v11, 0x0

    iget v12, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5Length:I

    add-int/lit8 v12, v12, -0x2

    invoke-virtual {v10, v0, v11, v12}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_10

    .line 672
    new-instance v9, Ljava/lang/String;

    const/4 v10, 0x0

    iget v11, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5Length:I

    add-int/lit8 v11, v11, -0x2

    invoke-direct {v9, v0, v10, v11}, Ljava/lang/String;-><init>([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    move-object v8, v9

    .end local v0    # "attribute":[B
    .end local v1    # "buffer":[B
    .end local v4    # "i":I
    .end local v5    # "markerLength":I
    .end local v6    # "markerOffset":I
    .end local v7    # "seekPos":I
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_10
    :goto_3
    move-object v9, v8

    .line 680
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 675
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 677
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method

.method public open(Ljava/lang/String;)Z
    .locals 5
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 240
    const/4 v2, 0x0

    .line 243
    .local v2, "isOpened":Z
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 245
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 246
    const/4 v3, 0x0

    .line 268
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return v3

    .line 248
    .restart local v1    # "file":Ljava/io/File;
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFilePath:Ljava/lang/String;

    .line 250
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v4, "rw"

    invoke-direct {v3, p1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    .line 252
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v3, :cond_1

    .line 253
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->IsJpegFormat()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 254
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->IsExifFormat()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_1

    .line 255
    const/4 v2, 0x1

    .line 264
    .end local v1    # "file":Ljava/io/File;
    :cond_1
    :goto_1
    if-nez v2, :cond_2

    .line 265
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->close()V

    :cond_2
    move v3, v2

    .line 268
    goto :goto_0

    .line 259
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v3, "ExifParser"

    const-string v4, "File is not founded."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setApp5Segment(Ljava/lang/String;)Z
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 403
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->setApp5Segment(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public setApp5Segment(Ljava/lang/String;Z)Z
    .locals 18
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "replaced"    # Z

    .prologue
    .line 407
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    if-nez v14, :cond_1

    .line 408
    const/4 v6, 0x0

    .line 563
    :cond_0
    :goto_0
    return v6

    .line 413
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v14

    if-nez v14, :cond_3

    .line 414
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 416
    :cond_3
    const/4 v6, 0x0

    .line 419
    .local v6, "hasApp5":Z
    :try_start_0
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp1DataOffset:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp1Length:I

    add-int/2addr v14, v15

    add-int/lit8 v9, v14, -0x2

    .line 422
    .local v9, "markerOffset":I
    const/4 v14, 0x2

    new-array v4, v14, [B

    .line 423
    .local v4, "buffer":[B
    const/4 v11, 0x0

    .line 425
    .local v11, "seekPos":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    const/16 v14, 0x40

    if-ge v7, v14, :cond_4

    .line 426
    const/4 v14, 0x1

    if-lt v7, v14, :cond_7

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp1DataOffset:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp1Length:I

    add-int/2addr v14, v15

    add-int/lit8 v14, v14, -0x2

    if-ne v9, v14, :cond_7

    .line 467
    :cond_4
    :goto_2
    const/4 v3, 0x0

    .line 469
    .local v3, "attribute":[B
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    if-nez v14, :cond_5

    .line 470
    const/high16 v14, 0x200000

    invoke-static {v14}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    .line 473
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    if-eqz v14, :cond_6

    .line 474
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    .line 476
    :cond_6
    if-nez v3, :cond_d

    .line 477
    const/4 v6, 0x0

    goto :goto_0

    .line 429
    .end local v3    # "attribute":[B
    :cond_7
    if-ltz v9, :cond_4

    int-to-long v14, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-gez v14, :cond_4

    .line 432
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v9

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 433
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v14, v4, v15, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_8

    .line 434
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 436
    :cond_8
    const/4 v14, 0x0

    aget-byte v14, v4, v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_c

    const/4 v14, 0x1

    aget-byte v14, v4, v14

    const/16 v15, -0x1f

    if-lt v14, v15, :cond_c

    const/4 v14, 0x1

    aget-byte v14, v4, v14

    const/16 v15, -0x11

    if-gt v14, v15, :cond_c

    .line 441
    const/4 v14, 0x1

    aget-byte v14, v4, v14

    const/16 v15, -0x1b

    if-ne v14, v15, :cond_9

    if-nez p2, :cond_9

    .line 442
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 443
    :cond_9
    const/4 v14, 0x1

    aget-byte v14, v4, v14

    const/16 v15, -0x1b

    if-ne v14, v15, :cond_a

    const/4 v14, 0x1

    move/from16 v0, p2

    if-ne v0, v14, :cond_a

    .line 444
    const/4 v6, 0x1

    .line 445
    goto :goto_2

    .line 447
    :cond_a
    add-int/lit8 v11, v9, 0x2

    .line 449
    if-ltz v11, :cond_4

    int-to-long v14, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-gez v14, :cond_4

    .line 452
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v11

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 456
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v14, v4, v15, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_b

    .line 457
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 459
    :cond_b
    const/4 v14, 0x0

    aget-byte v14, v4, v14

    and-int/lit16 v14, v14, 0xff

    mul-int/lit16 v14, v14, 0x100

    const/4 v15, 0x1

    aget-byte v15, v4, v15

    and-int/lit16 v15, v15, 0xff

    add-int v8, v14, v15

    .line 462
    .local v8, "markerLength":I
    add-int/lit8 v14, v8, 0x2

    add-int/2addr v9, v14

    .line 425
    .end local v8    # "markerLength":I
    :cond_c
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 479
    .restart local v3    # "attribute":[B
    :cond_d
    move v2, v9

    .line 481
    .local v2, "app5MarkerOffset":I
    add-int/lit8 v14, v2, 0x2

    add-int/lit8 v14, v14, 0x2

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5DataOffset:I

    .line 483
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5DataOffset:I

    if-lez v14, :cond_e

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5DataOffset:I

    int-to-long v14, v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_f

    .line 484
    :cond_e
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 486
    :cond_f
    const/4 v10, 0x0

    .line 488
    .local v10, "nextMarkerOffset":I
    if-eqz v6, :cond_14

    .line 489
    add-int/lit8 v11, v2, 0x2

    .line 491
    if-ltz v11, :cond_10

    int-to-long v14, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_11

    .line 492
    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 494
    :cond_11
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v11

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 497
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v14, v4, v15, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_12

    .line 498
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 500
    :cond_12
    const/4 v14, 0x0

    aget-byte v14, v4, v14

    and-int/lit16 v14, v14, 0xff

    mul-int/lit16 v14, v14, 0x100

    const/4 v15, 0x1

    aget-byte v15, v4, v15

    and-int/lit16 v15, v15, 0xff

    add-int v8, v14, v15

    .line 503
    .restart local v8    # "markerLength":I
    add-int v14, v2, v8

    add-int/lit8 v10, v14, 0x2

    .line 509
    .end local v8    # "markerLength":I
    :goto_3
    if-ltz v10, :cond_13

    int-to-long v14, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_15

    .line 510
    :cond_13
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 506
    :cond_14
    move v10, v2

    goto :goto_3

    .line 512
    :cond_15
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    .line 514
    .local v12, "oldFileSize":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v10

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 515
    array-length v14, v3

    int-to-long v0, v10

    move-wide/from16 v16, v0

    sub-long v16, v12, v16

    move-wide/from16 v0, v16

    long-to-int v15, v0

    if-ge v14, v15, :cond_16

    .line 516
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 518
    :cond_16
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    int-to-long v0, v10

    move-wide/from16 v16, v0

    sub-long v16, v12, v16

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v14, v3, v15, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_17

    .line 520
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 523
    :cond_17
    if-ltz v2, :cond_18

    int-to-long v14, v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_19

    .line 524
    :cond_18
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 527
    :cond_19
    const/4 v14, 0x0

    const/4 v15, -0x1

    aput-byte v15, v4, v14

    .line 528
    const/4 v14, 0x1

    const/16 v15, -0x1b

    aput-byte v15, v4, v14

    .line 530
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v2

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 531
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v14, v4, v15, v0}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 534
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v14

    array-length v14, v14

    add-int/lit8 v14, v14, 0x2

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5Length:I

    .line 535
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5Length:I

    and-int/lit16 v15, v15, 0xff

    int-to-byte v15, v15

    aput-byte v15, v4, v14

    .line 536
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5Length:I

    const v16, 0xff00

    and-int v15, v15, v16

    div-int/lit16 v15, v15, 0x100

    int-to-byte v15, v15

    aput-byte v15, v4, v14

    .line 537
    add-int/lit8 v11, v2, 0x2

    .line 538
    if-ltz v11, :cond_1a

    int-to-long v14, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_1b

    .line 539
    :cond_1a
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 540
    :cond_1b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v11

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 541
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v14, v4, v15, v0}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 543
    add-int/lit8 v14, v2, 0x2

    add-int/lit8 v11, v14, 0x2

    .line 544
    if-ltz v11, :cond_1c

    int-to-long v14, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_1d

    .line 545
    :cond_1c
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 546
    :cond_1d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v11

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 547
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v17

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v14 .. v17}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 549
    add-int/lit8 v14, v2, 0x2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mApp5Length:I

    add-int v11, v14, v15

    .line 550
    if-ltz v11, :cond_1e

    int-to-long v14, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_1f

    .line 551
    :cond_1e
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 552
    :cond_1f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v11

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 553
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    int-to-long v0, v10

    move-wide/from16 v16, v0

    sub-long v16, v12, v16

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v14, v3, v15, v0}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 555
    if-ge v11, v10, :cond_0

    goto/16 :goto_0

    .line 558
    .end local v2    # "app5MarkerOffset":I
    .end local v3    # "attribute":[B
    .end local v4    # "buffer":[B
    .end local v7    # "i":I
    .end local v9    # "markerOffset":I
    .end local v10    # "nextMarkerOffset":I
    .end local v11    # "seekPos":I
    .end local v12    # "oldFileSize":J
    :catch_0
    move-exception v5

    .line 560
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0
.end method
