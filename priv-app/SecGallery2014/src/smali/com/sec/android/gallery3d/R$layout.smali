.class public final Lcom/sec/android/gallery3d/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final account_setting:I = 0x7f030000

.field public static final acount_list_item:I = 0x7f030001

.field public static final action_bar_button_layout_for_album_view_view_mode:I = 0x7f030002

.field public static final action_bar_button_layout_for_detail_view_view_mode:I = 0x7f030003

.field public static final action_bar_button_layout_for_photo_view_mode:I = 0x7f030004

.field public static final action_bar_button_layout_for_photo_view_new_album_mode:I = 0x7f030005

.field public static final action_bar_button_layout_for_picker_mode:I = 0x7f030006

.field public static final action_bar_layout_for_gallerysearch_view:I = 0x7f030007

.field public static final action_bar_layout_for_search_view:I = 0x7f030008

.field public static final action_bar_spinner_dropdown_item:I = 0x7f030009

.field public static final action_bar_spinner_title:I = 0x7f03000a

.field public static final action_bar_tab_layout_for_album_view_view_mode:I = 0x7f03000b

.field public static final action_bar_text:I = 0x7f03000c

.field public static final action_bar_two_line_text:I = 0x7f03000d

.field public static final action_mode:I = 0x7f03000e

.field public static final action_select_drop_down_mode:I = 0x7f03000f

.field public static final action_select_mode:I = 0x7f030010

.field public static final action_select_on_action_bar_mode:I = 0x7f030011

.field public static final actionbar_done_and_cancel:I = 0x7f030012

.field public static final actionbar_drawer:I = 0x7f030013

.field public static final actionbar_location_view:I = 0x7f030014

.field public static final actionbar_moreinfo_done_and_cancel:I = 0x7f030015

.field public static final actionbar_title_done_and_cancel:I = 0x7f030016

.field public static final activity_chinasns_setting:I = 0x7f030017

.field public static final activity_gallery_share:I = 0x7f030018

.field public static final adapt_display_dialog:I = 0x7f030019

.field public static final add_user_tag_dialog_text_entry:I = 0x7f03001a

.field public static final add_user_tag_list_main:I = 0x7f03001b

.field public static final air_browse_popup:I = 0x7f03001c

.field public static final album_content:I = 0x7f03001d

.field public static final album_header:I = 0x7f03001e

.field public static final album_set_item:I = 0x7f03001f

.field public static final alert_buddy_share_contact_list:I = 0x7f030020

.field public static final alert_dialog_applist_item:I = 0x7f030021

.field public static final alert_dialog_do_not_show_again:I = 0x7f030022

.field public static final alert_dialog_gridlist:I = 0x7f030023

.field public static final alert_dialog_gridlist_item:I = 0x7f030024

.field public static final alert_dialog_slink_charge:I = 0x7f030025

.field public static final alert_dialog_slink_charges:I = 0x7f030026

.field public static final alert_dialog_text_entry:I = 0x7f030027

.field public static final alert_dialog_wifi_display:I = 0x7f030028

.field public static final allshare_device_list:I = 0x7f030029

.field public static final appwidget_main:I = 0x7f03002a

.field public static final change_player_list_header:I = 0x7f03002b

.field public static final change_player_list_item:I = 0x7f03002c

.field public static final change_player_list_title:I = 0x7f03002d

.field public static final chinasns_comment_list_footer:I = 0x7f03002e

.field public static final chinese_sns_comment_activity:I = 0x7f03002f

.field public static final chinese_sns_comment_fragment:I = 0x7f030030

.field public static final chn_usage_alert:I = 0x7f030031

.field public static final choose_widget_type:I = 0x7f030032

.field public static final comment_list_item:I = 0x7f030033

.field public static final contact_dialog:I = 0x7f030034

.field public static final contact_dialog_chn:I = 0x7f030035

.field public static final contact_dialog_chn_2:I = 0x7f030036

.field public static final contact_dialog_chn_with_sns:I = 0x7f030037

.field public static final contact_dialog_chn_with_sns_2:I = 0x7f030038

.field public static final contact_dialog_with_sns:I = 0x7f030039

.field public static final content_to_display_dialog:I = 0x7f03003a

.field public static final contextual_list_row:I = 0x7f03003b

.field public static final contextual_list_row_weather:I = 0x7f03003c

.field public static final contextual_tag_help:I = 0x7f03003d

.field public static final contextual_tag_settinglist_null_item:I = 0x7f03003e

.field public static final contextual_weather_tag_list_item:I = 0x7f03003f

.field public static final contextualtagsetting_fragment_main:I = 0x7f030040

.field public static final contextualtagsetting_main:I = 0x7f030041

.field public static final contextualtagweathersetting_main:I = 0x7f030042

.field public static final cover_view:I = 0x7f030043

.field public static final cropimage:I = 0x7f030044

.field public static final custom_dialog_operation:I = 0x7f030045

.field public static final custom_dialog_operation_with_message:I = 0x7f030046

.field public static final custom_preference_screen:I = 0x7f030047

.field public static final custom_tab_dialog:I = 0x7f030048

.field public static final custom_tab_dialog_white:I = 0x7f030049

.field public static final details:I = 0x7f03004a

.field public static final details_dialog_advance:I = 0x7f03004b

.field public static final details_dialog_s:I = 0x7f03004c

.field public static final details_dialog_s_baseinfo:I = 0x7f03004d

.field public static final details_item:I = 0x7f03004e

.field public static final details_item_s:I = 0x7f03004f

.field public static final details_list:I = 0x7f030050

.field public static final dialog_picker:I = 0x7f030051

.field public static final drag_and_drop:I = 0x7f030052

.field public static final draganddrop:I = 0x7f030053

.field public static final drawer_help:I = 0x7f030054

.field public static final drawer_help_new:I = 0x7f030055

.field public static final drawer_list_empty_view:I = 0x7f030056

.field public static final drawer_list_header:I = 0x7f030057

.field public static final drawer_list_header_expandable:I = 0x7f030058

.field public static final drawer_list_item:I = 0x7f030059

.field public static final drawer_search:I = 0x7f03005a

.field public static final eventview_album_layout:I = 0x7f03005b

.field public static final evf_presentation:I = 0x7f03005c

.field public static final face_tag_dialog:I = 0x7f03005d

.field public static final filter_item_layout:I = 0x7f03005e

.field public static final filter_panel_detail_hint_layout:I = 0x7f03005f

.field public static final filter_panel_layout:I = 0x7f030060

.field public static final filter_selected_item_layout:I = 0x7f030061

.field public static final filterby_setting:I = 0x7f030062

.field public static final focus_shot_help:I = 0x7f030063

.field public static final focus_shot_help_new:I = 0x7f030064

.field public static final fragment_planet:I = 0x7f030065

.field public static final gallery_search_view:I = 0x7f030066

.field public static final gallerysearch_history:I = 0x7f030067

.field public static final gallerysearch_history_delete_all:I = 0x7f030068

.field public static final gallerysearch_history_list:I = 0x7f030069

.field public static final gl_root_group:I = 0x7f03006a

.field public static final group_item_tagcloud_expandable:I = 0x7f03006b

.field public static final help_album_picker_layout:I = 0x7f03006c

.field public static final help_edit_pic_tap:I = 0x7f03006d

.field public static final help_face_list:I = 0x7f03006e

.field public static final help_layout:I = 0x7f03006f

.field public static final help_photo_picker_layout:I = 0x7f030070

.field public static final help_photonote:I = 0x7f030071

.field public static final help_photonote_toolbar:I = 0x7f030072

.field public static final image_list_item:I = 0x7f030073

.field public static final imagenote_spinner:I = 0x7f030074

.field public static final imagenote_toolbar:I = 0x7f030075

.field public static final imagenote_view:I = 0x7f030076

.field public static final ingest_activity_item_list:I = 0x7f030077

.field public static final ingest_date_tile:I = 0x7f030078

.field public static final ingest_fullsize:I = 0x7f030079

.field public static final ingest_thumbnail:I = 0x7f03007a

.field public static final item_tagcloud_expandable:I = 0x7f03007b

.field public static final layout_noitems_popup_description:I = 0x7f03007c

.field public static final listitem_slideshow_advanced_divider:I = 0x7f03007d

.field public static final listitem_slideshow_music:I = 0x7f03007e

.field public static final listitem_slideshow_music_white:I = 0x7f03007f

.field public static final magazine_widget_default:I = 0x7f030080

.field public static final magazine_widget_flipper_item_multi_left_rightupdown:I = 0x7f030081

.field public static final magazine_widget_flipper_item_multi_up_down:I = 0x7f030082

.field public static final magazine_widget_flipper_item_single:I = 0x7f030083

.field public static final main:I = 0x7f030084

.field public static final manage_offline_bar:I = 0x7f030085

.field public static final map_thumbnail_overlay:I = 0x7f030086

.field public static final mapfragment_main:I = 0x7f030087

.field public static final mapfragment_thumbnail:I = 0x7f030088

.field public static final moreinfo_category_dialog_list_item:I = 0x7f030089

.field public static final moreinfo_category_dialog_listview:I = 0x7f03008a

.field public static final moreinfo_details_item:I = 0x7f03008b

.field public static final moreinfo_expandable_subitem_delete:I = 0x7f03008c

.field public static final moreinfo_expandablelayout:I = 0x7f03008d

.field public static final moreinfo_item_content:I = 0x7f03008e

.field public static final moreinfo_item_icon:I = 0x7f03008f

.field public static final moreinfo_item_title:I = 0x7f030090

.field public static final moreinfo_location_editor:I = 0x7f030091

.field public static final moreinfo_location_editor_gps_popup:I = 0x7f030092

.field public static final moreinfo_location_item_edit:I = 0x7f030093

.field public static final moreinfo_main:I = 0x7f030094

.field public static final moreinfo_set_detailslist:I = 0x7f030095

.field public static final moreinfo_set_detailslist_header:I = 0x7f030096

.field public static final moreinfo_set_expandable:I = 0x7f030097

.field public static final moreinfo_set_location:I = 0x7f030098

.field public static final moreinfo_set_simple:I = 0x7f030099

.field public static final moreinfo_usertag_edit_edittext_set:I = 0x7f03009a

.field public static final moreinfo_usertag_edittext_set:I = 0x7f03009b

.field public static final moreinfo_usertag_editview:I = 0x7f03009c

.field public static final moreinfo_usertag_editview_listitem:I = 0x7f03009d

.field public static final moreinfo_usertag_item_edit:I = 0x7f03009e

.field public static final moreinfo_usertag_noitems:I = 0x7f03009f

.field public static final motion_dialog_guide_off:I = 0x7f0300a0

.field public static final motion_dialog_guide_on:I = 0x7f0300a1

.field public static final motion_help_panning:I = 0x7f0300a2

.field public static final motion_help_peek:I = 0x7f0300a3

.field public static final motion_help_tilt:I = 0x7f0300a4

.field public static final motion_preview:I = 0x7f0300a5

.field public static final motion_preview_white:I = 0x7f0300a6

.field public static final motion_tutorial:I = 0x7f0300a7

.field public static final motion_tutorial_info_dialog:I = 0x7f0300a8

.field public static final move_to_knox_choice_popup:I = 0x7f0300a9

.field public static final movie_view:I = 0x7f0300aa

.field public static final multigrid_content:I = 0x7f0300ab

.field public static final ocr_popup:I = 0x7f0300ac

.field public static final ocr_popup_item:I = 0x7f0300ad

.field public static final photo_frame:I = 0x7f0300ae

.field public static final photo_frame_easy_default:I = 0x7f0300af

.field public static final photo_frame_easy_flipper:I = 0x7f0300b0

.field public static final photo_frame_flipper_frame:I = 0x7f0300b1

.field public static final photo_frame_flipper_item_multi_left_rightupdown:I = 0x7f0300b2

.field public static final photo_frame_flipper_item_multi_up_down:I = 0x7f0300b3

.field public static final photo_frame_flipper_item_multi_up_downleftright:I = 0x7f0300b4

.field public static final photo_frame_flipper_item_single:I = 0x7f0300b5

.field public static final photo_frame_main_0sec:I = 0x7f0300b6

.field public static final photo_frame_main_3sec:I = 0x7f0300b7

.field public static final photo_frame_main_5sec:I = 0x7f0300b8

.field public static final photo_frame_main_7sec:I = 0x7f0300b9

.field public static final photo_frame_preference_category_frame_type:I = 0x7f0300ba

.field public static final photo_set_item:I = 0x7f0300bb

.field public static final photonote_view:I = 0x7f0300bc

.field public static final photopage_bottom_controls:I = 0x7f0300bd

.field public static final photopage_progress_bar:I = 0x7f0300be

.field public static final popup_list_item:I = 0x7f0300bf

.field public static final presentation_main:I = 0x7f0300c0

.field public static final quick_scroll_help:I = 0x7f0300c1

.field public static final quick_scroll_help_new:I = 0x7f0300c2

.field public static final recommend_dropbox_dialog:I = 0x7f0300c3

.field public static final replymanage_auto_update:I = 0x7f0300c4

.field public static final replymanage_preview:I = 0x7f0300c5

.field public static final replymanage_setting_main:I = 0x7f0300c6

.field public static final replymanage_wlan_update:I = 0x7f0300c7

.field public static final search_no_item_view:I = 0x7f0300c8

.field public static final search_view:I = 0x7f0300c9

.field public static final select_dialog_multichoice:I = 0x7f0300ca

.field public static final selection_mode_header:I = 0x7f0300cb

.field public static final selection_popup_row:I = 0x7f0300cc

.field public static final setting_main_frag_list_text_item:I = 0x7f0300cd

.field public static final setting_main_fragment_layout:I = 0x7f0300ce

.field public static final settings_delete_history_list:I = 0x7f0300cf

.field public static final settings_delete_history_list_item:I = 0x7f0300d0

.field public static final share_action_check_dialog:I = 0x7f0300d1

.field public static final share_via_dialog_applist_item:I = 0x7f0300d2

.field public static final share_via_list_layout:I = 0x7f0300d3

.field public static final sim_icon_list_item:I = 0x7f0300d4

.field public static final simple_list_item_1:I = 0x7f0300d5

.field public static final simple_list_item_2:I = 0x7f0300d6

.field public static final simple_list_item_3:I = 0x7f0300d7

.field public static final simple_list_item_single_choice:I = 0x7f0300d8

.field public static final simple_list_item_single_choice_advanced:I = 0x7f0300d9

.field public static final simple_list_slideshow_music_item:I = 0x7f0300da

.field public static final slideshow_pause_activity_layout:I = 0x7f0300db

.field public static final slideshow_pause_activity_layout_land:I = 0x7f0300dc

.field public static final slideshowsettings_main:I = 0x7f0300dd

.field public static final sns_eidt_list_item:I = 0x7f0300de

.field public static final sns_icon_list_item:I = 0x7f0300df

.field public static final sns_reply_preview:I = 0x7f0300e0

.field public static final tab_indicator:I = 0x7f0300e1

.field public static final tab_indicator_white:I = 0x7f0300e2

.field public static final tag_scrollview_horizontal_style:I = 0x7f0300e3

.field public static final tagcloud_layout:I = 0x7f0300e4

.field public static final tagcloud_layout_regular:I = 0x7f0300e5

.field public static final top_left_action_bar_buttons:I = 0x7f0300e6

.field public static final trim_menu:I = 0x7f0300e7

.field public static final trim_view:I = 0x7f0300e8

.field public static final wifisync_dialog:I = 0x7f0300e9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
