.class public interface abstract Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
.super Ljava/lang/Object;
.source "PhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract getSelectonModeBarHeight()I
.end method

.method public abstract isLongPressFinished()Z
.end method

.method public abstract onActionBarAllowed(Z)V
.end method

.method public abstract onActionBarWanted()V
.end method

.method public abstract onCommitDeleteImage()V
.end method

.method public abstract onCurrentImageUpdated()V
.end method

.method public abstract onDeleteImage(Lcom/sec/android/gallery3d/data/Path;I)V
.end method

.method public abstract onDown()V
.end method

.method public abstract onFilmModeChanged(Z)V
.end method

.method public abstract onFullScreenChanged(Z)V
.end method

.method public abstract onLongPress()V
.end method

.method public abstract onPictureCenter(Z)V
.end method

.method public abstract onScaleMin()Z
.end method

.method public abstract onSingleTapUp(II)V
.end method

.method public abstract onSizeChanged()V
.end method

.method public abstract onUndoBarVisibilityChanged(Z)V
.end method

.method public abstract onUndoDeleteImage()V
.end method
