.class public Lcom/sec/android/gallery3d/ui/PhotoView$ImageData;
.super Ljava/lang/Object;
.source "PhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImageData"
.end annotation


# instance fields
.field public bitmap:Landroid/graphics/Bitmap;

.field public rotation:I

.field texture:Lcom/sec/android/gallery3d/glrenderer/BasicTexture;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;I)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "rotation"    # I

    .prologue
    .line 4187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4188
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ImageData;->bitmap:Landroid/graphics/Bitmap;

    .line 4189
    iput p2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ImageData;->rotation:I

    .line 4191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ImageData;->texture:Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

    .line 4193
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;I)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p3, "rotation"    # I

    .prologue
    .line 4197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4198
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ImageData;->bitmap:Landroid/graphics/Bitmap;

    .line 4199
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ImageData;->texture:Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

    .line 4200
    iput p3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ImageData;->rotation:I

    .line 4201
    return-void
.end method
