.class public Lcom/sec/android/gallery3d/app/AlbumDataLoader;
.super Ljava/lang/Object;
.source "AlbumDataLoader.java"

# interfaces
.implements Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;,
        Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateContent;,
        Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;,
        Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;,
        Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;
    }
.end annotation


# static fields
.field private static final DATA_CACHE_SIZE:I = 0x80

.field private static final DEFAULT_INDEX:I = 0x0

.field private static final FIRST_LOAD_COUNT:I = 0x10

.field private static final MAX_LOAD_COUNT:I = 0x40

.field private static final MIN_LOAD_COUNT:I = 0x20

.field private static final MSG_LOAD_FINISH:I = 0x2

.field private static final MSG_LOAD_START:I = 0x1

.field private static final MSG_RUN_OBJECT:I = 0x3

.field private static final PATCH_LOAD_COUNT_LAND:I

.field private static final PATCH_LOAD_COUNT_PORT:I

.field private static final REMOTE_DEVICE_MAX_LOAD_COUNT:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "AlbumDataAdapter"


# instance fields
.field private mActiveEnd:I

.field private mActiveStart:I

.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

.field private mContentEnd:I

.field private mContentStart:I

.field private final mData:[Lcom/sec/android/gallery3d/data/MediaItem;

.field private mDataListener:Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;

.field private mFailedVersion:J

.field private mGenericMotionFocus:I

.field private mGroupIndex:J

.field private final mItemVersion:[J

.field private mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

.field private final mMainHandler:Landroid/os/Handler;

.field private mReloadTask:Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

.field private final mSetVersion:[J

.field private mSize:I

.field private mSource:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mSourceVersion:J

.field private mStartIndex:I

.field private mbRemoteDevice:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 53
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    sput v0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->PATCH_LOAD_COUNT_LAND:I

    .line 54
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    sput v1, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->PATCH_LOAD_COUNT_PORT:I

    return-void

    .line 53
    :cond_0
    const/4 v0, 0x5

    goto :goto_0

    .line 54
    :cond_1
    const/4 v1, 0x3

    goto :goto_1
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 6
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/16 v3, 0x80

    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveStart:I

    .line 70
    iput v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveEnd:I

    .line 72
    iput v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I

    .line 73
    iput v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentEnd:I

    .line 78
    iput-wide v4, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSourceVersion:J

    .line 81
    iput v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSize:I

    .line 88
    iput-wide v4, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mFailedVersion:J

    .line 91
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mGenericMotionFocus:I

    .line 92
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mGroupIndex:J

    .line 94
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mbRemoteDevice:Z

    .line 98
    iput v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mStartIndex:I

    .line 101
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 103
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 104
    new-array v0, v3, [Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    .line 105
    new-array v0, v3, [J

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mItemVersion:[J

    .line 106
    new-array v0, v3, [J

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSetVersion:[J

    .line 107
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mItemVersion:[J

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->fill([JJ)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSetVersion:[J

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->fill([JJ)V

    .line 110
    instance-of v0, p2, Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    .end local p2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getBaseSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    if-eqz v0, :cond_0

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mbRemoteDevice:Z

    .line 114
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$1;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$1;-><init>(Lcom/sec/android/gallery3d/app/AlbumDataLoader;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mMainHandler:Landroid/os/Handler;

    .line 135
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/app/AlbumReloader;)V
    .locals 1
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "albumReloader"    # Lcom/sec/android/gallery3d/app/AlbumReloader;

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 139
    iput-object p3, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    .line 140
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/AlbumReloader;->setDataListener(Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;)V

    .line 141
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Lcom/sec/android/gallery3d/app/LoadingListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mFailedVersion:J

    return-wide v0
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mbRemoteDevice:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/app/AlbumDataLoader;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;
    .param p1, "x1"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mFailedVersion:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->getPatchCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mDataListener:Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveEnd:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/gallery3d/app/AlbumDataLoader;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveEnd:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)[J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mItemVersion:[J

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)[Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveStart:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->getReloadInfo()Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Lcom/sec/android/gallery3d/app/AlbumReloader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/gallery3d/app/AlbumDataLoader;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;
    .param p1, "x1"    # Ljava/util/concurrent/Callable;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mGroupIndex:J

    return-wide v0
.end method

.method static synthetic access$2200(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/gallery3d/app/AlbumDataLoader;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->loadGroupItems(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSourceVersion:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/app/AlbumDataLoader;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;
    .param p1, "x1"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSourceVersion:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSize:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/gallery3d/app/AlbumDataLoader;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSize:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)[J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSetVersion:[J

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentEnd:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/gallery3d/app/AlbumDataLoader;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentEnd:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mStartIndex:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/gallery3d/app/AlbumDataLoader;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mStartIndex:I

    return p1
.end method

.method private clearSlot(I)V
    .locals 4
    .param p1, "slotIndex"    # I

    .prologue
    const-wide/16 v2, -0x1

    .line 202
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 203
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mItemVersion:[J

    aput-wide v2, v0, p1

    .line 204
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSetVersion:[J

    aput-wide v2, v0, p1

    .line 205
    return-void
.end method

.method private executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 264
    .local p1, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    new-instance v1, Ljava/util/concurrent/FutureTask;

    invoke-direct {v1, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 265
    .local v1, "task":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<TT;>;"
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mMainHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mMainHandler:Landroid/os/Handler;

    const/4 v4, 0x3

    invoke-virtual {v3, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 268
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 270
    :goto_0
    return-object v2

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, "e":Ljava/lang/InterruptedException;
    const/4 v2, 0x0

    goto :goto_0

    .line 271
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 272
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private getPatchCount()I
    .locals 2

    .prologue
    .line 534
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    .line 535
    .local v0, "orientation":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 536
    sget v1, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->PATCH_LOAD_COUNT_PORT:I

    .line 538
    :goto_0
    return v1

    :cond_0
    sget v1, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->PATCH_LOAD_COUNT_LAND:I

    goto :goto_0
.end method

.method private getReloadInfo()Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumReloader;->getLatestUpdateInfo()Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;

    move-result-object v0

    return-object v0
.end method

.method private loadGroupItems(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;)V
    .locals 8
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "info"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;

    .prologue
    const/4 v4, 0x0

    .line 509
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v3, :cond_2

    .line 510
    iget v3, p2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->size:I

    invoke-virtual {p1, v4, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemEx(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 515
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :goto_0
    if-eqz v2, :cond_3

    .line 516
    iget-object v3, p2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->items:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    .line 517
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->items:Ljava/util/ArrayList;

    .line 518
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 519
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mGroupIndex:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 520
    iget-object v3, p2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->items:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 512
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_2
    iget v3, p2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->size:I

    invoke-virtual {p1, v4, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    .restart local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    goto :goto_0

    .line 525
    :cond_3
    iget-object v3, p2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->items:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput v3, p2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->size:I

    .line 526
    return-void
.end method

.method private setContentWindow(II)V
    .locals 5
    .param p1, "contentStart"    # I
    .param p2, "contentEnd"    # I

    .prologue
    .line 208
    iget v4, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I

    if-ne p1, v4, :cond_1

    iget v4, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentEnd:I

    if-ne p2, v4, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentEnd:I

    .line 210
    .local v0, "end":I
    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I

    .line 213
    .local v3, "start":I
    monitor-enter p0

    .line 214
    :try_start_0
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I

    .line 215
    iput p2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentEnd:I

    .line 216
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    if-ge p1, v0, :cond_2

    if-lt v3, p2, :cond_3

    .line 219
    :cond_2
    move v1, v3

    .local v1, "i":I
    move v2, v0

    .local v2, "n":I
    :goto_1
    if-ge v1, v2, :cond_5

    .line 220
    rem-int/lit16 v4, v1, 0x80

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->clearSlot(I)V

    .line 219
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 216
    .end local v1    # "i":I
    .end local v2    # "n":I
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 223
    :cond_3
    move v1, v3

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, p1, :cond_4

    .line 224
    rem-int/lit16 v4, v1, 0x80

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->clearSlot(I)V

    .line 223
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 226
    :cond_4
    move v1, p2

    move v2, v0

    .restart local v2    # "n":I
    :goto_3
    if-ge v1, v2, :cond_5

    .line 227
    rem-int/lit16 v4, v1, 0x80

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->clearSlot(I)V

    .line 226
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 230
    :cond_5
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->notifyDirty()V

    goto :goto_0
.end method


# virtual methods
.method public changeSource(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 0
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->pause()V

    .line 150
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->resume()V

    .line 152
    return-void
.end method

.method public findItem(Lcom/sec/android/gallery3d/data/Path;)I
    .locals 4
    .param p1, "id"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 192
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentEnd:I

    if-ge v0, v2, :cond_1

    .line 193
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    rem-int/lit16 v3, v0, 0x80

    aget-object v1, v2, v3

    .line 194
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 198
    .end local v0    # "i":I
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_1
    return v0

    .line 192
    .restart local v0    # "i":I
    .restart local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public get(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 169
    const/4 v0, 0x0

    .line 174
    :goto_0
    return-object v0

    .line 171
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->isActive(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v1, v1

    rem-int v1, p1, v1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public getActiveEnd()I
    .locals 1

    .prologue
    .line 467
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveEnd:I

    return v0
.end method

.method public getActiveStart()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveStart:I

    return v0
.end method

.method public getGenericFocusIndex()I
    .locals 1

    .prologue
    .line 503
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mGenericMotionFocus:I

    return v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 495
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSourceVersion:J

    return-wide v0
.end method

.method public isActive(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 182
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveStart:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveEnd:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->isLoading()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->terminate()V

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

    .line 165
    :cond_0
    return-void
.end method

.method public reload()V
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->notifyDirty()V

    .line 392
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v0, :cond_0

    .line 158
    :goto_0
    return-void

    .line 156
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

    const-class v1, Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;-><init>(Lcom/sec/android/gallery3d/app/AlbumDataLoader;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

    .line 157
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$ReloadTask;->start()V

    goto :goto_0
.end method

.method public setActiveWindow(II)V
    .locals 6
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v4, 0x0

    .line 234
    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveStart:I

    if-ne p1, v3, :cond_1

    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveEnd:I

    if-ne p2, v3, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    if-gt p1, p2, :cond_3

    sub-int v3, p2, p1

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v5, v5

    if-gt v3, v5, :cond_3

    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSize:I

    if-gt p2, v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 239
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v2, v3

    .line 240
    .local v2, "length":I
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveStart:I

    .line 241
    iput p2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mActiveEnd:I

    .line 244
    if-eq p1, p2, :cond_0

    .line 246
    add-int v3, p1, p2

    div-int/lit8 v3, v3, 0x2

    div-int/lit8 v5, v2, 0x2

    sub-int/2addr v3, v5

    iget v5, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSize:I

    sub-int/2addr v5, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v1

    .line 248
    .local v1, "contentStart":I
    add-int v3, v1, v2

    iget v4, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSize:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 249
    .local v0, "contentEnd":I
    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I

    if-gt v3, p1, :cond_2

    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentEnd:I

    if-lt v3, p2, :cond_2

    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I

    sub-int v3, v1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/16 v4, 0x20

    if-le v3, v4, :cond_0

    .line 251
    :cond_2
    invoke-direct {p0, v1, v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->setContentWindow(II)V

    goto :goto_0

    .end local v0    # "contentEnd":I
    .end local v1    # "contentStart":I
    .end local v2    # "length":I
    :cond_3
    move v3, v4

    .line 236
    goto :goto_1
.end method

.method public setDataListener(Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;

    .prologue
    .line 256
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mDataListener:Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;

    .line 257
    return-void
.end method

.method public setGenericFocusIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 499
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mGenericMotionFocus:I

    .line 500
    return-void
.end method

.method public setGroupIndex(J)V
    .locals 1
    .param p1, "groupIndex"    # J

    .prologue
    .line 529
    iput-wide p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mGroupIndex:J

    .line 530
    return-void
.end method

.method public setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/LoadingListener;

    .prologue
    .line 260
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    .line 261
    return-void
.end method

.method public setMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 0
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 463
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 464
    return-void
.end method

.method public setStartIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 144
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mStartIndex:I

    .line 145
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSize:I

    return v0
.end method

.method public updateAllItem()V
    .locals 2

    .prologue
    .line 477
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mDataListener:Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;

    if-eqz v1, :cond_0

    .line 478
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentEnd:I

    if-ge v0, v1, :cond_0

    .line 479
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mDataListener:Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;

    invoke-interface {v1, v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;->onContentChanged(I)V

    .line 478
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 481
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public updateItem(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 471
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mDataListener:Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mDataListener:Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;->onContentChanged(I)V

    .line 474
    :cond_0
    return-void
.end method

.method public updateLocalFaceImage(I)V
    .locals 3
    .param p1, "localImageId"    # I

    .prologue
    .line 484
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mDataListener:Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;

    if-eqz v2, :cond_1

    .line 485
    iget v1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentEnd:I

    if-ge v1, v2, :cond_1

    .line 486
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->get(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 487
    .local v0, "faceItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 488
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mDataListener:Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;

    invoke-interface {v2, v1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$DataListener;->onContentChanged(I)V

    .line 485
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 492
    .end local v0    # "faceItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v1    # "i":I
    :cond_1
    return-void
.end method
