.class final Lcom/sec/android/gallery3d/util/HdmiUtils$1;
.super Landroid/os/Handler;
.source "HdmiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/util/HdmiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 56
    # getter for: Lcom/sec/android/gallery3d/util/HdmiUtils;->mListeners:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/gallery3d/util/HdmiUtils;->access$000()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;

    .line 57
    .local v1, "listener":Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/HdmiUtils;->sIsPlugged:Z

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;->onHdmiConnect(Z)V

    goto :goto_0

    .line 59
    .end local v1    # "listener":Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;
    :cond_0
    return-void
.end method
