.class Lcom/sec/android/gallery3d/ui/ActionModeHandler$2;
.super Ljava/lang/Object;
.source "ActionModeHandler.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/ActionModeHandler;->updateSupportedOperation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/ActionModeHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/ActionModeHandler;)V
    .locals 0

    .prologue
    .line 406
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ActionModeHandler$2;->this$0:Lcom/sec/android/gallery3d/ui/ActionModeHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 406
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/ActionModeHandler$2;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;
    .locals 14
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/16 v13, 0x12c

    const/16 v3, 0xa

    const/4 v9, 0x1

    const/4 v1, 0x0

    const/4 v12, 0x0

    .line 410
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ActionModeHandler$2;->this$0:Lcom/sec/android/gallery3d/ui/ActionModeHandler;

    # invokes: Lcom/sec/android/gallery3d/ui/ActionModeHandler;->getSelectedMediaObjects(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/util/ArrayList;
    invoke-static {v2, p1}, Lcom/sec/android/gallery3d/ui/ActionModeHandler;->access$100(Lcom/sec/android/gallery3d/ui/ActionModeHandler;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/util/ArrayList;

    move-result-object v11

    .line 411
    .local v11, "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-nez v11, :cond_1

    .line 412
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ActionModeHandler$2;->this$0:Lcom/sec/android/gallery3d/ui/ActionModeHandler;

    # getter for: Lcom/sec/android/gallery3d/ui/ActionModeHandler;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/ActionModeHandler;->access$400(Lcom/sec/android/gallery3d/ui/ActionModeHandler;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/gallery3d/ui/ActionModeHandler$2$1;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/gallery3d/ui/ActionModeHandler$2$1;-><init>(Lcom/sec/android/gallery3d/ui/ActionModeHandler$2;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 480
    :cond_0
    :goto_0
    return-object v12

    .line 423
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ActionModeHandler$2;->this$0:Lcom/sec/android/gallery3d/ui/ActionModeHandler;

    # invokes: Lcom/sec/android/gallery3d/ui/ActionModeHandler;->computeMenuOptions(Ljava/util/ArrayList;)J
    invoke-static {v2, v11}, Lcom/sec/android/gallery3d/ui/ActionModeHandler;->access$500(Lcom/sec/android/gallery3d/ui/ActionModeHandler;Ljava/util/ArrayList;)J

    move-result-wide v4

    .line 424
    .local v4, "operation":J
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 427
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 428
    .local v0, "numSelected":I
    if-ge v0, v3, :cond_3

    move v6, v9

    .line 430
    .local v6, "canSharePanoramas":Z
    :goto_1
    if-ge v0, v13, :cond_4

    .line 433
    .local v9, "canShare":Z
    :goto_2
    if-eqz v6, :cond_5

    new-instance v7, Lcom/sec/android/gallery3d/ui/ActionModeHandler$GetAllPanoramaSupports;

    invoke-direct {v7, v11, p1}, Lcom/sec/android/gallery3d/ui/ActionModeHandler$GetAllPanoramaSupports;-><init>(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)V

    .line 438
    .local v7, "supportCallback":Lcom/sec/android/gallery3d/ui/ActionModeHandler$GetAllPanoramaSupports;
    :goto_3
    if-eqz v6, :cond_6

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ActionModeHandler$2;->this$0:Lcom/sec/android/gallery3d/ui/ActionModeHandler;

    # invokes: Lcom/sec/android/gallery3d/ui/ActionModeHandler;->computePanoramaSharingIntent(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;I)Landroid/content/Intent;
    invoke-static {v1, p1, v3}, Lcom/sec/android/gallery3d/ui/ActionModeHandler;->access$600(Lcom/sec/android/gallery3d/ui/ActionModeHandler;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;I)Landroid/content/Intent;

    move-result-object v8

    .line 441
    .local v8, "share_panorama_intent":Landroid/content/Intent;
    :goto_4
    if-eqz v9, :cond_7

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ActionModeHandler$2;->this$0:Lcom/sec/android/gallery3d/ui/ActionModeHandler;

    # invokes: Lcom/sec/android/gallery3d/ui/ActionModeHandler;->computeSharingIntent(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;I)Landroid/content/Intent;
    invoke-static {v1, p1, v13}, Lcom/sec/android/gallery3d/ui/ActionModeHandler;->access$700(Lcom/sec/android/gallery3d/ui/ActionModeHandler;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;I)Landroid/content/Intent;

    move-result-object v10

    .line 445
    .local v10, "share_intent":Landroid/content/Intent;
    :goto_5
    if-eqz v6, :cond_2

    .line 446
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/ActionModeHandler$GetAllPanoramaSupports;->waitForPanoramaSupport()V

    .line 448
    :cond_2
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 451
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ActionModeHandler$2;->this$0:Lcom/sec/android/gallery3d/ui/ActionModeHandler;

    # getter for: Lcom/sec/android/gallery3d/ui/ActionModeHandler;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/ActionModeHandler;->access$400(Lcom/sec/android/gallery3d/ui/ActionModeHandler;)Landroid/os/Handler;

    move-result-object v13

    new-instance v1, Lcom/sec/android/gallery3d/ui/ActionModeHandler$2$2;

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v1 .. v10}, Lcom/sec/android/gallery3d/ui/ActionModeHandler$2$2;-><init>(Lcom/sec/android/gallery3d/ui/ActionModeHandler$2;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;JZLcom/sec/android/gallery3d/ui/ActionModeHandler$GetAllPanoramaSupports;Landroid/content/Intent;ZLandroid/content/Intent;)V

    invoke-virtual {v13, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .end local v6    # "canSharePanoramas":Z
    .end local v7    # "supportCallback":Lcom/sec/android/gallery3d/ui/ActionModeHandler$GetAllPanoramaSupports;
    .end local v8    # "share_panorama_intent":Landroid/content/Intent;
    .end local v9    # "canShare":Z
    .end local v10    # "share_intent":Landroid/content/Intent;
    :cond_3
    move v6, v1

    .line 428
    goto :goto_1

    .restart local v6    # "canSharePanoramas":Z
    :cond_4
    move v9, v1

    .line 430
    goto :goto_2

    .restart local v9    # "canShare":Z
    :cond_5
    move-object v7, v12

    .line 433
    goto :goto_3

    .line 438
    .restart local v7    # "supportCallback":Lcom/sec/android/gallery3d/ui/ActionModeHandler$GetAllPanoramaSupports;
    :cond_6
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    goto :goto_4

    .line 441
    .restart local v8    # "share_panorama_intent":Landroid/content/Intent;
    :cond_7
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    goto :goto_5
.end method
