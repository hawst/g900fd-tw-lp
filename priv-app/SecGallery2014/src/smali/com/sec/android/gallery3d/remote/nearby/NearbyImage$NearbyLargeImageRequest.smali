.class Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$NearbyLargeImageRequest;
.super Ljava/lang/Object;
.source "NearbyImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NearbyLargeImageRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field private final mUrl:Ljava/net/URL;

.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;Ljava/net/URL;)V
    .locals 0
    .param p2, "url"    # Ljava/net/URL;

    .prologue
    .line 308
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$NearbyLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$NearbyLargeImageRequest;->mUrl:Ljava/net/URL;

    .line 310
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 6
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 314
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$NearbyLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$100(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDownloadCache()Lcom/sec/android/gallery3d/data/DownloadCache;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$NearbyLargeImageRequest;->mUrl:Ljava/net/URL;

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$NearbyLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->generateKey()J
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$000(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p1, v3, v4}, Lcom/sec/android/gallery3d/data/DownloadCache;->downloadNoNetworkProxy(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v1

    .line 315
    .local v1, "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    if-nez v1, :cond_1

    .line 316
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Nearby : download failed "

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    const/4 v0, 0x0

    .line 328
    :cond_0
    :goto_0
    return-object v0

    .line 320
    :cond_1
    iget-object v2, v1, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {p1, v2, v3}, Lcom/sec/android/gallery3d/data/DecodeUtils;->createBitmapRegionDecoder(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    .line 322
    .local v0, "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Nearby : downloaded large image "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->associateWith(Ljava/lang/Object;)V

    .line 325
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$NearbyLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getWidth()I

    move-result v3

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDecodedWidth:I
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$302(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;I)I

    .line 326
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$NearbyLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getHeight()I

    move-result v3

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDecodedHeight:I
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$402(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;I)I

    goto :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 305
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$NearbyLargeImageRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    return-object v0
.end method
