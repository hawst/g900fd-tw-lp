.class public abstract Lcom/sec/android/gallery3d/app/ActivityState;
.super Ljava/lang/Object;
.source "ActivityState.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;
    }
.end annotation


# static fields
.field protected static final FLAG_ALLOW_LOCK_WHILE_SCREEN_ON:I = 0x10

.field protected static final FLAG_HIDE_ACTION_BAR:I = 0x1

.field protected static final FLAG_HIDE_STATUS_BAR:I = 0x2

.field protected static final FLAG_SCREEN_ON_ALWAYS:I = 0x8

.field protected static final FLAG_SCREEN_ON_WHEN_PLUGGED:I = 0x4

.field protected static final FLAG_SHOW_WHEN_LOCKED:I = 0x20

.field public static final KEY_FROM_ALBUM_VIEW_STATE:Ljava/lang/String; = "KEY_FROM_ALBUM_VIEW_STATE"

.field public static final KEY_FROM_SPC_GALLERY_WIDGET:Ljava/lang/String; = "KEY_FROM_SPC_GALLERY_WIDGET"

.field public static final KEY_GROUP_INDEX:Ljava/lang/String; = "KEY_GROUP_INDEX"

.field public static final KEY_ITEM_IS_HIDDEN:Ljava/lang/String; = "KEY_ITEM_IS_HIDDEN"

.field public static final KEY_ITEM_POSITION:Ljava/lang/String; = "KEY_ITEM_POSITION"

.field public static final KEY_ITEM_RCS:Ljava/lang/String; = "KEY_ITEM_RCS"

.field public static final KEY_MAP_VIEW_ZOOM_LEVEL:Ljava/lang/String; = "KEY_MAP_VIEW_ZOOM_LEVEL"

.field public static final KEY_MEDIA_ITEM_LOCATION:Ljava/lang/String; = "KEY_MEDIA_ITEM_LOCATION"

.field public static final KEY_MEDIA_ITEM_PATH:Ljava/lang/String; = "KEY_MEDIA_ITEM_PATH"

.field public static final KEY_MEDIA_SET_PATH:Ljava/lang/String; = "KEY_MEDIA_SET_PATH"

.field public static final KEY_MEDIA_SET_POSITION:Ljava/lang/String; = "KEY_MEDIA_SET_POSITION"

.field protected static final KEY_MEDIA_SET_POSITION_INIT:I = 0x0

.field protected static final KEY_MEDIA_SET_POSITION_INVALDE:I = -0x1

.field public static final KEY_NEW_ALBUM_NAME:Ljava/lang/String; = "KEY_NEW_ALBUM_NAME"

.field public static final KEY_NEW_ALBUM_PATH:Ljava/lang/String; = "KEY_NEW_ALBUM_PATH"

.field public static final KEY_NOITEMSVIEW_MIME_TYPE:Ljava/lang/String; = "KEY_NOITEMSVIEW_MIME_TYPE"

.field public static final KEY_NO_SPLIT_MODE:Ljava/lang/String; = "KEY_NO_SPLIT_MODE"

.field public static final KEY_PICK_MEDIA_TYPE:Ljava/lang/String; = "KEY_PICK_MEDIA_TYPE"

.field public static final KEY_SPC_DMR_MODE:Ljava/lang/String; = "KEY_SPC_DMR_MODE"

.field public static final KEY_SPC_GALLERY_WIDGET_MAX_CNT:Ljava/lang/String; = "KEY_SPC_GALLERY_WIDGET_MAX_CNT"

.field public static final KEY_SPC_NATIVE_PLAYER_MODE:Ljava/lang/String; = "KEY_SPC_NATIVE_PLAYER_MODE"

.field private static final KEY_TRANSITION_IN:Ljava/lang/String; = "transition-in"

.field public static final KEY_VIEW_BY:Ljava/lang/String; = "KEY_VIEW_BY"

.field public static final KEY_VIEW_FACE:Ljava/lang/String; = "KEY_VIEW_FACE"

.field public static final KEY_VIEW_REDRAW:Ljava/lang/String; = "KEY_VIEW_REDRAW"


# instance fields
.field protected mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

.field protected mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field protected mBackgroundColor:[F

.field protected mBundle:Landroid/os/Bundle;

.field private mContentPane:Lcom/sec/android/gallery3d/ui/GLView;

.field protected mData:Landroid/os/Bundle;

.field private mDestroyed:Z

.field protected mFlags:I

.field private mIntroAnimation:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

.field mIsFinishing:Z

.field private mNextTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

.field private mPlugged:Z

.field mPowerIntentReceiver:Landroid/content/BroadcastReceiver;

.field protected mReceivedResults:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

.field protected mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

.field private mSConnectManager:Lcom/sec/android/gallery3d/sconnect/SConnectManager;

.field private onConfigurationChangedListenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/app/GalleryEvent$OnConfigurationChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private onPauseListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;",
            ">;"
        }
    .end annotation
.end field

.field private onResumeListenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;",
            ">;"
        }
    .end annotation
.end field

.field private onStateResultListenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/app/GalleryEvent$OnStateResultListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mDestroyed:Z

    .line 78
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mPlugged:Z

    .line 79
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mIsFinishing:Z

    .line 83
    sget-object v0, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;->None:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mNextTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    .line 90
    new-instance v0, Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-direct {v0, v1, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 126
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onPauseListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onResumeListenerList:Ljava/util/ArrayList;

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onStateResultListenerList:Ljava/util/ArrayList;

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChangedListenerList:Ljava/util/ArrayList;

    .line 202
    new-instance v0, Lcom/sec/android/gallery3d/app/ActivityState$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/ActivityState$1;-><init>(Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mPowerIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 135
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/ActivityState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ActivityState;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mPlugged:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/app/ActivityState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ActivityState;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mPlugged:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/ActivityState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ActivityState;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->setScreenFlags()V

    return-void
.end method

.method private postOnConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 474
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChangedListenerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryEvent$OnConfigurationChangedListener;

    .line 475
    .local v1, "listener":Lcom/sec/android/gallery3d/app/GalleryEvent$OnConfigurationChangedListener;
    invoke-interface {v1, p1}, Lcom/sec/android/gallery3d/app/GalleryEvent$OnConfigurationChangedListener;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0

    .line 477
    .end local v1    # "listener":Lcom/sec/android/gallery3d/app/GalleryEvent$OnConfigurationChangedListener;
    :cond_0
    return-void
.end method

.method private postOnPause()V
    .locals 3

    .prologue
    .line 462
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onPauseListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;

    .line 463
    .local v1, "listener":Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;
    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;->onPause()V

    goto :goto_0

    .line 465
    .end local v1    # "listener":Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;
    :cond_0
    return-void
.end method

.method private postOnResume()V
    .locals 3

    .prologue
    .line 468
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onResumeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;

    .line 469
    .local v1, "listener":Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;
    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;->onResume()V

    goto :goto_0

    .line 471
    .end local v1    # "listener":Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;
    :cond_0
    return-void
.end method

.method private setScreenFlags()V
    .locals 4

    .prologue
    .line 218
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 219
    .local v1, "win":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 220
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    iget v2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mFlags:I

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mPlugged:Z

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mFlags:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    .line 222
    :cond_0
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v2, v2, 0x80

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 226
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mFlags:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_2

    .line 227
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 231
    :goto_1
    iget v2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mFlags:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_3

    .line 232
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v3, 0x80000

    or-int/2addr v2, v3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 236
    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 237
    return-void

    .line 224
    :cond_1
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v2, v2, -0x81

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_0

    .line 229
    :cond_2
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit8 v2, v2, -0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_1

    .line 234
    :cond_3
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, -0x80001

    and-int/2addr v2, v3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_2
.end method


# virtual methods
.method protected clearStateResult()V
    .locals 0

    .prologue
    .line 200
    return-void
.end method

.method configurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 170
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 171
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->postOnConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 172
    return-void
.end method

.method protected finalizeView()V
    .locals 0

    .prologue
    .line 400
    return-void
.end method

.method public getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method protected getBackgroundColor()[F
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mBackgroundColor:[F

    return-object v0
.end method

.method protected getBackgroundColorId()I
    .locals 1

    .prologue
    .line 187
    const/high16 v0, 0x7f0b0000

    return v0
.end method

.method public getContentsForDetailsDialog()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 372
    const/4 v0, 0x0

    return-object v0
.end method

.method public getContentsForFaceTag()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 380
    const/4 v0, 0x0

    return-object v0
.end method

.method public getData()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mData:Landroid/os/Bundle;

    return-object v0
.end method

.method public getMediaForDetails()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSupportMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method initialize(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 149
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mData:Landroid/os/Bundle;

    .line 150
    new-instance v0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/sconnect/SConnectManager;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mSConnectManager:Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    .line 151
    return-void
.end method

.method protected initializeView()V
    .locals 0

    .prologue
    .line 397
    return-void
.end method

.method isDestroyed()Z
    .locals 1

    .prologue
    .line 351
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mDestroyed:Z

    return v0
.end method

.method public isFinishing()Z
    .locals 1

    .prologue
    .line 355
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mIsFinishing:Z

    return v0
.end method

.method protected onBackPressed()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 159
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 176
    return-void
.end method

.method public onCoverModeChanged()V
    .locals 0

    .prologue
    .line 412
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->getBackgroundColorId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->intColorToFloatARGBArray(I)[F

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mBackgroundColor:[F

    .line 197
    return-void
.end method

.method protected onCreateActionBar(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 337
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 415
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mSConnectManager:Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->InitQuickConnectManager()V

    .line 346
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mSConnectManager:Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    .line 347
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mDestroyed:Z

    .line 348
    return-void
.end method

.method public onDirty()V
    .locals 0

    .prologue
    .line 391
    return-void
.end method

.method public onDirty(Z)V
    .locals 0
    .param p1, "isExitSelectionMode"    # Z

    .prologue
    .line 394
    return-void
.end method

.method public onGalleryEventReceived(Lcom/sec/android/gallery3d/app/GalleryEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/sec/android/gallery3d/app/GalleryEvent;

    .prologue
    .line 427
    return-void
.end method

.method protected onItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 341
    const/4 v0, 0x0

    return v0
.end method

.method public onMWLayoutChanged()V
    .locals 0

    .prologue
    .line 403
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 0
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 421
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 264
    iget v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mFlags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mPowerIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mNextTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    sget-object v1, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;->None:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    if-eq v0, v1, :cond_1

    .line 268
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getTransitionStore()Lcom/sec/android/gallery3d/app/TransitionStore;

    move-result-object v0

    const-string/jumbo v1, "transition-in"

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mNextTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/app/TransitionStore;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mContentPane:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PreparePageFadeoutTexture;->prepareFadeOutTexture(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 270
    sget-object v0, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;->None:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mNextTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mSConnectManager:Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->unregisterReceiver()V

    .line 274
    return-void
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 482
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 418
    return-void
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 406
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 322
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getTransitionStore()Lcom/sec/android/gallery3d/app/TransitionStore;

    move-result-object v1

    const-string v2, "fade_texture"

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/TransitionStore;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    .line 324
    .local v0, "fade":Lcom/sec/android/gallery3d/glrenderer/RawTexture;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getTransitionStore()Lcom/sec/android/gallery3d/app/TransitionStore;

    move-result-object v1

    const-string/jumbo v2, "transition-in"

    sget-object v3, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;->None:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/app/TransitionStore;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mNextTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    .line 326
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mNextTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    sget-object v2, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;->None:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    if-eq v1, v2, :cond_0

    .line 327
    new-instance v1, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mNextTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;-><init>(Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;Lcom/sec/android/gallery3d/glrenderer/RawTexture;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mIntroAnimation:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    .line 328
    sget-object v1, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;->None:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mNextTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    .line 331
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mSConnectManager:Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->registerReceiver()V

    .line 332
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 409
    return-void
.end method

.method protected onSaveState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 179
    return-void
.end method

.method protected onStateResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 182
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 424
    return-void
.end method

.method pause()V
    .locals 0

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onPause()V

    .line 259
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->postOnPause()V

    .line 260
    return-void
.end method

.method protected performHapticFeedback(I)V
    .locals 2
    .param p1, "feedbackConstant"    # I

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/view/View;->performHapticFeedback(II)Z

    .line 253
    return-void
.end method

.method public registerOnConfigurationChangedListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnConfigurationChangedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/GalleryEvent$OnConfigurationChangedListener;

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChangedListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 443
    return-void
.end method

.method public registerOnPauseListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;

    .prologue
    .line 430
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onPauseListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 431
    return-void
.end method

.method public registerOnResumeListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;

    .prologue
    .line 434
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onResumeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    return-void
.end method

.method public registerOnStateResultListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnStateResultListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/GalleryEvent$OnStateResultListener;

    .prologue
    .line 438
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onStateResultListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 439
    return-void
.end method

.method resume()V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 278
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 279
    .local v1, "activity":Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 280
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_1

    .line 281
    iget v6, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mFlags:I

    and-int/lit8 v6, v6, 0x1

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCoverMode(Landroid/app/Activity;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 282
    :cond_0
    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 286
    :goto_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getStateCount()I

    move-result v5

    .line 287
    .local v5, "stateCount":I
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryActionBar()Lcom/sec/android/gallery3d/app/GalleryActionBar;

    move-result-object v9

    if-le v5, v7, :cond_5

    move v6, v7

    :goto_1
    invoke-virtual {v9, v6, v7}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->setDisplayOptions(ZZ)V

    .line 289
    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 292
    .end local v5    # "stateCount":I
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 293
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->setScreenFlags()V

    .line 295
    iget v6, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mFlags:I

    and-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_6

    move v4, v7

    .line 296
    .local v4, "lightsOut":Z
    :goto_2
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v6

    invoke-interface {v6, v4}, Lcom/sec/android/gallery3d/ui/GLRoot;->setLightsOutMode(Z)V

    .line 298
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mReceivedResults:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    .line 299
    .local v2, "entry":Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;
    if-eqz v2, :cond_2

    .line 300
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mReceivedResults:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    .line 301
    iget v6, v2, Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;->requestCode:I

    iget v7, v2, Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;->resultCode:I

    iget-object v8, v2, Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;->resultData:Landroid/content/Intent;

    invoke-virtual {p0, v6, v7, v8}, Lcom/sec/android/gallery3d/app/ActivityState;->onStateResult(IILandroid/content/Intent;)V

    .line 304
    :cond_2
    iget v6, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mFlags:I

    and-int/lit8 v6, v6, 0x4

    if-eqz v6, :cond_3

    .line 306
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 307
    .local v3, "filter":Landroid/content/IntentFilter;
    const-string v6, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v3, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 308
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mPowerIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v6, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 311
    .end local v3    # "filter":Landroid/content/IntentFilter;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onResume()V

    .line 313
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->postOnResume()V

    .line 317
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getTransitionStore()Lcom/sec/android/gallery3d/app/TransitionStore;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/TransitionStore;->clear()V

    .line 318
    return-void

    .line 284
    .end local v2    # "entry":Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;
    .end local v4    # "lightsOut":Z
    :cond_4
    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_0

    .restart local v5    # "stateCount":I
    :cond_5
    move v6, v8

    .line 287
    goto :goto_1

    .end local v5    # "stateCount":I
    :cond_6
    move v4, v8

    .line 295
    goto :goto_2
.end method

.method public setActionBarManager(Lcom/sec/samsung/gallery/view/ActionBarManager;)V
    .locals 0
    .param p1, "actionBarManager"    # Lcom/sec/samsung/gallery/view/ActionBarManager;

    .prologue
    .line 368
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 369
    return-void
.end method

.method protected setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V
    .locals 2
    .param p1, "content"    # Lcom/sec/android/gallery3d/ui/GLView;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mContentPane:Lcom/sec/android/gallery3d/ui/GLView;

    .line 139
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mIntroAnimation:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mContentPane:Lcom/sec/android/gallery3d/ui/GLView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mIntroAnimation:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/GLView;->setIntroAnimation(Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;)V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mIntroAnimation:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mContentPane:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->getBackgroundColor()[F

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/GLView;->setBackgroundColor([F)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mContentPane:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 145
    return-void
.end method

.method public setFaceTagParam(Landroid/content/Intent;)V
    .locals 0
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 384
    return-void
.end method

.method public setPromptRequest(IILjava/lang/Object;)V
    .locals 0
    .param p1, "code"    # I
    .param p2, "parm"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    .line 388
    return-void
.end method

.method protected setStateResult(ILandroid/content/Intent;)V
    .locals 1
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    if-nez v0, :cond_0

    .line 165
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    iput p1, v0, Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;->resultCode:I

    .line 164
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    iput-object p2, v0, Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;->resultData:Landroid/content/Intent;

    goto :goto_0
.end method

.method protected transitionOnNextPause(Ljava/lang/Class;Ljava/lang/Class;Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;)V
    .locals 1
    .param p3, "hint"    # Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;",
            "Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 246
    .local p1, "outgoing":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    .local p2, "incoming":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    sget-object v0, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;->None:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->mNextTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation$Transition;

    .line 248
    return-void
.end method

.method public unregisterOnConfigurationChangedListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnConfigurationChangedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/GalleryEvent$OnConfigurationChangedListener;

    .prologue
    .line 458
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChangedListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 459
    return-void
.end method

.method public unregisterOnPauseListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;

    .prologue
    .line 446
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onPauseListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 447
    return-void
.end method

.method public unregisterOnResumeListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onResumeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 451
    return-void
.end method

.method public unregisterOnStateResultListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnStateResultListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/GalleryEvent$OnStateResultListener;

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ActivityState;->onStateResultListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 455
    return-void
.end method
