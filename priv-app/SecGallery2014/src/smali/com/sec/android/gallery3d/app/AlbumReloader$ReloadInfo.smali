.class public Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
.super Ljava/lang/Object;
.source "AlbumReloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/AlbumReloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ReloadInfo"
.end annotation


# instance fields
.field public size:I

.field public version:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    return-void
.end method

.method public constructor <init>(JI)V
    .locals 1
    .param p1, "version"    # J
    .param p3, "size"    # I

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-wide p1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->version:J

    .line 111
    iput p3, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->size:I

    .line 112
    return-void
.end method
