.class public Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;
.super Ljava/lang/Object;
.source "DsWallpaperSetting.java"


# static fields
.field private static mSetAsSim1:Z

.field private static mSetAsSim2:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    sput-boolean v0, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 33
    sput-boolean v0, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static clearSetting()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v0, :cond_1

    .line 47
    :cond_0
    sput-boolean v1, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 48
    sput-boolean v1, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    .line 50
    :cond_1
    return-void
.end method

.method private static getTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v1, :cond_1

    .line 37
    :cond_0
    const-string v0, "DsWallpaperSetting"

    .line 39
    .local v0, "TAG":Ljava/lang/String;
    const-string v1, "DsWallpaperSetting"

    .line 41
    :goto_0
    return-object v1

    .end local v0    # "TAG":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSetAsSIM1()Z
    .locals 1

    .prologue
    .line 165
    sget-boolean v0, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    return v0
.end method

.method public static isSetAsSIM2()Z
    .locals 1

    .prologue
    .line 169
    sget-boolean v0, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    return v0
.end method

.method public static selectSimByNetwork(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 53
    const/4 v0, 0x0

    .line 55
    .local v0, "isSelected":Z
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v1, :cond_1

    .line 56
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->clearSetting()V

    .line 58
    invoke-static {p0}, Lcom/sec/android/gallery3d/ds/SimInformation;->getSimState(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 59
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->isDualSim()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 90
    :cond_1
    :goto_0
    return v0

    .line 62
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getCurrentSim()I

    move-result v1

    if-nez v1, :cond_3

    .line 63
    sput-boolean v3, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 64
    sput-boolean v4, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    .line 70
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 66
    :cond_3
    sput-boolean v4, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 67
    sput-boolean v3, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    goto :goto_1

    .line 72
    :cond_4
    invoke-static {p0}, Lcom/sec/android/gallery3d/ds/SimInformation;->getSimState(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v3, :cond_6

    .line 73
    invoke-static {p0}, Lcom/sec/android/gallery3d/ds/SimInformation;->getCurrentSimSlot(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_5

    .line 74
    sput-boolean v3, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 75
    sput-boolean v4, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    .line 81
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 77
    :cond_5
    sput-boolean v4, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 78
    sput-boolean v3, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    goto :goto_2

    .line 83
    :cond_6
    sput-boolean v3, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 84
    sput-boolean v4, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    .line 86
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static selectSimByUser(I)V
    .locals 3
    .param p0, "command"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 94
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v0, :cond_1

    .line 96
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->clearSetting()V

    .line 98
    if-nez p0, :cond_2

    .line 99
    sput-boolean v1, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 100
    sput-boolean v1, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    .line 112
    :cond_1
    :goto_0
    return-void

    .line 101
    :cond_2
    if-ne p0, v1, :cond_3

    .line 102
    sput-boolean v1, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 103
    sput-boolean v2, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    goto :goto_0

    .line 104
    :cond_3
    const/4 v0, 0x2

    if-ne p0, v0, :cond_4

    .line 105
    sput-boolean v2, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 106
    sput-boolean v1, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    goto :goto_0

    .line 108
    :cond_4
    sput-boolean v1, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 109
    sput-boolean v2, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    goto :goto_0
.end method

.method public static setAsDsWallpaper(Landroid/content/Context;Landroid/graphics/Bitmap;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "wallpaper"    # Landroid/graphics/Bitmap;

    .prologue
    .line 115
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsMultiSIM:Z

    if-eqz v4, :cond_1

    .line 116
    :cond_0
    const-string v0, "setBitmap"

    .line 118
    .local v0, "METHOD_SET_BITMAP":Ljava/lang/String;
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 162
    .end local v0    # "METHOD_SET_BITMAP":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 120
    .restart local v0    # "METHOD_SET_BITMAP":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    .line 123
    .local v1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 124
    const-string v4, "setBitmap"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/graphics/Bitmap;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 125
    .local v3, "method":Ljava/lang/reflect/Method;
    sget-boolean v4, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    if-eqz v4, :cond_3

    .line 129
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    :cond_3
    sget-boolean v4, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    if-eqz v4, :cond_4

    .line 136
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    .line 154
    .end local v3    # "method":Ljava/lang/reflect/Method;
    :cond_4
    :goto_1
    sget-boolean v4, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim1:Z

    if-nez v4, :cond_1

    sget-boolean v4, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->mSetAsSim2:Z

    if-nez v4, :cond_1

    .line 156
    :try_start_1
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/app/WallpaperManager;->setBitmap(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 157
    :catch_0
    move-exception v2

    .line 158
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 138
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 139
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    invoke-static {}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "there is no setBitmap"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 141
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v2

    .line 142
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-static {}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string v5, "illegal argument of setBitmap"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 144
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v2

    .line 145
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-static {}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string v5, "illegal access of setBitmap"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 147
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v2

    .line 148
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-static {}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "worng invocation target of setBitmap"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1

    .line 150
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_5
    move-exception v2

    .line 151
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
