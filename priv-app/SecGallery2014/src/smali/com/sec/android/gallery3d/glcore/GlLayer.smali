.class public Lcom/sec/android/gallery3d/glcore/GlLayer;
.super Ljava/lang/Object;
.source "GlLayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;
    }
.end annotation


# static fields
.field private static final DEF_CHILD_COUNT:I = 0x4

.field public static final LAYER_STATE_ACTIVE:I = 0x1

.field public static final LAYER_STATE_NONE:I = 0x0

.field public static final LAYER_STATE_PAUSED:I = 0x2

.field private static final TAG:Ljava/lang/String; = "GlLayer"


# instance fields
.field protected isSlideShowActive:Z

.field protected isSlideShowMode:Z

.field public mAnimation:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlAnimationBase;",
            ">;"
        }
    .end annotation
.end field

.field public mChildCount:I

.field public mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

.field public mCommand:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlCmd;",
            ">;"
        }
    .end annotation
.end field

.field public mCunsumeScale:Z

.field public mDistance:F

.field public mFov:F

.field public mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field public mGlSubObject:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ">;"
        }
    .end annotation
.end field

.field public mHeight:I

.field public mHeightSpace:F

.field private mLayerCallback:Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;

.field mMDetectorListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

.field mMHoverDetectorListener:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

.field private mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

.field private mMoveHoverDetect:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

.field public mParent:Lcom/sec/android/gallery3d/glcore/GlLayer;

.field public mPendingLayerAnimation:Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;

.field public mState:I

.field public mWideMode:Z

.field public mWidth:I

.field public mWidthSpace:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 31
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlSubObject:Ljava/util/ArrayList;

    .line 32
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mParent:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 33
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 34
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    .line 36
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mFov:F

    .line 37
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mDistance:F

    .line 38
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    .line 39
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    .line 41
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCunsumeScale:Z

    .line 42
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    .line 45
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    .line 46
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveHoverDetect:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    .line 47
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mLayerCallback:Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;

    .line 48
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mPendingLayerAnimation:Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;

    .line 523
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlLayer$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlLayer$1;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMDetectorListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    .line 562
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlLayer$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlLayer$2;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMHoverDetectorListener:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

    .line 735
    return-void
.end method


# virtual methods
.method public addLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 5
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v3, 0x4

    const/4 v4, 0x0

    .line 51
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-nez v2, :cond_1

    .line 52
    new-array v2, v3, [Lcom/sec/android/gallery3d/glcore/GlLayer;

    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 60
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    aput-object p1, v2, v3

    .line 61
    iput-object p0, p1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mParent:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 62
    return-void

    .line 53
    :cond_1
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    if-ne v2, v3, :cond_0

    .line 54
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    int-to-float v2, v2

    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 56
    .local v0, "newLen":I
    new-array v1, v0, [Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 57
    .local v1, "temp":[Lcom/sec/android/gallery3d/glcore/GlLayer;
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 58
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    goto :goto_0
.end method

.method public addObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 2
    .param p1, "glObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v1, 0x1

    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlSubObject:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlSubObject:Ljava/util/ArrayList;

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlSubObject:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    :goto_0
    return-void

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    goto :goto_0
.end method

.method public create()V
    .locals 4

    .prologue
    .line 636
    const-string v1, "GlLayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "create = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    if-nez v1, :cond_1

    .line 638
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    .line 640
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onCreate()V

    .line 641
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v1, :cond_0

    .line 642
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    if-ge v0, v1, :cond_0

    .line 643
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->create()V

    .line 642
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 646
    .end local v0    # "i":I
    :cond_0
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMDetectorListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    .line 647
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMHoverDetectorListener:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveHoverDetect:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    .line 648
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mLayerCallback:Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;

    if-eqz v1, :cond_1

    .line 649
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mLayerCallback:Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;

    invoke-interface {v1, p0}, Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 652
    :cond_1
    return-void
.end method

.method public createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .locals 1
    .param p1, "virtualDescendantId"    # I

    .prologue
    .line 750
    const/4 v0, 0x0

    return-object v0
.end method

.method public destroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 655
    const-string v1, "GlLayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "destroy = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    if-eqz v1, :cond_1

    .line 657
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    .line 659
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v1, :cond_0

    .line 660
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 661
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->destroy()V

    .line 660
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 664
    .end local v0    # "i":I
    :cond_0
    iput-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    .line 665
    iput-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveHoverDetect:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    .line 666
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onDestroy()V

    .line 668
    :cond_1
    return-void
.end method

.method public dispatchDragEvent(Landroid/view/DragEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 319
    const/4 v1, 0x0

    .line 321
    .local v1, "retVal":Z
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 322
    const/4 v2, 0x0

    .line 328
    :goto_0
    return v2

    .line 324
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_1

    .line 325
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    .line 324
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 327
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onDragEvent(Landroid/view/DragEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    move v2, v1

    .line 328
    goto :goto_0
.end method

.method public dispatchGenericMotionCancel()V
    .locals 3

    .prologue
    .line 422
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 429
    :goto_0
    return-void

    .line 425
    :cond_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_1

    .line 426
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchGenericMotionCancel()V

    .line 425
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 428
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onGenericMotionCancel()V

    goto :goto_0
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 303
    const/4 v1, 0x0

    .line 305
    .local v1, "retVal":Z
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 306
    const/4 v2, 0x0

    .line 315
    :goto_0
    return v2

    .line 308
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_1

    .line 309
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    .line 308
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 311
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveHoverDetect:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    if-nez v2, :cond_2

    move v2, v1

    .line 312
    goto :goto_0

    .line 314
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveHoverDetect:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->onHover(Landroid/view/MotionEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    move v2, v1

    .line 315
    goto :goto_0
.end method

.method public dispatchKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 332
    const/4 v1, 0x0

    .line 334
    .local v1, "retVal":Z
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 335
    const/4 v2, 0x0

    .line 341
    :goto_0
    return v2

    .line 337
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_1

    .line 338
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    .line 337
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 340
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    move v2, v1

    .line 341
    goto :goto_0
.end method

.method public dispatchKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 358
    const/4 v1, 0x0

    .line 360
    .local v1, "retVal":Z
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 361
    const/4 v2, 0x0

    .line 367
    :goto_0
    return v2

    .line 363
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_1

    .line 364
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    .line 363
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 366
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    move v2, v1

    .line 367
    goto :goto_0
.end method

.method public dispatchKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 345
    const/4 v1, 0x0

    .line 347
    .local v1, "retVal":Z
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 348
    const/4 v2, 0x0

    .line 354
    :goto_0
    return v2

    .line 350
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_1

    .line 351
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    .line 350
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 353
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    move v2, v1

    .line 354
    goto :goto_0
.end method

.method public dispatchRotation(Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;)V
    .locals 3
    .param p1, "rotationDetector"    # Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;

    .prologue
    .line 412
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 419
    :goto_0
    return-void

    .line 415
    :cond_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_1

    .line 416
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchRotation(Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;)V

    .line 415
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 418
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onRotation(Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;)V

    goto :goto_0
.end method

.method public dispatchScale(Landroid/view/ScaleGestureDetector;)V
    .locals 3
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 387
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 390
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_2

    .line 391
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchScale(Landroid/view/ScaleGestureDetector;)V

    .line 390
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 393
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCunsumeScale:Z

    if-eqz v1, :cond_0

    .line 394
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onScale(Landroid/view/ScaleGestureDetector;)V

    goto :goto_0
.end method

.method public dispatchScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 5
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v2, 0x0

    .line 371
    const/4 v1, 0x0

    .line 373
    .local v1, "retVal":Z
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 374
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCunsumeScale:Z

    .line 383
    :goto_0
    return v2

    .line 378
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_1

    .line 379
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    move-result v2

    or-int/2addr v1, v2

    .line 378
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 381
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCunsumeScale:Z

    .line 382
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCunsumeScale:Z

    or-int/2addr v1, v2

    move v2, v1

    .line 383
    goto :goto_0
.end method

.method public dispatchScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 3
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 399
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 409
    :cond_0
    :goto_0
    return-void

    .line 402
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_2

    .line 403
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchScaleEnd(Landroid/view/ScaleGestureDetector;)V

    .line 402
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 405
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCunsumeScale:Z

    if-eqz v1, :cond_0

    .line 406
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCunsumeScale:Z

    .line 407
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onScaleEnd(Landroid/view/ScaleGestureDetector;)V

    goto :goto_0
.end method

.method public dispatchSurfaceChanged(IIZZ)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "wideMode"    # Z
    .param p4, "surfaceChanged"    # Z

    .prologue
    .line 275
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidth:I

    .line 276
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeight:I

    .line 277
    iput-boolean p3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWideMode:Z

    .line 278
    if-eqz p4, :cond_0

    .line 279
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onSurfaceChanged(II)V

    .line 281
    :cond_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 282
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchSurfaceChanged(IIZZ)V

    .line 281
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 284
    :cond_1
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 287
    const/4 v1, 0x0

    .line 289
    .local v1, "retVal":Z
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 290
    const/4 v2, 0x0

    .line 299
    :goto_0
    return v2

    .line 292
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_1

    .line 293
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    .line 292
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 295
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    move v2, v1

    .line 296
    goto :goto_0

    .line 298
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->onTouch(Landroid/view/MotionEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    move v2, v1

    .line 299
    goto :goto_0
.end method

.method public getActiveObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 153
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChild(I)Lcom/sec/android/gallery3d/glcore/GlLayer;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    if-lt p1, v0, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 730
    const/4 v0, -0x1

    return v0
.end method

.method public getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 599
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    goto :goto_0
.end method

.method public getHeightSpace()F
    .locals 1

    .prologue
    .line 607
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    return v0
.end method

.method protected getHoverListScrollMode()I
    .locals 1

    .prologue
    .line 474
    const/4 v0, 0x0

    return v0
.end method

.method public getLayerCallback()Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mLayerCallback:Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;

    return-object v0
.end method

.method public getParent()Lcom/sec/android/gallery3d/glcore/GlLayer;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mParent:Lcom/sec/android/gallery3d/glcore/GlLayer;

    return-object v0
.end method

.method public getRootObject()Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 146
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTopObjectList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v4, :cond_1

    .line 159
    const/4 v2, 0x0

    .line 169
    :cond_0
    return-object v2

    .line 160
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v3, v4, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 162
    .local v3, "rootObj":Lcom/sec/android/gallery3d/glcore/GlObject;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 163
    .local v2, "objList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/glcore/GlObject;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v4, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-ge v1, v4, :cond_0

    .line 164
    iget-object v4, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v0, v4, v1

    .line 165
    .local v0, "childObj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v4, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-ne v4, p0, :cond_2

    .line 166
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 594
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    goto :goto_0
.end method

.method public getWidthSpace()F
    .locals 1

    .prologue
    .line 603
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    return v0
.end method

.method public isActive()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 710
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCreated()Z
    .locals 1

    .prologue
    .line 706
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPaused()Z
    .locals 2

    .prologue
    .line 714
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelectedAt(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 463
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveHoverDetect:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 464
    :cond_0
    const/4 v0, 0x0

    .line 466
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveHoverDetect:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->isSelectedAt(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public isSlideShowActive(Z)V
    .locals 0
    .param p1, "mode"    # Z

    .prologue
    .line 699
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->isSlideShowActive:Z

    .line 700
    return-void
.end method

.method public lockRefresh()V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_0

    .line 267
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRefresh()V

    goto :goto_0
.end method

.method protected onClicked()Z
    .locals 1

    .prologue
    .line 448
    const/4 v0, 0x0

    return v0
.end method

.method protected onCommand(ILjava/lang/Object;III)V
    .locals 0
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    .line 503
    return-void
.end method

.method protected onCreate()V
    .locals 0

    .prologue
    .line 718
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 721
    return-void
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 470
    const/4 v0, 0x0

    return v0
.end method

.method protected onGenericMotionCancel()V
    .locals 0

    .prologue
    .line 490
    return-void
.end method

.method protected onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 478
    const/4 v0, 0x0

    return v0
.end method

.method protected onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 486
    const/4 v0, 0x0

    return v0
.end method

.method protected onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 482
    const/4 v0, 0x0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changeSize"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 506
    return-void
.end method

.method protected onLongClicked(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 452
    const/4 v0, 0x0

    return v0
.end method

.method protected onMoved(II)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 436
    const/4 v0, 0x0

    return v0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 727
    return-void
.end method

.method protected onPressed(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 432
    const/4 v0, 0x0

    return v0
.end method

.method protected onReleased(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 444
    const/4 v0, 0x0

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 724
    return-void
.end method

.method public onRotation(Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;)V
    .locals 0
    .param p1, "rotationDetector"    # Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;

    .prologue
    .line 456
    return-void
.end method

.method protected onScale(Landroid/view/ScaleGestureDetector;)V
    .locals 0
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 493
    return-void
.end method

.method protected onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 496
    const/4 v0, 0x0

    return v0
.end method

.method protected onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 0
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 500
    return-void
.end method

.method protected onScrolled(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    .line 440
    const/4 v0, 0x0

    return v0
.end method

.method protected onSurfaceChanged(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 509
    return-void
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 459
    const/4 v0, 0x0

    return v0
.end method

.method public pause()V
    .locals 4

    .prologue
    .line 671
    const-string v1, "GlLayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pause = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 673
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    .line 675
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v1, :cond_0

    .line 676
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 677
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->pause()V

    .line 676
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 680
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onPause()V

    .line 682
    :cond_1
    return-void
.end method

.method public performAction(IILandroid/os/Bundle;)Z
    .locals 1
    .param p1, "virtualViewId"    # I
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 754
    const/4 v0, 0x0

    return v0
.end method

.method public removeAllObject()V
    .locals 4

    .prologue
    .line 117
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v3, :cond_3

    .line 118
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlSubObject:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlSubObject:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_0

    .line 124
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlSubObject:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 125
    .local v2, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-eqz v2, :cond_2

    .line 126
    sget v3, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_DESTROYING:I

    iput v3, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    .line 127
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeAllListeners()V

    .line 128
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeAllAnimation()V

    .line 129
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->onDestroy()V

    .line 130
    sget v3, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_DESTROYED:I

    iput v3, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    .line 132
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlSubObject:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    .end local v2    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :goto_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 133
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 138
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAllObject()V

    goto :goto_0
.end method

.method public removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 1
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mAnimation:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_0
.end method

.method public removeCommand(I)V
    .locals 3
    .param p1, "command"    # I

    .prologue
    const/4 v2, 0x0

    .line 258
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_0

    .line 262
    :goto_0
    return-void

    .line 259
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    invoke-direct {v0, p1, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIII)V

    .line 260
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 261
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    goto :goto_0
.end method

.method public removeObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 2
    .param p1, "glObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_2

    .line 99
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlSubObject:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    sget v1, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_CREATED:I

    if-ne v0, v1, :cond_0

    .line 105
    sget v0, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_DESTROYING:I

    iput v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    .line 106
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeAllListeners()V

    .line 107
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeAllAnimation()V

    .line 108
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->onDestroy()V

    .line 109
    sget v0, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_DESTROYED:I

    iput v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    .line 110
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlSubObject:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 112
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    goto :goto_0
.end method

.method public resume()V
    .locals 4

    .prologue
    .line 685
    const-string v1, "GlLayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resume = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 687
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    .line 688
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onResume()V

    .line 690
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v1, :cond_0

    .line 691
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    if-ge v0, v1, :cond_0

    .line 692
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->resume()V

    .line 691
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 696
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 1
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 173
    iput-object p0, p1, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 174
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mAnimation:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mAnimation:Ljava/util/ArrayList;

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mAnimation:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    :goto_0
    return-void

    .line 179
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_0
.end method

.method public setBgResource(I)V
    .locals 1
    .param p1, "rsrcID"    # I

    .prologue
    .line 631
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_0

    .line 633
    :goto_0
    return-void

    .line 632
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setBgResource(I)V

    goto :goto_0
.end method

.method public setClearByColor(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 621
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_0

    .line 623
    :goto_0
    return-void

    .line 622
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setClearByColor(Z)V

    goto :goto_0
.end method

.method public setCommand(IIII)V
    .locals 2
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I

    .prologue
    .line 194
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIII)V

    .line 195
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_LAYER:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 196
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 197
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_1

    .line 198
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCommand:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 199
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCommand:Ljava/util/ArrayList;

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCommand:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    :goto_0
    return-void

    .line 202
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    goto :goto_0
.end method

.method public setCommand(IIIILjava/lang/Object;)V
    .locals 6
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 209
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIILjava/lang/Object;)V

    .line 210
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_LAYER:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 211
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 212
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_1

    .line 213
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCommand:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 214
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCommand:Ljava/util/ArrayList;

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCommand:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    :goto_0
    return-void

    .line 217
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    goto :goto_0
.end method

.method public setCommandDelayed(IIIIJ)V
    .locals 9
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "msecDelay"    # J

    .prologue
    .line 225
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    add-long v6, v2, p5

    .line 226
    .local v6, "expTime":J
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIIJ)V

    .line 227
    .local v1, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_LAYER:I

    iput v0, v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 228
    iput-object p0, v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 229
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCommand:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 231
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCommand:Ljava/util/ArrayList;

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCommand:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    goto :goto_0
.end method

.method public setCommandDelayed(IIIILjava/lang/Object;J)V
    .locals 8
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "obj"    # Ljava/lang/Object;
    .param p6, "msecDelay"    # J

    .prologue
    .line 242
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    add-long v6, v2, p6

    .line 243
    .local v6, "expTime":J
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIILjava/lang/Object;J)V

    .line 244
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_LAYER:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 245
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 246
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_1

    .line 247
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCommand:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 248
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCommand:Ljava/util/ArrayList;

    .line 249
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mCommand:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    :goto_0
    return-void

    .line 251
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    goto :goto_0
.end method

.method public setGlBackgroundColor(FFFF)V
    .locals 1
    .param p1, "red"    # F
    .param p2, "green"    # F
    .param p3, "blue"    # F
    .param p4, "alpha"    # F

    .prologue
    .line 626
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_0

    .line 628
    :goto_0
    return-void

    .line 627
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setGlBackgroundColor(FFFF)V

    goto :goto_0
.end method

.method public setHoverScrollFlexibleHeightMargin(F)V
    .locals 3
    .param p1, "topGlMargin"    # F

    .prologue
    .line 742
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    if-eqz v1, :cond_0

    .line 743
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeight:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    div-float/2addr v1, v2

    float-to-int v0, v1

    .line 744
    .local v0, "topMargin":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mMoveHoverDetect:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->setHoverScrollFlexibleHeightMargin(I)V

    .line 746
    .end local v0    # "topMargin":I
    :cond_0
    return-void
.end method

.method public setLayerCallback(Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;

    .prologue
    .line 512
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mLayerCallback:Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;

    .line 513
    return-void
.end method

.method public setNextImage(I)V
    .locals 1
    .param p1, "rsrcID"    # I

    .prologue
    .line 611
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_0

    .line 613
    :goto_0
    return-void

    .line 612
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setNextImage(I)V

    goto :goto_0
.end method

.method public setNextImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 616
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_0

    .line 618
    :goto_0
    return-void

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setNextImage(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setPendingLayerAnimation(Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;

    .prologue
    .line 520
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mPendingLayerAnimation:Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;

    .line 521
    return-void
.end method

.method public setRootView(Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 2
    .param p1, "rootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 65
    if-nez p1, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->unlockRefresh()V

    .line 67
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    if-ge v0, v1, :cond_1

    .line 68
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildLayer:[Lcom/sec/android/gallery3d/glcore/GlLayer;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->setRootView(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_1
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 71
    return-void
.end method

.method public setSlideShowMode(Z)V
    .locals 0
    .param p1, "mode"    # Z

    .prologue
    .line 703
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->isSlideShowMode:Z

    .line 704
    return-void
.end method

.method public unlockRefresh()V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_0

    .line 272
    :goto_0
    return-void

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRefresh()V

    goto :goto_0
.end method
