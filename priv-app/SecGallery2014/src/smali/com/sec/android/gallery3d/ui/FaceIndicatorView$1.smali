.class Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;
.super Landroid/os/Handler;
.source "FaceIndicatorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, -0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 174
    iget v6, p1, Landroid/os/Message;->what:I

    .line 176
    .local v6, "what":I
    packed-switch v6, :pswitch_data_0

    .line 224
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 225
    return-void

    .line 178
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 179
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "contact_popup_pos"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v3

    .line 180
    .local v3, "pos":[I
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string/jumbo v8, "show"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 181
    .local v5, "show":Z
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "head_pos"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 182
    .local v0, "headPos":[I
    if-eqz v3, :cond_2

    if-eqz v5, :cond_2

    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/PositionController;->inSlidingAnimation()Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/ContactPopup;->isMoreDialogShow()Z

    move-result v7

    if-nez v7, :cond_2

    .line 184
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 187
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    aget v7, v3, v12

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 188
    aget v7, v3, v11

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 189
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 190
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ActionBar;->isShowing()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 191
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v4, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 192
    .local v4, "screenHeight":I
    iget v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupHeight:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$300(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v8

    add-int v2, v7, v8

    .line 193
    .local v2, "popupBottom":I
    if-ge v4, v2, :cond_1

    .line 194
    iget v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    sub-int v8, v2, v4

    sub-int/2addr v7, v8

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 198
    .end local v2    # "popupBottom":I
    .end local v4    # "screenHeight":I
    :cond_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 199
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v7

    aget v8, v3, v11

    aget v9, v0, v11

    aget v10, v0, v12

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setHeadPosition(III)V

    .line 200
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    aget v8, v3, v11

    aget v9, v0, v11

    aget v10, v0, v12

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setHeadPos(III)V

    .line 201
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v7

    invoke-virtual {v7, v12}, Lcom/sec/android/gallery3d/ui/ContactPopup;->show(Z)V

    .line 202
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/ContactPopup;->invalidate()V

    .line 203
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    aget v8, v3, v11

    aget v9, v3, v12

    # invokes: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkNameTagOverlap(II)Z
    invoke-static {v7, v8, v9}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$400(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;II)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 204
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    goto/16 :goto_0

    .line 206
    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v7

    invoke-virtual {v7, v11}, Lcom/sec/android/gallery3d/ui/ContactPopup;->show(Z)V

    goto/16 :goto_0

    .line 212
    .end local v0    # "headPos":[I
    .end local v3    # "pos":[I
    .end local v5    # "show":Z
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # invokes: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkContactData(Z)V
    invoke-static {v7, v12}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$500(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Z)V

    .line 213
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    goto/16 :goto_0

    .line 216
    :pswitch_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$600(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 217
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$600(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;

    move-result-object v8

    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-interface {v8, v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;->onUpdate(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 219
    :cond_3
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    goto/16 :goto_0

    .line 176
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
