.class public Lcom/sec/android/gallery3d/sconnect/SConnectManager;
.super Ljava/lang/Object;
.source "SConnectManager.java"


# static fields
.field private static final ACTION_SCONNECT_START:Ljava/lang/String; = "com.samsung.android.sconnect.START"

.field private static final TAG:Ljava/lang/String;

.field private static sIsBezelSupported:Z


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mIsRegistered:Z

.field private mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

.field private mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->TAG:Ljava/lang/String;

    .line 38
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->sIsBezelSupported:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v1, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 40
    iput-object v1, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 43
    sget-boolean v1, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->sIsBezelSupported:Z

    if-nez v1, :cond_0

    .line 97
    :goto_0
    return-void

    .line 47
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 50
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v2, "quickconnect"

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/quickconnect/QuickConnectManager;

    iput-object v1, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    new-instance v1, Lcom/sec/android/gallery3d/sconnect/SConnectManager$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/sconnect/SConnectManager$1;-><init>(Lcom/sec/android/gallery3d/sconnect/SConnectManager;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    sget-object v1, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->TAG:Ljava/lang/String;

    const-string v2, "bezel action not supported"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->sIsBezelSupported:Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/sconnect/SConnectManager;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Landroid/app/Activity;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 26
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->startSConnect(Landroid/app/Activity;Ljava/util/ArrayList;)V

    return-void
.end method

.method private static startSConnect(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.sconnect.START"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 135
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 136
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 138
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :goto_0
    return-void

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const v2, 0x7f0e0140

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 141
    sget-object v2, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->TAG:Ljava/lang/String;

    const-string v3, "S Connect is not found"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public InitQuickConnectManager()V
    .locals 2

    .prologue
    .line 129
    sget-object v0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->TAG:Ljava/lang/String;

    const-string v1, "init mQuickConnectManager"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 131
    return-void
.end method

.method public registerReceiver()V
    .locals 3

    .prologue
    .line 100
    sget-boolean v0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->sIsBezelSupported:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mIsRegistered:Z

    if-eqz v0, :cond_2

    .line 101
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bezel error - supported : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->sIsBezelSupported:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", already registered : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mIsRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_1
    :goto_0
    return-void

    .line 105
    :cond_2
    sget-object v0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->TAG:Ljava/lang/String;

    const-string v1, "register listener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mIsRegistered:Z

    .line 107
    iget-object v0, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->registerListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    goto :goto_0
.end method

.method public unregisterReceiver()V
    .locals 3

    .prologue
    .line 113
    sget-boolean v0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->sIsBezelSupported:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mIsRegistered:Z

    if-nez v0, :cond_1

    .line 114
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bezel error - supported : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->sIsBezelSupported:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", already registered : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mIsRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :goto_0
    return-void

    .line 118
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "unregister listener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    iget-object v0, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    if-eqz v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 125
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mIsRegistered:Z

    goto :goto_0
.end method
