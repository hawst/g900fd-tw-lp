.class public Lcom/sec/android/gallery3d/gadget/LocalImagesSource;
.super Ljava/lang/Object;
.source "LocalImagesSource.java"

# interfaces
.implements Lcom/sec/android/gallery3d/gadget/WidgetSource;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAppWidgetId:I

.field private mCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private mContentDirty:Z

.field private mContentListener:Lcom/sec/android/gallery3d/data/ContentListener;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mFilePathList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNonLocalItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetId"    # I

    .prologue
    const/4 v4, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mCache:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mNonLocalItemList:Ljava/util/ArrayList;

    .line 52
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentDirty:Z

    .line 55
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContext:Landroid/content/Context;

    .line 56
    iput p2, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mAppWidgetId:I

    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 58
    new-instance v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource$1;

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource$1;-><init>(Lcom/sec/android/gallery3d/gadget/LocalImagesSource;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentObserver:Landroid/database/ContentObserver;

    .line 66
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mCache:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mNonLocalItemList:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1, v4}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->updateCache(Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    .line 69
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/gadget/LocalImagesSource;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/LocalImagesSource;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentDirty:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/gadget/LocalImagesSource;)Lcom/sec/android/gallery3d/data/ContentListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentListener:Lcom/sec/android/gallery3d/data/ContentListener;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static convertPathToFilePath(Lcom/sec/android/gallery3d/data/DataManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6
    .param p0, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/DataManager;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 236
    .local v1, "filePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_1

    .line 245
    :cond_0
    return-object v1

    .line 239
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/Path;

    .line 240
    .local v4, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 241
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v5, v3, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 242
    .local v0, "filePath":Ljava/lang/String;
    :goto_1
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 241
    .end local v0    # "filePath":Ljava/lang/String;
    :cond_2
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private getMediaPathId(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 136
    :try_start_0
    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 137
    .local v1, "strId":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 139
    .end local v1    # "strId":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 138
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static makeWidgetFileName(I)Ljava/lang/String;
    .locals 3
    .param p0, "appWidgetId"    # I

    .prologue
    .line 353
    new-instance v0, Ljava/lang/StringBuffer;

    const-string/jumbo v1, "widget_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 354
    .local v0, "buff":Ljava/lang/StringBuffer;
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ".paths"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 355
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static readWidgetPaths(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 293
    .local v8, "returnPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->makeWidgetFileName(I)Ljava/lang/String;

    move-result-object v1

    .line 294
    .local v1, "fileName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 295
    .local v2, "inStream":Ljava/io/FileInputStream;
    const/4 v4, 0x0

    .line 297
    .local v4, "objInStream":Ljava/io/ObjectInputStream;
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v2

    .line 298
    new-instance v5, Ljava/io/ObjectInputStream;

    invoke-direct {v5, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 302
    .end local v4    # "objInStream":Ljava/io/ObjectInputStream;
    .local v5, "objInStream":Ljava/io/ObjectInputStream;
    :try_start_1
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 303
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<*>;"
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 304
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    .line 305
    .local v6, "objectPath":Ljava/lang/Object;
    instance-of v9, v6, Lcom/sec/android/gallery3d/data/Path;

    if-eqz v9, :cond_1

    .line 306
    move-object v7, v3

    .line 307
    .local v7, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v9

    invoke-static {v9, v7}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->convertPathToFilePath(Lcom/sec/android/gallery3d/data/DataManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    .line 310
    invoke-static {p0, p1, v8}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->saveWidgetPaths(Landroid/content/Context;ILjava/util/ArrayList;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 322
    .end local v6    # "objectPath":Ljava/lang/Object;
    .end local v7    # "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_0
    :goto_0
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 323
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v4, v5

    .line 326
    .end local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<*>;"
    .end local v5    # "objInStream":Ljava/io/ObjectInputStream;
    .restart local v4    # "objInStream":Ljava/io/ObjectInputStream;
    :goto_1
    return-object v8

    .line 312
    .end local v4    # "objInStream":Ljava/io/ObjectInputStream;
    .restart local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<*>;"
    .restart local v5    # "objInStream":Ljava/io/ObjectInputStream;
    .restart local v6    # "objectPath":Ljava/lang/Object;
    :cond_1
    move-object v8, v3

    goto :goto_0

    .line 315
    .end local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<*>;"
    .end local v5    # "objInStream":Ljava/io/ObjectInputStream;
    .end local v6    # "objectPath":Ljava/lang/Object;
    .restart local v4    # "objInStream":Ljava/io/ObjectInputStream;
    :catch_0
    move-exception v0

    .line 316
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_2
    sget-object v9, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->TAG:Ljava/lang/String;

    const-string v10, "file not found while reading widget path : "

    invoke-static {v9, v10, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 322
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 323
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .line 317
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 318
    .local v0, "e":Ljava/io/IOException;
    :goto_3
    :try_start_3
    sget-object v9, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->TAG:Ljava/lang/String;

    const-string v10, "IO exception : "

    invoke-static {v9, v10, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 322
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 323
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .line 319
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 320
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    :goto_4
    :try_start_4
    sget-object v9, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->TAG:Ljava/lang/String;

    const-string v10, "Class not found : "

    invoke-static {v9, v10, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 322
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 323
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .line 322
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catchall_0
    move-exception v9

    :goto_5
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 323
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v9

    .line 322
    .end local v4    # "objInStream":Ljava/io/ObjectInputStream;
    .restart local v5    # "objInStream":Ljava/io/ObjectInputStream;
    :catchall_1
    move-exception v9

    move-object v4, v5

    .end local v5    # "objInStream":Ljava/io/ObjectInputStream;
    .restart local v4    # "objInStream":Ljava/io/ObjectInputStream;
    goto :goto_5

    .line 319
    .end local v4    # "objInStream":Ljava/io/ObjectInputStream;
    .restart local v5    # "objInStream":Ljava/io/ObjectInputStream;
    :catch_3
    move-exception v0

    move-object v4, v5

    .end local v5    # "objInStream":Ljava/io/ObjectInputStream;
    .restart local v4    # "objInStream":Ljava/io/ObjectInputStream;
    goto :goto_4

    .line 317
    .end local v4    # "objInStream":Ljava/io/ObjectInputStream;
    .restart local v5    # "objInStream":Ljava/io/ObjectInputStream;
    :catch_4
    move-exception v0

    move-object v4, v5

    .end local v5    # "objInStream":Ljava/io/ObjectInputStream;
    .restart local v4    # "objInStream":Ljava/io/ObjectInputStream;
    goto :goto_3

    .line 315
    .end local v4    # "objInStream":Ljava/io/ObjectInputStream;
    .restart local v5    # "objInStream":Ljava/io/ObjectInputStream;
    :catch_5
    move-exception v0

    move-object v4, v5

    .end local v5    # "objInStream":Ljava/io/ObjectInputStream;
    .restart local v4    # "objInStream":Ljava/io/ObjectInputStream;
    goto :goto_2
.end method

.method public static removeFile(Landroid/content/Context;I)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 348
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->makeWidgetFileName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 349
    .local v0, "fileName":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->removeFile(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private static removeFile(Ljava/lang/String;)Z
    .locals 3
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 340
    const/4 v1, 0x1

    .line 341
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 342
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 343
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    .line 344
    :cond_0
    return v1
.end method

.method public static saveWidgetPaths(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 258
    .local p2, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 259
    sget-object v5, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "invalid widget id : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :goto_0
    return-void

    .line 263
    :cond_0
    invoke-static {p1}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->makeWidgetFileName(I)Ljava/lang/String;

    move-result-object v1

    .line 264
    .local v1, "fileName":Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->removeFile(Landroid/content/Context;I)Z

    .line 266
    const/4 v4, 0x0

    .line 267
    .local v4, "outStream":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 269
    .local v2, "objOutStream":Ljava/io/ObjectOutputStream;
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0, v1, v5}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    .line 270
    new-instance v3, Ljava/io/ObjectOutputStream;

    invoke-direct {v3, v4}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    .end local v2    # "objOutStream":Ljava/io/ObjectOutputStream;
    .local v3, "objOutStream":Ljava/io/ObjectOutputStream;
    :try_start_1
    invoke-virtual {v3, p2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 277
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 278
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v2, v3

    .line 279
    .end local v3    # "objOutStream":Ljava/io/ObjectOutputStream;
    .restart local v2    # "objOutStream":Ljava/io/ObjectOutputStream;
    goto :goto_0

    .line 272
    :catch_0
    move-exception v0

    .line 273
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_2
    sget-object v5, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->TAG:Ljava/lang/String;

    const-string v6, "file not found while writing widget path : "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 277
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 278
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .line 274
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 275
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    sget-object v5, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->TAG:Ljava/lang/String;

    const-string v6, "IO exception : "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 277
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 278
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .line 277
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 278
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v5

    .line 277
    .end local v2    # "objOutStream":Ljava/io/ObjectOutputStream;
    .restart local v3    # "objOutStream":Ljava/io/ObjectOutputStream;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "objOutStream":Ljava/io/ObjectOutputStream;
    .restart local v2    # "objOutStream":Ljava/io/ObjectOutputStream;
    goto :goto_3

    .line 274
    .end local v2    # "objOutStream":Ljava/io/ObjectOutputStream;
    .restart local v3    # "objOutStream":Ljava/io/ObjectOutputStream;
    :catch_2
    move-exception v0

    move-object v2, v3

    .end local v3    # "objOutStream":Ljava/io/ObjectOutputStream;
    .restart local v2    # "objOutStream":Ljava/io/ObjectOutputStream;
    goto :goto_2

    .line 272
    .end local v2    # "objOutStream":Ljava/io/ObjectOutputStream;
    .restart local v3    # "objOutStream":Ljava/io/ObjectOutputStream;
    :catch_3
    move-exception v0

    move-object v2, v3

    .end local v3    # "objOutStream":Ljava/io/ObjectOutputStream;
    .restart local v2    # "objOutStream":Ljava/io/ObjectOutputStream;
    goto :goto_1
.end method

.method private updateCache(Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V
    .locals 17
    .param p3, "checkExistence"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "cache":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .local p2, "notLocalItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 146
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mAppWidgetId:I

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->readWidgetPaths(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mFilePathList:Ljava/util/ArrayList;

    .line 147
    const/4 v7, 0x0

    .line 148
    .local v7, "cursor":Landroid/database/Cursor;
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 150
    .local v13, "idMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :try_start_0
    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "_data"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 153
    if-eqz v7, :cond_4

    .line 154
    :cond_0
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 155
    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 156
    .local v12, "id":I
    const/4 v2, 0x1

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 157
    .local v9, "filePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mFilePathList:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 158
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v13, v9, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 162
    .end local v9    # "filePath":Ljava/lang/String;
    .end local v12    # "id":I
    :catch_0
    move-exception v2

    .line 164
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 167
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mFilePathList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 168
    .restart local v9    # "filePath":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->getMediaPathId(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    .line 169
    .local v12, "id":Ljava/lang/Integer;
    if-eqz v12, :cond_5

    .line 170
    const/4 v14, 0x1

    .line 171
    .local v14, "isValid":Z
    if-eqz p3, :cond_3

    .line 172
    const/4 v11, 0x0

    .line 173
    .local v11, "iStream":Ljava/io/InputStream;
    const/16 v16, 0x0

    .line 175
    .local v16, "uri":Landroid/net/Uri;
    :try_start_1
    invoke-static {v9}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v15

    .line 176
    .local v15, "path":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v2, v15}, Lcom/sec/android/gallery3d/data/DataManager;->getContentUri(Lcom/sec/android/gallery3d/data/Path;)Landroid/net/Uri;

    move-result-object v16

    .line 177
    if-eqz v16, :cond_2

    .line 178
    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v11

    .line 184
    :cond_2
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 187
    .end local v11    # "iStream":Ljava/io/InputStream;
    .end local v15    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v16    # "uri":Landroid/net/Uri;
    :cond_3
    :goto_3
    if-eqz v14, :cond_1

    .line 188
    invoke-static {v9, v12}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 164
    .end local v9    # "filePath":Ljava/lang/String;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v12    # "id":Ljava/lang/Integer;
    .end local v14    # "isValid":Z
    :cond_4
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 180
    .restart local v9    # "filePath":Ljava/lang/String;
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v11    # "iStream":Ljava/io/InputStream;
    .restart local v12    # "id":Ljava/lang/Integer;
    .restart local v14    # "isValid":Z
    .restart local v16    # "uri":Landroid/net/Uri;
    :catch_1
    move-exception v8

    .line 182
    .local v8, "e":Ljava/lang/Exception;
    const/4 v14, 0x0

    .line 184
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_3

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v2

    .line 192
    .end local v11    # "iStream":Ljava/io/InputStream;
    .end local v14    # "isValid":Z
    .end local v16    # "uri":Landroid/net/Uri;
    :cond_5
    invoke-interface {v13, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "id":Ljava/lang/Integer;
    check-cast v12, Ljava/lang/Integer;

    .line 193
    .restart local v12    # "id":Ljava/lang/Integer;
    if-eqz v12, :cond_1

    .line 194
    invoke-static {v9, v12}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 198
    .end local v9    # "filePath":Ljava/lang/String;
    .end local v12    # "id":Ljava/lang/Integer;
    :cond_6
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 230
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentObserver:Landroid/database/ContentObserver;

    .line 232
    :cond_0
    return-void
.end method

.method public getContentUri(I)Landroid/net/Uri;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getImage(I)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 82
    .local v0, "image":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_0

    .line 83
    sget-object v1, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->TAG:Ljava/lang/String;

    const-string v2, "image is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    const/4 v1, 0x0

    .line 89
    :goto_0
    return-object v1

    .line 86
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v2

    const-wide/16 v4, 0x20

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 87
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200a7

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    .line 89
    :cond_1
    invoke-static {v0}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->createSamsungWidgetBitmap(Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 94
    const/4 v3, 0x0

    .line 95
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v6, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mCache:Ljava/util/ArrayList;

    monitor-enter v6

    .line 96
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mCache:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt p1, v5, :cond_0

    const/4 v5, 0x0

    monitor-exit v6

    .line 116
    :goto_0
    return-object v5

    .line 99
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mCache:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 100
    .local v2, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 101
    .local v0, "filePath":Ljava/lang/String;
    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    .line 103
    .local v1, "id":Ljava/lang/Integer;
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->getMediaPathId(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 105
    .local v4, "pathId":Ljava/lang/Integer;
    if-eqz v4, :cond_2

    .line 106
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v5, v7, :cond_1

    .line 107
    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 114
    :goto_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v5, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0

    .line 109
    :cond_1
    :try_start_1
    sget-object v5, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    goto :goto_1

    .line 112
    :cond_2
    sget-object v5, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    goto :goto_1

    .line 114
    .end local v0    # "filePath":Ljava/lang/String;
    .end local v1    # "id":Ljava/lang/Integer;
    .end local v2    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v4    # "pathId":Ljava/lang/Integer;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5
.end method

.method public isDrm(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 126
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v0

    return v0
.end method

.method public reload()V
    .locals 6

    .prologue
    .line 202
    sget-object v4, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->TAG:Ljava/lang/String;

    const-string v5, "reload"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentDirty:Z

    if-nez v4, :cond_0

    .line 224
    :goto_0
    return-void

    .line 205
    :cond_0
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentDirty:Z

    .line 208
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 210
    .local v2, "token":J
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v0, "cache":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 212
    .local v1, "notLocalItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x1

    invoke-direct {p0, v0, v1, v4}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->updateCache(Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    .line 214
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mCache:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 215
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mCache:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 216
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mNonLocalItemList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 218
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mCache:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 219
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mNonLocalItemList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 220
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 222
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 220
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 222
    .end local v0    # "cache":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .end local v1    # "notLocalItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_1
    move-exception v4

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v4
.end method

.method public setContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/data/ContentListener;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentListener:Lcom/sec/android/gallery3d/data/ContentListener;

    .line 132
    return-void
.end method

.method public size()I
    .locals 2

    .prologue
    .line 73
    sget-object v0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "size()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mCache:Ljava/util/ArrayList;

    monitor-enter v1

    .line 75
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
