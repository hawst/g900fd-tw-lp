.class public Lcom/sec/android/gallery3d/remote/sns/SNSSource;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSource;
.source "SNSSource.java"


# static fields
.field public static final ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final IMAGE_MEDIA_ID:I = 0x1

.field private static final MAP_BATCH_COUNT:I = 0x64

.field public static final MERGE_ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final NO_MATCH:I = -0x1

.field private static final SNS_ALBUM:I = 0x1

.field private static final SNS_ALBUMSET:I = 0x0

.field private static final SNS_IMAGE_ALBUM:I = 0x2

.field private static final SNS_ITEM:I = 0x4

.field private static final SNS_MERGESET:I = 0x5

.field private static final SNS_MERGE_ALL_ALBUM:I = 0x6

.field private static final SNS_MERGE_IMAGE_ALBUM:I = 0x7

.field private static final SNS_MERGE_VIDEO_ALBUM:I = 0x8

.field private static final SNS_VIDEO_ALBUM:I = 0x3

.field private static final TAG:Ljava/lang/String; = "SNSSource"

.field private static accountsResult:[Landroid/accounts/Account;


# instance fields
.field private mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 64
    const-string v0, "/sns/all"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/sns/mergedall/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->SNS_MERGESET_BUCKETID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->MERGE_ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 203
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->accountsResult:[Landroid/accounts/Account;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 7
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x5

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 68
    const-string/jumbo v0, "sns"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/remote/RemoteMediaSource;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 62
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mUriMatcher:Landroid/content/UriMatcher;

    .line 70
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/all"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/image"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/video"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/all/*"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/video/*"

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/image/*"

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/mergedall"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/mergedimage"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/mergedvideo"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/mergedall/*"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/mergedvideo/*"

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/mergedimage/*"

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/item/*"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/all/*/*"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/image/*/*"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/sns/video/*/*"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.gallery3d.sns.contentprovider"

    const-string v2, "photos/#"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.gallery3d.provider"

    const-string/jumbo v2, "sns/item/#"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 90
    return-void
.end method

.method public static isSNSAccountsActive(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 209
    :try_start_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 210
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    const-string v2, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    sput-object v2, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->accountsResult:[Landroid/accounts/Account;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    .end local v0    # "accountManager":Landroid/accounts/AccountManager;
    :goto_0
    sget-object v2, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->accountsResult:[Landroid/accounts/Account;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->accountsResult:[Landroid/accounts/Account;

    array-length v2, v2

    if-eqz v2, :cond_0

    .line 215
    const/4 v2, 0x1

    .line 216
    :goto_1
    return v2

    .line 211
    :catch_0
    move-exception v1

    .line 212
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 216
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static isSNSDataExist(Landroid/content/Context;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 230
    sget-object v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    .line 232
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 233
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v7, 0x0

    .line 236
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "cache_status >= ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v11, 0x2

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 240
    if-nez v7, :cond_1

    .line 241
    const-string v2, "SNSSource"

    const-string v3, "cannot open sns database"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 255
    if-eqz v7, :cond_0

    .line 256
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_0
    move v2, v9

    :goto_0
    return v2

    .line 245
    :cond_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 246
    .local v6, "count":I
    if-lez v6, :cond_3

    .line 247
    const-string v2, "SNSSource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cached : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 255
    if-eqz v7, :cond_2

    .line 256
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    move v2, v10

    goto :goto_0

    .line 255
    :cond_3
    if-eqz v7, :cond_4

    .line 256
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    move v2, v9

    goto :goto_0

    .line 251
    .end local v6    # "count":I
    :catch_0
    move-exception v8

    .line 252
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "SNSSource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 255
    if-eqz v7, :cond_5

    .line 256
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    move v2, v9

    goto :goto_0

    .line 255
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v7, :cond_6

    .line 256
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v2
.end method

.method public static isSNSImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 1
    .param p0, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 262
    instance-of v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v0, :cond_0

    .line 263
    const/4 v0, 0x1

    .line 265
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 94
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 126
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bad path: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :pswitch_0
    new-instance v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 122
    :goto_0
    return-object v0

    .line 98
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getLongVar(I)J

    move-result-wide v2

    invoke-static {p1, v0, v2, v3, v6}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;JI)Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    move-result-object v0

    goto :goto_0

    .line 102
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getLongVar(I)J

    move-result-wide v2

    invoke-static {p1, v0, v2, v3, v4}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;JI)Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    move-result-object v0

    goto :goto_0

    .line 106
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getLongVar(I)J

    move-result-wide v2

    invoke-static {p1, v0, v2, v3, v5}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;JI)Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    move-result-object v0

    goto :goto_0

    .line 110
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getLongVar(I)J

    move-result-wide v2

    invoke-static {p1, v0, v2, v3}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;J)Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    move-result-object v0

    goto :goto_0

    .line 112
    :pswitch_5
    new-instance v0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    goto :goto_0

    .line 114
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getLongVar(I)J

    move-result-wide v2

    invoke-static {p1, v0, v2, v3, v6}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;JI)Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    move-result-object v0

    goto :goto_0

    .line 118
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getLongVar(I)J

    move-result-wide v2

    invoke-static {p1, v0, v2, v3, v4}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;JI)Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    move-result-object v0

    goto :goto_0

    .line 122
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getLongVar(I)J

    move-result-wide v2

    invoke-static {p1, v0, v2, v3, v5}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;JI)Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    move-result-object v0

    goto :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 133
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 141
    :goto_0
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 135
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_1

    .line 138
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v1, "SNSSource"

    const-string/jumbo v2, "uri: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 133
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getDefaultSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 146
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    .line 147
    .local v1, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v2, v1, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v2, :cond_1

    .line 148
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v2, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->MERGE_ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 149
    .local v0, "albumPath":Lcom/sec/android/gallery3d/data/Path;
    :goto_0
    check-cast v1, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .end local v1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getAlbumId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 151
    .end local v0    # "albumPath":Lcom/sec/android/gallery3d/data/Path;
    :goto_1
    return-object v2

    .line 148
    .restart local v1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0

    .line 151
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getTotalTargetCacheSize()J
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->getTotalTargetCacheSize(Landroid/content/ContentResolver;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalUsedCacheSize()J
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->getTotalUsedCacheSize(Landroid/content/ContentResolver;)J

    move-result-wide v0

    return-wide v0
.end method

.method public mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V
    .locals 12
    .param p2, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSource$PathId;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 169
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/16 v11, 0x1f4

    if-ge v10, v11, :cond_1

    .line 170
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSource;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    .line 201
    :cond_0
    return-void

    .line 175
    :cond_1
    sget-object v10, Lcom/sec/android/gallery3d/data/LocalSource;->sIdComparator:Ljava/util/Comparator;

    invoke-static {p1, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 177
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 178
    .local v8, "n":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v8, :cond_0

    .line 180
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 181
    .local v4, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .line 183
    .local v0, "count":I
    move v6, v1

    .local v6, "j":I
    :goto_1
    if-ge v6, v8, :cond_2

    .line 184
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 185
    .local v9, "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget-object v10, v9, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 186
    .local v2, "curId":J
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    add-int/lit8 v0, v0, 0x1

    const/16 v10, 0x64

    if-ne v0, v10, :cond_3

    .line 191
    .end local v2    # "curId":J
    .end local v9    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    :cond_2
    iget-object v10, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v10, v4}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->getMediaItemById(Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/util/ArrayList;)[Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    .line 193
    .local v5, "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    move v7, v1

    .local v7, "k":I
    :goto_2
    if-ge v7, v6, :cond_4

    .line 194
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 195
    .restart local v9    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget v10, v9, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->id:I

    sub-int v11, v7, v1

    aget-object v11, v5, v11

    invoke-interface {p2, v10, v11}, Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;->consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 193
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 183
    .end local v5    # "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v7    # "k":I
    .restart local v2    # "curId":J
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 199
    .end local v2    # "curId":J
    .end local v9    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .restart local v5    # "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v7    # "k":I
    :cond_4
    move v1, v6

    .line 200
    goto :goto_0
.end method

.method public pause()V
    .locals 0

    .prologue
    .line 227
    return-void
.end method

.method public requestSyncAll()V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->startSync(Landroid/content/Context;)V

    .line 271
    return-void
.end method

.method public resume()V
    .locals 0

    .prologue
    .line 222
    return-void
.end method
