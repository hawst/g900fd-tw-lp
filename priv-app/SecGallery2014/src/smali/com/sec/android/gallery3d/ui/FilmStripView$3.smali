.class Lcom/sec/android/gallery3d/ui/FilmStripView$3;
.super Lcom/sec/android/gallery3d/ui/SlotView$SimpleListener;
.source "FilmStripView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/FilmStripView;->initializeViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/FilmStripView;)V
    .locals 0

    .prologue
    .line 679
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/SlotView$SimpleListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 683
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mLockTouchEvent:Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$400(Lcom/sec/android/gallery3d/ui/FilmStripView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 687
    :cond_0
    :goto_0
    return-void

    .line 685
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mHelpMode:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$500(Lcom/sec/android/gallery3d/ui/FilmStripView;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 686
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # invokes: Lcom/sec/android/gallery3d/ui/FilmStripView;->onDown(I)V
    invoke-static {v0, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$600(Lcom/sec/android/gallery3d/ui/FilmStripView;I)V

    goto :goto_0
.end method

.method public onLongTap(I)V
    .locals 1
    .param p1, "slotIndex"    # I

    .prologue
    .line 707
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mLockTouchEvent:Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$400(Lcom/sec/android/gallery3d/ui/FilmStripView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 710
    :goto_0
    return-void

    .line 709
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->onLongTap(I)V

    goto :goto_0
.end method

.method public onScrollOverDistance(I)V
    .locals 3
    .param p1, "distance"    # I

    .prologue
    const/4 v2, 0x0

    .line 714
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$800(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getFilmStripLength()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getScreenWidth()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 715
    if-gez p1, :cond_1

    .line 716
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$800(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/EdgeView;->setVisibility(I)V

    .line 717
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$800(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    neg-int v1, p1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/EdgeView;->onPull(II)V

    .line 723
    :cond_0
    :goto_0
    return-void

    .line 719
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$800(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/EdgeView;->setVisibility(I)V

    .line 720
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$800(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/gallery3d/ui/EdgeView;->onPull(II)V

    goto :goto_0
.end method

.method public onScrollPositionChanged(II)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "total"    # I

    .prologue
    .line 727
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mLockTouchEvent:Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$400(Lcom/sec/android/gallery3d/ui/FilmStripView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 730
    :goto_0
    return-void

    .line 729
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->onScrollPositionChanged(II)V

    goto :goto_0
.end method

.method public onSingleTapUp(I)V
    .locals 1
    .param p1, "slotIndex"    # I

    .prologue
    .line 701
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->onSingleTapUp(I)V

    .line 702
    return-void
.end method

.method public onUp(Z)V
    .locals 2
    .param p1, "followedByLongPress"    # Z

    .prologue
    const/4 v1, 0x1

    .line 691
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mHelpMode:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$500(Lcom/sec/android/gallery3d/ui/FilmStripView;)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 692
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # invokes: Lcom/sec/android/gallery3d/ui/FilmStripView;->onUp(Z)V
    invoke-static {v0, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$700(Lcom/sec/android/gallery3d/ui/FilmStripView;Z)V

    .line 693
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$800(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 694
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$800(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/EdgeView;->onRelease()V

    .line 695
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$3;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$800(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/EdgeView;->setVisibility(I)V

    .line 697
    :cond_1
    return-void
.end method
