.class Lcom/sec/android/gallery3d/app/CropImage$3;
.super Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
.source "CropImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/app/CropImage;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/CropImage;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/ui/GLRoot;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/gallery3d/ui/GLRoot;

    .prologue
    .line 479
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;-><init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 16
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 482
    move-object/from16 v0, p1

    iget v10, v0, Landroid/os/Message;->what:I

    sparse-switch v10, :sswitch_data_0

    .line 615
    :goto_0
    return-void

    .line 487
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1000(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/app/ProgressDialog;

    move-result-object v11

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->dismissDialog(Landroid/app/Dialog;)V
    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1100(Lcom/sec/android/gallery3d/app/CropImage;Landroid/app/Dialog;)V

    .line 488
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->onBitmapRegionDecoderAvailable(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V
    invoke-static {v11, v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$1200(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    .line 489
    const/16 v10, 0x66

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/app/CropImage$3;->removeMessages(I)V

    goto :goto_0

    .line 497
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1000(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/app/ProgressDialog;

    move-result-object v11

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->dismissDialog(Landroid/app/Dialog;)V
    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1100(Lcom/sec/android/gallery3d/app/CropImage;Landroid/app/Dialog;)V

    .line 498
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Landroid/graphics/Bitmap;

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->onBitmapAvailable(Landroid/graphics/Bitmap;)V
    invoke-static {v11, v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$1300(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)V

    .line 499
    const/16 v10, 0x65

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/app/CropImage$3;->removeMessages(I)V

    goto :goto_0

    .line 511
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1000(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/app/ProgressDialog;

    move-result-object v11

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->dismissDialog(Landroid/app/Dialog;)V
    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1100(Lcom/sec/android/gallery3d/app/CropImage;Landroid/app/Dialog;)V

    .line 512
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->setResult(I)V

    .line 513
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const v11, 0x7f0e0038

    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 514
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/CropImage;->finish()V

    goto :goto_0

    .line 522
    :sswitch_3
    const-string v10, "SLinkCrop"

    const-string v11, "CropImage : MSG_SAVE_COMPLETE"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mLockScreen:Z
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$1400(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v10

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mBothScreen:Z
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$1500(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 524
    :cond_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->setLockScreenSettingAndBroadCast()V
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$1600(Lcom/sec/android/gallery3d/app/CropImage;)V

    .line 526
    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1000(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/app/ProgressDialog;

    move-result-object v11

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->dismissDialog(Landroid/app/Dialog;)V
    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1100(Lcom/sec/android/gallery3d/app/CropImage;Landroid/app/Dialog;)V

    .line 529
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mKnox:Z
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$1700(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v10

    if-eqz v10, :cond_4

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$1800(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v10

    if-eqz v10, :cond_4

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/CropImage;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX2(Landroid/content/Context;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 530
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/CropImage;->getExternalCacheDir()Ljava/io/File;

    move-result-object v2

    .line 531
    .local v2, "externalCacheDir":Ljava/io/File;
    if-nez v2, :cond_3

    .line 532
    const-string v10, "CropImage"

    const-string v11, "externalCacheDir is Null! "

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v11, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/CropImage;->mKnoxWallpaper:Landroid/graphics/Bitmap;
    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1902(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 534
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/app/CropImage;->setResult(ILandroid/content/Intent;)V

    .line 570
    .end local v2    # "externalCacheDir":Ljava/io/File;
    :goto_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsCropAndDelete:Z
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$2200(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 571
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$900(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->delete()V

    .line 575
    :cond_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/CropImage;->finish()V

    goto/16 :goto_0

    .line 536
    .restart local v2    # "externalCacheDir":Ljava/io/File;
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "knoxWallpaper.jpg"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 538
    .local v8, "path":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 539
    .local v3, "file":Ljava/io/File;
    const/4 v4, 0x0

    .line 541
    .local v4, "fout":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 542
    .end local v4    # "fout":Ljava/io/FileOutputStream;
    .local v5, "fout":Ljava/io/FileOutputStream;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mKnoxWallpaper:Landroid/graphics/Bitmap;
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$1900(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/graphics/Bitmap;

    move-result-object v10

    sget-object v11, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v12, 0x64

    invoke-virtual {v10, v11, v12, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 544
    new-instance v6, Landroid/content/Intent;

    const-string v10, "com.android.knoxlauncher.intent.action.SET_HOMESCREEN_WALLPAPER"

    invoke-direct {v6, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 545
    .local v6, "i":Landroid/content/Intent;
    const-string v10, "path"

    invoke-virtual {v6, v10, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 546
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v10, v6}, Lcom/sec/android/gallery3d/app/CropImage;->sendBroadcast(Landroid/content/Intent;)V

    .line 547
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v11, -0x1

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/app/CropImage;->setResult(ILandroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 554
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v11, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/CropImage;->mKnoxWallpaper:Landroid/graphics/Bitmap;
    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1902(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 555
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .line 549
    .end local v5    # "fout":Ljava/io/FileOutputStream;
    .end local v6    # "i":Landroid/content/Intent;
    .restart local v4    # "fout":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 551
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 552
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/app/CropImage;->setResult(ILandroid/content/Intent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 554
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v11, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/CropImage;->mKnoxWallpaper:Landroid/graphics/Bitmap;
    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1902(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 555
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_1

    .line 554
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v10

    :goto_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v12, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/CropImage;->mKnoxWallpaper:Landroid/graphics/Bitmap;
    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/app/CropImage;->access$1902(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 555
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v10

    .line 559
    .end local v2    # "externalCacheDir":Ljava/io/File;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "fout":Ljava/io/FileOutputStream;
    .end local v8    # "path":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Landroid/content/Intent;

    .line 560
    .local v7, "intent":Landroid/content/Intent;
    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "output"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/net/Uri;

    .line 562
    .local v9, "uri":Landroid/net/Uri;
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "cover_wallpaper"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 563
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->sendResultToSCover()V
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$2000(Lcom/sec/android/gallery3d/app/CropImage;)V

    .line 565
    :cond_5
    const-string/jumbo v10, "src_uri"

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mSrcUri:Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$2100(Lcom/sec/android/gallery3d/app/CropImage;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 566
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v11, -0x1

    invoke-virtual {v10, v11, v7}, Lcom/sec/android/gallery3d/app/CropImage;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_1

    .line 579
    .end local v7    # "intent":Landroid/content/Intent;
    .end local v9    # "uri":Landroid/net/Uri;
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mLockScreen:Z
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$1400(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v10

    if-nez v10, :cond_6

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mBothScreen:Z
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$1500(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 580
    :cond_6
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->setLockScreenSettingAndBroadCast()V
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$1600(Lcom/sec/android/gallery3d/app/CropImage;)V

    .line 582
    :cond_7
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1000(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/app/ProgressDialog;

    move-result-object v11

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->dismissDialog(Landroid/app/Dialog;)V
    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1100(Lcom/sec/android/gallery3d/app/CropImage;Landroid/app/Dialog;)V

    .line 584
    const-string v10, "SLinkCrop"

    const-string v11, "CropImage : MSG_SAVE_COMPLETE_SLINK"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Landroid/content/Intent;

    .line 586
    .restart local v7    # "intent":Landroid/content/Intent;
    const-string/jumbo v10, "src_uri"

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mSrcUri:Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$2100(Lcom/sec/android/gallery3d/app/CropImage;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 587
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v11, -0x1

    invoke-virtual {v10, v11, v7}, Lcom/sec/android/gallery3d/app/CropImage;->setResult(ILandroid/content/Intent;)V

    .line 589
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsCropAndDelete:Z
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$2200(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 590
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$900(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->delete()V

    .line 592
    :cond_8
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/CropImage;->finish()V

    goto/16 :goto_0

    .line 596
    .end local v7    # "intent":Landroid/content/Intent;
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1000(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/app/ProgressDialog;

    move-result-object v11

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->dismissDialog(Landroid/app/Dialog;)V
    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$1100(Lcom/sec/android/gallery3d/app/CropImage;Landroid/app/Dialog;)V

    .line 597
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->setResult(I)V

    .line 598
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/CropImage;->finish()V

    goto/16 :goto_0

    .line 603
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mLoadBitmapTask:Lcom/sec/android/gallery3d/util/Future;
    invoke-static {v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$2300(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v11

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->cancelTaskAndDismissProgressDialog(Lcom/sec/android/gallery3d/util/Future;)V
    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$2400(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/Future;)V

    .line 604
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/CropImage;->getAndroidContext()Landroid/content/Context;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const v12, 0x7f0e0033

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    const/16 v15, 0x64

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Lcom/sec/android/gallery3d/app/CropImage;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 605
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v12, 0x0

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Landroid/content/Intent;

    invoke-virtual {v11, v12, v10}, Lcom/sec/android/gallery3d/app/CropImage;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 609
    :sswitch_7
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;
    invoke-static {v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$2500(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v11

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->cancelTaskAndDismissProgressDialog(Lcom/sec/android/gallery3d/util/Future;)V
    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/app/CropImage;->access$2400(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/Future;)V

    .line 610
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/CropImage;->getAndroidContext()Landroid/content/Context;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const v12, 0x7f0e0033

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    const/16 v15, 0x64

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Lcom/sec/android/gallery3d/app/CropImage;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 611
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/app/CropImage$3;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    const/4 v12, 0x0

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Landroid/content/Intent;

    invoke-virtual {v11, v12, v10}, Lcom/sec/android/gallery3d/app/CropImage;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 554
    .restart local v2    # "externalCacheDir":Ljava/io/File;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fout":Ljava/io/FileOutputStream;
    .restart local v8    # "path":Ljava/lang/String;
    :catchall_1
    move-exception v10

    move-object v4, v5

    .end local v5    # "fout":Ljava/io/FileOutputStream;
    .restart local v4    # "fout":Ljava/io/FileOutputStream;
    goto/16 :goto_3

    .line 549
    .end local v4    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "fout":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v1

    move-object v4, v5

    .end local v5    # "fout":Ljava/io/FileOutputStream;
    .restart local v4    # "fout":Ljava/io/FileOutputStream;
    goto/16 :goto_2

    .line 482
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_3
        0x4 -> :sswitch_2
        0x5 -> :sswitch_5
        0x6 -> :sswitch_4
        0x65 -> :sswitch_6
        0x66 -> :sswitch_7
    .end sparse-switch
.end method
