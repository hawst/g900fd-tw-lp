.class Lcom/sec/android/gallery3d/app/GalleryActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "GalleryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/GalleryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/GalleryActivity;)V
    .locals 0

    .prologue
    .line 770
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$2;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 774
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 775
    .local v0, "action":Ljava/lang/String;
    const-string v8, "com.android.media.FACE_GET_SIMILAR_PERSONS_FINISHED"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 776
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$2;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/GalleryActivity;->access$100(Lcom/sec/android/gallery3d/app/GalleryActivity;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v7

    .line 777
    .local v7, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v8, v7, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v8, :cond_0

    move-object v8, v7

    .line 778
    check-cast v8, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->lockNotify()V

    .line 781
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    .line 782
    .local v6, "uri":Landroid/net/Uri;
    invoke-virtual {v6}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    .line 783
    .local v5, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    .line 784
    .local v1, "cnt":I
    add-int/lit8 v1, v1, -0x3

    .line 785
    if-lez v1, :cond_2

    .line 786
    const/4 v8, 0x2

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 787
    .local v2, "faceId":I
    new-array v4, v1, [I

    .line 788
    .local v4, "ids":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 789
    add-int/lit8 v8, v3, 0x3

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    aput v8, v4, v3

    .line 788
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 792
    :cond_1
    instance-of v8, v7, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v8, :cond_2

    .line 793
    check-cast v7, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .end local v7    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-virtual {v7, v2, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setUnnamedFaceCandidates(I[I)V

    .line 798
    .end local v1    # "cnt":I
    .end local v2    # "faceId":I
    .end local v3    # "i":I
    .end local v4    # "ids":[I
    .end local v5    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_2
    return-void
.end method
