.class public final Lcom/sec/android/gallery3d/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final defaultValue:I = 0x7f010004

.field public static final dividerWidth:I = 0x7f010013

.field public static final entries:I = 0x7f010006

.field public static final entryValues:I = 0x7f010005

.field public static final footerDividersEnabled:I = 0x7f010015

.field public static final headerDividersEnabled:I = 0x7f010014

.field public static final horizontalSpacing:I = 0x7f01000d

.field public static final icons:I = 0x7f010008

.field public static final images:I = 0x7f01000c

.field public static final itemHeight:I = 0x7f01000f

.field public static final key:I = 0x7f010003

.field public static final labelList:I = 0x7f010007

.field public static final largeIcons:I = 0x7f01000b

.field public static final listPreferredItemHeightSmall:I = 0x7f010000

.field public static final measureWithChild:I = 0x7f010018

.field public static final modes:I = 0x7f010009

.field public static final overScrollFooter:I = 0x7f010017

.field public static final overScrollHeader:I = 0x7f010016

.field public static final sephiroth_absHListViewStyle:I = 0x7f010011

.field public static final sephiroth_listPreferredItemWidth:I = 0x7f010012

.field public static final sephiroth_listViewStyle:I = 0x7f010010

.field public static final singleIcon:I = 0x7f01000a

.field public static final stackFromRight:I = 0x7f010019

.field public static final switchStyle:I = 0x7f010001

.field public static final title:I = 0x7f010002

.field public static final transcriptMode:I = 0x7f01001a

.field public static final verticalSpacing:I = 0x7f01000e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
