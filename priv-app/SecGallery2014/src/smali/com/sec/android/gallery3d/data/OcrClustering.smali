.class public Lcom/sec/android/gallery3d/data/OcrClustering;
.super Lcom/sec/android/gallery3d/data/Clustering;
.source "OcrClustering.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;
    }
.end annotation


# static fields
.field public static final OCR_FOLDER:Ljava/lang/String;

.field public static final OCR_STRINGDATA_KEYCODE:Ljava/lang/String; = "OCRDATA"

.field public static final OCR_STRINGDATA_TOKEN_SEPEARTOR:Ljava/lang/String; = "^#%"

.field private static final TAG:Ljava/lang/String; = "OcrClustering"


# instance fields
.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mKeyword:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Photo Reader"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/OcrClustering;->OCR_FOLDER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keyword"    # Ljava/lang/String;

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/Clustering;-><init>()V

    .line 43
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/OcrClustering;->mItems:Ljava/util/ArrayList;

    .line 93
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {p2, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/OcrClustering;->mKeyword:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :goto_0
    return-void

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/OcrClustering;->mKeyword:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/OcrClustering;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/OcrClustering;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/OcrClustering;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/OcrClustering;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering;->mItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getSearchKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 77
    if-nez p0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-object v2

    .line 81
    :cond_1
    const-string v3, "/cluster/"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 82
    const-string v3, "/"

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 83
    .local v1, "segments":[Ljava/lang/String;
    array-length v0, v1

    .line 84
    .local v0, "count":I
    const/4 v3, 0x2

    if-le v0, v3, :cond_0

    .line 85
    add-int/lit8 v2, v0, -0x2

    aget-object v2, v1, v2

    goto :goto_0
.end method

.method public static getSearchPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "parentPath"    # Ljava/lang/String;
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 64
    if-nez p1, :cond_0

    .line 72
    .end local p0    # "parentPath":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 68
    .restart local p0    # "parentPath":Ljava/lang/String;
    :cond_0
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {p1, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 72
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/cluster/{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}/ocr/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    goto :goto_0
.end method

.method public static isOCRMedia(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 4
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    instance-of v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v3, :cond_2

    .line 53
    check-cast p0, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local p0    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_1

    sget-object v3, Lcom/sec/android/gallery3d/data/OcrClustering;->OCR_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 54
    goto :goto_0

    .line 55
    .end local v0    # "path":Ljava/lang/String;
    .restart local p0    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    instance-of v3, p0, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v3, :cond_4

    .line 56
    check-cast p0, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p0    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v0

    .line 57
    .restart local v0    # "path":Ljava/lang/String;
    if-eqz v0, :cond_3

    sget-object v3, Lcom/sec/android/gallery3d/data/OcrClustering;->OCR_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .end local v0    # "path":Ljava/lang/String;
    .restart local p0    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    move v1, v2

    .line 59
    goto :goto_0
.end method


# virtual methods
.method public getCluster(I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering;->mItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getClusterKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method public getClusterName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getNumberOfClusters()I
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method

.method public run(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 1
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 102
    new-instance v0, Lcom/sec/android/gallery3d/data/OcrClustering$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/OcrClustering$1;-><init>(Lcom/sec/android/gallery3d/data/OcrClustering;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    .line 112
    return-void
.end method

.method searchImgText(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "searchWord"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 138
    new-instance v4, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;-><init>()V

    .line 139
    .local v4, "parser":Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;
    const-string v2, ""

    .line 143
    .local v2, "imgString":Ljava/lang/String;
    const/4 v0, 0x0

    .line 145
    .local v0, "bMatch":Z
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 185
    :goto_0
    return v7

    .line 149
    :cond_0
    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->open(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 150
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->getApp5Segment()Ljava/lang/String;

    move-result-object v2

    .line 151
    const-string v9, "OcrClustering"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ImageTextSearch"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    new-instance v5, Ljava/util/StringTokenizer;

    const-string v9, "^#%"

    invoke-direct {v5, v2, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    .local v5, "stringtokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v9

    add-int/lit8 v3, v9, -0x2

    .line 155
    .local v3, "itemNum":I
    const-string v9, "OcrClustering"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ImageTextSearch : Data num : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v9

    if-ge v9, v7, :cond_1

    .line 158
    const-string v7, "OcrClustering"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ImageTextSearch : OCR item num : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v8

    .line 159
    goto :goto_0

    .line 162
    :cond_1
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 163
    .local v6, "token":Ljava/lang/String;
    const-string v7, "OCRDATA"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 164
    const-string v7, "OcrClustering"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ImageTextSearch : invalid ocrKeyCode("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v8

    .line 165
    goto/16 :goto_0

    .line 168
    :cond_2
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 170
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_3

    .line 171
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 172
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(?i).*"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".*"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 173
    const-string v7, "OcrClustering"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ImageTextSearch : OCR match !!! <="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    const/4 v0, 0x1

    .line 181
    :cond_3
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/OcrClustering$ExifParser;->close()V

    .end local v1    # "i":I
    .end local v3    # "itemNum":I
    .end local v5    # "stringtokenizer":Ljava/util/StringTokenizer;
    .end local v6    # "token":Ljava/lang/String;
    :goto_2
    move v7, v0

    .line 185
    goto/16 :goto_0

    .line 177
    .restart local v1    # "i":I
    .restart local v3    # "itemNum":I
    .restart local v5    # "stringtokenizer":Ljava/util/StringTokenizer;
    .restart local v6    # "token":Ljava/lang/String;
    :cond_4
    const-string v7, "OcrClustering"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ImageTextSearch : OCR token : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 183
    .end local v1    # "i":I
    .end local v3    # "itemNum":I
    .end local v5    # "stringtokenizer":Ljava/util/StringTokenizer;
    .end local v6    # "token":Ljava/lang/String;
    :cond_5
    const-string v7, "OcrClustering"

    const-string v8, "ImageTextSearch : Jpeg read fail"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
