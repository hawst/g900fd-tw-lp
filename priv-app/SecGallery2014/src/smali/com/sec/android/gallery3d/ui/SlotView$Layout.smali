.class public Lcom/sec/android/gallery3d/ui/SlotView$Layout;
.super Ljava/lang/Object;
.source "SlotView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/SlotView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Layout"
.end annotation


# instance fields
.field private mContentLength:I

.field private mHeight:I

.field private mHorizontalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

.field private mIsScrollingRight:Z

.field private mScrollPosition:I

.field private mSlotCount:I

.field private mSlotGap:I

.field private mSlotHeight:I

.field private mSlotWidth:I

.field private mSpec:Lcom/sec/android/gallery3d/ui/SlotView$Spec;

.field private mUnitCount:I

.field private mVerticalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

.field private mVisibleEnd:I

.field private mVisibleStart:I

.field private mWidth:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/SlotView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/SlotView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 425
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444
    new-instance v0, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;-><init>(Lcom/sec/android/gallery3d/ui/SlotView$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVerticalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    .line 445
    new-instance v0, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;-><init>(Lcom/sec/android/gallery3d/ui/SlotView$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHorizontalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    .prologue
    .line 425
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    .prologue
    .line 425
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    .prologue
    .line 425
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    .prologue
    .line 425
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotCount:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    .prologue
    .line 425
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleEnd:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    .prologue
    .line 425
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleStart:I

    return v0
.end method

.method private initLayoutParameters()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 532
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSpec:Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotWidth:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 533
    iput v7, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    .line 534
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSpec:Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotWidth:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    .line 535
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSpec:Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotHeight:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    .line 543
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->access$700(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->access$700(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    iget v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    invoke-interface {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;->onSlotSizeChanged(II)V

    .line 547
    :cond_0
    const/4 v0, 0x2

    new-array v5, v0, [I

    .line 548
    .local v5, "padding":[I
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v0, :cond_3

    .line 549
    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mWidth:I

    iget v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHeight:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->initLayoutParameters(IIII[I)V

    .line 550
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVerticalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    aget v1, v5, v7

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->startAnimateTo(I)V

    .line 551
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHorizontalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    aget v1, v5, v8

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->startAnimateTo(I)V

    .line 559
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->updateVisibleSlotRange()V

    .line 561
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mIsScrollingRight:Z

    .line 563
    return-void

    .line 537
    .end local v5    # "padding":[I
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mWidth:I

    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHeight:I

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSpec:Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    iget v6, v0, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->rowsLand:I

    .line 538
    .local v6, "rows":I
    :goto_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSpec:Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotGap:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    .line 539
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHeight:I

    add-int/lit8 v1, v6, -0x1

    iget v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    div-int/2addr v0, v6

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    .line 540
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSpec:Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotHeightAdditional:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    goto :goto_0

    .line 537
    .end local v6    # "rows":I
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSpec:Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    iget v6, v0, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->rowsPort:I

    goto :goto_2

    .line 553
    .restart local v5    # "padding":[I
    :cond_3
    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHeight:I

    iget v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mWidth:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->initLayoutParameters(IIII[I)V

    .line 555
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVerticalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mAlignCenter:Z

    if-eqz v0, :cond_4

    aget v0, v5, v8

    :goto_3
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->startAnimateTo(I)V

    .line 557
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHorizontalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    aget v1, v5, v7

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->startAnimateTo(I)V

    goto :goto_1

    :cond_4
    move v0, v7

    .line 555
    goto :goto_3
.end method

.method private initLayoutParameters(IIII[I)V
    .locals 8
    .param p1, "majorLength"    # I
    .param p2, "minorLength"    # I
    .param p3, "majorUnitSize"    # I
    .param p4, "minorUnitSize"    # I
    .param p5, "padding"    # [I

    .prologue
    const/4 v7, 0x0

    .line 508
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v4, p2

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v5, p4

    div-int v2, v4, v5

    .line 509
    .local v2, "unitCount":I
    if-nez v2, :cond_0

    const/4 v2, 0x1

    .line 510
    :cond_0
    iput v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    .line 513
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotCount:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 514
    .local v0, "availableUnits":I
    mul-int v4, v0, p4

    add-int/lit8 v5, v0, -0x1

    iget v6, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    mul-int/2addr v5, v6

    add-int v3, v4, v5

    .line 516
    .local v3, "usedMinorLength":I
    sub-int v4, p2, v3

    div-int/lit8 v4, v4, 0x2

    aput v4, p5, v7

    .line 519
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotCount:I

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    div-int v1, v4, v5

    .line 520
    .local v1, "count":I
    mul-int v4, v1, p3

    add-int/lit8 v5, v1, -0x1

    iget v6, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    mul-int/2addr v5, v6

    add-int/2addr v4, v5

    iput v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mContentLength:I

    .line 524
    const/4 v4, 0x1

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mContentLength:I

    sub-int v5, p1, v5

    div-int/lit8 v5, v5, 0x2

    invoke-static {v7, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    aput v5, p5, v4

    .line 526
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mIsScrollingRight:Z

    .line 528
    return-void
.end method

.method private setVisibleRange(II)V
    .locals 3
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 607
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleStart:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleEnd:I

    if-ne p2, v0, :cond_1

    .line 625
    :cond_0
    :goto_0
    return-void

    .line 608
    :cond_1
    if-ge p1, p2, :cond_2

    .line 617
    iput p1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleStart:I

    .line 618
    iput p2, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleEnd:I

    .line 622
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->access$700(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 623
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->access$700(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleStart:I

    iget v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleEnd:I

    invoke-interface {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;->onVisibleRangeChanged(II)V

    goto :goto_0

    .line 620
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleEnd:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleStart:I

    goto :goto_1
.end method

.method private updateVisibleSlotRange()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 572
    iget v3, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mScrollPosition:I

    .line 574
    .local v3, "position":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-boolean v7, v7, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v7, :cond_0

    .line 575
    iget v7, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    iget v8, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v7, v8

    div-int v5, v3, v7

    .line 576
    .local v5, "startCol":I
    iget v7, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    mul-int/2addr v7, v5

    invoke-static {v9, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 577
    .local v4, "start":I
    iget v7, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mWidth:I

    add-int/2addr v7, v3

    iget v8, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    add-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v7, v8

    add-int/lit8 v7, v7, -0x1

    iget v8, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    iget v9, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v8, v9

    div-int v1, v7, v8

    .line 579
    .local v1, "endCol":I
    iget v7, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotCount:I

    iget v8, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    mul-int/2addr v8, v1

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 580
    .local v0, "end":I
    invoke-direct {p0, v4, v0}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->setVisibleRange(II)V

    .line 589
    .end local v1    # "endCol":I
    .end local v5    # "startCol":I
    :goto_0
    return-void

    .line 582
    .end local v0    # "end":I
    .end local v4    # "start":I
    :cond_0
    iget v7, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    iget v8, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v7, v8

    div-int v6, v3, v7

    .line 583
    .local v6, "startRow":I
    iget v7, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    mul-int/2addr v7, v6

    invoke-static {v9, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 584
    .restart local v4    # "start":I
    iget v7, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHeight:I

    add-int/2addr v7, v3

    iget v8, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    add-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v7, v8

    add-int/lit8 v7, v7, -0x1

    iget v8, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    iget v9, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v8, v9

    div-int v2, v7, v8

    .line 586
    .local v2, "endRow":I
    iget v7, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotCount:I

    iget v8, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    mul-int/2addr v8, v2

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 587
    .restart local v0    # "end":I
    invoke-direct {p0, v4, v0}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->setVisibleRange(II)V

    goto :goto_0
.end method


# virtual methods
.method public advanceAnimation(J)Z
    .locals 3
    .param p1, "animTime"    # J

    .prologue
    .line 679
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVerticalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->calculate(J)Z

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHorizontalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->calculate(J)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public getScrollLimit()I
    .locals 3

    .prologue
    .line 673
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mContentLength:I

    iget v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mWidth:I

    sub-int v0, v1, v2

    .line 674
    .local v0, "limit":I
    :goto_0
    if-gtz v0, :cond_0

    const/4 v0, 0x0

    .end local v0    # "limit":I
    :cond_0
    return v0

    .line 673
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mContentLength:I

    iget v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHeight:I

    sub-int v0, v1, v2

    goto :goto_0
.end method

.method public getSlotHeight()I
    .locals 1

    .prologue
    .line 489
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    return v0
.end method

.method public getSlotIndexByPosition(FF)I
    .locals 9
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v6, 0x0

    const/4 v7, -0x1

    .line 636
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v8

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-boolean v5, v5, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mScrollPosition:I

    :goto_0
    add-int v0, v8, v5

    .line 637
    .local v0, "absoluteX":I
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result v5

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-boolean v8, v8, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v8, :cond_2

    :goto_1
    add-int v1, v5, v6

    .line 639
    .local v1, "absoluteY":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHorizontalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->get()I

    move-result v5

    sub-int/2addr v0, v5

    .line 640
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVerticalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->get()I

    move-result v5

    sub-int/2addr v1, v5

    .line 642
    if-ltz v0, :cond_0

    if-gez v1, :cond_3

    .line 669
    :cond_0
    :goto_2
    return v7

    .end local v0    # "absoluteX":I
    .end local v1    # "absoluteY":I
    :cond_1
    move v5, v6

    .line 636
    goto :goto_0

    .line 637
    .restart local v0    # "absoluteX":I
    :cond_2
    iget v6, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mScrollPosition:I

    goto :goto_1

    .line 646
    .restart local v1    # "absoluteY":I
    :cond_3
    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v5, v6

    div-int v2, v0, v5

    .line 647
    .local v2, "columnIdx":I
    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v5, v6

    div-int v4, v1, v5

    .line 649
    .local v4, "rowIdx":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-boolean v5, v5, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-nez v5, :cond_4

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    if-ge v2, v5, :cond_0

    .line 653
    :cond_4
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-boolean v5, v5, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v5, :cond_5

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    if-ge v4, v5, :cond_0

    .line 657
    :cond_5
    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v5, v6

    rem-int v5, v0, v5

    iget v6, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    if-ge v5, v6, :cond_0

    .line 661
    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v5, v6

    rem-int v5, v1, v5

    iget v6, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    if-ge v5, v6, :cond_0

    .line 665
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-boolean v5, v5, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v5, :cond_7

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    mul-int/2addr v5, v2

    add-int v3, v5, v4

    .line 669
    .local v3, "index":I
    :goto_3
    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotCount:I

    if-lt v3, v5, :cond_6

    move v3, v7

    .end local v3    # "index":I
    :cond_6
    move v7, v3

    goto :goto_2

    .line 665
    :cond_7
    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    mul-int/2addr v5, v4

    add-int v3, v5, v2

    goto :goto_3
.end method

.method public getSlotRect(ILandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 7
    .param p1, "index"    # I
    .param p2, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 470
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-boolean v4, v4, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v4, :cond_0

    .line 471
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    div-int v0, p1, v4

    .line 472
    .local v0, "col":I
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    mul-int/2addr v4, v0

    sub-int v1, p1, v4

    .line 478
    .local v1, "row":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHorizontalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->get()I

    move-result v4

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v5, v6

    mul-int/2addr v5, v0

    add-int v2, v4, v5

    .line 479
    .local v2, "x":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVerticalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->get()I

    move-result v4

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I

    add-int/2addr v5, v6

    mul-int/2addr v5, v1

    add-int v3, v4, v5

    .line 480
    .local v3, "y":I
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    add-int/2addr v4, v2

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I

    add-int/2addr v5, v3

    invoke-virtual {p2, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 481
    return-object p2

    .line 474
    .end local v0    # "col":I
    .end local v1    # "row":I
    .end local v2    # "x":I
    .end local v3    # "y":I
    :cond_0
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    div-int v1, p1, v4

    .line 475
    .restart local v1    # "row":I
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mUnitCount:I

    mul-int/2addr v4, v1

    sub-int v0, p1, v4

    .restart local v0    # "col":I
    goto :goto_0
.end method

.method public getSlotWidth()I
    .locals 1

    .prologue
    .line 485
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I

    return v0
.end method

.method public getVisibleEnd()I
    .locals 1

    .prologue
    .line 632
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleEnd:I

    return v0
.end method

.method public getVisibleStart()I
    .locals 1

    .prologue
    .line 628
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleStart:I

    return v0
.end method

.method public setScrollPosition(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 592
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mScrollPosition:I

    if-ne v0, p1, :cond_0

    .line 604
    :goto_0
    return-void

    .line 602
    :cond_0
    iput p1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mScrollPosition:I

    .line 603
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->updateVisibleSlotRange()V

    goto :goto_0
.end method

.method public setSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 566
    iput p1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mWidth:I

    .line 567
    iput p2, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHeight:I

    .line 568
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->initLayoutParameters()V

    .line 569
    return-void
.end method

.method public setSlotCount(I)Z
    .locals 5
    .param p1, "slotCount"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 455
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotCount:I

    if-ne p1, v4, :cond_1

    .line 464
    :cond_0
    :goto_0
    return v2

    .line 456
    :cond_1
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotCount:I

    if-eqz v4, :cond_2

    .line 457
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHorizontalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->setEnabled(Z)V

    .line 458
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVerticalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->setEnabled(Z)V

    .line 460
    :cond_2
    iput p1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotCount:I

    .line 461
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHorizontalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->getTarget()I

    move-result v0

    .line 462
    .local v0, "hPadding":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVerticalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->getTarget()I

    move-result v1

    .line 463
    .local v1, "vPadding":I
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->initLayoutParameters()V

    .line 464
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVerticalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->getTarget()I

    move-result v4

    if-ne v1, v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mHorizontalPadding:Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;->getTarget()I

    move-result v4

    if-eq v0, v4, :cond_0

    :cond_3
    move v2, v3

    goto :goto_0
.end method

.method public setSlotSpec(Lcom/sec/android/gallery3d/ui/SlotView$Spec;)V
    .locals 0
    .param p1, "spec"    # Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    .prologue
    .line 451
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSpec:Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    .line 452
    return-void
.end method
