.class public Lcom/sec/android/gallery3d/util/BuaMediaVUXUtil;
.super Ljava/lang/Object;
.source "BuaMediaVUXUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BuaMediaVUXUtil"

.field public static TYPE_ALL:I

.field public static TYPE_DOCUMENT:I

.field public static TYPE_MUSIC:I

.field public static TYPE_PICTURE:I

.field public static TYPE_PICTURE_VIDEO:I

.field public static TYPE_VIDEO:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/gallery3d/util/BuaMediaVUXUtil;->TYPE_ALL:I

    .line 17
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/gallery3d/util/BuaMediaVUXUtil;->TYPE_MUSIC:I

    .line 18
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/gallery3d/util/BuaMediaVUXUtil;->TYPE_VIDEO:I

    .line 19
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/gallery3d/util/BuaMediaVUXUtil;->TYPE_PICTURE:I

    .line 20
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/gallery3d/util/BuaMediaVUXUtil;->TYPE_DOCUMENT:I

    .line 21
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/gallery3d/util/BuaMediaVUXUtil;->TYPE_PICTURE_VIDEO:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBuaMediaVuxAppIcon(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const/4 v2, 0x0

    .line 53
    .local v2, "buaMediaVUXAppIcon":Landroid/graphics/drawable/Drawable;
    :try_start_0
    const-string v7, "com.samsung.vuxbuamedia.BackupAssistantUtils"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 54
    .local v3, "buaMediaVux":Ljava/lang/Class;
    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/Class;

    .line 55
    .local v6, "paramTypes":[Ljava/lang/Class;
    const/4 v7, 0x0

    const-class v8, Landroid/content/Context;

    aput-object v8, v6, v7

    .line 57
    const/4 v7, 0x1

    new-array v1, v7, [Ljava/lang/Object;

    .line 58
    .local v1, "arglist":[Ljava/lang/Object;
    const/4 v7, 0x0

    aput-object p0, v1, v7

    .line 60
    const-string v7, "getBuaMediaVUXAppIcon"

    invoke-virtual {v3, v7, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 61
    .local v5, "m":Ljava/lang/reflect/Method;
    const/4 v7, 0x0

    invoke-virtual {v5, v7, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Landroid/graphics/drawable/Drawable;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    .end local v1    # "arglist":[Ljava/lang/Object;
    .end local v3    # "buaMediaVux":Ljava/lang/Class;
    .end local v5    # "m":Ljava/lang/reflect/Method;
    .end local v6    # "paramTypes":[Ljava/lang/Class;
    :goto_0
    return-object v2

    .line 63
    :catch_0
    move-exception v4

    .line 64
    .local v4, "e":Ljava/lang/Exception;
    const-string v7, "BuaMediaVUXUtil"

    const-string v8, "cant find com.samsung.vuxbuamedia.BackupAssistantUtils"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getBuaMediaVuxAppName(Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    const/4 v2, 0x0

    .line 77
    .local v2, "buaMediaVUXAppName":Ljava/lang/String;
    :try_start_0
    const-string v7, "com.samsung.vuxbuamedia.BackupAssistantUtils"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 78
    .local v3, "buaMediaVux":Ljava/lang/Class;
    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/Class;

    .line 79
    .local v6, "paramTypes":[Ljava/lang/Class;
    const/4 v7, 0x0

    const-class v8, Landroid/content/Context;

    aput-object v8, v6, v7

    .line 81
    const/4 v7, 0x1

    new-array v1, v7, [Ljava/lang/Object;

    .line 82
    .local v1, "arglist":[Ljava/lang/Object;
    const/4 v7, 0x0

    aput-object p0, v1, v7

    .line 84
    const-string v7, "getBuaMediaVUXAppName"

    invoke-virtual {v3, v7, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 85
    .local v5, "m":Ljava/lang/reflect/Method;
    const/4 v7, 0x0

    invoke-virtual {v5, v7, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    .end local v1    # "arglist":[Ljava/lang/Object;
    .end local v3    # "buaMediaVux":Ljava/lang/Class;
    .end local v5    # "m":Ljava/lang/reflect/Method;
    .end local v6    # "paramTypes":[Ljava/lang/Class;
    :goto_0
    return-object v2

    .line 87
    :catch_0
    move-exception v4

    .line 88
    .local v4, "e":Ljava/lang/Exception;
    const-string v7, "BuaMediaVUXUtil"

    const-string v8, "cant find com.samsung.vuxbuamedia.BackupAssistantUtils"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isBuaMediaVuxAppEnabled(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v3, 0x0

    .line 30
    .local v3, "isVuxAppEnabled":Z
    :try_start_0
    const-string v6, "com.samsung.vuxbuamedia.BackupAssistantUtils"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 31
    .local v1, "buaMediaVux":Ljava/lang/Class;
    const/4 v6, 0x1

    new-array v5, v6, [Ljava/lang/Class;

    .line 32
    .local v5, "paramTypes":[Ljava/lang/Class;
    const/4 v6, 0x0

    const-class v7, Landroid/content/Context;

    aput-object v7, v5, v6

    .line 34
    const/4 v6, 0x1

    new-array v0, v6, [Ljava/lang/Object;

    .line 35
    .local v0, "arglist":[Ljava/lang/Object;
    const/4 v6, 0x0

    aput-object p0, v0, v6

    .line 37
    const-string v6, "isBuaMediaVUXAppEnabled"

    invoke-virtual {v1, v6, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 38
    .local v4, "m":Ljava/lang/reflect/Method;
    const/4 v6, 0x0

    invoke-virtual {v4, v6, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 45
    .end local v0    # "arglist":[Ljava/lang/Object;
    .end local v1    # "buaMediaVux":Ljava/lang/Class;
    .end local v4    # "m":Ljava/lang/reflect/Method;
    .end local v5    # "paramTypes":[Ljava/lang/Class;
    :goto_0
    return v3

    .line 40
    :catch_0
    move-exception v2

    .line 41
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "BuaMediaVUXUtil"

    const-string v7, "cant find com.samsung.vuxbuamedia.BackupAssistantUtils"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static launchBuaMediaVUXApp(Landroid/content/Context;I)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    .line 98
    :try_start_0
    const-string v5, "com.samsung.vuxbuamedia.BackupAssistantUtils"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 99
    .local v1, "buaMediaVux":Ljava/lang/Class;
    const/4 v5, 0x2

    new-array v4, v5, [Ljava/lang/Class;

    .line 100
    .local v4, "paramTypes":[Ljava/lang/Class;
    const/4 v5, 0x0

    const-class v6, Landroid/content/Context;

    aput-object v6, v4, v5

    .line 101
    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    .line 103
    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/Object;

    .line 104
    .local v0, "arglist":[Ljava/lang/Object;
    const/4 v5, 0x0

    aput-object p0, v0, v5

    .line 105
    const/4 v5, 0x1

    new-instance v6, Ljava/lang/Integer;

    invoke-direct {v6, p1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v6, v0, v5

    .line 107
    const-string v5, "launchBuaMediaVUXApp"

    invoke-virtual {v1, v5, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 108
    .local v3, "launchApp":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    invoke-virtual {v3, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    .end local v0    # "arglist":[Ljava/lang/Object;
    .end local v1    # "buaMediaVux":Ljava/lang/Class;
    .end local v3    # "launchApp":Ljava/lang/reflect/Method;
    .end local v4    # "paramTypes":[Ljava/lang/Class;
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v2

    .line 111
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "BuaMediaVUXUtil"

    const-string v6, "cant find com.samsung.vuxbuamedia.BackupAssistantUtils"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static setBuaMediaVUXMenuItem(Landroid/content/Context;Landroid/view/MenuItem;Ljava/lang/Boolean;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "buaVUXItem"    # Landroid/view/MenuItem;
    .param p2, "IsActionBarMenuItem"    # Ljava/lang/Boolean;

    .prologue
    .line 122
    :try_start_0
    const-string v5, "com.samsung.vuxbuamedia.BackupAssistantUtils"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 123
    .local v1, "buaMediaVux":Ljava/lang/Class;
    const/4 v5, 0x3

    new-array v4, v5, [Ljava/lang/Class;

    .line 124
    .local v4, "paramTypes":[Ljava/lang/Class;
    const/4 v5, 0x0

    const-class v6, Landroid/content/Context;

    aput-object v6, v4, v5

    .line 125
    const/4 v5, 0x1

    const-class v6, Landroid/view/MenuItem;

    aput-object v6, v4, v5

    .line 126
    const/4 v5, 0x2

    const-class v6, Ljava/lang/Boolean;

    aput-object v6, v4, v5

    .line 128
    const/4 v5, 0x3

    new-array v0, v5, [Ljava/lang/Object;

    .line 129
    .local v0, "arglist":[Ljava/lang/Object;
    const/4 v5, 0x0

    aput-object p0, v0, v5

    .line 130
    const/4 v5, 0x1

    aput-object p1, v0, v5

    .line 131
    const/4 v5, 0x2

    aput-object p2, v0, v5

    .line 133
    const-string v5, "setBuaMediaVUXMenuItem"

    invoke-virtual {v1, v5, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 134
    .local v3, "launchApp":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    invoke-virtual {v3, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    .end local v0    # "arglist":[Ljava/lang/Object;
    .end local v1    # "buaMediaVux":Ljava/lang/Class;
    .end local v3    # "launchApp":Ljava/lang/reflect/Method;
    .end local v4    # "paramTypes":[Ljava/lang/Class;
    :goto_0
    return-void

    .line 136
    :catch_0
    move-exception v2

    .line 137
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "BuaMediaVUXUtil"

    const-string v6, "cant find com.samsung.vuxbuamedia.BackupAssistantUtils"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
