.class public Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "TCloudMergeAlbum.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;
    }
.end annotation


# static fields
.field private static final PAGE_SIZE:I = 0x40

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mBucketId:I

.field private final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mFetcher:[Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;

.field private mIndex:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "[I>;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field private final mSources:[Lcom/sec/android/gallery3d/data/MediaSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;[Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 6
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "sources"    # [Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;[",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v5, 0x0

    .line 51
    const/4 v4, 0x0

    invoke-direct {p0, p1, v4}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 48
    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    iput-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mIndex:Ljava/util/TreeMap;

    .line 53
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mComparator:Ljava/util/Comparator;

    .line 54
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .line 55
    array-length v4, p3

    if-nez v4, :cond_0

    const-string v4, ""

    :goto_0
    iput-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mName:Ljava/lang/String;

    .line 56
    array-length v4, p3

    if-nez v4, :cond_1

    const/4 v4, -0x1

    :goto_1
    iput v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mBucketId:I

    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_2
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 58
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 57
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 55
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    aget-object v4, p3, v5

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 56
    :cond_1
    aget-object v4, p3, v5

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v4

    goto :goto_1

    .line 60
    .restart local v0    # "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    :cond_2
    return-void
.end method

.method private invalidateCache()V
    .locals 6

    .prologue
    .line 74
    const/4 v1, 0x0

    .local v1, "i":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v2, v3

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 75
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mFetcher:[Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;->invalidate()V

    .line 74
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mIndex:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->clear()V

    .line 78
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mIndex:Ljava/util/TreeMap;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v5, v5

    new-array v5, v5, [I

    invoke-virtual {v3, v4, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    .end local v2    # "n":I
    :goto_1
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method private updateData()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 63
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v2, v2

    new-array v2, v2, [Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;

    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mFetcher:[Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;

    .line 64
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v1, v2

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 65
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mFetcher:[Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;

    new-instance v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v4, v4, v0

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;)V

    aput-object v3, v2, v0

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mIndex:Ljava/util/TreeMap;

    invoke-virtual {v2}, Ljava/util/TreeMap;->clear()V

    .line 68
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mIndex:Ljava/util/TreeMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v4, v4

    new-array v4, v4, [I

    invoke-virtual {v2, v3, v4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v2, v2

    if-nez v2, :cond_1

    const-string v2, ""

    :goto_1
    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mName:Ljava/lang/String;

    .line 70
    return-void

    .line 69
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v2, v2, v5

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public delete()V
    .locals 4

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 208
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->delete()V

    .line 207
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 210
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return-void
.end method

.method public getBucketId()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mBucketId:I

    return v0
.end method

.method public getBucketIdString()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 275
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v0, v0, v1

    check-cast v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->getBucketIdString()Ljava/lang/String;

    move-result-object v0

    .line 278
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 14
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v11, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mIndex:Ljava/util/TreeMap;

    if-nez v11, :cond_1

    .line 103
    const/4 v7, 0x0

    .line 165
    :cond_0
    :goto_0
    return-object v7

    .line 105
    :cond_1
    iget-object v11, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mIndex:Ljava/util/TreeMap;

    add-int/lit8 v12, p1, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/TreeMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    .line 106
    .local v2, "head":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Ljava/lang/Integer;[I>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .local v7, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-interface {v2}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 109
    sget-object v11, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->TAG:Ljava/lang/String;

    const-string v12, "Gallery: The head(SortedMap) is empty!"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 117
    :cond_2
    :try_start_0
    invoke-interface {v2}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 118
    .local v6, "markPos":I
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v2, v11}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [I

    invoke-virtual {v11}, [I->clone()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [I
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    .local v10, "subPos":[I
    iget-object v11, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v11, v11

    new-array v9, v11, [Lcom/sec/android/gallery3d/data/MediaItem;

    .line 125
    .local v9, "slot":[Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v10, :cond_0

    .line 129
    iget-object v11, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v8, v11

    .line 132
    .local v8, "size":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v8, :cond_4

    .line 133
    iget-object v11, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mFetcher:[Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;

    aget-object v1, v11, v3

    .line 134
    .local v1, "fetcher":Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;
    if-eqz v1, :cond_3

    .line 135
    aget v11, v10, v3

    invoke-virtual {v1, v11}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v11

    aput-object v11, v9, v3

    .line 132
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 119
    .end local v1    # "fetcher":Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;
    .end local v3    # "i":I
    .end local v6    # "markPos":I
    .end local v8    # "size":I
    .end local v9    # "slot":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v10    # "subPos":[I
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/util/NoSuchElementException;
    goto :goto_0

    .line 138
    .end local v0    # "e":Ljava/util/NoSuchElementException;
    .restart local v3    # "i":I
    .restart local v6    # "markPos":I
    .restart local v8    # "size":I
    .restart local v9    # "slot":[Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v10    # "subPos":[I
    :cond_4
    move v3, v6

    :goto_2
    add-int v11, p1, p2

    if-ge v3, v11, :cond_0

    .line 139
    const/4 v5, -0x1

    .line 140
    .local v5, "k":I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_3
    if-ge v4, v8, :cond_7

    .line 141
    aget-object v11, v9, v4

    if-eqz v11, :cond_6

    .line 142
    const/4 v11, -0x1

    if-eq v5, v11, :cond_5

    iget-object v11, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mComparator:Ljava/util/Comparator;

    aget-object v12, v9, v4

    aget-object v13, v9, v5

    invoke-interface {v11, v12, v13}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v11

    if-gez v11, :cond_6

    .line 143
    :cond_5
    move v5, v4

    .line 140
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 149
    :cond_7
    const/4 v11, -0x1

    if-eq v5, v11, :cond_0

    .line 152
    aget v11, v10, v5

    add-int/lit8 v11, v11, 0x1

    aput v11, v10, v5

    .line 153
    if-lt v3, p1, :cond_8

    .line 154
    aget-object v11, v9, v5

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    :cond_8
    iget-object v11, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mFetcher:[Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;

    aget-object v11, v11, v5

    if-eqz v11, :cond_9

    .line 157
    iget-object v11, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mFetcher:[Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;

    aget-object v11, v11, v5

    aget v12, v10, v5

    invoke-virtual {v11, v12}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum$FetchCache;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v11

    aput-object v11, v9, v5

    .line 160
    :cond_9
    iget-object v11, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mIndex:Ljava/util/TreeMap;

    if-eqz v11, :cond_a

    add-int/lit8 v11, v3, 0x1

    rem-int/lit8 v11, v11, 0x40

    if-nez v11, :cond_a

    .line 161
    iget-object v11, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mIndex:Ljava/util/TreeMap;

    add-int/lit8 v12, v3, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10}, [I->clone()Ljava/lang/Object;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->getTotalMediaItemCount()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 6

    .prologue
    .line 198
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v4, v4

    if-nez v4, :cond_0

    const-wide/16 v2, 0x0

    .line 199
    .local v2, "supported":J
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v1, v4

    .local v1, "n":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 200
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v4

    and-long/2addr v2, v4

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 198
    .end local v0    # "i":I
    .end local v1    # "n":I
    .end local v2    # "supported":J
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 202
    .restart local v0    # "i":I
    .restart local v1    # "n":I
    .restart local v2    # "supported":J
    :cond_1
    return-wide v2
.end method

.method public getTotalMediaItemCount()I
    .locals 6

    .prologue
    .line 170
    const/4 v1, 0x0

    .line 171
    .local v1, "count":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 172
    .local v4, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v5

    add-int/2addr v1, v5

    .line 171
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 174
    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return v1
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x1

    return v0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->notifyContentChanged()V

    .line 194
    return-void
.end method

.method public reload()J
    .locals 8

    .prologue
    .line 179
    const/4 v0, 0x0

    .line 180
    .local v0, "changed":Z
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v2, v3

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 181
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mDataVersion:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    const/4 v0, 0x1

    .line 180
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 183
    :cond_1
    if-eqz v0, :cond_2

    .line 184
    invoke-static {}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->nextVersionNumber()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mDataVersion:J

    .line 185
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->updateData()V

    .line 186
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->invalidateCache()V

    .line 188
    :cond_2
    iget-wide v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mDataVersion:J

    return-wide v4
.end method

.method public rotate(I)V
    .locals 4
    .param p1, "degrees"    # I

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 215
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->rotate(I)V

    .line 214
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 217
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return-void
.end method
