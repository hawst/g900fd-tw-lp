.class Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;
.super Ljava/lang/Object;
.source "WidgetService.java"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/gadget/WidgetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PhotoRVFactory"
.end annotation


# instance fields
.field private QUEUE_COUNT_FUTURE_BMP:I

.field private mAlbumPath:Ljava/lang/String;

.field private final mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mAppWidgetId:I

.field private mFrameType:I

.field private mFutureBmpMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mFuturePosition:I

.field private mFutureThread:Ljava/lang/Thread;

.field private mIsEmptyViewNow:Z

.field private mIsInit:Z

.field private mIsThreadStop:Z

.field private mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

.field private mSourceSize:I

.field private mTempContext:Landroid/content/Context;

.field private mType:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;IILjava/lang/String;I)V
    .locals 3
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "id"    # I
    .param p3, "type"    # I
    .param p4, "albumPath"    # Ljava/lang/String;
    .param p5, "frameType"    # I

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    .line 236
    iput v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFuturePosition:I

    .line 237
    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mTempContext:Landroid/content/Context;

    .line 238
    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    .line 239
    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 240
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->QUEUE_COUNT_FUTURE_BMP:I

    .line 241
    iput v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSourceSize:I

    .line 243
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsInit:Z

    .line 244
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsThreadStop:Z

    .line 247
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 248
    iput p2, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    .line 249
    iput p3, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mType:I

    .line 250
    iput-object p4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAlbumPath:Ljava/lang/String;

    .line 251
    iput p5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFrameType:I

    .line 252
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v0, :cond_0

    .line 253
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 255
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;

    .prologue
    .line 227
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFuturePosition:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsInit:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;
    .param p1, "x1"    # Z

    .prologue
    .line 227
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsInit:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;)Lcom/sec/android/gallery3d/gadget/WidgetSource;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mTempContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;

    .prologue
    .line 227
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->QUEUE_COUNT_FUTURE_BMP:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;

    .prologue
    .line 227
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSourceSize:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsThreadStop:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;
    .param p1, "x1"    # Ljava/lang/Thread;

    .prologue
    .line 227
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    return-object p1
.end method

.method private initSource()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 265
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v5, :cond_0

    .line 266
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->close()V

    .line 268
    :cond_0
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v5, :cond_3

    .line 269
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    if-eqz v5, :cond_2

    .line 270
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->isAlive()Z

    move-result v5

    if-ne v5, v6, :cond_1

    .line 271
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 272
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsThreadStop:Z

    .line 274
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    .line 275
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 276
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 278
    :cond_2
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsInit:Z

    .line 281
    :cond_3
    iget v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mType:I

    packed-switch v5, :pswitch_data_0

    .line 315
    :cond_4
    :goto_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v5, :cond_5

    .line 316
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v5, p0}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->setContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 317
    :cond_5
    return-void

    .line 283
    :pswitch_0
    new-instance v5, Lcom/sec/android/gallery3d/gadget/WidgetService$EmptySource;

    invoke-direct {v5}, Lcom/sec/android/gallery3d/gadget/WidgetService$EmptySource;-><init>()V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    goto :goto_0

    .line 286
    :pswitch_1
    new-instance v5, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    iget-object v6, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-direct {v5, v6, v7}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    goto :goto_0

    .line 289
    :pswitch_2
    const/4 v4, 0x0

    .line 291
    .local v4, "succeed":Z
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAlbumPath:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 292
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 293
    .local v1, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 294
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_6

    .line 295
    new-instance v5, Lcom/sec/android/gallery3d/gadget/MediaSetSource;

    iget-object v6, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAlbumPath:Ljava/lang/String;

    invoke-direct {v5, v1, v6}, Lcom/sec/android/gallery3d/gadget/MediaSetSource;-><init>(Lcom/sec/android/gallery3d/data/DataManager;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    const/4 v4, 0x1

    .line 304
    .end local v1    # "manager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_6
    :goto_1
    if-nez v4, :cond_4

    .line 305
    new-instance v5, Lcom/sec/android/gallery3d/gadget/WidgetService$EmptySource;

    invoke-direct {v5}, Lcom/sec/android/gallery3d/gadget/WidgetService$EmptySource;-><init>()V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    goto :goto_0

    .line 298
    :catch_0
    move-exception v0

    .line 299
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v5, "GalleryAppWidgetService"

    const-string v6, "exception while initiating album set path : "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 308
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .end local v4    # "succeed":Z
    :pswitch_3
    new-instance v5, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    iget-object v6, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-direct {v5, v6, v7}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    goto :goto_0

    .line 311
    :pswitch_4
    new-instance v5, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;

    iget-object v6, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    goto :goto_0

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private updateEmptyView(Z)V
    .locals 11
    .param p1, "showEmpty"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const v10, 0x7f0f016c

    const v9, 0x7f0f0169

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 354
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 357
    .local v2, "manager":Landroid/appwidget/AppWidgetManager;
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 358
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    if-eqz p1, :cond_0

    .line 359
    const-string v4, "GalleryAppWidgetService"

    const-string/jumbo v5, "show content view"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    new-instance v3, Landroid/widget/RemoteViews;

    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0300b6

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 362
    .local v3, "views":Landroid/widget/RemoteViews;
    invoke-virtual {v3, v9, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 363
    invoke-virtual {v3, v10, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 364
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    .line 365
    iput v7, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 376
    :goto_0
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/sec/android/gallery3d/gadget/WidgetService;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 377
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "appWidgetId"

    iget v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 378
    const-string/jumbo v4, "widget-type"

    iget v5, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 379
    const-string v4, "frame-type"

    iget v5, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 380
    const-string v4, "album-path"

    iget-object v5, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 381
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "widget://gallery/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 383
    const v4, 0x7f0f016a

    invoke-virtual {v3, v4, v1}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    .line 384
    const v4, 0x7f0f01e8

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->createPreferencePIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 386
    const v4, 0x7f0f016d

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->createTabToAddPIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 388
    const v4, 0x7f0f01e7

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->createTabToAddPIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 391
    iget v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-virtual {v2, v4, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 392
    return-void

    .line 367
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "views":Landroid/widget/RemoteViews;
    :cond_0
    const-string v4, "GalleryAppWidgetService"

    const-string/jumbo v5, "show content view"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    new-instance v3, Landroid/widget/RemoteViews;

    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getIntervalLayoutResId(Landroid/content/Context;I)I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 371
    .restart local v3    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {v3, v10, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 372
    invoke-virtual {v3, v9, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 373
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    goto/16 :goto_0
.end method


# virtual methods
.method protected createClickPIntent(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/high16 v3, 0x8000000

    .line 568
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/gallery3d/gadget/WidgetClickHandler;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 572
    .local v0, "clickIntent":Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-static {p1, v1, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public getCount()I
    .locals 7

    .prologue
    .line 335
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v2

    .line 336
    .local v2, "size":I
    if-lez v2, :cond_1

    .line 338
    iget v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFrameType:I

    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getSubframeCount(I)I

    move-result v3

    .line 339
    .local v3, "subframeCount":I
    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    .line 340
    div-int v0, v2, v3

    .line 341
    .local v0, "dividend":I
    rem-int v1, v2, v3

    .line 342
    .local v1, "remain":I
    if-lez v1, :cond_0

    .line 343
    add-int/lit8 v0, v0, 0x1

    .line 345
    :cond_0
    move v2, v0

    .line 348
    .end local v0    # "dividend":I
    .end local v1    # "remain":I
    .end local v3    # "subframeCount":I
    :cond_1
    const-string v4, "GalleryAppWidgetService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "page count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    return v2
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 395
    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 3

    .prologue
    .line 407
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0300b5

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 408
    .local v0, "rv":Landroid/widget/RemoteViews;
    return-object v0
.end method

.method public getSingleFrame(I)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "position"    # I

    .prologue
    .line 412
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v10}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    .line 413
    .local v1, "context":Landroid/content/Context;
    const/4 v8, 0x0

    .line 414
    .local v8, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v0, 0x0

    .line 416
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v10, :cond_0

    .line 417
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v10, p1}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    .line 418
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v10}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v10

    iput v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSourceSize:I

    .line 422
    :cond_0
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    move-result v10

    if-gtz v10, :cond_3

    if-eqz v8, :cond_3

    .line 423
    const-string v10, "GalleryAppWidgetService"

    const-string v11, "mFutureBitmap == null && position == 0"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v1, v8, v10, v11}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 462
    :cond_1
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsInit:Z

    const/4 v11, 0x1

    if-ne v10, v11, :cond_b

    const/4 v10, 0x0

    iput v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFuturePosition:I

    .line 465
    :goto_0
    iput-object v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mTempContext:Landroid/content/Context;

    .line 467
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    if-nez v10, :cond_2

    .line 468
    new-instance v10, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory$1;

    invoke-direct {v10, p0}, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory$1;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;)V

    iput-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    .line 501
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 502
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 504
    :cond_2
    return-object v0

    .line 426
    :cond_3
    if-eqz v8, :cond_1

    .line 427
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 428
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v1, v8, v10, v11}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 440
    :cond_4
    :goto_1
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    if-nez v10, :cond_1

    iget-boolean v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsInit:Z

    if-nez v10, :cond_1

    .line 441
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 442
    .local v4, "futureIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 443
    .local v9, "removeIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    iget v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->QUEUE_COUNT_FUTURE_BMP:I

    if-ge v5, v10, :cond_7

    add-int v10, p1, v5

    iget v11, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSourceSize:I

    rem-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 429
    .end local v4    # "futureIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v5    # "i":I
    .end local v9    # "removeIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_5
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 430
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_6

    .line 431
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v1, v8, v10, v11}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 432
    iget-object v11, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    monitor-enter v11

    .line 433
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    monitor-exit v11

    goto :goto_1

    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 437
    :cond_6
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    iget-object v11, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_1

    .line 445
    .restart local v4    # "futureIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v5    # "i":I
    .restart local v9    # "removeIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_7
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 446
    .local v6, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;>;"
    :cond_8
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 447
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 448
    .local v3, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 449
    .local v7, "key":I
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 451
    .end local v3    # "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    .end local v7    # "key":I
    :cond_9
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 452
    .local v2, "count":I
    const/4 v7, 0x0

    .restart local v7    # "key":I
    :goto_4
    if-ge v7, v2, :cond_1

    .line 453
    iget-object v11, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    monitor-enter v11

    .line 454
    :try_start_1
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_a

    .line 455
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 456
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    :cond_a
    monitor-exit v11

    .line 452
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 458
    :catchall_1
    move-exception v10

    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v10

    .line 463
    .end local v2    # "count":I
    .end local v4    # "futureIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v5    # "i":I
    .end local v6    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;>;"
    .end local v7    # "key":I
    .end local v9    # "removeIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_b
    add-int/lit8 v10, p1, 0x1

    iget v11, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSourceSize:I

    rem-int/2addr v10, v11

    iput v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFuturePosition:I

    goto/16 :goto_0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 19
    .param p1, "position"    # I

    .prologue
    .line 510
    const-string v16, "GalleryAppWidgetService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "WidgetId : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ", getViewAt : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v14

    .line 513
    .local v14, "token":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    .line 514
    .local v3, "context":Landroid/content/Context;
    new-instance v6, Landroid/widget/RemoteViews;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v16

    const v17, 0x7f0300b1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v6, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 516
    .local v6, "flipperFrame":Landroid/widget/RemoteViews;
    :try_start_0
    new-instance v5, Landroid/widget/RemoteViews;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFrameType:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getLayoutResId(I)I

    move-result v17

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v5, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 519
    .local v5, "flipperContent":Landroid/widget/RemoteViews;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFrameType:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getSubframeCount(I)I

    move-result v12

    .line 520
    .local v12, "subframeCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v11

    .line 522
    .local v11, "size":I
    if-lez v11, :cond_7

    .line 524
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v12, :cond_7

    .line 525
    mul-int v16, p1, v12

    add-int v16, v16, v7

    rem-int v9, v16, v11

    .line 527
    .local v9, "newPosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v9}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getContentUri(I)Landroid/net/Uri;

    move-result-object v13

    .line 528
    .local v13, "uri":Landroid/net/Uri;
    if-nez v13, :cond_1

    .line 524
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 531
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v9}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->isDrm(I)Z

    move-result v8

    .line 533
    .local v8, "isdrm":Z
    const/4 v2, 0x0

    .line 534
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v13}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v16

    const-string v17, "/picasa/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_2

    if-eqz v8, :cond_5

    .line 535
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v9}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getImage(I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 544
    :cond_3
    :goto_2
    if-eqz v2, :cond_0

    .line 545
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v0, v13}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExifOrientation(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v10

    .line 546
    .local v10, "rotation":I
    if-eqz v10, :cond_4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v16

    if-nez v16, :cond_4

    .line 547
    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-static {v2, v10, v0}, Lcom/sec/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 549
    :cond_4
    const v16, 0x7f0f01ea

    add-int v16, v16, v7

    move/from16 v0, v16

    invoke-virtual {v5, v0, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 550
    const v16, 0x7f0f01ea

    add-int v16, v16, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v13}, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->createClickPIntent(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v17

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v5, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 557
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "flipperContent":Landroid/widget/RemoteViews;
    .end local v7    # "i":I
    .end local v8    # "isdrm":Z
    .end local v9    # "newPosition":I
    .end local v10    # "rotation":I
    .end local v11    # "size":I
    .end local v12    # "subframeCount":I
    .end local v13    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v4

    .line 558
    .local v4, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_1
    const-string v16, "GalleryAppWidgetService"

    const-string v17, "caught array index out of bound exception. data might be changed while sleep mode"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 562
    invoke-static {v14, v15}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 564
    .end local v4    # "e":Ljava/lang/IndexOutOfBoundsException;
    :goto_3
    return-object v6

    .line 537
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "flipperContent":Landroid/widget/RemoteViews;
    .restart local v7    # "i":I
    .restart local v8    # "isdrm":Z
    .restart local v9    # "newPosition":I
    .restart local v11    # "size":I
    .restart local v12    # "subframeCount":I
    .restart local v13    # "uri":Landroid/net/Uri;
    :cond_5
    :try_start_2
    sget-boolean v16, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v16, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFrameType:I

    move/from16 v16, v0

    if-nez v16, :cond_6

    .line 538
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->getSingleFrame(I)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_2

    .line 539
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v9}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v16

    if-eqz v16, :cond_3

    .line 540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v9}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFrameType:I

    move/from16 v17, v0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v3, v0, v1, v7}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v2

    goto/16 :goto_2

    .line 556
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v7    # "i":I
    .end local v8    # "isdrm":Z
    .end local v9    # "newPosition":I
    .end local v13    # "uri":Landroid/net/Uri;
    :cond_7
    const v16, 0x7f0f01e9

    move/from16 v0, v16

    invoke-virtual {v6, v0, v5}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 562
    invoke-static {v14, v15}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_3

    .line 559
    .end local v5    # "flipperContent":Landroid/widget/RemoteViews;
    .end local v11    # "size":I
    .end local v12    # "subframeCount":I
    :catch_1
    move-exception v4

    .line 560
    .local v4, "e":Ljava/lang/NullPointerException;
    :try_start_3
    const-string v16, "GalleryAppWidgetService"

    const-string v17, "caught null pointer exception. check source is deleted"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 562
    invoke-static {v14, v15}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_3

    .end local v4    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v16

    invoke-static {v14, v15}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v16
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 399
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 403
    const/4 v0, 0x1

    return v0
.end method

.method public onContentDirty()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 605
    const-string v0, "GalleryAppWidgetService"

    const-string v1, "onContentDirty"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mType:I

    if-ne v0, v2, :cond_2

    .line 608
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v0

    if-ge v0, v2, :cond_1

    .line 609
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->updateEmptyView(Z)V

    .line 618
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    const v2, 0x7f0f016a

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 620
    return-void

    .line 611
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->updateEmptyView(Z)V

    goto :goto_0

    .line 613
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    if-eqz v0, :cond_0

    .line 614
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v0

    if-ge v0, v2, :cond_0

    .line 615
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->updateEmptyView(Z)V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 259
    const-string v0, "GalleryAppWidgetService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->initSource()V

    .line 262
    return-void
.end method

.method public onDataSetChanged()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 577
    const-string v4, "GalleryAppWidgetService"

    const-string v5, "onDataSetChanged"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 580
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 581
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    iget v4, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    iput v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mType:I

    .line 582
    iget v4, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    iput v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFrameType:I

    .line 583
    iget-object v4, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mAlbumPath:Ljava/lang/String;

    .line 584
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 586
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->initSource()V

    .line 588
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 590
    .local v2, "token":J
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v4, :cond_0

    .line 591
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->reload()V

    .line 594
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v4

    if-ge v4, v6, :cond_1

    .line 595
    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->updateEmptyView(Z)V

    .line 600
    :cond_0
    :goto_0
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 601
    return-void

    .line 597
    :cond_1
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->updateEmptyView(Z)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 321
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->close()V

    .line 323
    iput-object v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    .line 326
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v0, :cond_1

    .line 327
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 329
    iput-object v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 332
    :cond_1
    return-void
.end method
