.class final Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter$1;
.super Ljava/lang/Thread;
.source "SNSSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->startSync(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter$1;->val$context:Landroid/content/Context;

    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 51
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 52
    const-string v0, "content://com.sec.android.app.sns3.sp.facebook/sync_album"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 53
    .local v1, "albumUri":Landroid/net/Uri;
    const-string v0, "content://com.sec.android.app.sns3.sp.facebook/sync_gallery/album_id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 54
    .local v2, "photoUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter$1;->val$context:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter$1;->getId()J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->performSync(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;ZJ)Z

    .line 55
    return-void
.end method
