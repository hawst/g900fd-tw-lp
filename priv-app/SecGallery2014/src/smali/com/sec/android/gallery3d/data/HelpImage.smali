.class public Lcom/sec/android/gallery3d/data/HelpImage;
.super Lcom/sec/android/gallery3d/data/MediaItem;
.source "HelpImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;,
        Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;
    }
.end annotation


# static fields
.field static final ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final STATE_DOWNLOADED:I = 0x2

.field private static final STATE_DOWNLOADING:I = 0x1

.field private static final STATE_ERROR:I = -0x1

.field private static final STATE_INIT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "HelpImage"


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mCacheEntry:Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

.field private final mContentType:Ljava/lang/String;

.field private mFileDescriptor:Landroid/os/ParcelFileDescriptor;

.field private mHeight:I

.field private mRotation:I

.field private mState:I

.field private final mUri:Landroid/net/Uri;

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-string v0, "/uri/help/mediaset/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/HelpImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Landroid/net/Uri;)V
    .locals 3
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 68
    invoke-static {}, Lcom/sec/android/gallery3d/data/HelpImage;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p2, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 59
    iput v2, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mState:I

    .line 60
    iput v2, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mWidth:I

    .line 61
    iput v2, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mHeight:I

    .line 69
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    .line 70
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 71
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/data/HelpImage;->getMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mContentType:Ljava/lang/String;

    .line 72
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/HelpImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/HelpImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/HelpImage;->prepareInputFile(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/data/HelpImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/HelpImage;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/data/HelpImage;)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/HelpImage;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/data/HelpImage;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/HelpImage;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mWidth:I

    return p1
.end method

.method static synthetic access$502(Lcom/sec/android/gallery3d/data/HelpImage;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/HelpImage;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mHeight:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/data/HelpImage;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/HelpImage;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/HelpImage;->getResourceName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/data/HelpImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/HelpImage;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/HelpImage;->getResourceId()I

    move-result v0

    return v0
.end method

.method private getMimeType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 75
    const-string v3, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 76
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/data/HelpImage;->isdrm:Z

    if-eqz v3, :cond_1

    .line 77
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 95
    :cond_0
    :goto_0
    return-object v2

    .line 80
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "extension":Ljava/lang/String;
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "type":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 85
    if-nez v2, :cond_3

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/HelpImage;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 87
    .local v1, "filePath":Ljava/lang/String;
    if-eqz v1, :cond_3

    const-string v3, ".gif"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 88
    const-string v2, "image/gif"

    goto :goto_0

    .line 91
    .end local v0    # "extension":Ljava/lang/String;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "type":Ljava/lang/String;
    :cond_2
    const-string v3, "http"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 92
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->parseMimetypeFromHttpUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 95
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private getResourceId()I
    .locals 2

    .prologue
    .line 409
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "help_1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    const v0, 0x7f0202c7

    .line 416
    :goto_0
    return v0

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "help_2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 412
    const v0, 0x7f0202c8

    goto :goto_0

    .line 413
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "help_3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 414
    const v0, 0x7f0202c9

    goto :goto_0

    .line 416
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private getResourceName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "help_1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    const-string v0, "gallery_help_1"

    .line 405
    :goto_0
    return-object v0

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "help_2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 401
    const-string v0, "gallery_help_2"

    goto :goto_0

    .line 402
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "help_3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 403
    const-string v0, "gallery_help_3"

    goto :goto_0

    .line 405
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private openFileOrDownloadTempFile(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)V
    .locals 3
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/HelpImage;->openOrDownloadInner(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)I

    move-result v0

    .line 110
    .local v0, "state":I
    monitor-enter p0

    .line 111
    :try_start_0
    iput v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mState:I

    .line 112
    iget v1, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 113
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    .line 115
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 118
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 119
    monitor-exit p0

    .line 120
    return-void

    .line 119
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private openOrDownloadInner(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)I
    .locals 12
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x0

    const/4 v7, -0x1

    .line 123
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    .line 124
    .local v3, "scheme":Ljava/lang/String;
    const/4 v1, 0x0

    .line 125
    .local v1, "is":Ljava/io/InputStream;
    const-string v9, "content"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "android.resource"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "file"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 128
    :cond_0
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mContentType:Ljava/lang/String;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isJpeg(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 129
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v9, v10}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 130
    invoke-static {v1}, Lcom/sec/android/gallery3d/data/Exif;->getOrientation(Ljava/io/InputStream;)I

    move-result v9

    iput v9, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mRotation:I

    .line 132
    :cond_1
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    const-string v11, "r"

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 133
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-eqz v7, :cond_2

    .line 140
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 163
    :goto_0
    return v6

    .line 140
    :cond_2
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move v6, v8

    goto :goto_0

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_1
    const-string v6, "HelpImage"

    const-string v8, "fail to open: "

    invoke-static {v6, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move v6, v7

    goto :goto_0

    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v6

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v6

    .line 144
    :cond_3
    :try_start_2
    new-instance v9, Ljava/net/URI;

    iget-object v10, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v5

    .line 145
    .local v5, "url":Ljava/net/URL;
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDownloadCache()Lcom/sec/android/gallery3d/data/DownloadCache;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-virtual {v9, p1, v5, v10, v11}, Lcom/sec/android/gallery3d/data/DownloadCache;->download(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;Ljava/lang/String;Z)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mCacheEntry:Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    .line 146
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v9

    if-eqz v9, :cond_4

    .line 163
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .line 148
    :cond_4
    :try_start_3
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mCacheEntry:Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    if-nez v6, :cond_5

    .line 149
    const-string v6, "HelpImage"

    const-string v8, "download failed "

    invoke-static {v6, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 163
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move v6, v7

    goto :goto_0

    .line 152
    :cond_5
    :try_start_4
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mContentType:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isJpeg(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 153
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mCacheEntry:Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    iget-object v6, v6, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    invoke-direct {v2, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 154
    .end local v1    # "is":Ljava/io/InputStream;
    .local v2, "is":Ljava/io/InputStream;
    :try_start_5
    invoke-static {v2}, Lcom/sec/android/gallery3d/data/Exif;->getOrientation(Ljava/io/InputStream;)I

    move-result v6

    iput v6, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mRotation:I
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-object v1, v2

    .line 157
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    :cond_6
    :try_start_6
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mCacheEntry:Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    iget-object v6, v6, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    const/high16 v9, 0x10000000

    invoke-static {v6, v9}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 163
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move v6, v8

    goto :goto_0

    .line 159
    .end local v5    # "url":Ljava/net/URL;
    :catch_1
    move-exception v4

    .line 160
    .local v4, "t":Ljava/lang/Throwable;
    :goto_1
    :try_start_7
    const-string v6, "HelpImage"

    const-string v8, "download error"

    invoke-static {v6, v8, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 163
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move v6, v7

    goto/16 :goto_0

    .end local v4    # "t":Ljava/lang/Throwable;
    :catchall_1
    move-exception v6

    :goto_2
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v6

    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    .restart local v5    # "url":Ljava/net/URL;
    :catchall_2
    move-exception v6

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_2

    .line 159
    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    :catch_2
    move-exception v4

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_1
.end method

.method private prepareInputFile(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z
    .locals 5
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    const/4 v0, 0x0

    .line 169
    iget v2, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mState:I

    if-ne v2, v4, :cond_0

    .line 170
    iput v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mState:I

    .line 172
    :cond_0
    new-instance v2, Lcom/sec/android/gallery3d/data/HelpImage$1;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/data/HelpImage$1;-><init>(Lcom/sec/android/gallery3d/data/HelpImage;)V

    invoke-interface {p1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setCancelListener(Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;)V

    .line 182
    :goto_0
    monitor-enter p0

    .line 183
    :try_start_0
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 184
    monitor-exit p0

    .line 191
    :goto_1
    return v0

    .line 185
    :cond_1
    iget v2, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mState:I

    if-nez v2, :cond_2

    .line 186
    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mState:I

    .line 200
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/HelpImage;->openFileOrDownloadTempFile(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)V

    goto :goto_0

    .line 188
    :cond_2
    :try_start_1
    iget v2, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mState:I

    if-ne v2, v4, :cond_3

    .line 189
    monitor-exit p0

    goto :goto_1

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 190
    :cond_3
    :try_start_2
    iget v2, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 191
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    goto :goto_1

    .line 194
    :cond_4
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 198
    :goto_2
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 195
    :catch_0
    move-exception v2

    goto :goto_2
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 361
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 367
    return-void

    .line 365
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0xd

    const/16 v7, 0xc

    .line 322
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v1

    .line 323
    .local v1, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    iget v5, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mWidth:I

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mHeight:I

    if-eqz v5, :cond_0

    .line 324
    iget v5, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mWidth:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v7, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 325
    iget v5, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mHeight:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v8, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 327
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mContentType:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 328
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mContentType:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 330
    :cond_1
    const-string v5, "file"

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 331
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 332
    .local v3, "filePath":Ljava/lang/String;
    const/16 v5, 0xc8

    invoke-virtual {v1, v5, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 333
    invoke-static {v1, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->extractExifInfo(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)V

    .line 334
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 335
    iget v5, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mWidth:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v7, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->replaceDetail(ILjava/lang/Object;)V

    .line 337
    :cond_2
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v8}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 338
    iget v5, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mHeight:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v8, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->replaceDetail(ILjava/lang/Object;)V

    .line 340
    :cond_3
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/data/HelpImage;->isdrm:Z

    if-eqz v5, :cond_5

    .line 341
    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 342
    .local v4, "split":[Ljava/lang/String;
    array-length v5, v4

    if-lez v5, :cond_4

    .line 343
    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    aget-object v0, v4, v5

    .line 344
    .local v0, "caption":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-virtual {v1, v5, v0}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 346
    .end local v0    # "caption":Ljava/lang/String;
    :cond_4
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v5

    invoke-virtual {v5, v1, v3}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDetails(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v1

    .end local v4    # "split":[Ljava/lang/String;
    :cond_5
    move-object v2, v1

    .line 350
    .end local v1    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    .end local v3    # "filePath":Ljava/lang/String;
    .local v2, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    :goto_0
    return-object v2

    .end local v2    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    .restart local v1    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    :cond_6
    move-object v2, v1

    .end local v1    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    .restart local v2    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    goto :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 386
    const-string v0, "file"

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 389
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 394
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x0

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x2

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mContentType:Ljava/lang/String;

    return-object v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 381
    iget v0, p0, Lcom/sec/android/gallery3d/data/HelpImage;->mRotation:I

    return v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 306
    const-wide/16 v0, 0x204

    .line 307
    .local v0, "operation":J
    return-wide v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 371
    const/4 v0, 0x0

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v0, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;-><init>(Lcom/sec/android/gallery3d/data/HelpImage;I)V

    return-object v0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    new-instance v0, Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;-><init>(Lcom/sec/android/gallery3d/data/HelpImage;Lcom/sec/android/gallery3d/data/HelpImage$1;)V

    return-object v0
.end method
