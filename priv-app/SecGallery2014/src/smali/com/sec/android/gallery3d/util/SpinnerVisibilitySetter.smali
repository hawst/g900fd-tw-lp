.class public Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;
.super Ljava/lang/Object;
.source "SpinnerVisibilitySetter.java"


# static fields
.field private static final MIN_SPINNER_DISPLAY_TIME:J = 0x7d0L

.field private static final MSG_HIDE_SPINNER:I = 0x2

.field private static final MSG_RELEASE_INSTANCE:I = 0x3

.field private static final MSG_SHOW_SPINNER:I = 0x1

.field private static final SPINNER_DISPLAY_DELAY:J = 0x3e8L

.field static final sInstanceMap:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/app/Activity;",
            "Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mHandler:Landroid/os/Handler;

.field private mSpinnerVisibilityStartTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->sInstanceMap:Ljava/util/WeakHashMap;

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mSpinnerVisibilityStartTime:J

    .line 53
    new-instance v0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;-><init>(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mHandler:Landroid/os/Handler;

    .line 105
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mActivity:Landroid/app/Activity;

    .line 106
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mSpinnerVisibilityStartTime:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;
    .param p1, "x1"    # J

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mSpinnerVisibilityStartTime:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public static getInstance(Landroid/app/Activity;)Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 89
    sget-object v2, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->sInstanceMap:Ljava/util/WeakHashMap;

    monitor-enter v2

    .line 90
    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->sInstanceMap:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    .line 91
    .local v0, "setter":Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;
    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    .end local v0    # "setter":Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;
    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;-><init>(Landroid/app/Activity;)V

    .line 93
    .restart local v0    # "setter":Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;
    sget-object v1, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->sInstanceMap:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    :cond_0
    monitor-exit v2

    return-object v0

    .line 96
    .end local v0    # "setter":Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public hideAndRelease()V
    .locals 2

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->setSpinnerVisibility(Z)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 102
    return-void
.end method

.method public setSpinnerVisibility(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 109
    if-eqz p1, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 116
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
