.class public Lcom/sec/android/gallery3d/glcore/GlBackground;
.super Ljava/lang/Object;
.source "GlBackground.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/GlBackground$1;,
        Lcom/sec/android/gallery3d/glcore/GlBackground$Transitioner;
    }
.end annotation


# static fields
.field private static final BLUE_MASK:I = 0xff

.field private static final DEF_DELTA:F = 0.01f

.field private static final GREEN_MASK:I = 0xff00

.field private static final GREEN_MASK_SHIFT:I = 0x8

.field private static final KERNEL_NORM:[I

.field private static final KERNEL_SIZE:I = 0x9

.field private static final MAX_COLOR_VALUE:I = 0xff

.field private static final NUM_COLORS:I = 0x100

.field private static final RADIUS:I = 0x4

.field private static final RED_MASK:I = 0xff0000

.field private static final RED_MASK_SHIFT:I = 0x10

.field private static final START_FADE_X:I = 0x60

.field private static final TAG:Ljava/lang/String; = "GlBackground"

.field private static final THUMBNAIL_MAX_X:I = 0x80


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field mConvRatio:F

.field private mCurrent:I

.field mDelta:F

.field private mGL:Ljavax/microedition/khronos/opengles/GL11;

.field private final mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mHeight:I

.field private mNeedTransition:Z

.field private mRsrcID:I

.field private mTextureActive:[I

.field private mTextureID:[I

.field private final mTransitor:Lcom/sec/android/gallery3d/glcore/GlBackground$Transitioner;

.field private mUseBlending:Z

.field private mUseClearColor:Z

.field private mUseTransitioner:Z

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 55
    const/16 v1, 0x900

    new-array v1, v1, [I

    sput-object v1, Lcom/sec/android/gallery3d/glcore/GlBackground;->KERNEL_NORM:[I

    .line 74
    const/16 v0, 0x8ff

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 75
    sget-object v1, Lcom/sec/android/gallery3d/glcore/GlBackground;->KERNEL_NORM:[I

    div-int/lit8 v2, v0, 0x9

    aput v2, v1, v0

    .line 74
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 77
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljavax/microedition/khronos/opengles/GL11;Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "gl"    # Ljavax/microedition/khronos/opengles/GL11;
    .param p3, "rootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mCurrent:I

    .line 37
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureID:[I

    .line 38
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureActive:[I

    .line 40
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mNeedTransition:Z

    .line 63
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mRsrcID:I

    .line 64
    iput-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mBitmap:Landroid/graphics/Bitmap;

    .line 65
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseClearColor:Z

    .line 66
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseBlending:Z

    .line 67
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseTransitioner:Z

    .line 69
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mConvRatio:F

    .line 70
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mDelta:F

    .line 80
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mContext:Landroid/content/Context;

    .line 81
    iput-object p2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 82
    iput-object p3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureID:[I

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLUtilInstance()Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/TUtils;->getBase(I)I

    move-result v1

    aput v1, v0, v2

    .line 84
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureID:[I

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLUtilInstance()Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/glcore/TUtils;->getBase(I)I

    move-result v1

    aput v1, v0, v3

    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureID:[I

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLUtilInstance()Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/glcore/TUtils;->getBase(I)I

    move-result v1

    aput v1, v0, v4

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureActive:[I

    const v1, 0x84c0

    aput v1, v0, v2

    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureActive:[I

    const v1, 0x84c1

    aput v1, v0, v3

    .line 89
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlBackground$Transitioner;

    invoke-direct {v0, p0, v5}, Lcom/sec/android/gallery3d/glcore/GlBackground$Transitioner;-><init>(Lcom/sec/android/gallery3d/glcore/GlBackground;Lcom/sec/android/gallery3d/glcore/GlBackground$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTransitor:Lcom/sec/android/gallery3d/glcore/GlBackground$Transitioner;

    .line 90
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/glcore/GlBackground;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlBackground;

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mNeedTransition:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/glcore/GlBackground;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlBackground;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mNeedTransition:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/glcore/GlBackground;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlBackground;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mRsrcID:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/glcore/GlBackground;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlBackground;
    .param p1, "x1"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mRsrcID:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/glcore/GlBackground;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlBackground;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/glcore/GlBackground;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlBackground;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/glcore/GlBackground;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlBackground;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mCurrent:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/glcore/GlBackground;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlBackground;
    .param p1, "x1"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mCurrent:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/glcore/GlBackground;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlBackground;
    .param p1, "x1"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlBackground;->setResourceImage(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/glcore/GlBackground;Landroid/graphics/Bitmap;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlBackground;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlBackground;->setBitmapTexture(Landroid/graphics/Bitmap;II)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/glcore/GlBackground;)Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlBackground;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method private static boxBlurFilter([I[IIII)V
    .locals 19
    .param p0, "in"    # [I
    .param p1, "out"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "startFadeX"    # I

    .prologue
    .line 303
    const/4 v7, 0x0

    .line 304
    .local v7, "inPos":I
    add-int/lit8 v8, p2, -0x1

    .line 305
    .local v8, "maxX":I
    const/16 v16, 0x0

    .local v16, "y":I
    :goto_0
    move/from16 v0, v16

    move/from16 v1, p3

    if-ge v0, v1, :cond_3

    .line 307
    const/4 v14, 0x0

    .line 308
    .local v14, "red":I
    const/4 v5, 0x0

    .line 309
    .local v5, "green":I
    const/4 v4, 0x0

    .line 310
    .local v4, "blue":I
    const/4 v6, -0x4

    .local v6, "i":I
    :goto_1
    const/16 v17, 0x4

    move/from16 v0, v17

    if-gt v6, v0, :cond_0

    .line 311
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v6, v0, v8}, Lcom/sec/android/gallery3d/glcore/GlBackground;->clamp(III)I

    move-result v17

    add-int v17, v17, v7

    aget v3, p0, v17

    .line 312
    .local v3, "argb":I
    const/high16 v17, 0xff0000

    and-int v17, v17, v3

    shr-int/lit8 v17, v17, 0x10

    add-int v14, v14, v17

    .line 313
    const v17, 0xff00

    and-int v17, v17, v3

    shr-int/lit8 v17, v17, 0x8

    add-int v5, v5, v17

    .line 314
    and-int/lit16 v0, v3, 0xff

    move/from16 v17, v0

    add-int v4, v4, v17

    .line 310
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 317
    .end local v3    # "argb":I
    :cond_0
    move/from16 v0, v16

    move/from16 v1, p4

    if-ge v0, v1, :cond_1

    const/16 v2, 0xff

    .line 320
    .local v2, "alpha":I
    :goto_2
    move/from16 v11, v16

    .line 321
    .local v11, "outPos":I
    const/4 v15, 0x0

    .local v15, "x":I
    :goto_3
    move/from16 v0, p2

    if-eq v15, v0, :cond_2

    .line 323
    shl-int/lit8 v17, v2, 0x18

    sget-object v18, Lcom/sec/android/gallery3d/glcore/GlBackground;->KERNEL_NORM:[I

    aget v18, v18, v14

    shl-int/lit8 v18, v18, 0x10

    or-int v17, v17, v18

    sget-object v18, Lcom/sec/android/gallery3d/glcore/GlBackground;->KERNEL_NORM:[I

    aget v18, v18, v5

    shl-int/lit8 v18, v18, 0x8

    or-int v17, v17, v18

    sget-object v18, Lcom/sec/android/gallery3d/glcore/GlBackground;->KERNEL_NORM:[I

    aget v18, v18, v4

    or-int v17, v17, v18

    aput v17, p1, v11

    .line 327
    add-int/lit8 v17, v15, -0x4

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v0, v1, v8}, Lcom/sec/android/gallery3d/glcore/GlBackground;->clamp(III)I

    move-result v13

    .line 328
    .local v13, "prevX":I
    add-int/lit8 v17, v15, 0x4

    add-int/lit8 v17, v17, 0x1

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v0, v1, v8}, Lcom/sec/android/gallery3d/glcore/GlBackground;->clamp(III)I

    move-result v10

    .line 329
    .local v10, "nextX":I
    add-int v17, v7, v13

    aget v12, p0, v17

    .line 330
    .local v12, "prevArgb":I
    add-int v17, v7, v10

    aget v9, p0, v17

    .line 331
    .local v9, "nextArgb":I
    const/high16 v17, 0xff0000

    and-int v17, v17, v9

    const/high16 v18, 0xff0000

    and-int v18, v18, v12

    sub-int v17, v17, v18

    shr-int/lit8 v17, v17, 0x10

    add-int v14, v14, v17

    .line 332
    const v17, 0xff00

    and-int v17, v17, v9

    const v18, 0xff00

    and-int v18, v18, v12

    sub-int v17, v17, v18

    shr-int/lit8 v17, v17, 0x8

    add-int v5, v5, v17

    .line 333
    and-int/lit16 v0, v9, 0xff

    move/from16 v17, v0

    and-int/lit16 v0, v12, 0xff

    move/from16 v18, v0

    sub-int v17, v17, v18

    add-int v4, v4, v17

    .line 334
    add-int v11, v11, p3

    .line 321
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 317
    .end local v2    # "alpha":I
    .end local v9    # "nextArgb":I
    .end local v10    # "nextX":I
    .end local v11    # "outPos":I
    .end local v12    # "prevArgb":I
    .end local v13    # "prevX":I
    .end local v15    # "x":I
    :cond_1
    sub-int v17, p3, v16

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    mul-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    sub-int v18, p3, p4

    div-int v2, v17, v18

    goto :goto_2

    .line 336
    .restart local v2    # "alpha":I
    .restart local v11    # "outPos":I
    .restart local v15    # "x":I
    :cond_2
    add-int v7, v7, p2

    .line 305
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    .line 338
    .end local v2    # "alpha":I
    .end local v4    # "blue":I
    .end local v5    # "green":I
    .end local v6    # "i":I
    .end local v11    # "outPos":I
    .end local v14    # "red":I
    .end local v15    # "x":I
    :cond_3
    return-void
.end method

.method public static final clamp(FFF)F
    .locals 1
    .param p0, "val"    # F
    .param p1, "minVal"    # F
    .param p2, "maxVal"    # F

    .prologue
    .line 350
    cmpg-float v0, p0, p1

    if-gez v0, :cond_0

    .line 355
    .end local p1    # "minVal":F
    :goto_0
    return p1

    .line 352
    .restart local p1    # "minVal":F
    :cond_0
    cmpl-float v0, p0, p2

    if-lez v0, :cond_1

    move p1, p2

    .line 353
    goto :goto_0

    :cond_1
    move p1, p0

    .line 355
    goto :goto_0
.end method

.method public static final clamp(III)I
    .locals 0
    .param p0, "val"    # I
    .param p1, "minVal"    # I
    .param p2, "maxVal"    # I

    .prologue
    .line 341
    if-ge p0, p1, :cond_0

    .line 346
    .end local p1    # "minVal":I
    :goto_0
    return p1

    .line 343
    .restart local p1    # "minVal":I
    :cond_0
    if-le p0, p2, :cond_1

    move p1, p2

    .line 344
    goto :goto_0

    :cond_1
    move p1, p0

    .line 346
    goto :goto_0
.end method

.method private drawInter()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    const/16 v6, 0xde1

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 101
    const/4 v0, 0x0

    .line 104
    .local v0, "mixed":Z
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const v3, 0x84c0

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 105
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mDelta:F

    cmpl-float v2, v2, v4

    if-nez v2, :cond_2

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mConvRatio:F

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mConvRatio:F

    cmpl-float v2, v2, v5

    if-nez v2, :cond_2

    .line 106
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureID:[I

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mCurrent:I

    aget v3, v3, v4

    invoke-interface {v2, v6, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 123
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v2, v5, v5, v5, v5}, Ljavax/microedition/khronos/opengles/GL11;->glColor4f(FFFF)V

    .line 125
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v3, 0x1700

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 126
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL11;->glLoadIdentity()V

    .line 127
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v2, v7, v7, v5}, Ljavax/microedition/khronos/opengles/GL11;->glScalef(FFF)V

    .line 128
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v3, 0x1701

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 129
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL11;->glLoadIdentity()V

    .line 130
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/4 v3, 0x6

    const/4 v4, 0x4

    invoke-interface {v2, v3, v8, v4}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 132
    if-eqz v0, :cond_1

    .line 133
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v2, v6}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 134
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const v3, 0x84c0

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 136
    :cond_1
    return-void

    .line 108
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureID:[I

    aget v3, v3, v8

    invoke-interface {v2, v6, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 109
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mConvRatio:F

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mDelta:F

    add-float v1, v2, v3

    .line 110
    .local v1, "newRatio":F
    cmpl-float v2, v1, v5

    if-lez v2, :cond_3

    .line 111
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mConvRatio:F

    .line 112
    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mDelta:F

    .line 120
    :goto_1
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mConvRatio:F

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/glcore/GlBackground;->bindMixed(F)Z

    .line 121
    const/4 v0, 0x1

    goto :goto_0

    .line 113
    :cond_3
    cmpg-float v2, v1, v4

    if-gez v2, :cond_4

    .line 114
    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mConvRatio:F

    .line 115
    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mDelta:F

    goto :goto_1

    .line 117
    :cond_4
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mConvRatio:F

    .line 118
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestLayout()V

    goto :goto_1
.end method

.method public static final resizeBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "maxSize"    # I

    .prologue
    .line 359
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 360
    .local v3, "srcWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 361
    .local v2, "srcHeight":I
    move v4, p1

    .line 362
    .local v4, "width":I
    move v0, p1

    .line 363
    .local v0, "height":I
    const/4 v1, 0x0

    .line 364
    .local v1, "needsResize":Z
    if-le v3, v2, :cond_2

    .line 365
    if-le v3, p1, :cond_0

    .line 366
    const/4 v1, 0x1

    .line 367
    mul-int v5, p1, v2

    div-int v0, v5, v3

    .line 375
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 376
    const/4 v5, 0x1

    invoke-static {p0, v4, v0, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 378
    .end local p0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    return-object p0

    .line 370
    .restart local p0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    if-le v2, p1, :cond_0

    .line 371
    const/4 v1, 0x1

    .line 372
    mul-int v5, p1, v3

    div-int v4, v5, v2

    goto :goto_0
.end method

.method private setBitmapTexture(Landroid/graphics/Bitmap;II)V
    .locals 8
    .param p1, "source"    # Landroid/graphics/Bitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v7, 0x0

    const v5, 0x47012f00    # 33071.0f

    const/16 v6, 0xde1

    .line 198
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureID:[I

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mCurrent:I

    aget v2, v3, v4

    .line 199
    .local v2, "textureId":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureActive:[I

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mCurrent:I

    aget v1, v3, v4

    .line 201
    .local v1, "textureActive":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v3, v1}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 202
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v3, v6}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 204
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mWidth:I

    .line 205
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mHeight:I

    .line 206
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseTransitioner:Z

    if-eqz v3, :cond_2

    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlBackground;->load(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 208
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v3, v6, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 209
    if-eqz v0, :cond_0

    .line 210
    invoke-static {v6, v7, v0, v7}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 211
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 213
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v4, 0x2802

    invoke-interface {v3, v6, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 214
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v4, 0x2803

    invoke-interface {v3, v6, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 215
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v4, 0x2801

    const/high16 v5, 0x46180000    # 9728.0f

    invoke-interface {v3, v6, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 216
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v4, 0x2800

    const v5, 0x46180400    # 9729.0f

    invoke-interface {v3, v6, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 218
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mCurrent:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 219
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v3, v6}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 220
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const v4, 0x84c0

    invoke-interface {v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 222
    :cond_1
    return-void

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    move-object v0, p1

    .line 206
    goto :goto_0
.end method

.method private setResourceImage(I)V
    .locals 3
    .param p1, "resourceID"    # I

    .prologue
    .line 190
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 191
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/16 v1, 0x100

    const/16 v2, 0x80

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlBackground;->setBitmapTexture(Landroid/graphics/Bitmap;II)V

    .line 192
    return-void
.end method


# virtual methods
.method public bindMixed(F)Z
    .locals 10
    .param p1, "ratio"    # F

    .prologue
    const v9, 0x47057500    # 34165.0f

    const v8, 0x44408000    # 770.0f

    const/4 v7, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    const/16 v5, 0x2300

    .line 384
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 387
    .local v1, "gl":Ljavax/microedition/khronos/opengles/GL11;
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const v3, 0x84c1

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 388
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v3, 0xde1

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 389
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v3, 0xde1

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureID:[I

    aget v4, v4, v7

    invoke-interface {v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 395
    const/16 v2, 0x2200

    const v3, 0x47057000    # 34160.0f

    invoke-interface {v1, v5, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 397
    const v2, 0x8571

    invoke-interface {v1, v5, v2, v9}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 398
    const v2, 0x8572

    invoke-interface {v1, v5, v2, v9}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 402
    const/4 v2, 0x4

    new-array v0, v2, [F

    const/4 v2, 0x0

    aput v6, v0, v2

    aput v6, v0, v7

    const/4 v2, 0x2

    aput v6, v0, v2

    const/4 v2, 0x3

    aput p1, v0, v2

    .line 404
    .local v0, "color":[F
    const/16 v2, 0x2201

    const/4 v3, 0x0

    invoke-interface {v1, v5, v2, v0, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvfv(II[FI)V

    .line 407
    const v2, 0x8582

    const v3, 0x47057600    # 34166.0f

    invoke-interface {v1, v5, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 408
    const v2, 0x8592

    invoke-interface {v1, v5, v2, v8}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 411
    const v2, 0x858a

    const v3, 0x47057600    # 34166.0f

    invoke-interface {v1, v5, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 412
    const v2, 0x859a

    invoke-interface {v1, v5, v2, v8}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 413
    return v7
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 93
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mRsrcID:I

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mBitmap:Landroid/graphics/Bitmap;

    .line 95
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTransitor:Lcom/sec/android/gallery3d/glcore/GlBackground$Transitioner;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlBackground$Transitioner;->destroy()V

    .line 96
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTransitor:Lcom/sec/android/gallery3d/glcore/GlBackground$Transitioner;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeOnGLIdleListener(Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;)V

    .line 97
    return-void
.end method

.method public draw()V
    .locals 5

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseClearColor:Z

    if-eqz v0, :cond_1

    .line 141
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorRed:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorGreen:F

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorBlue:F

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v4, v4, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorAlpha:F

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glClearColor(FFFF)V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v1, 0x4100

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glClear(I)V

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlBackground;->drawInter()V

    goto :goto_0
.end method

.method public enableBlending(ZZ)V
    .locals 0
    .param p1, "enableBlending"    # Z
    .param p2, "enableTransitioner"    # Z

    .prologue
    .line 181
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseBlending:Z

    .line 182
    iput-boolean p2, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseTransitioner:Z

    .line 183
    return-void
.end method

.method public isTransitionEnabled()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseTransitioner:Z

    return v0
.end method

.method protected load(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 18
    .param p1, "source"    # Landroid/graphics/Bitmap;

    .prologue
    .line 253
    const/16 v1, 0x80

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlBackground;->resizeBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 254
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    .line 255
    .local v16, "sourceWidth":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    .line 256
    .local v15, "sourceHeight":I
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mWidth:I

    .line 257
    .local v10, "destWidth":I
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mHeight:I

    .line 258
    .local v9, "destHeight":I
    move/from16 v0, v16

    int-to-float v1, v0

    int-to-float v3, v10

    div-float v12, v1, v3

    .line 259
    .local v12, "fitX":F
    int-to-float v1, v15

    int-to-float v3, v9

    div-float v13, v1, v3

    .line 264
    .local v13, "fitY":F
    cmpg-float v1, v12, v13

    if-gez v1, :cond_0

    .line 266
    move/from16 v4, v16

    .line 267
    .local v4, "cropWidth":I
    int-to-float v1, v9

    mul-float/2addr v1, v12

    float-to-int v8, v1

    .line 268
    .local v8, "cropHeight":I
    const/4 v5, 0x0

    .line 269
    .local v5, "cropX":I
    sub-int v1, v15, v8

    div-int/lit8 v6, v1, 0x2

    .line 279
    .local v6, "cropY":I
    :goto_0
    mul-int v14, v4, v8

    .line 280
    .local v14, "numPixels":I
    new-array v2, v14, [I

    .line 281
    .local v2, "in":[I
    new-array v0, v14, [I

    move-object/from16 v17, v0

    .line 284
    .local v17, "tmp":[I
    const/4 v3, 0x0

    move-object/from16 v1, p1

    move v7, v4

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 293
    move-object/from16 v0, v17

    invoke-static {v2, v0, v4, v8, v4}, Lcom/sec/android/gallery3d/glcore/GlBackground;->boxBlurFilter([I[IIII)V

    .line 294
    const/16 v1, 0x60

    move-object/from16 v0, v17

    invoke-static {v0, v2, v8, v4, v1}, Lcom/sec/android/gallery3d/glcore/GlBackground;->boxBlurFilter([I[IIII)V

    .line 297
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v4, v8, v1}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 298
    .local v11, "filtered":Landroid/graphics/Bitmap;
    return-object v11

    .line 272
    .end local v2    # "in":[I
    .end local v4    # "cropWidth":I
    .end local v5    # "cropX":I
    .end local v6    # "cropY":I
    .end local v8    # "cropHeight":I
    .end local v11    # "filtered":Landroid/graphics/Bitmap;
    .end local v14    # "numPixels":I
    .end local v17    # "tmp":[I
    :cond_0
    int-to-float v1, v9

    mul-float/2addr v1, v13

    float-to-int v4, v1

    .line 273
    .restart local v4    # "cropWidth":I
    move v8, v15

    .line 274
    .restart local v8    # "cropHeight":I
    sub-int v1, v16, v4

    div-int/lit8 v5, v1, 0x2

    .line 275
    .restart local v5    # "cropX":I
    const/4 v6, 0x0

    .restart local v6    # "cropY":I
    goto :goto_0
.end method

.method public setBitmapImage(Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "source"    # Landroid/graphics/Bitmap;

    .prologue
    const v3, 0x47012f00    # 33071.0f

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/16 v4, 0xde1

    .line 227
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTextureID:[I

    const/4 v2, 0x2

    aget v0, v1, v2

    .line 229
    .local v0, "textureId":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 230
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v4}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 231
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v4, v0}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 232
    if-eqz p1, :cond_0

    .line 233
    invoke-static {v4, v6, p1, v6}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 234
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 236
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0x2802

    invoke-interface {v1, v4, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 237
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0x2803

    invoke-interface {v1, v4, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 238
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0x2801

    const/high16 v3, 0x46180000    # 9728.0f

    invoke-interface {v1, v4, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 239
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0x2800

    const v3, 0x46180400    # 9729.0f

    invoke-interface {v1, v4, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 241
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v5, v5, v5, v5}, Ljavax/microedition/khronos/opengles/GL11;->glColor4f(FFFF)V

    .line 242
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0x1700

    invoke-interface {v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 243
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL11;->glLoadIdentity()V

    .line 244
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v7, v7, v5}, Ljavax/microedition/khronos/opengles/GL11;->glScalef(FFF)V

    .line 245
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0x1701

    invoke-interface {v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 246
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL11;->glLoadIdentity()V

    .line 247
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/4 v2, 0x6

    const/4 v3, 0x4

    invoke-interface {v1, v2, v6, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 248
    return-void
.end method

.method public setClearByColor(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 177
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseClearColor:Z

    .line 178
    return-void
.end method

.method public setFirstImage(I)V
    .locals 0
    .param p1, "rsrcID"    # I

    .prologue
    .line 151
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlBackground;->setResourceImage(I)V

    .line 152
    return-void
.end method

.method public setFirstImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 155
    const/16 v0, 0x100

    const/16 v1, 0x80

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlBackground;->setBitmapTexture(Landroid/graphics/Bitmap;II)V

    .line 156
    return-void
.end method

.method public setNextImage(I)V
    .locals 2
    .param p1, "rsrcID"    # I

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseBlending:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseTransitioner:Z

    if-nez v0, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mRsrcID:I

    .line 162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mNeedTransition:Z

    .line 163
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTransitor:Lcom/sec/android/gallery3d/glcore/GlBackground$Transitioner;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeOnGLIdleListener(Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTransitor:Lcom/sec/android/gallery3d/glcore/GlBackground$Transitioner;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addOnGLIdleListener(Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;)V

    goto :goto_0
.end method

.method public setNextImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseBlending:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mUseTransitioner:Z

    if-nez v0, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mBitmap:Landroid/graphics/Bitmap;

    .line 171
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mNeedTransition:Z

    .line 172
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTransitor:Lcom/sec/android/gallery3d/glcore/GlBackground$Transitioner;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeOnGLIdleListener(Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlBackground;->mTransitor:Lcom/sec/android/gallery3d/glcore/GlBackground$Transitioner;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addOnGLIdleListener(Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;)V

    goto :goto_0
.end method
