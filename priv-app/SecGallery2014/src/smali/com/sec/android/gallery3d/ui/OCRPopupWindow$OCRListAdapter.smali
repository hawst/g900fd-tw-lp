.class public Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;
.super Landroid/widget/BaseAdapter;
.source "OCRPopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/OCRPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OCRListAdapter"
.end annotation


# instance fields
.field private final mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/OCRPopupWindow;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 345
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;->this$0:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 346
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;->mList:Ljava/util/ArrayList;

    .line 347
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;->mList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;->mList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 361
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 365
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;->mList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 366
    :cond_0
    const/4 v0, 0x0

    .line 367
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;->this$0:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$OCRListAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;

    # invokes: Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->createItemView(Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;Landroid/view/ViewGroup;)Landroid/view/View;
    invoke-static {v1, v0, p3}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->access$000(Lcom/sec/android/gallery3d/ui/OCRPopupWindow;Lcom/sec/android/gallery3d/ui/OCRPopupWindow$ItemInfo;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
