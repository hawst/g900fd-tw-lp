.class public Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
.super Ljava/lang/Object;
.source "AlbumSetSlidingWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumSetEntry"
.end annotation


# instance fields
.field public album:Lcom/sec/android/gallery3d/data/MediaSet;

.field public bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

.field public cacheFlag:I

.field public cacheStatus:I

.field public content:Lcom/sec/android/gallery3d/glrenderer/Texture;

.field public coverDataVersion:J

.field public coverItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;

.field public isWaitLoadingDisplayed:Z

.field private labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;

.field public labelTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

.field public rotation:I

.field public setDataVersion:J

.field public setPath:Lcom/sec/android/gallery3d/data/Path;

.field public sourceType:I

.field public title:Ljava/lang/String;

.field public totalCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;Lcom/sec/android/gallery3d/ui/BitmapLoader;)Lcom/sec/android/gallery3d/ui/BitmapLoader;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/ui/BitmapLoader;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;Lcom/sec/android/gallery3d/ui/BitmapLoader;)Lcom/sec/android/gallery3d/ui/BitmapLoader;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/ui/BitmapLoader;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;

    return-object p1
.end method
