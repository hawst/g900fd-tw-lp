.class Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;
.super Ljava/lang/Object;
.source "CropImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/CropImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveOutput"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCropRect:Landroid/graphics/RectF;

.field private mCropRectFace:Landroid/graphics/RectF;

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/CropImage;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/RectF;)V
    .locals 0
    .param p2, "cropRect"    # Landroid/graphics/RectF;

    .prologue
    .line 728
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 729
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->mCropRect:Landroid/graphics/RectF;

    .line 730
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 0
    .param p2, "cropRect"    # Landroid/graphics/RectF;
    .param p3, "cropRectFace"    # Landroid/graphics/RectF;

    .prologue
    .line 734
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;-><init>(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/RectF;)V

    .line 736
    iput-object p3, p0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->mCropRectFace:Landroid/graphics/RectF;

    .line 738
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/content/Intent;
    .locals 25
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 743
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->mCropRect:Landroid/graphics/RectF;

    .line 744
    .local v5, "cropRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    .line 746
    .local v8, "extra":Landroid/os/Bundle;
    new-instance v13, Landroid/graphics/Rect;

    iget v0, v5, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    iget v0, v5, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->round(F)I

    move-result v21

    iget v0, v5, Landroid/graphics/RectF;->right:F

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v22

    iget v0, v5, Landroid/graphics/RectF;->bottom:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->round(F)I

    move-result v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v13, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 750
    .local v13, "rect":Landroid/graphics/Rect;
    new-instance v15, Landroid/content/Intent;

    invoke-direct {v15}, Landroid/content/Intent;-><init>()V

    .line 754
    .local v15, "result":Landroid/content/Intent;
    const/4 v14, 0x0

    .line 755
    .local v14, "rectFace":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->mCropRectFace:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    .line 756
    new-instance v14, Landroid/graphics/Rect;

    .end local v14    # "rectFace":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->mCropRectFace:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->mCropRectFace:Landroid/graphics/RectF;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->round(F)I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->mCropRectFace:Landroid/graphics/RectF;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->mCropRectFace:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->round(F)I

    move-result v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 758
    .restart local v14    # "rectFace":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$900(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v17

    .line 759
    .local v17, "rotation":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$2700(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/ui/CropView;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/ui/CropView;->getImageWidth()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/CropImage;->access$2700(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/ui/CropView;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/ui/CropView;->getImageHeight()I

    move-result v21

    move/from16 v0, v17

    rsub-int v0, v0, 0x168

    move/from16 v22, v0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->rotateRectangle(Landroid/graphics/Rect;III)V
    invoke-static {v14, v0, v1, v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$2800(Landroid/graphics/Rect;III)V

    .line 760
    const-string v20, "cropped-rect"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 764
    .end local v17    # "rotation":I
    :cond_0
    const/4 v6, 0x0

    .line 765
    .local v6, "cropped":Landroid/graphics/Bitmap;
    const/4 v12, 0x0

    .line 766
    .local v12, "outputted":Z
    if-eqz v8, :cond_1f

    .line 767
    const-string v20, "output"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v19

    check-cast v19, Landroid/net/Uri;

    .line 768
    .local v19, "uri":Landroid/net/Uri;
    if-eqz v19, :cond_a

    .line 769
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v20

    if-eqz v20, :cond_2

    const/4 v15, 0x0

    .line 954
    .end local v15    # "result":Landroid/content/Intent;
    .end local v19    # "uri":Landroid/net/Uri;
    :cond_1
    :goto_0
    return-object v15

    .line 770
    .restart local v15    # "result":Landroid/content/Intent;
    .restart local v19    # "uri":Landroid/net/Uri;
    :cond_2
    const/4 v12, 0x1

    .line 775
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->isAGIFCrop()Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$2900(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 776
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$900(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/gallery3d/app/CropImage;->access$2700(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/ui/CropView;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/CropView;->getHighlightRect()Landroid/graphics/RectF;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/gallery3d/app/CropImage;->getCropRectangle(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/quramsoft/agifEncoder/QuramAGIFEncoder;->makeContactAGIF(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;)I

    move-result v16

    .line 777
    .local v16, "resultValue":I
    const/16 v20, 0x1

    move/from16 v0, v16

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 779
    const-string v20, "output"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0

    .line 784
    .end local v16    # "resultValue":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->getCroppedImage(Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;
    invoke-static {v0, v13, v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$3000(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 785
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsCallerId:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$3100(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 786
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/CropImage;->getAndroidContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/CropImage;->getCallerIdSavePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->saveCallerId(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/lang/String;)Z
    invoke-static {v0, v1, v6, v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$3200(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_8

    .line 787
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 789
    :cond_4
    const/4 v10, 0x0

    .line 790
    .local v10, "newCropped":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mNeedWallpaperLayout:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$3300(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 791
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->changeBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/app/CropImage;->access$3400(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 794
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    if-eqz v10, :cond_7

    .end local v10    # "newCropped":Landroid/graphics/Bitmap;
    :goto_2
    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->saveBitmapToUri(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Landroid/net/Uri;)Z
    invoke-static {v0, v1, v10, v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$3800(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Landroid/net/Uri;)Z

    move-result v20

    if-nez v20, :cond_8

    .line 795
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 792
    .restart local v10    # "newCropped":Landroid/graphics/Bitmap;
    :cond_6
    sget-boolean v20, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoCallCrop:Z

    if-eqz v20, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsFromVideoCall:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$3500(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-eqz v20, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RW:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$3600(Lcom/sec/android/gallery3d/app/CropImage;)I

    move-result v20

    if-lez v20, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RH:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$3700(Lcom/sec/android/gallery3d/app/CropImage;)I

    move-result v20

    if-lez v20, :cond_5

    .line 793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RW:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$3600(Lcom/sec/android/gallery3d/app/CropImage;)I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RH:I
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/CropImage;->access$3700(Lcom/sec/android/gallery3d/app/CropImage;)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v6, v0, v1}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownForVideoCall(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v10

    goto :goto_1

    :cond_7
    move-object v10, v6

    .line 794
    goto :goto_2

    .line 798
    .end local v10    # "newCropped":Landroid/graphics/Bitmap;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$900(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v20

    move-object/from16 v0, v20

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    move/from16 v20, v0

    if-eqz v20, :cond_9

    .line 799
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$900(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 802
    .local v9, "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    const-string/jumbo v20, "takenTime"

    iget-wide v0, v9, Lcom/sec/android/gallery3d/data/LocalImage;->dateTakenInMs:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v15, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 803
    const-string v20, "latitude"

    iget-wide v0, v9, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v15, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 804
    const-string v20, "longitude"

    iget-wide v0, v9, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v15, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 807
    .end local v9    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    :cond_9
    const-string v20, "output"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 808
    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 812
    :cond_a
    const-string v20, "data"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    .line 814
    .local v4, "bitmapInIntent":Landroid/graphics/Bitmap;
    const-string v20, "return-data"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v20

    if-nez v20, :cond_b

    if-eqz v4, :cond_10

    .line 819
    :cond_b
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v20

    if-eqz v20, :cond_c

    .line 820
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 822
    :cond_c
    const/4 v12, 0x1

    .line 823
    if-nez v6, :cond_d

    .line 824
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->getCroppedImage(Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;
    invoke-static {v0, v13, v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$3000(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 827
    :cond_d
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v11

    .line 829
    .local v11, "originalSize":I
    sget-boolean v20, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoCallCrop:Z

    if-eqz v20, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsFromVideoCall:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$3500(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-eqz v20, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RW:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$3600(Lcom/sec/android/gallery3d/app/CropImage;)I

    move-result v20

    if-lez v20, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RH:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$3700(Lcom/sec/android/gallery3d/app/CropImage;)I

    move-result v20

    if-lez v20, :cond_e

    .line 830
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RW:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$3600(Lcom/sec/android/gallery3d/app/CropImage;)I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RH:I
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/CropImage;->access$3700(Lcom/sec/android/gallery3d/app/CropImage;)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v6, v0, v1}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownForVideoCall(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 834
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$3900(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-eqz v20, :cond_11

    const v20, 0x9c40

    move/from16 v0, v20

    if-le v11, v0, :cond_11

    .line 835
    const v20, 0x9c40

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v6, v0, v1}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownToPixels(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 853
    :cond_f
    :goto_3
    const-string v20, "data"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 855
    .end local v11    # "originalSize":I
    :cond_10
    const-string v20, "set-as-wallpaper"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v20

    if-eqz v20, :cond_19

    .line 856
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v20

    if-eqz v20, :cond_13

    .line 857
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 839
    .restart local v11    # "originalSize":I
    :cond_11
    sget-boolean v20, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoCallCrop:Z

    if-eqz v20, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsFromVideoCall:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$3500(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-eqz v20, :cond_12

    const v20, 0x7d000

    move/from16 v0, v20

    if-le v11, v0, :cond_12

    .line 840
    const v20, 0x1f400

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v6, v0, v1}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownToPixels(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 841
    if-eqz v6, :cond_f

    .line 842
    const-string v20, "CropImage"

    const-string v21, "Original bitmap size(%s) re-size(%s) video call"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 845
    :cond_12
    const v20, 0x19000

    move/from16 v0, v20

    if-le v11, v0, :cond_f

    .line 846
    const/16 v20, 0x6400

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v6, v0, v1}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownToPixels(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 847
    if-eqz v6, :cond_f

    .line 848
    const-string v20, "CropImage"

    const-string v21, "Original bitmap size(%s) re-size(%s)"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 859
    .end local v11    # "originalSize":I
    :cond_13
    const/4 v12, 0x1

    .line 860
    if-nez v6, :cond_14

    .line 861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->getCroppedImage(Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;
    invoke-static {v0, v13, v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$3000(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 867
    :cond_14
    const/4 v10, 0x0

    .line 868
    .restart local v10    # "newCropped":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$200(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-nez v20, :cond_15

    .line 869
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->changeBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/app/CropImage;->access$3400(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 872
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mKnox:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$1700(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-eqz v20, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->getApplicationContext()Landroid/content/Context;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX2(Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_18

    .line 874
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    if-eqz v10, :cond_17

    .end local v10    # "newCropped":Landroid/graphics/Bitmap;
    :goto_4
    move-object/from16 v0, v20

    move-object/from16 v1, p1

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->setAsWallpaper(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Z
    invoke-static {v0, v1, v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$4000(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Z

    move-result v20

    if-nez v20, :cond_19

    .line 875
    const/4 v15, 0x0

    goto/16 :goto_0

    .restart local v10    # "newCropped":Landroid/graphics/Bitmap;
    :cond_17
    move-object v10, v6

    .line 874
    goto :goto_4

    .line 880
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsTablet:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$4100(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-eqz v20, :cond_1b

    .line 881
    if-eqz v10, :cond_1a

    .line 882
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    # setter for: Lcom/sec/android/gallery3d/app/CropImage;->mKnoxWallpaper:Landroid/graphics/Bitmap;
    invoke-static {v0, v10}, Lcom/sec/android/gallery3d/app/CropImage;->access$1902(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 894
    .end local v10    # "newCropped":Landroid/graphics/Bitmap;
    :cond_19
    :goto_5
    const-string v20, "set-as-lockscreen"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v20

    if-eqz v20, :cond_1e

    .line 895
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v20

    if-eqz v20, :cond_1c

    .line 896
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 884
    .restart local v10    # "newCropped":Landroid/graphics/Bitmap;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    # setter for: Lcom/sec/android/gallery3d/app/CropImage;->mKnoxWallpaper:Landroid/graphics/Bitmap;
    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/app/CropImage;->access$1902(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto :goto_5

    .line 887
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->getCroppedImage(Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;
    invoke-static {v0, v13, v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$3000(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;

    move-result-object v21

    # setter for: Lcom/sec/android/gallery3d/app/CropImage;->mKnoxWallpaper:Landroid/graphics/Bitmap;
    invoke-static/range {v20 .. v21}, Lcom/sec/android/gallery3d/app/CropImage;->access$1902(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto :goto_5

    .line 897
    .end local v10    # "newCropped":Landroid/graphics/Bitmap;
    :cond_1c
    const/4 v12, 0x1

    .line 899
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsTablet:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$4100(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-nez v20, :cond_1d

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v20

    if-eqz v20, :cond_21

    .line 900
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$200(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-nez v20, :cond_20

    .line 901
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->getCroppedImage(Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;
    invoke-static {v0, v13, v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$3000(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 902
    const/4 v10, 0x0

    .line 903
    .restart local v10    # "newCropped":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_1e

    .line 904
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->changeBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/app/CropImage;->access$3400(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 905
    if-eqz v10, :cond_1e

    .line 906
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move/from16 v2, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->saveLockScreenImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V
    invoke-static {v0, v1, v10, v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$4200(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V

    .line 907
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move/from16 v2, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->saveLockScreenImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V
    invoke-static {v0, v1, v10, v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$4200(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V

    .line 936
    .end local v10    # "newCropped":Landroid/graphics/Bitmap;
    :cond_1e
    :goto_6
    sget-boolean v20, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v20, :cond_1f

    .line 937
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->getSimSelectionID()I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$4300(Lcom/sec/android/gallery3d/app/CropImage;)I

    move-result v18

    .line 938
    .local v18, "simSelection_id":I
    const-string/jumbo v20, "sim_selection"

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 942
    .end local v4    # "bitmapInIntent":Landroid/graphics/Bitmap;
    .end local v18    # "simSelection_id":I
    .end local v19    # "uri":Landroid/net/Uri;
    :cond_1f
    if-nez v12, :cond_1

    .line 943
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v20

    if-eqz v20, :cond_25

    .line 944
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 911
    .restart local v4    # "bitmapInIntent":Landroid/graphics/Bitmap;
    .restart local v19    # "uri":Landroid/net/Uri;
    :cond_20
    if-eqz v6, :cond_1e

    .line 912
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move/from16 v2, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->saveLockScreenImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V
    invoke-static {v0, v1, v6, v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$4200(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V

    .line 913
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move/from16 v2, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->saveLockScreenImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V
    invoke-static {v0, v1, v6, v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$4200(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V

    goto :goto_6

    .line 917
    :cond_21
    if-eqz v6, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mBothScreen:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$1500(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-eqz v20, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$200(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-nez v20, :cond_23

    .line 918
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->getCroppedImage(Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;
    invoke-static {v0, v13, v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$3000(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 919
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move/from16 v2, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->saveLockScreenImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V
    invoke-static {v0, v1, v6, v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$4200(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V

    .line 922
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/CropImage;->access$200(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v20

    if-nez v20, :cond_24

    .line 923
    const/4 v10, 0x0

    .line 924
    .restart local v10    # "newCropped":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->getCroppedImage(Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;
    invoke-static {v0, v13, v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$3000(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 925
    if-eqz v6, :cond_1e

    .line 926
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->changeBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/app/CropImage;->access$3400(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 927
    if-eqz v10, :cond_1e

    .line 928
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move/from16 v2, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->saveLockScreenImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V
    invoke-static {v0, v1, v10, v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$4200(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_6

    .line 931
    .end local v10    # "newCropped":Landroid/graphics/Bitmap;
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move/from16 v2, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->saveLockScreenImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V
    invoke-static {v0, v1, v6, v2}, Lcom/sec/android/gallery3d/app/CropImage;->access$4200(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_6

    .line 946
    .end local v4    # "bitmapInIntent":Landroid/graphics/Bitmap;
    .end local v19    # "uri":Landroid/net/Uri;
    :cond_25
    if-nez v6, :cond_26

    .line 947
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->getCroppedImage(Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;
    invoke-static {v0, v13, v1}, Lcom/sec/android/gallery3d/app/CropImage;->access$3000(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 949
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->saveToMediaProvider(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Landroid/net/Uri;
    invoke-static {v0, v1, v6}, Lcom/sec/android/gallery3d/app/CropImage;->access$4400(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v7

    .line 950
    .local v7, "data":Landroid/net/Uri;
    if-eqz v7, :cond_1

    .line 951
    invoke-virtual {v15, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 720
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
