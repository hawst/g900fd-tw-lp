.class public Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter;
.super Lcom/sec/android/gallery3d/ds/SimIconListAdapter;
.source "SimSelectorDataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter$SimSelectorListItem;
    }
.end annotation


# static fields
.field public static final SELECT_SET_AS_FOR_ALL:I = 0x0

.field public static final SELECT_SET_AS_FOR_SIM1:I = 0x1

.field public static final SELECT_SET_AS_FOR_SIM2:I = 0x2


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-static {p1}, Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter;->getData(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/gallery3d/ds/SimIconListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 35
    return-void
.end method

.method protected static addItem(Ljava/util/List;Ljava/lang/String;II)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "resource"    # I
    .param p3, "command"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;",
            ">;",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;>;"
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v1, :cond_1

    .line 72
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter$SimSelectorListItem;

    invoke-direct {v0, p1, p2, p3}, Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter$SimSelectorListItem;-><init>(Ljava/lang/String;II)V

    .line 73
    .local v0, "temp":Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter$SimSelectorListItem;
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    .end local v0    # "temp":Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter$SimSelectorListItem;
    :cond_1
    return-void
.end method

.method protected static getData(Landroid/content/Context;)Ljava/util/List;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 47
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v2, :cond_1

    .line 48
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01a2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "allSim":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 51
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;>;"
    const/4 v2, -0x1

    invoke-static {v1, v0, v2, v4}, Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter;->addItem(Ljava/util/List;Ljava/lang/String;II)V

    .line 55
    invoke-static {p0, v4}, Lcom/sec/android/gallery3d/ds/SimInformation;->getSimName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v4}, Lcom/sec/android/gallery3d/ds/SimInformation;->getSimIcon(Landroid/content/Context;I)I

    move-result v3

    invoke-static {v1, v2, v3, v5}, Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter;->addItem(Ljava/util/List;Ljava/lang/String;II)V

    .line 59
    invoke-static {p0, v5}, Lcom/sec/android/gallery3d/ds/SimInformation;->getSimName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v5}, Lcom/sec/android/gallery3d/ds/SimInformation;->getSimIcon(Landroid/content/Context;I)I

    move-result v3

    const/4 v4, 0x2

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter;->addItem(Ljava/util/List;Ljava/lang/String;II)V

    .line 66
    .end local v0    # "allSim":Ljava/lang/String;
    .end local v1    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;>;"
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCommand(I)I
    .locals 2
    .param p1, "which"    # I

    .prologue
    .line 38
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v1, :cond_1

    .line 39
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter$SimSelectorListItem;

    .line 40
    .local v0, "item":Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter$SimSelectorListItem;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter$SimSelectorListItem;->getCommand()I

    move-result v1

    .line 42
    .end local v0    # "item":Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter$SimSelectorListItem;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
