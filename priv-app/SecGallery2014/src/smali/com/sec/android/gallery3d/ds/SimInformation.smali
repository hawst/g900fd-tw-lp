.class public Lcom/sec/android/gallery3d/ds/SimInformation;
.super Ljava/lang/Object;
.source "SimInformation.java"


# static fields
.field public static final SIM_ID_SIM1:I = 0x0

.field public static final SIM_ID_SIM2:I = 0x1

.field public static final SIM_STATE_DUAL_SIM:I = 0x2

.field public static final SIM_STATE_NO_SIM:I = 0x0

.field public static final SIM_STATE_SINGLE_SIM:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCurrentSim()I
    .locals 15

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 199
    const/4 v3, 0x0

    .line 201
    .local v3, "currentSim":I
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v11, :cond_1

    .line 203
    const-string v0, "getDefaultSimForVoiceCalls"

    .line 207
    .local v0, "METHOD_GET_DEFAULT_SIM_FOR_VOICE_CALLS":Ljava/lang/String;
    const-string v11, "phone"

    invoke-static {v11}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v11

    invoke-static {v11}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v10

    .line 211
    .local v10, "telephonyService":Lcom/android/internal/telephony/ITelephony;
    if-eqz v10, :cond_0

    .line 221
    :try_start_0
    const-string v11, "ril.MSIMM"

    const/4 v14, 0x0

    invoke-static {v11, v14}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 303
    .end local v0    # "METHOD_GET_DEFAULT_SIM_FOR_VOICE_CALLS":Ljava/lang/String;
    .end local v10    # "telephonyService":Lcom/android/internal/telephony/ITelephony;
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    move v11, v12

    .line 309
    :goto_1
    return v11

    .line 223
    .restart local v0    # "METHOD_GET_DEFAULT_SIM_FOR_VOICE_CALLS":Ljava/lang/String;
    .restart local v10    # "telephonyService":Lcom/android/internal/telephony/ITelephony;
    :catch_0
    move-exception v4

    .line 225
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 231
    .end local v0    # "METHOD_GET_DEFAULT_SIM_FOR_VOICE_CALLS":Ljava/lang/String;
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v10    # "telephonyService":Lcom/android/internal/telephony/ITelephony;
    :cond_1
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-eqz v11, :cond_0

    .line 233
    const/4 v2, 0x0

    .line 235
    .local v2, "classPlugInServiceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v1, 0x0

    .line 237
    .local v1, "classPlugInDsdsService":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-array v9, v13, [Ljava/lang/Class;

    const-class v11, Ljava/lang/String;

    aput-object v11, v9, v12

    .line 239
    .local v9, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v8, 0x0

    .line 241
    .local v8, "objDsdsService":Ljava/lang/Object;
    const/4 v6, 0x0

    .line 243
    .local v6, "methodGetService":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    .line 255
    .local v5, "methodGetDefaultSimForVoiceCalls":Ljava/lang/reflect/Method;
    :try_start_1
    const-string v11, "com.android.plugin.PlugInServiceManager"

    invoke-static {v11}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 257
    const-string v11, "com.android.plugin.dsds.PlugInDsdsService"

    invoke-static {v11}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 259
    const-string v11, "getService"

    invoke-virtual {v2, v11, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 261
    const-string v14, "getDefaultSimForVoiceCalls"

    const/4 v11, 0x0

    check-cast v11, [Ljava/lang/Class;

    invoke-virtual {v1, v14, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 263
    const-string v11, "NAME"

    invoke-virtual {v1, v11}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 265
    .local v7, "name":Ljava/lang/String;
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v11, v14

    invoke-virtual {v6, v2, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .line 267
    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v5, v8, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7

    move-result v3

    goto :goto_0

    .line 269
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "objDsdsService":Ljava/lang/Object;
    :catch_1
    move-exception v4

    .line 271
    .local v4, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v4}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 273
    .end local v4    # "e":Ljava/lang/ClassNotFoundException;
    :catch_2
    move-exception v4

    .line 275
    .local v4, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v4}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 277
    .end local v4    # "e":Ljava/lang/NoSuchMethodException;
    :catch_3
    move-exception v4

    .line 279
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 281
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v4

    .line 283
    .local v4, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v4}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 285
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    :catch_5
    move-exception v4

    .line 287
    .local v4, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v4}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 289
    .end local v4    # "e":Ljava/lang/NoSuchFieldException;
    :catch_6
    move-exception v4

    .line 291
    .local v4, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v4}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    .line 293
    .end local v4    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_7
    move-exception v4

    .line 295
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .end local v1    # "classPlugInDsdsService":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "classPlugInServiceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v5    # "methodGetDefaultSimForVoiceCalls":Ljava/lang/reflect/Method;
    .end local v6    # "methodGetService":Ljava/lang/reflect/Method;
    .end local v9    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :cond_2
    move v11, v13

    .line 309
    goto :goto_1
.end method

.method public static getCurrentSimSlot(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 319
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v7, :cond_0

    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v7, :cond_1

    .line 321
    :cond_0
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMapleBranch:Z

    if-eqz v7, :cond_2

    .line 323
    const-string v0, "ro.cmd.simcard"

    .line 325
    .local v0, "SP_KEY_CURRENT_SIM_SLOT":Ljava/lang/String;
    const-string v1, "2"

    .line 329
    .local v1, "SP_VALUE_SIM_SLOT_SLOT2":Ljava/lang/String;
    const-string v7, "ro.cmd.simcard"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 332
    .local v2, "mSimSlot":Ljava/lang/String;
    const-string v7, "2"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v6, v5

    .line 375
    .end local v0    # "SP_KEY_CURRENT_SIM_SLOT":Ljava/lang/String;
    .end local v1    # "SP_VALUE_SIM_SLOT_SLOT2":Ljava/lang/String;
    .end local v2    # "mSimSlot":Ljava/lang/String;
    :cond_1
    :goto_0
    return v6

    .line 344
    :cond_2
    const-string v0, "ril.MSIMM"

    .line 346
    .restart local v0    # "SP_KEY_CURRENT_SIM_SLOT":Ljava/lang/String;
    const-string v1, "1"

    .line 350
    .restart local v1    # "SP_VALUE_SIM_SLOT_SLOT2":Ljava/lang/String;
    const-string v7, "ril.MSIMM"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 352
    .restart local v2    # "mSimSlot":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "phone1_on"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v5, :cond_3

    move v3, v5

    .line 354
    .local v3, "sim1Enabled":Z
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "phone2_on"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v5, :cond_4

    move v4, v5

    .line 358
    .local v4, "sim2Enabled":Z
    :goto_2
    const-string v7, "1"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    move v6, v5

    .line 360
    goto :goto_0

    .end local v3    # "sim1Enabled":Z
    .end local v4    # "sim2Enabled":Z
    :cond_3
    move v3, v6

    .line 352
    goto :goto_1

    .restart local v3    # "sim1Enabled":Z
    :cond_4
    move v4, v6

    .line 354
    goto :goto_2

    .line 364
    .restart local v4    # "sim2Enabled":Z
    :cond_5
    if-nez v3, :cond_1

    if-eqz v4, :cond_1

    move v6, v5

    .line 365
    goto :goto_0
.end method

.method public static getSimIcon(Landroid/content/Context;I)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "simId"    # I

    .prologue
    const/4 v9, 0x1

    .line 597
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v8, :cond_0

    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v8, :cond_6

    .line 599
    :cond_0
    const-string v1, "SELECT_ICON_1"

    .line 601
    .local v1, "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_1":Ljava/lang/String;
    const-string v2, "SELECT_ICON_2"

    .line 605
    .local v2, "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_2":Ljava/lang/String;
    const/4 v7, 0x0

    .line 607
    .local v7, "simIconIdx":I
    const v6, 0x7f020445

    .line 609
    .local v6, "simIcon":I
    const/4 v5, 0x0

    .line 613
    .local v5, "iconNameField":Ljava/lang/String;
    if-nez p0, :cond_1

    .line 615
    const-string v8, "TAG"

    const-string v9, "context is null"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    .end local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_1":Ljava/lang/String;
    .end local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_2":Ljava/lang/String;
    .end local v5    # "iconNameField":Ljava/lang/String;
    .end local v6    # "simIcon":I
    .end local v7    # "simIconIdx":I
    :goto_0
    return v6

    .line 625
    .restart local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_1":Ljava/lang/String;
    .restart local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_2":Ljava/lang/String;
    .restart local v5    # "iconNameField":Ljava/lang/String;
    .restart local v6    # "simIcon":I
    .restart local v7    # "simIconIdx":I
    :cond_1
    :try_start_0
    const-class v3, Landroid/provider/Settings$System;

    .line 629
    .local v3, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-ne p1, v9, :cond_2

    .line 637
    const-string v8, "SELECT_ICON_2"

    invoke-virtual {v3, v8}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    .line 653
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-static {v8, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v7

    .line 709
    .end local v3    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_2
    packed-switch v7, :pswitch_data_0

    .line 767
    const v6, 0x7f020445

    goto :goto_0

    .line 647
    .restart local v3    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2
    const-string v8, "SELECT_ICON_1"

    invoke-virtual {v3, v8}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/lang/String;

    move-object v5, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_1

    .line 655
    .end local v3    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_0
    move-exception v4

    .line 657
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    if-ne p1, v9, :cond_3

    .line 659
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "illegal argument of SELECT_ICON_2"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    :goto_3
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 663
    :cond_3
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "illegal argument of SELECT_ICON_1"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 669
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v4

    .line 671
    .local v4, "e":Ljava/lang/IllegalAccessException;
    if-ne p1, v9, :cond_4

    .line 673
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "illegal access of SELECT_ICON_2"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    :goto_4
    invoke-virtual {v4}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 677
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "illegal access of SELECT_ICON_1"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 683
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v4

    .line 685
    .local v4, "e":Ljava/lang/NoSuchFieldException;
    if-ne p1, v9, :cond_5

    .line 687
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "there is no SELECT_ICON_2"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    :goto_5
    invoke-virtual {v4}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_2

    .line 691
    :cond_5
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "there is no SELECT_ICON_1"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 697
    .end local v4    # "e":Ljava/lang/NoSuchFieldException;
    :catch_3
    move-exception v4

    .line 699
    .local v4, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v4}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 701
    .end local v4    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catch_4
    move-exception v4

    .line 703
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 713
    .end local v4    # "e":Ljava/lang/Exception;
    :pswitch_0
    const v6, 0x7f020445

    .line 715
    goto/16 :goto_0

    .line 719
    :pswitch_1
    const v6, 0x7f020446

    .line 721
    goto/16 :goto_0

    .line 725
    :pswitch_2
    const v6, 0x7f020447

    .line 727
    goto/16 :goto_0

    .line 731
    :pswitch_3
    const v6, 0x7f02044d

    .line 733
    goto/16 :goto_0

    .line 737
    :pswitch_4
    const v6, 0x7f02044b

    .line 739
    goto/16 :goto_0

    .line 743
    :pswitch_5
    const v6, 0x7f02044a

    .line 745
    goto/16 :goto_0

    .line 749
    :pswitch_6
    const v6, 0x7f020449

    .line 751
    goto/16 :goto_0

    .line 755
    :pswitch_7
    const v6, 0x7f02044c

    .line 757
    goto/16 :goto_0

    .line 761
    :pswitch_8
    const v6, 0x7f020448

    .line 763
    goto/16 :goto_0

    .line 777
    .end local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_1":Ljava/lang/String;
    .end local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_2":Ljava/lang/String;
    .end local v5    # "iconNameField":Ljava/lang/String;
    .end local v6    # "simIcon":I
    .end local v7    # "simIconIdx":I
    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 709
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static getSimName(Landroid/content/Context;I)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "simId"    # I

    .prologue
    const/4 v9, 0x1

    .line 477
    const/4 v5, 0x0

    .line 479
    .local v5, "simName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 483
    .local v7, "simName_Chn":Ljava/lang/String;
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v8, :cond_0

    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v8, :cond_2

    .line 485
    :cond_0
    const/4 v6, 0x0

    .line 489
    .local v6, "simNameField":Ljava/lang/String;
    const-string v1, "SELECT_NAME_1"

    .line 491
    .local v1, "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_1":Ljava/lang/String;
    const-string v2, "SELECT_NAME_2"

    .line 497
    .local v2, "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_2":Ljava/lang/String;
    if-nez p0, :cond_1

    .line 499
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "context is null"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    const/4 v8, 0x0

    .line 589
    .end local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_1":Ljava/lang/String;
    .end local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_2":Ljava/lang/String;
    .end local v6    # "simNameField":Ljava/lang/String;
    :goto_0
    return-object v8

    .line 509
    .restart local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_1":Ljava/lang/String;
    .restart local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_2":Ljava/lang/String;
    .restart local v6    # "simNameField":Ljava/lang/String;
    :cond_1
    :try_start_0
    const-class v3, Landroid/provider/Settings$System;

    .line 513
    .local v3, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-ne p1, v9, :cond_3

    .line 521
    const-string v8, "SELECT_NAME_2"

    invoke-virtual {v3, v8}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    .line 537
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-static {v8, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .end local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_1":Ljava/lang/String;
    .end local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_2":Ljava/lang/String;
    .end local v3    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v6    # "simNameField":Ljava/lang/String;
    :cond_2
    :goto_2
    move-object v8, v5

    .line 589
    goto :goto_0

    .line 531
    .restart local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_1":Ljava/lang/String;
    .restart local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_2":Ljava/lang/String;
    .restart local v3    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v6    # "simNameField":Ljava/lang/String;
    :cond_3
    const-string v8, "SELECT_NAME_1"

    invoke-virtual {v3, v8}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/lang/String;

    move-object v6, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_1

    .line 539
    .end local v3    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_0
    move-exception v4

    .line 541
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    if-ne p1, v9, :cond_4

    .line 543
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "illegal argument of SELECT_NAME_2"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    :goto_3
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 547
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "illegal argument of SELECT_NAME_1"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 553
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v4

    .line 555
    .local v4, "e":Ljava/lang/IllegalAccessException;
    if-ne p1, v9, :cond_5

    .line 557
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "illegal access of SELECT_NAME_2"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    :goto_4
    invoke-virtual {v4}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 561
    :cond_5
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "illegal access of SELECT_NAME_1"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 567
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v4

    .line 569
    .local v4, "e":Ljava/lang/NoSuchFieldException;
    if-ne p1, v9, :cond_6

    .line 571
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "there is no SELECT_NAME_2"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    :goto_5
    invoke-virtual {v4}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_2

    .line 575
    :cond_6
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "there is no SELECT_NAME_1"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 581
    .end local v4    # "e":Ljava/lang/NoSuchFieldException;
    :catch_3
    move-exception v4

    .line 583
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public static getSimState(Landroid/content/Context;)I
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v12, :cond_0

    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v12, :cond_9

    .line 109
    :cond_0
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMapleBranch:Z

    if-eqz v12, :cond_4

    .line 111
    const-string v0, "ro.cmd.simcard"

    .line 113
    .local v0, "SP_KEY_CURRENT_SIM_SLOT":Ljava/lang/String;
    const-string v4, "1"

    .line 115
    .local v4, "SP_VALUE_SIM_SLOT_SLOT1":Ljava/lang/String;
    const-string v5, "2"

    .line 117
    .local v5, "SP_VALUE_SIM_SLOT_SLOT2":Ljava/lang/String;
    const-string v3, "3"

    .line 121
    .local v3, "SP_VALUE_SIM_SLOT_ALL":Ljava/lang/String;
    const-string v12, "ro.cmd.simcard"

    invoke-static {v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 125
    .local v7, "mSimSlot":Ljava/lang/String;
    const-string v12, "3"

    invoke-virtual {v12, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 127
    const/4 v12, 0x2

    .line 189
    .end local v0    # "SP_KEY_CURRENT_SIM_SLOT":Ljava/lang/String;
    .end local v3    # "SP_VALUE_SIM_SLOT_ALL":Ljava/lang/String;
    .end local v4    # "SP_VALUE_SIM_SLOT_SLOT1":Ljava/lang/String;
    .end local v5    # "SP_VALUE_SIM_SLOT_SLOT2":Ljava/lang/String;
    .end local v7    # "mSimSlot":Ljava/lang/String;
    :goto_0
    return v12

    .line 129
    .restart local v0    # "SP_KEY_CURRENT_SIM_SLOT":Ljava/lang/String;
    .restart local v3    # "SP_VALUE_SIM_SLOT_ALL":Ljava/lang/String;
    .restart local v4    # "SP_VALUE_SIM_SLOT_SLOT1":Ljava/lang/String;
    .restart local v5    # "SP_VALUE_SIM_SLOT_SLOT2":Ljava/lang/String;
    .restart local v7    # "mSimSlot":Ljava/lang/String;
    :cond_1
    const-string v12, "1"

    invoke-virtual {v12, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "2"

    invoke-virtual {v12, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 131
    :cond_2
    const/4 v12, 0x1

    goto :goto_0

    .line 135
    :cond_3
    const/4 v12, 0x0

    goto :goto_0

    .line 141
    .end local v0    # "SP_KEY_CURRENT_SIM_SLOT":Ljava/lang/String;
    .end local v3    # "SP_VALUE_SIM_SLOT_ALL":Ljava/lang/String;
    .end local v4    # "SP_VALUE_SIM_SLOT_SLOT1":Ljava/lang/String;
    .end local v5    # "SP_VALUE_SIM_SLOT_SLOT2":Ljava/lang/String;
    .end local v7    # "mSimSlot":Ljava/lang/String;
    :cond_4
    const-string v1, "ril.ICC_TYPE"

    .line 143
    .local v1, "SP_KEY_SIM_STATE_SIM1":Ljava/lang/String;
    const-string v2, "ril.ICC_TYPE_1"

    .line 145
    .local v2, "SP_KEY_SIM_STATE_SIM2":Ljava/lang/String;
    const-string v6, "0"

    .line 149
    .local v6, "SP_VALUE_SIM_STATE_ABSENT":Ljava/lang/String;
    const-string v12, "ril.ICC_TYPE"

    invoke-static {v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 151
    .local v9, "sim1State":Ljava/lang/String;
    const-string v12, "ril.ICC_TYPE_1"

    invoke-static {v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 153
    .local v11, "sim2State":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "phone1_on"

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_5

    const/4 v8, 0x1

    .line 155
    .local v8, "sim1Enabled":Z
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "phone2_on"

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_6

    const/4 v10, 0x1

    .line 157
    .local v10, "sim2Enabled":Z
    :goto_2
    const-string v12, "0"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7

    if-eqz v8, :cond_7

    const-string v12, "0"

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7

    if-eqz v10, :cond_7

    .line 163
    const/4 v12, 0x2

    goto :goto_0

    .line 153
    .end local v8    # "sim1Enabled":Z
    .end local v10    # "sim2Enabled":Z
    :cond_5
    const/4 v8, 0x0

    goto :goto_1

    .line 155
    .restart local v8    # "sim1Enabled":Z
    :cond_6
    const/4 v10, 0x0

    goto :goto_2

    .line 167
    .restart local v10    # "sim2Enabled":Z
    :cond_7
    const-string v12, "0"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    const-string v12, "0"

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 173
    const/4 v12, 0x0

    goto :goto_0

    .line 181
    :cond_8
    const/4 v12, 0x1

    goto :goto_0

    .line 189
    .end local v1    # "SP_KEY_SIM_STATE_SIM1":Ljava/lang/String;
    .end local v2    # "SP_KEY_SIM_STATE_SIM2":Ljava/lang/String;
    .end local v6    # "SP_VALUE_SIM_STATE_ABSENT":Ljava/lang/String;
    .end local v8    # "sim1Enabled":Z
    .end local v9    # "sim1State":Ljava/lang/String;
    .end local v10    # "sim2Enabled":Z
    .end local v11    # "sim2State":Ljava/lang/String;
    :cond_9
    const/4 v12, 0x0

    goto :goto_0
.end method

.method private static getTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v1, :cond_1

    .line 89
    :cond_0
    const-string v0, "SimInformation"

    .line 93
    .local v0, "TAG":Ljava/lang/String;
    const-string v1, "SimInformation"

    .line 97
    :goto_0
    return-object v1

    .end local v0    # "TAG":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isDualSim()Z
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 385
    const/4 v8, 0x0

    .line 389
    .local v8, "result":Z
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v9, :cond_1

    .line 391
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getActiveSubInfoList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x2

    if-lt v9, v10, :cond_0

    .line 393
    const/4 v8, 0x1

    .line 469
    :cond_0
    :goto_0
    return v8

    .line 397
    :cond_1
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-eqz v9, :cond_0

    .line 399
    const/4 v1, 0x0

    .line 401
    .local v1, "classPlugInServiceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    .line 403
    .local v0, "classPlugInDsdsService":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-array v7, v11, [Ljava/lang/Class;

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v10

    .line 405
    .local v7, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v6, 0x0

    .line 407
    .local v6, "objDsdsService":Ljava/lang/Object;
    const/4 v3, 0x0

    .line 409
    .local v3, "methodGetService":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    .line 423
    .local v4, "methodIsDualSim":Ljava/lang/reflect/Method;
    :try_start_0
    const-string v9, "com.android.plugin.PlugInServiceManager"

    invoke-static {v9}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 425
    const-string v9, "com.android.plugin.dsds.PlugInDsdsService"

    invoke-static {v9}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 427
    const-string v9, "getService"

    invoke-virtual {v1, v9, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 429
    const-string v10, "isDualSIM"

    const/4 v9, 0x0

    check-cast v9, [Ljava/lang/Class;

    invoke-virtual {v0, v10, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 431
    const-string v9, "NAME"

    invoke-virtual {v0, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 433
    .local v5, "name":Ljava/lang/String;
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    invoke-virtual {v3, v1, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 435
    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v4, v6, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    move-result v8

    goto :goto_0

    .line 437
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "objDsdsService":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 439
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 441
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v2

    .line 443
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 445
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v2

    .line 447
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 449
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v2

    .line 451
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 453
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v2

    .line 455
    .local v2, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v2}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 457
    .end local v2    # "e":Ljava/lang/NoSuchFieldException;
    :catch_5
    move-exception v2

    .line 459
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    .line 461
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_6
    move-exception v2

    .line 463
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
