.class public Lcom/sec/android/gallery3d/util/ReverseGeocoder;
.super Ljava/lang/Object;
.source "ReverseGeocoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;
    }
.end annotation


# static fields
.field public static final EARTH_RADIUS_METERS:I = 0x615299

.field private static final GEO_CACHE_FILE:Ljava/lang/String; = "rev_geocoding"

.field private static final GEO_CACHE_MAX_BYTES:I = 0x7d000

.field private static final GEO_CACHE_MAX_ENTRIES:I = 0x3e8

.field private static final GEO_CACHE_VERSION:I = 0x0

.field public static final LAT_MAX:I = 0x5a

.field public static final LAT_MIN:I = -0x5a

.field public static final LON_MAX:I = 0xb4

.field public static final LON_MIN:I = -0xb4

.field private static final MAX_COUNTRY_NAME_LENGTH:I = 0x8

.field private static final MAX_LOCALITY_MILE_RANGE:I = 0x14

.field private static final TAG:Ljava/lang/String;

.field private static sCurrentAddress:Landroid/location/Address;


# instance fields
.field private mCanceled:Z

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mGeoCache:Lcom/sec/android/gallery3d/common/BlobCache;

.field private mGeocoder:Landroid/location/Geocoder;

.field private mLookUpAddressThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mCanceled:Z

    .line 84
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mContext:Landroid/content/Context;

    .line 85
    new-instance v0, Landroid/location/Geocoder;

    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mGeocoder:Landroid/location/Geocoder;

    .line 86
    const-string v0, "rev_geocoding"

    const/16 v1, 0x3e8

    const v2, 0x7d000

    invoke-static {p1, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/util/CacheManager;->getCache(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mGeoCache:Lcom/sec/android/gallery3d/common/BlobCache;

    .line 89
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/util/ReverseGeocoder;DDJ)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    .param p1, "x1"    # D
    .param p3, "x2"    # D
    .param p5, "x3"    # J

    .prologue
    .line 39
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->insertGeoCache(DDJ)V

    return-void
.end method

.method private checkNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "locality"    # Ljava/lang/String;

    .prologue
    .line 355
    if-nez p1, :cond_1

    .line 356
    const-string p1, ""

    .line 359
    .end local p1    # "locality":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 357
    .restart local p1    # "locality":Ljava/lang/String;
    :cond_1
    const-string v0, "null"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    const-string p1, ""

    goto :goto_0
.end method

.method private getLocalityAdminForAddress(Landroid/location/Address;Z)Ljava/lang/String;
    .locals 4
    .param p1, "addr"    # Landroid/location/Address;
    .param p2, "approxLocation"    # Z

    .prologue
    .line 411
    if-nez p1, :cond_1

    .line 412
    const-string v1, ""

    .line 428
    :cond_0
    :goto_0
    return-object v1

    .line 413
    :cond_1
    invoke-virtual {p1}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v1

    .line 414
    .local v1, "localityAdminStr":Ljava/lang/String;
    if-eqz v1, :cond_3

    const-string v2, "null"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 415
    if-eqz p2, :cond_2

    .line 422
    :cond_2
    invoke-virtual {p1}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v0

    .line 423
    .local v0, "adminArea":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 424
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 428
    .end local v0    # "adminArea":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private insertGeoCache(DDJ)V
    .locals 15
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "locationKey"    # J

    .prologue
    .line 550
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mGeocoder:Landroid/location/Geocoder;

    const/4 v8, 0x1

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v8}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v9

    .line 552
    .local v9, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v9, :cond_2

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mCanceled:Z

    if-nez v3, :cond_2

    .line 553
    sget-object v3, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    const-string v4, " in an adding address routine"

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    const/4 v3, 0x0

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Address;

    .line 555
    .local v2, "address":Landroid/location/Address;
    new-instance v10, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v10}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 556
    .local v10, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v11, Ljava/io/DataOutputStream;

    invoke-direct {v11, v10}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 557
    .local v11, "dos":Ljava/io/DataOutputStream;
    invoke-virtual {v2}, Landroid/location/Address;->getLocale()Ljava/util/Locale;

    move-result-object v13

    .line 558
    .local v13, "locale":Ljava/util/Locale;
    invoke-virtual {v13}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 559
    invoke-virtual {v13}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 560
    invoke-virtual {v13}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 562
    invoke-virtual {v2}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 563
    invoke-virtual {v2}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v14

    .line 564
    .local v14, "numAddressLines":I
    invoke-virtual {v11, v14}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 565
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    if-gt v12, v14, :cond_0

    .line 566
    invoke-virtual {v2, v12}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 565
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 568
    :cond_0
    invoke-virtual {v2}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 569
    invoke-virtual {v2}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 570
    invoke-virtual {v2}, Landroid/location/Address;->getSubLocality()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 571
    invoke-virtual {v2}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 572
    invoke-virtual {v2}, Landroid/location/Address;->getSubAdminArea()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 574
    invoke-virtual {v2}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 575
    invoke-virtual {v2}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 576
    invoke-virtual {v2}, Landroid/location/Address;->getPostalCode()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 577
    invoke-virtual {v2}, Landroid/location/Address;->getPhone()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 578
    invoke-virtual {v2}, Landroid/location/Address;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 580
    invoke-virtual {v11}, Ljava/io/DataOutputStream;->flush()V

    .line 581
    iget-object v3, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mGeoCache:Lcom/sec/android/gallery3d/common/BlobCache;

    if-eqz v3, :cond_1

    .line 582
    sget-object v3, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    const-string v4, " insert to GeoCache"

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    iget-object v3, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mGeoCache:Lcom/sec/android/gallery3d/common/BlobCache;

    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    move-wide/from16 v0, p5

    invoke-virtual {v3, v0, v1, v4}, Lcom/sec/android/gallery3d/common/BlobCache;->insert(J[B)V

    .line 585
    :cond_1
    invoke-virtual {v11}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 589
    .end local v2    # "address":Landroid/location/Address;
    .end local v9    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v10    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v11    # "dos":Ljava/io/DataOutputStream;
    .end local v12    # "i":I
    .end local v13    # "locale":Ljava/util/Locale;
    .end local v14    # "numAddressLines":I
    :cond_2
    :goto_1
    return-void

    .line 587
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public static final readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;
    .locals 2
    .param p0, "dis"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 604
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 605
    .local v0, "retVal":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 606
    const/4 v0, 0x0

    .line 607
    .end local v0    # "retVal":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private valueIfEqual(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "a"    # Ljava/lang/String;
    .param p2, "b"    # Ljava/lang/String;

    .prologue
    .line 592
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .end local p1    # "a":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "a":Ljava/lang/String;
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public static final writeUTF(Ljava/io/DataOutputStream;Ljava/lang/String;)V
    .locals 1
    .param p0, "dos"    # Ljava/io/DataOutputStream;
    .param p1, "string"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 596
    if-nez p1, :cond_0

    .line 597
    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 601
    :goto_0
    return-void

    .line 599
    :cond_0
    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public computeAddress(Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;)Ljava/lang/String;
    .locals 49
    .param p1, "set"    # Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;

    .prologue
    .line 103
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    .line 104
    .local v6, "setMinLatitude":D
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLongitude:D

    .line 105
    .local v8, "setMinLongitude":D
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    .line 106
    .local v12, "setMaxLatitude":D
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLongitude:D

    .line 107
    .local v14, "setMaxLongitude":D
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    move-wide/from16 v16, v0

    sub-double v10, v10, v16

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    move-result-wide v10

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(D)D

    move-result-wide v16

    cmpg-double v5, v10, v16

    if-gez v5, :cond_0

    .line 109
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLatitude:D

    .line 110
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    .line 111
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLatitude:D

    .line 112
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    .line 114
    :cond_0
    const/4 v10, 0x1

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v10}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZ)Landroid/location/Address;

    move-result-object v4

    .line 115
    .local v4, "addr1":Landroid/location/Address;
    const/16 v16, 0x1

    move-object/from16 v11, p0

    invoke-virtual/range {v11 .. v16}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZ)Landroid/location/Address;

    move-result-object v29

    .line 116
    .local v29, "addr2":Landroid/location/Address;
    if-nez v4, :cond_1

    .line 117
    move-object/from16 v4, v29

    .line 118
    :cond_1
    if-nez v29, :cond_2

    .line 119
    move-object/from16 v29, v4

    .line 120
    :cond_2
    if-eqz v4, :cond_3

    if-nez v29, :cond_5

    .line 121
    :cond_3
    const/16 v35, 0x0

    .line 287
    :cond_4
    :goto_0
    return-object v35

    .line 126
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mContext:Landroid/content/Context;

    const-string v10, "location"

    invoke-virtual {v5, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Landroid/location/LocationManager;

    .line 128
    .local v44, "locationManager":Landroid/location/LocationManager;
    const/16 v43, 0x0

    .line 129
    .local v43, "location":Landroid/location/Location;
    invoke-virtual/range {v44 .. v44}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v48

    .line 130
    .local v48, "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v48, :cond_a

    invoke-interface/range {v48 .. v48}, Ljava/util/List;->size()I

    move-result v47

    .line 131
    .local v47, "providerSize":I
    :goto_1
    const/16 v42, 0x0

    .local v42, "i":I
    :goto_2
    move/from16 v0, v42

    move/from16 v1, v47

    if-ge v0, v1, :cond_6

    .line 132
    move-object/from16 v0, v48

    move/from16 v1, v42

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Ljava/lang/String;

    .line 133
    .local v46, "provider":Ljava/lang/String;
    if-eqz v46, :cond_b

    move-object/from16 v0, v44

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v43

    .line 134
    :goto_3
    if-eqz v43, :cond_c

    .line 137
    .end local v46    # "provider":Ljava/lang/String;
    :cond_6
    const-string v39, ""

    .line 138
    .local v39, "currentCity":Ljava/lang/String;
    const-string v38, ""

    .line 139
    .local v38, "currentAdminArea":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v40

    .line 140
    .local v40, "currentCountry":Ljava/lang/String;
    if-eqz v43, :cond_7

    .line 141
    invoke-virtual/range {v43 .. v43}, Landroid/location/Location;->getLatitude()D

    move-result-wide v18

    invoke-virtual/range {v43 .. v43}, Landroid/location/Location;->getLongitude()D

    move-result-wide v20

    const/16 v22, 0x1

    move-object/from16 v17, p0

    invoke-virtual/range {v17 .. v22}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZ)Landroid/location/Address;

    move-result-object v37

    .line 143
    .local v37, "currentAddress":Landroid/location/Address;
    if-nez v37, :cond_d

    .line 144
    sget-object v37, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->sCurrentAddress:Landroid/location/Address;

    .line 148
    :goto_4
    if-eqz v37, :cond_7

    invoke-virtual/range {v37 .. v37}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 149
    invoke-virtual/range {v37 .. v37}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    .line 150
    invoke-virtual/range {v37 .. v37}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    .line 151
    invoke-virtual/range {v37 .. v37}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    .line 155
    .end local v37    # "currentAddress":Landroid/location/Address;
    :cond_7
    const/16 v35, 0x0

    .line 156
    .local v35, "closestCommonLocation":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 157
    .local v28, "addr1Locality":Ljava/lang/String;
    invoke-virtual/range {v29 .. v29}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 158
    .local v33, "addr2Locality":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 159
    .local v25, "addr1AdminArea":Ljava/lang/String;
    invoke-virtual/range {v29 .. v29}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 160
    .local v30, "addr2AdminArea":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 161
    .local v27, "addr1CountryCode":Ljava/lang/String;
    invoke-virtual/range {v29 .. v29}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 163
    .local v32, "addr2CountryCode":Ljava/lang/String;
    move-object/from16 v0, v39

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    move-object/from16 v0, v39

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 164
    :cond_8
    move-object/from16 v45, v39

    .line 165
    .local v45, "otherCity":Ljava/lang/String;
    move-object/from16 v0, v39

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 166
    move-object/from16 v45, v33

    .line 167
    invoke-virtual/range {v45 .. v45}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_9

    .line 168
    move-object/from16 v45, v30

    .line 169
    move-object/from16 v0, v40

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 170
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v32

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    .line 173
    :cond_9
    move-object/from16 v33, v28

    .line 174
    move-object/from16 v30, v25

    .line 175
    move-object/from16 v32, v27

    .line 188
    :goto_5
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v5

    const/4 v10, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v10}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v10}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->valueIfEqual(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 189
    if-eqz v35, :cond_10

    const-string v5, "null"

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 190
    move-object/from16 v0, v39

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 191
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " - "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v45

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_0

    .line 130
    .end local v25    # "addr1AdminArea":Ljava/lang/String;
    .end local v27    # "addr1CountryCode":Ljava/lang/String;
    .end local v28    # "addr1Locality":Ljava/lang/String;
    .end local v30    # "addr2AdminArea":Ljava/lang/String;
    .end local v32    # "addr2CountryCode":Ljava/lang/String;
    .end local v33    # "addr2Locality":Ljava/lang/String;
    .end local v35    # "closestCommonLocation":Ljava/lang/String;
    .end local v38    # "currentAdminArea":Ljava/lang/String;
    .end local v39    # "currentCity":Ljava/lang/String;
    .end local v40    # "currentCountry":Ljava/lang/String;
    .end local v42    # "i":I
    .end local v45    # "otherCity":Ljava/lang/String;
    .end local v47    # "providerSize":I
    :cond_a
    const/16 v47, 0x0

    goto/16 :goto_1

    .line 133
    .restart local v42    # "i":I
    .restart local v46    # "provider":Ljava/lang/String;
    .restart local v47    # "providerSize":I
    :cond_b
    const/16 v43, 0x0

    goto/16 :goto_3

    .line 131
    :cond_c
    add-int/lit8 v42, v42, 0x1

    goto/16 :goto_2

    .line 146
    .end local v46    # "provider":Ljava/lang/String;
    .restart local v37    # "currentAddress":Landroid/location/Address;
    .restart local v38    # "currentAdminArea":Ljava/lang/String;
    .restart local v39    # "currentCity":Ljava/lang/String;
    .restart local v40    # "currentCountry":Ljava/lang/String;
    :cond_d
    sput-object v37, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->sCurrentAddress:Landroid/location/Address;

    goto/16 :goto_4

    .line 177
    .end local v37    # "currentAddress":Landroid/location/Address;
    .restart local v25    # "addr1AdminArea":Ljava/lang/String;
    .restart local v27    # "addr1CountryCode":Ljava/lang/String;
    .restart local v28    # "addr1Locality":Ljava/lang/String;
    .restart local v30    # "addr2AdminArea":Ljava/lang/String;
    .restart local v32    # "addr2CountryCode":Ljava/lang/String;
    .restart local v33    # "addr2Locality":Ljava/lang/String;
    .restart local v35    # "closestCommonLocation":Ljava/lang/String;
    .restart local v45    # "otherCity":Ljava/lang/String;
    :cond_e
    move-object/from16 v45, v28

    .line 178
    invoke-virtual/range {v45 .. v45}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_f

    .line 179
    move-object/from16 v45, v25

    .line 180
    move-object/from16 v0, v40

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 181
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    .line 184
    :cond_f
    move-object/from16 v28, v33

    .line 185
    move-object/from16 v25, v30

    .line 186
    move-object/from16 v27, v32

    goto/16 :goto_5

    .line 197
    :cond_10
    invoke-virtual {v4}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v29 .. v29}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v10}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->valueIfEqual(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 198
    if-eqz v35, :cond_11

    const-string v5, "null"

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 204
    .end local v45    # "otherCity":Ljava/lang/String;
    :cond_11
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move-object/from16 v2, v33

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->valueIfEqual(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 205
    if-eqz v35, :cond_13

    const-string v5, ""

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_13

    .line 206
    move-object/from16 v34, v25

    .line 207
    .local v34, "adminArea":Ljava/lang/String;
    move-object/from16 v36, v27

    .line 208
    .local v36, "countryCode":Ljava/lang/String;
    if-eqz v34, :cond_4

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_4

    .line 209
    move-object/from16 v0, v36

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    .line 210
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v34

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v36

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_0

    .line 212
    :cond_12
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v34

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_0

    .line 220
    .end local v34    # "adminArea":Ljava/lang/String;
    .end local v36    # "countryCode":Ljava/lang/String;
    :cond_13
    move-object/from16 v0, v38

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    move-object/from16 v0, v38

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 221
    const-string v5, ""

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 222
    move-object/from16 v28, v33

    .line 224
    :cond_14
    const-string v5, ""

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 225
    move-object/from16 v33, v28

    .line 227
    :cond_15
    const-string v5, ""

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_17

    .line 228
    move-object/from16 v0, v28

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 229
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v38

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_0

    .line 231
    :cond_16
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " - "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_0

    .line 239
    :cond_17
    const/4 v5, 0x1

    new-array v0, v5, [F

    move-object/from16 v24, v0

    .local v24, "distanceFloat":[F
    move-wide/from16 v16, v6

    move-wide/from16 v18, v8

    move-wide/from16 v20, v12

    move-wide/from16 v22, v14

    .line 240
    invoke-static/range {v16 .. v24}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 242
    const/4 v5, 0x0

    aget v5, v24, v5

    float-to-double v10, v5

    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/util/GalleryUtils;->toMile(D)D

    move-result-wide v10

    double-to-int v0, v10

    move/from16 v41, v0

    .line 243
    .local v41, "distance":I
    const/16 v5, 0x14

    move/from16 v0, v41

    if-ge v0, v5, :cond_18

    .line 246
    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->getLocalityAdminForAddress(Landroid/location/Address;Z)Ljava/lang/String;

    move-result-object v35

    .line 247
    if-nez v35, :cond_4

    .line 250
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->getLocalityAdminForAddress(Landroid/location/Address;Z)Ljava/lang/String;

    move-result-object v35

    .line 251
    if-nez v35, :cond_4

    .line 257
    :cond_18
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->valueIfEqual(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 258
    if-eqz v35, :cond_19

    const-string v5, ""

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_19

    .line 259
    move-object/from16 v36, v27

    .line 260
    .restart local v36    # "countryCode":Ljava/lang/String;
    move-object/from16 v0, v36

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 261
    if-eqz v36, :cond_4

    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_4

    .line 262
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v36

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_0

    .line 269
    .end local v36    # "countryCode":Ljava/lang/String;
    :cond_19
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->valueIfEqual(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 270
    if-eqz v35, :cond_1a

    const-string v5, ""

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 274
    :cond_1a
    invoke-virtual {v4}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v26

    .line 275
    .local v26, "addr1Country":Ljava/lang/String;
    invoke-virtual/range {v29 .. v29}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v31

    .line 276
    .local v31, "addr2Country":Ljava/lang/String;
    if-nez v26, :cond_1b

    .line 277
    move-object/from16 v26, v27

    .line 278
    :cond_1b
    if-nez v31, :cond_1c

    .line 279
    move-object/from16 v31, v32

    .line 280
    :cond_1c
    if-eqz v26, :cond_1d

    if-nez v31, :cond_1e

    .line 281
    :cond_1d
    const/16 v35, 0x0

    goto/16 :goto_0

    .line 282
    :cond_1e
    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v10, 0x8

    if-gt v5, v10, :cond_1f

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v10, 0x8

    if-le v5, v10, :cond_20

    .line 283
    :cond_1f
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " - "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v32

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_0

    .line 285
    :cond_20
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " - "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_0
.end method

.method public computeAddressOverLocality(Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;)Ljava/lang/String;
    .locals 32
    .param p1, "set"    # Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;

    .prologue
    .line 293
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    .line 294
    .local v6, "setMinLatitude":D
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLongitude:D

    .line 295
    .local v8, "setMinLongitude":D
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    .line 296
    .local v12, "setMaxLatitude":D
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLongitude:D

    .line 297
    .local v14, "setMaxLongitude":D
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    move-wide/from16 v28, v0

    sub-double v10, v10, v28

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    move-result-wide v10

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    move-wide/from16 v28, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    move-wide/from16 v30, v0

    sub-double v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->abs(D)D

    move-result-wide v28

    cmpg-double v5, v10, v28

    if-gez v5, :cond_0

    .line 299
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLatitude:D

    .line 300
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    .line 301
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLatitude:D

    .line 302
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    .line 304
    :cond_0
    const/4 v10, 0x1

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v10}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZ)Landroid/location/Address;

    move-result-object v4

    .line 305
    .local v4, "addr1":Landroid/location/Address;
    const/16 v16, 0x1

    move-object/from16 v11, p0

    invoke-virtual/range {v11 .. v16}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZ)Landroid/location/Address;

    move-result-object v21

    .line 306
    .local v21, "addr2":Landroid/location/Address;
    if-nez v4, :cond_1

    .line 307
    move-object/from16 v4, v21

    .line 308
    :cond_1
    if-nez v21, :cond_2

    .line 309
    move-object/from16 v21, v4

    .line 310
    :cond_2
    if-eqz v4, :cond_3

    if-nez v21, :cond_5

    .line 311
    :cond_3
    const/16 v26, 0x0

    .line 350
    :cond_4
    :goto_0
    return-object v26

    .line 314
    :cond_5
    const/16 v26, 0x0

    .line 315
    .local v26, "closestCommonLocation":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 316
    .local v20, "addr1Locality":Ljava/lang/String;
    invoke-virtual/range {v21 .. v21}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 317
    .local v25, "addr2Locality":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 318
    .local v17, "addr1AdminArea":Ljava/lang/String;
    invoke-virtual/range {v21 .. v21}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 319
    .local v22, "addr2AdminArea":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 320
    .local v19, "addr1CountryCode":Ljava/lang/String;
    invoke-virtual/range {v21 .. v21}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 323
    .local v24, "addr2CountryCode":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->valueIfEqual(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 324
    if-eqz v26, :cond_6

    const-string v5, ""

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 329
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->valueIfEqual(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 330
    if-eqz v26, :cond_7

    const-string v5, ""

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 334
    :cond_7
    invoke-virtual {v4}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v18

    .line 335
    .local v18, "addr1Country":Ljava/lang/String;
    invoke-virtual/range {v21 .. v21}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v23

    .line 336
    .local v23, "addr2Country":Ljava/lang/String;
    if-nez v18, :cond_8

    .line 337
    move-object/from16 v18, v19

    .line 338
    :cond_8
    if-nez v23, :cond_9

    .line 339
    move-object/from16 v23, v24

    .line 340
    :cond_9
    if-eqz v18, :cond_a

    if-nez v23, :cond_b

    .line 341
    :cond_a
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " - "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_0

    .line 343
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->valueIfEqual(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 344
    if-eqz v26, :cond_c

    const-string v5, ""

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 347
    :cond_c
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " - "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_0
.end method

.method public getLocality(DD)Ljava/lang/String;
    .locals 21
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 364
    const-wide/16 v6, 0x0

    cmpl-double v5, p1, v6

    if-nez v5, :cond_1

    const-wide/16 v6, 0x0

    cmpl-double v5, p3, v6

    if-nez v5, :cond_1

    .line 365
    const/4 v14, 0x0

    .line 405
    :cond_0
    :goto_0
    return-object v14

    .line 367
    :cond_1
    new-instance v20, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;

    invoke-direct/range {v20 .. v20}, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;-><init>()V

    .line 369
    .local v20, "set":Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;
    move-wide/from16 v16, p1

    .line 370
    .local v16, "itemLatitude":D
    move-wide/from16 v18, p3

    .line 372
    .local v18, "itemLongitude":D
    move-object/from16 v0, v20

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    cmpl-double v5, v6, v16

    if-lez v5, :cond_2

    .line 373
    move-wide/from16 v0, v16

    move-object/from16 v2, v20

    iput-wide v0, v2, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    .line 374
    move-wide/from16 v0, v18

    move-object/from16 v2, v20

    iput-wide v0, v2, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLongitude:D

    .line 376
    :cond_2
    move-object/from16 v0, v20

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    cmpg-double v5, v6, v16

    if-gez v5, :cond_3

    .line 377
    move-wide/from16 v0, v16

    move-object/from16 v2, v20

    iput-wide v0, v2, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    .line 378
    move-wide/from16 v0, v18

    move-object/from16 v2, v20

    iput-wide v0, v2, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLongitude:D

    .line 380
    :cond_3
    move-object/from16 v0, v20

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    cmpl-double v5, v6, v18

    if-lez v5, :cond_4

    .line 381
    move-wide/from16 v0, v16

    move-object/from16 v2, v20

    iput-wide v0, v2, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLatitude:D

    .line 382
    move-wide/from16 v0, v18

    move-object/from16 v2, v20

    iput-wide v0, v2, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    .line 384
    :cond_4
    move-object/from16 v0, v20

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    cmpg-double v5, v6, v18

    if-gez v5, :cond_5

    .line 385
    move-wide/from16 v0, v16

    move-object/from16 v2, v20

    iput-wide v0, v2, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLatitude:D

    .line 386
    move-wide/from16 v0, v18

    move-object/from16 v2, v20

    iput-wide v0, v2, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    .line 389
    :cond_5
    move-object/from16 v0, v20

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v12}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZZZ)Landroid/location/Address;

    move-result-object v4

    .line 390
    .local v4, "addr1":Landroid/location/Address;
    move-object/from16 v0, v20

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v12}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZZZ)Landroid/location/Address;

    move-result-object v13

    .line 391
    .local v13, "addr2":Landroid/location/Address;
    if-nez v4, :cond_6

    .line 392
    move-object v4, v13

    .line 393
    :cond_6
    if-nez v13, :cond_7

    .line 394
    move-object v13, v4

    .line 395
    :cond_7
    if-eqz v4, :cond_8

    if-nez v13, :cond_9

    .line 396
    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 399
    :cond_9
    invoke-virtual {v4}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 400
    .local v15, "locality":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 402
    .local v14, "countryName":Ljava/lang/String;
    const-string v5, ""

    invoke-virtual {v5, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    move-object v14, v15

    .line 405
    goto/16 :goto_0
.end method

.method public lookupAddress(DDZ)Landroid/location/Address;
    .locals 9
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "useCache"    # Z

    .prologue
    .line 433
    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZZZ)Landroid/location/Address;

    move-result-object v0

    return-object v0
.end method

.method public lookupAddress(DDZZZ)Landroid/location/Address;
    .locals 35
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "useCache"    # Z
    .param p6, "useThread"    # Z
    .param p7, "retry"    # Z

    .prologue
    .line 440
    const/16 v21, 0x0

    .line 441
    .local v21, "bos":Ljava/io/ByteArrayOutputStream;
    const/16 v26, 0x0

    .line 442
    .local v26, "dos":Ljava/io/DataOutputStream;
    const/16 v24, 0x0

    .line 443
    .local v24, "dis":Ljava/io/DataInputStream;
    invoke-static/range {p1 .. p4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v5

    if-nez v5, :cond_0

    .line 444
    const/4 v12, 0x0

    .line 545
    :goto_0
    return-object v12

    .line 449
    :cond_0
    const-wide v6, 0x4056800000000000L    # 90.0

    add-double v6, v6, p1

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, v8

    const-wide v8, 0x4056800000000000L    # 90.0

    mul-double/2addr v6, v8

    const-wide v8, 0x4066800000000000L    # 180.0

    add-double v8, v8, p3

    add-double/2addr v6, v8

    const-wide v8, 0x415854a640000000L    # 6378137.0

    mul-double/2addr v6, v8

    double-to-long v10, v6

    .line 452
    .local v10, "locationKey":J
    const/16 v22, 0x0

    .line 453
    .local v22, "cachedLocation":[B
    if-eqz p5, :cond_1

    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mGeoCache:Lcom/sec/android/gallery3d/common/BlobCache;

    if-eqz v5, :cond_1

    .line 454
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mGeoCache:Lcom/sec/android/gallery3d/common/BlobCache;

    invoke-virtual {v5, v10, v11}, Lcom/sec/android/gallery3d/common/BlobCache;->lookup(J)[B

    move-result-object v22

    .line 456
    :cond_1
    const/4 v12, 0x0

    .line 457
    .local v12, "address":Landroid/location/Address;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v5}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v30

    .line 460
    .local v30, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v22, :cond_2

    move-object/from16 v0, v22

    array-length v5, v0

    if-nez v5, :cond_5

    .line 461
    :cond_2
    if-eqz v30, :cond_3

    invoke-virtual/range {v30 .. v30}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mCanceled:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v5, :cond_4

    .line 462
    :cond_3
    const/4 v12, 0x0

    .line 539
    .end local v12    # "address":Landroid/location/Address;
    sget-object v5, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    const-string v6, "exception happened during getting an address"

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 541
    invoke-static/range {v26 .. v26}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 542
    invoke-static/range {v24 .. v24}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .line 464
    .restart local v12    # "address":Landroid/location/Address;
    :cond_4
    if-eqz p6, :cond_8

    .line 465
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mLookUpAddressThread:Ljava/lang/Thread;

    .line 466
    new-instance v4, Lcom/sec/android/gallery3d/util/ReverseGeocoder$1;

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-wide/from16 v8, p3

    invoke-direct/range {v4 .. v11}, Lcom/sec/android/gallery3d/util/ReverseGeocoder$1;-><init>(Lcom/sec/android/gallery3d/util/ReverseGeocoder;DDJ)V

    .line 472
    .local v4, "thread":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 473
    const-wide/16 v6, 0x7d0

    invoke-virtual {v4, v6, v7}, Ljava/lang/Thread;->join(J)V

    .line 481
    .end local v4    # "thread":Ljava/lang/Thread;
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mGeoCache:Lcom/sec/android/gallery3d/common/BlobCache;

    if-eqz v5, :cond_6

    .line 482
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mGeoCache:Lcom/sec/android/gallery3d/common/BlobCache;

    invoke-virtual {v5, v10, v11}, Lcom/sec/android/gallery3d/common/BlobCache;->lookup(J)[B

    move-result-object v22

    .line 484
    :cond_6
    if-eqz v22, :cond_7

    move-object/from16 v0, v22

    array-length v5, v0

    if-nez v5, :cond_9

    .line 485
    :cond_7
    sget-object v5, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    const-string v6, "cachedLocation is null"

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 486
    const/4 v12, 0x0

    .line 539
    .end local v12    # "address":Landroid/location/Address;
    sget-object v5, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    const-string v6, "exception happened during getting an address"

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 541
    invoke-static/range {v26 .. v26}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 542
    invoke-static/range {v24 .. v24}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 475
    .restart local v12    # "address":Landroid/location/Address;
    :cond_8
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    .local v32, "t":Ljava/lang/Long;
    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-wide/from16 v8, p3

    .line 476
    invoke-direct/range {v5 .. v11}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->insertGeoCache(DDJ)V

    .line 477
    sget-object v5, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insertGeoCache : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    sub-long/2addr v8, v14

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 534
    .end local v12    # "address":Landroid/location/Address;
    .end local v30    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v32    # "t":Ljava/lang/Long;
    :catch_0
    move-exception v5

    .line 539
    :goto_2
    sget-object v5, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    const-string v6, "exception happened during getting an address"

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 541
    invoke-static/range {v26 .. v26}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 542
    invoke-static/range {v24 .. v24}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 545
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 489
    .restart local v12    # "address":Landroid/location/Address;
    .restart local v30    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_9
    :try_start_3
    new-instance v25, Ljava/io/DataInputStream;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, v22

    invoke-direct {v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    move-object/from16 v0, v25

    invoke-direct {v0, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 491
    .end local v24    # "dis":Ljava/io/DataInputStream;
    .local v25, "dis":Ljava/io/DataInputStream;
    :try_start_4
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v28

    .line 492
    .local v28, "language":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v23

    .line 493
    .local v23, "country":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v33

    .line 494
    .local v33, "variant":Ljava/lang/String;
    const/16 v29, 0x0

    .line 495
    .local v29, "locale":Ljava/util/Locale;
    if-eqz v28, :cond_a

    .line 496
    if-nez v23, :cond_c

    .line 497
    new-instance v29, Ljava/util/Locale;

    .end local v29    # "locale":Ljava/util/Locale;
    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 504
    .restart local v29    # "locale":Ljava/util/Locale;
    :cond_a
    :goto_3
    if-eqz v29, :cond_b

    invoke-virtual/range {v29 .. v29}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 505
    :cond_b
    invoke-virtual/range {v25 .. v25}, Ljava/io/DataInputStream;->close()V

    .line 506
    if-nez p7, :cond_e

    .line 507
    const/16 v18, 0x0

    const/16 v20, 0x1

    move-object/from16 v13, p0

    move-wide/from16 v14, p1

    move-wide/from16 v16, p3

    move/from16 v19, p6

    invoke-virtual/range {v13 .. v20}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZZZ)Landroid/location/Address;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v12

    .line 539
    .end local v12    # "address":Landroid/location/Address;
    sget-object v5, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    const-string v6, "exception happened during getting an address"

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 541
    invoke-static/range {v26 .. v26}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 542
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object/from16 v24, v25

    .end local v25    # "dis":Ljava/io/DataInputStream;
    .restart local v24    # "dis":Ljava/io/DataInputStream;
    goto/16 :goto_0

    .line 498
    .end local v24    # "dis":Ljava/io/DataInputStream;
    .restart local v12    # "address":Landroid/location/Address;
    .restart local v25    # "dis":Ljava/io/DataInputStream;
    :cond_c
    if-nez v33, :cond_d

    .line 499
    :try_start_5
    new-instance v29, Ljava/util/Locale;

    .end local v29    # "locale":Ljava/util/Locale;
    move-object/from16 v0, v29

    move-object/from16 v1, v28

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v29    # "locale":Ljava/util/Locale;
    goto :goto_3

    .line 501
    :cond_d
    new-instance v29, Ljava/util/Locale;

    .end local v29    # "locale":Ljava/util/Locale;
    move-object/from16 v0, v29

    move-object/from16 v1, v28

    move-object/from16 v2, v23

    move-object/from16 v3, v33

    invoke-direct {v0, v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v29    # "locale":Ljava/util/Locale;
    goto :goto_3

    .line 509
    :cond_e
    sget-object v5, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    const-string v6, "locale is null"

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 510
    const/4 v12, 0x0

    .line 539
    .end local v12    # "address":Landroid/location/Address;
    sget-object v5, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    const-string v6, "exception happened during getting an address"

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 541
    invoke-static/range {v26 .. v26}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 542
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object/from16 v24, v25

    .end local v25    # "dis":Ljava/io/DataInputStream;
    .restart local v24    # "dis":Ljava/io/DataInputStream;
    goto/16 :goto_0

    .line 513
    .end local v24    # "dis":Ljava/io/DataInputStream;
    .restart local v12    # "address":Landroid/location/Address;
    .restart local v25    # "dis":Ljava/io/DataInputStream;
    :cond_f
    :try_start_6
    new-instance v12, Landroid/location/Address;

    .end local v12    # "address":Landroid/location/Address;
    move-object/from16 v0, v29

    invoke-direct {v12, v0}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 515
    .restart local v12    # "address":Landroid/location/Address;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Landroid/location/Address;->setThoroughfare(Ljava/lang/String;)V

    .line 516
    invoke-virtual/range {v25 .. v25}, Ljava/io/DataInputStream;->readInt()I

    move-result v31

    .line 517
    .local v31, "numAddressLines":I
    const/16 v27, 0x0

    .local v27, "i":I
    :goto_4
    move/from16 v0, v27

    move/from16 v1, v31

    if-gt v0, v1, :cond_10

    .line 518
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    move/from16 v0, v27

    invoke-virtual {v12, v0, v5}, Landroid/location/Address;->setAddressLine(ILjava/lang/String;)V

    .line 517
    add-int/lit8 v27, v27, 0x1

    goto :goto_4

    .line 520
    :cond_10
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Landroid/location/Address;->setFeatureName(Ljava/lang/String;)V

    .line 521
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    .line 522
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Landroid/location/Address;->setSubLocality(Ljava/lang/String;)V

    .line 523
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Landroid/location/Address;->setAdminArea(Ljava/lang/String;)V

    .line 524
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Landroid/location/Address;->setSubAdminArea(Ljava/lang/String;)V

    .line 526
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Landroid/location/Address;->setCountryName(Ljava/lang/String;)V

    .line 527
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Landroid/location/Address;->setCountryCode(Ljava/lang/String;)V

    .line 528
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Landroid/location/Address;->setPostalCode(Ljava/lang/String;)V

    .line 529
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Landroid/location/Address;->setPhone(Ljava/lang/String;)V

    .line 530
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->readUTF(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Landroid/location/Address;->setUrl(Ljava/lang/String;)V

    .line 531
    invoke-virtual/range {v25 .. v25}, Ljava/io/DataInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 539
    sget-object v5, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    const-string v6, "exception happened during getting an address"

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 541
    invoke-static/range {v26 .. v26}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 542
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object/from16 v24, v25

    .end local v25    # "dis":Ljava/io/DataInputStream;
    .restart local v24    # "dis":Ljava/io/DataInputStream;
    goto/16 :goto_0

    .line 539
    .end local v12    # "address":Landroid/location/Address;
    .end local v23    # "country":Ljava/lang/String;
    .end local v27    # "i":I
    .end local v28    # "language":Ljava/lang/String;
    .end local v29    # "locale":Ljava/util/Locale;
    .end local v30    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v31    # "numAddressLines":I
    .end local v33    # "variant":Ljava/lang/String;
    :catchall_0
    move-exception v5

    :goto_5
    sget-object v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->TAG:Ljava/lang/String;

    const-string v7, "exception happened during getting an address"

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 541
    invoke-static/range {v26 .. v26}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 542
    invoke-static/range {v24 .. v24}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v5

    .line 539
    .end local v24    # "dis":Ljava/io/DataInputStream;
    .restart local v25    # "dis":Ljava/io/DataInputStream;
    .restart local v30    # "networkInfo":Landroid/net/NetworkInfo;
    :catchall_1
    move-exception v5

    move-object/from16 v24, v25

    .end local v25    # "dis":Ljava/io/DataInputStream;
    .restart local v24    # "dis":Ljava/io/DataInputStream;
    goto :goto_5

    .line 534
    .end local v24    # "dis":Ljava/io/DataInputStream;
    .restart local v25    # "dis":Ljava/io/DataInputStream;
    :catch_1
    move-exception v5

    move-object/from16 v24, v25

    .end local v25    # "dis":Ljava/io/DataInputStream;
    .restart local v24    # "dis":Ljava/io/DataInputStream;
    goto/16 :goto_2
.end method

.method public onCancel()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mCanceled:Z

    .line 96
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mLookUpAddressThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->mLookUpAddressThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 98
    :cond_0
    return-void
.end method
