.class Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;
.super Ljava/lang/Object;
.source "PhotoDataAdapter.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenNailBufferJob"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/sec/android/gallery3d/ui/ScreenNail;",
        ">;"
    }
.end annotation


# instance fields
.field private mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

.field private mItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/samsung/gallery/decoder/CacheInterface;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p2, "ci"    # Lcom/sec/samsung/gallery/decoder/CacheInterface;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 1885
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1886
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    .line 1887
    iput-object p3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1888
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 11
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 1892
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v5

    .line 1893
    .local v5, "s":Lcom/sec/android/gallery3d/ui/ScreenNail;
    if-eqz v5, :cond_1

    move-object v7, v5

    .line 1929
    :cond_0
    :goto_0
    return-object v7

    .line 1895
    :cond_1
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->isTemporaryItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1896
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->newPlaceholderScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/ui/ScreenNail;
    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v7

    goto :goto_0

    .line 1899
    :cond_2
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v8, :cond_4

    .line 1900
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2900(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v2

    .line 1901
    .local v2, "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    if-eqz v2, :cond_0

    .line 1903
    const/4 v4, 0x0

    .line 1904
    .local v4, "info":Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->get()Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    move-result-object v3

    .line 1906
    .local v3, "data":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->getCacheBuffer(Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;

    move-result-object v4

    .line 1907
    if-nez v4, :cond_3

    .line 1908
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v8

    invoke-interface {v8, p1}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1909
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_3

    .line 1910
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    mul-int/2addr v8, v9

    mul-int/lit8 v6, v8, 0x4

    .line 1911
    .local v6, "size":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v9, v9, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    iget v9, v9, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->BUFFER_TYPE_MTN:I

    invoke-virtual {v8, v6, v9}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->getBuffer(II)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1912
    .local v1, "buffer":Ljava/nio/ByteBuffer;
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    invoke-virtual {v8, v0, v1}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->getImageBufferDataFromBitmap(Landroid/graphics/Bitmap;Ljava/nio/ByteBuffer;)Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 1916
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "buffer":Ljava/nio/ByteBuffer;
    .end local v6    # "size":I
    :cond_3
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 1919
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v8

    if-nez v8, :cond_0

    .line 1921
    if-eqz v4, :cond_0

    new-instance v7, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v9, v9, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    iget v9, v9, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->BUFFER_TYPE_MTN:I

    invoke-direct {v7, v8, v4, v9}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;-><init>(Lcom/sec/samsung/gallery/decoder/CacheInterface;Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;I)V

    goto/16 :goto_0

    .line 1916
    :catchall_0
    move-exception v7

    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    throw v7

    .line 1923
    .end local v2    # "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    .end local v3    # "data":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    .end local v4    # "info":Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    :cond_4
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8, v10}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v8

    invoke-interface {v8, p1}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1924
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v8

    if-nez v8, :cond_0

    .line 1925
    if-eqz v0, :cond_5

    .line 1926
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-static {v0, v8, v10}, Lcom/sec/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1929
    :cond_5
    if-eqz v0, :cond_0

    new-instance v7, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getTiledTextureResource()Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$500(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    move-result-object v8

    invoke-direct {v7, v0, v8}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;-><init>(Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 1881
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    return-object v0
.end method
