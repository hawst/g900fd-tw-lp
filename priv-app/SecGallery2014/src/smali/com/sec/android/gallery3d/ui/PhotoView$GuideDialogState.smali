.class final enum Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;
.super Ljava/lang/Enum;
.source "PhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "GuideDialogState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

.field public static final enum ADAPT_DISPLAY_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

.field public static final enum CONTEXTUAL_TAG_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

.field public static final enum MOTION_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

.field public static final enum NONE_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 400
    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    const-string v1, "NONE_DIALOG"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->NONE_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    const-string v1, "ADAPT_DISPLAY_DIALOG"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->ADAPT_DISPLAY_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    const-string v1, "MOTION_DIALOG"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->MOTION_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    const-string v1, "CONTEXTUAL_TAG_DIALOG"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->CONTEXTUAL_TAG_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    sget-object v1, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->NONE_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->ADAPT_DISPLAY_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->MOTION_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->CONTEXTUAL_TAG_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->$VALUES:[Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 400
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 400
    const-class v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;
    .locals 1

    .prologue
    .line 400
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->$VALUES:[Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    invoke-virtual {v0}, [Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    return-object v0
.end method
