.class public Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;
.super Landroid/content/ContentProvider;
.source "TableContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;
    }
.end annotation


# static fields
.field private static final ALBUM_TYPE_WHERE:Ljava/lang/String;

.field private static final INVALID_URI:Ljava/lang/String; = "Invalid URI: "

.field private static final NULL_COLUMN_HACK:Ljava/lang/String; = "_id"


# instance fields
.field protected mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

.field private final mMappings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;",
            ">;"
        }
    .end annotation
.end field

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id in (SELECT album_id FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "content_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIKE ?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->ALBUM_TYPE_WHERE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 42
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mMappings:Ljava/util/ArrayList;

    .line 254
    return-void
.end method

.method private final notifyChange(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 252
    return-void
.end method

.method private final whereWithId(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;

    .prologue
    .line 239
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 240
    .local v0, "id":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "_id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 241
    .local v1, "where":Ljava/lang/StringBuilder;
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 243
    const-string v2, " AND ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 247
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public addMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;)V
    .locals 4
    .param p1, "authority"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "mimeSubtype"    # Ljava/lang/String;
    .param p4, "table"    # Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mMappings:Ljava/util/ArrayList;

    .line 53
    .local v0, "mappings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;>;"
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 54
    .local v1, "matcher":Landroid/content/UriMatcher;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, p1, p2, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 55
    new-instance v2, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;

    const/4 v3, 0x0

    invoke-direct {v2, p4, p3, v3}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;-><init>(Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;Ljava/lang/String;Z)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 59
    new-instance v2, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;

    const/4 v3, 0x1

    invoke-direct {v2, p4, p3, v3}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;-><init>(Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;Ljava/lang/String;Z)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    return-void
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 166
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v7, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    .line 167
    .local v4, "match":I
    const/4 v7, -0x1

    if-eq v4, v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mMappings:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;

    move-object v3, v7

    .line 168
    .local v3, "mapping":Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;
    :goto_0
    if-eqz v3, :cond_0

    iget-boolean v7, v3, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;->hasId:Z

    if-eqz v7, :cond_2

    .line 169
    :cond_0
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invalid URI: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 167
    .end local v3    # "mapping":Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 173
    .restart local v3    # "mapping":Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;
    :cond_2
    iget-object v7, v3, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;->table:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v6

    .line 174
    .local v6, "tableName":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 175
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v5, 0x0

    .line 177
    .local v5, "numInserted":I
    :try_start_0
    array-length v2, p2

    .line 178
    .local v2, "length":I
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 179
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-eq v1, v2, :cond_3

    .line 180
    const-string v7, "_id"

    aget-object v8, p2, v1

    invoke-virtual {v0, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 179
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 182
    :cond_3
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    move v5, v2

    .line 185
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 186
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 188
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->notifyChange(Landroid/net/Uri;)V

    .line 189
    return v5

    .line 185
    .end local v1    # "i":I
    .end local v2    # "length":I
    :catchall_0
    move-exception v7

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 186
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v7
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 218
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 219
    .local v3, "match":I
    const/4 v5, -0x1

    if-ne v3, v5, :cond_0

    .line 220
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid URI: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 224
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mMappings:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;

    .line 225
    .local v2, "mapping":Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;
    iget-boolean v5, v2, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;->hasId:Z

    if-eqz v5, :cond_1

    .line 226
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->whereWithId(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 230
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 231
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v5, v2, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;->table:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v4

    .line 232
    .local v4, "tableName":Ljava/lang/String;
    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 233
    .local v0, "count":I
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->notifyChange(Landroid/net/Uri;)V

    .line 234
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 235
    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 74
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 75
    .local v1, "match":I
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    .line 76
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid URI: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 80
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mMappings:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;

    .line 81
    .local v0, "mapping":Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;
    iget-boolean v3, v0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;->hasId:Z

    if-eqz v3, :cond_1

    const-string/jumbo v2, "vnd.android.cursor.item"

    .line 82
    .local v2, "prefix":Ljava/lang/String;
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;->mimeSubtype:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 81
    .end local v2    # "prefix":Ljava/lang/String;
    :cond_1
    const-string/jumbo v2, "vnd.android.cursor.dir"

    goto :goto_0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 137
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 138
    .local v2, "match":I
    const/4 v6, -0x1

    if-eq v2, v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mMappings:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;

    move-object v1, v6

    .line 139
    .local v1, "mapping":Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;
    :goto_0
    if-eqz v1, :cond_0

    iget-boolean v6, v1, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;->hasId:Z

    if-eqz v6, :cond_2

    .line 140
    :cond_0
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Invalid URI: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 138
    .end local v1    # "mapping":Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 146
    .restart local v1    # "mapping":Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;
    :cond_2
    iget-object v6, v1, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;->table:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v3

    .line 147
    .local v3, "tableName":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 149
    .local v4, "rowId":J
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const-string v7, "_id"

    invoke-virtual {v6, v3, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    .line 155
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-lez v6, :cond_3

    .line 156
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->notifyChange(Landroid/net/Uri;)V

    .line 157
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    return-object v6

    .line 150
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Landroid/database/SQLException;
    throw v0

    .line 159
    .end local v0    # "e":Landroid/database/SQLException;
    :cond_3
    new-instance v6, Landroid/database/SQLException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to insert row at: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 17
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 88
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v15

    .line 89
    .local v15, "match":I
    const/4 v3, -0x1

    if-ne v15, v3, :cond_0

    .line 90
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid URI: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 94
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mMappings:Ljava/util/ArrayList;

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;

    .line 95
    .local v14, "mapping":Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;
    iget-boolean v3, v14, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;->hasId:Z

    if-eqz v3, :cond_1

    .line 96
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->whereWithId(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 102
    :cond_1
    iget-object v3, v14, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;->table:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v4

    .line 103
    .local v4, "tableName":Ljava/lang/String;
    const-string v3, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 105
    .local v11, "limit":Ljava/lang/String;
    if-nez p3, :cond_2

    sget-object v3, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 108
    const-string/jumbo v3, "type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 109
    .local v16, "type":Ljava/lang/String;
    const-string v3, "image"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 110
    sget-object p3, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->ALBUM_TYPE_WHERE:Ljava/lang/String;

    .line 111
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 p4, v0

    .end local p4    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    const-string v5, "image/%"

    aput-object v5, p4, v3

    .line 122
    .end local v16    # "type":Ljava/lang/String;
    .restart local p4    # "selectionArgs":[Ljava/lang/String;
    :cond_2
    :goto_0
    const/4 v12, 0x0

    .line 124
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v10, p5

    invoke-virtual/range {v3 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 127
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v12, v3, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_1
    return-object v12

    .line 114
    .end local v12    # "cursor":Landroid/database/Cursor;
    .restart local v16    # "type":Ljava/lang/String;
    :cond_3
    const-string/jumbo v3, "video"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 115
    sget-object p3, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->ALBUM_TYPE_WHERE:Ljava/lang/String;

    .line 116
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 p4, v0

    .end local p4    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    const-string/jumbo v5, "video/%"

    aput-object v5, p4, v3

    .restart local p4    # "selectionArgs":[Ljava/lang/String;
    goto :goto_0

    .line 128
    .end local v16    # "type":Ljava/lang/String;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v13

    .line 129
    .local v13, "e":Ljava/lang/Exception;
    const-string v3, "PicasaContentProvider"

    const-string v5, "Can not return query"

    invoke-static {v3, v5, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setDatabase(Landroid/database/sqlite/SQLiteOpenHelper;)V
    .locals 0
    .param p1, "database"    # Landroid/database/sqlite/SQLiteOpenHelper;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 48
    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 195
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 196
    .local v3, "match":I
    const/4 v5, -0x1

    if-ne v3, v5, :cond_0

    .line 197
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid URI: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 201
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mMappings:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;

    .line 202
    .local v2, "mapping":Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;
    iget-boolean v5, v2, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;->hasId:Z

    if-eqz v5, :cond_1

    .line 203
    invoke-direct {p0, p1, p3}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->whereWithId(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 207
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 208
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v5, v2, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider$Mapping;->table:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v4

    .line 209
    .local v4, "tableName":Ljava/lang/String;
    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 210
    .local v0, "count":I
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/TableContentProvider;->notifyChange(Landroid/net/Uri;)V

    .line 211
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 212
    return v0
.end method
