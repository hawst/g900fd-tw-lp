.class public Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;
.super Lcom/sec/android/gallery3d/data/ImageCacheRequest;
.source "LocalVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/LocalVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocalVideoRequest"
.end annotation


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mIsGolf:Z

.field private mLocalFilePath:Ljava/lang/String;

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mType:I


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;)V
    .locals 9
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "timeModified"    # J
    .param p5, "type"    # I
    .param p6, "localFilePath"    # Ljava/lang/String;

    .prologue
    .line 219
    invoke-static {p5}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/data/ImageCacheRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JII)V

    .line 220
    iput-object p6, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mLocalFilePath:Ljava/lang/String;

    .line 221
    iput p5, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mType:I

    .line 222
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;ZLcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 11
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "timeModified"    # J
    .param p5, "type"    # I
    .param p6, "localFilePath"    # Ljava/lang/String;
    .param p7, "isGolf"    # Z
    .param p8, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 227
    invoke-static/range {p5 .. p5}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v9

    const/4 v10, 0x1

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-wide v6, p3

    move/from16 v8, p5

    invoke-direct/range {v3 .. v10}, Lcom/sec/android/gallery3d/data/ImageCacheRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JIIZ)V

    .line 228
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mLocalFilePath:Ljava/lang/String;

    .line 229
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 230
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mIsGolf:Z

    .line 231
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 232
    return-void
.end method

.method private getVideoOrignalID(Ljava/lang/String;)J
    .locals 10
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 308
    const-wide/16 v8, -0x1

    .line 309
    .local v8, "id":J
    new-array v2, v4, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v5

    .line 310
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "_data=?"

    .line 311
    .local v3, "where":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 312
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 313
    .local v6, "c":Landroid/database/Cursor;
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 315
    .local v1, "videoUri":Landroid/net/Uri;
    const/4 v4, 0x1

    :try_start_0
    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 316
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 317
    const-string v4, "_id"

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 323
    :cond_0
    if-eqz v6, :cond_1

    .line 324
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 326
    :cond_1
    const/4 v6, 0x0

    .line 328
    :goto_0
    return-wide v8

    .line 320
    :catch_0
    move-exception v7

    .line 321
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 323
    if-eqz v6, :cond_2

    .line 324
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 326
    :cond_2
    const/4 v6, 0x0

    .line 327
    goto :goto_0

    .line 323
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v6, :cond_3

    .line 324
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 326
    :cond_3
    const/4 v6, 0x0

    throw v4
.end method


# virtual methods
.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mLocalFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public isPicture()Z
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x0

    return v0
.end method

.method public onDecodeOriginal(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;I)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 238
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mIsGolf:Z

    if-eqz v2, :cond_2

    .line 239
    new-instance v2, Lcom/sec/android/gallery3d/golf/GolfMgr;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/golf/GolfMgr;-><init>()V

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/golf/GolfMgr;->CreateThumbnails(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 240
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-object v1

    :cond_1
    move-object v1, v0

    .line 242
    goto :goto_0

    .line 246
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->createVideoThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 255
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v1, v0

    .line 258
    goto :goto_0
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const v4, 0x7f0200a8

    const/4 v3, 0x1

    .line 265
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isDrm(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 266
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isValidRights(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 267
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0203e6

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 290
    :cond_0
    :goto_0
    return-object v0

    .line 270
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->setBroken(Z)V

    .line 271
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mType:I

    if-ne v1, v3, :cond_2

    .line 272
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 275
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenMovieThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 280
    :cond_3
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 282
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 283
    const/4 v0, 0x0

    goto :goto_0

    .line 285
    :cond_4
    if-nez v0, :cond_0

    .line 288
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->setBroken(Z)V

    .line 290
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 208
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
