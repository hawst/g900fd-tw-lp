.class public Lcom/sec/android/gallery3d/provider/SearchParser;
.super Ljava/lang/Object;
.source "SearchParser.java"


# static fields
.field public static final SEARCH_LIMIT:I = 0x3e8


# instance fields
.field mEndId:Ljava/lang/String;

.field mEtime:J

.field mIsAllTagNull:Z

.field mKeyString:Ljava/lang/String;

.field mKeyValue:J

.field mLimit:I

.field mLoation:Ljava/lang/String;

.field mLocationValue:Ljava/lang/String;

.field mPeople:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mPeopleName:Ljava/lang/String;

.field mSearchFilter:Ljava/lang/String;

.field mStartId:Ljava/lang/String;

.field mStime:J

.field mUserDef:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mWeather:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mIsAllTagNull:Z

    .line 29
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mKeyValue:J

    .line 30
    iput v2, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mWeather:I

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mPeople:Ljava/util/ArrayList;

    .line 32
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mLimit:I

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mUserDef:Ljava/util/ArrayList;

    .line 34
    return-void
.end method


# virtual methods
.method public allTagCheck()Z
    .locals 1

    .prologue
    .line 165
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mIsAllTagNull:Z

    return v0
.end method

.method public getEndId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mEndId:Ljava/lang/String;

    return-object v0
.end method

.method public getEndTime()J
    .locals 2

    .prologue
    .line 149
    iget-wide v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mEtime:J

    return-wide v0
.end method

.method public getKeyString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mKeyString:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyValue()J
    .locals 2

    .prologue
    .line 117
    iget-wide v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mKeyValue:J

    return-wide v0
.end method

.method public getLimit()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mLimit:I

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mLoation:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mLocationValue:Ljava/lang/String;

    return-object v0
.end method

.method public getPeople()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mPeople:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPeopleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mPeopleName:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchFilter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mSearchFilter:Ljava/lang/String;

    return-object v0
.end method

.method public getStartId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mStartId:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mStime:J

    return-wide v0
.end method

.method public getUserDef()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mUserDef:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getWeather()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/sec/android/gallery3d/provider/SearchParser;->mWeather:I

    return v0
.end method

.method public parse(Landroid/net/Uri;)V
    .locals 22
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 38
    new-instance v12, Lcom/sec/android/gallery3d/provider/QueryParser;

    invoke-direct {v12}, Lcom/sec/android/gallery3d/provider/QueryParser;-><init>()V

    .line 39
    .local v12, "qParser":Lcom/sec/android/gallery3d/provider/QueryParser;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v13

    .line 40
    .local v13, "query":Ljava/lang/String;
    const-string v19, "limit"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 41
    .local v8, "limit":Ljava/lang/String;
    const-string v19, "location"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 42
    .local v9, "location":Ljava/lang/String;
    const-string/jumbo v19, "weather"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 43
    .local v17, "weather":Ljava/lang/String;
    const-string/jumbo v19, "stime"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 44
    .local v16, "stime":Ljava/lang/String;
    const-string v19, "etime"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 45
    .local v5, "etime":Ljava/lang/String;
    const-string v19, "people"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 46
    .local v10, "people":Ljava/lang/String;
    const-string/jumbo v19, "startid"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 47
    .local v15, "startId":Ljava/lang/String;
    const-string v19, "endid"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 48
    .local v4, "endId":Ljava/lang/String;
    const-string v19, "searchfilter"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 51
    .local v14, "searchFilter":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 52
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/provider/SearchParser;->mLimit:I

    .line 55
    :cond_0
    if-eqz v16, :cond_1

    .line 56
    invoke-static/range {v16 .. v16}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/gallery3d/provider/SearchParser;->mStime:J

    .line 59
    :cond_1
    if-eqz v5, :cond_2

    .line 60
    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/gallery3d/provider/SearchParser;->mEtime:J

    .line 63
    :cond_2
    if-eqz v15, :cond_3

    .line 64
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/gallery3d/provider/SearchParser;->mStartId:Ljava/lang/String;

    .line 67
    :cond_3
    if-eqz v4, :cond_4

    .line 68
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/gallery3d/provider/SearchParser;->mEndId:Ljava/lang/String;

    .line 71
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/provider/SearchParser;->mUserDef:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    const-string/jumbo v20, "userdef"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 73
    if-eqz v9, :cond_5

    .line 74
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/sec/android/gallery3d/provider/SearchParser;->mLocationValue:Ljava/lang/String;

    .line 77
    :cond_5
    if-eqz v10, :cond_6

    .line 78
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v11, v0, [Ljava/lang/String;

    .line 79
    .local v11, "peopleValue":[Ljava/lang/String;
    const-string v19, ","

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 80
    const/16 v19, 0x1

    aget-object v19, v11, v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/provider/SearchParser;->mPeopleName:Ljava/lang/String;

    .line 83
    .end local v11    # "peopleValue":[Ljava/lang/String;
    :cond_6
    if-eqz v17, :cond_7

    .line 84
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    .line 85
    .local v18, "weatherValue":[Ljava/lang/String;
    const-string v19, ","

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 86
    const/16 v19, 0x1

    aget-object v19, v18, v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/provider/SearchParser;->mWeather:I

    .line 89
    .end local v18    # "weatherValue":[Ljava/lang/String;
    :cond_7
    if-nez v17, :cond_8

    if-nez v10, :cond_8

    if-nez v9, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/provider/SearchParser;->mUserDef:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_8

    .line 90
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/provider/SearchParser;->mIsAllTagNull:Z

    .line 93
    :cond_8
    if-eqz v14, :cond_9

    .line 94
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/provider/SearchParser;->mSearchFilter:Ljava/lang/String;

    .line 97
    :cond_9
    invoke-virtual {v12, v13}, Lcom/sec/android/gallery3d/provider/QueryParser;->regexParser(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 98
    .local v7, "keywords":[Ljava/lang/String;
    if-eqz v7, :cond_b

    array-length v0, v7

    move/from16 v19, v0

    if-lez v19, :cond_b

    .line 99
    const-string v19, ""

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/provider/SearchParser;->mKeyString:Ljava/lang/String;

    .line 100
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v0, v7

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v6, v0, :cond_b

    .line 101
    aget-object v19, v7, v6

    const-string v20, "AND"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_a

    rem-int/lit8 v19, v6, 0x2

    if-eqz v19, :cond_a

    .line 102
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/provider/SearchParser;->mKeyString:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/provider/SearchParser;->mKeyString:Ljava/lang/String;

    .line 100
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 104
    :cond_a
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/provider/SearchParser;->mKeyString:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    aget-object v20, v7, v6

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/provider/SearchParser;->mKeyString:Ljava/lang/String;

    goto :goto_1

    .line 110
    .end local v6    # "i":I
    :cond_b
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 174
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 176
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/SearchParser;->getKeyString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 177
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/SearchParser;->getKeyString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/SearchParser;->getKeyValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/SearchParser;->getLocationValue()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 181
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Locatoin : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/SearchParser;->getLocationValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/SearchParser;->getPeople()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 185
    const-string v2, "People : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/SearchParser;->getPeople()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 187
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/SearchParser;->getPeople()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    :cond_2
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    .end local v0    # "i":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/SearchParser;->getWeather()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    .line 193
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Weather : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/SearchParser;->getWeather()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
