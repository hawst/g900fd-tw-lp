.class public Lcom/sec/android/gallery3d/data/ContentResolverDelegate;
.super Ljava/lang/Object;
.source "ContentResolverDelegate.java"


# static fields
.field private static final FILES:I = 0x2bc

.field private static final FILES_ID:I = 0x2bd

.field private static final IMAGES_MEDIA:I = 0x1

.field private static final IMAGES_MEDIA_ID:I = 0x2

.field private static final VIDEO_MEDIA:I = 0xc8

.field private static final VIDEO_MEDIA_ID:I = 0xc9

.field private static mMockMediaDB:Landroid/database/sqlite/SQLiteDatabase;

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 17
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->sUriMatcher:Landroid/content/UriMatcher;

    .line 27
    sget-object v0, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "media"

    const-string v2, "*/images/media"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 28
    sget-object v0, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "media"

    const-string v2, "*/images/media/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 30
    sget-object v0, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "media"

    const-string v2, "*/video/media"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 31
    sget-object v0, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "media"

    const-string v2, "*/video/media/#"

    const/16 v3, 0xc9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 33
    sget-object v0, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "media"

    const-string v2, "*/file"

    const/16 v3, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 34
    sget-object v0, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "media"

    const-string v2, "*/file/#"

    const/16 v3, 0x2bd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 35
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static delete(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 86
    sget-object v2, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->mMockMediaDB:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v2, :cond_0

    .line 87
    invoke-virtual {p0, p1, p2, p3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 92
    :goto_0
    return v0

    .line 89
    :cond_0
    const-string v1, "files"

    .line 90
    .local v1, "tableName":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->mMockMediaDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 91
    .local v0, "rowCount":I
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method private static getTableName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 99
    .local v0, "tableName":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 100
    .local v1, "type":I
    sparse-switch v1, :sswitch_data_0

    .line 114
    :goto_0
    return-object v0

    .line 103
    :sswitch_0
    const-string v0, "images"

    .line 104
    goto :goto_0

    .line 107
    :sswitch_1
    const-string/jumbo v0, "video"

    .line 108
    goto :goto_0

    .line 110
    :sswitch_2
    const-string v0, "files"

    goto :goto_0

    .line 100
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0xc8 -> :sswitch_1
        0xc9 -> :sswitch_1
        0x2bc -> :sswitch_2
    .end sparse-switch
.end method

.method public static query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->mMockMediaDB:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    .line 57
    const/4 v8, 0x0

    .line 59
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 80
    .end local v8    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-object v8

    .line 60
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 61
    .local v9, "e":Landroid/database/CursorWindowAllocationException;
    invoke-virtual {v9}, Landroid/database/CursorWindowAllocationException;->printStackTrace()V

    goto :goto_0

    .line 65
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v9    # "e":Landroid/database/CursorWindowAllocationException;
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v10

    .line 66
    .local v10, "type":I
    sparse-switch v10, :sswitch_data_0

    .line 80
    :goto_1
    sget-object v0, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->mMockMediaDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {p1}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->getTableName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    move-object v4, p4

    move-object/from16 v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto :goto_0

    .line 68
    :sswitch_0
    const-string p3, "_id=?"

    .line 69
    const/4 v0, 0x1

    new-array p4, v0, [Ljava/lang/String;

    .end local p4    # "selectionArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, p4, v1

    .line 72
    .restart local p4    # "selectionArgs":[Ljava/lang/String;
    goto :goto_1

    .line 74
    :sswitch_1
    const-string p3, "_id=?"

    .line 75
    const/4 v0, 0x1

    new-array p4, v0, [Ljava/lang/String;

    .end local p4    # "selectionArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, p4, v1

    .restart local p4    # "selectionArgs":[Ljava/lang/String;
    goto :goto_1

    .line 66
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0xc9 -> :sswitch_1
    .end sparse-switch
.end method

.method public static setMockDbInstance(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 38
    sput-object p0, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->mMockMediaDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 39
    return-void
.end method

.method public static update(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 43
    sget-object v2, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->mMockMediaDB:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v2, :cond_0

    .line 44
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 50
    :goto_0
    return v0

    .line 46
    :cond_0
    const-string v1, "files"

    .line 47
    .local v1, "tableName":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->mMockMediaDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 48
    .local v0, "returnValue":I
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method
