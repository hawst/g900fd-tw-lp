.class public Lcom/sec/android/gallery3d/data/EventAlbumManager;
.super Ljava/lang/Object;
.source "EventAlbumManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;,
        Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;,
        Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;
    }
.end annotation


# static fields
.field public static final COUNT_UINT:I = 0x64

.field private static final DAY_UINT:J = 0x5265c00L

.field public static final LOCK:Ljava/lang/Object;

.field private static final SMALL_SET_SIZE:I = 0xa

.field private static final SMALL_SET_SIZE_FOR_TIME:I = 0x14

.field private static final TAG:Ljava/lang/String; = "LocalSuggestAlbumData"

.field private static final WEEK_UINT:J = 0x7L

.field private static final ZOOM_LEVEL:[D

.field private static sInstance:Lcom/sec/android/gallery3d/data/EventAlbumManager;


# instance fields
.field private final mAlbumComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private mCurLocality:Ljava/lang/String;

.field private mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mEventFileObserver:Landroid/os/FileObserver;

.field mItemSets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

.field private mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

.field private mProgressDialog:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->LOCK:Ljava/lang/Object;

    .line 75
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->sInstance:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    .line 101
    const/16 v0, 0x16

    new-array v0, v0, [D

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->ZOOM_LEVEL:[D

    return-void

    :array_0
    .array-data 8
        0x406ea00000000000L    # 245.0
        0x406c200000000000L    # 225.0
        0x406a400000000000L    # 210.0
        0x4067c00000000000L    # 190.0
        0x4066800000000000L    # 180.0
        0x4064c00000000000L    # 166.0
        0x4062c00000000000L    # 150.0
        0x405f400000000000L    # 125.0
        0x405c400000000000L    # 113.0
        0x4050400000000000L    # 65.0
        0x4041000000000000L    # 34.0
        0x4031000000000000L    # 17.0
        0x4022000000000000L    # 9.0
        0x4014000000000000L    # 5.0
        0x4000000000000000L    # 2.0
        0x3ff4cccccccccccdL    # 1.3
        0x3fe6666666666666L    # 0.7
        0x3fe0000000000000L    # 0.5
        0x3fb999999999999aL    # 0.1
        0x3fb47ae147ae147bL    # 0.08
        0x3fa47ae147ae147bL    # 0.04
        0x3f947ae147ae147bL    # 0.02
    .end array-data
.end method

.method private constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mCurLocality:Ljava/lang/String;

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mEventFileObserver:Landroid/os/FileObserver;

    .line 87
    new-instance v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/EventAlbumManager$1;-><init>(Lcom/sec/android/gallery3d/data/EventAlbumManager;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mComparator:Ljava/util/Comparator;

    .line 94
    new-instance v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/EventAlbumManager$2;-><init>(Lcom/sec/android/gallery3d/data/EventAlbumManager;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mAlbumComparator:Ljava/util/Comparator;

    .line 107
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 108
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 109
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 111
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->CAMERA_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    new-instance v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$3;

    sget-object v1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->CAMERA_PATH:Ljava/lang/String;

    const/16 v2, 0x240

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/gallery3d/data/EventAlbumManager$3;-><init>(Lcom/sec/android/gallery3d/data/EventAlbumManager;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mEventFileObserver:Landroid/os/FileObserver;

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mEventFileObserver:Landroid/os/FileObserver;

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mEventFileObserver:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->startWatching()V

    .line 125
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/EventAlbumManager;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/EventAlbumManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/data/EventAlbumManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/EventAlbumManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mCurLocality:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/data/EventAlbumManager;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/EventAlbumManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/data/EventAlbumManager;Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/EventAlbumManager;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getSelectedItemList(Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/data/EventAlbumManager;Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/EventAlbumManager;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/util/List;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->createNewEventAlbumName(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/data/EventAlbumManager;Ljava/util/ArrayList;ILjava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/EventAlbumManager;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->isDuplicated(Ljava/util/ArrayList;ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/data/EventAlbumManager;Landroid/content/Context;Ljava/util/ArrayList;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/EventAlbumManager;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/util/ArrayList;
    .param p3, "x3"    # I
    .param p4, "x4"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->showCreateEventDialog(Landroid/content/Context;Ljava/util/ArrayList;ILjava/lang/String;)V

    return-void
.end method

.method private createNewEventAlbumName(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 375
    .local p2, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const-string v0, ""

    .line 376
    .local v0, "locality":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 377
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getLocality(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 380
    :cond_0
    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    if-nez v0, :cond_2

    .line 381
    :cond_1
    const v1, 0x7f0e0069

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 383
    :cond_2
    return-object v0
.end method

.method private deleteFromSuggestionInfo(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/16 v5, 0x64

    .line 550
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 551
    .local v2, "itemUnit":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 552
    .local v0, "deleteWhere":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_6

    .line 553
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v5, :cond_2

    move v3, v5

    .line 554
    .local v3, "size":I
    :goto_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v3, :cond_4

    .line 555
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 556
    const-string v4, " OR "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    :cond_0
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v4, :cond_3

    .line 559
    const-string v4, "_id"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/LocalImage;

    iget v4, v4, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 563
    :cond_1
    :goto_3
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 554
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 553
    .end local v1    # "i":I
    .end local v3    # "size":I
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto :goto_1

    .line 560
    .restart local v1    # "i":I
    .restart local v3    # "size":I
    :cond_3
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v4, :cond_1

    .line 561
    const-string v4, "_id"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/LocalVideo;

    iget v4, v4, Lcom/sec/android/gallery3d/data/LocalVideo;->id:I

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 565
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_5

    .line 566
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v6, "suggest_event_info"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v6, v7, v8}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 568
    :cond_5
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 569
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 570
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .line 572
    .end local v1    # "i":I
    .end local v3    # "size":I
    :cond_6
    return-void
.end method

.method private deleteIfNotExist(ZLjava/lang/String;Ljava/util/ArrayList;Ljava/lang/StringBuilder;)V
    .locals 12
    .param p1, "isImage"    # Z
    .param p2, "table"    # Ljava/lang/String;
    .param p4, "where"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/StringBuilder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 986
    .local p3, "idsLocalDB":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz p1, :cond_1

    .line 987
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 988
    .local v1, "uri":Landroid/net/Uri;
    const-string v11, "_id"

    .line 993
    .local v11, "idEntry":Ljava/lang/String;
    :goto_0
    const/4 v6, 0x0

    .line 995
    .local v6, "cursorMediaDB":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v11, v2, v3

    invoke-virtual/range {p4 .. p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 998
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 999
    .local v7, "deleteWhere":Ljava/lang/StringBuilder;
    if-eqz v6, :cond_2

    .line 1000
    :cond_0
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1001
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 1002
    .local v10, "id":I
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1003
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1016
    .end local v7    # "deleteWhere":Ljava/lang/StringBuilder;
    .end local v10    # "id":I
    :catch_0
    move-exception v8

    .line 1017
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1021
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1023
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_2
    return-void

    .line 990
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "cursorMediaDB":Landroid/database/Cursor;
    .end local v11    # "idEntry":Ljava/lang/String;
    :cond_1
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 991
    .restart local v1    # "uri":Landroid/net/Uri;
    const-string v11, "_id"

    .restart local v11    # "idEntry":Ljava/lang/String;
    goto :goto_0

    .line 1007
    .restart local v6    # "cursorMediaDB":Landroid/database/Cursor;
    .restart local v7    # "deleteWhere":Ljava/lang/StringBuilder;
    :cond_2
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_3
    :try_start_2
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v9, v0, :cond_4

    .line 1008
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 1009
    const-string v0, " OR "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1011
    :cond_3
    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1007
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 1013
    :cond_4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 1014
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p2, v2, v3}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1021
    :cond_5
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_2

    .line 1018
    .end local v7    # "deleteWhere":Ljava/lang/StringBuilder;
    .end local v9    # "i":I
    :catch_1
    move-exception v8

    .line 1019
    .local v8, "e":Landroid/database/CursorWindowAllocationException;
    :try_start_3
    invoke-virtual {v8}, Landroid/database/CursorWindowAllocationException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1021
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_2

    .end local v8    # "e":Landroid/database/CursorWindowAllocationException;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getCalendarEvent(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 27
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1242
    const-string v6, "content://com.android.calendar/events"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 1243
    .local v7, "uri":Landroid/net/Uri;
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 1244
    .local v22, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 1246
    .local v12, "cTime":J
    const/4 v15, 0x0

    .line 1248
    .local v15, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "dtstart > "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getExpiredTime()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " and "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "dtstart"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " < "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1251
    .local v9, "selection":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string/jumbo v11, "title"

    aput-object v11, v8, v10

    const/4 v10, 0x1

    const-string v11, "eventLocation"

    aput-object v11, v8, v10

    const/4 v10, 0x2

    const-string v11, "dtstart"

    aput-object v11, v8, v10

    const/4 v10, 0x3

    const-string v11, "dtend"

    aput-object v11, v8, v10

    const/4 v10, 0x0

    const-string v11, "dtstart"

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 1259
    if-eqz v15, :cond_2

    .line 1260
    :cond_0
    :goto_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1261
    new-instance v19, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;-><init>()V

    .line 1262
    .local v19, "item":Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;
    const/4 v6, 0x0

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v19

    iput-object v6, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->title:Ljava/lang/String;

    .line 1263
    const/4 v6, 0x1

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v19

    iput-object v6, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->location:Ljava/lang/String;

    .line 1264
    const/4 v6, 0x2

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    move-object/from16 v0, v19

    iput-wide v10, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->dtstart:J

    .line 1265
    const/4 v6, 0x3

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    move-object/from16 v0, v19

    iput-wide v10, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->dtend:J

    .line 1267
    move-object/from16 v0, v19

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->location:Ljava/lang/String;

    if-eqz v6, :cond_0

    const-string v6, ""

    move-object/from16 v0, v19

    iget-object v8, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->location:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1269
    const-string v6, ""

    move-object/from16 v0, v19

    iget-object v8, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->title:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1270
    const v6, 0x7f0e0461

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v19

    iput-object v6, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->title:Ljava/lang/String;

    .line 1271
    :cond_1
    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1274
    .end local v9    # "selection":Ljava/lang/String;
    .end local v19    # "item":Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;
    :catch_0
    move-exception v16

    .line 1275
    .local v16, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1277
    invoke-static {v15}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1280
    .end local v16    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_1
    new-instance v26, Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;-><init>(Landroid/content/Context;)V

    .line 1281
    .local v26, "reverseGeocoder":Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    new-instance v17, Landroid/location/Geocoder;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 1282
    .local v17, "geocoder":Landroid/location/Geocoder;
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_2
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v6

    move/from16 v0, v18

    if-ge v0, v6, :cond_4

    .line 1284
    :try_start_2
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;

    iget-object v6, v6, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->location:Ljava/lang/String;

    const/4 v8, 0x2

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v8}, Landroid/location/Geocoder;->getFromLocationName(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v23

    .line 1285
    .local v23, "listAddress":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v23, :cond_3

    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v6

    const/4 v8, 0x1

    if-ne v6, v8, :cond_3

    .line 1286
    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/Address;

    invoke-virtual {v6}, Landroid/location/Address;->getLatitude()D

    move-result-wide v20

    .line 1287
    .local v20, "lat":D
    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/Address;

    invoke-virtual {v6}, Landroid/location/Address;->getLongitude()D

    move-result-wide v24

    .line 1288
    .local v24, "lon":D
    move-object/from16 v0, v26

    move-wide/from16 v1, v20

    move-wide/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->getLocality(DD)Ljava/lang/String;

    move-result-object v14

    .line 1289
    .local v14, "convertedLocation":Ljava/lang/String;
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;

    iput-object v14, v6, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->location:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1282
    .end local v14    # "convertedLocation":Ljava/lang/String;
    .end local v20    # "lat":D
    .end local v23    # "listAddress":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v24    # "lon":D
    :goto_3
    add-int/lit8 v18, v18, 0x1

    goto :goto_2

    .line 1277
    .end local v17    # "geocoder":Landroid/location/Geocoder;
    .end local v18    # "i":I
    .end local v26    # "reverseGeocoder":Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    .restart local v9    # "selection":Ljava/lang/String;
    :cond_2
    invoke-static {v15}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v9    # "selection":Ljava/lang/String;
    :catchall_0
    move-exception v6

    invoke-static {v15}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v6

    .line 1291
    .restart local v17    # "geocoder":Landroid/location/Geocoder;
    .restart local v18    # "i":I
    .restart local v23    # "listAddress":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .restart local v26    # "reverseGeocoder":Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    :cond_3
    :try_start_3
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1292
    add-int/lit8 v18, v18, -0x1

    goto :goto_3

    .line 1294
    .end local v23    # "listAddress":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :catch_1
    move-exception v16

    .line 1295
    .local v16, "e":Ljava/io/IOException;
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1298
    .end local v16    # "e":Ljava/io/IOException;
    :cond_4
    return-object v22
.end method

.method private getDate(Ljava/lang/Long;)Ljava/lang/String;
    .locals 4
    .param p1, "timeMillis"    # Ljava/lang/Long;

    .prologue
    .line 1302
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const v1, 0x10014

    invoke-static {v0, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDistance(Lcom/sec/android/gallery3d/data/MediaSet;)[D
    .locals 18
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 747
    const-wide v10, 0x4056800000000000L    # 90.0

    .line 748
    .local v10, "minLatitude":D
    const-wide v6, -0x3fa9800000000000L    # -90.0

    .line 749
    .local v6, "maxLatitude":D
    const-wide v12, 0x4066800000000000L    # 180.0

    .line 750
    .local v12, "minLongitude":D
    const-wide v8, -0x3f99800000000000L    # -180.0

    .line 751
    .local v8, "maxLongitude":D
    const/4 v14, 0x2

    new-array v2, v14, [D

    .line 752
    .local v2, "distance":[D
    const/4 v14, 0x2

    new-array v4, v14, [D

    .line 753
    .local v4, "itemLocation":[D
    const/4 v14, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v5

    .line 755
    .local v5, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v14

    if-ge v3, v14, :cond_4

    .line 756
    const/4 v15, 0x0

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v16

    aput-wide v16, v4, v15

    .line 757
    const/4 v15, 0x1

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v16

    aput-wide v16, v4, v15

    .line 759
    const/4 v14, 0x0

    aget-wide v14, v4, v14

    const/16 v16, 0x1

    aget-wide v16, v4, v16

    invoke-static/range {v14 .. v17}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 760
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v14

    cmpl-double v14, v10, v14

    if-lez v14, :cond_0

    .line 761
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v10

    .line 762
    :cond_0
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v14

    cmpg-double v14, v6, v14

    if-gez v14, :cond_1

    .line 763
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v6

    .line 764
    :cond_1
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v14

    cmpl-double v14, v12, v14

    if-lez v14, :cond_2

    .line 765
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v12

    .line 766
    :cond_2
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v14

    cmpg-double v14, v8, v14

    if-gez v14, :cond_3

    .line 767
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v8

    .line 755
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 770
    :cond_4
    const/4 v14, 0x0

    sub-double v16, v6, v10

    aput-wide v16, v2, v14

    .line 771
    const/4 v14, 0x1

    sub-double v16, v8, v12

    aput-wide v16, v2, v14

    .line 773
    return-object v2
.end method

.method private getImageContentValues(Ljava/util/ArrayList;ILjava/lang/String;ZJ)[Landroid/content/ContentValues;
    .locals 9
    .param p2, "bucketId"    # I
    .param p3, "bucketName"    # Ljava/lang/String;
    .param p4, "isSuggestion"    # Z
    .param p5, "albumCreateTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;I",
            "Ljava/lang/String;",
            "ZJ)[",
            "Landroid/content/ContentValues;"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v5, 0x1

    .line 791
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v3, v4, [Landroid/content/ContentValues;

    .line 792
    .local v3, "values":[Landroid/content/ContentValues;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 793
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v4, :cond_0

    .line 794
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 795
    .local v1, "image":Lcom/sec/android/gallery3d/data/LocalImage;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->getItemId()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 796
    .local v2, "pkey":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->getContentValues()Landroid/content/ContentValues;

    move-result-object v4

    aput-object v4, v3, v0

    .line 797
    aget-object v4, v3, v0

    const-string v6, "pkey"

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 798
    aget-object v4, v3, v0

    const-string v6, "bucket_id"

    invoke-virtual {v4, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 799
    aget-object v4, v3, v0

    const-string v6, "bucket_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 800
    aget-object v4, v3, v0

    const-string v6, "bucket_display_name"

    invoke-virtual {v4, v6, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    aget-object v6, v3, v0

    const-string/jumbo v7, "suggestion"

    if-ne p4, v5, :cond_1

    move v4, v5

    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 802
    aget-object v4, v3, v0

    const-string v6, "album_create_time"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 792
    .end local v1    # "image":Lcom/sec/android/gallery3d/data/LocalImage;
    .end local v2    # "pkey":Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 801
    .restart local v1    # "image":Lcom/sec/android/gallery3d/data/LocalImage;
    .restart local v2    # "pkey":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 805
    .end local v1    # "image":Lcom/sec/android/gallery3d/data/LocalImage;
    .end local v2    # "pkey":Ljava/lang/String;
    :cond_2
    return-object v3
.end method

.method public static getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;
    .locals 2
    .param p0, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 128
    sget-object v1, Lcom/sec/android/gallery3d/data/EventAlbumManager;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 129
    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->sInstance:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    .line 130
    new-instance v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    sput-object v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->sInstance:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    .line 132
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->sInstance:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    monitor-exit v1

    return-object v0

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getLocality(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 672
    .local p2, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;

    invoke-direct {v6}, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;-><init>()V

    .line 674
    .local v6, "set":Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    if-ge v0, v7, :cond_4

    .line 675
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    instance-of v7, v7, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v7, :cond_3

    .line 676
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 677
    .local v1, "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    iget-wide v8, v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;->latitude:D

    iget-wide v10, v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;->longitude:D

    invoke-static {v8, v9, v10, v11}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 679
    iget-wide v2, v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;->latitude:D

    .line 680
    .local v2, "itemLatitude":D
    iget-wide v4, v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;->longitude:D

    .line 682
    .local v4, "itemLongitude":D
    iget-wide v8, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    cmpl-double v7, v8, v2

    if-lez v7, :cond_0

    .line 683
    iput-wide v2, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    .line 684
    iput-wide v4, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLongitude:D

    .line 686
    :cond_0
    iget-wide v8, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    cmpg-double v7, v8, v2

    if-gez v7, :cond_1

    .line 687
    iput-wide v2, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    .line 688
    iput-wide v4, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLongitude:D

    .line 690
    :cond_1
    iget-wide v8, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    cmpl-double v7, v8, v4

    if-lez v7, :cond_2

    .line 691
    iput-wide v2, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLatitude:D

    .line 692
    iput-wide v4, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    .line 694
    :cond_2
    iget-wide v8, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    cmpg-double v7, v8, v4

    if-gez v7, :cond_3

    .line 695
    iput-wide v2, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLatitude:D

    .line 696
    iput-wide v4, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    .line 674
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    .end local v2    # "itemLatitude":D
    .end local v4    # "itemLongitude":D
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 701
    :cond_4
    new-instance v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    invoke-direct {v7, p1}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v6}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->computeAddressOverLocality(Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;)Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method private getSelectedItemList(Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/util/ArrayList;
    .locals 6
    .param p1, "selectionModeProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/ui/SelectionManager;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v2

    .line 209
    .local v2, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 210
    .local v1, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 211
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v4, :cond_1

    .line 212
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 213
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v4, 0x0

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 210
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 214
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v4, :cond_0

    .line 215
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 218
    :cond_2
    return-object v1
.end method

.method private getTodayTime()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1311
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1312
    .local v0, "today":Ljava/util/Calendar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 1313
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method private getVideoContentValues(Ljava/util/ArrayList;ILjava/lang/String;ZJ)[Landroid/content/ContentValues;
    .locals 9
    .param p2, "bucketId"    # I
    .param p3, "bucketName"    # Ljava/lang/String;
    .param p4, "isSuggestion"    # Z
    .param p5, "albumCreateTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;I",
            "Ljava/lang/String;",
            "ZJ)[",
            "Landroid/content/ContentValues;"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v5, 0x1

    .line 809
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v2, v4, [Landroid/content/ContentValues;

    .line 810
    .local v2, "values":[Landroid/content/ContentValues;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 811
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v4, :cond_0

    .line 812
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/LocalVideo;

    .line 813
    .local v3, "video":Lcom/sec/android/gallery3d/data/LocalVideo;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalVideo;->getItemId()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 814
    .local v1, "pkey":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalVideo;->getContentValues()Landroid/content/ContentValues;

    move-result-object v4

    aput-object v4, v2, v0

    .line 815
    aget-object v4, v2, v0

    const-string v6, "pkey"

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 816
    aget-object v4, v2, v0

    const-string v6, "bucket_id"

    invoke-virtual {v4, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 817
    aget-object v4, v2, v0

    const-string v6, "bucket_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 818
    aget-object v4, v2, v0

    const-string v6, "bucket_display_name"

    invoke-virtual {v4, v6, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    aget-object v6, v2, v0

    const-string/jumbo v7, "suggestion"

    if-ne p4, v5, :cond_1

    move v4, v5

    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 820
    aget-object v4, v2, v0

    const-string v6, "album_create_time"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 810
    .end local v1    # "pkey":Ljava/lang/String;
    .end local v3    # "video":Lcom/sec/android/gallery3d/data/LocalVideo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 819
    .restart local v1    # "pkey":Ljava/lang/String;
    .restart local v3    # "video":Lcom/sec/android/gallery3d/data/LocalVideo;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 823
    .end local v1    # "pkey":Ljava/lang/String;
    .end local v3    # "video":Lcom/sec/android/gallery3d/data/LocalVideo;
    :cond_2
    return-object v2
.end method

.method private isDuplicated(Ljava/util/ArrayList;ILjava/lang/String;)I
    .locals 3
    .param p2, "id"    # I
    .param p3, "newName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;",
            ">;I",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 607
    .local p1, "albumMap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;>;"
    const/4 v0, 0x0

    .line 608
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 609
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;->name:Ljava/lang/String;

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 610
    add-int/lit8 v0, v0, 0x1

    .line 608
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 612
    :cond_1
    return v0
.end method

.method private showCreateEventDialog(Landroid/content/Context;Ljava/util/ArrayList;ILjava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "bucketId"    # I
    .param p4, "newAlbumName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 271
    .local p2, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 272
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    const/4 v2, 0x1

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;-><init>(Landroid/content/Context;ZLjava/util/ArrayList;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    .line 273
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->showNewAlbumDialog()V

    .line 274
    return-void
.end method


# virtual methods
.method public addExistAlbum(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;ILjava/lang/String;J)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "selectionModeProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .param p3, "bucketId"    # I
    .param p4, "AlbumName"    # Ljava/lang/String;
    .param p5, "albumCreateTime"    # J

    .prologue
    .line 319
    const/4 v5, 0x0

    .line 320
    .local v5, "isSuggestion":Z
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getSelectedItemList(Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/util/ArrayList;

    move-result-object v2

    .line 322
    .local v2, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v0, 0x0

    .line 323
    .local v0, "count":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v9, "images_event_album"

    move-object v1, p0

    move v3, p3

    move-object v4, p4

    move-wide/from16 v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getImageContentValues(Ljava/util/ArrayList;ILjava/lang/String;ZJ)[Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v8, v9, v1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addEventInfo(Ljava/lang/String;[Landroid/content/ContentValues;)I

    move-result v1

    add-int/2addr v0, v1

    .line 325
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v9, "video_event_album"

    move-object v1, p0

    move v3, p3

    move-object v4, p4

    move-wide/from16 v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getVideoContentValues(Ljava/util/ArrayList;ILjava/lang/String;ZJ)[Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v8, v9, v1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addEventInfo(Ljava/lang/String;[Landroid/content/ContentValues;)I

    move-result v1

    add-int/2addr v0, v1

    .line 328
    if-lez v0, :cond_0

    .line 329
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->EVENT_ALBUM_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 330
    const v1, 0x7f0e01c6

    invoke-static {p1, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 334
    :goto_0
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 335
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v3, "EXIT_SELECTION_MODE"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 337
    return-void

    .line 332
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    const v1, 0x7f0e01c8

    invoke-static {p1, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public addSuggestEventInfo(Landroid/content/Context;Ljava/util/ArrayList;)I
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p2, "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v7, 0x1

    .line 827
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 828
    .local v2, "size":I
    new-instance v1, Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    invoke-direct {v1, p1}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;-><init>(Landroid/content/Context;)V

    .line 830
    .local v1, "reverseGeocoder":Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    new-array v3, v2, [Landroid/content/ContentValues;

    .line 831
    .local v3, "values":[Landroid/content/ContentValues;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 832
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->needStop()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 833
    const/4 v4, 0x0

    .line 847
    :goto_1
    return v4

    .line 835
    :cond_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    aput-object v4, v3, v0

    .line 836
    aget-object v5, v3, v0

    const-string v6, "_id"

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 837
    aget-object v5, v3, v0

    const-string v6, "latitude"

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 838
    aget-object v5, v3, v0

    const-string v6, "longitude"

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 839
    aget-object v5, v3, v0

    const-string v6, "_data"

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    aget-object v5, v3, v0

    const-string v6, "datetaken"

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 841
    aget-object v5, v3, v0

    const-string v6, "locality"

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v8

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v10

    invoke-virtual {v1, v8, v9, v10, v11}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->getLocality(DD)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v4, :cond_1

    .line 843
    aget-object v4, v3, v0

    const-string/jumbo v5, "type"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 831
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 845
    :cond_1
    aget-object v4, v3, v0

    const-string/jumbo v5, "type"

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    .line 847
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v5, "suggest_event_info"

    invoke-virtual {v4, v5, v3, v7}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addEventInfo(Ljava/lang/String;[Landroid/content/ContentValues;Z)I

    move-result v4

    goto/16 :goto_1
.end method

.method public createNewEventAlbum(Landroid/content/Context;Ljava/util/ArrayList;ILjava/lang/String;ZJ)Z
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "bucketId"    # I
    .param p4, "newAlbumName"    # Ljava/lang/String;
    .param p5, "isRename"    # Z
    .param p6, "albumCreateTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;I",
            "Ljava/lang/String;",
            "ZJ)Z"
        }
    .end annotation

    .prologue
    .line 278
    .local p2, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    move-object/from16 v5, p1

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v14

    .line 279
    .local v14, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    if-nez p5, :cond_0

    .line 280
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getTimeString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v13

    .line 281
    .local v13, "newAlbumTime":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/local/event/all/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result p3

    .line 283
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getEventAlbumMap()Ljava/util/ArrayList;

    move-result-object v4

    .line 284
    .local v4, "albumMap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;>;"
    move-object/from16 v0, p0

    move/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v0, v4, v1, v2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->isDuplicated(Ljava/util/ArrayList;ILjava/lang/String;)I

    move-result v5

    if-lez v5, :cond_0

    .line 285
    const v5, 0x7f0e0217

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 286
    const/4 v5, 0x0

    .line 314
    .end local v4    # "albumMap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;>;"
    .end local v13    # "newAlbumTime":Ljava/lang/String;
    .end local p1    # "context":Landroid/content/Context;
    :goto_0
    return v5

    .line 289
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    const-wide/16 v6, 0x0

    cmp-long v5, p6, v6

    if-nez v5, :cond_1

    .line 290
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p6

    .line 293
    :cond_1
    const/4 v9, 0x0

    .line 294
    .local v9, "isSuggestion":Z
    const/4 v12, 0x0

    .line 295
    .local v12, "count":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v16, "images_event_album"

    move-object/from16 v5, p0

    move-object/from16 v6, p2

    move/from16 v7, p3

    move-object/from16 v8, p4

    move-wide/from16 v10, p6

    invoke-direct/range {v5 .. v11}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getImageContentValues(Ljava/util/ArrayList;ILjava/lang/String;ZJ)[Landroid/content/ContentValues;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addEventInfo(Ljava/lang/String;[Landroid/content/ContentValues;)I

    move-result v5

    add-int/2addr v12, v5

    .line 297
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v16, "video_event_album"

    move-object/from16 v5, p0

    move-object/from16 v6, p2

    move/from16 v7, p3

    move-object/from16 v8, p4

    move-wide/from16 v10, p6

    invoke-direct/range {v5 .. v11}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getVideoContentValues(Ljava/util/ArrayList;ILjava/lang/String;ZJ)[Landroid/content/ContentValues;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addEventInfo(Ljava/lang/String;[Landroid/content/ContentValues;)I

    move-result v5

    add-int/2addr v12, v5

    .line 300
    if-lez v12, :cond_3

    .line 301
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->EVENT_ALBUM_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 302
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->deleteFromSuggestionInfo(Ljava/util/ArrayList;)V

    .line 303
    if-nez p5, :cond_2

    .line 304
    const v5, 0x7f0e0448

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 305
    :cond_2
    invoke-virtual {v14}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 306
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-static/range {p1 .. p1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "EXIT_SELECTION_MODE"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 308
    const/4 v5, 0x1

    goto :goto_0

    .line 310
    .restart local p1    # "context":Landroid/content/Context;
    :cond_3
    const v5, 0x7f0e01c8

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 311
    invoke-virtual {v14}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 312
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-static/range {p1 .. p1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "EXIT_SELECTION_MODE"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 314
    const/4 v5, 0x1

    goto/16 :goto_0
.end method

.method public deleteEvent(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Z)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "selectionModeProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .param p3, "exitSelectionMode"    # Z

    .prologue
    .line 474
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getSelectedItemList(Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/util/ArrayList;

    move-result-object v5

    .line 475
    .local v5, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 477
    .local v6, "itemUnit":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 478
    .local v3, "imageWhere":Ljava/lang/StringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .local v10, "videoWhere":Ljava/lang/StringBuilder;
    move-object/from16 v11, p1

    .line 480
    check-cast v11, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v11

    const-string v12, "MapView"

    invoke-virtual {v11, v12}, Lcom/sec/android/gallery3d/data/DataManager;->getLocalItemList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 483
    .local v7, "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v1, 0x0

    .line 484
    .local v1, "count":I
    :goto_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_a

    .line 485
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/16 v12, 0x64

    if-le v11, v12, :cond_3

    const/16 v9, 0x64

    .line 486
    .local v9, "size":I
    :goto_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v9, :cond_6

    .line 487
    if-eqz v7, :cond_0

    .line 488
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 489
    .local v4, "index":I
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 490
    .local v8, "mediaItemlistSize":I
    if-ltz v4, :cond_0

    if-le v8, v4, :cond_0

    .line 491
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 494
    .end local v4    # "index":I
    .end local v8    # "mediaItemlistSize":I
    :cond_0
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    instance-of v11, v11, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v11, :cond_4

    .line 495
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    if-lez v11, :cond_1

    .line 496
    const-string v11, " OR "

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 498
    :cond_1
    const-string v11, "("

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "_id"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/data/LocalImage;

    iget v11, v11, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " AND "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "bucket_id"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/data/LocalImage;

    iget v11, v11, Lcom/sec/android/gallery3d/data/LocalImage;->bucketId:I

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    :cond_2
    :goto_3
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 486
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 485
    .end local v2    # "i":I
    .end local v9    # "size":I
    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    goto :goto_1

    .line 503
    .restart local v2    # "i":I
    .restart local v9    # "size":I
    :cond_4
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    instance-of v11, v11, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v11, :cond_2

    .line 504
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    if-lez v11, :cond_5

    .line 505
    const-string v11, " OR "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 507
    :cond_5
    const-string v11, "("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "_id"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/data/LocalVideo;

    iget v11, v11, Lcom/sec/android/gallery3d/data/LocalVideo;->id:I

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " AND "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "bucket_id"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/data/LocalVideo;

    iget v11, v11, Lcom/sec/android/gallery3d/data/LocalVideo;->bucketId:I

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 515
    :cond_6
    if-eqz v7, :cond_7

    move-object/from16 v11, p1

    .line 516
    check-cast v11, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v11

    const-string v12, "MapView"

    invoke-virtual {v11, v12, v7}, Lcom/sec/android/gallery3d/data/DataManager;->setLocalItemList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 520
    :cond_7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    if-lez v11, :cond_8

    .line 521
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v12, "images_event_album"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v13, v14}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v11

    add-int/2addr v1, v11

    .line 522
    :cond_8
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    if-lez v11, :cond_9

    .line 523
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v12, "video_event_album"

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v13, v14}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v11

    add-int/2addr v1, v11

    .line 525
    :cond_9
    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 526
    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 527
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 528
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .line 532
    .end local v2    # "i":I
    .end local v9    # "size":I
    :cond_a
    if-eqz p3, :cond_c

    .line 533
    if-lez v1, :cond_b

    .line 534
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    sget-object v12, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->EVENT_ALBUM_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 536
    :cond_b
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-static/range {p1 .. p1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v11

    const-string v12, "EXIT_SELECTION_MODE"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 539
    :cond_c
    return-void
.end method

.method public deleteSDcardItems(Ljava/lang/String;)I
    .locals 9
    .param p1, "storage"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 542
    const/4 v0, 0x0

    .line 543
    .local v0, "count":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v2, "suggest_event_info"

    const-string v3, "_data LIKE ?"

    new-array v4, v8, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 544
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v2, "images_event_album"

    const-string v3, "_data LIKE ?"

    new-array v4, v8, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 545
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v2, "video_event_album"

    const-string v3, "_data LIKE ?"

    new-array v4, v8, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 546
    return v0
.end method

.method public dismissDialog()V
    .locals 1

    .prologue
    .line 1317
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    if-eqz v0, :cond_0

    .line 1318
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->dismissDialog()V

    .line 1320
    :cond_0
    return-void
.end method

.method public getBurstshotItems(Lcom/sec/android/gallery3d/data/MediaItem;)Ljava/util/ArrayList;
    .locals 14
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1323
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1325
    .local v10, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const-string v3, "bucket_id = ? AND group_id = ? "

    .line 1327
    .local v3, "WHERE":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_0

    move-object v11, p1

    .line 1328
    check-cast v11, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 1329
    .local v11, "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    const/4 v7, 0x0

    .line 1331
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v1, "images_event_album"

    sget-object v2, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget v12, v11, Lcom/sec/android/gallery3d/data/LocalImage;->bucketId:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    const/4 v5, 0x1

    iget-wide v12, v11, Lcom/sec/android/gallery3d/data/LocalImage;->groupId:J

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1337
    if-nez v7, :cond_1

    .line 1338
    const-string v0, "LocalSuggestAlbumData"

    const-string v1, "query fail: "

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1353
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1356
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v11    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    :cond_0
    :goto_0
    return-object v10

    .line 1341
    .restart local v7    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    :cond_1
    :goto_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1342
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 1343
    .local v8, "id":I
    sget-object v0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    iget v1, v11, Lcom/sec/android/gallery3d/data/LocalImage;->bucketId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 1344
    .local v6, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 1345
    .local v9, "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    if-nez v9, :cond_2

    .line 1346
    new-instance v9, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v9    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v9, v6, v0, v7}, Lcom/sec/android/gallery3d/data/LocalImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .line 1350
    .restart local v9    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :goto_2
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1353
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v8    # "id":I
    .end local v9    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 1348
    .restart local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v8    # "id":I
    .restart local v9    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_2
    :try_start_2
    invoke-virtual {v9, v7}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->updateContent(Landroid/database/Cursor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 1353
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v8    # "id":I
    .end local v9    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_3
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public getEventAlbumMap()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 417
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 418
    .local v9, "albumMap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 420
    .local v13, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "bucket_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "album_create_time"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "bucket_display_name"

    aput-object v1, v2, v0

    .line 422
    .local v2, "projection":[Ljava/lang/String;
    const-string v5, "album_create_time DESC"

    .line 424
    .local v5, "orderClause":Ljava/lang/String;
    const/4 v11, 0x0

    .line 426
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v1, "images_event_album"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 428
    if-eqz v11, :cond_2

    .line 430
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 431
    .local v10, "bucketId":I
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 432
    const/4 v0, 0x1

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 433
    .local v6, "albumCreateTime":J
    const/4 v0, 0x2

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 434
    .local v14, "name":Ljava/lang/String;
    new-instance v8, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;

    invoke-direct {v8, v10, v6, v7, v14}, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;-><init>(IJLjava/lang/String;)V

    .line 435
    .local v8, "albumInfo":Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 436
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 438
    .end local v6    # "albumCreateTime":J
    .end local v8    # "albumInfo":Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;
    .end local v14    # "name":Ljava/lang/String;
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 443
    .end local v10    # "bucketId":I
    :cond_2
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 446
    :goto_0
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string v1, "bucket_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "album_create_time"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "bucket_display_name"

    aput-object v1, v2, v0

    .line 448
    .restart local v2    # "projection":[Ljava/lang/String;
    const-string v5, "datetaken DESC, _id DESC"

    .line 450
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v1, "video_event_album"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 452
    if-eqz v11, :cond_5

    .line 454
    :cond_3
    const/4 v0, 0x0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 455
    .restart local v10    # "bucketId":I
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 456
    const/4 v0, 0x1

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 457
    .restart local v6    # "albumCreateTime":J
    const/4 v0, 0x2

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 458
    .restart local v14    # "name":Ljava/lang/String;
    new-instance v8, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;

    invoke-direct {v8, v10, v6, v7, v14}, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;-><init>(IJLjava/lang/String;)V

    .line 459
    .restart local v8    # "albumInfo":Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 460
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 462
    .end local v6    # "albumCreateTime":J
    .end local v8    # "albumInfo":Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;
    .end local v14    # "name":Ljava/lang/String;
    :cond_4
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_3

    .line 467
    .end local v10    # "bucketId":I
    :cond_5
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 469
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mAlbumComparator:Ljava/util/Comparator;

    invoke-static {v9, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 470
    return-object v9

    .line 440
    :catch_0
    move-exception v12

    .line 441
    .local v12, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 443
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 464
    :catch_1
    move-exception v12

    .line 465
    .restart local v12    # "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 467
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v0

    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getEventItemCount()I
    .locals 9

    .prologue
    .line 387
    const/4 v6, 0x0

    .line 388
    .local v6, "count":I
    const/4 v7, 0x0

    .line 390
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "count(*)"

    aput-object v1, v2, v0

    .line 391
    .local v2, "COUNT_PROJECTION":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v1, "images_event_album"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 393
    if-eqz v7, :cond_0

    .line 394
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    add-int/2addr v6, v0

    .line 399
    :cond_0
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 402
    .end local v2    # "COUNT_PROJECTION":[Ljava/lang/String;
    :goto_0
    const/4 v0, 0x1

    :try_start_1
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "count(*)"

    aput-object v1, v2, v0

    .line 403
    .restart local v2    # "COUNT_PROJECTION":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v1, "video_event_album"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 405
    if-eqz v7, :cond_1

    .line 406
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    add-int/2addr v6, v0

    .line 411
    :cond_1
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 413
    .end local v2    # "COUNT_PROJECTION":[Ljava/lang/String;
    :goto_1
    return v6

    .line 396
    :catch_0
    move-exception v8

    .line 397
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 399
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 408
    :catch_1
    move-exception v8

    .line 409
    .restart local v8    # "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 411
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getExpiredTime()J
    .locals 4

    .prologue
    .line 1307
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getTodayTime()J

    move-result-wide v0

    const-wide/32 v2, 0x240c8400

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getInitialScale(Lcom/sec/android/gallery3d/data/MediaSet;)I
    .locals 8
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 724
    const/16 v2, 0x15

    .line 725
    .local v2, "zoom":I
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getDistance(Lcom/sec/android/gallery3d/data/MediaSet;)[D

    move-result-object v0

    .line 726
    .local v0, "distance":[D
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v3, 0x15

    if-ge v1, v3, :cond_1

    .line 727
    sget-object v3, Lcom/sec/android/gallery3d/data/EventAlbumManager;->ZOOM_LEVEL:[D

    aget-wide v4, v3, v1

    const/4 v3, 0x0

    aget-wide v6, v0, v3

    cmpg-double v3, v4, v6

    if-ltz v3, :cond_0

    sget-object v3, Lcom/sec/android/gallery3d/data/EventAlbumManager;->ZOOM_LEVEL:[D

    aget-wide v4, v3, v1

    const/4 v3, 0x1

    aget-wide v6, v0, v3

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2

    .line 728
    :cond_0
    move v2, v1

    .line 733
    :cond_1
    const/16 v3, 0xd

    if-le v2, v3, :cond_3

    .line 734
    const/16 v2, 0xd

    .line 742
    :goto_1
    return v2

    .line 726
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 736
    :cond_3
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public getItemSets()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1231
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mItemSets:Ljava/util/List;

    return-object v0
.end method

.method public getLocation(Lcom/sec/android/gallery3d/data/MediaSet;)[D
    .locals 9
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v8, 0x0

    .line 705
    if-nez p1, :cond_1

    const/4 v3, 0x0

    .line 720
    :cond_0
    :goto_0
    return-object v3

    .line 707
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v4

    invoke-virtual {p1, v8, v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 708
    .local v2, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v4, 0x2

    new-array v3, v4, [D

    .line 710
    .local v3, "set":[D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 711
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v4, :cond_2

    .line 712
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 713
    .local v1, "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    iget-wide v4, v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;->latitude:D

    iget-wide v6, v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;->longitude:D

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 714
    iget-wide v4, v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;->latitude:D

    aput-wide v4, v3, v8

    .line 715
    const/4 v4, 0x1

    iget-wide v6, v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;->longitude:D

    aput-wide v6, v3, v4

    goto :goto_0

    .line 710
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getTimeString(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 777
    .local p1, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 778
    .local v8, "timeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 779
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v1, :cond_0

    .line 780
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 781
    .local v7, "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getDateInMs()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 778
    .end local v7    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 784
    :cond_1
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ""

    .line 786
    :goto_1
    return-object v1

    .line 785
    :cond_2
    invoke-static {v8}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 786
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    check-cast v1, Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const v6, 0x10014

    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public handleCropEvent(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Lcom/sec/android/gallery3d/data/Path;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "originalSetPathString"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 616
    const/4 v1, 0x0

    .line 617
    .local v1, "count":I
    const/4 v3, 0x0

    .line 618
    .local v3, "isSuggestion":Z
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {p3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 619
    .local v6, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v9, p2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    .line 620
    .local v5, "originalMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v5, :cond_0

    .line 621
    const/4 v9, 0x0

    .line 642
    :goto_0
    return-object v9

    .line 622
    :cond_0
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v9, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    .line 624
    .local v4, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v9, 0x1

    new-array v8, v9, [Landroid/content/ContentValues;

    .line 625
    .local v8, "values":[Landroid/content/ContentValues;
    instance-of v9, v4, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v9, :cond_2

    move-object v2, v4

    .line 626
    check-cast v2, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 627
    .local v2, "image":Lcom/sec/android/gallery3d/data/LocalImage;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v0

    .line 628
    .local v0, "bucketId":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getItemId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 629
    .local v7, "pkey":Ljava/lang/String;
    const/4 v9, 0x0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getContentValues()Landroid/content/ContentValues;

    move-result-object v10

    aput-object v10, v8, v9

    .line 630
    const/4 v9, 0x0

    aget-object v9, v8, v9

    const-string v10, "pkey"

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 631
    const/4 v9, 0x0

    aget-object v9, v8, v9

    const-string v10, "bucket_id"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 632
    const/4 v9, 0x0

    aget-object v9, v8, v9

    const-string v10, "bucket_id"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 633
    const/4 v9, 0x0

    aget-object v9, v8, v9

    const-string v10, "bucket_display_name"

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    const/4 v9, 0x0

    aget-object v10, v8, v9

    const-string/jumbo v11, "suggestion"

    const/4 v9, 0x1

    if-ne v3, v9, :cond_1

    const/4 v9, 0x1

    :goto_1
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v10, v11, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 636
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v10, "images_event_album"

    invoke-virtual {v9, v10, v8}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addEventInfo(Ljava/lang/String;[Landroid/content/ContentValues;)I

    move-result v1

    .line 637
    if-lez v1, :cond_2

    .line 638
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->EVENT_ALBUM_URI:Landroid/net/Uri;

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 639
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getItemId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    goto/16 :goto_0

    .line 634
    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    .line 642
    .end local v0    # "bucketId":I
    .end local v2    # "image":Lcom/sec/android/gallery3d/data/LocalImage;
    .end local v7    # "pkey":Ljava/lang/String;
    :cond_2
    const/4 v9, 0x0

    goto/16 :goto_0
.end method

.method public hasEventItem(Landroid/content/Context;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 852
    const/4 v6, 0x0

    .line 853
    .local v6, "bExist":Z
    const/4 v8, 0x0

    .line 855
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v1, "images_event_album"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_data"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 857
    if-eqz v8, :cond_1

    .line 859
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 860
    .local v10, "filepath":Ljava/lang/String;
    if-eqz v10, :cond_2

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 861
    const/4 v6, 0x1

    .line 869
    .end local v10    # "filepath":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 871
    :goto_1
    if-eqz v6, :cond_3

    move v7, v6

    .line 891
    .end local v6    # "bExist":Z
    .local v7, "bExist":Z
    :goto_2
    return v7

    .line 864
    .end local v7    # "bExist":Z
    .restart local v6    # "bExist":Z
    .restart local v10    # "filepath":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 866
    .end local v10    # "filepath":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 867
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 869
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 875
    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v1, "video_event_album"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_data"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 877
    if-eqz v8, :cond_5

    .line 879
    :cond_4
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 880
    .restart local v10    # "filepath":Ljava/lang/String;
    if-eqz v10, :cond_6

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v0

    if-eqz v0, :cond_6

    .line 881
    const/4 v6, 0x1

    .line 889
    .end local v10    # "filepath":Ljava/lang/String;
    :cond_5
    :goto_3
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_4
    move v7, v6

    .line 891
    .end local v6    # "bExist":Z
    .restart local v7    # "bExist":Z
    goto :goto_2

    .line 884
    .end local v7    # "bExist":Z
    .restart local v6    # "bExist":Z
    .restart local v10    # "filepath":Ljava/lang/String;
    :cond_6
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    if-nez v0, :cond_4

    goto :goto_3

    .line 886
    .end local v10    # "filepath":Ljava/lang/String;
    :catch_1
    move-exception v9

    .line 887
    .restart local v9    # "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 889
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_4

    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v0

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public loadItems(Landroid/content/Context;)Ljava/util/List;
    .locals 46
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1048
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1049
    .local v10, "CalendaritemSets":Ljava/util/List;, "Ljava/util/List<Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;>;>;"
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 1050
    .local v28, "itemSets":Ljava/util/List;, "Ljava/util/List<Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;>;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 1052
    .local v12, "cTime":J
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 1053
    .local v25, "eventItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;>;"
    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    .line 1054
    .local v33, "remainedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;>;"
    const/4 v4, 0x7

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v6, v4

    const/4 v4, 0x1

    const-string v5, "latitude"

    aput-object v5, v6, v4

    const/4 v4, 0x2

    const-string v5, "longitude"

    aput-object v5, v6, v4

    const/4 v4, 0x3

    const-string v5, "_data"

    aput-object v5, v6, v4

    const/4 v4, 0x4

    const-string v5, "datetaken"

    aput-object v5, v6, v4

    const/4 v4, 0x5

    const-string v5, "locality"

    aput-object v5, v6, v4

    const/4 v4, 0x6

    const-string/jumbo v5, "type"

    aput-object v5, v6, v4

    .line 1061
    .local v6, "projection":[Ljava/lang/String;
    const-string v9, "locality ASC, datetaken DESC, _id DESC"

    .line 1063
    .local v9, "orderClause":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "datetaken > "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getExpiredTime()J

    move-result-wide v44

    move-wide/from16 v0, v44

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1065
    .local v7, "imageWhere":Ljava/lang/String;
    new-instance v34, Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    move-object/from16 v0, v34

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;-><init>(Landroid/content/Context;)V

    .line 1066
    .local v34, "reverseGeocoder":Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    const/16 v18, 0x0

    .line 1068
    .local v18, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v5, "suggest_event_info"

    const/4 v8, 0x0

    invoke-virtual/range {v4 .. v9}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 1070
    if-nez v18, :cond_0

    .line 1071
    const-string v4, "LocalSuggestAlbumData"

    const-string v5, "cursor is null!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1094
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1227
    :goto_0
    return-object v28

    .line 1075
    :cond_0
    :try_start_1
    new-instance v24, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    invoke-direct/range {v24 .. v24}, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;-><init>()V

    .line 1076
    .local v24, "ei":Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;
    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v24

    iput v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->id:I

    .line 1077
    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    move-object/from16 v0, v24

    iput-wide v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->lat:D

    .line 1078
    const/4 v4, 0x2

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    move-object/from16 v0, v24

    iput-wide v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->lon:D

    .line 1079
    const/4 v4, 0x3

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    iput-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->data:Ljava/lang/String;

    .line 1080
    const/4 v4, 0x4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, v24

    iput-wide v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->dateInMs:J

    .line 1081
    const/4 v4, 0x5

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    iput-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    .line 1082
    const/4 v4, 0x6

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    const/4 v4, 0x1

    :goto_1
    move-object/from16 v0, v24

    iput-boolean v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->is_image:Z

    .line 1083
    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    if-nez v4, :cond_1

    move-object/from16 v0, v24

    iget-wide v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->lat:D

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->lon:D

    move-wide/from16 v44, v0

    move-wide/from16 v0, v44

    invoke-static {v4, v5, v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1084
    move-object/from16 v0, v24

    iget-wide v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->lat:D

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->lon:D

    move-wide/from16 v44, v0

    move-object/from16 v0, v34

    move-wide/from16 v1, v44

    invoke-virtual {v0, v4, v5, v1, v2}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->getLocality(DD)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    iput-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    .line 1086
    :cond_1
    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    if-nez v4, :cond_2

    .line 1087
    const-string v4, ""

    move-object/from16 v0, v24

    iput-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    .line 1089
    :cond_2
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1090
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 1094
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1099
    .end local v24    # "ei":Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;
    :goto_2
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getCalendarEvent(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v32

    .line 1100
    .local v32, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;>;"
    const/16 v26, 0x0

    .local v26, "i":I
    :goto_3
    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v26

    if-ge v0, v4, :cond_9

    .line 1101
    move-object/from16 v0, v32

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;

    iget-wide v0, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->dtstart:J

    move-wide/from16 v22, v0

    .line 1102
    .local v22, "dtstart":J
    move-object/from16 v0, v32

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;

    iget-wide v0, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->dtend:J

    move-wide/from16 v20, v0

    .line 1103
    .local v20, "dtend":J
    move-object/from16 v0, v32

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;

    iget-object v0, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->title:Ljava/lang/String;

    move-object/from16 v40, v0

    .line 1105
    .local v40, "title":Ljava/lang/String;
    cmp-long v4, v12, v20

    if-gez v4, :cond_5

    .line 1100
    :cond_3
    :goto_4
    add-int/lit8 v26, v26, 0x1

    goto :goto_3

    .line 1082
    .end local v20    # "dtend":J
    .end local v22    # "dtstart":J
    .end local v26    # "i":I
    .end local v32    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;>;"
    .end local v40    # "title":Ljava/lang/String;
    .restart local v24    # "ei":Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;
    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1091
    .end local v24    # "ei":Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;
    :catch_0
    move-exception v19

    .line 1092
    .local v19, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1094
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_2

    .end local v19    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v4

    .line 1108
    .restart local v20    # "dtend":J
    .restart local v22    # "dtstart":J
    .restart local v26    # "i":I
    .restart local v32    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;>;"
    .restart local v40    # "title":Ljava/lang/String;
    :cond_5
    new-instance v36, Ljava/util/ArrayList;

    invoke-direct/range {v36 .. v36}, Ljava/util/ArrayList;-><init>()V

    .line 1109
    .local v36, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;>;"
    const/16 v29, 0x0

    .local v29, "k":I
    :goto_5
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v29

    if-ge v0, v4, :cond_6

    .line 1110
    move-object/from16 v0, v25

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-wide v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->dateInMs:J

    cmp-long v4, v4, v22

    if-gez v4, :cond_7

    .line 1119
    :cond_6
    invoke-virtual/range {v36 .. v36}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/16 v5, 0xa

    if-lt v4, v5, :cond_3

    .line 1120
    move-object/from16 v0, v36

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1121
    move-object/from16 v0, v25

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 1113
    :cond_7
    move-object/from16 v0, v25

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-wide v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->dateInMs:J

    cmp-long v4, v4, v20

    if-gez v4, :cond_8

    move-object/from16 v0, v32

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->location:Ljava/lang/String;

    if-eqz v4, :cond_8

    move-object/from16 v0, v32

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;

    iget-object v5, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$CalendarEventItem;->location:Ljava/lang/String;

    move-object/from16 v0, v25

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1115
    move-object/from16 v0, v25

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    move-object/from16 v0, v40

    iput-object v0, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    .line 1116
    move-object/from16 v0, v25

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1109
    :cond_8
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_5

    .line 1125
    .end local v20    # "dtend":J
    .end local v22    # "dtstart":J
    .end local v29    # "k":I
    .end local v36    # "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;>;"
    .end local v40    # "title":Ljava/lang/String;
    :cond_9
    const/16 v27, -0x1

    .line 1127
    .local v27, "index":I
    const-string v14, ""

    .line 1128
    .local v14, "curLocality":Ljava/lang/String;
    const-wide/16 v16, 0x0

    .line 1129
    .local v16, "curTime":J
    const-wide/16 v38, 0x0

    .line 1130
    .local v38, "tempTime":J
    const-string v15, ""

    .line 1131
    .local v15, "curTimeString":Ljava/lang/String;
    const-string v37, ""

    .line 1132
    .local v37, "tempTimeString":Ljava/lang/String;
    const/16 v26, 0x0

    :goto_6
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v26

    if-ge v0, v4, :cond_d

    .line 1133
    const-string v5, ""

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 1134
    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-wide v0, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->dateInMs:J

    move-wide/from16 v38, v0

    .line 1135
    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 1136
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1137
    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-object v14, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    .line 1138
    const-wide/16 v16, 0x0

    .line 1139
    add-int/lit8 v27, v27, 0x1

    .line 1145
    :cond_a
    :goto_7
    move-wide/from16 v16, v38

    .line 1146
    move-object/from16 v0, v28

    move/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1132
    :cond_b
    add-int/lit8 v26, v26, 0x1

    goto :goto_6

    .line 1140
    :cond_c
    sub-long v4, v16, v38

    const-wide/32 v44, 0x5265c00

    cmp-long v4, v4, v44

    if-lez v4, :cond_a

    .line 1141
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1142
    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-object v14, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    .line 1143
    add-int/lit8 v27, v27, 0x1

    goto :goto_7

    .line 1149
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mComparator:Ljava/util/Comparator;

    move-object/from16 v0, v28

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1151
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_e

    .line 1152
    const/4 v4, 0x0

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/util/ArrayList;

    .line 1153
    .local v31, "lastSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;>;"
    const/4 v4, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    .line 1154
    .local v30, "lastItem":Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mCurLocality:Ljava/lang/String;

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mCurLocality:Ljava/lang/String;

    move-object/from16 v0, v30

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    move-object/from16 v0, v30

    iget-wide v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->dateInMs:J

    sub-long v4, v12, v4

    const-wide/32 v44, 0x5265c00

    cmp-long v4, v4, v44

    if-gez v4, :cond_e

    .line 1155
    move-object/from16 v0, v33

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1156
    const/4 v4, 0x0

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1160
    .end local v30    # "lastItem":Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;
    .end local v31    # "lastSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;>;"
    :cond_e
    const/16 v26, 0x0

    :goto_8
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v26

    if-ge v0, v4, :cond_10

    .line 1161
    move-object/from16 v0, v28

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/16 v5, 0xa

    if-ge v4, v5, :cond_f

    .line 1162
    move-object/from16 v0, v28

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1163
    add-int/lit8 v26, v26, -0x1

    .line 1160
    :goto_9
    add-int/lit8 v26, v26, 0x1

    goto :goto_8

    .line 1165
    :cond_f
    move-object/from16 v0, v28

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Collection;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    goto :goto_9

    .line 1168
    :cond_10
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v11

    .line 1171
    .local v11, "LocalityEventCount":I
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v27, v4, -0x1

    .line 1172
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getTodayTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getDate(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v41

    .line 1173
    .local v41, "today":Ljava/lang/String;
    const/16 v26, 0x0

    :goto_a
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v26

    if-ge v0, v4, :cond_13

    .line 1174
    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-wide v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->dateInMs:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getDate(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v37

    .line 1175
    move-object/from16 v0, v41

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1173
    :goto_b
    add-int/lit8 v26, v26, 0x1

    goto :goto_a

    .line 1177
    :cond_11
    move-object/from16 v0, v37

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_12

    .line 1178
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179
    move-object/from16 v15, v37

    .line 1180
    add-int/lit8 v27, v27, 0x1

    .line 1182
    :cond_12
    move-object/from16 v0, v28

    move/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 1185
    :cond_13
    move/from16 v26, v11

    :goto_c
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v26

    if-ge v0, v4, :cond_15

    .line 1186
    move-object/from16 v0, v28

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/16 v5, 0x14

    if-ge v4, v5, :cond_14

    .line 1187
    move-object/from16 v0, v28

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1188
    add-int/lit8 v26, v26, -0x1

    .line 1185
    :goto_d
    add-int/lit8 v26, v26, 0x1

    goto :goto_c

    .line 1190
    :cond_14
    move-object/from16 v0, v28

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Collection;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    goto :goto_d

    .line 1194
    :cond_15
    const/16 v26, 0x0

    :goto_e
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v26

    if-ge v0, v4, :cond_17

    .line 1195
    const-string v5, ""

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-wide v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->dateInMs:J

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getTodayTime()J

    move-result-wide v44

    cmp-long v4, v4, v44

    if-lez v4, :cond_16

    .line 1196
    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1194
    :cond_16
    add-int/lit8 v26, v26, 0x1

    goto :goto_e

    .line 1201
    :cond_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v5, "suggest_event_info"

    const/4 v8, 0x0

    const/16 v43, 0x0

    move-object/from16 v0, v43

    invoke-virtual {v4, v5, v8, v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1203
    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->size()I

    move-result v35

    .line 1204
    .local v35, "size":I
    if-lez v35, :cond_1a

    .line 1205
    move/from16 v0, v35

    new-array v0, v0, [Landroid/content/ContentValues;

    move-object/from16 v42, v0

    .line 1206
    .local v42, "values":[Landroid/content/ContentValues;
    const/16 v26, 0x0

    :goto_f
    move/from16 v0, v26

    move/from16 v1, v35

    if-ge v0, v1, :cond_19

    .line 1207
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    aput-object v4, v42, v26

    .line 1208
    aget-object v4, v42, v26

    if-eqz v4, :cond_18

    .line 1209
    aget-object v5, v42, v26

    const-string v8, "_id"

    move-object/from16 v0, v33

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->id:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v8, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1210
    aget-object v5, v42, v26

    const-string v8, "latitude"

    move-object/from16 v0, v33

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-wide v0, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->lat:D

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v5, v8, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1211
    aget-object v5, v42, v26

    const-string v8, "longitude"

    move-object/from16 v0, v33

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-wide v0, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->lon:D

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v5, v8, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1212
    aget-object v5, v42, v26

    const-string v8, "_data"

    move-object/from16 v0, v33

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->data:Ljava/lang/String;

    invoke-virtual {v5, v8, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1213
    aget-object v5, v42, v26

    const-string v8, "datetaken"

    move-object/from16 v0, v33

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-wide v0, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->dateInMs:J

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v5, v8, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1214
    aget-object v5, v42, v26

    const-string v8, "locality"

    move-object/from16 v0, v33

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    invoke-virtual {v5, v8, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    aget-object v5, v42, v26

    const-string/jumbo v8, "type"

    move-object/from16 v0, v33

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-boolean v4, v4, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->is_image:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v5, v8, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1206
    :cond_18
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_f

    .line 1218
    :cond_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v5, "suggest_event_info"

    move-object/from16 v0, v42

    invoke-virtual {v4, v5, v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addEventInfo(Ljava/lang/String;[Landroid/content/ContentValues;)I

    .line 1222
    .end local v42    # "values":[Landroid/content/ContentValues;
    :cond_1a
    const/16 v26, 0x0

    :goto_10
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v26

    if-ge v0, v4, :cond_1b

    .line 1223
    move/from16 v0, v26

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1222
    add-int/lit8 v26, v26, 0x1

    goto :goto_10

    .line 1226
    :cond_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mComparator:Ljava/util/Comparator;

    move-object/from16 v0, v28

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/16 :goto_0
.end method

.method public prepareCreateEventDialog(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "selectionModeProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    const/4 v3, 0x1

    .line 222
    const/4 v1, 0x0

    const v2, 0x7f0e0032

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v3, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 223
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 224
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/sec/android/gallery3d/data/EventAlbumManager$5;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/data/EventAlbumManager$5;-><init>(Lcom/sec/android/gallery3d/data/EventAlbumManager;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 234
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;

    invoke-direct {v1, p0, p2, p1}, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;-><init>(Lcom/sec/android/gallery3d/data/EventAlbumManager;Lcom/sec/android/gallery3d/ui/SelectionManager;Landroid/content/Context;)V

    const-string v2, "prepareCreateEventDialog"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 267
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 268
    return-void
.end method

.method public releaseInstance()V
    .locals 2

    .prologue
    .line 137
    sget-object v1, Lcom/sec/android/gallery3d/data/EventAlbumManager;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 138
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->sInstance:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    .line 139
    monitor-exit v1

    .line 140
    return-void

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeExpiredEvent()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 354
    const/4 v1, 0x1

    .line 355
    .local v1, "isSuggestion":Z
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getExpiredTime()J

    move-result-wide v2

    .line 357
    .local v2, "time":J
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 358
    .local v0, "imageWhere":Ljava/lang/StringBuilder;
    const-string v5, "("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v8, "suggestion"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-ne v1, v6, :cond_0

    move v5, v6

    :goto_0
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " AND "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "datetaken"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "<"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ")"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 364
    .local v4, "videoWhere":Ljava/lang/StringBuilder;
    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v8, "suggestion"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-ne v1, v6, :cond_1

    :goto_1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "datetaken"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v6, "images_event_album"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v9}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 371
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v6, "video_event_album"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v9}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 372
    return-void

    .end local v4    # "videoWhere":Ljava/lang/StringBuilder;
    :cond_0
    move v5, v7

    .line 358
    goto :goto_0

    .restart local v4    # "videoWhere":Ljava/lang/StringBuilder;
    :cond_1
    move v6, v7

    .line 364
    goto :goto_1
.end method

.method public removeSuggestionMark(I)V
    .locals 8
    .param p1, "bucketId"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    .line 340
    const/4 v1, 0x0

    .line 341
    .local v1, "isSuggestion":Z
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 342
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v5, "suggestion"

    if-ne v1, v4, :cond_0

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 345
    .local v0, "imageWhere":Ljava/lang/StringBuilder;
    const-string v4, "bucket_id"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 346
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 347
    .local v3, "videoWhere":Ljava/lang/StringBuilder;
    const-string v4, "bucket_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 349
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v5, "images_event_album"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v2, v6, v7}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 350
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v5, "video_event_album"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v2, v6, v7}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 351
    return-void

    .line 342
    .end local v0    # "imageWhere":Ljava/lang/StringBuilder;
    .end local v3    # "videoWhere":Ljava/lang/StringBuilder;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public renameEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "orgName"    # Ljava/lang/String;
    .param p3, "newName"    # Ljava/lang/String;

    .prologue
    .line 575
    move-object/from16 v2, p1

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v14

    .line 576
    .local v14, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-direct {p0, v14}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getSelectedItemList(Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/util/ArrayList;

    move-result-object v4

    .line 578
    .local v4, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getTimeString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v12

    .line 579
    .local v12, "newAlbumTime":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/local/event/all/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v5

    .line 580
    .local v5, "newBucketId":I
    const/4 v13, 0x0

    .line 581
    .local v13, "oldBucketId":I
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 582
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getBucketId()I

    move-result v13

    .line 585
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getEventAlbumMap()Ljava/util/ArrayList;

    move-result-object v10

    .line 586
    .local v10, "albumMap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;>;"
    move-object/from16 v0, p3

    invoke-direct {p0, v10, v5, v0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->isDuplicated(Ljava/util/ArrayList;ILjava/lang/String;)I

    move-result v2

    if-lez v2, :cond_1

    .line 587
    const v2, 0x7f0e0217

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 588
    const/4 v2, 0x0

    .line 603
    .end local p1    # "context":Landroid/content/Context;
    :goto_0
    return v2

    .line 590
    .restart local p1    # "context":Landroid/content/Context;
    :cond_1
    const-wide/16 v8, 0x0

    .line 591
    .local v8, "albumCreateTime":J
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v11, v2, :cond_2

    .line 592
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;

    iget v2, v2, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;->bucketId:I

    if-ne v2, v13, :cond_3

    .line 593
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;

    iget-wide v8, v2, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;->albumCreateTime:J

    .line 597
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {p0, v0, v14, v2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->deleteEvent(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Z)V

    .line 598
    const/4 v7, 0x1

    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v6, p3

    invoke-virtual/range {v2 .. v9}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->createNewEventAlbum(Landroid/content/Context;Ljava/util/ArrayList;ILjava/lang/String;ZJ)Z

    .line 600
    invoke-virtual {v14}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 601
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-static/range {p1 .. p1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "EXIT_SELECTION_MODE"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 603
    const/4 v2, 0x1

    goto :goto_0

    .line 591
    .restart local p1    # "context":Landroid/content/Context;
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/LoadingListener;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    .line 144
    return-void
.end method

.method public updateCurLocality(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 147
    invoke-static {}, Lcom/sec/android/gallery3d/util/LocationUtils;->getInstance()Lcom/sec/android/gallery3d/util/LocationUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/util/LocationUtils;->getLocation()Landroid/location/Location;

    move-result-object v0

    .line 148
    .local v0, "curLocation":Landroid/location/Location;
    if-eqz v0, :cond_0

    .line 149
    new-instance v1, Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    invoke-direct {v1, p1}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;-><init>(Landroid/content/Context;)V

    .line 150
    .local v1, "reverseGeocoder":Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/sec/android/gallery3d/data/EventAlbumManager$4;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/android/gallery3d/data/EventAlbumManager$4;-><init>(Lcom/sec/android/gallery3d/data/EventAlbumManager;Lcom/sec/android/gallery3d/util/ReverseGeocoder;Landroid/location/Location;)V

    const-string/jumbo v4, "updateCurLocalityThread"

    invoke-direct {v2, v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 157
    .end local v1    # "reverseGeocoder":Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    :cond_0
    return-void
.end method

.method public updateFilePath(Lcom/sec/android/gallery3d/data/MediaItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "oldFilePath"    # Ljava/lang/String;
    .param p3, "newFilePath"    # Ljava/lang/String;
    .param p4, "newFileName"    # Ljava/lang/String;

    .prologue
    .line 646
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 648
    .local v2, "value":Landroid/content/ContentValues;
    const-string v1, ""

    .line 649
    .local v1, "table":Ljava/lang/String;
    const-string v3, ""

    .line 650
    .local v3, "where":Ljava/lang/String;
    const-string v4, "\'"

    const-string v5, "\'\'"

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 651
    .local v0, "path":Ljava/lang/String;
    instance-of v4, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v4, :cond_2

    .line 652
    const-string v1, "images_event_album"

    .line 653
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_data=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 654
    const-string v4, "_id"

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 655
    const-string v4, "_data"

    invoke-virtual {v2, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    const-string/jumbo v4, "title"

    invoke-virtual {v2, v4, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    :cond_0
    :goto_0
    const-string v4, ""

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 666
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v1, v2, v5, v6}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 668
    :cond_1
    return-void

    .line 657
    :cond_2
    instance-of v4, p1, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v4, :cond_0

    .line 658
    const-string/jumbo v1, "video_event_album"

    .line 659
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_data=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 660
    const-string v4, "_id"

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 661
    const-string v4, "_data"

    invoke-virtual {v2, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    const-string/jumbo v4, "title"

    invoke-virtual {v2, v4, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateLocalDB(ZLjava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "isImage"    # Z
    .param p2, "table"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 895
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->updateLocalDB(ZLjava/lang/String;Ljava/lang/Integer;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public updateLocalDB(ZLjava/lang/String;Ljava/lang/Integer;)Ljava/util/List;
    .locals 22
    .param p1, "isImage"    # Z
    .param p2, "table"    # Ljava/lang/String;
    .param p3, "bucketId"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 899
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 900
    .local v20, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 904
    .local v19, "idsLocalDB":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v8, 0x0

    .line 905
    .local v8, "selection":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 906
    const-string v18, "_id"

    .line 907
    .local v18, "idEntry":Ljava/lang/String;
    const-string v13, "_data"

    .line 908
    .local v13, "dataEntry":Ljava/lang/String;
    if-eqz p3, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bucket_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 915
    :goto_0
    const/4 v12, 0x0

    .line 917
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const/4 v6, 0x2

    new-array v7, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v18, v7, v6

    const/4 v6, 0x1

    aput-object v13, v7, v6

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v6, p2

    invoke-virtual/range {v5 .. v10}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 920
    if-nez v12, :cond_3

    .line 921
    const-string v5, "LocalSuggestAlbumData"

    const-string v6, "query fail: "

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 978
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 980
    :goto_1
    return-object v20

    .line 908
    .end local v12    # "cursor":Landroid/database/Cursor;
    :cond_0
    const/4 v8, 0x0

    goto :goto_0

    .line 910
    .end local v13    # "dataEntry":Ljava/lang/String;
    .end local v18    # "idEntry":Ljava/lang/String;
    :cond_1
    const-string v18, "_id"

    .line 911
    .restart local v18    # "idEntry":Ljava/lang/String;
    const-string v13, "_data"

    .line 912
    .restart local v13    # "dataEntry":Ljava/lang/String;
    if-eqz p3, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bucket_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_2
    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    goto :goto_2

    .line 925
    .restart local v12    # "cursor":Landroid/database/Cursor;
    :cond_3
    :try_start_1
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    .line 926
    .local v21, "where":Ljava/lang/StringBuilder;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 927
    .local v14, "deleteWhere":Ljava/lang/StringBuilder;
    const/4 v11, 0x0

    .line 929
    .local v11, "count":I
    :cond_4
    add-int/lit8 v11, v11, 0x1

    .line 930
    const/4 v5, 0x0

    invoke-interface {v12, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 931
    .local v17, "id":I
    const/4 v5, 0x1

    invoke-interface {v12, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 932
    .local v16, "filepath":Ljava/lang/String;
    if-eqz v16, :cond_6

    new-instance v5, Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_6

    .line 933
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_5

    .line 934
    const-string v5, " OR "

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 936
    :cond_5
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 941
    :cond_6
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 942
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 943
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_7

    .line 944
    const-string v5, " OR "

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 946
    :cond_7
    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 948
    :cond_8
    const/16 v5, 0x64

    if-lt v11, v5, :cond_b

    .line 949
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_9

    .line 950
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v19

    move-object/from16 v4, v21

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->deleteIfNotExist(ZLjava/lang/String;Ljava/util/ArrayList;Ljava/lang/StringBuilder;)V

    .line 951
    const/4 v5, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 953
    :cond_9
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_a

    .line 954
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v5, v0, v6, v7}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 955
    const/4 v5, 0x0

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 957
    :cond_a
    const/4 v11, 0x0

    .line 959
    :cond_b
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_4

    .line 961
    if-lez v11, :cond_d

    .line 962
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_c

    .line 963
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v19

    move-object/from16 v4, v21

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->deleteIfNotExist(ZLjava/lang/String;Ljava/util/ArrayList;Ljava/lang/StringBuilder;)V

    .line 964
    const/4 v5, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 966
    :cond_c
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_d

    .line 967
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v5, v0, v6, v7}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 968
    const/4 v5, 0x0

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->setLength(I)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 978
    :cond_d
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_1

    .line 971
    .end local v11    # "count":I
    .end local v14    # "deleteWhere":Ljava/lang/StringBuilder;
    .end local v16    # "filepath":Ljava/lang/String;
    .end local v17    # "id":I
    .end local v21    # "where":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v15

    .line 972
    .local v15, "e":Ljava/lang/IllegalStateException;
    :try_start_2
    invoke-virtual {v15}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 978
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_1

    .line 973
    .end local v15    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v15

    .line 974
    .local v15, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    invoke-virtual {v15}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 978
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_1

    .line 975
    .end local v15    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_2
    move-exception v15

    .line 976
    .local v15, "e":Landroid/database/CursorWindowAllocationException;
    :try_start_4
    invoke-virtual {v15}, Landroid/database/CursorWindowAllocationException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 978
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_1

    .end local v15    # "e":Landroid/database/CursorWindowAllocationException;
    :catchall_0
    move-exception v5

    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v5
.end method

.method public updateSuggestAlbum(Landroid/content/Context;)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 160
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 161
    .local v4, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v2, 0x0

    .line 162
    .local v2, "count":I
    const/4 v7, 0x1

    .line 164
    .local v7, "isSuggestion":Z
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->loadItems(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mItemSets:Ljava/util/List;

    .line 165
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mItemSets:Ljava/util/List;

    monitor-enter v15

    .line 166
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mItemSets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 167
    monitor-exit v15

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    const/4 v12, 0x0

    .local v12, "indexOfSet":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mItemSets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v12, v3, :cond_6

    .line 169
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 170
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mItemSets:Ljava/util/List;

    invoke-interface {v3, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    .line 172
    .local v10, "eventlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;>;"
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v3

    if-ge v11, v3, :cond_3

    .line 174
    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-boolean v3, v3, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->is_image:Z

    if-eqz v3, :cond_2

    .line 175
    sget-object v16, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget v3, v3, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->id:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v14

    .line 179
    .local v14, "path":Lcom/sec/android/gallery3d/data/Path;
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3, v14}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 177
    .end local v14    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_2
    sget-object v16, Lcom/sec/android/gallery3d/data/LocalVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget v3, v3, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->id:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v14

    .restart local v14    # "path":Lcom/sec/android/gallery3d/data/Path;
    goto :goto_3

    .line 182
    .end local v14    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_3
    const-wide/16 v8, 0x0

    .line 183
    .local v8, "albumCreateTime":J
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_5

    .line 184
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;

    iget-object v6, v3, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;->locality:Ljava/lang/String;

    .line 185
    .local v6, "newAlbumName":Ljava/lang/String;
    const-string v3, ""

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 186
    const v3, 0x7f0e0069

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 188
    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getTimeString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v13

    .line 189
    .local v13, "newAlbumTime":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 190
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "/local/event/all/"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    .line 191
    .local v5, "bucketId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-object/from16 v16, v0

    const-string v17, "images_event_album"

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getImageContentValues(Ljava/util/ArrayList;ILjava/lang/String;ZJ)[Landroid/content/ContentValues;

    move-result-object v3

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addEventInfo(Ljava/lang/String;[Landroid/content/ContentValues;)I

    move-result v3

    add-int/2addr v2, v3

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-object/from16 v16, v0

    const-string/jumbo v17, "video_event_album"

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getVideoContentValues(Ljava/util/ArrayList;ILjava/lang/String;ZJ)[Landroid/content/ContentValues;

    move-result-object v3

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addEventInfo(Ljava/lang/String;[Landroid/content/ContentValues;)I

    move-result v3

    add-int/2addr v2, v3

    .line 168
    .end local v5    # "bucketId":I
    .end local v6    # "newAlbumName":Ljava/lang/String;
    .end local v13    # "newAlbumTime":Ljava/lang/String;
    :cond_5
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1

    .line 198
    .end local v8    # "albumCreateTime":J
    .end local v10    # "eventlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventItem;>;"
    .end local v11    # "i":I
    :cond_6
    if-lez v2, :cond_7

    .line 199
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v16, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->EVENT_ALBUM_URI:Landroid/net/Uri;

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 201
    :cond_7
    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    if-eqz v3, :cond_0

    .line 204
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/EventAlbumManager;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    const/4 v15, 0x0

    invoke-interface {v3, v15}, Lcom/sec/android/gallery3d/app/LoadingListener;->onLoadingFinished(Z)V

    goto/16 :goto_0

    .line 201
    .end local v12    # "indexOfSet":I
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method
