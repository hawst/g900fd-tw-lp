.class Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;
.super Ljava/lang/Object;
.source "NearbyClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PlayInfo"
.end annotation


# instance fields
.field public item:Lcom/sec/android/gallery3d/data/MediaItem;

.field public mediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field public viewer:Lcom/samsung/android/allshare/media/ImageViewer;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/media/ImageViewer;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p1, "viewer"    # Lcom/samsung/android/allshare/media/ImageViewer;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 703
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;->viewer:Lcom/samsung/android/allshare/media/ImageViewer;

    .line 704
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;->mediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 705
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 706
    return-void
.end method
