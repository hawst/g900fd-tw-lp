.class public interface abstract Lcom/sec/android/gallery3d/app/GalleryApp;
.super Ljava/lang/Object;
.source "GalleryApp.java"


# virtual methods
.method public abstract checkFaceInCurrentGroup(ILjava/lang/String;)Z
.end method

.method public abstract checkInGroupCluster()Z
.end method

.method public abstract getAlbumPath()Ljava/lang/String;
.end method

.method public abstract getAndroidContext()Landroid/content/Context;
.end method

.method public abstract getAppIntent()Landroid/content/Intent;
.end method

.method public abstract getCacheInterface()Lcom/sec/samsung/gallery/decoder/CacheInterface;
.end method

.method public abstract getContentResolver()Landroid/content/ContentResolver;
.end method

.method public abstract getCurrentClusterType()I
.end method

.method public abstract getDataManager()Lcom/sec/android/gallery3d/data/DataManager;
.end method

.method public abstract getDocumentUpdateStatus()Z
.end method

.method public abstract getDownloadCache()Lcom/sec/android/gallery3d/data/DownloadCache;
.end method

.method public abstract getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;
.end method

.method public abstract getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;
.end method

.method public abstract getMainLooper()Landroid/os/Looper;
.end method

.method public abstract getRefreshOperation()I
.end method

.method public abstract getRefreshState()Z
.end method

.method public abstract getResources()Landroid/content/res/Resources;
.end method

.method public abstract getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;
.end method

.method public abstract getSortByType()I
.end method

.method public abstract getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;
.end method

.method public abstract isArcMode()Z
.end method

.method public abstract isDocMode()Z
.end method

.method public abstract isFestivalMode()Z
.end method

.method public abstract isPhotoPage()Z
.end method

.method public abstract isSlideShowMode()Z
.end method

.method public abstract isTaskComplete()Z
.end method

.method public abstract needCleanCache()Z
.end method

.method public abstract registerContactObserver(Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;)V
.end method

.method public abstract setAlbumPath(Ljava/lang/String;)V
.end method

.method public abstract setAppIntent(Landroid/content/Intent;)V
.end method

.method public abstract setCleanCacheStatus(Z)V
.end method

.method public abstract setCurrentClusterType(I)V
.end method

.method public abstract setDocumentUpdateStatus(Z)V
.end method

.method public abstract setIsPhotoPage(Z)V
.end method

.method public abstract setPersonGroupsToGlobal(Landroid/util/SparseArray;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<[",
            "Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setRefreshOperation(I)V
.end method

.method public abstract setRefreshState(Z)V
.end method

.method public abstract setSlideShowMode(Z)V
.end method

.method public abstract setSortByType(I)V
.end method

.method public abstract setTaskStatus(Z)V
.end method

.method public abstract unregisterContactObserver(Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;)V
.end method
