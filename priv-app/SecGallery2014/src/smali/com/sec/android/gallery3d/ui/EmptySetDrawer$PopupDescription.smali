.class Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;
.super Ljava/lang/Object;
.source "EmptySetDrawer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/EmptySetDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PopupDescription"
.end annotation


# instance fields
.field private mGallery:Lcom/sec/android/gallery3d/app/GalleryActivity;

.field private final mImgGetter:Landroid/text/Html$ImageGetter;

.field private mMainLayout:Landroid/view/ViewGroup;

.field private mNoItemBg:Landroid/widget/ImageView;

.field private mNoItemImage:Landroid/widget/ImageView;

.field private mNoItemText:Landroid/widget/TextView;

.field private mPopupLayout:Landroid/view/View;

.field private mPopupText:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;Lcom/sec/android/gallery3d/app/GalleryActivity;)V
    .locals 1
    .param p2, "gallery"    # Lcom/sec/android/gallery3d/app/GalleryActivity;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412
    new-instance v0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription$1;-><init>(Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mImgGetter:Landroid/text/Html$ImageGetter;

    .line 184
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mGallery:Lcom/sec/android/gallery3d/app/GalleryActivity;

    .line 186
    const v0, 0x7f0f0175

    invoke-virtual {p2, v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mMainLayout:Landroid/view/ViewGroup;

    .line 187
    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    # setter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDisplayHeight:I
    invoke-static {p1, v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$002(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;I)I

    .line 188
    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    # setter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDisplayWidth:I
    invoke-static {p1, v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$202(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;I)I

    .line 189
    return-void
.end method

.method private createTextWithCameraIcon(I)Landroid/text/Spanned;
    .locals 4
    .param p1, "stringID"    # I

    .prologue
    .line 406
    const-string v0, "<img src=\"camera_icon\"/>"

    .line 407
    .local v0, "imgSrc":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 408
    .local v1, "srcStr":Ljava/lang/String;
    const-string v2, "%s"

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 409
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mImgGetter:Landroid/text/Html$ImageGetter;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    return-object v2
.end method

.method private isCameraButtonVisible()Z
    .locals 3

    .prologue
    .line 387
    const/4 v1, 0x1

    .line 388
    .local v1, "visible":Z
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isUsePick()Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$600(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v0

    .line 389
    .local v0, "hideCameraButton":Z
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isUsePick()Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$600(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isWilcoxds3gzmEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isBaffinRDEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMs013gzmEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 392
    :cond_0
    const/4 v0, 0x1

    .line 394
    :cond_1
    if-nez v0, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isContactPick()Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$500(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isPersonPick()Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$400(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->isConnectedDevice()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 395
    :cond_2
    const/4 v1, 0x0

    .line 398
    :cond_3
    return v1
.end method

.method private isConnectedDevice()Z
    .locals 2

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getPreviousViewState()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPhotoSignaturePick()Z
    .locals 2

    .prologue
    .line 378
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "single-album"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-eqz v0, :cond_0

    .line 380
    const/4 v0, 0x1

    .line 383
    :goto_0
    return v0

    .line 381
    :catch_0
    move-exception v0

    .line 383
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setNoItemBgParameter()V
    .locals 3

    .prologue
    .line 257
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupLayout:Landroid/view/View;

    const v2, 0x7f0f0163

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemBg:Landroid/widget/ImageView;

    .line 258
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isLandscape()Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$700(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 259
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemBg:Landroid/widget/ImageView;

    const v2, 0x7f020520

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 260
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemBg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 261
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$900(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getNoItemsHeightLandscape()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 262
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemBg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 269
    :goto_0
    return-void

    .line 264
    .end local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemBg:Landroid/widget/ImageView;

    const v2, 0x7f020521

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 265
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemBg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 266
    .restart local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$900(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getNoItemsHeightPortrait()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 267
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemBg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private setNoItemImageParameter()V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupLayout:Landroid/view/View;

    const v1, 0x7f0f0161

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemImage:Landroid/widget/ImageView;

    .line 193
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEmptyScreenGalleryIcon:Z

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemImage:Landroid/widget/ImageView;

    const v1, 0x7f0201dc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 197
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemImage:Landroid/widget/ImageView;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private setNoItemText()V
    .locals 6

    .prologue
    const v5, 0x7f0e0168

    .line 212
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupLayout:Landroid/view/View;

    const v4, 0x7f0f0162

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemText:Landroid/widget/TextView;

    .line 213
    const/4 v0, 0x0

    .line 214
    .local v0, "description":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$300(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v1

    .line 215
    .local v1, "viewByType":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isPersonPick()Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$400(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isContactPick()Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$500(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 216
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0167

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 236
    :goto_0
    if-eqz v0, :cond_2

    .line 237
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDisplayWidth:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$200(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x4

    div-int/lit8 v2, v3, 0x5

    .line 238
    .local v2, "width":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemText:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isLandscape()Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$700(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 240
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWvga(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 241
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDisplayWidth:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$200(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x4

    div-int/lit8 v2, v3, 0x5

    .line 247
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemText:Landroid/widget/TextView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 248
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemText:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setWidth(I)V

    .line 250
    .end local v2    # "width":I
    :cond_2
    return-void

    .line 220
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isUsePick()Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$600(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 221
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 222
    :cond_4
    sget-object v3, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v3

    if-ne v3, v1, :cond_6

    .line 223
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    if-eqz v3, :cond_5

    .line 224
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e016d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 226
    :cond_5
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e016b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 228
    :cond_6
    sget-object v3, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v3

    if-ne v3, v1, :cond_7

    .line 229
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e016c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 230
    :cond_7
    sget-object v3, Lcom/sec/samsung/gallery/core/ViewByFilterType;->PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v3

    if-ne v3, v1, :cond_8

    .line 231
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 233
    :cond_8
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 243
    .restart local v2    # "width":I
    :cond_9
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDisplayWidth:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$200(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)I

    move-result v3

    shr-int/lit8 v2, v3, 0x1

    goto/16 :goto_1
.end method

.method private setNoItemTextParameter()V
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mNoItemText:Landroid/widget/TextView;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->FONT_COLOR:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$800()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 254
    return-void
.end method

.method private setPopupLayout()V
    .locals 3

    .prologue
    .line 310
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupLayout:Landroid/view/View;

    const v2, 0x7f0f0160

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    .line 311
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 312
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    if-nez v0, :cond_0

    .line 336
    :goto_0
    return-void

    .line 314
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$900(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewBubblePopupTopMargin()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 316
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isUsePick()Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$600(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$900(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewBubblePopupRightPickerMargin()I

    move-result v1

    :goto_1
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 319
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isEspressoEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 320
    const/16 v1, 0x3f

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 331
    :cond_1
    :goto_2
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 332
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 334
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->bringToFront()V

    .line 335
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 316
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$900(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewBubblePopupRigthMargin()I

    move-result v1

    goto :goto_1

    .line 321
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 322
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$900(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewBubblePopupRigthNoMenuMargin()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto :goto_2

    .line 323
    :cond_4
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 324
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->isHelpMenuAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isUsePick()Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$600(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->isConnectedDevice()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 326
    :cond_5
    const/16 v1, 0xf

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto :goto_2

    .line 328
    :cond_6
    const/16 v1, 0x97

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto :goto_2
.end method

.method private setTextOfPopup()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 344
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$300(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v1

    .line 346
    .local v1, "viewByType":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->isTextNotNeeded()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 347
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 374
    :goto_0
    return-void

    .line 348
    :cond_0
    sget-object v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 350
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$300(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    .line 351
    .local v0, "tagType":Lcom/sec/samsung/gallery/core/TabTagType;
    sget-object v2, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$2;->$SwitchMap$com$sec$samsung$gallery$core$TabTagType:[I

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/TabTagType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 365
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVZW_NoItemBubble:Z

    if-eqz v2, :cond_4

    .line 366
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    const v3, 0x7f0e0160

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->createTextWithCameraIcon(I)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 353
    :pswitch_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVZW_NoItemBubble:Z

    if-eqz v2, :cond_2

    .line 354
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    const v3, 0x7f0e0165

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->createTextWithCameraIcon(I)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 356
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    const v3, 0x7f0e0163

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 359
    :pswitch_1
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVZW_NoItemBubble:Z

    if-eqz v2, :cond_3

    .line 360
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    const v3, 0x7f0e0166

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->createTextWithCameraIcon(I)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 362
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    const v3, 0x7f0e0164

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 368
    :cond_4
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    const v3, 0x7f0e015d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 372
    .end local v0    # "tagType":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_5
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 351
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public isTextNotNeeded()Z
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isUsePick()Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$600(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isPersonPick()Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$400(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isContactPick()Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$500(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->isPhotoSignaturePick()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->isCameraButtonVisible()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeLayout()V
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mMainLayout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mMainLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 274
    :cond_0
    return-void
.end method

.method public setLayout()V
    .locals 5

    .prologue
    const/16 v4, 0xfa

    .line 277
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mMainLayout:Landroid/view/ViewGroup;

    const v1, 0x7f0f015f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupLayout:Landroid/view/View;

    .line 278
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupLayout:Landroid/view/View;

    if-nez v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mGallery:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03007c

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mMainLayout:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupLayout:Landroid/view/View;

    .line 281
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_4

    .line 282
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupLayout:Landroid/view/View;

    invoke-static {v4, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 286
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->setNoItemText()V

    .line 288
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_1

    .line 289
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->setNoItemBgParameter()V

    .line 290
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->setNoItemTextParameter()V

    .line 291
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->setNoItemImageParameter()V

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_2

    .line 294
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mMainLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 296
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # invokes: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isUsePick()Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$600(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEmptyScreenPopup:Z

    if-eqz v0, :cond_3

    .line 297
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->this$0:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    # getter for: Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCameraCmd;->isCameraAppEnabed(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 298
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->setPopupLayout()V

    .line 299
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->setTextOfPopup()V

    .line 307
    :cond_3
    :goto_1
    return-void

    .line 284
    :cond_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupLayout:Landroid/view/View;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 301
    :cond_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 302
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 303
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    goto :goto_1
.end method

.method public showText(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 209
    :goto_0
    return-void

    .line 204
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->isTextNotNeeded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->mPopupText:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
