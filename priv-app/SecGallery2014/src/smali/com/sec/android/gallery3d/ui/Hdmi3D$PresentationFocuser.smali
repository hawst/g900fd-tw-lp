.class Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;
.super Landroid/content/BroadcastReceiver;
.source "Hdmi3D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/Hdmi3D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PresentationFocuser"
.end annotation


# static fields
.field private static final ACTION_PRESENTATION_FOCUS_CHANGED:Ljava/lang/String; = "com.sec.android.app.PRESENTATION_FOCUS_CHANGED"

.field private static final KEY_APP_NAME:Ljava/lang/String; = "app_name"

.field private static final VALUE_GALLERY:Ljava/lang/String; = "gallery"


# instance fields
.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mIsRegistered:Z

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/Hdmi3D;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/ui/Hdmi3D;)V
    .locals 2

    .prologue
    .line 234
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->this$0:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 244
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.PRESENTATION_FOCUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->mIntentFilter:Landroid/content/IntentFilter;

    .line 247
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->mIsRegistered:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/ui/Hdmi3D;Lcom/sec/android/gallery3d/ui/Hdmi3D$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/ui/Hdmi3D;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/ui/Hdmi3D$1;

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;-><init>(Lcom/sec/android/gallery3d/ui/Hdmi3D;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 272
    const-string v1, "app_name"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 273
    .local v0, "appName":Ljava/lang/String;
    # getter for: Lcom/sec/android/gallery3d/ui/Hdmi3D;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "presentation focus changed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const-string v1, "gallery"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 275
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->this$0:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    # getter for: Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->access$400(Lcom/sec/android/gallery3d/ui/Hdmi3D;)Landroid/app/Presentation;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 276
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->this$0:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    # getter for: Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->access$400(Lcom/sec/android/gallery3d/ui/Hdmi3D;)Landroid/app/Presentation;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Presentation;->dismiss()V

    .line 279
    :cond_0
    return-void
.end method

.method public registerReceiver(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->mIsRegistered:Z

    if-nez v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 259
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->mIsRegistered:Z

    .line 261
    :cond_0
    return-void
.end method

.method public sendBroadcast(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 250
    # getter for: Lcom/sec/android/gallery3d/ui/Hdmi3D;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "send broadcast : gallery"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.PRESENTATION_FOCUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 252
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "app_name"

    const-string v2, "gallery"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 253
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 254
    return-void
.end method

.method public unRegisterReceiver(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->mIsRegistered:Z

    if-eqz v0, :cond_0

    .line 265
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 266
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->mIsRegistered:Z

    .line 268
    :cond_0
    return-void
.end method
