.class public Lcom/sec/android/gallery3d/settings/AccountSettingManager;
.super Ljava/lang/Object;
.source "AccountSettingManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;,
        Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AccountSettingManager"

.field private static accountsResult:[Landroid/accounts/Account;


# instance fields
.field private contextualTagSettingsFagmentDisplayed:Z

.field private mApplication:Landroid/app/Application;

.field private final mAuthAccounts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mIsCloudContentSyncOn:Z

.field private mIsDropboxSignIn:Z

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnAccountsUpdateListener:Landroid/accounts/OnAccountsUpdateListener;

.field private mSelectedPreferenceItem:I

.field private mStatusChangeListenerHandle:Ljava/lang/Object;

.field private mSyncStatusObserver:Landroid/content/SyncStatusObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->accountsResult:[Landroid/accounts/Account;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mAuthAccounts:Ljava/util/ArrayList;

    .line 28
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mHandler:Landroid/os/Handler;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mListeners:Ljava/util/List;

    .line 33
    iput v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mSelectedPreferenceItem:I

    .line 85
    new-instance v0, Lcom/sec/android/gallery3d/settings/AccountSettingManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager$2;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingManager;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mOnAccountsUpdateListener:Landroid/accounts/OnAccountsUpdateListener;

    .line 92
    new-instance v0, Lcom/sec/android/gallery3d/settings/AccountSettingManager$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager$3;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingManager;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mSyncStatusObserver:Landroid/content/SyncStatusObserver;

    .line 221
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->contextualTagSettingsFagmentDisplayed:Z

    .line 106
    iput-object p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mContext:Landroid/content/Context;

    .line 107
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->updateAuthAccounts()V

    .line 108
    return-void
.end method

.method static synthetic access$002([Landroid/accounts/Account;)[Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # [Landroid/accounts/Account;

    .prologue
    .line 23
    sput-object p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->accountsResult:[Landroid/accounts/Account;

    return-object p0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/settings/AccountSettingManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->updateAuthAccounts()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/settings/AccountSettingManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->notifySyncAccontsChanged()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/settings/AccountSettingManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private static getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v5, 0x0

    new-array v3, v5, [Landroid/accounts/Account;

    .line 57
    .local v3, "accounts":[Landroid/accounts/Account;
    new-instance v2, Ljava/lang/Thread;

    new-instance v5, Lcom/sec/android/gallery3d/settings/AccountSettingManager$1;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager$1;-><init>(Landroid/content/Context;)V

    const-string v6, "AccountSettingThread"

    invoke-direct {v2, v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 69
    .local v2, "accountThread":Ljava/lang/Thread;
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 70
    const-wide/16 v6, 0x1388

    invoke-virtual {v2, v6, v7}, Ljava/lang/Thread;->join(J)V

    .line 71
    invoke-virtual {v2}, Ljava/lang/Thread;->isAlive()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 72
    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 73
    const-string v5, "AccountSettingManager"

    const-string v6, "Account Thread Interrupt!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_0
    :goto_0
    return-object v3

    .line 75
    :cond_1
    sget-object v5, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->accountsResult:[Landroid/accounts/Account;

    if-eqz v5, :cond_0

    .line 76
    sget-object v5, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->accountsResult:[Landroid/accounts/Account;

    invoke-virtual {v5}, [Landroid/accounts/Account;->clone()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, [Landroid/accounts/Account;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 79
    :catch_0
    move-exception v4

    .line 80
    .local v4, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v4}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private getAuthorizedAccount([Landroid/accounts/Account;)Ljava/util/ArrayList;
    .locals 12
    .param p1, "accounts"    # [Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v10

    .line 116
    .local v10, "syncAdapters":[Landroid/content/SyncAdapterType;
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 118
    .local v1, "accountTypeToAuthorities":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    array-length v8, v10

    .local v8, "n":I
    :goto_0
    if-ge v5, v8, :cond_2

    .line 119
    aget-object v9, v10, v5

    .line 120
    .local v9, "sa":Landroid/content/SyncAdapterType;
    invoke-virtual {v9}, Landroid/content/SyncAdapterType;->isUserVisible()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 121
    iget-object v11, v9, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    invoke-virtual {v1, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 122
    .local v4, "authorities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v4, :cond_0

    .line 123
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "authorities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 124
    .restart local v4    # "authorities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v11, v9, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    invoke-virtual {v1, v11, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    :cond_0
    iget-object v11, v9, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    .end local v4    # "authorities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 130
    .end local v9    # "sa":Landroid/content/SyncAdapterType;
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 131
    .local v2, "authAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;>;"
    const/4 v5, 0x0

    :goto_1
    array-length v11, p1

    if-eq v5, v11, :cond_5

    .line 132
    aget-object v0, p1, v5

    .line 133
    .local v0, "account":Landroid/accounts/Account;
    const/4 v3, 0x0

    .line 134
    .local v3, "authToken":Ljava/lang/String;
    iget-object v11, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 135
    .restart local v4    # "authorities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v4, :cond_4

    .line 136
    const/4 v6, 0x0

    .local v6, "j":I
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    .local v7, "m":I
    :goto_2
    if-ge v6, v7, :cond_4

    .line 137
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "authToken":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 138
    .restart local v3    # "authToken":Ljava/lang/String;
    if-eqz v3, :cond_3

    const-string v11, "com.sec.android.gallery3d"

    invoke-virtual {v3, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 139
    new-instance v11, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;

    invoke-direct {v11, v3, v0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;-><init>(Ljava/lang/String;Landroid/accounts/Account;)V

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 131
    .end local v6    # "j":I
    .end local v7    # "m":I
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 145
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "authorities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    return-object v2
.end method

.method private notifyCloudStatusChanged()V
    .locals 4

    .prologue
    .line 157
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isDropBoxSignIn(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mIsDropboxSignIn:Z

    .line 158
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudContentSyncOn(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mIsCloudContentSyncOn:Z

    .line 159
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;

    .line 160
    .local v1, "listener":Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mIsDropboxSignIn:Z

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mIsCloudContentSyncOn:Z

    invoke-interface {v1, v2, v3}, Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;->onCloudStatusUpdated(ZZ)V

    goto :goto_0

    .line 162
    .end local v1    # "listener":Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;
    :cond_0
    return-void
.end method

.method private notifySyncAccontsChanged()V
    .locals 3

    .prologue
    .line 165
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;

    .line 166
    .local v1, "listener":Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mAuthAccounts:Ljava/util/ArrayList;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;->onSyncAccontsUpdated(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 168
    .end local v1    # "listener":Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;
    :cond_0
    return-void
.end method

.method private updateAuthAccounts()V
    .locals 2

    .prologue
    .line 199
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->getAuthorizedAccount([Landroid/accounts/Account;)Ljava/util/ArrayList;

    move-result-object v0

    .line 200
    .local v0, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;>;"
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mAuthAccounts:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 201
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mAuthAccounts:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 202
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->notifySyncAccontsChanged()V

    .line 203
    return-void
.end method


# virtual methods
.method public getAuthAccounts()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mAuthAccounts:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSelectedPreferenceItem()I
    .locals 1

    .prologue
    .line 214
    iget v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mSelectedPreferenceItem:I

    return v0
.end method

.method public hasHiddenItem()Z
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mApplication:Landroid/app/Application;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/HiddenSource;->hasHiddenMedias(Lcom/sec/android/gallery3d/app/GalleryApp;)Z

    move-result v0

    return v0
.end method

.method public isCloudContentSyncOn()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mIsCloudContentSyncOn:Z

    return v0
.end method

.method public isContextualTagSettingsFagmentDisplayed()Z
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->contextualTagSettingsFagmentDisplayed:Z

    return v0
.end method

.method public isDropboxSignIn()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mIsDropboxSignIn:Z

    return v0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mStatusChangeListenerHandle:Ljava/lang/Object;

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mOnAccountsUpdateListener:Landroid/accounts/OnAccountsUpdateListener;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    .line 173
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 176
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mSyncStatusObserver:Landroid/content/SyncStatusObserver;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mStatusChangeListenerHandle:Ljava/lang/Object;

    .line 181
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mOnAccountsUpdateListener:Landroid/accounts/OnAccountsUpdateListener;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->notifySyncAccontsChanged()V

    .line 187
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->notifyCloudStatusChanged()V

    .line 188
    return-void

    .line 182
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerOnAccountUpdatedListener(Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    return-void
.end method

.method public setApplication(Landroid/app/Application;)V
    .locals 0
    .param p1, "application"    # Landroid/app/Application;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mApplication:Landroid/app/Application;

    .line 211
    return-void
.end method

.method public setContextualTagSettingsFagmentDisplayed(Z)V
    .locals 0
    .param p1, "isDisplayed"    # Z

    .prologue
    .line 228
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->contextualTagSettingsFagmentDisplayed:Z

    .line 229
    return-void
.end method

.method public setSelectedPreferenceItem(I)V
    .locals 0
    .param p1, "selectedItem"    # I

    .prologue
    .line 218
    iput p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mSelectedPreferenceItem:I

    .line 219
    return-void
.end method

.method public unregisterOnAccountUpdatedListener(Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 196
    return-void
.end method
