.class public Lcom/sec/android/gallery3d/util/GalleryTimeUtils;
.super Ljava/lang/Object;
.source "GalleryTimeUtils.java"


# static fields
.field private static CURRENT_TIME:J = 0x0L

.field private static final DAY:J = 0x5265c00L

.field public static final DAY_MODE:I = 0x1

.field private static DAY_MODE_START_TIME:J = 0x0L

.field public static final MONTH_MODE:I = 0x3

.field private static MONTH_MODE_START_TIME:J = 0x0L

.field private static final TAG:Ljava/lang/String;

.field private static final THREE_WEEK:J = 0x6c258c00L

.field public static final TODAY_MODE:I = 0x0

.field private static TODAY_MODE_START_TIME:J = 0x0L

.field private static final WEEK:J = 0x240c8400L

.field public static final WEEK_MODE:I = 0x2

.field private static WEEK_MODE_START_TIME:J = 0x0L

.field public static final YEAR_MODE:I = 0x4

.field private static zoomMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 10
    const-class v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->TAG:Ljava/lang/String;

    .line 22
    sput-wide v2, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->TODAY_MODE_START_TIME:J

    .line 23
    sput-wide v2, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->DAY_MODE_START_TIME:J

    .line 24
    sput-wide v2, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->WEEK_MODE_START_TIME:J

    .line 25
    sput-wide v2, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->MONTH_MODE_START_TIME:J

    .line 26
    sput-wide v2, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->CURRENT_TIME:J

    .line 27
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->zoomMode:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTimeMode(J)I
    .locals 2
    .param p0, "time"    # J

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 61
    .local v0, "mode":I
    sget v1, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->zoomMode:I

    packed-switch v1, :pswitch_data_0

    .line 73
    :goto_0
    return v0

    .line 63
    :pswitch_0
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->getTimeModeInDefaultMode(J)I

    move-result v0

    .line 64
    goto :goto_0

    .line 66
    :pswitch_1
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->getTimeModeInMonthMode(J)I

    move-result v0

    .line 67
    goto :goto_0

    .line 69
    :pswitch_2
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->getTimeModeInYearMode(J)I

    move-result v0

    goto :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static getTimeModeInDefaultMode(J)I
    .locals 6
    .param p0, "time"    # J

    .prologue
    const/4 v0, 0x1

    .line 77
    sget-wide v2, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->CURRENT_TIME:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 78
    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->TAG:Ljava/lang/String;

    const-string v2, "Not updated time table"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 82
    :cond_1
    sget-wide v2, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->CURRENT_TIME:J

    cmp-long v1, v2, p0

    if-gez v1, :cond_2

    .line 83
    sget-wide p0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->CURRENT_TIME:J

    .line 86
    :cond_2
    sget-wide v2, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->TODAY_MODE_START_TIME:J

    cmp-long v1, p0, v2

    if-ltz v1, :cond_3

    .line 87
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    :cond_3
    sget-wide v2, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->DAY_MODE_START_TIME:J

    cmp-long v1, p0, v2

    if-gez v1, :cond_0

    .line 90
    sget-wide v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->WEEK_MODE_START_TIME:J

    cmp-long v0, p0, v0

    if-ltz v0, :cond_4

    .line 91
    const/4 v0, 0x2

    goto :goto_0

    .line 92
    :cond_4
    sget-wide v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->MONTH_MODE_START_TIME:J

    cmp-long v0, p0, v0

    if-ltz v0, :cond_5

    .line 93
    const/4 v0, 0x3

    goto :goto_0

    .line 95
    :cond_5
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private static getTimeModeInMonthMode(J)I
    .locals 1
    .param p0, "time"    # J

    .prologue
    .line 100
    const/4 v0, 0x3

    return v0
.end method

.method private static getTimeModeInYearMode(J)I
    .locals 1
    .param p0, "time"    # J

    .prologue
    .line 104
    const/4 v0, 0x4

    return v0
.end method

.method public static getZoomMode()I
    .locals 1

    .prologue
    .line 55
    sget v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->zoomMode:I

    return v0
.end method

.method public static setZoomMode(I)V
    .locals 3
    .param p0, "mode"    # I

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setZoomMode("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->zoomMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    sput p0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->zoomMode:I

    .line 52
    return-void
.end method

.method public static updateTimeTableWithLatestDay(J)V
    .locals 12
    .param p0, "latestTime"    # J

    .prologue
    const-wide/32 v10, 0x5265c00

    const/4 v1, 0x0

    .line 30
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 31
    .local v8, "date":Ljava/util/Calendar;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 32
    .local v9, "today":Ljava/util/Calendar;
    invoke-virtual {v8, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 33
    new-instance v0, Ljava/util/Date;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Ljava/util/Date;-><init>(IIIIII)V

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 34
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x2

    invoke-virtual {v8, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x5

    invoke-virtual {v8, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v9, v0, v1, v2}, Ljava/util/Calendar;->set(III)V

    .line 36
    sub-long v0, p0, v10

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    sput-wide v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->TODAY_MODE_START_TIME:J

    .line 37
    sput-wide p0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->CURRENT_TIME:J

    .line 39
    sget-wide v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->TODAY_MODE_START_TIME:J

    const-wide/32 v2, 0x240c8400

    sub-long/2addr v0, v2

    sput-wide v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->DAY_MODE_START_TIME:J

    .line 41
    sget-wide v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->TODAY_MODE_START_TIME:J

    const-wide/32 v2, 0x6c258c00

    sub-long/2addr v0, v2

    sput-wide v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->WEEK_MODE_START_TIME:J

    .line 43
    sget-wide v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->WEEK_MODE_START_TIME:J

    invoke-virtual {v8, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 44
    const/4 v0, 0x6

    invoke-virtual {v8, v0}, Ljava/util/Calendar;->get(I)I

    move-result v7

    .line 46
    .local v7, "currentDay":I
    sget-wide v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->WEEK_MODE_START_TIME:J

    int-to-long v2, v7

    mul-long/2addr v2, v10

    sub-long/2addr v0, v2

    sput-wide v0, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->MONTH_MODE_START_TIME:J

    .line 47
    return-void
.end method
