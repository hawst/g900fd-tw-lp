.class public Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "TCloudAlbumSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;
    }
.end annotation


# static fields
.field public static final PATH_ALL:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_MERGE_ALL:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_MERGE_IMAGE:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_MERGE_VIDEO:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mAlbums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field

.field private final mBaseUri:Landroid/net/Uri;

.field private final mName:Ljava/lang/String;

.field private final mNotifierImage:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private final mNotifierVideo:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private final mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    const-class v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->TAG:Ljava/lang/String;

    .line 44
    const-string v0, "/tCloud/all"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_ALL:Lcom/sec/android/gallery3d/data/Path;

    .line 45
    const-string v0, "/tCloud/image"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    .line 46
    const-string v0, "/tCloud/video"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/tCloud/mergedall/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->TCLOUD_MERGESET_BUCKETID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_MERGE_ALL:Lcom/sec/android/gallery3d/data/Path;

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/tCloud/mergedimage/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->TCLOUD_MERGESET_BUCKETID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_MERGE_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/tCloud/mergedvideo/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->TCLOUD_MERGESET_BUCKETID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_MERGE_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .line 67
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mType:I

    .line 69
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->TCLOUD_BUCKETS_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mBaseUri:Landroid/net/Uri;

    .line 70
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseImageUri:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mNotifierImage:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 71
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseVideoUri:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mNotifierVideo:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 72
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v0, :cond_0

    .line 73
    const-string v0, "T cloud"

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mName:Ljava/lang/String;

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0093

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mName:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getBucketIdString(Landroid/content/ContentResolver;ILcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)Ljava/lang/String;
    .locals 7
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "bucketId"    # I
    .param p2, "refer"    # Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .prologue
    .line 256
    iget-object v0, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->TCLOUD_BUCKETS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 257
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 259
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v2, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->PROJECTION_BUCKET:[Ljava/lang/String;

    const-string v3, "bucket_id = ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 264
    if-nez v6, :cond_1

    .line 265
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->TAG:Ljava/lang/String;

    const-string v2, "query fail: "

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    const-string v0, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    if-eqz v6, :cond_0

    .line 272
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v0

    .line 268
    :cond_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Files_BUCKET_ID:Ljava/lang/String;

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 271
    :goto_1
    if-eqz v6, :cond_0

    .line 272
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 268
    :cond_2
    :try_start_2
    const-string v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 271
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 272
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static getBucketName(Landroid/content/ContentResolver;ILcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)Ljava/lang/String;
    .locals 7
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "bucketId"    # I
    .param p2, "refer"    # Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .prologue
    .line 235
    iget-object v0, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->TCLOUD_BUCKETS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 237
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 239
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v2, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->PROJECTION_BUCKET:[Ljava/lang/String;

    const-string v3, "bucket_id = ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 243
    if-nez v6, :cond_0

    .line 244
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->TAG:Ljava/lang/String;

    const-string v2, "query fail: "

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    const-string v0, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_0
    return-object v0

    .line 247
    :cond_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Files_BUCKET_DISPLAY_NAME:Ljava/lang/String;

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 250
    :goto_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 247
    :cond_1
    :try_start_2
    const-string v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 250
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getTCloudAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 13
    .param p1, "manager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p2, "type"    # I
    .param p3, "parent"    # Lcom/sec/android/gallery3d/data/Path;
    .param p4, "id"    # I
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "bucketIdString"    # Ljava/lang/String;

    .prologue
    .line 160
    invoke-virtual/range {p3 .. p4}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 161
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v10

    .line 162
    .local v10, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v10, :cond_0

    .line 163
    check-cast v10, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v10    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    move-object v0, v10

    .line 172
    :goto_0
    return-object v0

    .line 165
    .restart local v10    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 182
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :pswitch_1
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v4, 0x1

    move/from16 v3, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 169
    :pswitch_2
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v4, 0x0

    move/from16 v3, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 171
    :pswitch_3
    sget-object v9, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 172
    .local v9, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;

    const/4 v2, 0x2

    new-array v11, v2, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v12, 0x0

    const/4 v4, 0x2

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v2, :cond_1

    sget-object v5, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_MERGE_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    :goto_1
    move-object v2, p0

    move-object v3, p1

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->getTCloudAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x1

    const/4 v4, 0x4

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v2, :cond_2

    sget-object v5, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_MERGE_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    :goto_2
    move-object v2, p0

    move-object v3, p1

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->getTCloudAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    aput-object v2, v11, v12

    invoke-direct {v0, v1, v9, v11}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    :cond_1
    sget-object v5, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_1

    :cond_2
    sget-object v5, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_2

    .line 165
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I
    .locals 5
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "name":[Ljava/lang/String;
    array-length v2, v0

    if-ge v2, v1, :cond_0

    .line 82
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 85
    :cond_0
    const-string v2, "all"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "mergedall"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 86
    :cond_1
    const/4 v1, 0x6

    .line 92
    :cond_2
    :goto_0
    return v1

    .line 88
    :cond_3
    const-string v2, "image"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "mergedimage"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 91
    const-string/jumbo v1, "video"

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "mergedvideo"

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 92
    :cond_4
    const/4 v1, 0x4

    goto :goto_0

    .line 94
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private loadBucketEntries(Landroid/database/Cursor;)[Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 212
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 214
    .local v3, "buffer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;>;"
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 215
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v5, v5, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Files_BUCKET_ID:Ljava/lang/String;

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 217
    .local v0, "bucketId":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v5, v5, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Files_BUCKET_DISPLAY_NAME:Ljava/lang/String;

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 220
    .local v2, "bucketName":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v5, v5, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Files_BUCKET_ID:Ljava/lang/String;

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 222
    .local v1, "bucketIdString":Ljava/lang/String;
    new-instance v4, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;

    invoke-direct {v4, v0, v2, v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 223
    .local v4, "entry":Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 224
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 228
    .end local v0    # "bucketId":I
    .end local v1    # "bucketIdString":Ljava/lang/String;
    .end local v2    # "bucketName":Ljava/lang/String;
    .end local v4    # "entry":Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;
    :catchall_0
    move-exception v5

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v5

    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 230
    sget-object v5, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "buffer.size:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;

    return-object v5
.end method


# virtual methods
.method public getBucketId()I
    .locals 2

    .prologue
    .line 346
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v0, :cond_0

    .line 347
    sget v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->TCLOUD_MERGESET_BUCKETID:I

    .line 351
    :goto_0
    return v0

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 349
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getBucketId()I

    move-result v0

    goto :goto_0

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 5
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 317
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v4, :cond_3

    .line 318
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 319
    .local v1, "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v3, 0x0

    .line 320
    .local v3, "totalItemCount":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 321
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    .line 322
    if-lt p1, v3, :cond_0

    .line 323
    sub-int/2addr p1, v3

    .line 324
    goto :goto_0

    .line 326
    :cond_0
    sub-int v4, v3, p1

    if-ge v4, p2, :cond_1

    .line 327
    sub-int v4, v3, p1

    invoke-virtual {v2, p1, v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 329
    sub-int v4, v3, p1

    sub-int/2addr p2, v4

    .line 330
    const/4 p1, 0x0

    goto :goto_0

    .line 332
    :cond_1
    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 338
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v3    # "totalItemCount":I
    :cond_2
    :goto_1
    return-object v1

    :cond_3
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1
.end method

.method public getMediaItemCount()I
    .locals 4

    .prologue
    .line 304
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v3, :cond_0

    .line 305
    const/4 v2, 0x0

    .line 306
    .local v2, "totalItemCount":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 307
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    add-int/2addr v2, v3

    .line 308
    goto :goto_0

    .line 311
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v2    # "totalItemCount":I
    :cond_0
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItemCount()I

    move-result v2

    :cond_1
    return v2
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 283
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v0, :cond_0

    .line 284
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->getSubMediaSetCount(Z)I

    move-result v0

    .line 286
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getSubMediaSetCount(Z)I
    .locals 1
    .param p1, "forMerge"    # Z

    .prologue
    .line 290
    if-eqz p1, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 293
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 113
    const-wide/16 v0, 0x400

    .line 114
    .local v0, "supported":J
    return-wide v0
.end method

.method public getTotalMediaItemCount()I
    .locals 4

    .prologue
    .line 104
    const/4 v0, 0x0

    .line 105
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 106
    .local v2, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    add-int/2addr v0, v3

    .line 107
    goto :goto_0

    .line 108
    .end local v2    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return v0
.end method

.method public isAlbumSetEmpty()Z
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 357
    const/4 v0, 0x0

    .line 358
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected loadSubMediaSets()Ljava/util/ArrayList;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mBaseUri:Landroid/net/Uri;

    .line 129
    .local v3, "uri":Landroid/net/Uri;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v11, "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    const/4 v13, 0x0

    .line 132
    .local v13, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v4, v4, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->PROJECTION_BUCKET:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 135
    if-nez v13, :cond_0

    .line 136
    sget-object v2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->TAG:Ljava/lang/String;

    const-string v4, "cannot open local database: "

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v11, v2

    .line 155
    .end local v11    # "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    :goto_0
    return-object v11

    .line 140
    .restart local v11    # "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->loadBucketEntries(Landroid/database/Cursor;)[Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;

    move-result-object v15

    .line 141
    .local v15, "entries":[Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v5

    .line 142
    .local v5, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    move-object v12, v15

    .local v12, "arr$":[Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;
    array-length v0, v12

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v18, 0x0

    .local v18, "i$":I
    :goto_1
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_1

    aget-object v16, v12, v18

    .line 143
    .local v16, "entry":Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mType:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v0, v16

    iget v8, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;->bucketId:I

    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;->bucketName:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v10, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;->bucketIdString:Ljava/lang/String;

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->getTCloudAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 146
    .end local v16    # "entry":Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;
    :cond_1
    const/16 v17, 0x0

    .local v17, "i":I
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v20

    .local v20, "n":I
    :goto_2
    move/from16 v0, v17

    move/from16 v1, v20

    if-ge v0, v1, :cond_2

    .line 147
    move/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 153
    :cond_2
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 149
    .end local v5    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v12    # "arr$":[Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;
    .end local v15    # "entries":[Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;
    .end local v17    # "i":I
    .end local v18    # "i$":I
    .end local v19    # "len$":I
    .end local v20    # "n":I
    :catch_0
    move-exception v14

    .line 150
    .local v14, "e":Ljava/lang/SecurityException;
    :try_start_2
    sget-object v2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->TAG:Ljava/lang/String;

    const-string v4, "Cloud DB Permission error"

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-virtual {v14}, Ljava/lang/SecurityException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 153
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v14    # "e":Ljava/lang/SecurityException;
    :catchall_0
    move-exception v2

    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mNotifierImage:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mNotifierVideo:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v1

    or-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->loadSubMediaSets()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mAlbums:Ljava/util/ArrayList;

    .line 122
    invoke-static {}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mDataVersion:J

    .line 124
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->mDataVersion:J

    return-wide v0
.end method
