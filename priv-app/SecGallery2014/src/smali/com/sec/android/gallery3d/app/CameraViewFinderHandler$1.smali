.class Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$1;
.super Landroid/os/Handler;
.source "CameraViewFinderHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$1;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 102
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 116
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 117
    return-void

    .line 104
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$1;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # invokes: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->updatePresentation()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$000(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)V

    goto :goto_0

    .line 107
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$1;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->startEVF()V

    goto :goto_0

    .line 110
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$1;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # invokes: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->stopEVFInternal()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$100(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)V

    goto :goto_0

    .line 113
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$1;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->setTvoutEVFStatus(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$200(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;Z)V

    goto :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
