.class public abstract Lcom/sec/android/gallery3d/remote/RemoteMediaSource;
.super Lcom/sec/android/gallery3d/data/MediaSource;
.source "RemoteMediaSource.java"


# instance fields
.field protected final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    .line 28
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/RemoteMediaSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 29
    return-void
.end method


# virtual methods
.method public abstract requestSyncAll()V
.end method
