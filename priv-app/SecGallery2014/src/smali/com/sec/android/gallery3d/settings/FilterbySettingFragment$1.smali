.class Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$1;
.super Ljava/lang/Object;
.source "FilterbySettingFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 56
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-ne v4, v2, :cond_2

    .line 57
    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v5

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 58
    .local v0, "item":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v4, v0, Landroid/preference/SwitchPreference;

    if-nez v4, :cond_1

    :cond_0
    move v2, v3

    .line 75
    .end local v0    # "item":Ljava/lang/Object;
    :goto_0
    return v2

    .restart local v0    # "item":Ljava/lang/Object;
    :cond_1
    move-object v1, v0

    .line 61
    check-cast v1, Landroid/preference/SwitchPreference;

    .line 62
    .local v1, "preference":Landroid/preference/SwitchPreference;
    packed-switch p2, :pswitch_data_0

    move v2, v3

    .line 72
    goto :goto_0

    .line 64
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->access$000(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 65
    invoke-virtual {v1, v3}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    goto :goto_0

    .line 68
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->access$000(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 69
    invoke-virtual {v1, v2}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    goto :goto_0

    .end local v0    # "item":Ljava/lang/Object;
    .end local v1    # "preference":Landroid/preference/SwitchPreference;
    :cond_2
    move v2, v3

    .line 75
    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
