.class public final Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;
.super Ljava/lang/Object;
.source "PicasaApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$Columns;,
        Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    }
.end annotation


# static fields
.field private static final BASE_QUERY_STRING:Ljava/lang/String;

.field private static final DEFAULT_BASE_URL:Ljava/lang/String; = "https://picasaweb.google.com/data/feed/api/"

.field private static final FULL_SIZE:Ljava/lang/String; = "d"

.field public static final RESULT_ERROR:I = 0x2

.field public static final RESULT_NOT_MODIFIED:I = 0x1

.field public static final RESULT_OK:I = 0x0

.field public static final SCREENNAILL_PATH_SEGMENT:Ljava/lang/String; = "/s640/"

.field private static final SCREENNAIL_SIZE:Ljava/lang/String; = "640u"

.field private static final SETTINGS_PICASA_GDATA_BASE_URL_KEY:Ljava/lang/String; = "picasa_gdata_base_url"

.field private static final TAG:Ljava/lang/String; = "PicasaAPI"

.field public static final THUBMNAIL_PATH_SEGMENT:Ljava/lang/String; = "/s200-c/"

.field private static final THUMBNAIL_SIZE:Ljava/lang/String; = "200c"

.field public static final THUMBNAIL_SIZE_THRESHOLD:I = 0xc8

.field private static accountsResult:[Landroid/accounts/Account;


# instance fields
.field private final mAlbumInstance:Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

.field private mAuth:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

.field private final mClient:Lcom/sec/android/gallery3d/remote/picasa/GDataClient;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mOperation:Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;

.field private final mParser:Lcom/sec/android/gallery3d/remote/picasa/GDataParser;

.field private final mPhotoInstance:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "?max-results=1000"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 68
    .local v0, "query":Ljava/lang/StringBuilder;
    const-string v1, "&imgmax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "d"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    const-string v1, "&thumbsize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "200c"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "640u"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    const-string v1, "&visibility=visible"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->BASE_QUERY_STRING:Ljava/lang/String;

    .line 95
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->accountsResult:[Landroid/accounts/Account;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mOperation:Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;

    .line 78
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mParser:Lcom/sec/android/gallery3d/remote/picasa/GDataParser;

    .line 79
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mAlbumInstance:Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

    .line 80
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mPhotoInstance:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .line 182
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mContentResolver:Landroid/content/ContentResolver;

    .line 183
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mClient:Lcom/sec/android/gallery3d/remote/picasa/GDataClient;

    .line 184
    return-void
.end method

.method public static canonicalizeUsername(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "username"    # Ljava/lang/String;

    .prologue
    .line 170
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    .line 171
    const-string v0, "@gmail."

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "@googlemail."

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    :cond_0
    const/4 v0, 0x0

    const/16 v1, 0x40

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 176
    :cond_1
    return-object p0
.end method

.method public static getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    const/4 v3, 0x0

    new-array v1, v3, [Landroid/accounts/Account;

    .line 102
    .local v1, "accounts":[Landroid/accounts/Account;
    :try_start_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 103
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.google"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    sput-object v3, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->accountsResult:[Landroid/accounts/Account;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    .end local v0    # "accountManager":Landroid/accounts/AccountManager;
    :goto_0
    sget-object v3, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->accountsResult:[Landroid/accounts/Account;

    if-eqz v3, :cond_0

    .line 108
    sget-object v3, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->accountsResult:[Landroid/accounts/Account;

    invoke-virtual {v3}, [Landroid/accounts/Account;->clone()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "accounts":[Landroid/accounts/Account;
    check-cast v1, [Landroid/accounts/Account;

    .line 111
    .restart local v1    # "accounts":[Landroid/accounts/Account;
    :cond_0
    return-object v1

    .line 104
    :catch_0
    move-exception v2

    .line 105
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getAuthenticatedAccounts(Landroid/content/Context;)[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 116
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    .line 117
    .local v2, "accounts":[Landroid/accounts/Account;
    if-nez v2, :cond_0

    .line 118
    const/4 v10, 0x0

    new-array v2, v10, [Landroid/accounts/Account;

    .line 119
    :cond_0
    array-length v8, v2

    .line 121
    .local v8, "numAccounts":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 122
    .local v3, "authAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-eq v7, v8, :cond_2

    .line 123
    aget-object v0, v2, v7

    .line 127
    .local v0, "account":Landroid/accounts/Account;
    :try_start_0
    const-string v10, "lh2"

    const/4 v11, 0x1

    invoke-virtual {v1, v0, v10, v11}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 140
    .local v5, "authToken":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 141
    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v10}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->canonicalizeUsername(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 142
    .local v9, "username":Ljava/lang/String;
    new-instance v10, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    invoke-direct {v10, v9, v5, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;)V

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 122
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v9    # "username":Ljava/lang/String;
    :cond_1
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 144
    :catch_0
    move-exception v6

    .line 145
    .local v6, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v6}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_1

    .line 146
    .end local v6    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v6

    .line 147
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 148
    .end local v6    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v6

    .line 149
    .local v6, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v6}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_1

    .line 150
    .end local v6    # "e":Landroid/accounts/AuthenticatorException;
    :catch_3
    move-exception v6

    .line 151
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 154
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v4, v10, [Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    .line 155
    .local v4, "authArray":[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 156
    return-object v4
.end method


# virtual methods
.method public deleteEntry(Ljava/lang/String;)I
    .locals 6
    .param p1, "editUri"    # Ljava/lang/String;

    .prologue
    .line 372
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mOperation:Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;

    monitor-enter v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 373
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mOperation:Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;

    .line 374
    .local v1, "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    .line 375
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mClient:Lcom/sec/android/gallery3d/remote/picasa/GDataClient;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 376
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mClient:Lcom/sec/android/gallery3d/remote/picasa/GDataClient;

    invoke-virtual {v2, p1, v1}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->delete(Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V

    .line 377
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 378
    :try_start_3
    iget v2, v1, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->outStatus:I

    const/16 v4, 0xc8

    if-ne v2, v4, :cond_0

    .line 379
    const/4 v2, 0x0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 387
    .end local v1    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    :goto_0
    return v2

    .line 377
    .restart local v1    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v2

    .line 383
    .end local v1    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v2
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    .line 384
    :catch_0
    move-exception v0

    .line 385
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "PicasaAPI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteEntry: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    const/4 v2, 0x2

    goto :goto_0

    .line 381
    .restart local v1    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    :cond_0
    :try_start_7
    const-string v2, "PicasaAPI"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteEntry: failed with status code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->outStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1
.end method

.method public getAlbumPhotos(Landroid/accounts/AccountManager;Landroid/content/SyncResult;Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;)I
    .locals 16
    .param p1, "accountManager"    # Landroid/accounts/AccountManager;
    .param p2, "syncResult"    # Landroid/content/SyncResult;
    .param p3, "album"    # Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;
    .param p4, "handler"    # Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;

    .prologue
    .line 283
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mContentResolver:Landroid/content/ContentResolver;

    const-string v10, "picasa_gdata_base_url"

    invoke-static {v9, v10}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 285
    .local v2, "baseUrl":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    if-eqz v2, :cond_1

    .end local v2    # "baseUrl":Ljava/lang/String;
    :goto_0
    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 286
    .local v3, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v9, "user/"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mAuth:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    iget-object v9, v9, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->user:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    const-string v9, "/albumid/"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    move-object/from16 v0, p3

    iget-wide v10, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->id:J

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 290
    sget-object v9, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->BASE_QUERY_STRING:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    const-string v9, "&kind=photo"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    :try_start_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mOperation:Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;

    monitor-enter v10
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_4

    .line 295
    :try_start_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mOperation:Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;

    .line 296
    .local v6, "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->photosEtag:Ljava/lang/String;

    iput-object v9, v6, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    .line 297
    const/4 v8, 0x0

    .line 298
    .local v8, "retry":Z
    const/4 v5, 0x1

    .line 300
    .local v5, "numRetries":I
    :cond_0
    const/4 v8, 0x0

    .line 301
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mClient:Lcom/sec/android/gallery3d/remote/picasa/GDataClient;

    monitor-enter v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 302
    :try_start_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mClient:Lcom/sec/android/gallery3d/remote/picasa/GDataClient;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12, v6}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->get(Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V

    .line 303
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 304
    :try_start_3
    iget v9, v6, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->outStatus:I

    sparse-switch v9, :sswitch_data_0

    .line 323
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    .line 324
    const/4 v9, 0x2

    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 367
    .end local v5    # "numRetries":I
    .end local v6    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .end local v8    # "retry":Z
    :goto_1
    return v9

    .line 285
    .end local v3    # "builder":Ljava/lang/StringBuilder;
    .restart local v2    # "baseUrl":Ljava/lang/String;
    :cond_1
    const-string v2, "https://picasaweb.google.com/data/feed/api/"

    goto :goto_0

    .line 303
    .end local v2    # "baseUrl":Ljava/lang/String;
    .restart local v3    # "builder":Ljava/lang/StringBuilder;
    .restart local v5    # "numRetries":I
    .restart local v6    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .restart local v8    # "retry":Z
    :catchall_0
    move-exception v9

    :try_start_4
    monitor-exit v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v9

    .line 356
    .end local v5    # "numRetries":I
    .end local v6    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .end local v8    # "retry":Z
    :catchall_1
    move-exception v9

    monitor-exit v10
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v9
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_6 .. :try_end_6} :catch_4

    .line 358
    :catch_0
    move-exception v4

    .line 359
    .local v4, "e":Ljava/io/IOException;
    const-string v9, "PicasaAPI"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getAlbumPhotos: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v9, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, v9, Landroid/content/SyncStats;->numIoExceptions:J

    .line 361
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 367
    .end local v4    # "e":Ljava/io/IOException;
    :goto_2
    const/4 v9, 0x2

    goto :goto_1

    .line 308
    .restart local v5    # "numRetries":I
    .restart local v6    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .restart local v8    # "retry":Z
    :sswitch_0
    const/4 v9, 0x1

    :try_start_7
    monitor-exit v10

    goto :goto_1

    .line 312
    :sswitch_1
    if-nez v8, :cond_2

    .line 313
    const/4 v8, 0x1

    .line 314
    const-string v9, "lh2"

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mAuth:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    iget-object v11, v11, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->authToken:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v11}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_2
    if-nez v5, :cond_3

    .line 317
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v9, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v9, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 326
    :cond_3
    :sswitch_2
    add-int/lit8 v5, v5, -0x1

    .line 327
    if-eqz v8, :cond_4

    if-gez v5, :cond_0

    .line 330
    :cond_4
    iget-object v9, v6, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    move-object/from16 v0, p3

    iput-object v9, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->photosEtag:Ljava/lang/String;

    .line 333
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mParser:Lcom/sec/android/gallery3d/remote/picasa/GDataParser;

    monitor-enter v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 334
    :try_start_8
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mParser:Lcom/sec/android/gallery3d/remote/picasa/GDataParser;

    .line 335
    .local v7, "parser":Lcom/sec/android/gallery3d/remote/picasa/GDataParser;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mPhotoInstance:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    invoke-virtual {v7, v9}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->setEntry(Lcom/sec/android/gallery3d/remote/picasa/Entry;)V

    .line 336
    move-object/from16 v0, p4

    invoke-virtual {v7, v0}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->setHandler(Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 338
    :try_start_9
    iget-object v9, v6, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    sget-object v12, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-static {v9, v12, v7}, Landroid/util/Xml;->parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V
    :try_end_9
    .catch Ljava/net/SocketException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/AssertionError; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 355
    :try_start_a
    monitor-exit v11
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 356
    :try_start_b
    monitor-exit v10
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 357
    const/4 v9, 0x0

    goto :goto_1

    .line 339
    :catch_1
    move-exception v4

    .line 340
    .local v4, "e":Ljava/net/SocketException;
    :try_start_c
    const-string v9, "PicasaAPI"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAlbumPhotos: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    .line 342
    invoke-virtual {v4}, Ljava/net/SocketException;->printStackTrace()V

    .line 343
    const/4 v9, 0x2

    monitor-exit v11
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :try_start_d
    monitor-exit v10
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto/16 :goto_1

    .line 344
    .end local v4    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v4

    .line 345
    .local v4, "e":Ljava/net/SocketTimeoutException;
    :try_start_e
    const-string v9, "PicasaAPI"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAlbumPhotos: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    .line 347
    invoke-virtual {v4}, Ljava/net/SocketTimeoutException;->printStackTrace()V

    .line 348
    const/4 v9, 0x2

    monitor-exit v11
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :try_start_f
    monitor-exit v10
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_1

    .line 349
    .end local v4    # "e":Ljava/net/SocketTimeoutException;
    :catch_3
    move-exception v4

    .line 350
    .local v4, "e":Ljava/lang/AssertionError;
    :try_start_10
    const-string v9, "PicasaAPI"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAlbumPhotos: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    .line 352
    invoke-virtual {v4}, Ljava/lang/AssertionError;->printStackTrace()V

    .line 353
    const/4 v9, 0x2

    monitor-exit v11
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    :try_start_11
    monitor-exit v10
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto/16 :goto_1

    .line 355
    .end local v4    # "e":Ljava/lang/AssertionError;
    .end local v7    # "parser":Lcom/sec/android/gallery3d/remote/picasa/GDataParser;
    :catchall_2
    move-exception v9

    :try_start_12
    monitor-exit v11
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    :try_start_13
    throw v9
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 362
    .end local v5    # "numRetries":I
    .end local v6    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .end local v8    # "retry":Z
    :catch_4
    move-exception v4

    .line 363
    .local v4, "e":Lorg/xml/sax/SAXException;
    const-string v9, "PicasaAPI"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getAlbumPhotos: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v9, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, v9, Landroid/content/SyncStats;->numParseExceptions:J

    .line 365
    invoke-virtual {v4}, Lorg/xml/sax/SAXException;->printStackTrace()V

    goto/16 :goto_2

    .line 304
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_2
        0x130 -> :sswitch_0
        0x191 -> :sswitch_1
        0x193 -> :sswitch_1
    .end sparse-switch
.end method

.method public getAlbums(Landroid/accounts/AccountManager;Landroid/content/SyncResult;Lcom/sec/android/gallery3d/remote/picasa/UserEntry;Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;)I
    .locals 16
    .param p1, "accountManager"    # Landroid/accounts/AccountManager;
    .param p2, "syncResult"    # Landroid/content/SyncResult;
    .param p3, "user"    # Lcom/sec/android/gallery3d/remote/picasa/UserEntry;
    .param p4, "handler"    # Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;

    .prologue
    .line 197
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mContentResolver:Landroid/content/ContentResolver;

    const-string v10, "picasa_gdata_base_url"

    invoke-static {v9, v10}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 199
    .local v2, "baseUrl":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    if-eqz v2, :cond_2

    .end local v2    # "baseUrl":Ljava/lang/String;
    :goto_0
    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 200
    .local v3, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v9, "user/"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mAuth:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    iget-object v9, v9, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->user:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    sget-object v9, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->BASE_QUERY_STRING:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    const-string v9, "&kind=album"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    :try_start_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mOperation:Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;

    monitor-enter v10
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_4

    .line 207
    :try_start_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mOperation:Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;

    .line 208
    .local v6, "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->albumsEtag:Ljava/lang/String;

    iput-object v9, v6, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    .line 209
    const/4 v8, 0x0

    .line 210
    .local v8, "retry":Z
    const/4 v5, 0x1

    .line 212
    .local v5, "numRetries":I
    :cond_0
    const/4 v8, 0x0

    .line 213
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mClient:Lcom/sec/android/gallery3d/remote/picasa/GDataClient;

    monitor-enter v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 214
    :try_start_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mClient:Lcom/sec/android/gallery3d/remote/picasa/GDataClient;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12, v6}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->get(Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V

    .line 215
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 216
    :try_start_3
    iget v9, v6, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->outStatus:I

    sparse-switch v9, :sswitch_data_0

    .line 234
    :cond_1
    :goto_1
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    .line 235
    const/4 v9, 0x2

    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 276
    .end local v5    # "numRetries":I
    .end local v6    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .end local v8    # "retry":Z
    :goto_2
    return v9

    .line 199
    .end local v3    # "builder":Ljava/lang/StringBuilder;
    .restart local v2    # "baseUrl":Ljava/lang/String;
    :cond_2
    const-string v2, "https://picasaweb.google.com/data/feed/api/"

    goto :goto_0

    .line 215
    .end local v2    # "baseUrl":Ljava/lang/String;
    .restart local v3    # "builder":Ljava/lang/StringBuilder;
    .restart local v5    # "numRetries":I
    .restart local v6    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .restart local v8    # "retry":Z
    :catchall_0
    move-exception v9

    :try_start_4
    monitor-exit v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v9

    .line 267
    .end local v5    # "numRetries":I
    .end local v6    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .end local v8    # "retry":Z
    :catchall_1
    move-exception v9

    monitor-exit v10
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v9
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_6 .. :try_end_6} :catch_4

    .line 269
    :catch_0
    move-exception v4

    .line 270
    .local v4, "e":Ljava/io/IOException;
    const-string v9, "PicasaAPI"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getAlbums: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v9, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, v9, Landroid/content/SyncStats;->numIoExceptions:J

    .line 276
    .end local v4    # "e":Ljava/io/IOException;
    :goto_3
    const/4 v9, 0x2

    goto :goto_2

    .line 220
    .restart local v5    # "numRetries":I
    .restart local v6    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .restart local v8    # "retry":Z
    :sswitch_0
    const/4 v9, 0x1

    :try_start_7
    monitor-exit v10

    goto :goto_2

    .line 223
    :sswitch_1
    if-nez v8, :cond_3

    .line 224
    const-string v9, "com.google"

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mAuth:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    iget-object v11, v11, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->authToken:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v11}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const/4 v8, 0x1

    .line 227
    :cond_3
    if-nez v5, :cond_1

    .line 228
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v9, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v9, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_1

    .line 237
    :sswitch_2
    add-int/lit8 v5, v5, -0x1

    .line 238
    if-eqz v8, :cond_4

    if-gez v5, :cond_0

    .line 241
    :cond_4
    iget-object v9, v6, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    move-object/from16 v0, p3

    iput-object v9, v0, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->albumsEtag:Ljava/lang/String;

    .line 244
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mParser:Lcom/sec/android/gallery3d/remote/picasa/GDataParser;

    monitor-enter v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 245
    :try_start_8
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mParser:Lcom/sec/android/gallery3d/remote/picasa/GDataParser;

    .line 246
    .local v7, "parser":Lcom/sec/android/gallery3d/remote/picasa/GDataParser;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mAlbumInstance:Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

    invoke-virtual {v7, v9}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->setEntry(Lcom/sec/android/gallery3d/remote/picasa/Entry;)V

    .line 247
    move-object/from16 v0, p4

    invoke-virtual {v7, v0}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->setHandler(Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 249
    :try_start_9
    iget-object v9, v6, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    sget-object v12, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-static {v9, v12, v7}, Landroid/util/Xml;->parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V
    :try_end_9
    .catch Ljava/net/SocketException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/AssertionError; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 266
    :try_start_a
    monitor-exit v11
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 267
    :try_start_b
    monitor-exit v10
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 268
    const/4 v9, 0x0

    goto :goto_2

    .line 250
    :catch_1
    move-exception v4

    .line 251
    .local v4, "e":Ljava/net/SocketException;
    :try_start_c
    const-string v9, "PicasaAPI"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAlbums SocketException: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    .line 253
    invoke-virtual {v4}, Ljava/net/SocketException;->printStackTrace()V

    .line 254
    const/4 v9, 0x2

    monitor-exit v11
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :try_start_d
    monitor-exit v10
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto/16 :goto_2

    .line 255
    .end local v4    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v4

    .line 256
    .local v4, "e":Ljava/net/SocketTimeoutException;
    :try_start_e
    const-string v9, "PicasaAPI"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAlbums SocketTimeoutException: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    .line 258
    invoke-virtual {v4}, Ljava/net/SocketTimeoutException;->printStackTrace()V

    .line 259
    const/4 v9, 0x2

    monitor-exit v11
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :try_start_f
    monitor-exit v10
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_2

    .line 260
    .end local v4    # "e":Ljava/net/SocketTimeoutException;
    :catch_3
    move-exception v4

    .line 261
    .local v4, "e":Ljava/lang/AssertionError;
    :try_start_10
    const-string v9, "PicasaAPI"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAlbums AssertionError: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v9, Landroid/content/SyncStats;->numIoExceptions:J

    .line 263
    invoke-virtual {v4}, Ljava/lang/AssertionError;->printStackTrace()V

    .line 264
    const/4 v9, 0x2

    monitor-exit v11
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    :try_start_11
    monitor-exit v10
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto/16 :goto_2

    .line 266
    .end local v4    # "e":Ljava/lang/AssertionError;
    .end local v7    # "parser":Lcom/sec/android/gallery3d/remote/picasa/GDataParser;
    :catchall_2
    move-exception v9

    :try_start_12
    monitor-exit v11
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    :try_start_13
    throw v9
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 272
    .end local v5    # "numRetries":I
    .end local v6    # "operation":Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .end local v8    # "retry":Z
    :catch_4
    move-exception v4

    .line 273
    .local v4, "e":Lorg/xml/sax/SAXException;
    const-string v9, "PicasaAPI"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getAlbums: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v9, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, v9, Landroid/content/SyncStats;->numParseExceptions:J

    goto/16 :goto_3

    .line 216
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_2
        0x130 -> :sswitch_0
        0x191 -> :sswitch_1
        0x193 -> :sswitch_1
    .end sparse-switch
.end method

.method public setAuth(Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;)V
    .locals 3
    .param p1, "auth"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mAuth:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    .line 188
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mClient:Lcom/sec/android/gallery3d/remote/picasa/GDataClient;

    monitor-enter v1

    .line 189
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->mClient:Lcom/sec/android/gallery3d/remote/picasa/GDataClient;

    iget-object v2, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->authToken:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->setAuthToken(Ljava/lang/String;)V

    .line 190
    monitor-exit v1

    .line 191
    return-void

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
