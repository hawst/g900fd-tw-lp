.class public Lcom/sec/android/gallery3d/ui/GestureRecognizer;
.super Ljava/lang/Object;
.source "GestureRecognizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/GestureRecognizer$1;,
        Lcom/sec/android/gallery3d/ui/GestureRecognizer$MyDownUpListener;,
        Lcom/sec/android/gallery3d/ui/GestureRecognizer$MyScaleListener;,
        Lcom/sec/android/gallery3d/ui/GestureRecognizer$MyGestureListener;,
        Lcom/sec/android/gallery3d/ui/GestureRecognizer$Listener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "GestureRecognizer"


# instance fields
.field private final ACCELERATION_RATIO:F

.field private final NANOS_TO_MS:I

.field private final PREDICTION_TIME:F

.field private mCurTime:J

.field private mCurXVel:F

.field private mCurYVel:F

.field private final mDownUpDetector:Lcom/sec/android/gallery3d/ui/DownUpDetector;

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mLastTime:J

.field private mLastXVel:F

.field private mLastYVel:F

.field private final mListener:Lcom/sec/android/gallery3d/ui/GestureRecognizer$Listener;

.field private final mScaleDetector:Landroid/view/ScaleGestureDetector;

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mViewH:I

.field private mViewW:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/GestureRecognizer$Listener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/gallery3d/ui/GestureRecognizer$Listener;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput v1, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mViewW:I

    iput v1, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mViewH:I

    .line 56
    iput v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mLastXVel:F

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mLastYVel:F

    .line 57
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mLastTime:J

    .line 58
    const v0, 0xf4240

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->NANOS_TO_MS:I

    .line 59
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->PREDICTION_TIME:F

    .line 60
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->ACCELERATION_RATIO:F

    .line 63
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mListener:Lcom/sec/android/gallery3d/ui/GestureRecognizer$Listener;

    .line 64
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/sec/android/gallery3d/ui/GestureRecognizer$MyGestureListener;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/gallery3d/ui/GestureRecognizer$MyGestureListener;-><init>(Lcom/sec/android/gallery3d/ui/GestureRecognizer;Lcom/sec/android/gallery3d/ui/GestureRecognizer$1;)V

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v3, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mGestureDetector:Landroid/view/GestureDetector;

    .line 66
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lcom/sec/android/gallery3d/ui/GestureRecognizer$MyScaleListener;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/gallery3d/ui/GestureRecognizer$MyScaleListener;-><init>(Lcom/sec/android/gallery3d/ui/GestureRecognizer;Lcom/sec/android/gallery3d/ui/GestureRecognizer$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 68
    new-instance v0, Lcom/sec/android/gallery3d/ui/DownUpDetector;

    new-instance v1, Lcom/sec/android/gallery3d/ui/GestureRecognizer$MyDownUpListener;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/gallery3d/ui/GestureRecognizer$MyDownUpListener;-><init>(Lcom/sec/android/gallery3d/ui/GestureRecognizer;Lcom/sec/android/gallery3d/ui/GestureRecognizer$1;)V

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/DownUpDetector;-><init>(Lcom/sec/android/gallery3d/ui/DownUpDetector$DownUpListener;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mDownUpDetector:Lcom/sec/android/gallery3d/ui/DownUpDetector;

    .line 69
    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/GestureRecognizer;)Lcom/sec/android/gallery3d/ui/GestureRecognizer$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/GestureRecognizer;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mListener:Lcom/sec/android/gallery3d/ui/GestureRecognizer$Listener;

    return-object v0
.end method

.method private onTouchEventTracker(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 96
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTouchPrediction:Z

    if-nez v0, :cond_0

    .line 130
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_1

    .line 101
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 104
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 106
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 107
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 110
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 112
    iget-wide v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mCurTime:J

    iput-wide v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mLastTime:J

    .line 113
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mCurTime:J

    .line 115
    iget v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mCurXVel:F

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mLastXVel:F

    .line 116
    iget v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mCurYVel:F

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mLastYVel:F

    .line 117
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mCurXVel:F

    .line 118
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mCurYVel:F

    .line 119
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->getPredictedX(F)F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    goto :goto_0

    .line 123
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 125
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mLastTime:J

    .line 126
    iput v2, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mLastXVel:F

    .line 127
    iput v2, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mLastYVel:F

    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public cancelScale()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 83
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 84
    .local v0, "now":J
    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 86
    .local v8, "cancelEvent":Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, v8}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 87
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    .line 88
    return-void
.end method

.method public getPredictedX(F)F
    .locals 8
    .param p1, "realX"    # F

    .prologue
    .line 133
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTouchPrediction:Z

    if-nez v4, :cond_0

    .line 143
    .end local p1    # "realX":F
    :goto_0
    return p1

    .line 137
    .restart local p1    # "realX":F
    :cond_0
    iget-wide v4, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mCurTime:J

    iget-wide v6, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mLastTime:J

    sub-long v2, v4, v6

    .line 138
    .local v2, "deltaTime":J
    iget v4, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mCurXVel:F

    iget v5, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mLastXVel:F

    sub-float/2addr v4, v5

    long-to-float v5, v2

    div-float v1, v4, v5

    .line 139
    .local v1, "curXAcceleration":F
    iget v4, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mCurXVel:F

    const/high16 v5, 0x3f800000    # 1.0f

    mul-float/2addr v5, v1

    add-float/2addr v4, v5

    const/high16 v5, 0x41200000    # 10.0f

    mul-float v0, v4, v5

    .line 143
    .local v0, "compensateX":F
    add-float/2addr p1, v0

    goto :goto_0
.end method

.method public isDown()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mDownUpDetector:Lcom/sec/android/gallery3d/ui/DownUpDetector;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/DownUpDetector;->isDown()Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->onTouchEventTracker(Landroid/view/MotionEvent;)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 75
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mDownUpDetector:Lcom/sec/android/gallery3d/ui/DownUpDetector;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/DownUpDetector;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 76
    return-void
.end method

.method public setViewSize(II)V
    .locals 0
    .param p1, "viewW"    # I
    .param p2, "viewH"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mViewW:I

    .line 92
    iput p2, p0, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->mViewH:I

    .line 93
    return-void
.end method
