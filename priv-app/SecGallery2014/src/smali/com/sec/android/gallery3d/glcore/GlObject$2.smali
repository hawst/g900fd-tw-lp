.class Lcom/sec/android/gallery3d/glcore/GlObject$2;
.super Ljava/lang/Object;
.source "GlObject.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field handled:Z

.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlObject;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 0

    .prologue
    .line 1851
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isReachedBoundary()I
    .locals 1

    .prologue
    .line 1912
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    if-eqz v0, :cond_0

    .line 1913
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;->isReachedBoundary()I

    move-result v0

    .line 1915
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMove(II)Z
    .locals 3
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 1864
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    .line 1865
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    if-eqz v0, :cond_0

    .line 1866
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;->onMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    .line 1868
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject;->onMoved(II)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    .line 1869
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    return v0
.end method

.method public onPenEnter(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1883
    const/4 v0, 0x0

    .line 1884
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    if-eqz v1, :cond_0

    .line 1885
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectDetect:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->getFirstPoint()Landroid/graphics/PointF;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;->onPenEnter(Landroid/graphics/PointF;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1887
    :cond_0
    return v0
.end method

.method public onPenMove(IIZ)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "listScrollActive"    # Z

    .prologue
    .line 1902
    const/4 v0, 0x0

    .line 1903
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    if-eqz v1, :cond_0

    .line 1904
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectDetect:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->getPenSelectRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-interface {v1, v2, p3}, Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;->onPenMove(Landroid/graphics/Rect;Z)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1907
    :cond_0
    return v0
.end method

.method public onPenSelect(II)Z
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1892
    const/4 v0, 0x0

    .line 1893
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    if-eqz v1, :cond_0

    .line 1894
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectDetect:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->getSecondPoint()Landroid/graphics/PointF;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectDetect:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->getPenSelectRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;->onPenSelect(Landroid/graphics/PointF;Landroid/graphics/Rect;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1897
    :cond_0
    return v0
.end method

.method public onPress(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1855
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    .line 1856
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    if-eqz v0, :cond_0

    .line 1857
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;->onPress(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    .line 1859
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject;->onPressed(II)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    .line 1860
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    return v0
.end method

.method public onRelease(IIII)Z
    .locals 7
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 1873
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    .line 1874
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    if-eqz v0, :cond_0

    .line 1875
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;->onRelease(Lcom/sec/android/gallery3d/glcore/GlObject;IIII)Z

    move-result v0

    or-int/2addr v0, v6

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    .line 1877
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlObject;->onReleased(IIII)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    .line 1878
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$2;->handled:Z

    return v0
.end method
