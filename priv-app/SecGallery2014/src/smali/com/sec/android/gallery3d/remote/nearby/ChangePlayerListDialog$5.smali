.class Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$5;
.super Ljava/lang/Object;
.source "ChangePlayerListDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)V
    .locals 0

    .prologue
    .line 269
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$5;->this$0:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 272
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$5;->this$0:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListView:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->access$700(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    .line 273
    .local v0, "headerViewCount":I
    if-lt p3, v0, :cond_0

    .line 274
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$5;->this$0:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->access$400(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 277
    :cond_0
    add-int/lit8 v1, v0, 0x1

    if-ge p3, v1, :cond_1

    .line 282
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$5;->this$0:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$5;->this$0:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->access$600(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    move-result-object v1

    sub-int v3, p3, v0

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->handleChangePlayer(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;)V
    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->access$800(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;)V

    goto :goto_0
.end method
