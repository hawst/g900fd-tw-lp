.class public Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "MagazineWidgetPreferenceActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;
    }
.end annotation


# static fields
.field private static final LAYOUT_POSITION:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field public static mIntervalItems:[Ljava/lang/CharSequence;


# instance fields
.field public mAppWidgetId:I

.field private mIntervalSelected:I

.field private mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaMountReceiver:Landroid/content/BroadcastReceiver;

.field private mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mIntervalSelected:I

    .line 38
    new-instance v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$1;-><init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;

    .line 53
    new-instance v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$2;-><init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mMediaMountReceiver:Landroid/content/BroadcastReceiver;

    .line 67
    new-instance v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$3;-><init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    .line 133
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mIntervalSelected:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mIntervalSelected:I

    return p1
.end method

.method private getMediaEjectFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 79
    return-object v0
.end method

.method private getMediaMountFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 64
    .local v0, "inFilter":Landroid/content/IntentFilter;
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 65
    return-object v0
.end method


# virtual methods
.method protected isValidFragment(Ljava/lang/String;)Z
    .locals 1
    .param p1, "fragmentName"    # Ljava/lang/String;

    .prologue
    .line 84
    const-class v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 130
    .local p1, "target":Ljava/util/List;, "Ljava/util/List<Landroid/preference/PreferenceActivity$Header;>;"
    const v0, 0x7f07001c

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->loadHeadersFromResource(ILjava/util/List;)V

    .line 131
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v9, 0x1000

    const/4 v5, 0x3

    const v8, 0x7f0e0138

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 91
    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->TAG:Ljava/lang/String;

    const-string v4, "onCreate!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_0

    .line 93
    const v3, 0x7f110039

    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->setTheme(I)V

    .line 95
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    const v4, 0x106000d

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setIcon(I)V

    .line 104
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 105
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "appWidgetId"

    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mAppWidgetId:I

    .line 107
    new-array v3, v5, [Ljava/lang/CharSequence;

    sput-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mIntervalItems:[Ljava/lang/CharSequence;

    .line 108
    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mIntervalItems:[Ljava/lang/CharSequence;

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0, v8, v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    .line 109
    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mIntervalItems:[Ljava/lang/CharSequence;

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0, v8, v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 110
    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mIntervalItems:[Ljava/lang/CharSequence;

    const/4 v4, 0x2

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v8, v5}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 112
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 113
    .local v1, "deleteFilter":Landroid/content/IntentFilter;
    const-string v3, "android.appwidget.action.APPWIDGET_DELETED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 114
    iget-object v3, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 115
    iget-object v3, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mMediaMountReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->getMediaMountFilter()Landroid/content/IntentFilter;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 116
    iget-object v3, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->getMediaEjectFilter()Landroid/content/IntentFilter;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 117
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 118
    return-void

    .line 98
    .end local v1    # "deleteFilter":Landroid/content/IntentFilter;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 99
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 100
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 101
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 102
    invoke-virtual {v0, v9, v9}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mMediaMountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 125
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 126
    return-void
.end method
