.class Lcom/sec/android/gallery3d/ui/PhotoView$10;
.super Ljava/lang/Object;
.source "PhotoView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;->createPointAnimationListener(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

.field final synthetic val$activity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0

    .prologue
    .line 4991
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->val$activity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 5
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 4995
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # operator++ for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10208(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    .line 4996
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # operator++ for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_2:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10308(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    .line 4997
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->val$activity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPanning(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4998
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    const v4, 0x7f0f01c2

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 4999
    .local v0, "tap":Landroid/widget/ImageView;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10200(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10400(Lcom/sec/android/gallery3d/ui/PhotoView;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 5000
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 5001
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10400(Lcom/sec/android/gallery3d/ui/PhotoView;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10200(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v4

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/animation/Animation;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 5025
    .end local v0    # "tap":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    return-void

    .line 5004
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->val$activity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpTilt(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 5005
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    const v4, 0x7f0f01d1

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 5006
    .local v1, "tap_1":Landroid/widget/ImageView;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    const v4, 0x7f0f01d2

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 5007
    .local v2, "tap_2":Landroid/widget/ImageView;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10200(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10400(Lcom/sec/android/gallery3d/ui/PhotoView;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 5008
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    .line 5009
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10400(Lcom/sec/android/gallery3d/ui/PhotoView;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10200(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v4

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/animation/Animation;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 5012
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_2:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10300(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10400(Lcom/sec/android/gallery3d/ui/PhotoView;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 5013
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 5014
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10400(Lcom/sec/android/gallery3d/ui/PhotoView;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_2:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10300(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v4

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/animation/Animation;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 5017
    .end local v1    # "tap_1":Landroid/widget/ImageView;
    .end local v2    # "tap_2":Landroid/widget/ImageView;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->val$activity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPeek(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5018
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    const v4, 0x7f0f01c9

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 5019
    .restart local v0    # "tap":Landroid/widget/ImageView;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10200(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10400(Lcom/sec/android/gallery3d/ui/PhotoView;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 5020
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 5021
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10400(Lcom/sec/android/gallery3d/ui/PhotoView;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$10;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10200(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v4

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/animation/Animation;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 5030
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 5035
    return-void
.end method
