.class Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;
.super Ljava/lang/Object;
.source "PhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/GestureRecognizer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyGestureListener"
.end annotation


# instance fields
.field private mAccScale:F

.field private mCanChangeMode:Z

.field private mDeltaY:I

.field private mDownInScrolling:Z

.field private mFirstScrollX:Z

.field private mHadFling:Z

.field private mIgnoreScalingGesture:Z

.field private mIgnoreSwipingGesture:Z

.field private mIgnoreUpEvent:Z

.field private mIgnoreZoomOutEffect:Z

.field private mModeChanged:Z

.field private mScrolledAfterDown:Z

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PhotoView;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 1

    .prologue
    .line 1738
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1739
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreUpEvent:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/ui/PhotoView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/ui/PhotoView$1;

    .prologue
    .line 1738
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    return-void
.end method

.method static synthetic access$10500(Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;
    .param p1, "x1"    # I

    .prologue
    .line 1738
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->deleteAfterAnimation(I)V

    return-void
.end method

.method private calculateDeltaY(F)I
    .locals 5
    .param p1, "delta"    # F

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 1988
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxDeletable:Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5900(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1989
    add-float v2, p1, v4

    float-to-int v2, v2

    .line 2000
    :goto_0
    return v2

    .line 1993
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v1

    .line 1994
    .local v1, "size":I
    const v2, 0x3e19999a    # 0.15f

    int-to-float v3, v1

    mul-float v0, v2, v3

    .line 1995
    .local v0, "maxScrollDistance":F
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    int-to-float v3, v1

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    .line 1996
    const/4 v2, 0x0

    cmpl-float v2, p1, v2

    if-lez v2, :cond_1

    move p1, v0

    .line 2000
    :goto_1
    add-float v2, p1, v4

    float-to-int v2, v2

    goto :goto_0

    .line 1996
    :cond_1
    neg-float p1, v0

    goto :goto_1

    .line 1998
    :cond_2
    int-to-float v2, v1

    div-float v2, p1, v2

    const v3, 0x3fc90fdb

    mul-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->sin(F)F

    move-result v2

    mul-float p1, v0, v2

    goto :goto_1
.end method

.method private checkIgnoreFlingEvent()Z
    .locals 4

    .prologue
    .line 2035
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mScaleEndStartTime:J
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6200(Lcom/sec/android/gallery3d/ui/PhotoView;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x12c

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 2036
    const/4 v0, 0x1

    .line 2038
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private deleteAfterAnimation(I)V
    .locals 6
    .param p1, "duration"    # I

    .prologue
    .line 2078
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 2079
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_0

    .line 2088
    :goto_0
    return-void

    .line 2081
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onCommitDeleteImage()V

    .line 2082
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getCurrentIndex()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v4

    add-int/2addr v3, v4

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoIndexHint:I
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6302(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 2083
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x4

    # |= operator for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$576(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 2084
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1300(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 2085
    .local v1, "m":Landroid/os/Message;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2086
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v2

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 2087
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1300(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v2

    int-to-long v4, p1

    invoke-virtual {v2, v1, v4, v5}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method private flingImages(FFF)Z
    .locals 12
    .param p1, "velocityX"    # F
    .param p2, "velocityY"    # F
    .param p3, "dY"    # F

    .prologue
    const v11, 0x7fffffff

    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2042
    add-float v7, p1, v10

    float-to-int v5, v7

    .line 2043
    .local v5, "vx":I
    add-float v7, p2, v10

    float-to-int v6, v7

    .line 2044
    .local v6, "vy":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2045
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Lcom/sec/android/gallery3d/ui/PositionController;->flingPage(II)Z

    move-result v9

    .line 2074
    :cond_0
    :goto_0
    return v9

    .line 2047
    :cond_1
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v7

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v7, v7, v10

    if-lez v7, :cond_2

    .line 2048
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/sec/android/gallery3d/ui/PositionController;->flingFilmX(I)Z

    move-result v9

    goto :goto_0

    .line 2052
    :cond_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v7

    if-eq v7, v11, :cond_0

    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxDeletable:Z
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5900(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2055
    const/16 v7, 0x9c4

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v4

    .line 2056
    .local v4, "maxVelocity":I
    const/16 v7, 0x1f4

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v2

    .line 2057
    .local v2, "escapeVelocity":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v7

    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v10}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v10

    invoke-virtual {v7, v10}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    .line 2059
    .local v0, "centerY":I
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-le v7, v2, :cond_6

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v7

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v10

    if-le v7, v10, :cond_6

    if-lez v6, :cond_4

    move v7, v8

    :goto_1
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    if-le v0, v10, :cond_5

    move v10, v8

    :goto_2
    if-ne v7, v10, :cond_6

    move v3, v8

    .line 2062
    .local v3, "fastEnough":Z
    :goto_3
    if-eqz v3, :cond_0

    .line 2063
    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 2064
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v7

    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v10}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v10

    invoke-virtual {v7, v10, v6}, Lcom/sec/android/gallery3d/ui/PositionController;->flingFilmY(II)I

    move-result v1

    .line 2065
    .local v1, "duration":I
    if-ltz v1, :cond_0

    .line 2066
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v7

    if-gez v6, :cond_3

    move v9, v8

    :cond_3
    invoke-virtual {v7, v9}, Lcom/sec/android/gallery3d/ui/PositionController;->setPopFromTop(Z)V

    .line 2067
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->deleteAfterAnimation(I)V

    .line 2070
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v7, v11}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5802(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    move v9, v8

    .line 2071
    goto/16 :goto_0

    .end local v1    # "duration":I
    .end local v3    # "fastEnough":Z
    :cond_4
    move v7, v9

    .line 2059
    goto :goto_1

    :cond_5
    move v10, v9

    goto :goto_2

    :cond_6
    move v3, v9

    goto :goto_3
.end method

.method private startExtraScalingIfNeeded()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2215
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCancelExtraScalingPending:Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1000(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2216
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1300(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v0

    const/4 v1, 0x2

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 2217
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->setExtraScalingRange(Z)V

    .line 2218
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCancelExtraScalingPending:Z
    invoke-static {v0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1002(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    .line 2220
    :cond_0
    return-void
.end method

.method private stopExtraScalingIfNeeded()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2223
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCancelExtraScalingPending:Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1000(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2224
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1300(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->removeMessages(I)V

    .line 2225
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->setExtraScalingRange(Z)V

    .line 2226
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCancelExtraScalingPending:Z
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1002(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    .line 2228
    :cond_0
    return-void
.end method


# virtual methods
.method public onDoubleTap(FF)Z
    .locals 13
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v11, 0x3f000000    # 0.5f

    const v10, 0x4013d70a    # 2.31f

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1863
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreSwipingGesture:Z

    if-eqz v2, :cond_0

    move v2, v3

    .line 1926
    :goto_0
    return v2

    .line 1865
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/RangeArray;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->isCamera()Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v4

    .line 1866
    goto :goto_0

    .line 1868
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->isHelpViewingScrollMode()Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4800(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v3

    .line 1869
    goto :goto_0

    .line 1871
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1872
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserDobuleTap()V

    .line 1875
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5000(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1900(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v2

    if-eq v2, v3, :cond_5

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1900(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v2

    const/4 v5, 0x3

    if-eq v2, v5, :cond_5

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isVideoPlayIcon()Z

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingProgressState:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3300(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v2

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_RUNNING:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3400()I

    move-result v5

    if-ne v2, v5, :cond_6

    :cond_5
    move v2, v3

    .line 1878
    goto :goto_0

    .line 1882
    :cond_6
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    .line 1883
    .local v0, "controller":Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v1

    .line 1886
    .local v1, "scale":F
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreUpEvent:Z

    .line 1888
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x1f4

    sub-long/2addr v6, v8

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPanningStartTime:J
    invoke-static {v2, v6, v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5102(Lcom/sec/android/gallery3d/ui/PhotoView;J)J

    .line 1891
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v2, :cond_7

    .line 1892
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreMenuItemPressed:Z
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4502(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    .line 1895
    :cond_7
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v2, :cond_8

    .line 1896
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreMenuItemPressed:Z
    invoke-static {v2, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4502(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    .line 1900
    :cond_8
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1902
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/anim/FloatAnimation;

    move-result-object v2

    if-eqz v2, :cond_9

    move v2, v3

    .line 1903
    goto/16 :goto_0

    .line 1906
    :cond_9
    cmpg-float v2, v1, v11

    if-gez v2, :cond_a

    .line 1907
    mul-float v2, v1, v10

    invoke-static {v11, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-virtual {v0, p1, p2, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->zoomIn(FFF)V

    .line 1915
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->setHelpTextAndState(IZ)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5300(Lcom/sec/android/gallery3d/ui/PhotoView;IZ)V

    .line 1916
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setImageChanging(Z)V

    .line 1917
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->onDoubleTapZoomBegin()V

    :goto_2
    move v2, v3

    .line 1926
    goto/16 :goto_0

    .line 1908
    :cond_a
    cmpg-float v2, v1, v12

    if-gez v2, :cond_b

    .line 1909
    mul-float v2, v1, v10

    invoke-static {v12, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-virtual {v0, p1, p2, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->zoomIn(FFF)V

    goto :goto_1

    .line 1911
    :cond_b
    const v2, 0x3fa66666    # 1.3f

    mul-float v5, v1, v10

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-virtual {v0, p1, p2, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->zoomIn(FFF)V

    goto :goto_1

    .line 1920
    :cond_c
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->resetToFullView()V

    .line 1922
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->setHelpTextAndState(IZ)V
    invoke-static {v2, v3, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5300(Lcom/sec/android/gallery3d/ui/PhotoView;IZ)V

    .line 1923
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->onDoubleTapZoomEnd()V

    goto :goto_2
.end method

.method public onDown(FF)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const-wide/16 v6, 0x0

    const v5, 0x7fffffff

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2232
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v2, 0x4

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->checkHideUndoBar(I)V
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1700(Lcom/sec/android/gallery3d/ui/PhotoView;I)V

    .line 2234
    iput v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mDeltaY:I

    .line 2235
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mModeChanged:Z

    .line 2237
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreSwipingGesture:Z

    if-eqz v1, :cond_1

    .line 2287
    :cond_0
    :goto_0
    return-void

    .line 2240
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # |= operator for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I
    invoke-static {v1, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$576(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 2242
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->isScrolling()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2243
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mDownInScrolling:Z

    .line 2244
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->stopScrolling()V

    .line 2248
    :goto_1
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mHadFling:Z

    .line 2249
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mScrolledAfterDown:Z

    .line 2250
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2251
    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v1, p1

    float-to-int v0, v1

    .line 2254
    .local v0, "xi":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->hitTest(II)I

    move-result v2

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5802(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 2256
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevBound:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1500(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v2

    if-lt v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mNextBound:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1400(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v2

    if-le v1, v2, :cond_6

    .line 2257
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v1, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5802(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 2268
    .end local v0    # "xi":I
    :goto_2
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v1, :cond_3

    .line 2269
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreMenuItemPressed:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4500(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2270
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->onItemPressed(FF)V

    .line 2275
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2276
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onDown()V

    .line 2279
    :cond_4
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreScroll:Z
    invoke-static {v1, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5702(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    .line 2280
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_NONE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/MotionDetector;->checkState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2281
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPanningStartTime:J
    invoke-static {v1, v6, v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5102(Lcom/sec/android/gallery3d/ui/PhotoView;J)J

    .line 2282
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPeekStartTime:J
    invoke-static {v1, v6, v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6602(Lcom/sec/android/gallery3d/ui/PhotoView;J)J

    .line 2283
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_MOVE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/MotionDetector;->setState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)V

    .line 2284
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/util/MotionDetector;->updateFocusPoint(FF)V

    goto/16 :goto_0

    .line 2246
    :cond_5
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mDownInScrolling:Z

    goto/16 :goto_1

    .line 2259
    .restart local v0    # "xi":I
    :cond_6
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/RangeArray;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->isDeletable()Z

    move-result v1

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxDeletable:Z
    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5902(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    goto/16 :goto_2

    .line 2263
    .end local v0    # "xi":I
    :cond_7
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v1, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5802(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    goto/16 :goto_2
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v5, 0x1

    .line 2006
    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onFling start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2007
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 2010
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    sub-float/2addr v3, v4

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->checkFlingCondition(FF)Z
    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5400(Lcom/sec/android/gallery3d/ui/PhotoView;FF)Z

    move-result v0

    .line 2012
    .local v0, "isFlingCondition":Z
    if-nez v0, :cond_1

    .line 2031
    .end local v0    # "isFlingCondition":Z
    :cond_0
    :goto_0
    return v5

    .line 2014
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setScrolling(Z)V

    .line 2017
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->checkFlingVelocity(FF)V

    .line 2020
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreSwipingGesture:Z

    if-nez v1, :cond_0

    .line 2021
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mModeChanged:Z

    if-nez v1, :cond_0

    .line 2022
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->checkIgnoreFlingEvent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2023
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->swipeImages(FF)Z
    invoke-static {v1, p3, p4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6100(Lcom/sec/android/gallery3d/ui/PhotoView;FF)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2024
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreUpEvent:Z

    .line 2030
    :goto_1
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mHadFling:Z

    goto :goto_0

    .line 2026
    :cond_2
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 2028
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-direct {p0, p3, p4, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->flingImages(FFF)Z

    goto :goto_1
.end method

.method public onLongPress(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v4, 0x0

    .line 2340
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMyGestureLongPressDetected:Z
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6802(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    .line 2342
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v2, :cond_0

    .line 2343
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2344
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->resetPressedIndex()V

    .line 2349
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    .line 2351
    .local v0, "controller":Lcom/sec/android/gallery3d/ui/PositionController;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v2

    sget-object v3, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_TILT:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/util/MotionDetector;->checkState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIsSelectionMode:Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6900(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2395
    :cond_3
    :goto_0
    return-void

    .line 2355
    :cond_4
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$7000(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inExpansionMode()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2360
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v2, :cond_3

    .line 2361
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->isLongPressFinished()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2364
    :cond_5
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$7100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$7100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getAgifMode()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$7100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2367
    :cond_6
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setMultiWindow(Landroid/content/Context;)V

    .line 2369
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMFDMenu:Z

    if-nez v2, :cond_3

    .line 2370
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 2371
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isBurstShotPlayIconPressed()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2372
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->setBurstShotPlayIconPressed(Z)V

    .line 2377
    :cond_7
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagAvailable(Landroid/content/Context;)Z

    move-result v1

    .line 2379
    .local v1, "faceTag":Z
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragNDropInMultiwindow:Z

    if-eqz v2, :cond_b

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v2, :cond_b

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2380
    if-eqz v1, :cond_a

    .line 2381
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mDragAndDropDialog:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$7200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->getValueOfDoNotShowAgain()Z

    move-result v2

    if-nez v2, :cond_9

    .line 2382
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    float-to-int v3, p1

    float-to-int v4, p2

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->showDragAndDropDialog(II)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$7300(Lcom/sec/android/gallery3d/ui/PhotoView;II)V

    goto/16 :goto_0

    .line 2373
    .end local v1    # "faceTag":Z
    :cond_8
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isBurstShotSettingIconPressed()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2374
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->setBurstShotSettingIconPressed(Z)V

    goto :goto_1

    .line 2383
    .restart local v1    # "faceTag":Z
    :cond_9
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2, p1, p2, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->checkManualFD(FFZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2384
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    float-to-int v3, p1

    float-to-int v4, p2

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->startManualFD(II)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$7400(Lcom/sec/android/gallery3d/ui/PhotoView;II)V

    goto/16 :goto_0

    .line 2387
    :cond_a
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->startDrag()V
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$7500(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    goto/16 :goto_0

    .line 2389
    :cond_b
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2, p1, p2, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->checkManualFD(FFZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2390
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    float-to-int v3, p1

    float-to-int v4, p2

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->startManualFD(II)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$7400(Lcom/sec/android/gallery3d/ui/PhotoView;II)V

    goto/16 :goto_0
.end method

.method public onScale(FFF)Z
    .locals 7
    .param p1, "focusX"    # F
    .param p2, "focusY"    # F
    .param p3, "scale"    # F

    .prologue
    const v6, 0x3f7d70a4    # 0.99f

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2124
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreSwipingGesture:Z

    if-eqz v2, :cond_1

    .line 2187
    :cond_0
    :goto_0
    return v3

    .line 2126
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreScalingGesture:Z

    if-nez v2, :cond_0

    .line 2128
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mModeChanged:Z

    if-nez v2, :cond_0

    .line 2130
    invoke-static {p3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p3}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v3, v4

    .line 2131
    goto :goto_0

    .line 2132
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isVideoPlayIcon()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2133
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2134
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mAccScale:F

    mul-float/2addr v2, p3

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mAccScale:F

    .line 2135
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mAccScale:F

    cmpg-float v2, v2, v6

    if-gtz v2, :cond_0

    .line 2136
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onScaleMin()Z

    goto :goto_0

    .line 2142
    :cond_4
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v2

    invoke-virtual {v2, p3, p1, p2}, Lcom/sec/android/gallery3d/ui/PositionController;->scaleBy(FFF)I

    move-result v1

    .line 2147
    .local v1, "outOfRange":I
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mAccScale:F

    mul-float/2addr v2, p3

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mAccScale:F

    .line 2148
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreZoomOutEffect:Z

    if-nez v2, :cond_5

    .line 2149
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mAccScale:F

    const v5, 0x3f99999a    # 1.2f

    cmpl-float v2, v2, v5

    if-lez v2, :cond_9

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreZoomOutEffect:Z

    .line 2150
    :cond_5
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mAccScale:F

    cmpg-float v2, v2, v6

    if-lez v2, :cond_6

    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mAccScale:F

    const v5, 0x3f8147ae    # 1.01f

    cmpl-float v2, v2, v5

    if-lez v2, :cond_a

    :cond_6
    move v0, v3

    .line 2153
    .local v0, "largeEnough":Z
    :goto_2
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mCanChangeMode:Z

    if-eqz v2, :cond_e

    if-eqz v0, :cond_e

    .line 2154
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreZoomOutEffect:Z

    if-nez v2, :cond_7

    if-gez v1, :cond_7

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_7
    if-lez v1, :cond_e

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 2155
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->stopExtraScalingIfNeeded()V

    .line 2159
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v5, -0x2

    # &= operator for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I
    invoke-static {v2, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$572(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 2160
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2161
    const-string v2, "PinchOut"

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/UsageStatistics;->setPendingTransitionCause(Ljava/lang/String;)V

    .line 2167
    :goto_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 2168
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onScaleMin()Z

    .line 2169
    const/4 v1, 0x0

    .line 2176
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->onScaleEnd()V

    .line 2177
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mModeChanged:Z

    goto/16 :goto_0

    .end local v0    # "largeEnough":Z
    :cond_9
    move v2, v4

    .line 2149
    goto :goto_1

    :cond_a
    move v0, v4

    .line 2150
    goto :goto_2

    .line 2164
    .restart local v0    # "largeEnough":Z
    :cond_b
    const-string v2, "PinchIn"

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/UsageStatistics;->setPendingTransitionCause(Ljava/lang/String;)V

    goto :goto_3

    .line 2171
    :cond_c
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v5

    if-nez v5, :cond_d

    move v4, v3

    :cond_d
    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setFilmMode(Z)V

    goto :goto_4

    .line 2182
    :cond_e
    if-eqz v1, :cond_f

    .line 2183
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->startExtraScalingIfNeeded()V

    goto/16 :goto_0

    .line 2185
    :cond_f
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->stopExtraScalingIfNeeded()V

    goto/16 :goto_0
.end method

.method public onScaleBegin(FF)Z
    .locals 5
    .param p1, "focusX"    # F
    .param p2, "focusY"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2093
    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6000()Ljava/lang/String;

    move-result-object v1

    const-string v4, "onScaleBegin start"

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2094
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6400(Lcom/sec/android/gallery3d/ui/PhotoView;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCoverMode(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2095
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mModeChanged:Z

    .line 2097
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreZoomOutEffect:Z

    .line 2098
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreSwipingGesture:Z

    if-eqz v1, :cond_2

    .line 2119
    :cond_1
    :goto_0
    return v3

    .line 2101
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/RangeArray;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->isCamera()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreScalingGesture:Z

    .line 2102
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreScalingGesture:Z

    if-nez v1, :cond_1

    .line 2105
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/ui/PositionController;->beginScale(FF)V

    .line 2108
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setImageChanging(Z)V

    .line 2109
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v4

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->onScaleBegin(F)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2116
    :goto_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    move v1, v3

    :goto_2
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mCanChangeMode:Z

    .line 2118
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mAccScale:F

    goto :goto_0

    .line 2110
    :catch_0
    move-exception v0

    .line 2111
    .local v0, "Ne":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .end local v0    # "Ne":Ljava/lang/NullPointerException;
    :cond_4
    move v1, v2

    .line 2116
    goto :goto_2
.end method

.method public onScaleEnd()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2192
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreSwipingGesture:Z

    if-eqz v0, :cond_1

    .line 2212
    :cond_0
    :goto_0
    return-void

    .line 2194
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreScalingGesture:Z

    if-nez v0, :cond_0

    .line 2196
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mModeChanged:Z

    if-nez v0, :cond_0

    .line 2198
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->endScale()V

    .line 2199
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGestureRecognizer:Lcom/sec/android/gallery3d/ui/GestureRecognizer;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$800(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/GestureRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->isDown()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2200
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreScroll:Z
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5702(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    .line 2203
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->onScaleEnd()V

    .line 2205
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->getMinScale()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_3

    .line 2206
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->setHelpTextAndState(IZ)V
    invoke-static {v0, v2, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5300(Lcom/sec/android/gallery3d/ui/PhotoView;IZ)V

    .line 2210
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mScaleEndStartTime:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6202(Lcom/sec/android/gallery3d/ui/PhotoView;J)J

    .line 2211
    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onScaleEnd end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2208
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->setHelpTextAndState(IZ)V
    invoke-static {v0, v2, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5300(Lcom/sec/android/gallery3d/ui/PhotoView;IZ)V

    goto :goto_1
.end method

.method public onScroll(FFFF)Z
    .locals 8
    .param p1, "dx"    # F
    .param p2, "dy"    # F
    .param p3, "totalX"    # F
    .param p4, "totalY"    # F

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1933
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreSwipingGesture:Z

    if-eqz v4, :cond_1

    .line 1984
    :cond_0
    :goto_0
    return v5

    .line 1936
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->checkFlingCondition(FF)Z
    invoke-static {v4, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5400(Lcom/sec/android/gallery3d/ui/PhotoView;FF)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1938
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpViewingMode:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5500(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMultiTouch:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5600(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1940
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5000(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v4

    if-nez v4, :cond_0

    .line 1942
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreScroll:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1946
    :cond_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setScrolling(Z)V

    .line 1948
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v4, :cond_4

    .line 1949
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 1950
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->resetPressedIndex()V

    .line 1951
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->hidePopupMenu()V

    .line 1956
    :cond_4
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mScrolledAfterDown:Z

    if-nez v4, :cond_5

    .line 1957
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mScrolledAfterDown:Z

    .line 1958
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v7

    cmpl-float v4, v4, v7

    if-lez v4, :cond_7

    move v4, v5

    :goto_1
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mFirstScrollX:Z

    .line 1961
    :cond_5
    neg-float v4, p1

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1962
    .local v1, "dxi":I
    neg-float v4, p2

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1963
    .local v2, "dyi":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1964
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mFirstScrollX:Z

    if-eqz v4, :cond_8

    .line 1965
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->scrollFilmX(I)V

    .line 1981
    :cond_6
    :goto_2
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreUpEvent:Z

    goto/16 :goto_0

    .end local v1    # "dxi":I
    .end local v2    # "dyi":I
    :cond_7
    move v4, v6

    .line 1958
    goto :goto_1

    .line 1967
    .restart local v1    # "dxi":I
    .restart local v2    # "dyi":I
    :cond_8
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v4

    const v7, 0x7fffffff

    if-eq v4, v7, :cond_0

    .line 1969
    invoke-direct {p0, p4}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->calculateDeltaY(F)I

    move-result v3

    .line 1970
    .local v3, "newDeltaY":I
    iget v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mDeltaY:I

    sub-int v0, v3, v4

    .line 1971
    .local v0, "d":I
    if-eqz v0, :cond_6

    .line 1972
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v4

    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v7

    invoke-virtual {v4, v7, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->scrollFilmY(II)V

    .line 1973
    iput v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mDeltaY:I

    goto :goto_2

    .line 1977
    .end local v0    # "d":I
    .end local v3    # "newDeltaY":I
    :cond_9
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->scrollPage(II)V

    goto :goto_2
.end method

.method public onSingleTapUp(FF)Z
    .locals 12
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 1774
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0xe

    if-ge v8, v9, :cond_0

    .line 1775
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$500(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v8

    and-int/lit8 v8, v8, 0x1

    if-nez v8, :cond_0

    .line 1776
    const/4 v8, 0x1

    .line 1858
    :goto_0
    return v8

    .line 1782
    :cond_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v9, -0x2

    # &= operator for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I
    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$572(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 1784
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mDownInScrolling:Z

    if-nez v8, :cond_2

    .line 1785
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/high16 v9, 0x3f000000    # 0.5f

    add-float/2addr v9, p1

    float-to-int v9, v9

    const/high16 v10, 0x3f000000    # 0.5f

    add-float/2addr v10, p2

    float-to-int v10, v10

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->switchToHitPicture(II)V
    invoke-static {v8, v9, v10}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4000(Lcom/sec/android/gallery3d/ui/PhotoView;II)V

    .line 1790
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 1791
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const-wide/16 v6, 0x0

    .line 1792
    .local v6, "supported":J
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v6

    .line 1793
    :cond_1
    const-wide/16 v8, 0x4000

    and-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_2

    .line 1794
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->setFilmMode(Z)V

    .line 1795
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreUpEvent:Z

    .line 1796
    const/4 v8, 0x1

    goto :goto_0

    .line 1801
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v6    # "supported":J
    :cond_2
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/gallery3d/ui/GLRoot;->getCompensationMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    .line 1803
    .local v2, "m":Landroid/graphics/Matrix;
    if-nez v2, :cond_3

    .line 1804
    const/4 v8, 0x1

    goto :goto_0

    .line 1807
    :cond_3
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1808
    .local v0, "inv":Landroid/graphics/Matrix;
    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1809
    const/4 v8, 0x2

    new-array v5, v8, [F

    const/4 v8, 0x0

    aput p1, v5, v8

    const/4 v8, 0x1

    aput p2, v5, v8

    .line 1812
    .local v5, "pts":[F
    invoke-virtual {v0, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1813
    const/4 v8, 0x0

    aget v8, v5, v8

    const/high16 v9, 0x3f000000    # 0.5f

    add-float/2addr v8, v9

    float-to-int v3, v8

    .line 1814
    .local v3, "pX":I
    const/4 v8, 0x1

    aget v8, v5, v8

    const/high16 v9, 0x3f000000    # 0.5f

    add-float/2addr v8, v9

    float-to-int v4, v8

    .line 1817
    .local v4, "pY":I
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v8, :cond_6

    .line 1818
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 1819
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->singleTapUp(II)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1821
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFirstFaceTagDialog:Z
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4200(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1822
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->startFaceTagDialog()V
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4300(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    .line 1827
    :cond_4
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIsShowBars:Z
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4400(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1834
    :cond_5
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 1842
    :cond_6
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v8, :cond_7

    .line 1843
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v8, v8, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreMenuItemPressed:Z
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4500(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 1844
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v8

    const-class v9, Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/app/StateManager;->hasStateClass(Ljava/lang/Class;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 1845
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v8, v8, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v8, p1, p2}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->onItemPressed(FF)V

    .line 1851
    :cond_7
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v8

    if-eqz v8, :cond_8

    .line 1852
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v8

    invoke-interface {v8, v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onSingleTapUp(II)V

    .line 1855
    :cond_8
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4600(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SpenOcrView;

    move-result-object v8

    if-eqz v8, :cond_9

    .line 1856
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4600(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SpenOcrView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->deletePopup()V

    .line 1858
    :cond_9
    const/4 v8, 0x1

    goto/16 :goto_0
.end method

.method public onUp()V
    .locals 8

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    .line 2292
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setScrolling(Z)V

    .line 2294
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreSwipingGesture:Z

    if-eqz v3, :cond_0

    .line 2331
    :goto_0
    return-void

    .line 2297
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v5, -0x2

    # &= operator for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I
    invoke-static {v3, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$572(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 2298
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$600(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/EdgeView;->onRelease()V

    .line 2302
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mScrolledAfterDown:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mFirstScrollX:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v3

    const v5, 0x7fffffff

    if-eq v3, v5, :cond_1

    .line 2304
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v2

    .line 2305
    .local v2, "r":Landroid/graphics/Rect;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v1

    .line 2306
    .local v1, "h":I
    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    int-to-float v5, v1

    mul-float/2addr v5, v7

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const v5, 0x3ecccccd    # 0.4f

    int-to-float v6, v1

    mul-float/2addr v5, v6

    cmpl-float v3, v3, v5

    if-lez v3, :cond_1

    .line 2307
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v5

    invoke-virtual {v3, v5, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->flingFilmY(II)I

    move-result v0

    .line 2308
    .local v0, "duration":I
    if-ltz v0, :cond_1

    .line 2309
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v5

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    int-to-float v6, v1

    mul-float/2addr v6, v7

    cmpg-float v3, v3, v6

    if-gez v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v5, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->setPopFromTop(Z)V

    .line 2310
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->deleteAfterAnimation(I)V

    .line 2315
    .end local v0    # "duration":I
    .end local v1    # "h":I
    .end local v2    # "r":Landroid/graphics/Rect;
    :cond_1
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v3, :cond_2

    .line 2316
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreMenuItemPressed:Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4500(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2317
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->resetPressedIndex()V

    .line 2320
    :cond_2
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreUpEvent:Z

    if-eqz v3, :cond_4

    .line 2321
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreUpEvent:Z

    goto/16 :goto_0

    .restart local v0    # "duration":I
    .restart local v1    # "h":I
    .restart local v2    # "r":Landroid/graphics/Rect;
    :cond_3
    move v3, v4

    .line 2309
    goto :goto_1

    .line 2325
    .end local v0    # "duration":I
    .end local v1    # "h":I
    .end local v2    # "r":Landroid/graphics/Rect;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mHadFling:Z

    if-nez v3, :cond_5

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mFirstScrollX:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->snapToNeighborImage()Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 2327
    :cond_5
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->snapback()V
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1600(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    .line 2330
    :cond_6
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreScroll:Z
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5702(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    goto/16 :goto_0
.end method

.method public setSwipingEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 2334
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->mIgnoreSwipingGesture:Z

    .line 2335
    return-void

    .line 2334
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
