.class public Lcom/sec/android/gallery3d/gadget/WidgetConfigure;
.super Landroid/app/Activity;
.source "WidgetConfigure.java"


# static fields
.field private static final KEY_PICKED_ITEM:Ljava/lang/String; = "picked-item"

.field public static final KEY_WIDGET_TYPE:Ljava/lang/String; = "widget-type"

.field private static MAX_WIDGET_SIDE:I = 0x0

.field private static final REQUEST_CHOOSE_ALBUM:I = 0x1

.field private static final REQUEST_CHOOSE_PERSON:I = 0x3

.field private static final REQUEST_CROP_IMAGE:I = 0x6

.field private static final REQUEST_GET_PHOTO:I = 0x5

.field private static final REQUEST_GET_PHOTOS:I = 0x0

.field private static final REQUEST_SHUFFLE_ALL:I = 0x2

.field private static final REQUEST_WIDGET_TYPE:I = 0x4

.field public static final RESULT_ERROR:I = 0x1

.field private static final TAG:Ljava/lang/String; = "WidgetConfigure"

.field private static WIDGET_SCALE_FACTOR:F


# instance fields
.field private mAppWidgetId:I

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mMenuList:[Ljava/lang/String;

.field private mPickedItem:Landroid/net/Uri;

.field private mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const/high16 v0, 0x3fc00000    # 1.5f

    sput v0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->WIDGET_SCALE_FACTOR:F

    .line 77
    const/16 v0, 0x168

    sput v0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->MAX_WIDGET_SIDE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 270
    new-instance v0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure$1;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetConfigure;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;

    .line 283
    new-instance v0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure$2;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetConfigure;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/gadget/WidgetConfigure;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetConfigure;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/gadget/WidgetConfigure;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetConfigure;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->setWidgetType(I)V

    return-void
.end method

.method private setChoosenAlbum(Landroid/content/Intent;)V
    .locals 4
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 234
    const/4 v0, 0x0

    .line 235
    .local v0, "albumPath":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 236
    const-string v3, "ALBUM_PATH"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 239
    :cond_0
    new-instance v2, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 240
    .local v2, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v1

    .line 241
    .local v1, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v3, 0x2

    iput v3, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 242
    iput-object v0, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    .line 261
    :try_start_0
    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 262
    iget v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 267
    return-void

    .line 265
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v3
.end method

.method private setChoosenPerson(Landroid/content/Intent;)V
    .locals 13
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 388
    const/4 v7, 0x0

    .line 389
    .local v7, "personIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 390
    .local v9, "resolver":Landroid/content/ContentResolver;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 391
    .local v2, "filePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v8, 0x0

    .line 392
    .local v8, "personIdPath":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v10}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v5

    .line 394
    .local v5, "mDataManager":Lcom/sec/android/gallery3d/data/DataManager;
    if-eqz p1, :cond_1

    .line 395
    const-string v10, "personIds"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getIntegerArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 397
    if-nez v7, :cond_2

    .line 398
    :try_start_0
    const-string v10, "WidgetConfigure"

    const-string v11, "personIdList is null! setChoosenPerson()"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 414
    :cond_0
    :goto_0
    iget v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-static {v5, v2}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->convertPathToFilePath(Lcom/sec/android/gallery3d/data/DataManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-static {p0, v10, v11}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->saveWidgetPaths(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 417
    new-instance v3, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 418
    .local v3, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v3, v10}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v1

    .line 419
    .local v1, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v10, 0x4

    iput v10, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 420
    iput-object v8, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    .line 423
    :try_start_1
    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 424
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 426
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 429
    .end local v1    # "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    .end local v3    # "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    :cond_1
    return-void

    .line 400
    :cond_2
    :try_start_2
    invoke-static {v9, v7}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getPersonImagePath(Landroid/content/ContentResolver;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 402
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 403
    .local v6, "personId":I
    if-nez v8, :cond_3

    .line 404
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    .line 406
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v8

    goto :goto_1

    .line 410
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "personId":I
    :catch_0
    move-exception v0

    .line 411
    .local v0, "e":Ljava/lang/Exception;
    const-string v10, "WidgetConfigure"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "showWidgetTypeDialog() exception: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 426
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    .restart local v3    # "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    :catchall_0
    move-exception v10

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v10
.end method

.method private setChoosenPhoto(Landroid/content/Intent;)V
    .locals 12
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x1

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 203
    .local v3, "res":Landroid/content/res/Resources;
    const v8, 0x7f0d0060

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    .line 204
    .local v7, "width":F
    const v8, 0x7f0d0061

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 208
    .local v1, "height":F
    sget v8, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->WIDGET_SCALE_FACTOR:F

    sget v9, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->MAX_WIDGET_SIDE:I

    int-to-float v9, v9

    invoke-static {v7, v1}, Ljava/lang/Math;->max(FF)F

    move-result v10

    div-float/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 211
    .local v4, "scale":F
    mul-float v8, v7, v4

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 212
    .local v6, "widgetWidth":I
    mul-float v8, v1, v4

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 214
    .local v5, "widgetHeight":I
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mPickedItem:Landroid/net/Uri;

    .line 215
    new-instance v8, Landroid/content/Intent;

    const-string v9, "com.android.camera.action.CROP"

    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mPickedItem:Landroid/net/Uri;

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v9, "outputX"

    invoke-virtual {v8, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "outputY"

    invoke-virtual {v8, v9, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "aspectX"

    invoke-virtual {v8, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "aspectY"

    invoke-virtual {v8, v9, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "scaleUpIfNeeded"

    invoke-virtual {v8, v9, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "scale"

    invoke-virtual {v8, v9, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "return-data"

    invoke-virtual {v8, v9, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    .line 224
    .local v2, "request":Landroid/content/Intent;
    const/4 v8, 0x6

    :try_start_0
    invoke-virtual {p0, v2, v8}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :goto_0
    return-void

    .line 225
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v8, "WidgetConfigure"

    const-string v9, "Activity Not Found"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private setChoosenPhotos(Landroid/content/Intent;)V
    .locals 11
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 362
    if-eqz p1, :cond_1

    .line 363
    const-string v7, "selectedItems"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 364
    .local v6, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 365
    .local v4, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    .line 366
    .local v3, "mDataManager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 367
    .local v5, "uri":Landroid/net/Uri;
    const/4 v7, 0x0

    invoke-virtual {v3, v5, v7}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 370
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_0
    iget v7, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->convertPathToFilePath(Lcom/sec/android/gallery3d/data/DataManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-static {p0, v7, v8}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->saveWidgetPaths(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 373
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "mDataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v4    # "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .end local v6    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 374
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v7, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 375
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v7, 0x3

    iput v7, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 377
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 378
    iget v7, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 380
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 383
    const/4 v7, -0x1

    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    const-string v9, "appWidgetId"

    iget v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->setResult(ILandroid/content/Intent;)V

    .line 384
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->finish()V

    .line 385
    return-void

    .line 380
    :catchall_0
    move-exception v7

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v7
.end method

.method private setDefaultWidget()V
    .locals 6

    .prologue
    .line 342
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 343
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 344
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v2, 0x0

    iput v2, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 346
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 347
    iget v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 352
    const/4 v2, -0x1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "appWidgetId"

    iget v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->setResult(ILandroid/content/Intent;)V

    .line 353
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->finish()V

    .line 354
    return-void

    .line 349
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v2
.end method

.method private setDynamicFrame(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 477
    if-eqz p1, :cond_0

    .line 478
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mMenuList:[Ljava/lang/String;

    .line 482
    :goto_0
    return-void

    .line 480
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mMenuList:[Ljava/lang/String;

    goto :goto_0
.end method

.method private setPhotoWidget(Landroid/content/Intent;)V
    .locals 4
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 186
    const-string v2, "data"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 187
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 189
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    :try_start_0
    iget v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    iget-object v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mPickedItem:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setPhoto(ILandroid/net/Uri;Landroid/graphics/Bitmap;)Z

    .line 190
    iget v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 193
    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 195
    const/4 v0, 0x0

    .line 198
    :cond_0
    return-void

    .line 192
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 193
    if-eqz v0, :cond_1

    .line 194
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 195
    const/4 v0, 0x0

    :cond_1
    throw v2
.end method

.method private setWidgetType(I)V
    .locals 8
    .param p1, "widgetType"    # I

    .prologue
    const/4 v7, 0x1

    .line 433
    packed-switch p1, :pswitch_data_0

    .line 474
    :goto_0
    return-void

    .line 435
    :pswitch_0
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 436
    .local v3, "request":Landroid/content/Intent;
    const-string v5, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 437
    const-string v5, "image/*"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 438
    const-string v5, "photo-pick"

    invoke-virtual {v3, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 439
    const-string v5, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v3, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 440
    const/4 v5, 0x0

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 443
    .end local v3    # "request":Landroid/content/Intent;
    :pswitch_1
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 444
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "android.intent.action.GET_CONTENT"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 445
    const-string v5, "image/*"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 446
    const-string v5, "album-pick"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 447
    const-string v5, "photo-pick"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 448
    const-string v5, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 449
    invoke-virtual {p0, v2, v7}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 452
    .end local v2    # "intent":Landroid/content/Intent;
    :pswitch_2
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 453
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 454
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    iget v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    iput v5, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->widgetId:I

    .line 455
    iput v7, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 457
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 458
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 460
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v5

    .line 464
    .end local v0    # "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    .end local v1    # "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    :pswitch_3
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 465
    .local v4, "requestPerson":Landroid/content/Intent;
    const-string v5, "android.intent.action.PERSON_PICK"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 466
    const-string v5, "image/*"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 467
    const-string v5, "person-pick"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 468
    const-string v5, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 469
    const/4 v5, 0x3

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 433
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    .locals 4
    .param p1, "entry"    # Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-static {v0, v1, p1}, Lcom/sec/android/gallery3d/gadget/PhotoAppWidgetProvider;->buildAndUpdateWidget(Landroid/content/Context;ILcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V

    .line 148
    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "appWidgetId"

    iget v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->setResult(ILandroid/content/Intent;)V

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->finish()V

    .line 151
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 155
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 156
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->setResult(ILandroid/content/Intent;)V

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->finish()V

    .line 182
    :goto_0
    return-void

    .line 171
    :cond_0
    if-nez p1, :cond_1

    .line 172
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->setChoosenPhotos(Landroid/content/Intent;)V

    goto :goto_0

    .line 173
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 174
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->setChoosenAlbum(Landroid/content/Intent;)V

    goto :goto_0

    .line 175
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 176
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->setChoosenPerson(Landroid/content/Intent;)V

    goto :goto_0

    .line 180
    :cond_3
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown request: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 87
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 89
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "appWidgetId"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    .line 91
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.appwidget.action.APPWIDGET_DELETED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 92
    .local v0, "deleteFilter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 96
    iget v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mAppWidgetId:I

    if-nez v2, :cond_0

    .line 98
    const-string v2, "WidgetConfigure"

    const-string v3, "AppWidgetId is invalid. cancelled"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->setResult(I)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->finish()V

    .line 133
    :goto_0
    return-void

    .line 107
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "SHOW_WIDGET_TYPE"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 109
    .local v1, "needShowWidgetTypeDialog":Z
    if-nez v1, :cond_1

    .line 110
    const-string v2, "WidgetConfigure"

    const-string v3, "set default widget of S Camera"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->setDefaultWidget()V

    goto :goto_0

    .line 117
    .end local v1    # "needShowWidgetTypeDialog":Z
    :cond_1
    const-string v2, "WidgetConfigure"

    const-string/jumbo v3, "show widget type dialog"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDynamicFrame:Z

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->setDynamicFrame(Z)V

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->showWidgetTypeDialog()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 312
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 313
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 298
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 300
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkLowStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 301
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUMS:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "sys.usb.config"

    const-string v1, "none"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mass_storage"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e016e

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;II)V

    .line 303
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->finish()V

    .line 306
    :cond_0
    return-void
.end method

.method protected onSaveInstanceStates(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 136
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 137
    const-string v0, "picked-item"

    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mPickedItem:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 138
    return-void
.end method

.method public showWidgetTypeDialog()V
    .locals 4

    .prologue
    .line 317
    :try_start_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e009f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->mMenuList:[Ljava/lang/String;

    new-instance v3, Lcom/sec/android/gallery3d/gadget/WidgetConfigure$5;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure$5;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetConfigure;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0046

    new-instance v3, Lcom/sec/android/gallery3d/gadget/WidgetConfigure$4;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure$4;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetConfigure;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/gallery3d/gadget/WidgetConfigure$3;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure$3;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetConfigure;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :goto_0
    return-void

    .line 336
    :catch_0
    move-exception v0

    .line 337
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "WidgetConfigure"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "showWidgetTypeDialog() exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
