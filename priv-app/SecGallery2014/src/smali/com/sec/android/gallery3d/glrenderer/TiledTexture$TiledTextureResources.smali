.class public Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;
.super Ljava/lang/Object;
.source "TiledTexture.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glrenderer/TiledTexture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TiledTextureResources"
.end annotation


# instance fields
.field private final sBitmapPaint:Landroid/graphics/Paint;

.field private final sCanvas:Landroid/graphics/Canvas;

.field private final sPaint:Landroid/graphics/Paint;

.field private final sUploadBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v1, 0x100

    .line 387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;->sUploadBitmap:Landroid/graphics/Bitmap;

    .line 389
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;->sUploadBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;->sCanvas:Landroid/graphics/Canvas;

    .line 390
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;->sBitmapPaint:Landroid/graphics/Paint;

    .line 391
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;->sBitmapPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 392
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;->sPaint:Landroid/graphics/Paint;

    .line 393
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;->sPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 394
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;->sPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 395
    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;->sUploadBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;)Landroid/graphics/Canvas;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;->sCanvas:Landroid/graphics/Canvas;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;->sBitmapPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;->sPaint:Landroid/graphics/Paint;

    return-object v0
.end method
