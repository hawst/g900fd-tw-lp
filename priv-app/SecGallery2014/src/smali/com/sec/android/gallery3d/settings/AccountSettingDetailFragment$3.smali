.class Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$3;
.super Ljava/lang/Object;
.source "AccountSettingDetailFragment.java"

# interfaces
.implements Landroid/app/FragmentManager$OnBackStackChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$3;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackStackChanged()V
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$3;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$300(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$3;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$300(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/preference/Preference;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$3;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0e00bc

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$3;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # invokes: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->updateContextualTagPrefSwitchState()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$400(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    .line 253
    :cond_0
    return-void

    .line 250
    :cond_1
    const v0, 0x7f0e0187

    goto :goto_0
.end method
