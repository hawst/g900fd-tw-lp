.class public Lcom/sec/android/gallery3d/data/SnailItem;
.super Lcom/sec/android/gallery3d/data/MediaItem;
.source "SnailItem.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SnailItem"


# instance fields
.field private mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 35
    invoke-static {}, Lcom/sec/android/gallery3d/data/SnailItem;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 36
    return-void
.end method


# virtual methods
.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 104
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    const-string v0, ""

    return-object v0
.end method

.method public getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SnailItem;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Lcom/sec/android/gallery3d/data/SnailItem$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/SnailItem$1;-><init>(Lcom/sec/android/gallery3d/data/SnailItem;)V

    return-object v0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lcom/sec/android/gallery3d/data/SnailItem$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/SnailItem$2;-><init>(Lcom/sec/android/gallery3d/data/SnailItem;)V

    return-object v0
.end method

.method public setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V
    .locals 0
    .param p1, "screenNail"    # Lcom/sec/android/gallery3d/ui/ScreenNail;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/SnailItem;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 89
    return-void
.end method

.method public updateVersion()V
    .locals 2

    .prologue
    .line 92
    invoke-static {}, Lcom/sec/android/gallery3d/data/SnailItem;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/SnailItem;->mDataVersion:J

    .line 93
    return-void
.end method
