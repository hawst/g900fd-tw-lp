.class public interface abstract Lcom/sec/android/gallery3d/data/OnProgressListener;
.super Ljava/lang/Object;
.source "OnProgressListener.java"


# virtual methods
.method public abstract handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
.end method

.method public abstract handleOperation(Ljava/util/List;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract onCompleted(Z)V
.end method

.method public abstract onProgress(II)V
.end method
