.class public Lcom/sec/android/gallery3d/data/ImageDiskCache;
.super Ljava/lang/Object;
.source "ImageDiskCache.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ImageCachable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/ImageDiskCache$ImageData;,
        Lcom/sec/android/gallery3d/data/ImageDiskCache$MetaData;
    }
.end annotation


# static fields
.field static final ATTRIBUTE_CACHE_MAX_BYTES:I = 0x20000

.field private static final IMAGE_CACHE_ATTRIBUTE:Ljava/lang/String; = "attributecache"

.field private static final IMAGE_CACHE_FILE:Ljava/lang/String; = "imgcache"

.field static final IMAGE_CACHE_MAX_BYTES:I = 0xc800000

.field private static final IMAGE_CACHE_MAX_ENTRIES:I = 0x1388

.field private static final IMAGE_CACHE_MICRO:Ljava/lang/String; = "micro"

.field private static final IMAGE_CACHE_MINIMICRO:Ljava/lang/String; = "mini"

.field private static final IMAGE_CACHE_VERSION:I = 0x7

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCache:Lcom/sec/android/gallery3d/common/BlobCache;

.field private mCacheAttribute:Lcom/sec/android/gallery3d/common/BlobCache;

.field private mCacheMicro:Lcom/sec/android/gallery3d/common/BlobCache;

.field private mCacheMini:Lcom/sec/android/gallery3d/common/BlobCache;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/android/gallery3d/data/ImageDiskCache;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v1, 0xc800000

    const/16 v3, 0x1388

    const/4 v2, 0x7

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mContext:Landroid/content/Context;

    .line 66
    const-string v0, "imgcache"

    invoke-static {p1, v0, v3, v1, v2}, Lcom/sec/android/gallery3d/util/CacheManager;->getCache(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCache:Lcom/sec/android/gallery3d/common/BlobCache;

    .line 71
    const-string v0, "micro"

    invoke-static {p1, v0, v3, v1, v2}, Lcom/sec/android/gallery3d/util/CacheManager;->getCache(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMicro:Lcom/sec/android/gallery3d/common/BlobCache;

    .line 74
    const-string v0, "mini"

    invoke-static {p1, v0, v3, v1, v2}, Lcom/sec/android/gallery3d/util/CacheManager;->getCache(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMini:Lcom/sec/android/gallery3d/common/BlobCache;

    .line 79
    const-string v0, "attributecache"

    const/high16 v1, 0x20000

    invoke-static {p1, v0, v3, v1, v2}, Lcom/sec/android/gallery3d/util/CacheManager;->getCache(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheAttribute:Lcom/sec/android/gallery3d/common/BlobCache;

    .line 82
    return-void
.end method

.method private checkCache(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v1, 0xc800000

    const/16 v3, 0x1388

    const/4 v2, 0x7

    .line 231
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCache:Lcom/sec/android/gallery3d/common/BlobCache;

    if-nez v0, :cond_0

    .line 232
    const-string v0, "imgcache"

    invoke-static {p1, v0, v3, v1, v2}, Lcom/sec/android/gallery3d/util/CacheManager;->getCache(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCache:Lcom/sec/android/gallery3d/common/BlobCache;

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMicro:Lcom/sec/android/gallery3d/common/BlobCache;

    if-nez v0, :cond_1

    .line 238
    const-string v0, "micro"

    invoke-static {p1, v0, v3, v1, v2}, Lcom/sec/android/gallery3d/util/CacheManager;->getCache(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMicro:Lcom/sec/android/gallery3d/common/BlobCache;

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMini:Lcom/sec/android/gallery3d/common/BlobCache;

    if-nez v0, :cond_2

    .line 244
    const-string v0, "mini"

    invoke-static {p1, v0, v3, v1, v2}, Lcom/sec/android/gallery3d/util/CacheManager;->getCache(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMini:Lcom/sec/android/gallery3d/common/BlobCache;

    .line 250
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheAttribute:Lcom/sec/android/gallery3d/common/BlobCache;

    if-nez v0, :cond_3

    .line 251
    const-string v0, "attributecache"

    const/high16 v1, 0x20000

    invoke-static {p1, v0, v3, v1, v2}, Lcom/sec/android/gallery3d/util/CacheManager;->getCache(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheAttribute:Lcom/sec/android/gallery3d/common/BlobCache;

    .line 255
    :cond_3
    return-void
.end method

.method private getCache(I)Lcom/sec/android/gallery3d/common/BlobCache;
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 51
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMicro:Lcom/sec/android/gallery3d/common/BlobCache;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMicro:Lcom/sec/android/gallery3d/common/BlobCache;

    .line 58
    :goto_0
    return-object v0

    .line 53
    :cond_0
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMini:Lcom/sec/android/gallery3d/common/BlobCache;

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMini:Lcom/sec/android/gallery3d/common/BlobCache;

    goto :goto_0

    .line 55
    :cond_1
    const/4 v0, 0x6

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheAttribute:Lcom/sec/android/gallery3d/common/BlobCache;

    if-eqz v0, :cond_2

    .line 56
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheAttribute:Lcom/sec/android/gallery3d/common/BlobCache;

    goto :goto_0

    .line 58
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCache:Lcom/sec/android/gallery3d/common/BlobCache;

    goto :goto_0
.end method

.method private static isSameKey([B[B)Z
    .locals 5
    .param p0, "key"    # [B
    .param p1, "buffer"    # [B

    .prologue
    const/4 v2, 0x0

    .line 190
    array-length v1, p0

    .line 191
    .local v1, "n":I
    array-length v3, p1

    if-ge v3, v1, :cond_1

    .line 199
    :cond_0
    :goto_0
    return v2

    .line 194
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 195
    aget-byte v3, p0, v0

    aget-byte v4, p1, v0

    if-ne v3, v4, :cond_0

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 199
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public clearImageData([BI)V
    .locals 4
    .param p1, "key"    # [B
    .param p2, "type"    # I

    .prologue
    .line 149
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/data/ImageDiskCache;->getCache(I)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    .line 151
    .local v0, "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->crc64Long([B)J

    move-result-wide v2

    .line 152
    .local v2, "cacheKey":J
    monitor-enter v0

    .line 154
    :try_start_0
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/common/BlobCache;->clearEntry(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    :goto_0
    :try_start_1
    monitor-exit v0

    .line 160
    return-void

    .line 158
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 155
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getCurrentDiskCacheSize()J
    .locals 6

    .prologue
    .line 204
    const-wide/16 v0, 0x0

    .line 205
    .local v0, "cacheSize":J
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/data/ImageDiskCache;->checkCache(Landroid/content/Context;)V

    .line 206
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCache:Lcom/sec/android/gallery3d/common/BlobCache;

    monitor-enter v3

    .line 207
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCache:Lcom/sec/android/gallery3d/common/BlobCache;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/common/BlobCache;->getCurrentCacheSize()J

    move-result-wide v0

    .line 208
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMicro:Lcom/sec/android/gallery3d/common/BlobCache;

    if-eqz v2, :cond_0

    .line 212
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMicro:Lcom/sec/android/gallery3d/common/BlobCache;

    monitor-enter v3

    .line 213
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMicro:Lcom/sec/android/gallery3d/common/BlobCache;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/common/BlobCache;->getCurrentCacheSize()J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 214
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 216
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMini:Lcom/sec/android/gallery3d/common/BlobCache;

    if-eqz v2, :cond_1

    .line 217
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMini:Lcom/sec/android/gallery3d/common/BlobCache;

    monitor-enter v3

    .line 218
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheMini:Lcom/sec/android/gallery3d/common/BlobCache;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/common/BlobCache;->getCurrentCacheSize()J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 219
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 222
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheAttribute:Lcom/sec/android/gallery3d/common/BlobCache;

    if-eqz v2, :cond_2

    .line 223
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheAttribute:Lcom/sec/android/gallery3d/common/BlobCache;

    monitor-enter v3

    .line 224
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache;->mCacheAttribute:Lcom/sec/android/gallery3d/common/BlobCache;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/common/BlobCache;->getCurrentCacheSize()J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 225
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 227
    :cond_2
    return-wide v0

    .line 208
    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    .line 214
    :catchall_1
    move-exception v2

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v2

    .line 219
    :catchall_2
    move-exception v2

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v2

    .line 225
    :catchall_3
    move-exception v2

    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v2
.end method

.method public getImageData([BILcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z
    .locals 7
    .param p1, "key"    # [B
    .param p2, "type"    # I
    .param p3, "buffer"    # Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    .prologue
    const/4 v4, 0x0

    .line 96
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/data/ImageDiskCache;->getCache(I)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    .line 97
    .local v0, "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    if-nez v0, :cond_1

    .line 98
    sget-object v5, Lcom/sec/android/gallery3d/data/ImageDiskCache;->TAG:Ljava/lang/String;

    const-string v6, "BlobCache is null"

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    :goto_0
    return v4

    .line 102
    :cond_1
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->crc64Long([B)J

    move-result-wide v2

    .line 104
    .local v2, "cacheKey":J
    :try_start_0
    new-instance v1, Lcom/sec/android/gallery3d/common/BlobCache$LookupRequest;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/common/BlobCache$LookupRequest;-><init>()V

    .line 105
    .local v1, "request":Lcom/sec/android/gallery3d/common/BlobCache$LookupRequest;
    iput-wide v2, v1, Lcom/sec/android/gallery3d/common/BlobCache$LookupRequest;->key:J

    .line 106
    iget-object v5, p3, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    iput-object v5, v1, Lcom/sec/android/gallery3d/common/BlobCache$LookupRequest;->buffer:[B

    .line 107
    monitor-enter v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :try_start_1
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/common/BlobCache;->lookup(Lcom/sec/android/gallery3d/common/BlobCache$LookupRequest;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 109
    monitor-exit v0

    goto :goto_0

    .line 110
    :catchall_0
    move-exception v5

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 117
    .end local v1    # "request":Lcom/sec/android/gallery3d/common/BlobCache$LookupRequest;
    :catch_0
    move-exception v5

    goto :goto_0

    .line 110
    .restart local v1    # "request":Lcom/sec/android/gallery3d/common/BlobCache$LookupRequest;
    :cond_2
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 111
    :try_start_4
    iget-object v5, v1, Lcom/sec/android/gallery3d/common/BlobCache$LookupRequest;->buffer:[B

    invoke-static {p1, v5}, Lcom/sec/android/gallery3d/data/ImageDiskCache;->isSameKey([B[B)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 112
    iget-object v5, v1, Lcom/sec/android/gallery3d/common/BlobCache$LookupRequest;->buffer:[B

    iput-object v5, p3, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    .line 113
    array-length v5, p1

    iput v5, p3, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    .line 114
    iget v5, v1, Lcom/sec/android/gallery3d/common/BlobCache$LookupRequest;->length:I

    iget v6, p3, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    sub-int/2addr v5, v6

    iput v5, p3, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 115
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public putImageData([BI[B)V
    .locals 6
    .param p1, "key"    # [B
    .param p2, "type"    # I
    .param p3, "value"    # [B

    .prologue
    .line 127
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/data/ImageDiskCache;->getCache(I)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v1

    .line 128
    .local v1, "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    if-nez v1, :cond_0

    .line 144
    :goto_0
    return-void

    .line 132
    :cond_0
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->crc64Long([B)J

    move-result-wide v2

    .line 133
    .local v2, "cacheKey":J
    array-length v4, p1

    array-length v5, p3

    add-int/2addr v4, v5

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 134
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 135
    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 136
    monitor-enter v1

    .line 138
    :try_start_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/common/BlobCache;->insert(J[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    :goto_1
    :try_start_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 139
    :catch_0
    move-exception v4

    goto :goto_1
.end method
