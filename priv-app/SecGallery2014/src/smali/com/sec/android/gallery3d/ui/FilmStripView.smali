.class public Lcom/sec/android/gallery3d/ui/FilmStripView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "FilmStripView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;
.implements Lcom/sec/android/gallery3d/ui/UserInteractionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;
    }
.end annotation


# static fields
.field private static final HIDE_ANIMATION_DURATION:I = 0x12c

.field private static final ITEM_RATIO:F = 0.8f

.field private static final MSG_HIDE_SCROLLBAR:I = 0x1

.field private static final SLOT_GAP:I = -0x14

.field private static final TAG:Ljava/lang/String; = "FilmStripView"

.field private static final TIMEOUT:I = 0x1f4

.field public static mUse3DFilmStrip:Z


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

.field private mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

.field private mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

.field private mAnimation:Lcom/sec/android/gallery3d/anim/AlphaAnimation;

.field private mBarSize:I

.field private mBottomMargin:I

.field private mContentSize:I

.field private mDisableSelectMode:Z

.field private mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

.field private mFilmStripVisible:Z

.field private mGripSize:I

.field private mGripWidth:I

.field private mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

.field private mHelpMode:I

.field private mIsInitialized:Z

.field private mIsSelectioMode:Z

.field private mLeftStart:I

.field private mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

.field private mLoadingFinished:Z

.field private mLockTouchEvent:Z

.field private mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mMidMargin:I

.field private mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

.field private mScreenWidth:I

.field private mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

.field mScrollPosition:I

.field protected mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

.field private mThumbSize:I

.field private mTopMargin:I

.field private mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

.field private final mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mUse3DFilmStrip:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/util/KeyBoardManager;IZLcom/sec/android/gallery3d/app/AlbumReloader;)V
    .locals 5
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "keyboardManager"    # Lcom/sec/android/gallery3d/util/KeyBoardManager;
    .param p4, "helpMode"    # I
    .param p5, "disableSelectMode"    # Z
    .param p6, "albumReloader"    # Lcom/sec/android/gallery3d/app/AlbumReloader;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 126
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 74
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mLockTouchEvent:Z

    .line 75
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mFilmStripVisible:Z

    .line 89
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mLoadingFinished:Z

    .line 95
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mIsSelectioMode:Z

    .line 98
    iput v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mHelpMode:I

    .line 99
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mDisableSelectMode:Z

    .line 102
    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    .line 107
    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mVibrator:Landroid/os/Vibrator;

    .line 556
    new-instance v4, Lcom/sec/android/gallery3d/ui/FilmStripView$2;

    invoke-direct {v4, p0}, Lcom/sec/android/gallery3d/ui/FilmStripView$2;-><init>(Lcom/sec/android/gallery3d/ui/FilmStripView;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 781
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollPosition:I

    .line 127
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 128
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 129
    iput-object p6, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    .line 130
    iput p4, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mHelpMode:I

    .line 131
    iget v4, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mHelpMode:I

    if-eqz v4, :cond_1

    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mIsInitialized:Z

    .line 132
    iput-boolean p5, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mDisableSelectMode:Z

    .line 133
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->initializeViews()V

    .line 134
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->initializeData(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 136
    new-instance v2, Lcom/sec/android/gallery3d/ui/FilmStripView$1;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/sec/android/gallery3d/ui/FilmStripView$1;-><init>(Lcom/sec/android/gallery3d/ui/FilmStripView;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    .line 155
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v2, :cond_0

    .line 156
    new-instance v2, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

    invoke-direct {v2}, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;-><init>()V

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mGLViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->initAccessibilityListener()V

    .line 158
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mGLViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mGLViewAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;->setAccessibilityNodeInfoListener(Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;)V

    .line 160
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getScrollWidth()I

    move-result v1

    .line 161
    .local v1, "gripWidth":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getScreenWidth()I

    move-result v2

    if-le v1, v2, :cond_2

    .line 167
    :goto_1
    return-void

    .end local v1    # "gripWidth":I
    :cond_1
    move v2, v3

    .line 131
    goto :goto_0

    .line 164
    .restart local v1    # "gripWidth":I
    :cond_2
    iget v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mContentSize:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 165
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0114

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 166
    .local v0, "gripHeight":I
    new-instance v2, Lcom/sec/android/gallery3d/ui/ScrollBarView;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3, v0, v1}, Lcom/sec/android/gallery3d/ui/ScrollBarView;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/ScrollBarView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/SlotView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/app/AlbumDataLoader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/gallery3d/ui/FilmStripView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mLeftStart:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/ui/FilmStripView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mTopMargin:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/FilmStripView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mFilmStripVisible:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/FilmStripView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mLockTouchEvent:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/FilmStripView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mHelpMode:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/FilmStripView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;
    .param p1, "x1"    # I

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->onDown(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/ui/FilmStripView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->onUp(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/EdgeView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/gallery3d/ui/FilmStripView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FilmStripView;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mLoadingFinished:Z

    return p1
.end method

.method private getGenericFocusIndex()I
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->getGenericFocusIndex()I

    move-result v0

    .line 355
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private initializeData(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 3
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 745
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mDisableSelectMode:Z

    if-nez v0, :cond_0

    .line 746
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setSourceMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 748
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/app/AlbumReloader;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .line 749
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    new-instance v1, Lcom/sec/android/gallery3d/ui/FilmStripView$4;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/ui/FilmStripView$4;-><init>(Lcom/sec/android/gallery3d/ui/FilmStripView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V

    .line 761
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->setModel(Lcom/sec/android/gallery3d/app/AlbumDataLoader;Z)V

    .line 763
    return-void
.end method

.method private initializeViews()V
    .locals 7

    .prologue
    .line 651
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->get(Landroid/content/Context;)Lcom/sec/android/gallery3d/app/Config$PhotoPage;

    move-result-object v0

    .line 652
    .local v0, "config":Lcom/sec/android/gallery3d/app/Config$PhotoPage;
    iget v2, v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripTopMargin:I

    iput v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mTopMargin:I

    .line 653
    iget v2, v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripMidMargin:I

    iput v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mMidMargin:I

    .line 654
    iget v2, v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripBottomMargin:I

    iput v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mBottomMargin:I

    .line 655
    iget v2, v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripContentSize:I

    iput v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mContentSize:I

    .line 656
    iget v2, v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripThumbSize:I

    iput v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mThumbSize:I

    .line 657
    iget v2, v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripBarSize:I

    iput v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mBarSize:I

    .line 658
    iget v2, v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripGripSize:I

    iput v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mGripSize:I

    .line 659
    iget v2, v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripGripWidth:I

    iput v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mGripWidth:I

    .line 661
    new-instance v1, Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/ui/SlotView$Spec;-><init>()V

    .line 662
    .local v1, "spec":Lcom/sec/android/gallery3d/ui/SlotView$Spec;
    sget-boolean v2, Lcom/sec/android/gallery3d/ui/FilmStripView;->mUse3DFilmStrip:Z

    if-eqz v2, :cond_0

    .line 663
    iget v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mThumbSize:I

    int-to-float v2, v2

    const v3, 0x3f4ccccd    # 0.8f

    div-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v1, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotWidth:I

    .line 664
    iget v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mThumbSize:I

    iput v2, v1, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotHeight:I

    .line 665
    const/16 v2, -0x14

    iput v2, v1, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotGap:I

    .line 672
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 673
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, p0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setSelectionListener(Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;)V

    .line 674
    new-instance v2, Lcom/sec/android/gallery3d/ui/SlotView;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3, v1}, Lcom/sec/android/gallery3d/ui/SlotView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/SlotView$Spec;)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    .line 675
    new-instance v2, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget v6, v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->placeholderColor:I

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/SlotView;Lcom/sec/android/gallery3d/ui/SelectionManager;I)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    .line 677
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/SlotView;->setSlotRenderer(Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;)V

    .line 679
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    new-instance v3, Lcom/sec/android/gallery3d/ui/FilmStripView$3;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/FilmStripView$3;-><init>(Lcom/sec/android/gallery3d/ui/FilmStripView;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/SlotView;->setListener(Lcom/sec/android/gallery3d/ui/SlotView$Listener;)V

    .line 732
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/SlotView;->setOverscrollEffect(I)V

    .line 734
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/SlotView;->setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V

    .line 736
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 738
    new-instance v2, Lcom/sec/android/gallery3d/ui/EdgeView;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/gallery3d/ui/EdgeView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    .line 739
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 740
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/EdgeView;->setVisibility(I)V

    .line 741
    return-void

    .line 667
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mThumbSize:I

    iput v2, v1, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotWidth:I

    .line 668
    iget v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mThumbSize:I

    iput v2, v1, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotHeight:I

    .line 669
    const/4 v2, 0x0

    iput v2, v1, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotGap:I

    goto :goto_0
.end method

.method private onDown(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 373
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->get(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 374
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_1

    const/4 v1, 0x0

    .line 375
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    .line 377
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    if-eqz v2, :cond_0

    .line 378
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFilmStripSelectionMode:Z

    if-nez v2, :cond_2

    .line 379
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->showScrollBar()V

    .line 383
    :goto_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 384
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->hideScrollBar()V

    .line 386
    :cond_0
    return-void

    .line 374
    .end local v1    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0

    .line 381
    .restart local v1    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_2
    const/16 v2, 0x1f4

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->refreshHidingMessage(I)V

    goto :goto_1
.end method

.method private onUp(Z)V
    .locals 1
    .param p1, "followedByLongPress"    # Z

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    .line 390
    return-void
.end method

.method private playHaptic()V
    .locals 4

    .prologue
    .line 839
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mVibrator:Landroid/os/Vibrator;

    if-nez v0, :cond_0

    .line 840
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mVibrator:Landroid/os/Vibrator;

    .line 841
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 842
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 843
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 845
    :cond_1
    return-void
.end method

.method private refreshHidingMessage(I)V
    .locals 4
    .param p1, "timeout"    # I

    .prologue
    const/4 v1, 0x1

    .line 794
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 795
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->removeMessages(I)V

    .line 798
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    if-eqz v0, :cond_1

    .line 799
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 800
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->showScrollBar()V

    .line 801
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    if-eqz v0, :cond_1

    .line 802
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 806
    :cond_1
    return-void
.end method

.method private selectItem(I)V
    .locals 3
    .param p1, "slotIndex"    # I

    .prologue
    .line 472
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->get(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 473
    .local v0, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v1, v0, Lcom/sec/android/gallery3d/data/ActionImage;

    if-eqz v1, :cond_0

    .line 487
    :goto_0
    return-void

    .line 476
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->hasAddedSelectionIndex(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 477
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->removeSelectionIndex(I)V

    .line 478
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    if-eqz v1, :cond_1

    .line 479
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;->onSelectionChanged(Lcom/sec/android/gallery3d/data/MediaItem;ZI)V

    .line 486
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->invalidate()V

    goto :goto_0

    .line 481
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->addSelectionIndex(I)V

    .line 482
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    if-eqz v1, :cond_1

    .line 483
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;->onSelectionChanged(Lcom/sec/android/gallery3d/data/MediaItem;ZI)V

    goto :goto_1
.end method

.method private updateBottomMargin()V
    .locals 3

    .prologue
    .line 344
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 345
    .local v0, "r":Landroid/content/res/Resources;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 346
    const v1, 0x7f0d010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mBottomMargin:I

    .line 350
    :goto_0
    return-void

    .line 348
    :cond_0
    const v1, 0x7f0d010d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mBottomMargin:I

    goto :goto_0
.end method


# virtual methods
.method public changeSource(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 1
    .param p1, "set"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 766
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->pause()V

    .line 767
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->setMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 768
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->resume()V

    .line 769
    return-void
.end method

.method public checkIfInitiated()Z
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->checkIfFilmStripInitiated()Z

    move-result v0

    return v0
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 542
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->destroy()V

    .line 543
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setSelectionListener(Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;)V

    .line 544
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    .line 545
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 546
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    .line 547
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 292
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 323
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mFilmStripVisible:Z

    if-eqz v2, :cond_4

    .line 324
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 326
    :goto_1
    return v2

    .line 295
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserInteractionBegin()V

    goto :goto_0

    .line 299
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserInteractionEnd()V

    goto :goto_0

    .line 304
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    if-eqz v2, :cond_0

    .line 305
    const/4 v1, -0x1

    .line 306
    .local v1, "previousIndex":I
    const/16 v2, 0x3ea

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 307
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getGenericFocusIndex()I

    move-result v1

    .line 308
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/ui/SlotView;->getSlotIndexByPosition(FF)I

    move-result v0

    .line 309
    .local v0, "index":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 310
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    invoke-interface {v2, p0}, Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;->onGenericMotionCancel(Lcom/sec/android/gallery3d/ui/GLView;)V

    goto :goto_0

    .line 311
    :cond_2
    if-eq v1, v0, :cond_0

    .line 312
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 315
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mGLViewAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    invoke-interface {v3}, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;->getIndexOffset()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->setFocusedIndex(I)V

    .line 318
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    invoke-interface {v2, p0, v0}, Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;->onGenericMotionEnter(Lcom/sec/android/gallery3d/ui/GLView;I)V

    goto :goto_0

    .line 326
    .end local v0    # "index":I
    .end local v1    # "previousIndex":I
    :cond_4
    const/4 v2, 0x1

    goto :goto_1

    .line 292
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x3e8 -> :sswitch_2
        0x3ea -> :sswitch_2
        0x3eb -> :sswitch_2
    .end sparse-switch
.end method

.method public enterSelectionMode()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 457
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mIsSelectioMode:Z

    if-eqz v0, :cond_1

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 460
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mIsSelectioMode:Z

    .line 461
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    if-eqz v0, :cond_2

    .line 462
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;->onSelectionModeChanged(Z)V

    .line 464
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->clearAllSelectionIndex()V

    .line 466
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;->onLongTap(I)V

    goto :goto_0
.end method

.method public exitSelectionMode()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 502
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mIsSelectioMode:Z

    .line 503
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;->onSelectionModeChanged(Z)V

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    if-eqz v0, :cond_1

    .line 507
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->clearAllSelectionIndex()V

    .line 509
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->invalidate()V

    .line 510
    return-void
.end method

.method public forceHide()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAnimation:Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAnimation:Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/anim/AlphaAnimation;->forceStop()V

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    if-eqz v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->hideScrollBar()V

    .line 222
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setVisibility(I)V

    .line 223
    return-void
.end method

.method public getFilmStripLength()I
    .locals 3

    .prologue
    .line 823
    const/4 v0, 0x1

    .line 824
    .local v0, "ret":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v1, :cond_0

    .line 825
    iget v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mContentSize:I

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v2

    mul-int v0, v1, v2

    .line 827
    :cond_0
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .end local v0    # "ret":I
    :cond_1
    return v0
.end method

.method public getScreenWidth()I
    .locals 3

    .prologue
    .line 809
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v0

    .line 810
    .local v0, "multiWindowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 811
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    .line 813
    :goto_0
    return v1

    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v1

    goto :goto_0
.end method

.method public getScrollWidth()I
    .locals 3

    .prologue
    .line 818
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getScreenWidth()I

    move-result v0

    .line 819
    .local v0, "width":I
    mul-int v1, v0, v0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getFilmStripLength()I

    move-result v2

    div-int/2addr v1, v2

    return v1
.end method

.method public hide()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 200
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mFilmStripVisible:Z

    .line 201
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    if-eqz v1, :cond_0

    .line 202
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->hideScrollBar()V

    .line 204
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 213
    :goto_0
    return-void

    .line 205
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/anim/AlphaAnimation;-><init>(FF)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAnimation:Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    .line 207
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAnimation:Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    const/16 v2, 0x12c

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/anim/AlphaAnimation;->setDuration(I)V

    .line 208
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAnimation:Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->startAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V

    .line 209
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 210
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "FilmStripView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IllegalStateException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected initAccessibilityListener()V
    .locals 1

    .prologue
    .line 848
    new-instance v0, Lcom/sec/android/gallery3d/ui/FilmStripView$5;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/FilmStripView$5;-><init>(Lcom/sec/android/gallery3d/ui/FilmStripView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mGLViewAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    .line 930
    return-void
.end method

.method public initialize(I)V
    .locals 1
    .param p1, "startIndex"    # I

    .prologue
    .line 190
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mIsInitialized:Z

    .line 191
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->setSlotPosition(I)V

    .line 192
    return-void
.end method

.method public initialized()Z
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mIsInitialized:Z

    return v0
.end method

.method public isSelectionMode()Z
    .locals 1

    .prologue
    .line 498
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mIsSelectioMode:Z

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 260
    iput p2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mLeftStart:I

    .line 261
    if-nez p1, :cond_1

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    iget v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mTopMargin:I

    sub-int v3, p4, p2

    iget v4, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mTopMargin:I

    iget v5, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mContentSize:I

    add-int/2addr v4, v5

    invoke-virtual {v1, v6, v2, v3, v4}, Lcom/sec/android/gallery3d/ui/SlotView;->layout(IIII)V

    .line 265
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->getFocusIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SlotView;->scrollTo(I)V

    .line 266
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    if-eqz v1, :cond_2

    .line 267
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    iget v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mTopMargin:I

    sub-int v3, p4, p2

    iget v4, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mTopMargin:I

    iget v5, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mContentSize:I

    add-int/2addr v4, v5

    invoke-virtual {v1, v6, v2, v3, v4}, Lcom/sec/android/gallery3d/ui/EdgeView;->layout(IIII)V

    .line 269
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    if-eqz v1, :cond_0

    .line 270
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0114

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 271
    .local v0, "scrollBarHeight":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    iget v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mTopMargin:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mContentSize:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mTopMargin:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mContentSize:I

    add-int/2addr v3, v4

    add-int/2addr v3, v0

    invoke-virtual {v1, p2, v2, p4, v3}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->layout(IIII)V

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getScreenWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScreenWidth:I

    .line 273
    iget v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScreenWidth:I

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getFilmStripLength()I

    move-result v2

    div-int/2addr v1, v2

    if-lt v1, v7, :cond_3

    .line 274
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->setVisibility(I)V

    goto :goto_0

    .line 276
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onLongTap(I)V
    .locals 3
    .param p1, "slotIndex"    # I

    .prologue
    const/4 v2, 0x1

    .line 421
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFilmStripSelectionMode:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mDisableSelectMode:Z

    if-eqz v1, :cond_2

    .line 422
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->onSingleTapUp(I)V

    .line 453
    :cond_1
    :goto_0
    return-void

    .line 425
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->get(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 427
    .local v0, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v1, v0, Lcom/sec/android/gallery3d/data/UriImage;

    if-nez v1, :cond_1

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/UriVideo;

    if-nez v1, :cond_1

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/HelpImage;

    if-nez v1, :cond_1

    .line 431
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mIsSelectioMode:Z

    if-eqz v1, :cond_3

    .line 432
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->playHaptic()V

    .line 433
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->selectItem(I)V

    .line 434
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    if-eqz v1, :cond_1

    .line 435
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    invoke-interface {v1, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;->onSlotSelected(I)Z

    goto :goto_0

    .line 437
    :cond_3
    if-eqz v0, :cond_1

    .line 438
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->playHaptic()V

    .line 439
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mIsSelectioMode:Z

    .line 440
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    if-eqz v1, :cond_4

    .line 441
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;->onSelectionModeChanged(Z)V

    .line 443
    :cond_4
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->clearAllSelectionIndex()V

    .line 444
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->addSelectionIndex(I)V

    .line 446
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    if-eqz v1, :cond_5

    .line 447
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    invoke-interface {v1, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;->onLongTap(I)V

    .line 448
    :cond_5
    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    if-eqz v1, :cond_6

    .line 449
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    invoke-interface {v1, v0, v2, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;->onSelectionChanged(Lcom/sec/android/gallery3d/data/MediaItem;ZI)V

    .line 451
    :cond_6
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    .line 250
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->updateBottomMargin()V

    .line 251
    iget v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mTopMargin:I

    iget v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mContentSize:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mMidMargin:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mBarSize:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mBottomMargin:I

    add-int v0, v1, v2

    .line 252
    .local v0, "height":I
    invoke-static {p0}, Lcom/sec/android/gallery3d/ui/MeasureHelper;->getInstance(Lcom/sec/android/gallery3d/ui/GLView;)Lcom/sec/android/gallery3d/ui/MeasureHelper;

    move-result-object v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/ui/MeasureHelper;->setPreferredContentSize(II)Lcom/sec/android/gallery3d/ui/MeasureHelper;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getPaddings()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2}, Lcom/sec/android/gallery3d/ui/MeasureHelper;->measure(IILandroid/graphics/Rect;)V

    .line 255
    return-void
.end method

.method public onScrollPositionChanged(II)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "total"    # I

    .prologue
    .line 783
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFilmStripSelectionMode:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 791
    :cond_0
    :goto_0
    return-void

    .line 786
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollPosition:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollPosition:I

    if-eq v0, p2, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollPosition:I

    if-eq v0, p1, :cond_2

    .line 787
    const/16 v0, 0x1f4

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->refreshHidingMessage(I)V

    .line 790
    :cond_2
    iput p1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollPosition:I

    goto :goto_0
.end method

.method public onSelectionChange(Lcom/sec/android/gallery3d/data/Path;Z)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "selected"    # Z

    .prologue
    .line 648
    return-void
.end method

.method public onSelectionModeChange(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 642
    return-void
.end method

.method public onSingleTapUp(I)V
    .locals 5
    .param p1, "slotIndex"    # I

    .prologue
    const/4 v4, -0x1

    .line 393
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mIsSelectioMode:Z

    if-eqz v3, :cond_2

    .line 394
    if-eq p1, v4, :cond_0

    .line 395
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v2

    .line 396
    .local v2, "soundUtils":Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    if-eqz v2, :cond_0

    .line 397
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 400
    .end local v2    # "soundUtils":Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->playHaptic()V

    .line 401
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->selectItem(I)V

    .line 418
    :cond_1
    :goto_0
    return-void

    .line 402
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    invoke-interface {v3, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;->onSlotSelected(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 403
    if-eq p1, v4, :cond_3

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v3, :cond_3

    .line 404
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 406
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 407
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v3

    const/16 v4, 0x5304

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 408
    :cond_4
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setFocusIndex(I)V

    .line 409
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mHelpMode:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 410
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    const v4, 0x7f0f01c4

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 412
    .local v0, "currTextView":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e02c6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 413
    .local v1, "pinchInToZoomOutText":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 414
    const v3, 0x7f0e02c7

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 287
    const/4 v0, 0x1

    return v0
.end method

.method public onUserDobuleTap()V
    .locals 0

    .prologue
    .line 636
    return-void
.end method

.method public onUserFlingUp()V
    .locals 0

    .prologue
    .line 630
    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    .prologue
    .line 624
    return-void
.end method

.method public onUserInteractionBegin()V
    .locals 0

    .prologue
    .line 612
    return-void
.end method

.method public onUserInteractionEnd()V
    .locals 0

    .prologue
    .line 618
    return-void
.end method

.method protected onVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 242
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->onVisibilityChanged(I)V

    .line 243
    if-nez p1, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserInteraction()V

    .line 246
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 525
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->pause()V

    .line 526
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->pause()V

    .line 527
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->setGenericFocusIndex(I)V

    .line 528
    invoke-static {}, Lcom/sec/android/gallery3d/ui/MeasureHelper;->releaseInstance()V

    .line 529
    return-void
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 8
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 360
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    if-eqz v3, :cond_0

    .line 361
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/gallery3d/ui/FilmStripView;->renderChild(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 363
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    if-eqz v3, :cond_1

    .line 364
    iget v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScreenWidth:I

    int-to-double v4, v3

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getFilmStripLength()I

    move-result v3

    iget v6, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScreenWidth:I

    sub-int/2addr v3, v6

    int-to-double v6, v3

    div-double v0, v4, v6

    .line 365
    .local v0, "mRatio":D
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SlotView;->getScrollX()I

    move-result v3

    int-to-double v4, v3

    mul-double/2addr v4, v0

    double-to-int v2, v4

    .line 366
    .local v2, "position":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScreenWidth:I

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->setContentPosition(II)V

    .line 367
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/gallery3d/ui/FilmStripView;->renderChild(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 369
    .end local v0    # "mRatio":D
    .end local v2    # "position":I
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 370
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->resume()V

    .line 533
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->resume()V

    .line 536
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v0, :cond_0

    .line 537
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->updateItemSize()V

    .line 539
    :cond_0
    return-void
.end method

.method public selectAll(I)V
    .locals 1
    .param p1, "maxSlotIndex"    # I

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->selectAll(I)V

    .line 491
    return-void
.end method

.method public setFocusIndex(I)V
    .locals 1
    .param p1, "slotIndex"    # I

    .prologue
    .line 513
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->setFocusIndex(I)V

    .line 514
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mLoadingFinished:Z

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SlotView;->makeSlotVisible(I)V

    .line 517
    :cond_0
    return-void
.end method

.method public setGenericFocusIndex(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 331
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    if-eqz v1, :cond_1

    .line 332
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->getGenericFocusIndex()I

    move-result v1

    if-eq p1, v1, :cond_2

    const/4 v0, 0x1

    .line 333
    .local v0, "refresh":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->setGenericFocusIndex(I)V

    .line 334
    if-eqz v0, :cond_0

    .line 335
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->setGenericFocusIndex(I)V

    .line 337
    :cond_0
    if-ltz p1, :cond_1

    .line 338
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    :goto_1
    invoke-virtual {v2, v3, p1, v1}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/Object;ILcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 341
    .end local v0    # "refresh":Z
    :cond_1
    return-void

    .line 332
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 338
    .restart local v0    # "refresh":Z
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public setGroupIndex(J)V
    .locals 1
    .param p1, "groupIndex"    # J

    .prologue
    .line 591
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->setGroupIndex(J)V

    .line 594
    :cond_0
    return-void
.end method

.method public setListener(Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    .line 171
    return-void
.end method

.method public setLockTouchEvent(Z)V
    .locals 0
    .param p1, "lockTouch"    # Z

    .prologue
    .line 605
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mLockTouchEvent:Z

    .line 606
    return-void
.end method

.method public setOnGenericMotionListener(Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    .line 179
    return-void
.end method

.method public setStartIndex(I)V
    .locals 1
    .param p1, "slotIndex"    # I

    .prologue
    .line 520
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->setStartIndex(I)V

    .line 521
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SlotView;->setStartIndex(I)V

    .line 522
    return-void
.end method

.method public setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 175
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 773
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    if-eqz v0, :cond_0

    .line 775
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->hideScrollBar()V

    .line 778
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->setVisibility(I)V

    .line 779
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mIsInitialized:Z

    if-nez v0, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mFilmStripVisible:Z

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->startAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V

    .line 186
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setVisibility(I)V

    goto :goto_0
.end method

.method public unselectAll()V
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumView:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->unselectAll()V

    .line 495
    return-void
.end method

.method public updateAllItem()V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->updateAllItem()V

    .line 233
    :cond_0
    return-void
.end method

.method public updateItem(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->updateItem(I)V

    .line 228
    :cond_0
    return-void
.end method

.method public updateItemSize()V
    .locals 0

    .prologue
    .line 551
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->requestLayout()V

    .line 552
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->updateScrollWidth()V

    .line 553
    return-void
.end method

.method public updateLocalFaceImage(I)V
    .locals 1
    .param p1, "localImageId"    # I

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mAlbumDataAdapter:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->updateLocalFaceImage(I)V

    .line 238
    :cond_0
    return-void
.end method

.method public updateScrollWidth()V
    .locals 2

    .prologue
    .line 831
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getScreenWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getFilmStripLength()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 832
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->hideScrollBar()V

    .line 836
    :cond_0
    :goto_0
    return-void

    .line 835
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getScrollWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->setGripWidth(I)V

    goto :goto_0
.end method
