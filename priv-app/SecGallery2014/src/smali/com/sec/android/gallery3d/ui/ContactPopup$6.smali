.class Lcom/sec/android/gallery3d/ui/ContactPopup$6;
.super Ljava/lang/Object;
.source "ContactPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/ContactPopup;->reloadWidget()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/ContactPopup;)V
    .locals 0

    .prologue
    .line 591
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$6;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 594
    const-string v1, "ContactPopup"

    const-string v2, "Social button clicked!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$6;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 596
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$6;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$600(Lcom/sec/android/gallery3d/ui/ContactPopup;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 605
    :cond_0
    :goto_0
    return-void

    .line 599
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$6;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$602(Lcom/sec/android/gallery3d/ui/ContactPopup;Z)Z

    .line 600
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$6;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$1100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/widget/ImageButton;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 601
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$6;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$6;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$1100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getId()I

    move-result v2

    const/4 v3, 0x0

    # invokes: Lcom/sec/android/gallery3d/ui/ContactPopup;->resetButtonsClickable(IZ)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$800(Lcom/sec/android/gallery3d/ui/ContactPopup;IZ)V

    .line 602
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$6;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v1

    iget-object v0, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->lookupKey:Ljava/lang/String;

    .line 603
    .local v0, "lookupkey":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$6;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # invokes: Lcom/sec/android/gallery3d/ui/ContactPopup;->gotoSnsWallEx(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$1200(Lcom/sec/android/gallery3d/ui/ContactPopup;Ljava/lang/String;)V

    goto :goto_0
.end method
