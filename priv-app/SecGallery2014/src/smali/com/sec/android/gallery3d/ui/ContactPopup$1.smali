.class Lcom/sec/android/gallery3d/ui/ContactPopup$1;
.super Ljava/lang/Object;
.source "ContactPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/ContactPopup;->setContactData(Lcom/sec/samsung/gallery/access/contact/ContactData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/ContactPopup;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$1;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 213
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 214
    .local v1, "intent":Landroid/content/Intent;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fb://post/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$1;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/samsung/gallery/access/contact/ContactData;->messageId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 216
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$1;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :goto_0
    return-void

    .line 217
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "ContactPopup"

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
