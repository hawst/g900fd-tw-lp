.class public Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "AllInOneLocalAlbum.java"


# static fields
.field private static final COUNT_PROJECTION:[Ljava/lang/String;

.field private static final INVALID_COUNT:I = -0x1

.field private static final ORDER_BY_DATATAKEN_ASC:I = 0x1

.field private static final ORDER_BY_DATATAKEN_DESC:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AllInOneLocalAlbum"

.field private static final mFilesUri:Landroid/net/Uri;

.field private static final mImageUri:Landroid/net/Uri;

.field private static final mVideoUri:Landroid/net/Uri;

.field private static final mWatchUriImage:[Landroid/net/Uri;

.field private static final mWatchUriVideo:[Landroid/net/Uri;


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mBaseUri:Landroid/net/Uri;

.field private mCachedCount:I

.field private mIndexProjection:[Ljava/lang/String;

.field private mIsImage:Z

.field private mItemPath:Lcom/sec/android/gallery3d/data/Path;

.field private mMediaType:I

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mOrder:I

.field private mProjection:[Ljava/lang/String;

.field private final mResolver:Landroid/content/ContentResolver;

.field private mSlideshowSupport:Z

.field private mSupportShare:Z

.field private mWatchUri:[Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "count(*)"

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    .line 31
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mImageUri:Landroid/net/Uri;

    .line 32
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mVideoUri:Landroid/net/Uri;

    .line 33
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mFilesUri:Landroid/net/Uri;

    .line 34
    new-array v0, v3, [Landroid/net/Uri;

    sget-object v1, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mImageUri:Landroid/net/Uri;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mWatchUriImage:[Landroid/net/Uri;

    .line 35
    new-array v0, v3, [Landroid/net/Uri;

    sget-object v1, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mVideoUri:Landroid/net/Uri;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mWatchUriVideo:[Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 3
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "type"    # I

    .prologue
    const/4 v2, 0x1

    .line 53
    invoke-static {}, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p2, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mCachedCount:I

    .line 48
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mSupportShare:Z

    .line 49
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mSlideshowSupport:Z

    .line 54
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 55
    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mResolver:Landroid/content/ContentResolver;

    .line 56
    iput p3, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mMediaType:I

    .line 57
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->initInfoByType(I)V

    .line 58
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mWatchUri:[Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p1}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;[Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 59
    return-void
.end method

.method private getExcludeClause(I)Ljava/lang/String;
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 190
    packed-switch p1, :pswitch_data_0

    .line 196
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_data not like\'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CLOUD_CACHE_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "exclude":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 192
    .end local v0    # "exclude":Ljava/lang/String;
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_data not like\'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CLOUD_CACHE_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 193
    .restart local v0    # "exclude":Ljava/lang/String;
    goto :goto_0

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method private getIndexOfPath(Lcom/sec/android/gallery3d/data/Path;II)I
    .locals 12
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "start"    # I
    .param p3, "count"    # I

    .prologue
    const/4 v11, -0x1

    .line 225
    if-nez p1, :cond_0

    .line 253
    :goto_0
    return v11

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 229
    .local v1, "uri":Landroid/net/Uri;
    iget v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mMediaType:I

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->getOrderBy(I)Ljava/lang/String;

    move-result-object v5

    .line 230
    .local v5, "orderClause":Ljava/lang/String;
    const/4 v6, 0x0

    .line 231
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 232
    .local v8, "index":I
    const/4 v9, 0x0

    .line 234
    .local v9, "isFound":Z
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mIndexProjection:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mMediaType:I

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->getExcludeClause(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 236
    if-nez v6, :cond_1

    .line 237
    const-string v0, "AllInOneLocalAlbum"

    const-string v2, "query fail: getIndexOfPath"

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 240
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 241
    .local v10, "targetId":I
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 242
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-ne v10, v0, :cond_3

    .line 243
    const/4 v9, 0x1

    .line 251
    :cond_2
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 253
    .end local v10    # "targetId":I
    :goto_2
    if-eqz v9, :cond_4

    .end local v8    # "index":I
    :goto_3
    move v11, v8

    goto :goto_0

    .line 246
    .restart local v8    # "index":I
    .restart local v10    # "targetId":I
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 248
    .end local v10    # "targetId":I
    :catch_0
    move-exception v7

    .line 249
    .local v7, "e":Ljava/lang/NumberFormatException;
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 251
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_2

    .end local v7    # "e":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    :cond_4
    move v8, v11

    .line 253
    goto :goto_3
.end method

.method private getOrderBy(I)Ljava/lang/String;
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 152
    packed-switch p1, :pswitch_data_0

    .line 159
    const-string v0, "datetaken DESC, _id DESC"

    .line 163
    .local v0, "orderby":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 154
    .end local v0    # "orderby":Ljava/lang/String;
    :pswitch_0
    const-string v0, "datetaken DESC, _id DESC"

    .line 156
    .restart local v0    # "orderby":Ljava/lang/String;
    goto :goto_0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method private initInfoByType(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 167
    packed-switch p1, :pswitch_data_0

    .line 178
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mIsImage:Z

    .line 179
    sget-object v0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mImageUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mBaseUri:Landroid/net/Uri;

    .line 180
    sget-object v0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mWatchUriImage:[Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mWatchUri:[Landroid/net/Uri;

    .line 181
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 182
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mProjection:[Ljava/lang/String;

    .line 183
    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mIndexProjection:[Ljava/lang/String;

    .line 186
    :goto_0
    return-void

    .line 169
    :pswitch_0
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mIsImage:Z

    .line 170
    sget-object v0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mVideoUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mBaseUri:Landroid/net/Uri;

    .line 171
    sget-object v0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mWatchUriVideo:[Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mWatchUri:[Landroid/net/Uri;

    .line 172
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 173
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalVideo;->PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mProjection:[Ljava/lang/String;

    .line 174
    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mIndexProjection:[Ljava/lang/String;

    goto :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getIndexOfItemEx(Lcom/sec/android/gallery3d/data/Path;I)I
    .locals 6
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "hint"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 204
    const/16 v0, 0x1388

    .line 207
    .local v0, "batchCnt":I
    add-int/lit16 v5, p2, -0x9c4

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 208
    .local v2, "start":I
    invoke-direct {p0, p1, v2, v0}, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->getIndexOfPath(Lcom/sec/android/gallery3d/data/Path;II)I

    move-result v1

    .line 209
    .local v1, "index":I
    if-eq v1, v4, :cond_0

    .line 210
    add-int v3, v2, v1

    .line 219
    :goto_0
    return v3

    .line 213
    :cond_0
    if-nez v2, :cond_1

    move v2, v0

    .line 215
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->getMediaItemCount()I

    move-result v3

    if-le v2, v3, :cond_2

    move v3, v4

    .line 216
    goto :goto_0

    :cond_1
    move v2, v3

    .line 213
    goto :goto_1

    .line 217
    :cond_2
    invoke-direct {p0, p1, v2, v0}, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->getIndexOfPath(Lcom/sec/android/gallery3d/data/Path;II)I

    move-result v1

    .line 218
    if-eq v1, v4, :cond_3

    .line 219
    add-int v3, v2, v1

    goto :goto_0

    .line 220
    :cond_3
    add-int/2addr v2, v0

    goto :goto_1
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 13
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 94
    .local v8, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 96
    .local v1, "uri":Landroid/net/Uri;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 97
    .local v12, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mMediaType:I

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->getOrderBy(I)Ljava/lang/String;

    move-result-object v5

    .line 98
    .local v5, "orderClause":Ljava/lang/String;
    const/4 v7, 0x0

    .line 100
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mProjection:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mMediaType:I

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->getExcludeClause(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 102
    if-nez v7, :cond_0

    .line 103
    const-string v0, "AllInOneLocalAlbum"

    const-string v2, "query fail: "

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 125
    :goto_0
    return-object v12

    .line 107
    :cond_0
    :goto_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 109
    .local v10, "id":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 110
    .local v6, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mIsImage:Z

    invoke-static {v6, v7, v8, v0, v2}, Lcom/sec/android/gallery3d/data/LocalAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v11

    .line 112
    .local v11, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v11, :cond_0

    .line 114
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 116
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v10    # "id":I
    .end local v11    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :catch_0
    move-exception v9

    .line 118
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 123
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_1
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 119
    :catch_1
    move-exception v9

    .line 121
    .local v9, "e":Landroid/database/CursorWindowAllocationException;
    :try_start_3
    invoke-virtual {v9}, Landroid/database/CursorWindowAllocationException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 123
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v9    # "e":Landroid/database/CursorWindowAllocationException;
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getMediaItemCount()I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 72
    iget v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mCachedCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 73
    const/4 v6, 0x0

    .line 75
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mBaseUri:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mMediaType:I

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->getExcludeClause(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 77
    if-nez v6, :cond_0

    .line 78
    const-string v0, "AllInOneLocalAlbum"

    const-string v1, "query fail"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v7

    .line 88
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v0

    .line 81
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 82
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mCachedCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 88
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mCachedCount:I

    goto :goto_0

    .line 84
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    const-string v0, "LocalAll"

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 4

    .prologue
    .line 139
    const-wide v0, 0x400000200405L

    .line 142
    .local v0, "supported":J
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mSupportShare:Z

    if-nez v2, :cond_0

    .line 143
    const-wide/16 v2, -0x5

    and-long/2addr v0, v2

    .line 144
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mSlideshowSupport:Z

    if-eqz v2, :cond_1

    .line 145
    const-wide v2, -0x400000000001L

    and-long/2addr v0, v2

    .line 147
    :cond_1
    return-wide v0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mCachedCount:I

    .line 65
    invoke-static {}, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mDataVersion:J

    .line 67
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/AllInOneLocalAlbum;->mDataVersion:J

    return-wide v0
.end method
