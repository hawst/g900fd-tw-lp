.class Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;
.super Ljava/lang/Object;
.source "NearbyDeviceCoverItem.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HttpImageRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private mType:I

.field private mUriString:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Ljava/lang/String;I)V
    .locals 5
    .param p2, "uriString"    # Ljava/lang/String;
    .param p3, "type"    # I

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->mUriString:Ljava/lang/String;

    .line 110
    iput p3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->mType:I

    .line 112
    if-eqz p2, :cond_0

    const-string/jumbo v3, "wlan0"

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDevice:Lcom/samsung/android/allshare/Device;
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$100(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Lcom/samsung/android/allshare/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/allshare/Device;->getNIC()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 113
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "host":Ljava/lang/String;
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPort()I

    move-result v1

    .line 116
    .local v1, "port":I
    new-instance v2, Lorg/apache/http/HttpHost;

    invoke-direct {v2, v0, v1}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    .line 117
    .local v2, "proxy":Lorg/apache/http/HttpHost;
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 118
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v3

    const-string v4, "http.route.default-proxy"

    invoke-interface {v3, v4, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 120
    .end local v0    # "host":Ljava/lang/String;
    .end local v1    # "port":I
    .end local v2    # "proxy":Lorg/apache/http/HttpHost;
    :cond_0
    return-void
.end method


# virtual methods
.method initHttpClient()V
    .locals 5

    .prologue
    .line 194
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    const-string v4, "Nearby Cover Item"

    invoke-static {v4}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v4

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Landroid/net/http/AndroidHttpClient;)Landroid/net/http/AndroidHttpClient;

    .line 195
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->mUriString:Ljava/lang/String;

    if-eqz v3, :cond_0

    const-string/jumbo v3, "wlan0"

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDevice:Lcom/samsung/android/allshare/Device;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$100(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Lcom/samsung/android/allshare/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/allshare/Device;->getNIC()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 196
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->mUriString:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "host":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->mUriString:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPort()I

    move-result v1

    .line 199
    .local v1, "port":I
    new-instance v2, Lorg/apache/http/HttpHost;

    invoke-direct {v2, v0, v1}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    .line 200
    .local v2, "proxy":Lorg/apache/http/HttpHost;
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v3

    const-string v4, "http.route.default-proxy"

    invoke-interface {v3, v4, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 202
    .end local v0    # "host":Ljava/lang/String;
    .end local v1    # "port":I
    .end local v2    # "proxy":Lorg/apache/http/HttpHost;
    :cond_0
    return-void
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 24
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 124
    const/4 v14, 0x0

    .line 125
    .local v14, "bitmap":Landroid/graphics/Bitmap;
    const/16 v18, 0x0

    .line 126
    .local v18, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$300(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v2

    .line 128
    .local v2, "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->get()Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    move-result-object v7

    .line 130
    .local v7, "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$400(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDataVersion:J
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$500(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->mType:I

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/gallery3d/data/ImageCacheService;->getImageData(Lcom/sec/android/gallery3d/data/Path;JILcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z

    move-result v17

    .line 131
    .local v17, "found":Z
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    .line 143
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 190
    :goto_0
    return-object v3

    .line 132
    :cond_0
    if-eqz v17, :cond_2

    .line 133
    :try_start_1
    new-instance v21, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 134
    .local v21, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v21

    iput-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 135
    iget-object v3, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    iget v4, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    iget v5, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v3, v4, v5, v1}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodeUsingPool(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;[BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 136
    if-nez v14, :cond_1

    .line 137
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$600()Ljava/lang/String;

    move-result-object v3

    const-string v4, "decode cached failed "

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    const/4 v3, 0x0

    .line 143
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    goto :goto_0

    .line 140
    :cond_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->convertToThumbnail(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v3, v14}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$700(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .line 143
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    goto :goto_0

    .end local v21    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 147
    :try_start_3
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$600()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mUriString : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->mUriString:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    new-instance v19, Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->mUriString:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 149
    .end local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .local v19, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v3

    if-nez v3, :cond_3

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->initHttpClient()V

    .line 152
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v22

    .line 153
    .local v22, "response":Lorg/apache/http/HttpResponse;
    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v23

    .line 154
    .local v23, "statusCode":I
    const/16 v3, 0xc8

    move/from16 v0, v23

    if-eq v0, v3, :cond_5

    .line 155
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$600()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " while retrieving bitmap from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->mUriString:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 157
    const/4 v3, 0x0

    .line 185
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 187
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Landroid/net/http/AndroidHttpClient;)Landroid/net/http/AndroidHttpClient;

    :cond_4
    move-object/from16 v18, v19

    .end local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto/16 :goto_0

    .line 143
    .end local v17    # "found":Z
    .end local v22    # "response":Lorg/apache/http/HttpResponse;
    .end local v23    # "statusCode":I
    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    throw v3

    .line 160
    .end local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v17    # "found":Z
    .restart local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v22    # "response":Lorg/apache/http/HttpResponse;
    .restart local v23    # "statusCode":I
    :cond_5
    :try_start_5
    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result-object v16

    .line 161
    .local v16, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v16, :cond_e

    .line 162
    const/16 v20, 0x0

    .line 164
    .local v20, "inputStream":Ljava/io/InputStream;
    :try_start_6
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v20

    .line 165
    invoke-static/range {v20 .. v20}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 166
    if-nez v14, :cond_8

    .line 167
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$600()Ljava/lang/String;

    move-result-object v3

    const-string v4, "can not decode inputstream"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 168
    const/4 v3, 0x0

    .line 174
    if-eqz v20, :cond_6

    .line 175
    :try_start_7
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    .line 176
    :cond_6
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 185
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 187
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Landroid/net/http/AndroidHttpClient;)Landroid/net/http/AndroidHttpClient;

    :cond_7
    move-object/from16 v18, v19

    .end local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto/16 :goto_0

    .line 170
    .end local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :cond_8
    :try_start_8
    invoke-static {v14}, Lcom/sec/android/gallery3d/common/BitmapUtils;->compressBitmap(Landroid/graphics/Bitmap;)[B

    move-result-object v13

    .line 171
    .local v13, "array":[B
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$800(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDataVersion:J
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$900(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->mType:I

    move-object v8, v2

    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/gallery3d/data/ImageCacheService;->putImageData(Lcom/sec/android/gallery3d/data/Path;JI[B)V

    .line 172
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->convertToThumbnail(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v3, v14}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$700(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result-object v3

    .line 174
    if-eqz v20, :cond_9

    .line 175
    :try_start_9
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    .line 176
    :cond_9
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 185
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 187
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Landroid/net/http/AndroidHttpClient;)Landroid/net/http/AndroidHttpClient;

    :cond_a
    move-object/from16 v18, v19

    .end local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto/16 :goto_0

    .line 174
    .end local v13    # "array":[B
    .end local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :catchall_1
    move-exception v3

    if-eqz v20, :cond_b

    .line 175
    :try_start_a
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    .line 176
    :cond_b
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->consumeContent()V

    throw v3
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 179
    .end local v16    # "entity":Lorg/apache/http/HttpEntity;
    .end local v20    # "inputStream":Ljava/io/InputStream;
    .end local v22    # "response":Lorg/apache/http/HttpResponse;
    .end local v23    # "statusCode":I
    :catch_0
    move-exception v15

    move-object/from16 v18, v19

    .line 180
    .end local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .local v15, "e":Ljava/lang/Exception;
    .restart local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :goto_1
    if-eqz v18, :cond_c

    .line 181
    :try_start_b
    invoke-virtual/range {v18 .. v18}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 182
    :cond_c
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$600()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Error while retrieving nearby cover item"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 185
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v3

    if-eqz v3, :cond_d

    .line 186
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 187
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Landroid/net/http/AndroidHttpClient;)Landroid/net/http/AndroidHttpClient;

    .line 190
    .end local v15    # "e":Ljava/lang/Exception;
    :cond_d
    :goto_2
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 185
    .end local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v16    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v22    # "response":Lorg/apache/http/HttpResponse;
    .restart local v23    # "statusCode":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v3

    if-eqz v3, :cond_10

    .line 186
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 187
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Landroid/net/http/AndroidHttpClient;)Landroid/net/http/AndroidHttpClient;

    move-object/from16 v18, v19

    .end local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto :goto_2

    .line 185
    .end local v16    # "entity":Lorg/apache/http/HttpEntity;
    .end local v22    # "response":Lorg/apache/http/HttpResponse;
    .end local v23    # "statusCode":I
    :catchall_2
    move-exception v3

    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v4

    if-eqz v4, :cond_f

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 187
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Landroid/net/http/AndroidHttpClient;)Landroid/net/http/AndroidHttpClient;

    :cond_f
    throw v3

    .line 185
    .end local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :catchall_3
    move-exception v3

    move-object/from16 v18, v19

    .end local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto :goto_3

    .line 179
    :catch_1
    move-exception v15

    goto :goto_1

    .end local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v16    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v22    # "response":Lorg/apache/http/HttpResponse;
    .restart local v23    # "statusCode":I
    :cond_10
    move-object/from16 v18, v19

    .end local v19    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v18    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto :goto_2
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 102
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
