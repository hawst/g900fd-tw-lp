.class Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;
.super Ljava/lang/Object;
.source "FaceIndicatorView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->showAlertDialog(Landroid/content/Context;ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)V
    .locals 0

    .prologue
    .line 2680
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v6, 0x1

    .line 2684
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.android.camera.action.CROP"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2685
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2686
    const-string v3, "is-caller-id"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2687
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRectScale:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2300(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/graphics/RectF;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2688
    const/4 v3, 0x4

    new-array v2, v3, [F

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRectScale:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2300(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->left:F

    aput v4, v2, v3

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRectScale:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2300(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/graphics/RectF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/RectF;->top:F

    aput v3, v2, v6

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRectScale:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2300(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->right:F

    aput v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRectScale:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2300(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    aput v4, v2, v3

    .line 2689
    .local v2, "values":[F
    const-string v3, "face-position"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[F)Landroid/content/Intent;

    .line 2690
    const-string v3, "info"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "left = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRectScale:Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2300(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->left:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", top = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRectScale:Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2300(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", right = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRectScale:Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2300(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->right:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bottom = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRectScale:Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2300(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2696
    .end local v2    # "values":[F
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$900(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    .line 2697
    .local v0, "data":Landroid/net/Uri;
    const-string v3, "info"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "data = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2698
    const-string v3, "aspectX"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2699
    const-string v3, "aspectY"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2700
    const-string v3, "outputX"

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getCropSizeforSetCallerID(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2701
    const-string v3, "outputY"

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getCropSizeforSetCallerID(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2702
    const-string v3, "return-data"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2703
    const-string v3, "output"

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CropImage;->getCallerIdSavePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2705
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2706
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    const/16 v4, 0x305

    invoke-virtual {v3, v1, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2707
    return-void
.end method
