.class Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment$1;
.super Ljava/lang/Object;
.source "AccountSettingMainFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 140
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefXmls:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 141
    .local v0, "prefXml":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    const v1, 0x7f070009

    if-ne v0, v1, :cond_0

    .line 143
    const v0, 0x7f07000a

    .line 147
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;

    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefNames:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->access$100(Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->launchDetailFragment(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;

    # setter for: Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActiveItemIndex:I
    invoke-static {v1, p3}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->access$202(Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;I)I

    .line 149
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;

    # setter for: Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActivePreference:I
    invoke-static {v1, v0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->access$302(Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;I)I

    .line 150
    if-eqz p2, :cond_1

    .line 151
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Landroid/view/View;->setActivated(Z)V

    .line 154
    :cond_1
    return-void
.end method
