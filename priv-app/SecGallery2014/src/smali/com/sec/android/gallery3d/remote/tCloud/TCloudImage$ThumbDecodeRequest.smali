.class Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;
.super Ljava/lang/Object;
.source "TCloudImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ThumbDecodeRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final mType:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;I)V
    .locals 0
    .param p2, "type"    # I

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput p2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->mType:I

    .line 167
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    .line 170
    const/4 v0, 0x0

    .line 172
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v3, v4

    .line 205
    :goto_0
    return-object v3

    .line 175
    :cond_0
    invoke-interface {p1, v10}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    move-result v3

    if-nez v3, :cond_1

    move-object v3, v4

    .line 176
    goto :goto_0

    .line 179
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->access$100(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    move-result-object v5

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->access$000(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    iget-wide v6, v6, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->id:J

    invoke-virtual {v5, v3, v6, v7}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->getThumbnail(Landroid/content/Context;J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 184
    :goto_1
    if-nez v0, :cond_2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->cachedPath:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 185
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 186
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 187
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->cachedPath:Ljava/lang/String;

    const/16 v5, 0x400

    iget v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->mType:I

    invoke-static {p1, v3, v2, v5, v6}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodeThumbnail(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 190
    .end local v2    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_2
    invoke-interface {p1, v8}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    move-result v3

    if-nez v3, :cond_3

    move-object v3, v4

    .line 191
    goto :goto_0

    .line 180
    :catch_0
    move-exception v1

    .line 181
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 193
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_3
    if-eqz v0, :cond_5

    .line 194
    iget v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->mType:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_6

    .line 195
    iget v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->mType:I

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v3

    invoke-static {v0, v3, v8}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 200
    :cond_4
    :goto_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    iget v3, v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    if-eqz v3, :cond_5

    .line 201
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    iget v3, v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    invoke-static {v0, v3, v9}, Lcom/sec/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_5
    move-object v3, v0

    .line 205
    goto :goto_0

    .line 196
    :cond_6
    iget v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->mType:I

    if-ne v3, v10, :cond_4

    .line 197
    iget v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->mType:I

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v3

    invoke-static {v0, v3, v8}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 198
    invoke-static {v0, v9}, Lcom/sec/android/gallery3d/common/BitmapUtils;->cropThumnailBitmap(Landroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 162
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
