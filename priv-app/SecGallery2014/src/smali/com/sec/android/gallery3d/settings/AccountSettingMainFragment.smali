.class public Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;
.super Landroid/app/Fragment;
.source "AccountSettingMainFragment.java"

# interfaces
.implements Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;


# static fields
.field public static final BACK_STACK_PREFS:Ljava/lang/String; = ":android:prefs"

.field public static final PREVIOUSLY_SELECTED_ITEM:Ljava/lang/String; = "PREVIOUSLY_SELECTED_ITEM"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private PREF_NAMES:[I

.field private PREF_XMLS:[I

.field private mActiveItemIndex:I

.field private mActivePreference:I

.field private mAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

.field private mListView:Landroid/widget/ListView;

.field private final mPrefNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPrefXmls:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActiveItemIndex:I

    .line 31
    iput v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActivePreference:I

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefXmls:Ljava/util/ArrayList;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefNames:Ljava/util/ArrayList;

    .line 41
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->PREF_NAMES:[I

    .line 51
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->PREF_XMLS:[I

    .line 64
    iput-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    .line 65
    iput-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    .line 66
    return-void

    .line 41
    nop

    :array_0
    .array-data 4
        0x7f0e026b
        0x7f0e027c
        0x7f0e0179
        0x7f0e0277
        0x7f0e00a8
        0x7f0e027e
        0x7f0e0308
    .end array-data

    .line 51
    :array_1
    .array-data 4
        0x7f070016
        0x7f07000b
        0x7f07000f
        0x7f070000
        0x7f070009
        0x7f070008
        0x7f070007
        0x7f070001
        0x7f07000c
    .end array-data
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/settings/AccountSettingManager;Landroid/app/FragmentBreadCrumbs;)V
    .locals 1
    .param p1, "manager"    # Lcom/sec/android/gallery3d/settings/AccountSettingManager;
    .param p2, "fragmentBreadCrumbs"    # Landroid/app/FragmentBreadCrumbs;

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActiveItemIndex:I

    .line 31
    iput v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActivePreference:I

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefXmls:Ljava/util/ArrayList;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefNames:Ljava/util/ArrayList;

    .line 41
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->PREF_NAMES:[I

    .line 51
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->PREF_XMLS:[I

    .line 69
    iput-object p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    .line 70
    iput-object p2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->getSelectedPreferenceItem()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActivePreference:I

    .line 72
    return-void

    .line 41
    :array_0
    .array-data 4
        0x7f0e026b
        0x7f0e027c
        0x7f0e0179
        0x7f0e0277
        0x7f0e00a8
        0x7f0e027e
        0x7f0e0308
    .end array-data

    .line 51
    :array_1
    .array-data 4
        0x7f070016
        0x7f07000b
        0x7f07000f
        0x7f070000
        0x7f070009
        0x7f070008
        0x7f070007
        0x7f070001
        0x7f07000c
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefXmls:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefNames:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActiveItemIndex:I

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActivePreference:I

    return p1
.end method

.method private showBreadCrumbs(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v1, 0x0

    .line 251
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    invoke-virtual {v0, p1, v1}, Landroid/app/FragmentBreadCrumbs;->setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    invoke-virtual {v0, v1, v1, v1}, Landroid/app/FragmentBreadCrumbs;->setParentTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 255
    :cond_0
    return-void
.end method

.method private updateList()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 176
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    if-nez v3, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefXmls:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 181
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefNames:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 183
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->PREF_NAMES:[I

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 184
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->PREF_NAMES:[I

    aget v1, v3, v0

    .line 185
    .local v1, "nameId":I
    sparse-switch v1, :sswitch_data_0

    .line 225
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefNames:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefXmls:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->PREF_XMLS:[I

    aget v4, v4, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 187
    :sswitch_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->getAuthAccounts()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 192
    :sswitch_1
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsWifiOnlyModel:Z

    if-nez v3, :cond_3

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSnsSettings:Z

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOXInstallMode(Landroid/content/Context;)Z

    move-result v3

    if-ne v3, v5, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 198
    :sswitch_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->isDropboxSignIn()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_2

    .line 203
    :sswitch_3
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-nez v3, :cond_2

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-nez v3, :cond_2

    goto :goto_2

    .line 208
    :sswitch_4
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSoundScene:Z

    if-eqz v3, :cond_3

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSupportSoundAndShot:Z

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOXInstallMode(Landroid/content/Context;)Z

    move-result v3

    if-ne v3, v5, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 214
    :sswitch_5
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialPhotoAlbum:Z

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOXInstallMode(Landroid/content/Context;)Z

    move-result v3

    if-ne v3, v5, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 219
    :sswitch_6
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-nez v3, :cond_2

    goto/16 :goto_2

    .line 228
    .end local v1    # "nameId":I
    :cond_4
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3}, Landroid/widget/ArrayAdapter;->clear()V

    .line 229
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefNames:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 231
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mListView:Landroid/widget/ListView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefNames:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 232
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_5

    iget v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActivePreference:I

    const v4, 0x7f07000a

    if-ne v3, v4, :cond_5

    .line 233
    const v3, 0x7f070009

    iput v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActivePreference:I

    .line 235
    :cond_5
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefXmls:Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActivePreference:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 236
    .local v2, "pos":I
    if-gez v2, :cond_6

    .line 237
    iget v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActiveItemIndex:I

    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefNames:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 239
    :cond_6
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mListView:Landroid/widget/ListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setSoundEffectsEnabled(Z)V

    .line 240
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mListView:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    int-to-long v6, v2

    invoke-virtual {v3, v4, v2, v6, v7}, Landroid/widget/ListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 241
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setSoundEffectsEnabled(Z)V

    goto/16 :goto_0

    .line 185
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0e00a8 -> :sswitch_3
        0x7f0e0179 -> :sswitch_6
        0x7f0e026b -> :sswitch_0
        0x7f0e0277 -> :sswitch_2
        0x7f0e027c -> :sswitch_1
        0x7f0e027e -> :sswitch_4
        0x7f0e0308 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public launchDetailFragment(Ljava/lang/String;I)Z
    .locals 9
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "resourceId"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 75
    iget-object v6, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    if-nez v6, :cond_0

    .line 105
    :goto_0
    return v4

    .line 79
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->showBreadCrumbs(Ljava/lang/CharSequence;)V

    .line 80
    new-instance v1, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    iget-object v6, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-direct {v1, v6, p2}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingManager;I)V

    .line 91
    .local v1, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 92
    .local v3, "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, ":android:prefs"

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 93
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 94
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/FragmentManager;->popBackStack()V

    .line 94
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 98
    .end local v2    # "i":I
    :cond_1
    const v6, 0x7f0f000a

    invoke-virtual {v3, v6, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 99
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 100
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v4, v5

    .line 101
    goto :goto_0

    .line 102
    .end local v1    # "fragment":Landroid/app/Fragment;
    .end local v3    # "transaction":Landroid/app/FragmentTransaction;
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to switch to fragment "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 117
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->registerOnAccountUpdatedListener(Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;)V

    .line 121
    :cond_0
    return-void
.end method

.method public onCloudStatusUpdated(ZZ)V
    .locals 0
    .param p1, "isDropboxSignIn"    # Z
    .param p2, "isCloudContentSyncOn"    # Z

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->updateList()V

    .line 126
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 130
    const v1, 0x7f0300ce

    invoke-virtual {p1, v1, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 131
    .local v0, "rootView":Landroid/view/View;
    const v1, 0x7f0f020e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mListView:Landroid/widget/ListView;

    .line 133
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mListView:Landroid/widget/ListView;

    if-eqz v1, :cond_0

    .line 134
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0300cd

    const v4, 0x7f0f002f

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    .line 136
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mListView:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment$1;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 156
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->updateList()V

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefNames:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mPrefXmls:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->launchDetailFragment(Ljava/lang/String;I)Z

    .line 159
    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->unregisterOnAccountUpdatedListener(Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;)V

    .line 167
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 168
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 111
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    iget v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->mActivePreference:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->setSelectedPreferenceItem(I)V

    .line 113
    return-void
.end method

.method public onSyncAccontsUpdated(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 172
    .local p1, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;>;"
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;->updateList()V

    .line 173
    return-void
.end method
