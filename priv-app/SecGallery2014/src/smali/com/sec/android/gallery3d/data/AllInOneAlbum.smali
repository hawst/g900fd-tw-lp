.class public Lcom/sec/android/gallery3d/data/AllInOneAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "AllInOneAlbum.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# instance fields
.field private final mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mIsTimeOrder:Z

.field private final mSubSets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/Path;)V
    .locals 2
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 21
    invoke-static {}, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p2, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mSubSets:Ljava/util/ArrayList;

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mIsTimeOrder:Z

    .line 22
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 24
    return-void
.end method

.method private addMediaSets(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 4
    .param p1, "set"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 149
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mSubSets:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mIsTimeOrder:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 153
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_2

    .line 154
    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->addMediaSets(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 153
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 157
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "i":I
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v1

    .local v1, "n":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 158
    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->addMediaSets(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 161
    .end local v1    # "n":I
    :cond_2
    return-void
.end method


# virtual methods
.method public addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/data/ContentListener;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 166
    return-void
.end method

.method public getCameraBucketId()I
    .locals 2

    .prologue
    .line 189
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mSubSets:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mSubSets:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->isCameraPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mSubSets:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v1

    .line 194
    :goto_1
    return v1

    .line 189
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 194
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 7
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 81
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v0, 0x0

    .line 83
    .local v0, "added":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v4, 0x0

    .line 84
    .local v4, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    move v3, p2

    .line 85
    .local v3, "remain":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mSubSets:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_5

    .line 86
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mSubSets:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 87
    .restart local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v4, :cond_1

    .line 85
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 90
    :cond_1
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    .line 91
    .local v5, "subCount":I
    if-le p1, v5, :cond_2

    .line 92
    sub-int/2addr p1, v5

    .line 93
    goto :goto_1

    .line 95
    :cond_2
    sub-int/2addr v5, p1

    .line 96
    if-le v5, v3, :cond_3

    .line 97
    move v5, v3

    .line 98
    :cond_3
    invoke-virtual {v4, p1, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_4

    .line 100
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 101
    :cond_4
    const/4 p1, 0x0

    .line 102
    sub-int/2addr v3, v5

    .line 103
    if-gtz v3, :cond_0

    .line 110
    .end local v5    # "subCount":I
    :cond_5
    return-object v2
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v0

    return v0
.end method

.method public getMediaItemEx(II)Ljava/util/ArrayList;
    .locals 7
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 116
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v0, 0x0

    .line 117
    .local v0, "added":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v4, 0x0

    .line 118
    .local v4, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    move v3, p2

    .line 119
    .local v3, "remain":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mSubSets:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_5

    .line 120
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mSubSets:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 121
    .restart local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v4, :cond_1

    .line 119
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 124
    :cond_1
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    .line 125
    .local v5, "subCount":I
    if-le p1, v5, :cond_2

    .line 126
    sub-int/2addr p1, v5

    .line 127
    goto :goto_1

    .line 129
    :cond_2
    sub-int/2addr v5, p1

    .line 130
    if-le v5, v3, :cond_3

    .line 131
    move v5, v3

    .line 132
    :cond_3
    invoke-virtual {v4, p1, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemEx(II)Ljava/util/ArrayList;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_4

    .line 134
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 135
    :cond_4
    const/4 p1, 0x0

    .line 136
    sub-int/2addr v3, v5

    .line 137
    if-gtz v3, :cond_0

    .line 144
    .end local v5    # "subCount":I
    :cond_5
    return-object v2
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    const-string v0, "All"

    return-object v0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->notifyContentChanged()V

    .line 186
    return-void
.end method

.method public reload()J
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mDataVersion:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 62
    invoke-static {}, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mDataVersion:J

    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mSubSets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 64
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->addMediaSets(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 70
    :cond_0
    :goto_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mDataVersion:J

    return-wide v0

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mSubSets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->addMediaSets(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0
.end method

.method public removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/data/ContentListener;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 171
    return-void
.end method

.method public setSortByType(Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V
    .locals 10
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "type"    # I
    .param p3, "isTime"    # Z

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 28
    invoke-interface {p1, p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->setSortByType(I)V

    .line 29
    iput-boolean p3, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mIsTimeOrder:Z

    .line 30
    if-eqz p3, :cond_1

    .line 31
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    if-eqz v8, :cond_0

    .line 32
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v8, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->setForceReload(Z)V

    .line 57
    :cond_0
    return-void

    .line 35
    :cond_1
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/ComboAlbumSet;

    if-eqz v8, :cond_0

    .line 36
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v8, Lcom/sec/android/gallery3d/data/ComboAlbumSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->getMediaSets()[Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 37
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v4, :cond_3

    instance-of v8, v4, Lcom/sec/android/gallery3d/data/MtpDeviceSet;

    if-eqz v8, :cond_3

    move-object v8, v4

    .line 38
    check-cast v8, Lcom/sec/android/gallery3d/data/MtpDeviceSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->notifyDirty()V

    .line 39
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v6

    .local v6, "n":I
    :goto_1
    if-ge v1, v6, :cond_5

    .line 40
    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MtpDevice;

    .line 41
    .local v5, "mtpDevice":Lcom/sec/android/gallery3d/data/MtpDevice;
    if-eqz v5, :cond_2

    .line 42
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MtpDevice;->notifyDirty()V

    .line 39
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 45
    .end local v1    # "i":I
    .end local v5    # "mtpDevice":Lcom/sec/android/gallery3d/data/MtpDevice;
    .end local v6    # "n":I
    :cond_3
    if-eqz v4, :cond_5

    instance-of v8, v4, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    if-eqz v8, :cond_5

    move-object v8, v4

    .line 46
    check-cast v8, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;->notifyDirty()V

    .line 47
    const/4 v1, 0x0

    .restart local v1    # "i":I
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v6

    .restart local v6    # "n":I
    :goto_2
    if-ge v1, v6, :cond_5

    .line 48
    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    .line 49
    .local v7, "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    if-eqz v7, :cond_4

    .line 50
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->notifyDirty()V

    .line 47
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 36
    .end local v1    # "i":I
    .end local v6    # "n":I
    .end local v7    # "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
