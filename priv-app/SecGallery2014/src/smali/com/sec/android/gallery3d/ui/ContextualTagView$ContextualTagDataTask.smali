.class Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;
.super Landroid/os/AsyncTask;
.source "ContextualTagView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/ContextualTagView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContextualTagDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V
    .locals 1

    .prologue
    .line 1231
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1232
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mWeather:Ljava/lang/String;

    .line 1233
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDate:Ljava/lang/String;

    .line 1234
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    .line 1235
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    .line 1236
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategory:Ljava/lang/String;

    .line 1237
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    .line 1238
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1229
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v2, 0x0

    .line 1243
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # getter for: Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$500(Lcom/sec/android/gallery3d/ui/ContextualTagView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setIcon(Landroid/content/Context;)V

    .line 1244
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # invokes: Lcom/sec/android/gallery3d/ui/ContextualTagView;->setDate()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$600(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V

    .line 1246
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1247
    sput-object v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDate:Ljava/lang/String;

    .line 1248
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # setter for: Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$702(Lcom/sec/android/gallery3d/ui/ContextualTagView;Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;)Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 1281
    :goto_0
    return-object v2

    .line 1252
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # invokes: Lcom/sec/android/gallery3d/ui/ContextualTagView;->setFace()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$800(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V

    .line 1253
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1254
    sput-object v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    goto :goto_0

    .line 1258
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # invokes: Lcom/sec/android/gallery3d/ui/ContextualTagView;->setLocation()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$900(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V

    .line 1259
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1260
    sput-object v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    .line 1261
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # setter for: Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$1002(Lcom/sec/android/gallery3d/ui/ContextualTagView;Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;)Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto :goto_0

    .line 1265
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # invokes: Lcom/sec/android/gallery3d/ui/ContextualTagView;->setCategory()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$1100(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V

    .line 1266
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1267
    sput-object v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategory:Ljava/lang/String;

    .line 1268
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # setter for: Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$1202(Lcom/sec/android/gallery3d/ui/ContextualTagView;Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;)Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto :goto_0

    .line 1272
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    sget-object v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    # invokes: Lcom/sec/android/gallery3d/ui/ContextualTagView;->setUserTags(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$1300(Lcom/sec/android/gallery3d/ui/ContextualTagView;Ljava/lang/String;)V

    .line 1273
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1274
    sput-object v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    .line 1275
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # setter for: Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$1402(Lcom/sec/android/gallery3d/ui/ContextualTagView;Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;)Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto :goto_0

    .line 1279
    :cond_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->onContextualTag()V

    goto :goto_0
.end method
