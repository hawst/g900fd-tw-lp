.class Lcom/sec/android/gallery3d/glcore/GlLayer$2;
.super Ljava/lang/Object;
.source "GlLayer.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlLayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 0

    .prologue
    .line 562
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getListScrollMode()I
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getHoverListScrollMode()I

    move-result v0

    return v0
.end method

.method public onMove(III)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "direction"    # I

    .prologue
    .line 571
    invoke-static {p3}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->setHoveringIconTo(I)V

    .line 572
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onMoved(II)Z

    move-result v0

    return v0
.end method

.method public onPress(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 565
    invoke-static {}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->setHoveringIconToDefault()V

    .line 566
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onPressed(II)Z

    move-result v0

    return v0
.end method

.method public onRelease(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 577
    invoke-static {}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->setHoveringIconToDefault()V

    .line 578
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$2;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onReleased(IIII)Z

    move-result v0

    return v0
.end method
