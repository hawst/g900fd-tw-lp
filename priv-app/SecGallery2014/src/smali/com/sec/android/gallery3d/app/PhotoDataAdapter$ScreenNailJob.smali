.class Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;
.super Ljava/lang/Object;
.source "PhotoDataAdapter.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenNailJob"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/sec/android/gallery3d/ui/ScreenNail;",
        ">;"
    }
.end annotation


# instance fields
.field private mItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 1059
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1060
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1061
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 6
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1067
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v1

    .line 1068
    .local v1, "s":Lcom/sec/android/gallery3d/ui/ScreenNail;
    if-eqz v1, :cond_0

    .line 1085
    .end local v1    # "s":Lcom/sec/android/gallery3d/ui/ScreenNail;
    :goto_0
    return-object v1

    .line 1072
    .restart local v1    # "s":Lcom/sec/android/gallery3d/ui/ScreenNail;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->isTemporaryItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1073
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->newPlaceholderScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/ui/ScreenNail;
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v1

    goto :goto_0

    .line 1076
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v4

    invoke-interface {v4, p1}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1077
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v1, v3

    goto :goto_0

    .line 1078
    :cond_2
    if-eqz v0, :cond_3

    .line 1079
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v0, v4, v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1083
    :cond_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_4

    .line 1084
    .local v2, "useBitmapScreenNail":Z
    :goto_1
    if-nez v0, :cond_5

    move-object v1, v3

    goto :goto_0

    .line 1083
    .end local v2    # "useBitmapScreenNail":Z
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 1085
    .restart local v2    # "useBitmapScreenNail":Z
    :cond_5
    if-eqz v2, :cond_6

    new-instance v3, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    invoke-direct {v3, v0}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    :goto_2
    move-object v1, v3

    goto :goto_0

    :cond_6
    new-instance v3, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getTiledTextureResource()Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$500(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;-><init>(Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;)V

    goto :goto_2
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 1056
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    return-object v0
.end method
