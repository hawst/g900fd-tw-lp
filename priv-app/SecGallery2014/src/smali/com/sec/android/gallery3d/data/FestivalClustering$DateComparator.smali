.class Lcom/sec/android/gallery3d/data/FestivalClustering$DateComparator;
.super Ljava/lang/Object;
.source "FestivalClustering.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/FestivalClustering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DateComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/FestivalClustering;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/data/FestivalClustering;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/FestivalClustering$DateComparator;->this$0:Lcom/sec/android/gallery3d/data/FestivalClustering;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/data/FestivalClustering;Lcom/sec/android/gallery3d/data/FestivalClustering$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/data/FestivalClustering;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/data/FestivalClustering$1;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/FestivalClustering$DateComparator;-><init>(Lcom/sec/android/gallery3d/data/FestivalClustering;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)I
    .locals 4
    .param p1, "item1"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .param p2, "item2"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalClustering$DateComparator;->this$0:Lcom/sec/android/gallery3d/data/FestivalClustering;

    # getter for: Lcom/sec/android/gallery3d/data/FestivalClustering;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/gallery3d/data/FestivalClustering;->access$000(Lcom/sec/android/gallery3d/data/FestivalClustering;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 51
    iget-wide v0, p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    iget-wide v2, p2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->compare(JJ)I

    move-result v0

    .line 53
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    iget-wide v2, p2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->compare(JJ)I

    move-result v0

    neg-int v0, v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 48
    check-cast p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/data/FestivalClustering$DateComparator;->compare(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)I

    move-result v0

    return v0
.end method
