.class public Lcom/sec/android/gallery3d/data/LocalImage;
.super Lcom/sec/android/gallery3d/data/LocalMediaItem;
.source "LocalImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/LocalImage$Value;,
        Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;,
        Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;
    }
.end annotation


# static fields
.field public static final INDEX_BUCKET_ID:I = 0xa

.field public static final INDEX_CAPTION:I = 0x1

.field public static final INDEX_DATA:I = 0x8

.field public static final INDEX_DATE_ADDED:I = 0x6

.field public static final INDEX_DATE_MODIFIED:I = 0x7

.field public static final INDEX_DATE_TAKEN:I = 0x5

.field public static final INDEX_GROUP_ID:I = 0xe

.field public static final INDEX_HEIGHT:I = 0xd

.field public static final INDEX_ID:I = 0x0

.field public static final INDEX_LATITUDE:I = 0x3

.field public static final INDEX_LONGITUDE:I = 0x4

.field public static final INDEX_MIME_TYPE:I = 0x2

.field public static final INDEX_ORIENTATION:I = 0x9

.field public static final INDEX_SEF_FILE_TYPE:I = 0x10

.field public static final INDEX_SIZE:I = 0xb

.field public static final INDEX_SPHERICAL_MOSAIC:I = 0xf

.field public static final INDEX_WIDTH:I = 0xc

.field private static final INVALID_COUNT:I = -0x1

.field public static final ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field public static PROJECTION:[Ljava/lang/String;

.field private static final PROJECTION_SCAM:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isRotationSupoorted:Z

.field public isSoundScene:Z

.field private isSupportedByRegionDecoder:Z

.field private mArcHeight:I

.field private mArcWidth:I

.field private mAttributes:Lcom/sec/android/gallery3d/data/LocalImageAttributes;

.field mCachedBurstImageCount:I

.field private mFaceImagePath:Lcom/sec/android/gallery3d/data/Path;

.field private mFaceImagePathForExpansion:Lcom/sec/android/gallery3d/data/Path;

.field private mFaceImagePaths:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mFacePaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mImageNoteType:I

.field private mIsDeleted:Z

.field private mIsFaceInfoDeleted:Z

.field private mIsFaceScanned:Z

.field public mIsGolf:Z

.field private mIsRotateSearch:Z

.field private mPanoramaMetadata:Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;

.field private regionDecoderSupportedInitialized:Z

.field public rotation:I

.field private rotationSupportedInitialized:Z

.field public seffiletype:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 77
    const-class v0, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;

    .line 79
    const-string v0, "/local/image/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 105
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "title"

    aput-object v1, v0, v4

    const-string v1, "mime_type"

    aput-object v1, v0, v5

    const-string v1, "latitude"

    aput-object v1, v0, v6

    const-string v1, "longitude"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "date_added"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "orientation"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "group_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "spherical_mosaic"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "sef_file_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    .line 128
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "title"

    aput-object v1, v0, v4

    const-string v1, "mime_type"

    aput-object v1, v0, v5

    const-string v1, "latitude"

    aput-object v1, v0, v6

    const-string v1, "longitude"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "date_added"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "orientation"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "height"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "group_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION_SCAM:[Ljava/lang/String;

    .line 147
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION_SCAM:[Ljava/lang/String;

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    .line 153
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalImage;->updateWidthAndHeightProjection()V

    .line 154
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V
    .locals 8
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "id"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 197
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalImage;->nextVersionNumber()J

    move-result-wide v4

    invoke-direct {p0, p1, v4, v5, p2}, Lcom/sec/android/gallery3d/data/LocalMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;JLcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 165
    new-instance v4, Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;

    invoke-direct {v4, p0}, Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;-><init>(Lcom/sec/android/gallery3d/data/MediaObject;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mPanoramaMetadata:Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;

    .line 168
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsDeleted:Z

    .line 169
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsFaceInfoDeleted:Z

    .line 170
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsFaceScanned:Z

    .line 171
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsGolf:Z

    .line 172
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isSoundScene:Z

    .line 175
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotationSupportedInitialized:Z

    .line 178
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->regionDecoderSupportedInitialized:Z

    .line 181
    iput v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mImageNoteType:I

    .line 183
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsRotateSearch:Z

    .line 1350
    iput-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePath:Lcom/sec/android/gallery3d/data/Path;

    .line 1351
    iput-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePathForExpansion:Lcom/sec/android/gallery3d/data/Path;

    .line 1352
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    .line 1414
    iput v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcWidth:I

    iput v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcHeight:I

    .line 1456
    iput-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    .line 1567
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mCachedBurstImageCount:I

    .line 198
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 199
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 200
    .local v3, "uri":Landroid/net/Uri;
    const/4 v0, 0x0

    .line 202
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v4, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    invoke-static {v2, v3, v4, p3}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getItemCursor(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 204
    :cond_0
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cursor invalid : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    :catch_0
    move-exception v1

    .line 212
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 216
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 206
    :cond_1
    :try_start_2
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->loadFromCursor(Landroid/database/Cursor;)V

    .line 208
    new-instance v4, Lcom/sec/android/gallery3d/data/LocalImageAttributes;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v4, v5, p0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/LocalImage;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mAttributes:Lcom/sec/android/gallery3d/data/LocalImageAttributes;

    .line 209
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mAttributes:Lcom/sec/android/gallery3d/data/LocalImageAttributes;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->updateAttributes()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 214
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v4
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 188
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalImage;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/sec/android/gallery3d/data/LocalMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;JLcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 165
    new-instance v0, Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;-><init>(Lcom/sec/android/gallery3d/data/MediaObject;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mPanoramaMetadata:Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;

    .line 168
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsDeleted:Z

    .line 169
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsFaceInfoDeleted:Z

    .line 170
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsFaceScanned:Z

    .line 171
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsGolf:Z

    .line 172
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isSoundScene:Z

    .line 175
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotationSupportedInitialized:Z

    .line 178
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->regionDecoderSupportedInitialized:Z

    .line 181
    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mImageNoteType:I

    .line 183
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsRotateSearch:Z

    .line 1350
    iput-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePath:Lcom/sec/android/gallery3d/data/Path;

    .line 1351
    iput-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePathForExpansion:Lcom/sec/android/gallery3d/data/Path;

    .line 1352
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    .line 1414
    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcWidth:I

    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcHeight:I

    .line 1456
    iput-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    .line 1567
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mCachedBurstImageCount:I

    .line 189
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/data/LocalImage;->loadFromCursor(Landroid/database/Cursor;)V

    .line 191
    new-instance v0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/LocalImage;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mAttributes:Lcom/sec/android/gallery3d/data/LocalImageAttributes;

    .line 192
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mAttributes:Lcom/sec/android/gallery3d/data/LocalImageAttributes;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->updateAttributes()V

    .line 194
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/LocalImage;)Lcom/sec/android/gallery3d/data/LocalImageAttributes;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/LocalImage;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getLocalImageAttributes()Lcom/sec/android/gallery3d/data/LocalImageAttributes;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static getAllFacesRectFromCache(I)Landroid/graphics/RectF;
    .locals 1
    .param p0, "id"    # I

    .prologue
    .line 1051
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->getAllFacesRect(I)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method private static getExifOrientation(I)Ljava/lang/String;
    .locals 5
    .param p0, "orientation"    # I

    .prologue
    const/4 v4, 0x1

    .line 698
    sparse-switch p0, :sswitch_data_0

    .line 708
    :try_start_0
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_0

    .line 710
    :catch_0
    move-exception v0

    .line 711
    .local v0, "e":Ljava/lang/AssertionError;
    sget-object v1, Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AssertionError"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .end local v0    # "e":Ljava/lang/AssertionError;
    :goto_0
    return-object v1

    .line 700
    :sswitch_0
    const/4 v1, 0x1

    :try_start_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 702
    :sswitch_1
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 704
    :sswitch_2
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 706
    :sswitch_3
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/AssertionError; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_0

    .line 698
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method public static getFaceCount(Landroid/content/ContentResolver;I)I
    .locals 8
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "imageId"    # I

    .prologue
    .line 1139
    const/4 v7, 0x0

    .line 1140
    .local v7, "faceCursor":Landroid/database/Cursor;
    const/4 v6, -0x1

    .line 1143
    .local v6, "faceCount":I
    :try_start_0
    const-string v0, "content://media/external/file"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "face_count"

    aput-object v3, v2, v0

    const-string v3, "_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1148
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1149
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 1152
    :cond_0
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1155
    return v6

    .line 1152
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getFileInfo()V
    .locals 1

    .prologue
    .line 1439
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->getFileInfo(Z)V

    .line 1440
    return-void
.end method

.method private getFileInfo(Z)V
    .locals 3
    .param p1, "ignoreDrm"    # Z

    .prologue
    const/4 v2, 0x1

    .line 1444
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isdrm:Z

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 1454
    :goto_0
    return-void

    .line 1448
    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1449
    .local v0, "opts":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1450
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 1451
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1452
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcWidth:I

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    .line 1453
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcHeight:I

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    goto :goto_0
.end method

.method private getLocalImageAttributes()Lcom/sec/android/gallery3d/data/LocalImageAttributes;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mAttributes:Lcom/sec/android/gallery3d/data/LocalImageAttributes;

    return-object v0
.end method

.method private isRotationSupoorted()Z
    .locals 1

    .prologue
    .line 1242
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotationSupportedInitialized:Z

    if-nez v0, :cond_0

    .line 1243
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/BitmapUtils;->isRotationSupported(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isRotationSupoorted:Z

    .line 1244
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotationSupportedInitialized:Z

    .line 1246
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isRotationSupoorted:Z

    return v0
.end method

.method private isSupportedByRegionDecoder()Z
    .locals 1

    .prologue
    .line 1234
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->regionDecoderSupportedInitialized:Z

    if-nez v0, :cond_0

    .line 1235
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/BitmapUtils;->isSupportedByRegionDecoder(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isSupportedByRegionDecoder:Z

    .line 1236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->regionDecoderSupportedInitialized:Z

    .line 1238
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isSupportedByRegionDecoder:Z

    return v0
.end method

.method private loadFromCursor(Landroid/database/Cursor;)V
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 219
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    .line 220
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->caption:Ljava/lang/String;

    .line 221
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    .line 222
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    .line 223
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    .line 224
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateTakenInMs:J

    .line 225
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateAddedInSec:J

    .line 226
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateModifiedInSec:J

    .line 227
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    .line 228
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    .line 229
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->bucketId:I

    .line 230
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->fileSize:J

    .line 231
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    .line 232
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    .line 235
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGroupId:Z

    if-eqz v0, :cond_0

    .line 236
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->groupId:J

    .line 238
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcWidth:I

    .line 239
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcHeight:I

    .line 242
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    if-eqz v0, :cond_1

    .line 243
    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->sphericalMosaic:I

    .line 244
    :cond_1
    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    .line 246
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isDrm(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 247
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isdrm:Z

    .line 248
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/data/LocalImage;->getFileInfo(Z)V

    .line 251
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 252
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isSceretBoxItem:Z

    .line 255
    :cond_3
    return-void
.end method

.method private static updateAllFacesRectInCache(ILandroid/graphics/RectF;)V
    .locals 0
    .param p0, "id"    # I
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 1055
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->updateAllFacesRectInCache(ILandroid/graphics/RectF;)V

    .line 1056
    return-void
.end method

.method private static updateWidthAndHeightProjection()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 158
    sget-boolean v0, Lcom/sec/android/gallery3d/common/ApiHelper;->HAS_MEDIA_COLUMNS_WIDTH_AND_HEIGHT:Z

    if-eqz v0, :cond_0

    .line 159
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    const/16 v1, 0xc

    const-string/jumbo v2, "width"

    aput-object v2, v0, v1

    .line 160
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    const/16 v1, 0xd

    const-string v2, "height"

    aput-object v2, v0, v1

    .line 162
    :cond_0
    return-void
.end method


# virtual methods
.method protected addHiddenInfo(Landroid/content/ContentValues;)Z
    .locals 8
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v4, 0x0

    .line 1520
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    const/high16 v6, -0x40000000    # -2.0f

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    const/high16 v6, -0x40800000    # -1.0f

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_0

    .line 1522
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1523
    .local v1, "faceInfo":Landroid/os/Parcel;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;

    invoke-virtual {v5, v1, v4}, Landroid/graphics/RectF;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1524
    const-string v5, "faces_rect"

    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1525
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1528
    .end local v1    # "faceInfo":Landroid/os/Parcel;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    .line 1529
    .local v0, "dbMgr":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    const-string v5, "images_hidden_album"

    invoke-virtual {v0, v5, p1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addHiddenItem(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1530
    .local v2, "hiddenItemId":J
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-ltz v5, :cond_1

    .line 1531
    const/4 v4, 0x1

    .line 1534
    :goto_0
    return v4

    .line 1533
    :cond_1
    sget-object v5, Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "addHiddenInfo values="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " faild with id = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public clearCachedPanoramaSupport()V
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mPanoramaMetadata:Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;->clearCachedValues()V

    .line 629
    return-void
.end method

.method protected copyTo(Ljava/lang/String;)V
    .locals 14
    .param p1, "dstFolderPath"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x1

    const/4 v5, 0x0

    .line 1182
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1183
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    .line 1184
    .local v7, "bucketName":Ljava/lang/String;
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1186
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getContentValues()Landroid/content/ContentValues;

    move-result-object v12

    .line 1187
    .local v12, "values":Landroid/content/ContentValues;
    const-string v2, "_id"

    invoke-virtual {v12, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1188
    const-string v2, "bucket_id"

    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getBucketId(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1189
    const-string v2, "bucket_display_name"

    invoke-virtual {v12, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    const-string v3, "\'"

    const-string v4, "\'\'"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1192
    .local v11, "path":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_data=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v12, v2, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_0

    .line 1193
    invoke-virtual {v0, v1, v12}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1195
    :cond_0
    sget-object v2, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    const-string v3, "_data=?"

    new-array v4, v13, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v11, v4, v6

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v8

    .line 1196
    .local v8, "cr":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1197
    new-instance v9, Lcom/sec/android/gallery3d/data/LocalImage;

    new-instance v2, Lcom/sec/android/gallery3d/data/Path;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/data/Path;-><init>()V

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v9, v2, v3, v8}, Lcom/sec/android/gallery3d/data/LocalImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .line 1198
    .local v9, "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    .line 1199
    .local v10, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v10, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1200
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v10, v13}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->requestFaceScan(Landroid/content/Context;Ljava/util/List;Z)V

    .line 1201
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1203
    .end local v9    # "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    .end local v10    # "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_1
    return-void
.end method

.method public delete()V
    .locals 13

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 640
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isHiddenItem()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 641
    sget-object v8, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v8

    .line 642
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const-string v9, "images_hidden_album"

    invoke-virtual {p0, v7, v9}, Lcom/sec/android/gallery3d/data/LocalImage;->deleteHiddenItem(Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/lang/String;)V

    .line 643
    monitor-exit v8

    .line 694
    :cond_0
    :goto_0
    return-void

    .line 643
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .line 647
    :cond_1
    iget v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v7, v9}, Lcom/sec/android/gallery3d/data/LocalImage;->updateAllFacesRectInCache(ILandroid/graphics/RectF;)V

    .line 651
    iget-boolean v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsDeleted:Z

    if-nez v7, :cond_0

    .line 654
    iput-boolean v8, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsDeleted:Z

    .line 656
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 658
    sget-object v8, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v8

    .line 659
    :try_start_1
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 661
    .local v1, "baseUri":Landroid/net/Uri;
    :try_start_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v9, "_id=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    iget v12, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v7, v1, v9, v10}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->delete(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 669
    :try_start_3
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v7

    iget v9, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v7, v9}, Lcom/sec/samsung/gallery/access/face/FaceList;->removeFaceByFileId(Landroid/content/Context;I)V

    .line 671
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->isArcMode()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 672
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v9, 0x1

    invoke-interface {v7, v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->setRefreshOperation(I)V

    .line 673
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    if-eqz v7, :cond_5

    .line 674
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v7, :cond_2

    .line 675
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/Face;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v3, v0, v4

    .line 676
    .local v3, "face":Lcom/sec/android/gallery3d/data/Face;
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v9

    invoke-static {v7, v9}, Lcom/sec/samsung/gallery/access/face/FaceList;->remove(Landroid/content/Context;I)V

    .line 675
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 665
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/data/Face;
    .end local v3    # "face":Lcom/sec/android/gallery3d/data/Face;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catch_0
    move-exception v2

    .line 666
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 667
    monitor-exit v8

    goto :goto_0

    .line 691
    .end local v1    # "baseUri":Landroid/net/Uri;
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v7

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v7

    .line 680
    .restart local v1    # "baseUri":Landroid/net/Uri;
    :cond_2
    :try_start_4
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/Path;

    .line 681
    .local v6, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 682
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 686
    .end local v6    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_4
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    .line 690
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_5
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->setVersion()V

    .line 691
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0
.end method

.method public deleteFaceImage()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1251
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsDeleted:Z

    if-eqz v1, :cond_0

    .line 1265
    :goto_0
    return-void

    .line 1255
    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsDeleted:Z

    .line 1257
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 1258
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1259
    .local v0, "baseUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "_id=?"

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1264
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->setArcVersionNumber()V

    goto :goto_0
.end method

.method public deleteFaceInfo()V
    .locals 7

    .prologue
    .line 1271
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsFaceInfoDeleted:Z

    if-eqz v5, :cond_0

    .line 1299
    :goto_0
    return-void

    .line 1274
    :cond_0
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsFaceInfoDeleted:Z

    .line 1276
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 1279
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->isArcMode()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1280
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    if-eqz v5, :cond_4

    .line 1281
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v5, :cond_1

    .line 1282
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/Face;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 1283
    .local v1, "face":Lcom/sec/android/gallery3d/data/Face;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v6

    invoke-static {v5, v6}, Lcom/sec/samsung/gallery/access/face/FaceList;->remove(Landroid/content/Context;I)V

    .line 1282
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1287
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/data/Face;
    .end local v1    # "face":Lcom/sec/android/gallery3d/data/Face;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/Path;

    .line 1288
    .local v4, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1289
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1293
    .end local v4    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_3
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    .line 1297
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->setArcVersionNumber()V

    goto :goto_0
.end method

.method public getAllFaceImagePaths()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1403
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getArcHeight()I
    .locals 2

    .prologue
    .line 1428
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcWidth:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcHeight:I

    if-nez v0, :cond_1

    .line 1429
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getFileInfo()V

    .line 1432
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_3

    .line 1433
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcWidth:I

    .line 1435
    :goto_0
    return v0

    :cond_3
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcHeight:I

    goto :goto_0
.end method

.method public getArcWidth()I
    .locals 2

    .prologue
    .line 1417
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcWidth:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcHeight:I

    if-nez v0, :cond_1

    .line 1418
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getFileInfo()V

    .line 1421
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_3

    .line 1422
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcHeight:I

    .line 1424
    :goto_0
    return v0

    :cond_3
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcWidth:I

    goto :goto_0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 799
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isHiddenItem()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 800
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 803
    :goto_0
    return-object v1

    .line 802
    :cond_0
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 803
    .local v0, "baseUri":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method protected getContentValues()Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 1207
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1208
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "_id"

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1209
    const-string/jumbo v1, "title"

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->caption:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    const-string v1, "mime_type"

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1211
    const-string v1, "latitude"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1212
    const-string v1, "longitude"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1213
    const-string v1, "datetaken"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateTakenInMs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1214
    const-string v1, "date_added"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateAddedInSec:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1215
    const-string v1, "date_modified"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateModifiedInSec:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1216
    const-string v1, "_data"

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    const-string v1, "orientation"

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1218
    const-string v1, "bucket_id"

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->bucketId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1219
    const-string v1, "_size"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->fileSize:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1220
    const-string/jumbo v1, "width"

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1221
    const-string v1, "height"

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1222
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGroupId:Z

    if-eqz v1, :cond_0

    .line 1223
    const-string v1, "group_id"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->groupId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1225
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    if-eqz v1, :cond_1

    .line 1226
    const-string/jumbo v1, "spherical_mosaic"

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->sphericalMosaic:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1228
    :cond_1
    const-string v1, "sef_file_type"

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1230
    return-object v0
.end method

.method public getDataVersion(I)J
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 1472
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 1473
    :cond_0
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mDataVersion:J

    .line 1481
    :goto_0
    return-wide v2

    .line 1475
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_2

    .line 1476
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1477
    .local v0, "version":Ljava/lang/Long;
    if-eqz v0, :cond_2

    .line 1478
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_0

    .line 1481
    .end local v0    # "version":Ljava/lang/Long;
    :cond_2
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mDataVersion:J

    goto :goto_0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0xd

    const/16 v3, 0xc

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 814
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 818
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateTakenInMs:J

    invoke-static {v2, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 821
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    if-nez v1, :cond_1

    .line 822
    :cond_0
    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/data/LocalImage;->getFileInfo(Z)V

    .line 824
    :cond_1
    const-string v1, "image/jpeg"

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 827
    const/16 v1, 0x10

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 828
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->extractExifInfo(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)V

    .line 830
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 831
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->replaceDetail(ILjava/lang/Object;)V

    .line 833
    :cond_2
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 834
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v8, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->replaceDetail(ILjava/lang/Object;)V

    .line 856
    :cond_3
    :goto_0
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 857
    const/16 v1, 0xf

    const-string v2, "%dx%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 859
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isdrm:Z

    if-eqz v1, :cond_4

    .line 860
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDetails(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 863
    :cond_4
    return-object v0

    .line 837
    :cond_5
    const/16 v1, 0xc8

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 838
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->caption:Ljava/lang/String;

    invoke-virtual {v0, v7, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 839
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 840
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v8, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 842
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 843
    const/16 v1, 0x9

    const/4 v2, 0x2

    new-array v2, v2, [D

    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    aput-wide v4, v2, v6

    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    aput-wide v4, v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 844
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 845
    const/16 v1, 0xb

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 852
    :goto_1
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->fileSize:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    .line 853
    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->fileSize:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 847
    :cond_6
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v9}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 848
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v9}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 849
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v9}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method public getFaceCount()I
    .locals 3

    .prologue
    .line 1133
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getFaceCount(Landroid/content/ContentResolver;I)I

    move-result v0

    .line 1134
    .local v0, "faceCount":I
    if-ltz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsFaceScanned:Z

    .line 1135
    return v0

    .line 1134
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFaceImagePath()Lcom/sec/android/gallery3d/data/Path;
    .locals 1

    .prologue
    .line 1391
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method public getFaceImagePathForExpansion()Lcom/sec/android/gallery3d/data/Path;
    .locals 1

    .prologue
    .line 1399
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePathForExpansion:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method public getFacePath(I)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1486
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1487
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 1493
    :goto_0
    return-object v0

    .line 1489
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 1490
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0

    .line 1493
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    goto :goto_0
.end method

.method public getFaces()[Lcom/sec/android/gallery3d/data/Face;
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    return-object v0
.end method

.method public getGroup(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "contactLookupKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1340
    const/4 v1, 0x0

    .line 1342
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getGroup(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1343
    sget-object v2, Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Gallery Group:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1347
    :goto_0
    return-object v1

    .line 1344
    :catch_0
    move-exception v0

    .line 1345
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 878
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    return v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 1321
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getBucketId(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemId()I
    .locals 1

    .prologue
    .line 1303
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    return v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 1506
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 1511
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    return-wide v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 809
    const/4 v0, 0x2

    return v0
.end method

.method public getName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "contactLookupKey"    # Ljava/lang/String;

    .prologue
    .line 1327
    const/4 v1, 0x0

    .line 1329
    .local v1, "result":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1331
    sget-object v2, Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Gallery Name:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1335
    :goto_0
    return-object v1

    .line 1332
    :catch_0
    move-exception v0

    .line 1333
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getPanoramaSupport(Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;

    .prologue
    .line 623
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mPanoramaMetadata:Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/gallery3d/app/PanoramaMetadataSupport;->getPanoramaSupport(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;)V

    .line 624
    return-void
.end method

.method public getRectOfAllFaces()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1089
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces(Z)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getRectOfAllFaces(Z)Landroid/graphics/RectF;
    .locals 6
    .param p1, "ignoreRotation"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    const/high16 v4, -0x40000000    # -2.0f

    .line 1093
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;

    if-nez v2, :cond_1

    .line 1129
    :cond_0
    :goto_0
    return-object v0

    .line 1096
    :cond_1
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1097
    .local v1, "rect":Landroid/graphics/RectF;
    if-eqz v1, :cond_2

    iget v2, v1, Landroid/graphics/RectF;->top:F

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_0

    iget v2, v1, Landroid/graphics/RectF;->top:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 1100
    :cond_2
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v4, v4, v5, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1101
    .local v0, "convertedFaceRect":Landroid/graphics/RectF;
    if-eqz p1, :cond_3

    .line 1102
    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 1103
    iget v2, v1, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 1104
    iget v2, v1, Landroid/graphics/RectF;->right:F

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 1105
    iget v2, v1, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_0

    .line 1107
    :cond_3
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    const/16 v3, 0x5a

    if-ne v2, v3, :cond_4

    .line 1108
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v2, v2

    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 1109
    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 1110
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v2, v2

    iget v3, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 1111
    iget v2, v1, Landroid/graphics/RectF;->right:F

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_0

    .line 1112
    :cond_4
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    const/16 v3, 0xb4

    if-ne v2, v3, :cond_5

    .line 1113
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v2, v2

    iget v3, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 1114
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v2, v2

    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 1115
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v2, v2

    iget v3, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 1116
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v2, v2

    iget v3, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 1117
    :cond_5
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    const/16 v3, 0x10e

    if-ne v2, v3, :cond_6

    .line 1118
    iget v2, v1, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 1119
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v2, v2

    iget v3, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 1120
    iget v2, v1, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 1121
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v2, v2

    iget v3, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 1123
    :cond_6
    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 1124
    iget v2, v1, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 1125
    iget v2, v1, Landroid/graphics/RectF;->right:F

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 1126
    iget v2, v1, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 868
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    return v0
.end method

.method public getSupportedOperations()J
    .locals 12

    .prologue
    const-wide/high16 v10, 0x4000000000000L

    const-wide/16 v8, 0x40

    const-wide/16 v6, 0x20

    .line 532
    const-wide v0, 0x410083800425L    # 3.5311099971743E-310

    .line 535
    .local v0, "operation":J
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isHiddenItem()Z

    move-result v2

    if-nez v2, :cond_4

    .line 536
    const-wide v2, 0x7078000004200208L    # 5.961669535509792E233

    or-long/2addr v0, v2

    .line 541
    invoke-virtual {p0, v6, v7}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsGolf:Z

    if-nez v2, :cond_0

    .line 542
    const-wide v2, 0x100000000L

    or-long/2addr v0, v2

    .line 545
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isHideBlockedItem()Z

    move-result v2

    if-nez v2, :cond_1

    .line 546
    or-long/2addr v0, v10

    .line 549
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isRotationSupoorted()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 550
    const-wide/16 v2, 0x2

    or-long/2addr v0, v2

    .line 553
    :cond_2
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->groupId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    .line 554
    const-wide v2, 0x100000000000L

    or-long/2addr v0, v2

    .line 555
    const-wide v2, 0x200000000L

    or-long/2addr v0, v2

    .line 558
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isSupportImageNote()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 559
    const-wide v2, 0x400000000L

    or-long/2addr v0, v2

    .line 563
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isGolf()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isBroken()Z

    move-result v2

    if-nez v2, :cond_5

    .line 564
    const-wide v2, 0x2810000000L

    or-long/2addr v0, v2

    .line 567
    :cond_5
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v2, :cond_6

    .line 568
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isSceretBoxItem:Z

    if-eqz v2, :cond_f

    .line 569
    const-wide/32 v2, 0x20000000

    or-long/2addr v0, v2

    .line 570
    const-wide/32 v2, -0x40000001

    and-long/2addr v0, v2

    .line 577
    :cond_6
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isGolf()Z

    move-result v2

    if-nez v2, :cond_7

    .line 578
    const-wide/high16 v2, -0x8000000000000000L

    or-long/2addr v0, v2

    .line 581
    :cond_7
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isSupportedByRegionDecoder()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsGolf:Z

    if-nez v2, :cond_8

    .line 582
    or-long/2addr v0, v8

    .line 586
    :cond_8
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 587
    const-wide/16 v2, 0x10

    or-long/2addr v0, v2

    .line 591
    :cond_9
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    if-eqz v2, :cond_a

    .line 592
    const-wide v2, 0x200000000000L

    or-long/2addr v0, v2

    .line 595
    :cond_a
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlashAnnotate:Z

    if-eqz v2, :cond_b

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsGolf:Z

    if-nez v2, :cond_b

    .line 596
    const-wide/32 v2, 0x80000

    or-long/2addr v0, v2

    .line 599
    :cond_b
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    if-eqz v2, :cond_c

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->sphericalMosaic:I

    if-eqz v2, :cond_c

    .line 600
    const-wide v2, 0x4000000000L

    or-long/2addr v0, v2

    .line 603
    :cond_c
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isdrm:Z

    if-eqz v2, :cond_e

    .line 604
    const-wide/16 v0, 0x401

    .line 605
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->drmInfo:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;->mOriginalMime:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->isSupportedByRegionDecoder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 606
    or-long/2addr v0, v8

    .line 608
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isHiddenItem()Z

    move-result v2

    if-nez v2, :cond_e

    .line 609
    or-long/2addr v0, v10

    .line 610
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->drmInfo:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;

    iget v2, v2, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;->mDrmType:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_10

    .line 611
    const-wide v2, 0x10000000004L

    or-long/2addr v0, v2

    .line 618
    :cond_e
    :goto_1
    return-wide v0

    .line 572
    :cond_f
    const-wide/32 v2, 0x40000000

    or-long/2addr v0, v2

    .line 573
    const-wide/32 v2, -0x20000001

    and-long/2addr v0, v2

    goto :goto_0

    .line 612
    :cond_10
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->drmInfo:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;

    iget v2, v2, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;->mDrmType:I

    if-nez v2, :cond_e

    .line 613
    or-long/2addr v0, v6

    goto :goto_1
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 873
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    return v0
.end method

.method public isBurstShotImage()Z
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1570
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mCachedBurstImageCount:I

    const/4 v4, -0x1

    if-ne v1, v4, :cond_2

    .line 1571
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "group_id = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getGroupId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " and bucket_id = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getBucketId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1573
    .local v3, "selection":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v8

    .line 1574
    .local v8, "mContext":Landroid/content/Context;
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1575
    .local v0, "mResolver":Landroid/content/ContentResolver;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGroupId:Z

    if-nez v1, :cond_0

    .line 1596
    .end local v0    # "mResolver":Landroid/content/ContentResolver;
    .end local v3    # "selection":Ljava/lang/String;
    .end local v8    # "mContext":Landroid/content/Context;
    :goto_0
    return v10

    .line 1578
    .restart local v0    # "mResolver":Landroid/content/ContentResolver;
    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v8    # "mContext":Landroid/content/Context;
    :cond_0
    const/4 v6, 0x0

    .line 1580
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x1

    :try_start_0
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "count(*)"

    aput-object v4, v2, v1

    .line 1581
    .local v2, "COUNT_PROJECTION":[Ljava/lang/String;
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1583
    if-nez v6, :cond_1

    .line 1584
    sget-object v1, Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;

    const-string v4, "query fail"

    invoke-static {v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1585
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mCachedBurstImageCount:I
    :try_end_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1593
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 1588
    :cond_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 1589
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mCachedBurstImageCount:I
    :try_end_1
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1593
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1596
    .end local v0    # "mResolver":Landroid/content/ContentResolver;
    .end local v2    # "COUNT_PROJECTION":[Ljava/lang/String;
    .end local v3    # "selection":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v8    # "mContext":Landroid/content/Context;
    :cond_2
    :goto_1
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mCachedBurstImageCount:I

    if-le v1, v9, :cond_3

    move v1, v9

    :goto_2
    move v10, v1

    goto :goto_0

    .line 1590
    .restart local v0    # "mResolver":Landroid/content/ContentResolver;
    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "mContext":Landroid/content/Context;
    :catch_0
    move-exception v7

    .line 1591
    .local v7, "e":Landroid/database/CursorWindowAllocationException;
    :try_start_2
    invoke-virtual {v7}, Landroid/database/CursorWindowAllocationException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1593
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v7    # "e":Landroid/database/CursorWindowAllocationException;
    :catchall_0
    move-exception v1

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1

    .end local v0    # "mResolver":Landroid/content/ContentResolver;
    .end local v3    # "selection":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v8    # "mContext":Landroid/content/Context;
    :cond_3
    move v1, v10

    .line 1596
    goto :goto_2
.end method

.method public isBurstShotImage(Ljava/lang/String;)Z
    .locals 12
    .param p1, "selection"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1539
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    .line 1540
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1541
    .local v0, "mResolver":Landroid/content/ContentResolver;
    const/4 v9, 0x0

    .line 1542
    .local v9, "mCachedBurstImageCount":I
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGroupId:Z

    if-nez v1, :cond_0

    .line 1562
    :goto_0
    return v11

    .line 1545
    :cond_0
    const/4 v7, 0x0

    .line 1547
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x1

    :try_start_0
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "count(*)"

    aput-object v3, v2, v1

    .line 1548
    .local v2, "COUNT_PROJECTION":[Ljava/lang/String;
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1550
    if-nez v7, :cond_1

    .line 1551
    sget-object v1, Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;

    const-string v3, "query fail"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1552
    const/4 v9, 0x0

    .line 1560
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 1555
    :cond_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 1556
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v9

    .line 1560
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1562
    .end local v2    # "COUNT_PROJECTION":[Ljava/lang/String;
    :goto_1
    if-le v9, v10, :cond_2

    move v1, v10

    :goto_2
    move v11, v1

    goto :goto_0

    .line 1557
    :catch_0
    move-exception v8

    .line 1558
    .local v8, "e":Landroid/database/CursorWindowAllocationException;
    :try_start_2
    invoke-virtual {v8}, Landroid/database/CursorWindowAllocationException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1560
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v8    # "e":Landroid/database/CursorWindowAllocationException;
    :catchall_0
    move-exception v1

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1

    :cond_2
    move v1, v11

    .line 1562
    goto :goto_2
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 1463
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsDeleted:Z

    return v0
.end method

.method public isDirty()Z
    .locals 4

    .prologue
    .line 1407
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcDataVersion:J

    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalImage;->getArcVersionNumber()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFaceInfoDeleted()Z
    .locals 1

    .prologue
    .line 1467
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsFaceInfoDeleted:Z

    return v0
.end method

.method public isFaceScanced()Z
    .locals 1

    .prologue
    .line 1159
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsFaceScanned:Z

    if-nez v0, :cond_0

    .line 1160
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getFaceCount()I

    .line 1161
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsFaceScanned:Z

    return v0
.end method

.method public declared-synchronized isFileModified(Landroid/database/Cursor;)Z
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1313
    monitor-enter p0

    const/4 v2, 0x7

    :try_start_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 1314
    .local v0, "modifiedInSec":J
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/LocalImage;->updateContent(Landroid/database/Cursor;)V

    .line 1315
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateModifiedInSec:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    monitor-exit p0

    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 1313
    .end local v0    # "modifiedInSec":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public isGolf()Z
    .locals 2

    .prologue
    .line 1308
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    const-string v1, "image/golf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSupportImageNote()Z
    .locals 2

    .prologue
    .line 1497
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->bucketId:I

    sget v1, Lcom/sec/android/gallery3d/util/MediaSetUtils;->IMAGENOTE_BUCKET_ID:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->bucketId:I

    sget v1, Lcom/sec/android/gallery3d/util/MediaSetUtils;->PHOTOFRAME_BUCKET_ID:I

    if-ne v0, v1, :cond_1

    :cond_0
    const-wide/32 v0, 0x80000

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1499
    :cond_1
    const/4 v0, 0x1

    .line 1501
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadFace()[Lcom/sec/android/gallery3d/data/Face;
    .locals 1

    .prologue
    .line 915
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->loadFace(Z)[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v0

    return-object v0
.end method

.method public loadFace(Z)[Lcom/sec/android/gallery3d/data/Face;
    .locals 36
    .param p1, "isManualFD"    # Z

    .prologue
    .line 918
    if-nez p1, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v5, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isDirty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 919
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    .line 1047
    :goto_0
    return-object v5

    .line 921
    :cond_0
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    .line 922
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isBroken()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 923
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    goto :goto_0

    .line 925
    :cond_1
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    if-nez v5, :cond_3

    .line 926
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getFileInfo()V

    .line 928
    :cond_3
    const/16 v18, 0x0

    .local v18, "faceId":I
    const/16 v25, 0x0

    .local v25, "personId":I
    const/16 v27, 0x0

    .local v27, "recommendedId":I
    const/16 v20, 0x0

    .local v20, "groupId":I
    const/4 v10, 0x0

    .local v10, "faceData":I
    const/4 v12, 0x0

    .local v12, "autoGroup":I
    const/16 v30, 0x0

    .line 929
    .local v30, "similarity":I
    const/16 v23, 0x0

    .local v23, "left":I
    const/16 v33, 0x0

    .local v33, "top":I
    const/16 v28, 0x0

    .local v28, "right":I
    const/4 v13, 0x0

    .line 930
    .local v13, "bottom":I
    const/16 v24, 0x0

    .local v24, "leftRotated":I
    const/16 v34, 0x0

    .local v34, "topRotated":I
    const/16 v29, 0x0

    .local v29, "rightRotated":I
    const/4 v14, 0x0

    .line 931
    .local v14, "bottomRotated":I
    const/16 v32, 0x0

    .line 932
    .local v32, "tempRecommendId":I
    new-instance v26, Ljava/util/HashMap;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashMap;-><init>()V

    .line 933
    .local v26, "recommendFaces":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/gallery3d/data/LocalImage$Value;>;"
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 934
    .local v4, "resolver":Landroid/content/ContentResolver;
    const/16 v17, 0x0

    .line 935
    .local v17, "faceCursor":Landroid/database/Cursor;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    .line 937
    :try_start_0
    sget-object v5, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    const/16 v7, 0xb

    new-array v6, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v11, "_id"

    aput-object v11, v6, v7

    const/4 v7, 0x1

    const-string v11, "person_id"

    aput-object v11, v6, v7

    const/4 v7, 0x2

    const-string v11, "recommended_id"

    aput-object v11, v6, v7

    const/4 v7, 0x3

    const-string v11, "group_id"

    aput-object v11, v6, v7

    const/4 v7, 0x4

    const-string v11, "face_data"

    aput-object v11, v6, v7

    const/4 v7, 0x5

    const-string v11, "pos_left"

    aput-object v11, v6, v7

    const/4 v7, 0x6

    const-string v11, "pos_top"

    aput-object v11, v6, v7

    const/4 v7, 0x7

    const-string v11, "pos_right"

    aput-object v11, v6, v7

    const/16 v7, 0x8

    const-string v11, "pos_bottom"

    aput-object v11, v6, v7

    const/16 v7, 0x9

    const-string v11, "auto_group"

    aput-object v11, v6, v7

    const/16 v7, 0xa

    const-string/jumbo v11, "similarity"

    aput-object v11, v6, v7

    const-string v7, "image_id=?"

    const/4 v11, 0x1

    new-array v8, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    move/from16 v35, v0

    invoke-static/range {v35 .. v35}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v35

    aput-object v35, v8, v11

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 947
    if-eqz v17, :cond_11

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 948
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v15

    .line 949
    .local v15, "count":I
    new-array v0, v15, [Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v31, v0
    :try_end_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 950
    .local v31, "tempFaces":[Lcom/sec/android/gallery3d/data/Face;
    const/16 v21, 0x0

    .local v21, "i":I
    move/from16 v22, v21

    .end local v21    # "i":I
    .local v22, "i":I
    move/from16 v8, v32

    .end local v32    # "tempRecommendId":I
    .local v8, "tempRecommendId":I
    move/from16 v9, v20

    .end local v20    # "groupId":I
    .local v9, "groupId":I
    move/from16 v6, v18

    .line 952
    .end local v18    # "faceId":I
    .local v6, "faceId":I
    :goto_1
    const/4 v5, 0x0

    :try_start_1
    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 953
    const/4 v5, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    .line 954
    const/4 v5, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    .line 955
    const/4 v5, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 958
    const/4 v5, 0x4

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 959
    const/4 v5, 0x5

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 960
    const/4 v5, 0x6

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 961
    const/4 v5, 0x7

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    .line 962
    const/16 v5, 0x8

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 963
    const/16 v5, 0x9

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 964
    const/16 v5, 0xa

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v30

    .line 966
    if-gez v23, :cond_4

    const/16 v23, 0x0

    .line 967
    :cond_4
    if-gez v33, :cond_5

    const/16 v33, 0x0

    .line 968
    :cond_5
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    move/from16 v0, v28

    if-le v0, v5, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    move/from16 v28, v0

    .line 969
    :cond_6
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    if-le v13, v5, :cond_7

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    .line 971
    :cond_7
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    const/16 v7, 0x5a

    if-ne v5, v7, :cond_9

    .line 972
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    sub-int v24, v5, v13

    .line 973
    move/from16 v34, v23

    .line 974
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    sub-int v29, v5, v33

    .line 975
    move/from16 v14, v28

    .line 993
    :goto_2
    move/from16 v8, v27

    .line 994
    const/4 v5, 0x1

    move/from16 v0, v27

    if-le v0, v5, :cond_d

    move/from16 v0, v27

    move/from16 v1, v25

    if-ne v0, v1, :cond_d

    .line 995
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 996
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/gallery3d/data/LocalImage$Value;

    .line 997
    .local v19, "faceValue":Lcom/sec/android/gallery3d/data/LocalImage$Value;
    move-object/from16 v0, v19

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage$Value;->mMaxSimilarity:I

    const v7, 0x7fffffff

    if-eq v5, v7, :cond_8

    .line 998
    move-object/from16 v0, v19

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage$Value;->mFaceIndex:I

    aget-object v5, v31, v5

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Lcom/sec/android/gallery3d/data/Face;->setRecommendedId(I)V

    .line 999
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, v19

    iget v7, v0, Lcom/sec/android/gallery3d/data/LocalImage$Value;->mFaceId:I

    invoke-static {v5, v7}, Lcom/sec/samsung/gallery/access/face/FaceList;->setFaceUnknownIgnoreAutoGroup(Landroid/content/Context;I)V

    .line 1002
    move/from16 v0, v22

    move-object/from16 v1, v19

    iput v0, v1, Lcom/sec/android/gallery3d/data/LocalImage$Value;->mFaceIndex:I

    .line 1003
    const v5, 0x7fffffff

    move-object/from16 v0, v19

    iput v5, v0, Lcom/sec/android/gallery3d/data/LocalImage$Value;->mMaxSimilarity:I

    .line 1004
    move-object/from16 v0, v19

    iput v6, v0, Lcom/sec/android/gallery3d/data/LocalImage$Value;->mFaceId:I

    .line 1005
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1034
    .end local v19    # "faceValue":Lcom/sec/android/gallery3d/data/LocalImage$Value;
    :cond_8
    :goto_3
    add-int/lit8 v21, v22, 0x1

    .end local v22    # "i":I
    .restart local v21    # "i":I
    new-instance v5, Lcom/sec/android/gallery3d/data/Face;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, ""

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v11, Landroid/graphics/Rect;

    move/from16 v0, v24

    move/from16 v1, v34

    move/from16 v2, v29

    invoke-direct {v11, v0, v1, v2, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/gallery3d/data/Face;-><init>(ILjava/lang/String;IIILandroid/graphics/Rect;I)V

    aput-object v5, v31, v22

    .line 1036
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_10

    .line 1037
    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;
    :try_end_1
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1043
    .end local v15    # "count":I
    .end local v21    # "i":I
    .end local v31    # "tempFaces":[Lcom/sec/android/gallery3d/data/Face;
    :goto_4
    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1046
    :goto_5
    invoke-virtual/range {v26 .. v26}, Ljava/util/HashMap;->clear()V

    .line 1047
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    goto/16 :goto_0

    .line 976
    .restart local v15    # "count":I
    .restart local v22    # "i":I
    .restart local v31    # "tempFaces":[Lcom/sec/android/gallery3d/data/Face;
    :cond_9
    :try_start_2
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    const/16 v7, 0xb4

    if-ne v5, v7, :cond_a

    .line 977
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    sub-int v24, v5, v28

    .line 978
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    sub-int v34, v5, v13

    .line 979
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    sub-int v29, v5, v23

    .line 980
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    sub-int v14, v5, v33

    goto/16 :goto_2

    .line 981
    :cond_a
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    const/16 v7, 0x10e

    if-ne v5, v7, :cond_b

    .line 982
    move/from16 v24, v33

    .line 983
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    sub-int v34, v5, v28

    .line 984
    move/from16 v29, v13

    .line 985
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    sub-int v14, v5, v23

    goto/16 :goto_2

    .line 987
    :cond_b
    move/from16 v24, v23

    .line 988
    move/from16 v34, v33

    .line 989
    move/from16 v29, v28

    .line 990
    move v14, v13

    goto/16 :goto_2

    .line 1008
    :cond_c
    new-instance v19, Lcom/sec/android/gallery3d/data/LocalImage$Value;

    const v5, 0x7fffffff

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move/from16 v2, v22

    invoke-direct {v0, v1, v2, v6, v5}, Lcom/sec/android/gallery3d/data/LocalImage$Value;-><init>(Lcom/sec/android/gallery3d/data/LocalImage;III)V

    .line 1009
    .restart local v19    # "faceValue":Lcom/sec/android/gallery3d/data/LocalImage$Value;
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3

    .line 1040
    .end local v19    # "faceValue":Lcom/sec/android/gallery3d/data/LocalImage$Value;
    .end local v22    # "i":I
    :catch_0
    move-exception v16

    .line 1041
    .end local v15    # "count":I
    .end local v31    # "tempFaces":[Lcom/sec/android/gallery3d/data/Face;
    .local v16, "e":Landroid/database/CursorWindowAllocationException;
    :goto_6
    :try_start_3
    invoke-virtual/range {v16 .. v16}, Landroid/database/CursorWindowAllocationException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1043
    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_5

    .line 1011
    .end local v16    # "e":Landroid/database/CursorWindowAllocationException;
    .restart local v15    # "count":I
    .restart local v22    # "i":I
    .restart local v31    # "tempFaces":[Lcom/sec/android/gallery3d/data/Face;
    :cond_d
    const/4 v5, 0x1

    move/from16 v0, v27

    if-le v0, v5, :cond_8

    if-eqz v30, :cond_8

    .line 1012
    :try_start_4
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 1013
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/gallery3d/data/LocalImage$Value;

    .line 1014
    .restart local v19    # "faceValue":Lcom/sec/android/gallery3d/data/LocalImage$Value;
    move-object/from16 v0, v19

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage$Value;->mMaxSimilarity:I

    move/from16 v0, v30

    if-ge v5, v0, :cond_e

    .line 1015
    move-object/from16 v0, v19

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalImage$Value;->mFaceIndex:I

    aget-object v5, v31, v5

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Lcom/sec/android/gallery3d/data/Face;->setRecommendedId(I)V

    .line 1016
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, v19

    iget v7, v0, Lcom/sec/android/gallery3d/data/LocalImage$Value;->mFaceId:I

    invoke-static {v5, v7}, Lcom/sec/samsung/gallery/access/face/FaceList;->setFaceUnknownIgnoreAutoGroup(Landroid/content/Context;I)V

    .line 1019
    move/from16 v0, v22

    move-object/from16 v1, v19

    iput v0, v1, Lcom/sec/android/gallery3d/data/LocalImage$Value;->mFaceIndex:I

    .line 1020
    move/from16 v0, v30

    move-object/from16 v1, v19

    iput v0, v1, Lcom/sec/android/gallery3d/data/LocalImage$Value;->mMaxSimilarity:I

    .line 1021
    move-object/from16 v0, v19

    iput v6, v0, Lcom/sec/android/gallery3d/data/LocalImage$Value;->mFaceId:I

    .line 1022
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_3

    .line 1043
    .end local v15    # "count":I
    .end local v19    # "faceValue":Lcom/sec/android/gallery3d/data/LocalImage$Value;
    .end local v22    # "i":I
    .end local v31    # "tempFaces":[Lcom/sec/android/gallery3d/data/Face;
    :catchall_0
    move-exception v5

    :goto_7
    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v5

    .line 1024
    .restart local v15    # "count":I
    .restart local v19    # "faceValue":Lcom/sec/android/gallery3d/data/LocalImage$Value;
    .restart local v22    # "i":I
    .restart local v31    # "tempFaces":[Lcom/sec/android/gallery3d/data/Face;
    :cond_e
    const/4 v8, 0x1

    .line 1025
    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v6}, Lcom/sec/samsung/gallery/access/face/FaceList;->setFaceUnknownIgnoreAutoGroup(Landroid/content/Context;I)V

    goto/16 :goto_3

    .line 1029
    .end local v19    # "faceValue":Lcom/sec/android/gallery3d/data/LocalImage$Value;
    :cond_f
    new-instance v19, Lcom/sec/android/gallery3d/data/LocalImage$Value;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move/from16 v2, v22

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v6, v3}, Lcom/sec/android/gallery3d/data/LocalImage$Value;-><init>(Lcom/sec/android/gallery3d/data/LocalImage;III)V

    .line 1030
    .restart local v19    # "faceValue":Lcom/sec/android/gallery3d/data/LocalImage$Value;
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_3

    .line 1043
    .end local v6    # "faceId":I
    .end local v8    # "tempRecommendId":I
    .end local v9    # "groupId":I
    .end local v15    # "count":I
    .end local v19    # "faceValue":Lcom/sec/android/gallery3d/data/LocalImage$Value;
    .end local v22    # "i":I
    .end local v31    # "tempFaces":[Lcom/sec/android/gallery3d/data/Face;
    .restart local v18    # "faceId":I
    .restart local v20    # "groupId":I
    .restart local v32    # "tempRecommendId":I
    :catchall_1
    move-exception v5

    move/from16 v8, v32

    .end local v32    # "tempRecommendId":I
    .restart local v8    # "tempRecommendId":I
    move/from16 v9, v20

    .end local v20    # "groupId":I
    .restart local v9    # "groupId":I
    move/from16 v6, v18

    .end local v18    # "faceId":I
    .restart local v6    # "faceId":I
    goto :goto_7

    .line 1040
    .end local v6    # "faceId":I
    .end local v8    # "tempRecommendId":I
    .end local v9    # "groupId":I
    .restart local v18    # "faceId":I
    .restart local v20    # "groupId":I
    .restart local v32    # "tempRecommendId":I
    :catch_1
    move-exception v16

    move/from16 v8, v32

    .end local v32    # "tempRecommendId":I
    .restart local v8    # "tempRecommendId":I
    move/from16 v9, v20

    .end local v20    # "groupId":I
    .restart local v9    # "groupId":I
    move/from16 v6, v18

    .end local v18    # "faceId":I
    .restart local v6    # "faceId":I
    goto/16 :goto_6

    .restart local v15    # "count":I
    .restart local v21    # "i":I
    .restart local v31    # "tempFaces":[Lcom/sec/android/gallery3d/data/Face;
    :cond_10
    move/from16 v22, v21

    .end local v21    # "i":I
    .restart local v22    # "i":I
    goto/16 :goto_1

    .end local v6    # "faceId":I
    .end local v8    # "tempRecommendId":I
    .end local v9    # "groupId":I
    .end local v15    # "count":I
    .end local v22    # "i":I
    .end local v31    # "tempFaces":[Lcom/sec/android/gallery3d/data/Face;
    .restart local v18    # "faceId":I
    .restart local v20    # "groupId":I
    .restart local v32    # "tempRecommendId":I
    :cond_11
    move/from16 v8, v32

    .end local v32    # "tempRecommendId":I
    .restart local v8    # "tempRecommendId":I
    move/from16 v9, v20

    .end local v20    # "groupId":I
    .restart local v9    # "groupId":I
    move/from16 v6, v18

    .end local v18    # "faceId":I
    .restart local v6    # "faceId":I
    goto/16 :goto_4
.end method

.method public declared-synchronized loadRectOfAllFaces()V
    .locals 5

    .prologue
    .line 1059
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->isHiddenItem()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 1073
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1063
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    const/high16 v2, -0x40000000    # -2.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 1067
    :cond_2
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->getAllFacesRectFromCache(I)Landroid/graphics/RectF;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;

    .line 1068
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;

    if-nez v1, :cond_0

    .line 1069
    new-instance v0, Landroid/graphics/RectF;

    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1071
    .local v0, "rect":Landroid/graphics/RectF;
    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1059
    .end local v0    # "rect":Landroid/graphics/RectF;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public reloadName()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 896
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    .line 897
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "_data"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 899
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 900
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 904
    :cond_0
    if-eqz v6, :cond_1

    .line 905
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 908
    :cond_1
    return-void

    .line 904
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 905
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public removeManualFD()V
    .locals 8

    .prologue
    .line 1165
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    if-nez v6, :cond_1

    .line 1177
    :cond_0
    return-void

    .line 1168
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/Face;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 1169
    .local v1, "face":Lcom/sec/android/gallery3d/data/Face;
    if-nez v1, :cond_3

    .line 1168
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1170
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Face;->getPersonId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1171
    .local v4, "personId":I
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Face;->getRecommendedId()I

    move-result v5

    .line 1173
    .local v5, "recommendeId":I
    const/4 v6, 0x1

    if-le v5, v6, :cond_4

    if-eq v5, v4, :cond_2

    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Face;->getAutoGroup()I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_2

    .line 1174
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v7

    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/access/face/FaceList;->remove(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 9
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 303
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    .line 304
    .local v7, "path":Ljava/lang/String;
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v0, :cond_1

    .line 306
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->faces:[Lcom/sec/android/gallery3d/data/Face;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Face;->getFacePath()Ljava/lang/String;

    move-result-object v7

    .line 313
    :cond_0
    :goto_0
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateModifiedInSec:J

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsGolf:Z

    move v6, p1

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;Z)V

    return-object v1

    .line 309
    :cond_1
    const/4 p1, 0x2

    goto :goto_0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 476
    new-instance v0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    return-object v0
.end method

.method public rotate(I)V
    .locals 18
    .param p1, "degrees"    # I

    .prologue
    .line 718
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 719
    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 720
    .local v2, "baseUri":Landroid/net/Uri;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 721
    .local v11, "values":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    add-int v12, v12, p1

    rem-int/lit16 v9, v12, 0x168

    .line 722
    .local v9, "rotation":I
    if-gez v9, :cond_0

    .line 723
    add-int/lit16 v9, v9, 0x168

    .line 725
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    const-string v13, "image/jpeg"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 726
    new-instance v7, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v7}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 727
    .local v7, "exifInterface":Lcom/sec/android/gallery3d/exif/ExifInterface;
    sget v12, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_ORIENTATION:I

    invoke-static {v9}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getOrientationValueForRotation(I)S

    move-result v13

    invoke-static {v13}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    invoke-virtual {v7, v12, v13}, Lcom/sec/android/gallery3d/exif/ExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v10

    .line 729
    .local v10, "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    if-eqz v10, :cond_1

    .line 731
    :try_start_0
    invoke-virtual {v7, v10}, Lcom/sec/android/gallery3d/exif/ExifInterface;->setTag(Lcom/sec/android/gallery3d/exif/ExifTag;)Lcom/sec/android/gallery3d/exif/ExifTag;

    .line 732
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-virtual {v7, v12}, Lcom/sec/android/gallery3d/exif/ExifInterface;->forceRewriteExif(Ljava/lang/String;)V

    .line 733
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->fileSize:J

    .line 734
    const-string v12, "_size"

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/android/gallery3d/data/LocalImage;->fileSize:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 753
    :cond_1
    :goto_0
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->fileSize:J

    .line 754
    const-string v12, "_size"

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/android/gallery3d/data/LocalImage;->fileSize:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 755
    const-string v12, "orientation"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 756
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v12}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "_id=?"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v2, v11, v13, v14}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->update(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 759
    .local v8, "result":I
    sget-object v12, Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Update row count ["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v12, :cond_2

    .line 762
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v12}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v12

    const-string v13, "images_event_album"

    const-string v14, "_id=?"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v12, v13, v11, v14, v15}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 769
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v12}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-static {v12, v13}, Lcom/sec/android/gallery3d/util/GalleryUtils;->notifyImageRotation(Landroid/content/Context;Ljava/lang/String;)V

    .line 771
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsRotateSearch:Z

    if-eqz v12, :cond_3

    .line 772
    move-object/from16 v0, p0

    iput v9, v0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    .line 773
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalImage;->nextVersionNumber()J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mDataVersion:J

    .line 774
    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/gallery3d/data/LocalImage;->setRotateForSearch(Z)V

    .line 777
    :cond_3
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->setVersion()V

    .line 780
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mImageNoteType:I

    if-lez v12, :cond_4

    .line 781
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v12, :cond_4

    .line 782
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v12}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    move/from16 v0, p1

    int-to-double v14, v0

    invoke-static {v12, v13, v14, v15}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteUtils;->rotateNoteDoc(Landroid/content/Context;Ljava/lang/String;D)V

    .line 795
    .end local v7    # "exifInterface":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .end local v8    # "result":I
    .end local v10    # "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    :cond_4
    :goto_1
    return-void

    .line 735
    .restart local v7    # "exifInterface":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .restart local v10    # "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    :catch_0
    move-exception v4

    .line 736
    .local v4, "e":Ljava/io/FileNotFoundException;
    sget-object v12, Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;

    const-string v13, "cannot find file to set exif: "

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 738
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v4

    .line 739
    .local v4, "e":Ljava/io/IOException;
    sget-object v12, Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;

    const-string v13, "cannot set exif data - write orientation"

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    :try_start_1
    new-instance v6, Landroid/media/ExifInterface;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-direct {v6, v12}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 743
    .local v6, "exif":Landroid/media/ExifInterface;
    const-string v12, "Orientation"

    invoke-static {v9}, Lcom/sec/android/gallery3d/data/LocalImage;->getExifOrientation(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    invoke-virtual {v6}, Landroid/media/ExifInterface;->saveAttributes()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    .line 746
    .end local v6    # "exif":Landroid/media/ExifInterface;
    :catch_2
    move-exception v5

    .line 747
    .local v5, "e1":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 789
    .end local v4    # "e":Ljava/io/IOException;
    .end local v5    # "e1":Ljava/io/IOException;
    .end local v7    # "exifInterface":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .end local v10    # "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    :cond_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v12}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    .line 790
    .local v3, "context":Landroid/content/Context;
    const v12, 0x7f0e0107

    invoke-static {v3, v12}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method public setFaceImagePath(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/Long;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "version"    # Ljava/lang/Long;

    .prologue
    .line 1355
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePath:Lcom/sec/android/gallery3d/data/Path;

    .line 1356
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1357
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1365
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsFaceInfoDeleted:Z

    .line 1366
    return-void

    .line 1359
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1360
    .local v0, "ver":Ljava/lang/Long;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1361
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setFaceImagePathForExpansion(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 1395
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePathForExpansion:Lcom/sec/android/gallery3d/data/Path;

    .line 1396
    return-void
.end method

.method public setFacePaths(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1458
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    .line 1459
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsFaceInfoDeleted:Z

    .line 1460
    return-void
.end method

.method public setRectOfAllFaces(Landroid/graphics/RectF;)V
    .locals 3
    .param p1, "newRect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v2, 0x0

    const/high16 v1, -0x40000000    # -2.0f

    .line 1076
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1078
    .local v0, "rect":Landroid/graphics/RectF;
    if-eqz p1, :cond_0

    .line 1079
    iget v1, p1, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1080
    iget v1, p1, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1081
    iget v1, p1, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1082
    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1083
    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;

    .line 1084
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->facesRect:Landroid/graphics/RectF;

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->updateAllFacesRectInCache(ILandroid/graphics/RectF;)V

    .line 1086
    :cond_0
    return-void
.end method

.method public setRotateForSearch(Z)V
    .locals 0
    .param p1, "rotate"    # Z

    .prologue
    .line 1515
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsRotateSearch:Z

    .line 1516
    return-void
.end method

.method public updateArcDataVersion(J)V
    .locals 1
    .param p1, "arcDV"    # J

    .prologue
    .line 1411
    iput-wide p1, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcDataVersion:J

    .line 1412
    return-void
.end method

.method protected updateFaceDataVersion()V
    .locals 8

    .prologue
    .line 1369
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    if-nez v5, :cond_1

    .line 1388
    :cond_0
    return-void

    .line 1372
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFacePaths:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 1375
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    .line 1376
    .local v2, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mFaceImagePaths:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 1378
    .local v4, "paths":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Path;

    .line 1379
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v3, :cond_2

    .line 1380
    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    .line 1381
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v5, v1, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v5, :cond_3

    move-object v5, v1

    .line 1382
    check-cast v5, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->updateVersion()J

    .line 1383
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaObject;->getDataVersion()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/gallery3d/data/LocalImage;->setFaceImagePath(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/Long;)V

    goto :goto_0

    .line 1385
    :cond_3
    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mDataVersion:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/gallery3d/data/LocalImage;->setFaceImagePath(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/Long;)V

    goto :goto_0
.end method

.method protected updateFromCursor(Landroid/database/Cursor;)Z
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 259
    new-instance v1, Lcom/sec/android/gallery3d/util/UpdateHelper;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/util/UpdateHelper;-><init>()V

    .line 260
    .local v1, "uh":Lcom/sec/android/gallery3d/util/UpdateHelper;
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    .line 261
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->caption:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->caption:Ljava/lang/String;

    .line 262
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    .line 263
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(DD)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    .line 264
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    const/4 v4, 0x4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(DD)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    .line 265
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateTakenInMs:J

    const/4 v4, 0x5

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateTakenInMs:J

    .line 266
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateAddedInSec:J

    const/4 v4, 0x6

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateAddedInSec:J

    .line 267
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateModifiedInSec:J

    const/4 v4, 0x7

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->dateModifiedInSec:J

    .line 268
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    const/16 v3, 0x8

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    .line 269
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    const/16 v3, 0x9

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    .line 270
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->bucketId:I

    const/16 v3, 0xa

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->bucketId:I

    .line 271
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->fileSize:J

    const/16 v4, 0xb

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->fileSize:J

    .line 274
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGroupId:Z

    if-eqz v2, :cond_0

    .line 275
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->groupId:J

    const/16 v4, 0xe

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    .line 277
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isdrm:Z

    if-nez v2, :cond_1

    .line 278
    const/16 v2, 0xc

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    .line 279
    const/16 v2, 0xd

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    .line 280
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcWidth:I

    .line 281
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mArcHeight:I

    .line 285
    :cond_1
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    if-eqz v2, :cond_2

    .line 286
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->sphericalMosaic:I

    const/16 v3, 0xf

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->sphericalMosaic:I

    .line 287
    :cond_2
    const/16 v2, 0x10

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    .line 289
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mCachedBurstImageCount:I

    .line 291
    invoke-virtual {p0, v1, p1, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->isItemRenamed(Lcom/sec/android/gallery3d/util/UpdateHelper;Landroid/database/Cursor;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 292
    .local v0, "result":Z
    :goto_0
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mAttributes:Lcom/sec/android/gallery3d/data/LocalImageAttributes;

    if-eqz v2, :cond_3

    .line 293
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->mAttributes:Lcom/sec/android/gallery3d/data/LocalImageAttributes;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->updateAttributes()V

    .line 294
    :cond_3
    return v0

    .line 291
    .end local v0    # "result":Z
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/util/UpdateHelper;->isUpdated()Z

    move-result v0

    goto :goto_0
.end method

.method public updateSoundShotAttribute()V
    .locals 3

    .prologue
    .line 1601
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSoundScene:Z

    if-nez v0, :cond_0

    .line 1606
    :goto_0
    return-void

    .line 1604
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundScene(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isSoundScene:Z

    .line 1605
    const-wide/16 v0, 0x10

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalImage;->isSoundScene:Z

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    goto :goto_0
.end method
