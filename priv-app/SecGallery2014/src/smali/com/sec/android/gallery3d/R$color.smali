.class public final Lcom/sec/android/gallery3d/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final action_bar_spinner_popup_item_text_color:I = 0x7f0b0050

.field public static final actionbar_bg_color:I = 0x7f0b0033

.field public static final actionbar_bg_ripple_color:I = 0x7f0b0034

.field public static final actionbar_menu_text_disabled:I = 0x7f0b0037

.field public static final actionbar_menu_text_enabled:I = 0x7f0b0036

.field public static final actionbar_menu_text_pressed:I = 0x7f0b0038

.field public static final actionbar_menu_text_selector:I = 0x7f0b0072

.field public static final actionbar_spinner_text_color:I = 0x7f0b0035

.field public static final add_tags_background:I = 0x7f0b006a

.field public static final album_background:I = 0x7f0b0009

.field public static final album_placeholder:I = 0x7f0b000a

.field public static final album_view_title_font_color:I = 0x7f0b005d

.field public static final albumset_background:I = 0x7f0b0003

.field public static final albumset_label_background:I = 0x7f0b0005

.field public static final albumset_label_count:I = 0x7f0b0007

.field public static final albumset_label_title:I = 0x7f0b0006

.field public static final albumset_placeholder:I = 0x7f0b0004

.field public static final albumset_reorder_stroke:I = 0x7f0b0008

.field public static final alert_dialog_contents_text_color:I = 0x7f0b0067

.field public static final best_photo_text_color:I = 0x7f0b0073

.field public static final bitmap_screennail_placeholder:I = 0x7f0b000f

.field public static final breadcrumb_separator:I = 0x7f0b0030

.field public static final bright_foreground_disabled_holo_dark:I = 0x7f0b0026

.field public static final bright_foreground_holo_dark:I = 0x7f0b0027

.field public static final button_dark_transparent_background:I = 0x7f0b0012

.field public static final cache_background:I = 0x7f0b000d

.field public static final cache_placeholder:I = 0x7f0b000e

.field public static final common_header_subtitle:I = 0x7f0b0045

.field public static final common_header_subtitle_line:I = 0x7f0b0046

.field public static final common_header_title:I = 0x7f0b0044

.field public static final common_list_subtitle:I = 0x7f0b0043

.field public static final common_list_title:I = 0x7f0b0042

.field public static final content_disable_text_white_theme_color:I = 0x7f0b0074

.field public static final content_text_color:I = 0x7f0b0075

.field public static final crop_shadow_color:I = 0x7f0b006c

.field public static final crop_shadow_wp_color:I = 0x7f0b006d

.field public static final crop_wp_markers:I = 0x7f0b006e

.field public static final custom_theme_color:I = 0x7f0b0070

.field public static final darker_transparent:I = 0x7f0b006f

.field public static final default_background:I = 0x7f0b0000

.field public static final delete_history_list_text_color:I = 0x7f0b0064

.field public static final detailview_actionbar_bg_color:I = 0x7f0b004d

.field public static final dialog_title_shadow:I = 0x7f0b0031

.field public static final drawer_expandable_header:I = 0x7f0b0048

.field public static final drawer_expandable_header_pressed:I = 0x7f0b0049

.field public static final drawer_expandable_title:I = 0x7f0b0047

.field public static final drawer_item_text:I = 0x7f0b004a

.field public static final drawer_item_text_normal:I = 0x7f0b004b

.field public static final drawer_item_text_selected:I = 0x7f0b004c

.field public static final drawer_list_header_expandable_selector:I = 0x7f0b0076

.field public static final edit_location_gps_popup_text_color:I = 0x7f0b0058

.field public static final event_view_suggestion_text_bg_color:I = 0x7f0b006b

.field public static final evf_lcd_touch_screen_title:I = 0x7f0b0032

.field public static final face_detect_fail:I = 0x7f0b002a

.field public static final face_detect_start:I = 0x7f0b0028

.field public static final face_detect_success:I = 0x7f0b0029

.field public static final facetag_communication_button_text_color:I = 0x7f0b0077

.field public static final gallerysearch_history_text_color:I = 0x7f0b0078

.field public static final gray:I = 0x7f0b002b

.field public static final holo_blue_light:I = 0x7f0b0025

.field public static final icon_disabled_color:I = 0x7f0b001b

.field public static final indicator_background:I = 0x7f0b001d

.field public static final ingest_date_tile_text:I = 0x7f0b0014

.field public static final ingest_highlight_semitransparent:I = 0x7f0b0013

.field public static final list_text_main:I = 0x7f0b0079

.field public static final location_items_count_text_color:I = 0x7f0b0053

.field public static final location_touched_items_count_text_color:I = 0x7f0b0054

.field public static final magazine_widget_text:I = 0x7f0b0059

.field public static final magazine_widget_text_shadow:I = 0x7f0b005a

.field public static final map_view_thumbnaillist_color:I = 0x7f0b005e

.field public static final map_view_thumbnaillist_map_address_color:I = 0x7f0b005f

.field public static final mode_selection_border:I = 0x7f0b0024

.field public static final moreinfo_content_text_color:I = 0x7f0b003e

.field public static final moreinfo_default_bg_color:I = 0x7f0b003d

.field public static final moreinfo_edit_text_color:I = 0x7f0b0040

.field public static final moreinfo_list_item_text_color:I = 0x7f0b003f

.field public static final new_tagcloud_item_text_color_selector:I = 0x7f0b007a

.field public static final on_viewfinder_label_background_color:I = 0x7f0b0017

.field public static final pano_progress_done:I = 0x7f0b0021

.field public static final pano_progress_empty:I = 0x7f0b0020

.field public static final pano_progress_indication:I = 0x7f0b0022

.field public static final pano_progress_indication_fast:I = 0x7f0b0023

.field public static final photo_background:I = 0x7f0b000b

.field public static final photo_placeholder:I = 0x7f0b000c

.field public static final popup_background:I = 0x7f0b001f

.field public static final popup_title_color:I = 0x7f0b001e

.field public static final primary_text:I = 0x7f0b007b

.field public static final quick_Scroll_center_text_color:I = 0x7f0b0066

.field public static final quick_scroll_text_color:I = 0x7f0b0065

.field public static final recording_time_elapsed_text:I = 0x7f0b0015

.field public static final recording_time_remaining_text:I = 0x7f0b0016

.field public static final review_background:I = 0x7f0b001a

.field public static final review_control_pressed_color:I = 0x7f0b0018

.field public static final review_control_pressed_fan_color:I = 0x7f0b0019

.field public static final search_input_field_cursor_color:I = 0x7f0b0061

.field public static final search_input_field_text:I = 0x7f0b0062

.field public static final search_no_item_view_bg_color:I = 0x7f0b0068

.field public static final search_no_item_view_text_color:I = 0x7f0b0069

.field public static final search_spannable_text:I = 0x7f0b0063

.field public static final search_tag_list_bg_color:I = 0x7f0b0071

.field public static final searchview_background_color:I = 0x7f0b0057

.field public static final searchview_hint_color:I = 0x7f0b0055

.field public static final searchview_text_color:I = 0x7f0b0056

.field public static final selection_mode_bar_title_selector:I = 0x7f0b007c

.field public static final selection_mode_title_disabled_color:I = 0x7f0b004f

.field public static final selection_mode_title_enabled_color:I = 0x7f0b004e

.field public static final settings_activity_background:I = 0x7f0b002d

.field public static final settings_item_title:I = 0x7f0b002c

.field public static final settings_main_frag_list_text_item_color:I = 0x7f0b002f

.field public static final settings_main_frag_text_selector:I = 0x7f0b007d

.field public static final settings_main_fragment_footer:I = 0x7f0b002e

.field public static final slideshow_background:I = 0x7f0b0010

.field public static final slideshow_list_item_focus:I = 0x7f0b0041

.field public static final slideshow_pause_text_selector:I = 0x7f0b007e

.field public static final slideshowsetting_add_music_item:I = 0x7f0b0011

.field public static final splitview_background_color:I = 0x7f0b0051

.field public static final splitview_new_album_sperator_color:I = 0x7f0b0052

.field public static final tab_indicator_selector:I = 0x7f0b007f

.field public static final tab_indicator_selector_white:I = 0x7f0b0080

.field public static final tab_list_item_text_color_dark:I = 0x7f0b0081

.field public static final tab_list_item_text_color_white:I = 0x7f0b0082

.field public static final tag_buddy_preview_in_settings:I = 0x7f0b005b

.field public static final tag_buddy_textcolor:I = 0x7f0b005c

.field public static final tag_cloud_button_select_anim_from_color:I = 0x7f0b0039

.field public static final tag_cloud_button_select_anim_to_color:I = 0x7f0b003a

.field public static final tag_cloud_list_bg_color:I = 0x7f0b003b

.field public static final tag_cloud_list_expanded_bg_color:I = 0x7f0b003c

.field public static final tagcloud_item_text_color_selector:I = 0x7f0b0083

.field public static final tagcloud_title_text_color_selector:I = 0x7f0b0084

.field public static final time_lapse_arc:I = 0x7f0b001c

.field public static final tw_background:I = 0x7f0b0001

.field public static final white_search_color:I = 0x7f0b0060

.field public static final winset_dialog_title_color:I = 0x7f0b0002


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
