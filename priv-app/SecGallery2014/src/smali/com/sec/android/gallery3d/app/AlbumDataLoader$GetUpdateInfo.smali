.class Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;
.super Ljava/lang/Object;
.source "AlbumDataLoader.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/AlbumDataLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetUpdateInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final mVersion:J

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AlbumDataLoader;J)V
    .locals 0
    .param p2, "version"    # J

    .prologue
    .line 288
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289
    iput-wide p2, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->mVersion:J

    .line 290
    return-void
.end method


# virtual methods
.method public call()Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v14, 0x10

    const/4 v8, 0x0

    .line 294
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mFailedVersion:J
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$100(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)J

    move-result-wide v10

    iget-wide v12, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->mVersion:J

    cmp-long v9, v10, v12

    if-nez v9, :cond_1

    move-object v2, v8

    .line 326
    :cond_0
    :goto_0
    return-object v2

    .line 298
    :cond_1
    new-instance v2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;

    invoke-direct {v2, v8}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;-><init>(Lcom/sec/android/gallery3d/app/AlbumDataLoader$1;)V

    .line 299
    .local v2, "info":Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;
    iget-wide v6, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->mVersion:J

    .line 300
    .local v6, "version":J
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSourceVersion:J
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$400(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)J

    move-result-wide v10

    iput-wide v10, v2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->version:J

    .line 301
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSize:I
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$500(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I

    move-result v9

    iput v9, v2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->size:I

    .line 302
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSetVersion:[J
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$600(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)[J

    move-result-object v5

    .line 304
    .local v5, "setVersion":[J
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentEnd:I
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$700(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I
    invoke-static {v10}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$800(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I

    move-result v10

    if-eq v9, v10, :cond_3

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mStartIndex:I
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$900(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I

    move-result v9

    if-lez v9, :cond_3

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mbRemoteDevice:Z
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$1000(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 305
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mStartIndex:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$900(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # invokes: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->getPatchCount()I
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$1100(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I

    move-result v9

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$800(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    iput v8, v2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->reloadStart:I

    .line 306
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSize:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$500(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I

    move-result v8

    iget v9, v2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->reloadStart:I

    sub-int/2addr v8, v9

    if-ge v8, v14, :cond_2

    .line 307
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$800(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSize:I
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$500(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I

    move-result v9

    add-int/lit8 v9, v9, -0x10

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    iput v8, v2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->reloadStart:I

    .line 309
    :cond_2
    iput v14, v2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->reloadCount:I

    .line 310
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mStartIndex:I
    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$902(Lcom/sec/android/gallery3d/app/AlbumDataLoader;I)I

    goto :goto_0

    .line 314
    :cond_3
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentStart:I
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$800(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I

    move-result v0

    .local v0, "i":I
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mContentEnd:I
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$700(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)I

    move-result v4

    .local v4, "n":I
    :goto_1
    if-ge v0, v4, :cond_6

    .line 315
    rem-int/lit16 v1, v0, 0x80

    .line 316
    .local v1, "index":I
    aget-wide v10, v5, v1

    cmp-long v9, v10, v6

    if-eqz v9, :cond_5

    .line 317
    iput v0, v2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->reloadStart:I

    .line 318
    const/16 v3, 0x40

    .line 319
    .local v3, "load_cnt":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mbRemoteDevice:Z
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$1000(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 320
    const/16 v3, 0x1388

    .line 322
    :cond_4
    sub-int v8, v4, v0

    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    iput v8, v2, Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;->reloadCount:I

    goto/16 :goto_0

    .line 314
    .end local v3    # "load_cnt":I
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 326
    .end local v1    # "index":I
    :cond_6
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumDataLoader;->mSourceVersion:J
    invoke-static {v9}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->access$400(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)J

    move-result-wide v10

    iget-wide v12, p0, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->mVersion:J

    cmp-long v9, v10, v12

    if-nez v9, :cond_0

    move-object v2, v8

    goto/16 :goto_0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AlbumDataLoader$GetUpdateInfo;->call()Lcom/sec/android/gallery3d/app/AlbumDataLoader$UpdateInfo;

    move-result-object v0

    return-object v0
.end method
