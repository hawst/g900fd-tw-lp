.class public Lcom/sec/android/gallery3d/data/BurstshotAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "BurstshotAlbum.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BurstshotAlbum"

.field public static final TOP_PATH:Lcom/sec/android/gallery3d/data/Path;


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mGroupId:Ljava/lang/Long;

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-string v0, "/local/burstshot"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->TOP_PATH:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/lang/Long;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "groupId"    # Ljava/lang/Long;

    .prologue
    .line 40
    invoke-static {}, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mList:Ljava/util/ArrayList;

    .line 41
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 42
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mGroupId:Ljava/lang/Long;

    .line 43
    return-void
.end method


# virtual methods
.method public createBurstshotMediaSet()Z
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 96
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 97
    .local v8, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 98
    .local v0, "mResolver":Landroid/content/ContentResolver;
    const/4 v7, 0x0

    .line 99
    .local v7, "cursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 101
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "group_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mGroupId:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 102
    .local v3, "selection":Ljava/lang/String;
    const-string v5, "datetaken DESC, _id DESC"

    .line 103
    .local v5, "orderClause":Ljava/lang/String;
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 105
    if-nez v7, :cond_0

    .line 122
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v1, v12

    .line 124
    .end local v3    # "selection":Ljava/lang/String;
    .end local v5    # "orderClause":Ljava/lang/String;
    :goto_0
    return v1

    .line 108
    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v5    # "orderClause":Ljava/lang/String;
    :cond_0
    :goto_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 109
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 110
    .local v10, "id":I
    sget-object v1, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1, v10}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 111
    .local v6, "childPath":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v8, v6}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 112
    .local v11, "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    if-nez v11, :cond_1

    .line 113
    new-instance v11, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v11    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v11, v6, v1, v7}, Lcom/sec/android/gallery3d/data/LocalImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .line 117
    .restart local v11    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :goto_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 119
    .end local v3    # "selection":Ljava/lang/String;
    .end local v5    # "orderClause":Ljava/lang/String;
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v10    # "id":I
    .end local v11    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :catch_0
    move-exception v9

    .line 120
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 122
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 124
    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_3
    const/4 v1, 0x1

    goto :goto_0

    .line 115
    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v5    # "orderClause":Ljava/lang/String;
    .restart local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v10    # "id":I
    .restart local v11    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_1
    :try_start_3
    invoke-virtual {v11, v7}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->updateContent(Landroid/database/Cursor;)V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 122
    .end local v3    # "selection":Ljava/lang/String;
    .end local v5    # "orderClause":Ljava/lang/String;
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v10    # "id":I
    .end local v11    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :catchall_0
    move-exception v1

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1

    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v5    # "orderClause":Ljava/lang/String;
    :cond_2
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_3
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 3
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int/2addr v1, p1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 49
    if-gtz p2, :cond_0

    .line 53
    :goto_0
    return-object v0

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mList:Ljava/util/ArrayList;

    add-int v2, p1, p2

    invoke-virtual {v1, p1, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPathOnFileSystem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 71
    const-wide/16 v0, 0x1

    .line 72
    .local v0, "supported":J
    return-wide v0
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    return v0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 64
    invoke-static {}, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mDataVersion:J

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->createBurstshotMediaSet()Z

    .line 66
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/BurstshotAlbum;->mDataVersion:J

    return-wide v0
.end method
