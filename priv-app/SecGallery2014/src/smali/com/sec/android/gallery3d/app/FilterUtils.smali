.class public Lcom/sec/android/gallery3d/app/FilterUtils;
.super Ljava/lang/Object;
.source "FilterUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/FilterUtils$1;
    }
.end annotation


# static fields
.field public static final CLUSTER_AUTO_BY_FACE_RECOGNITION:I = 0x100

.field public static final CLUSTER_BY_ALBUM:I = 0x1

.field public static final CLUSTER_BY_DOCUMENT:I = 0x400

.field public static final CLUSTER_BY_FACE:I = 0x20

.field public static final CLUSTER_BY_FACE_CONFIRMED:I = 0x8000

.field public static final CLUSTER_BY_FACE_CONFIRMED_RECOMMEND:I = 0x10000

.field public static final CLUSTER_BY_FACE_NAMED:I = 0x4000

.field public static final CLUSTER_BY_FACE_RECOGNITION:I = 0x40

.field public static final CLUSTER_BY_FESTIVAL:I = 0x100000

.field public static final CLUSTER_BY_GALLERYSEARCH:I = 0x40000

.field public static final CLUSTER_BY_GALLERYSEARCHALL:I = 0x80000

.field public static final CLUSTER_BY_GROUP:I = 0x80

.field public static final CLUSTER_BY_LOCATION:I = 0x4

.field public static final CLUSTER_BY_ONEALBUM:I = 0x200

.field public static final CLUSTER_BY_SEARCH:I = 0x800

.field public static final CLUSTER_BY_SIZE:I = 0x10

.field public static final CLUSTER_BY_TAG:I = 0x8

.field public static final CLUSTER_BY_TIME:I = 0x2

.field public static final CLUSTER_BY_TIME_SEARCH:I = 0x1000

.field private static final CLUSTER_CURRENT_TYPE:I = 0x4

.field private static final CLUSTER_TYPE:I = 0x0

.field private static final CLUSTER_TYPE_AUTOFACE:Ljava/lang/String; = "autoface"

.field public static final CLUSTER_TYPE_DOCUMENT:Ljava/lang/String; = "document"

.field public static final CLUSTER_TYPE_EVENT:Ljava/lang/String; = "event"

.field private static final CLUSTER_TYPE_F:I = 0x2

.field public static final CLUSTER_TYPE_FACE:Ljava/lang/String; = "face"

.field public static final CLUSTER_TYPE_FACE_CONFIRMED:Ljava/lang/String; = "face_confirmed"

.field public static final CLUSTER_TYPE_FACE_CONFIRMED_RECOMMEND:Ljava/lang/String; = "face_confirmed_recommend"

.field public static final CLUSTER_TYPE_FACE_NAMED:Ljava/lang/String; = "face_named"

.field public static final CLUSTER_TYPE_FESTIVAL:Ljava/lang/String; = "festival"

.field public static final CLUSTER_TYPE_GALLERYSEARCH:Ljava/lang/String; = "gallerysearch"

.field public static final CLUSTER_TYPE_GALLERYSEARCHALL:Ljava/lang/String; = "gallerysearchall"

.field public static final CLUSTER_TYPE_GROUP:Ljava/lang/String; = "group"

.field private static final CLUSTER_TYPE_LOCATION:Ljava/lang/String; = "location"

.field public static final CLUSTER_TYPE_PEOPLE:Ljava/lang/String; = "people"

.field public static final CLUSTER_TYPE_SEARCH:Ljava/lang/String; = "search"

.field private static final CLUSTER_TYPE_SIZE:Ljava/lang/String; = "size"

.field private static final CLUSTER_TYPE_TAG:Ljava/lang/String; = "tag"

.field private static final CLUSTER_TYPE_TIME:Ljava/lang/String; = "time"

.field public static final CLUSTER_TYPE_TIME_SEARCH:Ljava/lang/String; = "time_search"

.field private static final DEBUG:Z = false

.field public static final FILTER_ALL:I = 0x4

.field private static final FILTER_CURRENT_TYPE:I = 0x5

.field public static final FILTER_IMAGE_ONLY:I = 0x1

.field public static final FILTER_MIMETYPE_ALL:Ljava/lang/String; = "all"

.field public static final FILTER_MIME_TYPE:I = 0x8

.field private static final FILTER_TYPE:I = 0x1

.field private static final FILTER_TYPE_F:I = 0x3

.field public static final FILTER_VIDEO_ONLY:I = 0x2

.field private static final TAG:Ljava/lang/String; = "FilterUtils"

.field private static mIsClustered:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/app/FilterUtils;->mIsClustered:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 461
    return-void
.end method

.method private static getAppliedFilters(Lcom/sec/android/gallery3d/data/Path;[I)V
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "result"    # [I

    .prologue
    .line 171
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/gallery3d/app/FilterUtils;->getAppliedFilters(Lcom/sec/android/gallery3d/data/Path;[IZ)V

    .line 172
    return-void
.end method

.method private static getAppliedFilters(Lcom/sec/android/gallery3d/data/Path;[IZ)V
    .locals 11
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "result"    # [I
    .param p2, "underCluster"    # Z

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v3

    .line 177
    .local v3, "segments":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v3

    if-ge v1, v6, :cond_1

    .line 178
    aget-object v6, v3, v1

    const-string/jumbo v7, "{"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 179
    aget-object v6, v3, v1

    invoke-static {v6}, Lcom/sec/android/gallery3d/data/Path;->splitSequence(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 180
    .local v4, "sets":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    array-length v6, v4

    if-ge v2, v6, :cond_0

    .line 181
    aget-object v6, v4, v2

    invoke-static {v6}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    .line 182
    .local v5, "sub":Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v5, p1, p2}, Lcom/sec/android/gallery3d/app/FilterUtils;->getAppliedFilters(Lcom/sec/android/gallery3d/data/Path;[IZ)V

    .line 180
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 177
    .end local v2    # "j":I
    .end local v4    # "sets":[Ljava/lang/String;
    .end local v5    # "sub":Lcom/sec/android/gallery3d/data/Path;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 188
    :cond_1
    aget-object v6, v3, v8

    const-string v7, "cluster"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 190
    array-length v6, v3

    if-ne v6, v10, :cond_2

    .line 191
    const/4 p2, 0x1

    .line 194
    :cond_2
    aget-object v6, v3, v9

    invoke-static {v6}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Ljava/lang/String;)I

    move-result v0

    .line 195
    .local v0, "ctype":I
    aget v6, p1, v8

    or-int/2addr v6, v0

    aput v6, p1, v8

    .line 196
    aput v0, p1, v10

    .line 197
    if-eqz p2, :cond_3

    .line 198
    aget v6, p1, v9

    or-int/2addr v6, v0

    aput v6, p1, v9

    .line 201
    .end local v0    # "ctype":I
    :cond_3
    return-void
.end method

.method public static getFilerBasePath(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "mediaType"    # I
    .param p2, "filterMimeType"    # Ljava/lang/String;

    .prologue
    .line 387
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/filter/mediatype/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 388
    .local v0, "basePath":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 389
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "##"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 394
    :cond_0
    return-object v0
.end method

.method public static isClustered()Z
    .locals 1

    .prologue
    .line 447
    sget-boolean v0, Lcom/sec/android/gallery3d/app/FilterUtils;->mIsClustered:Z

    return v0
.end method

.method public static newClusterPath(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "clusterType"    # I

    .prologue
    .line 247
    sparse-switch p1, :sswitch_data_0

    .line 305
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/gallery3d/app/FilterUtils;->mIsClustered:Z

    .line 312
    .end local p0    # "base":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 249
    .restart local p0    # "base":Ljava/lang/String;
    :sswitch_0
    const-string/jumbo v0, "time"

    .line 310
    .local v0, "kind":Ljava/lang/String;
    :goto_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/app/FilterUtils;->mIsClustered:Z

    .line 312
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/cluster/{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 252
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_1
    const-string v0, "location"

    .line 253
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 255
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_2
    const-string/jumbo v0, "tag"

    .line 256
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 258
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_3
    const-string/jumbo v0, "size"

    .line 259
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 261
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_4
    const-string v0, "face"

    .line 262
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 265
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_5
    const-string v0, "face_named"

    .line 266
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 268
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_6
    const-string v0, "face_confirmed"

    .line 269
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 271
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_7
    const-string v0, "face_confirmed_recommend"

    .line 272
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 274
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_8
    const-string v0, "group"

    .line 275
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 277
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_9
    const-string v0, "autoface"

    .line 278
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 281
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_a
    const-string v0, "document"

    .line 282
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 285
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_b
    const-string v0, "festival"

    .line 286
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 288
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_c
    const-string v0, "onealbum"

    .line 289
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 291
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_d
    const-string v0, "search"

    .line 292
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 294
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_e
    const-string v0, "gallerysearchall"

    .line 295
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 297
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_f
    const-string v0, "gallerysearch"

    .line 298
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 300
    .end local v0    # "kind":Ljava/lang/String;
    :sswitch_10
    const-string/jumbo v0, "time_search"

    .line 301
    .restart local v0    # "kind":Ljava/lang/String;
    goto :goto_1

    .line 247
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_2
        0x10 -> :sswitch_3
        0x20 -> :sswitch_4
        0x80 -> :sswitch_8
        0x100 -> :sswitch_9
        0x200 -> :sswitch_c
        0x400 -> :sswitch_a
        0x800 -> :sswitch_d
        0x1000 -> :sswitch_10
        0x4000 -> :sswitch_5
        0x8000 -> :sswitch_6
        0x10000 -> :sswitch_7
        0x40000 -> :sswitch_f
        0x80000 -> :sswitch_e
        0x100000 -> :sswitch_b
    .end sparse-switch
.end method

.method public static newFilterPath(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "filterType"    # I

    .prologue
    .line 230
    packed-switch p1, :pswitch_data_0

    .line 241
    .end local p0    # "base":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 232
    .restart local p0    # "base":Ljava/lang/String;
    :pswitch_0
    const/4 v0, 0x2

    .line 241
    .local v0, "mediaType":I
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/filter/mediatype/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 235
    .end local v0    # "mediaType":I
    :pswitch_1
    const/4 v0, 0x4

    .line 236
    .restart local v0    # "mediaType":I
    goto :goto_1

    .line 230
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static newFilterPath(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "filterType"    # I
    .param p2, "filterMimeType"    # Ljava/lang/String;

    .prologue
    .line 360
    packed-switch p1, :pswitch_data_0

    .line 370
    .end local p0    # "base":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 362
    .restart local p0    # "base":Ljava/lang/String;
    :pswitch_0
    const/4 v0, 0x2

    .line 370
    .local v0, "mediaType":I
    :goto_1
    invoke-static {p0, v0, p2}, Lcom/sec/android/gallery3d/app/FilterUtils;->getFilerBasePath(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 365
    .end local v0    # "mediaType":I
    :pswitch_1
    const/4 v0, 0x4

    .line 366
    .restart local v0    # "mediaType":I
    goto :goto_1

    .line 360
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static newFilterPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "folder"    # Ljava/lang/String;

    .prologue
    .line 375
    const/4 v0, 0x0

    .line 376
    .local v0, "basePath":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 377
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/filter/folder/1/{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 378
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "##"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 382
    :goto_0
    return-object v0

    .line 380
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/filter/folder/0/{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static removeOneClusterFromPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "base"    # Ljava/lang/String;

    .prologue
    .line 322
    const/4 v1, 0x1

    new-array v0, v1, [Z

    .line 323
    .local v0, "done":[Z
    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/app/FilterUtils;->removeOneClusterFromPath(Ljava/lang/String;[Z)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static removeOneClusterFromPath(Ljava/lang/String;[Z)Ljava/lang/String;
    .locals 9
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "done"    # [Z

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 327
    aget-boolean v5, p1, v7

    if-eqz v5, :cond_0

    .line 352
    .end local p0    # "base":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 329
    .restart local p0    # "base":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lcom/sec/android/gallery3d/data/Path;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 330
    .local v3, "segments":[Ljava/lang/String;
    aget-object v5, v3, v7

    const-string v6, "cluster"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 331
    aput-boolean v8, p1, v7

    .line 332
    aget-object v5, v3, v8

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/Path;->splitSequence(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object p0, v5, v7

    goto :goto_0

    .line 335
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 336
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v5, v3

    if-ge v0, v5, :cond_5

    .line 337
    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    aget-object v5, v3, v0

    const-string/jumbo v6, "{"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 339
    const-string/jumbo v5, "{"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    aget-object v5, v3, v0

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/Path;->splitSequence(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 341
    .local v4, "sets":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    array-length v5, v4

    if-ge v1, v5, :cond_3

    .line 342
    if-lez v1, :cond_2

    .line 343
    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    :cond_2
    aget-object v5, v4, v1

    invoke-static {v5, p1}, Lcom/sec/android/gallery3d/app/FilterUtils;->removeOneClusterFromPath(Ljava/lang/String;[Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 347
    :cond_3
    const-string/jumbo v5, "}"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    .end local v1    # "j":I
    .end local v4    # "sets":[Ljava/lang/String;
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 349
    :cond_4
    aget-object v5, v3, v0

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 352
    :cond_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static removeOneFilterFromPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "base"    # Ljava/lang/String;

    .prologue
    .line 413
    const/4 v1, 0x1

    new-array v0, v1, [Z

    .line 414
    .local v0, "done":[Z
    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/app/FilterUtils;->removeOneFilterFromPath(Ljava/lang/String;[Z)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static removeOneFilterFromPath(Ljava/lang/String;[Z)Ljava/lang/String;
    .locals 12
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "done"    # [Z

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 418
    aget-boolean v8, p1, v10

    if-eqz v8, :cond_0

    .line 443
    .end local p0    # "base":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 420
    .restart local p0    # "base":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lcom/sec/android/gallery3d/data/Path;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 421
    .local v6, "segments":[Ljava/lang/String;
    aget-object v8, v6, v10

    const-string v9, "filter"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    aget-object v8, v6, v11

    const-string v9, "mediatype"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 422
    aput-boolean v11, p1, v10

    .line 423
    const/4 v8, 0x3

    aget-object v8, v6, v8

    invoke-static {v8}, Lcom/sec/android/gallery3d/data/Path;->splitSequence(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    aget-object p0, v8, v10

    goto :goto_0

    .line 426
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 427
    .local v4, "sb":Ljava/lang/StringBuilder;
    move-object v0, v6

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v5, v0, v1

    .line 428
    .local v5, "segment":Ljava/lang/String;
    const-string v8, "/"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_4

    invoke-virtual {v5, v10}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x7b

    if-ne v8, v9, :cond_4

    .line 430
    const-string/jumbo v8, "{"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    invoke-static {v5}, Lcom/sec/android/gallery3d/data/Path;->splitSequence(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 432
    .local v7, "sets":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    array-length v8, v7

    if-ge v2, v8, :cond_3

    .line 433
    if-lez v2, :cond_2

    .line 434
    const-string v8, ","

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 436
    :cond_2
    aget-object v8, v7, v2

    invoke-static {v8, p1}, Lcom/sec/android/gallery3d/app/FilterUtils;->removeOneFilterFromPath(Ljava/lang/String;[Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 438
    :cond_3
    const-string/jumbo v8, "}"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 427
    .end local v2    # "j":I
    .end local v7    # "sets":[Ljava/lang/String;
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 440
    :cond_4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 443
    .end local v5    # "segment":Ljava/lang/String;
    :cond_5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static setClustered(Z)V
    .locals 0
    .param p0, "var"    # Z

    .prologue
    .line 451
    sput-boolean p0, Lcom/sec/android/gallery3d/app/FilterUtils;->mIsClustered:Z

    .line 452
    return-void
.end method

.method private static setMenuItemApplied(Lcom/sec/android/gallery3d/app/GalleryActionBar;IZZ)V
    .locals 1
    .param p0, "model"    # Lcom/sec/android/gallery3d/app/GalleryActionBar;
    .param p1, "id"    # I
    .param p2, "applied"    # Z
    .param p3, "updateTitle"    # Z

    .prologue
    .line 220
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->setClusterItemEnabled(IZ)V

    .line 221
    return-void

    .line 220
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static setMenuItemAppliedEnabled(Lcom/sec/android/gallery3d/app/GalleryActionBar;IZZZ)V
    .locals 0
    .param p0, "model"    # Lcom/sec/android/gallery3d/app/GalleryActionBar;
    .param p1, "id"    # I
    .param p2, "applied"    # Z
    .param p3, "enabled"    # Z
    .param p4, "updateTitle"    # Z

    .prologue
    .line 224
    invoke-virtual {p0, p1, p3}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->setClusterItemEnabled(IZ)V

    .line 225
    return-void
.end method

.method public static setupMenuItems(Lcom/sec/android/gallery3d/app/GalleryActionBar;Lcom/sec/android/gallery3d/data/Path;Z)V
    .locals 12
    .param p0, "actionBar"    # Lcom/sec/android/gallery3d/app/GalleryActionBar;
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "inAlbum"    # Z

    .prologue
    const/4 v11, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 133
    const/4 v6, 0x6

    new-array v5, v6, [I

    .line 134
    .local v5, "result":[I
    invoke-static {p1, v5}, Lcom/sec/android/gallery3d/app/FilterUtils;->getAppliedFilters(Lcom/sec/android/gallery3d/data/Path;[I)V

    .line 135
    aget v1, v5, v8

    .line 136
    .local v1, "ctype":I
    aget v3, v5, v7

    .line 137
    .local v3, "ftype":I
    const/4 v6, 0x3

    aget v4, v5, v6

    .line 138
    .local v4, "ftypef":I
    aget v0, v5, v11

    .line 139
    .local v0, "ccurrent":I
    const/4 v6, 0x5

    aget v2, v5, v6

    .line 141
    .local v2, "fcurrent":I
    const/4 v10, 0x2

    and-int/lit8 v6, v1, 0x2

    if-eqz v6, :cond_1

    move v9, v7

    :goto_0
    and-int/lit8 v6, v0, 0x2

    if-eqz v6, :cond_2

    move v6, v7

    :goto_1
    invoke-static {p0, v10, v9, v6}, Lcom/sec/android/gallery3d/app/FilterUtils;->setMenuItemApplied(Lcom/sec/android/gallery3d/app/GalleryActionBar;IZZ)V

    .line 143
    and-int/lit8 v6, v1, 0x4

    if-eqz v6, :cond_3

    move v9, v7

    :goto_2
    and-int/lit8 v6, v0, 0x4

    if-eqz v6, :cond_4

    move v6, v7

    :goto_3
    invoke-static {p0, v11, v9, v6}, Lcom/sec/android/gallery3d/app/FilterUtils;->setMenuItemApplied(Lcom/sec/android/gallery3d/app/GalleryActionBar;IZZ)V

    .line 145
    const/16 v10, 0x8

    and-int/lit8 v6, v1, 0x8

    if-eqz v6, :cond_5

    move v9, v7

    :goto_4
    and-int/lit8 v6, v0, 0x8

    if-eqz v6, :cond_6

    move v6, v7

    :goto_5
    invoke-static {p0, v10, v9, v6}, Lcom/sec/android/gallery3d/app/FilterUtils;->setMenuItemApplied(Lcom/sec/android/gallery3d/app/GalleryActionBar;IZZ)V

    .line 147
    const/16 v10, 0x20

    and-int/lit8 v6, v1, 0x20

    if-eqz v6, :cond_7

    move v9, v7

    :goto_6
    and-int/lit8 v6, v0, 0x20

    if-eqz v6, :cond_8

    move v6, v7

    :goto_7
    invoke-static {p0, v10, v9, v6}, Lcom/sec/android/gallery3d/app/FilterUtils;->setMenuItemApplied(Lcom/sec/android/gallery3d/app/GalleryActionBar;IZZ)V

    .line 150
    if-eqz p2, :cond_0

    if-nez v1, :cond_9

    :cond_0
    move v6, v7

    :goto_8
    invoke-virtual {p0, v7, v6}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->setClusterItemVisibility(IZ)V

    .line 152
    const v10, 0x7f0f025f

    if-nez v1, :cond_a

    move v9, v7

    :goto_9
    if-nez v0, :cond_b

    move v6, v7

    :goto_a
    invoke-static {p0, v10, v9, v6}, Lcom/sec/android/gallery3d/app/FilterUtils;->setMenuItemApplied(Lcom/sec/android/gallery3d/app/GalleryActionBar;IZZ)V

    .line 157
    const v11, 0x7f0e037a

    and-int/lit8 v6, v3, 0x1

    if-eqz v6, :cond_c

    move v10, v7

    :goto_b
    and-int/lit8 v6, v3, 0x1

    if-nez v6, :cond_d

    if-nez v4, :cond_d

    move v9, v7

    :goto_c
    and-int/lit8 v6, v2, 0x1

    if-eqz v6, :cond_e

    move v6, v7

    :goto_d
    invoke-static {p0, v11, v10, v9, v6}, Lcom/sec/android/gallery3d/app/FilterUtils;->setMenuItemAppliedEnabled(Lcom/sec/android/gallery3d/app/GalleryActionBar;IZZZ)V

    .line 161
    const v11, 0x7f0e037b

    and-int/lit8 v6, v3, 0x2

    if-eqz v6, :cond_f

    move v10, v7

    :goto_e
    and-int/lit8 v6, v3, 0x2

    if-nez v6, :cond_10

    if-nez v4, :cond_10

    move v9, v7

    :goto_f
    and-int/lit8 v6, v2, 0x2

    if-eqz v6, :cond_11

    move v6, v7

    :goto_10
    invoke-static {p0, v11, v10, v9, v6}, Lcom/sec/android/gallery3d/app/FilterUtils;->setMenuItemAppliedEnabled(Lcom/sec/android/gallery3d/app/GalleryActionBar;IZZZ)V

    .line 165
    const v10, 0x7f0e037c

    if-nez v3, :cond_12

    move v9, v7

    :goto_11
    if-eqz v3, :cond_13

    if-nez v4, :cond_13

    move v6, v7

    :goto_12
    if-nez v2, :cond_14

    :goto_13
    invoke-static {p0, v10, v9, v6, v7}, Lcom/sec/android/gallery3d/app/FilterUtils;->setMenuItemAppliedEnabled(Lcom/sec/android/gallery3d/app/GalleryActionBar;IZZZ)V

    .line 167
    return-void

    :cond_1
    move v9, v8

    .line 141
    goto/16 :goto_0

    :cond_2
    move v6, v8

    goto/16 :goto_1

    :cond_3
    move v9, v8

    .line 143
    goto :goto_2

    :cond_4
    move v6, v8

    goto :goto_3

    :cond_5
    move v9, v8

    .line 145
    goto :goto_4

    :cond_6
    move v6, v8

    goto :goto_5

    :cond_7
    move v9, v8

    .line 147
    goto :goto_6

    :cond_8
    move v6, v8

    goto :goto_7

    :cond_9
    move v6, v8

    .line 150
    goto :goto_8

    :cond_a
    move v9, v8

    .line 152
    goto :goto_9

    :cond_b
    move v6, v8

    goto :goto_a

    :cond_c
    move v10, v8

    .line 157
    goto :goto_b

    :cond_d
    move v9, v8

    goto :goto_c

    :cond_e
    move v6, v8

    goto :goto_d

    :cond_f
    move v10, v8

    .line 161
    goto :goto_e

    :cond_10
    move v9, v8

    goto :goto_f

    :cond_11
    move v6, v8

    goto :goto_10

    :cond_12
    move v9, v8

    .line 165
    goto :goto_11

    :cond_13
    move v6, v8

    goto :goto_12

    :cond_14
    move v7, v8

    goto :goto_13
.end method

.method public static switchClusterPath(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "clusterType"    # I

    .prologue
    .line 317
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/FilterUtils;->removeOneClusterFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static switchFilterPath(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "filterType"    # I

    .prologue
    .line 399
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/FilterUtils;->removeOneFilterFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/gallery3d/app/FilterUtils;->newFilterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static switchFilterPath(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "filterType"    # I
    .param p2, "filterMimeType"    # Ljava/lang/String;

    .prologue
    .line 404
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/FilterUtils;->removeOneFilterFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/sec/android/gallery3d/app/FilterUtils;->newFilterPath(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static switchFilterPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "folder"    # Ljava/lang/String;

    .prologue
    .line 408
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/FilterUtils;->removeOneFilterFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/gallery3d/app/FilterUtils;->newFilterPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I
    .locals 3
    .param p0, "selectedTabTag"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    const/4 v0, 0x2

    .line 467
    sget-object v1, Lcom/sec/android/gallery3d/app/FilterUtils$1;->$SwitchMap$com$sec$samsung$gallery$core$TabTagType:[I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/core/TabTagType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 479
    const/4 v0, 0x0

    :goto_0
    :pswitch_0
    return v0

    .line 469
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 471
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 475
    :pswitch_3
    const/16 v0, 0x20

    goto :goto_0

    .line 467
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private static toClusterType(Ljava/lang/String;)I
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 204
    const-string/jumbo v0, "time"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    const/4 v0, 0x2

    .line 215
    :goto_0
    return v0

    .line 206
    :cond_0
    const-string v0, "location"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    const/4 v0, 0x4

    goto :goto_0

    .line 208
    :cond_1
    const-string/jumbo v0, "tag"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 209
    const/16 v0, 0x8

    goto :goto_0

    .line 210
    :cond_2
    const-string/jumbo v0, "size"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 211
    const/16 v0, 0x10

    goto :goto_0

    .line 212
    :cond_3
    const-string v0, "face"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 213
    const/16 v0, 0x20

    goto :goto_0

    .line 215
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static toFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)I
    .locals 2
    .param p0, "mediaFilterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    .line 455
    sget-object v0, Lcom/sec/android/gallery3d/app/FilterUtils$1;->$SwitchMap$com$sec$samsung$gallery$core$MediaType$MediaFilterType:[I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 463
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 457
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 459
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 461
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 455
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
