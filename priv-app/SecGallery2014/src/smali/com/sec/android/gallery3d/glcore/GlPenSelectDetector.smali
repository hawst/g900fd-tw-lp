.class public Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;
.super Ljava/lang/Object;
.source "GlPenSelectDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;
    }
.end annotation


# static fields
.field private static final LIST_SCROLL_INTERVAL:J = 0x12cL

.field private static final LIST_SCROLL_RECOGNITION_EVT:I = 0x0

.field private static final LIST_SCROLL_TICK_EVT:I = 0x1

.field private static final LIST_SCROLL_TICK_INTERVAL:I = 0xa

.field private static final MIN_FLING_VELOCITY:F = 500.0f

.field private static final MOVE_STEP:I = 0x14

.field private static final PAST_COUNT:I = 0x8

.field private static final PENMOVE_SCROLL_DETECT_AREA:I = 0x64

.field public static final PENMOVE_SCROLL_DOWN:I = 0x2

.field public static final PENMOVE_SCROLL_NONE:I = 0x0

.field public static final PENMOVE_SCROLL_UP:I = 0x1

.field public static final REACHED_BOTTOM_BOUNDARY:I = 0x1

.field public static final REACHED_TOP_BOUNDARY:I = -0x1

.field private static final TAG:Ljava/lang/String; = "GlPenSelectDetector"

.field private static sInstance:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;


# instance fields
.field private mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

.field private mEnd:Landroid/graphics/PointF;

.field private mHandled:Z

.field private mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

.field private mIsActionDown:Z

.field private mIsActionUp:Z

.field private mIsListScroll:Z

.field private mIsScrollPaused:Z

.field private mLMs:[J

.field private mLX:[I

.field private mLY:[I

.field private mListScrollActive:Z

.field private mLxIndex:I

.field private mMoveEvent:Landroid/view/MotionEvent;

.field private mMoveY:I

.field private mPressMs:J

.field private mPressX:I

.field private mPressY:I

.field private mRect:Landroid/graphics/Rect;

.field private mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mScrollDirection:I

.field private mStart:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 1
    .param p1, "rootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;)V
    .locals 3
    .param p1, "rootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;
    .param p2, "listener"    # Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    .prologue
    const/4 v0, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 31
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 32
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRect:Landroid/graphics/Rect;

    .line 35
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mStart:Landroid/graphics/PointF;

    .line 36
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mEnd:Landroid/graphics/PointF;

    .line 39
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveY:I

    .line 41
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mScrollDirection:I

    .line 44
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLX:[I

    .line 45
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLY:[I

    .line 46
    new-array v0, v2, [J

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLMs:[J

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mListScrollActive:Z

    .line 50
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsListScroll:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsScrollPaused:Z

    .line 55
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsActionUp:Z

    .line 56
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsActionDown:Z

    .line 74
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 75
    iput-object p2, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    .line 77
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$1;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$1;-><init>(Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    .prologue
    .line 11
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mListScrollActive:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mListScrollActive:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;)Lcom/sec/android/gallery3d/glcore/GlHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->updateMovement(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;Landroid/view/MotionEvent;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;
    .param p1, "x1"    # Landroid/view/MotionEvent;
    .param p2, "x2"    # Z

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->calcMovement(Landroid/view/MotionEvent;Z)Z

    move-result v0

    return v0
.end method

.method private calcMovement(Landroid/view/MotionEvent;Z)Z
    .locals 21
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "stop"    # Z

    .prologue
    .line 213
    const/16 v16, 0x0

    .line 215
    .local v16, "wasConsumed":Z
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v17

    move/from16 v0, v17

    float-to-int v12, v0

    .line 216
    .local v12, "nx":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v17

    move/from16 v0, v17

    float-to-int v13, v0

    .line 217
    .local v13, "ny":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v10

    .line 219
    .local v10, "ms":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLX:[I

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLxIndex:I

    move/from16 v18, v0

    aput v12, v17, v18

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLY:[I

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLxIndex:I

    move/from16 v18, v0

    aput v13, v17, v18

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLMs:[J

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLxIndex:I

    move/from16 v18, v0

    aput-wide v10, v17, v18

    .line 222
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLxIndex:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLxIndex:I

    .line 223
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLxIndex:I

    move/from16 v17, v0

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_0

    .line 224
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLxIndex:I

    .line 225
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mPressY:I

    move/from16 v17, v0

    sub-int v9, v13, v17

    .line 227
    .local v9, "movY":I
    if-eqz p2, :cond_4

    .line 228
    const/4 v8, 0x0

    .line 229
    .local v8, "fIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLX:[I

    move-object/from16 v17, v0

    aget v17, v17, v8

    sub-int v6, v12, v17

    .line 230
    .local v6, "dx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLY:[I

    move-object/from16 v17, v0

    aget v17, v17, v8

    sub-int v7, v13, v17

    .line 231
    .local v7, "dy":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLMs:[J

    move-object/from16 v17, v0

    aget-wide v18, v17, v8

    sub-long v4, v10, v18

    .line 232
    .local v4, "duration":J
    int-to-float v0, v6

    move/from16 v17, v0

    const/high16 v18, 0x44fa0000    # 2000.0f

    mul-float v17, v17, v18

    long-to-float v0, v4

    move/from16 v18, v0

    div-float v14, v17, v18

    .line 233
    .local v14, "velocityX":F
    int-to-float v0, v7

    move/from16 v17, v0

    const/high16 v18, 0x44fa0000    # 2000.0f

    mul-float v17, v17, v18

    long-to-float v0, v4

    move/from16 v18, v0

    div-float v15, v17, v18

    .line 234
    .local v15, "velocityY":F
    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v17

    const/high16 v18, 0x43fa0000    # 500.0f

    cmpg-float v17, v17, v18

    if-gez v17, :cond_1

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v17

    const/high16 v18, 0x43fa0000    # 500.0f

    cmpg-float v17, v17, v18

    if-gez v17, :cond_1

    .line 235
    const/4 v14, 0x0

    .line 236
    const/4 v15, 0x0

    .line 239
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mScrollDirection:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mScrollDirection:I

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsActionUp:Z

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 241
    :cond_2
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsActionUp:Z

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-interface {v0, v1, v9, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;->onRelease(IIII)Z

    move-result v17

    or-int v16, v16, v17

    .line 247
    .end local v4    # "duration":J
    .end local v6    # "dx":I
    .end local v7    # "dy":I
    .end local v8    # "fIndex":I
    .end local v14    # "velocityX":F
    .end local v15    # "velocityY":F
    :cond_3
    :goto_0
    return v16

    .line 245
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v0, v1, v9}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;->onMove(II)Z

    move-result v17

    or-int v16, v16, v17

    goto :goto_0
.end method

.method private checkPoint(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->getDirection(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mScrollDirection:I

    .line 128
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mScrollDirection:I

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 131
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getDirection(II)I
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 136
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v2

    .line 137
    .local v2, "point":Landroid/graphics/Point;
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x3f800000    # 1.0f

    mul-float v1, v5, v6

    .line 138
    .local v1, "height":F
    const/high16 v5, 0x42c80000    # 100.0f

    iget v6, v2, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    div-float/2addr v6, v1

    mul-float/2addr v5, v6

    float-to-int v4, v5

    .line 139
    .local v4, "verticalArea":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/ActionBar;->getHeight()I

    move-result v0

    .line 141
    .local v0, "actionbarHeight":I
    const/4 v3, 0x0

    .line 143
    .local v3, "scrollType":I
    if-le p2, v0, :cond_1

    add-int v5, v0, v4

    if-ge p2, v5, :cond_1

    .line 144
    const/4 v3, 0x1

    .line 148
    :cond_0
    :goto_0
    return v3

    .line 145
    :cond_1
    iget v5, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v5, v4

    if-le p2, v5, :cond_0

    .line 146
    const/4 v3, 0x2

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;
    .locals 2
    .param p0, "rootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 59
    const-class v1, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->sInstance:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->sInstance:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    .line 62
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->sInstance:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private onPenMoveScroll(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v5, 0xd4

    const/4 v6, 0x1

    .line 97
    const/4 v1, 0x0

    .line 99
    .local v1, "wasConsumed":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .line 100
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 102
    .local v3, "y":I
    invoke-direct {p0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->checkPoint(II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 103
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsActionUp:Z

    if-ne v4, v6, :cond_1

    .line 104
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mListScrollActive:Z

    if-nez v4, :cond_0

    .line 105
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeMessage(I)V

    .line 107
    :cond_0
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mListScrollActive:Z

    .line 109
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->processListScrollEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 123
    :goto_0
    return v1

    .line 111
    :cond_2
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsListScroll:Z

    if-eqz v4, :cond_4

    .line 112
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsScrollPaused:Z

    .line 113
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 114
    .local v0, "cancel":Landroid/view/MotionEvent;
    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->setAction(I)V

    .line 115
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->processListScrollEvent(Landroid/view/MotionEvent;)Z

    .line 116
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 121
    .end local v0    # "cancel":Landroid/view/MotionEvent;
    :cond_3
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 117
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-ne v4, v5, :cond_3

    .line 118
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveEvent:Landroid/view/MotionEvent;

    if-eqz v4, :cond_3

    .line 119
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveEvent:Landroid/view/MotionEvent;

    invoke-direct {p0, v4, v6}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->calcMovement(Landroid/view/MotionEvent;Z)Z

    goto :goto_1
.end method

.method private processListScrollEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v4, 0xd5

    const/16 v3, 0xd3

    const/4 v13, 0x1

    const/4 v2, 0x0

    .line 152
    const/4 v10, 0x0

    .line 154
    .local v10, "wasConsumed":Z
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsListScroll:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 155
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->setAction(I)V

    .line 158
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 159
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v11, v1

    .line 160
    .local v11, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v12, v1

    .line 161
    .local v12, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    .line 163
    .local v8, "ms":J
    if-ne v0, v3, :cond_4

    .line 164
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsScrollPaused:Z

    if-nez v1, :cond_1

    .line 165
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mPressX:I

    .line 166
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mPressY:I

    .line 169
    :cond_1
    iput-wide v8, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mPressMs:J

    .line 170
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLX:[I

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mPressX:I

    aput v3, v1, v2

    .line 171
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLY:[I

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mPressY:I

    aput v3, v1, v2

    .line 172
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLMs:[J

    iget-wide v4, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mPressMs:J

    aput-wide v4, v1, v2

    .line 173
    iput v13, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mLxIndex:I

    .line 175
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsScrollPaused:Z

    if-nez v1, :cond_2

    .line 176
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mPressY:I

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveY:I

    .line 178
    :cond_2
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsScrollPaused:Z

    .line 180
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mPressX:I

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mPressY:I

    invoke-interface {v1, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;->onPress(II)Z

    move-result v10

    .line 181
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const-wide/16 v6, 0x12c

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessageDelayed(IIIIJ)V

    .line 182
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveEvent:Landroid/view/MotionEvent;

    .line 183
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mListScrollActive:Z

    .line 184
    iput-boolean v13, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsListScroll:Z

    .line 202
    :cond_3
    :goto_0
    return v10

    .line 185
    :cond_4
    if-ne v0, v4, :cond_6

    .line 186
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsListScroll:Z

    if-eqz v1, :cond_5

    .line 187
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mScrollDirection:I

    if-nez v1, :cond_5

    .line 188
    invoke-direct {p0, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->getDirection(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mScrollDirection:I

    .line 190
    :cond_5
    const/4 v10, 0x1

    goto :goto_0

    .line 191
    :cond_6
    const/16 v1, 0xd4

    if-ne v0, v1, :cond_3

    .line 192
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mListScrollActive:Z

    if-nez v1, :cond_7

    .line 193
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeMessage(I)V

    .line 197
    :goto_1
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mScrollDirection:I

    .line 198
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mListScrollActive:Z

    .line 199
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsListScroll:Z

    .line 200
    const/4 v10, 0x1

    goto :goto_0

    .line 195
    :cond_7
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveEvent:Landroid/view/MotionEvent;

    invoke-direct {p0, v1, v13}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->calcMovement(Landroid/view/MotionEvent;Z)Z

    goto :goto_1
.end method

.method public static declared-synchronized releaseInstance()V
    .locals 2

    .prologue
    .line 66
    const-class v0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput-object v1, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->sInstance:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    monitor-exit v0

    return-void

    .line 66
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private updateMovement(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/high16 v2, 0x41a00000    # 20.0f

    .line 251
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mScrollDirection:I

    if-ne v0, v3, :cond_2

    .line 252
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;->isReachedBoundary()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 253
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mStart:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 255
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveY:I

    add-int/lit8 v0, v0, 0x14

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveY:I

    .line 256
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mPressX:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveY:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 264
    :cond_1
    :goto_0
    return-object p1

    .line 257
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mScrollDirection:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 258
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;->isReachedBoundary()I

    move-result v0

    if-eq v0, v3, :cond_3

    .line 259
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mStart:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 261
    :cond_3
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveY:I

    add-int/lit8 v0, v0, -0x14

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveY:I

    .line 262
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mPressX:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mMoveY:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    goto :goto_0
.end method


# virtual methods
.method public getFirstPoint()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mStart:Landroid/graphics/PointF;

    return-object v0
.end method

.method public getPenSelectRect()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mStart:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mEnd:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mStart:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 330
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mEnd:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 335
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mStart:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mEnd:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 336
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mStart:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 337
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mEnd:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 342
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRect:Landroid/graphics/Rect;

    return-object v0

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mEnd:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 333
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mStart:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mEnd:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 340
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mStart:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_1
.end method

.method public getSecondPoint()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mEnd:Landroid/graphics/PointF;

    return-object v0
.end method

.method public onTouch(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 276
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandled:Z

    .line 277
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 278
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    .line 279
    .local v2, "ms":J
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v1, v5

    .line 280
    .local v1, "nx":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v4, v5

    .line 282
    .local v4, "ny":I
    const/16 v5, 0xd3

    if-ne v0, v5, :cond_1

    .line 283
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsActionDown:Z

    .line 284
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->onPenMoveScroll(Landroid/view/MotionEvent;)Z

    .line 285
    invoke-virtual {p0, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->setFirstPoint(II)V

    .line 286
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandled:Z

    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    invoke-interface {v6, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;->onPenEnter(II)Z

    move-result v6

    or-int/2addr v5, v6

    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandled:Z

    .line 306
    :cond_0
    :goto_0
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandled:Z

    :goto_1
    return v5

    .line 287
    :cond_1
    const/16 v5, 0xd4

    if-ne v0, v5, :cond_2

    .line 288
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsActionUp:Z

    .line 289
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsActionDown:Z

    .line 290
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->onPenMoveScroll(Landroid/view/MotionEvent;)Z

    .line 291
    invoke-virtual {p0, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->setSecondPoint(II)V

    .line 292
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandled:Z

    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    invoke-interface {v6, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;->onPenSelect(II)Z

    move-result v6

    or-int/2addr v5, v6

    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandled:Z

    goto :goto_0

    .line 293
    :cond_2
    const/16 v5, 0xd5

    if-ne v0, v5, :cond_3

    .line 294
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->onPenMoveScroll(Landroid/view/MotionEvent;)Z

    .line 295
    invoke-virtual {p0, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->setSecondPoint(II)V

    .line 296
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandled:Z

    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    iget-boolean v7, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mListScrollActive:Z

    invoke-interface {v6, v1, v4, v7}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;->onPenMove(IIZ)Z

    move-result v6

    or-int/2addr v5, v6

    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandled:Z

    goto :goto_0

    .line 297
    :cond_3
    const/4 v5, 0x3

    if-ne v0, v5, :cond_0

    .line 298
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsActionUp:Z

    .line 299
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsActionDown:Z

    if-nez v5, :cond_4

    .line 300
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandled:Z

    goto :goto_1

    .line 301
    :cond_4
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mIsActionDown:Z

    .line 302
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->onPenMoveScroll(Landroid/view/MotionEvent;)Z

    .line 303
    invoke-virtual {p0, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->setSecondPoint(II)V

    .line 304
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandled:Z

    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    invoke-interface {v6, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;->onPenSelect(II)Z

    move-result v6

    or-int/2addr v5, v6

    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mHandled:Z

    goto :goto_0
.end method

.method public onTouch(Landroid/view/MotionEvent;Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "listener"    # Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    .prologue
    .line 268
    iput-object p2, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    .line 269
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->onTouch(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setFirstPoint(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mStart:Landroid/graphics/PointF;

    int-to-float v1, p1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 319
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mStart:Landroid/graphics/PointF;

    int-to-float v1, p2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 320
    return-void
.end method

.method public setSecondPoint(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mEnd:Landroid/graphics/PointF;

    int-to-float v1, p1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 324
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->mEnd:Landroid/graphics/PointF;

    int-to-float v1, p2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 325
    return-void
.end method
