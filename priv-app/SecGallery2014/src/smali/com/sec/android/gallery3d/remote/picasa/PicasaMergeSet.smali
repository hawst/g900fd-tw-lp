.class public Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "PicasaMergeSet.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PicasaMergeSet"


# instance fields
.field private final mName:Ljava/lang/String;

.field private mRootSet:Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 41
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->getBucketId()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;

    .line 42
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0096

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->mName:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public static find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 47
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    return-object v0
.end method


# virtual methods
.method public getBucketId()I
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    if-nez v0, :cond_0

    .line 101
    const/4 v0, -0x1

    .line 102
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItemCount()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;

    return-object v0
.end method

.method public getSubMediaSetCount()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 57
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAlbumGrouping:Z

    if-eqz v1, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->getMediaItemCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 69
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getSupportedOperations()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalMediaItems(I)Landroid/database/Cursor;
    .locals 9
    .param p1, "filterType"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 106
    const-string v6, "content_type LIKE ?"

    .line 107
    .local v6, "TYPE_WHERE_CLAUSE":Ljava/lang/String;
    const/4 v3, 0x0

    .line 108
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 109
    .local v4, "args":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 111
    .local v7, "cursor":Landroid/database/Cursor;
    packed-switch p1, :pswitch_data_0

    .line 127
    :pswitch_0
    const/4 v3, 0x0

    .line 128
    const/4 v4, 0x0

    .line 134
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "_id"

    aput-object v8, v2, v5

    const/4 v5, 0x1

    const-string v8, "latitude"

    aput-object v8, v2, v5

    const/4 v5, 0x2

    const-string v8, "longitude"

    aput-object v8, v2, v5

    const/4 v5, 0x3

    const-string v8, "date_taken"

    aput-object v8, v2, v5

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 144
    :goto_1
    return-object v7

    .line 113
    :pswitch_1
    const-string v3, "content_type LIKE ?"

    .line 114
    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "args":[Ljava/lang/String;
    const-string v0, "image/%"

    aput-object v0, v4, v1

    .line 117
    .restart local v4    # "args":[Ljava/lang/String;
    goto :goto_0

    .line 120
    :pswitch_2
    const-string v3, "content_type LIKE ?"

    .line 121
    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "args":[Ljava/lang/String;
    const-string/jumbo v0, "video/%"

    aput-object v0, v4, v1

    .line 124
    .restart local v4    # "args":[Ljava/lang/String;
    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    goto :goto_1

    .line 111
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public reload()J
    .locals 4

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->reload()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->mDataVersion:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 75
    invoke-static {}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->mDataVersion:J

    .line 77
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->mDataVersion:J

    return-wide v0
.end method
