.class public Lcom/sec/android/gallery3d/data/GroupClustering;
.super Lcom/sec/android/gallery3d/data/Clustering;
.source "GroupClustering.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/GroupClustering$Value;,
        Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FaceClustering"

.field private static final UNGROUPED_ID:I


# instance fields
.field private mClusters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mGroupIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mKeys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNameIDs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPersonGroups:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<[",
            "Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;",
            ">;"
        }
    .end annotation
.end field

.field private mPersonNames:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/Clustering;-><init>()V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mNames:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mNameIDs:Ljava/util/ArrayList;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mKeys:Ljava/util/ArrayList;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mGroupIds:Ljava/util/ArrayList;

    .line 34
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mPersonNames:Landroid/util/SparseArray;

    .line 35
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mPersonGroups:Landroid/util/SparseArray;

    .line 37
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method private getPersonGroups(I)[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    .locals 19
    .param p1, "personId"    # I

    .prologue
    .line 253
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isContactDBAvailable(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 254
    const/4 v11, 0x0

    .line 325
    :cond_0
    :goto_0
    return-object v11

    .line 256
    :cond_1
    const/4 v11, 0x0

    .line 257
    .local v11, "groups":[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mPersonGroups:Landroid/util/SparseArray;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "groups":[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    check-cast v11, [Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;

    .line 258
    .restart local v11    # "groups":[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    if-nez v11, :cond_0

    .line 261
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 262
    .local v2, "cr":Landroid/content/ContentResolver;
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/gallery3d/data/GroupClustering;->getPersonName(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v16

    .line 263
    .local v16, "personName":Ljava/lang/String;
    const/4 v15, 0x0

    .line 264
    .local v15, "lookupkey":Ljava/lang/String;
    const/4 v8, 0x0

    .line 265
    .local v8, "c":Landroid/database/Cursor;
    const/4 v14, 0x0

    .line 266
    .local v14, "ids":[I
    const/4 v9, 0x0

    .line 268
    .local v9, "count":I
    if-eqz v16, :cond_3

    const-string v3, "/"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 269
    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 270
    .local v17, "values":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v15, v17, v3

    .line 272
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "data1"

    aput-object v6, v4, v5

    const-string v5, "mimetype=? and lookup=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string/jumbo v18, "vnd.android.cursor.item/group_membership"

    aput-object v18, v6, v7

    const/4 v7, 0x1

    aput-object v15, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 274
    if-eqz v8, :cond_2

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 275
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 276
    new-array v14, v9, [I

    .line 277
    const/4 v12, 0x0

    .local v12, "i":I
    move v13, v12

    .line 279
    .end local v12    # "i":I
    .local v13, "i":I
    :goto_1
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "i":I
    .restart local v12    # "i":I
    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aput v3, v14, v13

    .line 280
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_c

    .line 283
    .end local v12    # "i":I
    :cond_2
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 287
    .end local v17    # "values":[Ljava/lang/String;
    :cond_3
    const/4 v10, 0x0

    .line 288
    .local v10, "group":Ljava/lang/String;
    if-lez v9, :cond_a

    .line 289
    new-array v11, v9, [Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;

    .line 290
    const/4 v12, 0x0

    .restart local v12    # "i":I
    :goto_2
    if-ge v12, v9, :cond_b

    .line 292
    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "title"

    aput-object v6, v4, v5

    const-string v5, "_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aget v18, v14, v12

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 294
    if-eqz v8, :cond_4

    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 295
    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v10

    .line 298
    :cond_4
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 301
    if-eqz v10, :cond_9

    .line 302
    const-string v3, "Family"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 303
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mContext:Landroid/content/Context;

    const v4, 0x7f0e019d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 311
    :cond_5
    :goto_3
    new-instance v3, Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;

    aget v4, v14, v12

    const/4 v5, -0x1

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v10, v5}, Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;-><init>(Lcom/sec/android/gallery3d/data/GroupClustering;ILjava/lang/String;I)V

    aput-object v3, v11, v12

    .line 290
    :goto_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 283
    .end local v10    # "group":Ljava/lang/String;
    .end local v12    # "i":I
    .restart local v17    # "values":[Ljava/lang/String;
    :catchall_0
    move-exception v3

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3

    .line 298
    .end local v17    # "values":[Ljava/lang/String;
    .restart local v10    # "group":Ljava/lang/String;
    .restart local v12    # "i":I
    :catchall_1
    move-exception v3

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3

    .line 304
    :cond_6
    const-string v3, "Friends"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 305
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mContext:Landroid/content/Context;

    const v4, 0x7f0e019e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_3

    .line 306
    :cond_7
    const-string v3, "Coworkers"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 307
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mContext:Landroid/content/Context;

    const v4, 0x7f0e019f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_3

    .line 308
    :cond_8
    const-string v3, "My Contacts"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 309
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mContext:Landroid/content/Context;

    const v4, 0x7f0e01a0

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_3

    .line 314
    :cond_9
    const-string v3, "FaceClustering"

    const-string v4, "group is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0068

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 316
    new-instance v3, Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;

    const/4 v4, 0x0

    const v5, 0x7f0e0068

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v10, v5}, Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;-><init>(Lcom/sec/android/gallery3d/data/GroupClustering;ILjava/lang/String;I)V

    aput-object v3, v11, v12

    goto :goto_4

    .line 320
    .end local v12    # "i":I
    :cond_a
    const/4 v3, 0x1

    new-array v11, v3, [Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;

    .line 321
    const/4 v3, 0x0

    new-instance v4, Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mContext:Landroid/content/Context;

    const v7, 0x7f0e0068

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0e0068

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5, v6, v7}, Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;-><init>(Lcom/sec/android/gallery3d/data/GroupClustering;ILjava/lang/String;I)V

    aput-object v4, v11, v3

    .line 324
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mPersonGroups:Landroid/util/SparseArray;

    move/from16 v0, p1

    invoke-virtual {v3, v0, v11}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_0

    .end local v10    # "group":Ljava/lang/String;
    .restart local v12    # "i":I
    .restart local v17    # "values":[Ljava/lang/String;
    :cond_c
    move v13, v12

    .end local v12    # "i":I
    .restart local v13    # "i":I
    goto/16 :goto_1
.end method


# virtual methods
.method public getCluster(I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getClusterKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mKeys:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getClusterName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mNames:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getClusterNameID(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mNameIDs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getGroupId(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getNumberOfClusters()I
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getPersonGroups()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<[",
            "Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mPersonGroups:Landroid/util/SparseArray;

    return-object v0
.end method

.method getPersonName(Landroid/content/ContentResolver;I)Ljava/lang/String;
    .locals 10
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "personId"    # I

    .prologue
    const/4 v2, 0x1

    .line 226
    const/4 v8, 0x0

    .local v8, "personName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 227
    .local v7, "joinedName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mPersonNames:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "personName":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 228
    .restart local v8    # "personName":Ljava/lang/String;
    if-eqz v8, :cond_0

    move-object v9, v8

    .line 249
    .end local v8    # "personName":Ljava/lang/String;
    .local v9, "personName":Ljava/lang/String;
    :goto_0
    return-object v9

    .line 230
    .end local v9    # "personName":Ljava/lang/String;
    .restart local v8    # "personName":Ljava/lang/String;
    :cond_0
    if-le p2, v2, :cond_2

    .line 231
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    int-to-long v2, p2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 232
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 234
    .local v6, "c":Landroid/database/Cursor;
    const/4 v0, 0x2

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "name"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v3, "user_data"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 237
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 239
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 240
    invoke-static {v8, v7}, Lcom/sec/samsung/gallery/access/face/PersonList;->getPersonName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 241
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering;->mPersonNames:Landroid/util/SparseArray;

    invoke-virtual {v0, p2, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    :cond_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    :goto_1
    move-object v9, v8

    .line 249
    .end local v8    # "personName":Ljava/lang/String;
    .restart local v9    # "personName":Ljava/lang/String;
    goto :goto_0

    .line 244
    .end local v9    # "personName":Ljava/lang/String;
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v6    # "c":Landroid/database/Cursor;
    .restart local v8    # "personName":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 247
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_2
    const-string/jumbo v8, "unkown"

    goto :goto_1
.end method

.method public run(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 41
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 56
    const/16 v27, 0x0

    .line 57
    .local v27, "isByGroup":Z
    const/16 v26, 0x0

    .line 59
    .local v26, "imgIDs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mClusters:Ljava/util/ArrayList;

    .line 61
    move-object/from16 v0, p1

    instance-of v5, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-nez v5, :cond_0

    move-object/from16 v0, p1

    instance-of v5, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v5, :cond_2

    .line 62
    :cond_0
    const/16 v27, 0x1

    .line 63
    new-instance v26, Ljava/util/HashMap;

    .end local v26    # "imgIDs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct/range {v26 .. v26}, Ljava/util/HashMap;-><init>()V

    .line 64
    .restart local v26    # "imgIDs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v25, 0x0

    .line 65
    .local v25, "imgID":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v29

    .line 67
    .local v29, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 68
    const/4 v5, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/face"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 69
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 70
    .local v28, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v23

    .line 71
    .local v23, "idNames":[Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v25, v23, v5

    .line 72
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 76
    .end local v22    # "i$":Ljava/util/Iterator;
    .end local v23    # "idNames":[Ljava/lang/String;
    .end local v28    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .restart local v22    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 77
    .restart local v28    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v25

    .line 78
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 84
    .end local v22    # "i$":Ljava/util/Iterator;
    .end local v25    # "imgID":Ljava/lang/String;
    .end local v28    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v29    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_2
    new-instance v33, Ljava/util/TreeMap;

    invoke-direct/range {v33 .. v33}, Ljava/util/TreeMap;-><init>()V

    .line 85
    .local v33, "namedMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/GroupClustering$Value;>;"
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .local v13, "buf":Ljava/lang/StringBuilder;
    const/16 v18, 0x0

    .local v18, "faceId":I
    const/16 v36, 0x0

    .local v36, "personId":I
    const/16 v37, 0x0

    .local v37, "recommendedId":I
    const/16 v20, 0x0

    .local v20, "groupId":I
    const/16 v17, 0x0

    .local v17, "faceData":I
    const/4 v11, 0x0

    .local v11, "autoGroup":I
    const/16 v24, 0x0

    .line 88
    .local v24, "imageId":I
    const/16 v31, 0x0

    .local v31, "left":I
    const/16 v40, 0x0

    .local v40, "top":I
    const/16 v38, 0x0

    .local v38, "right":I
    const/4 v12, 0x0

    .line 89
    .local v12, "bottom":I
    const/16 v35, 0x0

    .line 90
    .local v35, "path":Lcom/sec/android/gallery3d/data/Path;
    const/16 v34, 0x0

    .line 91
    .local v34, "p":Ljava/lang/String;
    const/16 v30, 0x0

    .line 93
    .local v30, "key":Ljava/lang/String;
    const/16 v21, 0x0

    .line 94
    .local v21, "groups":[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 95
    .local v3, "resolver":Landroid/content/ContentResolver;
    const/16 v16, 0x0

    .line 96
    .local v16, "faceCursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getCloudIds(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v14

    .line 99
    .local v14, "cloudIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :try_start_0
    sget-object v4, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    const/16 v5, 0xb

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "_id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "person_id"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "recommended_id"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "group_id"

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "face_data"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "auto_group"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "pos_left"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    const-string v7, "pos_top"

    aput-object v7, v5, v6

    const/16 v6, 0x8

    const-string v7, "pos_right"

    aput-object v7, v5, v6

    const/16 v6, 0x9

    const-string v7, "pos_bottom"

    aput-object v7, v5, v6

    const/16 v6, 0xa

    const-string v7, "image_id"

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "recommended_id desc,group_id desc"

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 108
    if-eqz v16, :cond_5

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 110
    :cond_3
    const/4 v5, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 111
    const/4 v5, 0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v36

    .line 112
    const/4 v5, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v37

    .line 113
    const/4 v5, 0x3

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 114
    const/4 v5, 0x4

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 115
    const/4 v5, 0x5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 116
    const/4 v5, 0x6

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v31

    .line 117
    const/4 v5, 0x7

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v40

    .line 118
    const/16 v5, 0x8

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v38

    .line 119
    const/16 v5, 0x9

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 120
    const/16 v5, 0xa

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    .line 123
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 169
    :cond_4
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_3

    .line 173
    :cond_5
    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 176
    invoke-virtual/range {v33 .. v33}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .restart local v22    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/Map$Entry;

    .line 177
    .local v15, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/gallery3d/data/GroupClustering$Value;>;"
    invoke-interface {v15}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v30

    .end local v30    # "key":Ljava/lang/String;
    check-cast v30, Ljava/lang/String;

    .line 178
    .restart local v30    # "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mKeys:Ljava/util/ArrayList;

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mNames:Ljava/util/ArrayList;

    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/GroupClustering$Value;

    iget-object v5, v5, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mName:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mNameIDs:Ljava/util/ArrayList;

    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/GroupClustering$Value;

    iget v5, v5, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mNameId:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mClusters:Ljava/util/ArrayList;

    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/GroupClustering$Value;

    iget-object v5, v5, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/GroupClustering;->mGroupIds:Ljava/util/ArrayList;

    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/GroupClustering$Value;

    iget v5, v5, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mGroupId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 127
    .end local v15    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/gallery3d/data/GroupClustering$Value;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    :cond_6
    if-eqz v27, :cond_7

    .line 128
    if-eqz v26, :cond_7

    :try_start_1
    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 131
    :cond_7
    const/4 v5, 0x0

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    invoke-virtual {v13, v5, v6}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 132
    const-string v5, "/face/"

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v40

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v38

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v37

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v36

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "group"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 137
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    .line 138
    invoke-static/range {v34 .. v34}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v35

    .line 139
    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/data/GroupClustering;->getPersonGroups(I)[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;

    move-result-object v21

    .line 140
    if-eqz v21, :cond_4

    .line 141
    move-object/from16 v10, v21

    .local v10, "arr$":[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    array-length v0, v10

    move/from16 v32, v0

    .local v32, "len$":I
    const/16 v22, 0x0

    .local v22, "i$":I
    :goto_3
    move/from16 v0, v22

    move/from16 v1, v32

    if-ge v0, v1, :cond_4

    aget-object v19, v10, v22

    .line 142
    .local v19, "group":Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    move-object/from16 v0, v19

    iget v5, v0, Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;->mId:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    .line 143
    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/GroupClustering$Value;

    .line 144
    .local v4, "value":Lcom/sec/android/gallery3d/data/GroupClustering$Value;
    if-nez v4, :cond_8

    .line 145
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 146
    .local v8, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    new-instance v4, Lcom/sec/android/gallery3d/data/GroupClustering$Value;

    .end local v4    # "value":Lcom/sec/android/gallery3d/data/GroupClustering$Value;
    move-object/from16 v0, v19

    iget v6, v0, Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;->mId:I

    move-object/from16 v0, v19

    iget-object v7, v0, Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;->mName:Ljava/lang/String;

    move-object/from16 v0, v19

    iget v9, v0, Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;->mNameId:I

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/gallery3d/data/GroupClustering$Value;-><init>(Lcom/sec/android/gallery3d/data/GroupClustering;ILjava/lang/String;Ljava/util/ArrayList;I)V

    .line 147
    .restart local v4    # "value":Lcom/sec/android/gallery3d/data/GroupClustering$Value;
    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    .end local v8    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_8
    const/16 v39, 0x0

    .line 151
    .local v39, "tmp":Lcom/sec/android/gallery3d/data/Path;
    iget-object v5, v4, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mRecord:Ljava/util/HashMap;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    .end local v39    # "tmp":Lcom/sec/android/gallery3d/data/Path;
    check-cast v39, Lcom/sec/android/gallery3d/data/Path;

    .restart local v39    # "tmp":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v39, :cond_a

    .line 152
    move/from16 v0, v37

    move/from16 v1, v36

    if-ne v0, v1, :cond_9

    const/4 v5, 0x1

    move/from16 v0, v37

    if-le v0, v5, :cond_9

    .line 141
    :goto_4
    add-int/lit8 v22, v22, 0x1

    goto :goto_3

    .line 155
    :cond_9
    iget-object v5, v4, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mRecord:Ljava/util/HashMap;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    iget-object v5, v4, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mList:Ljava/util/ArrayList;

    move-object/from16 v0, v39

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 160
    :cond_a
    iget-object v5, v4, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mRecord:Ljava/util/HashMap;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v35

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    move/from16 v0, v37

    move/from16 v1, v36

    if-ne v0, v1, :cond_b

    const/4 v5, 0x1

    move/from16 v0, v37

    if-le v0, v5, :cond_b

    .line 164
    iget-object v5, v4, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mList:Ljava/util/ArrayList;

    const/4 v6, 0x0

    move-object/from16 v0, v35

    invoke-virtual {v5, v6, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 173
    .end local v4    # "value":Lcom/sec/android/gallery3d/data/GroupClustering$Value;
    .end local v10    # "arr$":[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    .end local v19    # "group":Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    .end local v22    # "i$":I
    .end local v32    # "len$":I
    .end local v39    # "tmp":Lcom/sec/android/gallery3d/data/Path;
    :catchall_0
    move-exception v5

    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v5

    .line 166
    .restart local v4    # "value":Lcom/sec/android/gallery3d/data/GroupClustering$Value;
    .restart local v10    # "arr$":[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    .restart local v19    # "group":Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    .restart local v22    # "i$":I
    .restart local v32    # "len$":I
    .restart local v39    # "tmp":Lcom/sec/android/gallery3d/data/Path;
    :cond_b
    :try_start_2
    iget-object v5, v4, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mList:Ljava/util/ArrayList;

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 184
    .end local v4    # "value":Lcom/sec/android/gallery3d/data/GroupClustering$Value;
    .end local v10    # "arr$":[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    .end local v19    # "group":Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    .end local v32    # "len$":I
    .end local v39    # "tmp":Lcom/sec/android/gallery3d/data/Path;
    .local v22, "i$":Ljava/util/Iterator;
    :cond_c
    return-void
.end method
