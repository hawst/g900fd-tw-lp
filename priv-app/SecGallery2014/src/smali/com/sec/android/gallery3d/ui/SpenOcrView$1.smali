.class Lcom/sec/android/gallery3d/ui/SpenOcrView$1;
.super Landroid/os/Handler;
.source "SpenOcrView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/SpenOcrView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/SpenOcrView;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, -0x1

    .line 75
    iget v7, p1, Landroid/os/Message;->what:I

    .line 77
    .local v7, "what":I
    packed-switch v7, :pswitch_data_0

    .line 118
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 119
    return-void

    .line 80
    :pswitch_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # getter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$000(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Landroid/content/Context;

    move-result-object v8

    check-cast v8, Landroid/app/Activity;

    new-instance v9, Lcom/sec/android/gallery3d/ui/SpenOcrView$1$1;

    invoke-direct {v9, p0}, Lcom/sec/android/gallery3d/ui/SpenOcrView$1$1;-><init>(Lcom/sec/android/gallery3d/ui/SpenOcrView$1;)V

    invoke-virtual {v8, v9}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 89
    :pswitch_1
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v8, :cond_1

    .line 91
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/content/Intent;

    .line 92
    .local v4, "requestIntent":Landroid/content/Intent;
    const-string/jumbo v8, "type"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 93
    .local v6, "type":I
    const-string/jumbo v8, "text"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 94
    .local v5, "text":Ljava/lang/String;
    const-string v8, "pos"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/graphics/Point;

    .line 96
    .local v2, "pos":Landroid/graphics/Point;
    if-eq v6, v9, :cond_2

    .line 97
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # invokes: Lcom/sec/android/gallery3d/ui/SpenOcrView;->prepareShowOCR(ILjava/lang/String;Landroid/graphics/Point;)V
    invoke-static {v8, v6, v5, v2}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$200(Lcom/sec/android/gallery3d/ui/SpenOcrView;ILjava/lang/String;Landroid/graphics/Point;)V

    .line 98
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # setter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mPoint:Landroid/graphics/Point;
    invoke-static {v8, v2}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$302(Lcom/sec/android/gallery3d/ui/SpenOcrView;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 103
    .end local v2    # "pos":Landroid/graphics/Point;
    .end local v4    # "requestIntent":Landroid/content/Intent;
    .end local v5    # "text":Ljava/lang/String;
    .end local v6    # "type":I
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    const/4 v9, 0x0

    # invokes: Lcom/sec/android/gallery3d/ui/SpenOcrView;->setState(I)V
    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$400(Lcom/sec/android/gallery3d/ui/SpenOcrView;I)V

    .line 104
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->invalidate()V

    goto :goto_0

    .line 100
    .restart local v2    # "pos":Landroid/graphics/Point;
    .restart local v4    # "requestIntent":Landroid/content/Intent;
    .restart local v5    # "text":Ljava/lang/String;
    .restart local v6    # "type":I
    :cond_2
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mPoint:Landroid/graphics/Point;
    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$302(Lcom/sec/android/gallery3d/ui/SpenOcrView;Landroid/graphics/Point;)Landroid/graphics/Point;

    goto :goto_1

    .line 107
    .end local v2    # "pos":Landroid/graphics/Point;
    .end local v4    # "requestIntent":Landroid/content/Intent;
    .end local v5    # "text":Ljava/lang/String;
    .end local v6    # "type":I
    :pswitch_2
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # getter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRPopup:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$500(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 108
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Point;

    .line 109
    .local v1, "point":Landroid/graphics/Point;
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # getter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRPopup:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$500(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # getter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mImageBounds:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$600(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Landroid/graphics/Rect;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Rect;->left:I

    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # getter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mImageBounds:Landroid/graphics/Rect;
    invoke-static {v10}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$600(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Landroid/graphics/Rect;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Rect;->right:I

    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # getter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mImageBounds:Landroid/graphics/Rect;
    invoke-static {v11}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$600(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Landroid/graphics/Rect;

    move-result-object v11

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v8, v9, v10, v11}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->setPopupBounds(III)V

    .line 110
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # getter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mMediaWidth:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$700(Lcom/sec/android/gallery3d/ui/SpenOcrView;)I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # getter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mImageBounds:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$600(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x3f800000    # 1.0f

    mul-float/2addr v9, v10

    div-float v3, v8, v9

    .line 111
    .local v3, "ratio":F
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    iget-object v8, v8, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    iget-object v9, v9, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    div-float/2addr v8, v3

    float-to-int v0, v8

    .line 112
    .local v0, "height":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # getter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRPopup:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$500(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    move-result-object v8

    iget v9, v1, Landroid/graphics/Point;->x:I

    iget v10, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v8, v9, v10, v0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->showAtPosition(III)V

    goto/16 :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
