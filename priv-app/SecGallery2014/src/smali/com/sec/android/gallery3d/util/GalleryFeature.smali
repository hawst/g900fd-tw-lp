.class public Lcom/sec/android/gallery3d/util/GalleryFeature;
.super Ljava/lang/Object;
.source "GalleryFeature.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;
    }
.end annotation


# static fields
.field private static final SLINK_API_SUPPORTED_VERSION:I = 0x3e8

.field private static final SLINK_SDK_PACKAGE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink"

.field private static final TAG:Ljava/lang/String;

.field public static mAddAutoRotationIcon:Z

.field public static mBurstShotGrouping:Z

.field public static mChangePicasaSyncTypeToThumb:Z

.field public static mCheckVersionOfContextProvider:Z

.field public static final mCountryCode:Ljava/lang/String;

.field public static mDefaultRotationPhoneType:Z

.field public static mDeviceType:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

.field public static mDisableEditShowAsAction:Z

.field public static mDisableHwAccelerationforImgaeNote:Z

.field public static mDisableMenuHide:Z

.field public static mDisableMenuSeaAsContact:Z

.field public static mDisableMenuShareViaMsg:Z

.field public static mDisablePinchZoomWhenPointersOutOfThumbnail:Z

.field public static mDisableShowMap:Z

.field public static mDisableStudioShowAsAction:Z

.field public static mDisableUseResizeLargeThumbnail:Z

.field public static mDisableUseXivLargeThumbnail:Z

.field public static mDisplaySelectAllButtonInLimitationPickMode:Z

.field public static mFaceTagDefaultValue:Z

.field public static mHasOCR:Z

.field public static mHorizontalScroll:Z

.field public static mIsChn:Z

.field public static mIsCropInTB:Z

.field public static mIsDecorAlwaysVisible:Z

.field public static mIsGolfIconSupported:Z

.field public static mIsGuestMode:Z

.field private static mIsInited:Z

.field public static mIsJPN:Z

.field public static mIsLDU:Z

.field public static mIsLognlifemodeEnabled:Z

.field public static mIsMultiSIM:Z

.field public static mIsNoteModel:Z

.field public static mIsPortraitTablet:Z

.field public static mIsSPCEnabled:Z

.field public static mIsSequenceIconSupported:Z

.field public static mIsSupportSoundAndShot:Z

.field public static mIsTwHk:Z

.field public static mIsVZW:Z

.field public static mIsWifiOnlyModel:Z

.field public static mMaxCountForSelectionLoadingTask:I

.field public static mMmsEnabled:Z

.field public static mNoUseBackIconInSelection:Z

.field public static mNoUseCopytoClipboardInDetailview:Z

.field public static mNoUseMovetoAlbumInTimeview:Z

.field public static mNoUseSortbyPhotoView:Z

.field public static final mProductName:Ljava/lang/String;

.field public static final mSalesCode:Ljava/lang/String;

.field public static mSupportKnoxMove:Z

.field public static final mSystemDeviceType:Ljava/lang/String;

.field public static mSystemUiVisibility:I

.field public static mTranslateGlTextCanvas:Z

.field public static mUse3DPanorama:Z

.field public static mUse3DTour:Z

.field public static mUse3DViewMode:Z

.field public static mUseATT_DateTag:Z

.field public static mUseATT_DefaultWidget:Z

.field public static mUseATT_PhotoEditTilteName:Z

.field public static mUseAccountSettings:Z

.field public static mUseAccuWeather:Z

.field public static mUseAdaptDisplayDialog:Z

.field public static mUseAddTagAsMoreinfo:Z

.field public static mUseAddTagSelectionDropDown:Z

.field public static mUseAdvancedSoundSceneShare:Z

.field public static mUseAirBrowse:Z

.field public static mUseAirButton:Z

.field public static mUseAirJump:Z

.field public static mUseAirViewActionBar:Z

.field public static mUseAlbumGrouping:Z

.field public static mUseAllviewBurstshot:Z

.field public static mUseAsyncRender:Z

.field public static mUseAutoHide:Z

.field public static mUseAutoScrollTimeView:Z

.field public static mUseBaidu:Z

.field public static mUseBaiduCloudDragDrop:Z

.field public static mUseBaiduPositioning:Z

.field public static mUseBurstShotSettingIcon:Z

.field public static mUseCGGMultiSIM:Z

.field public static mUseCameraButtonAtDetailView:Z

.field public static mUseCameraShotAndMore:Z

.field public static mUseCameraSwitchBoost:Z

.field public static mUseCardEnable:Z

.field public static mUseCategoryViewDefault:Z

.field public static mUseChangeThumbnail:Z

.field public static mUseChnCharacter:Z

.field public static mUseChnUsageAlertPopup:Z

.field public static mUseChooserActivity:Z

.field public static mUseCircleFace:Z

.field public static mUseCloud:Z

.field public static mUseCmcc:Z

.field public static mUseCollage:Z

.field public static mUseContextualAwareness:Z

.field public static mUseCreateAGIF:Z

.field public static mUseCreateGIF:Z

.field public static mUseCreateVideoAlbum:Z

.field public static mUseCubeDropEffect:Z

.field public static mUseCustomFilterBy:Z

.field public static mUseCustomMaxZoomLevel:Z

.field public static mUseDCIMFolderMerge:Z

.field public static mUseDCM:Z

.field public static mUseDCMRelatedCategory:Z

.field public static mUseDebug:Z

.field public static mUseDefaultAlbumView:Z

.field public static mUseDelayedDnieSetting:Z

.field public static mUseDialogWithMessage:Z

.field public static mUseDisableScrollBar:Z

.field public static mUseDisableTileAlphablending:Z

.field public static mUseDivideMapChn:Z

.field public static mUseDocumentClassifier:Z

.field public static mUseDownloadableHelp:Z

.field public static mUseDragAndDropInExpandSplitView:Z

.field public static mUseDragHereTextColorForWQHD:Z

.field public static mUseDragNDropInMultiwindow:Z

.field public static mUseDrawerExpanderMode:Z

.field public static mUseDrawerIconNotSetAlpha:Z

.field public static mUseDualBooster:Z

.field public static mUseDualSIM:Z

.field public static mUseDynamicFrame:Z

.field public static mUseEVF:Z

.field public static mUseEasymodeHelp:Z

.field public static mUseEditBurstShot:Z

.field public static mUseEditIconShowInCategory:Z

.field public static mUseEmptyScreenGalleryIcon:Z

.field public static mUseEmptyScreenPopup:Z

.field public static mUseEventView:Z

.field public static mUseFLDRMOnly:Z

.field public static mUseFaceBook:Z

.field public static mUseFaceTag:Z

.field public static mUseFaceThumbnail:Z

.field public static mUseFestival:Z

.field public static mUseFileInfoManager:Z

.field public static mUseFilmStripSelectionMode:Z

.field public static mUseFilterBG:Z

.field public static mUseFindoSearch:Z

.field public static mUseFixScrollingAlbum:Z

.field public static mUseFlashAnnotate:Z

.field public static mUseFlexibleCloudAlbumFormat:Z

.field public static mUseFlingInTalkBack:Z

.field public static mUseFonbletPictureFrame:Z

.field public static mUseFunctionSimplification:Z

.field public static mUseGalleryCrop:Z

.field public static mUseGalleryRotate:Z

.field public static mUseGallerySearchUISeperatedMode:Z

.field public static mUseGapInLand:Z

.field public static mUseGetDirection:Z

.field public static mUseGroupId:Z

.field public static mUseHWKey:Z

.field public static mUseHaptic:Z

.field public static mUseHelp:Z

.field public static mUseHideChangePlayerText:Z

.field public static mUseHomeAsUpFromSetting:Z

.field public static mUseHoveringUI:Z

.field public static mUseIdSortByASC:Z

.field public static mUseKnoxMode:Z

.field public static mUseLargeLoadingCount:Z

.field public static mUseLargeThumbnail:Z

.field public static mUseLastShare:Z

.field public static mUseLaunchTranslucent:Z

.field public static mUseLoadTask:Z

.field public static mUseLocationActionbarPanel:Z

.field public static mUseLocationInfo:Z

.field public static mUseLongPressAlbums:Z

.field public static mUseLowAnimation:Z

.field public static mUseLowMemoryCheckDialog:Z

.field public static mUseMFDMenu:Z

.field public static mUseMagazineWidget:Z

.field public static mUseMagicShotIconEnter:Z

.field public static mUseManulMulticore:Z

.field public static mUseMapleBranch:Z

.field public static mUseMergeAlbum:Z

.field public static mUseMobilePrint:Z

.field public static mUseMoreInfo:Z

.field public static mUseMotion:Z

.field public static mUseMotionTutorialDialog:Z

.field public static mUseMoveFirstAlbum:Z

.field public static mUseMpo3DDisplay:Z

.field public static mUseMs01GUI:Z

.field public static mUseMultiSIM:Z

.field public static mUseMultiWindow:Z

.field public static mUseMultiplier:Z

.field public static mUseMyFilesTabletName:Z

.field public static mUseNXPTrimApp:Z

.field public static mUseNearby:Z

.field public static mUseNearbyDMR:Z

.field public static mUseNewAlbumDrop:Z

.field public static mUseNewDragHere:Z

.field public static mUseNewFilterByHeaderForDarkTheme:Z

.field public static mUseNewOverlayHelp:Z

.field public static mUseNewSearchUI:Z

.field public static mUseOCR:Z

.field public static mUseOCRPreloadService:Z

.field public static mUseOMADrmSkip:Z

.field public static mUseOldEasyUx:Z

.field public static mUseOnlineHelp:Z

.field public static mUseOverlayHelp:Z

.field public static mUsePNGFullDecoding:Z

.field public static mUsePSTouch:Z

.field public static mUsePanoramaPlayBoost:Z

.field public static mUsePanoramaSlideshow:Z

.field public static mUsePhotoEditor:Z

.field public static mUsePhotoEditorAtShareList:Z

.field public static mUsePhotoNote:Z

.field public static mUsePhotoNoteEditor:Z

.field public static mUsePhotoReader:Z

.field public static mUsePhotoSignature:Z

.field public static mUsePhotoViewSearchText:Z

.field public static mUsePlugInDsdsService:Z

.field public static mUsePopupMenuDialog:Z

.field public static mUsePositionControllerScale:Z

.field public static mUsePostInflateNavigationDrawer:Z

.field public static mUsePremultipleAlphaBlend:Z

.field public static mUseQuickScrollTypeWinset:Z

.field public static mUseReOrderAlbums:Z

.field public static mUseReOrderAlbumsSlectionMode:Z

.field public static mUseReodereredMenuForWQHD:Z

.field public static mUseResouceManager:Z

.field public static mUseRobotoLightFontForAlbumName:Z

.field public static mUseRotationAnimaionInDetailView:Z

.field public static mUseSCamDetails:Z

.field public static mUseSDL:Z

.field public static mUseSLinkIcon:Z

.field public static mUseSPCDMR:Z

.field public static mUseSPCDMRPinchZoom:Z

.field public static mUseSamsungLinkApi:Z

.field public static mUseSaveMMSImage:Z

.field public static mUseScaleDownEndEffect:Z

.field public static mUseSecImaging:Z

.field public static mUseSecMMCodec:Z

.field public static mUseSecretBox:Z

.field public static mUseSelectAllForLimitationPickMode:Z

.field public static mUseSelectionAndSplitPinch:Z

.field public static mUseSelectionBar:Z

.field public static mUseSelectionOnActionBar:Z

.field public static mUseSetAsSNSImage:Z

.field public static mUseSettings:Z

.field public static mUseShareLocationConfirmDialog:Z

.field public static mUseShowCameraShortCut:Z

.field public static mUseShowExpansionIcon:Z

.field public static mUseSideMirrorView:Z

.field public static mUseSimpleDetails:Z

.field public static mUseSlideSettingPopup:Z

.field public static mUseSlideshowFilter:Z

.field public static mUseSlideshowVideoPlay:Z

.field public static mUseSnsSettings:Z

.field public static mUseSocialPhotoAlbum:Z

.field public static mUseSocialTag:Z

.field public static mUseSocialTag20:Z

.field public static mUseSocialTag20Chn:Z

.field public static mUseSocialTag20ChnWithWeChat:Z

.field public static mUseSoundScene:Z

.field public static mUseStoryAlbum:Z

.field public static mUseTCloud:Z

.field public static mUseTCloudMandatory:Z

.field public static mUseTDataCache:Z

.field public static mUseTalkBackToast:Z

.field public static mUseTapMenu:Z

.field public static mUseTencentDrag:Z

.field public static mUseTestLocationView:Z

.field public static mUseTestResize:Z

.field public static mUseTextureSizePowerOf2:Z

.field public static mUseTileAlphaBlending:Z

.field public static mUseTimeAllSelectMode:Z

.field public static mUseTimeViewCache:Z

.field public static mUseTimelineAlphaFadeOut:Z

.field public static mUseTouchPrediction:Z

.field public static mUseTrimAvailability:Z

.field public static mUseTvOut:Z

.field public static mUseUMS:Z

.field public static mUseUSA_PhotoEditTilteName:Z

.field public static mUseUndoDeletion:Z

.field public static final mUseVUXBuaMedia:Z

.field public static mUseVZW_ContextualTagPopUp:Z

.field public static mUseVZW_DefaultWidget:Z

.field public static mUseVZW_MotionDialogText:Z

.field public static mUseVZW_NoItemBubble:Z

.field public static mUseVibetones:Z

.field public static mUseVideoCallCrop:Z

.field public static mUseVideoClip:Z

.field public static mUseVideoWall:Z

.field public static mUseWaitForShrinkAnimationUntilDecodingCompleted:Z

.field public static mUseWallpaperCropNavigation:Z

.field public static mUseWeatherNews:Z

.field public static mUseWfd:Z

.field public static mUseWhiteTagBuddy:Z

.field public static mUseWhiteTheme:Z

.field public static mUseWifiSyncNoti:Z

.field public static mUseWritingPanel:Z

.field public static mUseXiv:Z

.field public static mUseXivFilledReusePoolMode:Z

.field public static mUseXivStopRDWhileZoomin:Z

.field public static mUseXivTileReuse:Z

.field public static mUseXivWSTN:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 55
    const-class v0, Lcom/sec/android/gallery3d/util/GalleryFeature;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->TAG:Ljava/lang/String;

    .line 57
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsInited:Z

    .line 60
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    .line 61
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotionTutorialDialog:Z

    .line 62
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    .line 63
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    .line 64
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDocumentClassifier:Z

    .line 65
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceBook:Z

    .line 66
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    .line 67
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHoveringUI:Z

    .line 68
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    .line 69
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNoteEditor:Z

    .line 70
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20:Z

    .line 71
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAccountSettings:Z

    .line 72
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DViewMode:Z

    .line 73
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCubeDropEffect:Z

    .line 74
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag:Z

    .line 75
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSettings:Z

    .line 76
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewAlbumDrop:Z

    .line 77
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFilmStripSelectionMode:Z

    .line 78
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAccuWeather:Z

    .line 79
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoEditorAtShareList:Z

    .line 80
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirBrowse:Z

    .line 81
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHWKey:Z

    .line 82
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGalleryRotate:Z

    .line 83
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGalleryCrop:Z

    .line 84
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDefaultRotationPhoneType:Z

    .line 85
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseResouceManager:Z

    .line 86
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseStoryAlbum:Z

    .line 87
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoClip:Z

    .line 88
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCollage:Z

    .line 89
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHelp:Z

    .line 90
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    .line 91
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMobilePrint:Z

    .line 92
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiplier:Z

    .line 93
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSnsSettings:Z

    .line 94
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    .line 95
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTvOut:Z

    .line 96
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    .line 97
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCRPreloadService:Z

    .line 98
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateAGIF:Z

    .line 99
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGroupId:Z

    .line 100
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSaveMMSImage:Z

    .line 101
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecMMCodec:Z

    .line 102
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUndoDeletion:Z

    .line 103
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBurstShotSettingIcon:Z

    .line 104
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFileInfoManager:Z

    .line 105
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    .line 106
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    .line 107
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DTour:Z

    .line 108
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLowMemoryCheckDialog:Z

    .line 109
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbumsSlectionMode:Z

    .line 110
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePanoramaSlideshow:Z

    .line 113
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWfd:Z

    .line 116
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    .line 117
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivWSTN:Z

    .line 118
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivTileReuse:Z

    .line 119
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivStopRDWhileZoomin:Z

    .line 120
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirJump:Z

    .line 121
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    .line 122
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    .line 123
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableUseXivLargeThumbnail:Z

    .line 124
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableUseResizeLargeThumbnail:Z

    .line 125
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivFilledReusePoolMode:Z

    .line 126
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAsyncRender:Z

    .line 127
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mHasOCR:Z

    .line 128
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoViewSearchText:Z

    .line 129
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTapMenu:Z

    .line 130
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLaunchTranslucent:Z

    .line 131
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUMS:Z

    .line 132
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsWifiOnlyModel:Z

    .line 133
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePSTouch:Z

    .line 134
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCIMFolderMerge:Z

    .line 135
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoWall:Z

    .line 136
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    .line 137
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFestival:Z

    .line 138
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSCamDetails:Z

    .line 139
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    .line 140
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTencentDrag:Z

    .line 141
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTDataCache:Z

    .line 142
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mFaceTagDefaultValue:Z

    .line 143
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVZW_MotionDialogText:Z

    .line 144
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVZW_ContextualTagPopUp:Z

    .line 145
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVZW_NoItemBubble:Z

    .line 146
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    .line 147
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWeatherNews:Z

    .line 148
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DefaultWidget:Z

    .line 149
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVZW_DefaultWidget:Z

    .line 150
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseShareLocationConfirmDialog:Z

    .line 151
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraButtonAtDetailView:Z

    .line 152
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUSA_PhotoEditTilteName:Z

    .line 153
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_PhotoEditTilteName:Z

    .line 154
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableShowMap:Z

    .line 155
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnCharacter:Z

    .line 156
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20Chn:Z

    .line 157
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20ChnWithWeChat:Z

    .line 158
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    .line 159
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSetAsSNSImage:Z

    .line 160
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    .line 161
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMRPinchZoom:Z

    .line 162
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdaptDisplayDialog:Z

    .line 163
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTimelineAlphaFadeOut:Z

    .line 164
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    .line 165
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    .line 166
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLowAnimation:Z

    .line 167
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePlugInDsdsService:Z

    .line 168
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragNDropInMultiwindow:Z

    .line 169
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaiduCloudDragDrop:Z

    .line 170
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v3, "SEC_FLOATING_FEATURE_ALLSHARE_CONFIG_VERSION"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "NONE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNearby:Z

    .line 171
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v3, "SEC_FLOATING_FEATURE_ALLSHARE_CONFIG_VERSION"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "ALL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNearbyDMR:Z

    .line 172
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v3, "SEC_FLOATING_FEATURE_FRAMEWORK_SUPPORT_VIBETONZ"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVibetones:Z

    .line 173
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTalkBackToast:Z

    .line 174
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    .line 175
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAutoHide:Z

    .line 176
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDynamicFrame:Z

    .line 177
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTextureSizePowerOf2:Z

    .line 178
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMpo3DDisplay:Z

    .line 179
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePremultipleAlphaBlend:Z

    .line 180
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoSignature:Z

    .line 181
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableHwAccelerationforImgaeNote:Z

    .line 182
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHideChangePlayerText:Z

    .line 183
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOldEasyUx:Z

    .line 184
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTrimAvailability:Z

    .line 185
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEditBurstShot:Z

    .line 186
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDisableTileAlphablending:Z

    .line 187
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAllviewBurstshot:Z

    .line 188
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNXPTrimApp:Z

    .line 189
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWritingPanel:Z

    .line 190
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    .line 191
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTagBuddy:Z

    .line 193
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    .line 194
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mHorizontalScroll:Z

    .line 195
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    .line 196
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCMRelatedCategory:Z

    .line 197
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHomeAsUpFromSetting:Z

    .line 198
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    .line 199
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraSwitchBoost:Z

    .line 200
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePanoramaPlayBoost:Z

    .line 201
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseShowCameraShortCut:Z

    .line 202
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDrawerExpanderMode:Z

    .line 203
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewFilterByHeaderForDarkTheme:Z

    .line 204
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEmptyScreenPopup:Z

    .line 205
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEmptyScreenGalleryIcon:Z

    .line 206
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOnlineHelp:Z

    .line 207
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLocationActionbarPanel:Z

    .line 208
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowVideoPlay:Z

    .line 209
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCustomMaxZoomLevel:Z

    .line 210
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloudMandatory:Z

    .line 211
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDefaultAlbumView:Z

    .line 212
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCircleFace:Z

    .line 213
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseScaleDownEndEffect:Z

    .line 214
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSequenceIconSupported:Z

    .line 215
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSimpleDetails:Z

    .line 216
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCategoryViewDefault:Z

    .line 217
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragAndDropInExpandSplitView:Z

    .line 218
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTouchPrediction:Z

    .line 219
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mNoUseBackIconInSelection:Z

    .line 220
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mNoUseSortbyPhotoView:Z

    .line 221
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAddTagAsMoreinfo:Z

    .line 222
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHaptic:Z

    .line 223
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mNoUseMovetoAlbumInTimeview:Z

    .line 224
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    .line 225
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEditIconShowInCategory:Z

    .line 226
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mNoUseCopytoClipboardInDetailview:Z

    .line 227
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewOverlayHelp:Z

    .line 228
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseQuickScrollTypeWinset:Z

    .line 229
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseIdSortByASC:Z

    .line 230
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDelayedDnieSetting:Z

    .line 231
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWaitForShrinkAnimationUntilDecodingCompleted:Z

    .line 232
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraShotAndMore:Z

    .line 235
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLocationInfo:Z

    .line 236
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mMmsEnabled:Z

    .line 237
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuSeaAsContact:Z

    .line 238
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuShareViaMsg:Z

    .line 239
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    .line 240
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mChangePicasaSyncTypeToThumb:Z

    .line 241
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    .line 242
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsLDU:Z

    .line 245
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    .line 246
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    .line 247
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    .line 248
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsMultiSIM:Z

    .line 249
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCGGMultiSIM:Z

    .line 250
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSoundScene:Z

    .line 251
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMapleBranch:Z

    .line 253
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMagazineWidget:Z

    .line 256
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTestResize:Z

    .line 257
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTestLocationView:Z

    .line 260
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    .line 261
    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    .line 262
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mCountryCode:Ljava/lang/String;

    .line 263
    const-string v0, "ro.build.characteristics"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSystemDeviceType:Ljava/lang/String;

    .line 265
    sput v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSystemUiVisibility:I

    .line 266
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->PHONE:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDeviceType:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    .line 267
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoEditor:Z

    .line 268
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDialogWithMessage:Z

    .line 269
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMagicShotIconEnter:Z

    .line 270
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSDL:Z

    .line 271
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEVF:Z

    .line 272
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCmcc:Z

    .line 273
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWifiSyncNoti:Z

    .line 274
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsPortraitTablet:Z

    .line 276
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewDragHere:Z

    .line 278
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFindoSearch:Z

    .line 279
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirButton:Z

    .line 280
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAutoScrollTimeView:Z

    .line 281
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTimeViewCache:Z

    .line 282
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGetDirection:Z

    .line 283
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    .line 284
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateVideoAlbum:Z

    .line 285
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAlbumGrouping:Z

    .line 286
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLongPressAlbums:Z

    .line 288
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    .line 289
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    .line 292
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMs01GUI:Z

    .line 293
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePopupMenuDialog:Z

    .line 296
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialPhotoAlbum:Z

    .line 297
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsNoteModel:Z

    .line 299
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableStudioShowAsAction:Z

    .line 300
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableEditShowAsAction:Z

    .line 301
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSideMirrorView:Z

    .line 302
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    .line 303
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDebug:Z

    .line 304
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCardEnable:Z

    .line 305
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOMADrmSkip:Z

    .line 306
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFLDRMOnly:Z

    .line 307
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMFDMenu:Z

    .line 308
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMyFilesTabletName:Z

    .line 309
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    .line 310
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLastShare:Z

    .line 311
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChooserActivity:Z

    .line 312
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbums:Z

    .line 313
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionAndSplitPinch:Z

    .line 315
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsLognlifemodeEnabled:Z

    .line 318
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSupportKnoxMove:Z

    .line 319
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseKnoxMode:Z

    .line 321
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseManulMulticore:Z

    .line 323
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGapInLand:Z

    .line 326
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSupportSoundAndShot:Z

    .line 329
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    .line 332
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaiduPositioning:Z

    .line 334
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMoreInfo:Z

    .line 336
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mBurstShotGrouping:Z

    .line 342
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mCheckVersionOfContextProvider:Z

    .line 344
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsGuestMode:Z

    .line 346
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseRotationAnimaionInDetailView:Z

    .line 347
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualBooster:Z

    .line 350
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTimeAllSelectMode:Z

    .line 352
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSLinkIcon:Z

    .line 354
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsTwHk:Z

    .line 356
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsJPN:Z

    .line 358
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowFilter:Z

    .line 360
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideSettingPopup:Z

    .line 362
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsGolfIconSupported:Z

    .line 364
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsDecorAlwaysVisible:Z

    .line 365
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFonbletPictureFrame:Z

    .line 366
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDisableScrollBar:Z

    .line 367
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoCallCrop:Z

    .line 368
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLoadTask:Z

    .line 369
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirViewActionBar:Z

    .line 370
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlashAnnotate:Z

    .line 371
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsCropInTB:Z

    .line 373
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDivideMapChn:Z

    .line 375
    const/16 v0, 0x12c

    sput v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mMaxCountForSelectionLoadingTask:I

    .line 377
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlexibleCloudAlbumFormat:Z

    .line 378
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePostInflateNavigationDrawer:Z

    .line 379
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsVZW:Z

    .line 380
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateGIF:Z

    .line 381
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    .line 384
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCustomFilterBy:Z

    .line 385
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseShowExpansionIcon:Z

    .line 386
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLargeLoadingCount:Z

    .line 387
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoReader:Z

    .line 388
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReodereredMenuForWQHD:Z

    .line 389
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragHereTextColorForWQHD:Z

    .line 390
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    .line 391
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEasymodeHelp:Z

    .line 392
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePositionControllerScale:Z

    .line 393
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMoveFirstAlbum:Z

    .line 394
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAddTagSelectionDropDown:Z

    .line 395
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLargeThumbnail:Z

    .line 396
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecImaging:Z

    .line 397
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisablePinchZoomWhenPointersOutOfThumbnail:Z

    .line 398
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChangeThumbnail:Z

    .line 399
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFunctionSimplification:Z

    .line 400
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFilterBG:Z

    .line 401
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOverlayHelp:Z

    .line 402
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseRobotoLightFontForAlbumName:Z

    .line 403
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDrawerIconNotSetAlpha:Z

    .line 404
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisplaySelectAllButtonInLimitationPickMode:Z

    .line 407
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mTranslateGlTextCanvas:Z

    return-void

    :cond_0
    move v0, v2

    .line 170
    goto/16 :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338
    return-void
.end method

.method private static checkAddAutoRotationIcon()V
    .locals 0

    .prologue
    .line 1471
    return-void
.end method

.method private static checkBaiduCloudDragDrop()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2036
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    if-ne v0, v2, :cond_1

    .line 2037
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gduoszn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2040
    :cond_0
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaiduCloudDragDrop:Z

    .line 2043
    :cond_1
    return-void
.end method

.method private static checkCMCCModel()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 990
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jaltezm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jfltezm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "delos3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "t0voltezm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jfltecsfbzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "t0ltezm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "melius3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "baffinvetd3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs03ltezm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 992
    :cond_0
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    .line 993
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSnsSettings:Z

    .line 994
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCmcc:Z

    .line 995
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGetDirection:Z

    .line 997
    :cond_1
    return-void
.end method

.method private static checkCropInTB()V
    .locals 2

    .prologue
    .line 2293
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsCropInTB:Z

    .line 2295
    return-void

    .line 2293
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkCscFeature()V
    .locals 3

    .prologue
    .line 1831
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    .line 1836
    .local v0, "cscFeature":Lcom/sec/android/app/CscFeature;
    const-string v2, "CscFeature_Gallery_DisableMenuSetAsContact"

    invoke-virtual {v0, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuSeaAsContact:Z

    .line 1838
    const-string v2, "CscFeature_Gallery_DisableMenuShareViaMsg"

    invoke-virtual {v0, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuShareViaMsg:Z

    .line 1840
    const-string v2, "CscFeature_Gallery_AddAutoRotationIcon"

    invoke-virtual {v0, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    .line 1842
    const-string v2, "CscFeature_Gallery_DefSyncSize4Picasa"

    invoke-virtual {v0, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1844
    .local v1, "syncSizeDef":Ljava/lang/String;
    const-string/jumbo v2, "thumbnail"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1845
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mChangePicasaSyncTypeToThumb:Z

    .line 1849
    :goto_0
    return-void

    .line 1847
    :cond_0
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mChangePicasaSyncTypeToThumb:Z

    goto :goto_0
.end method

.method private static checkDefaultRotationPhoneType()V
    .locals 2

    .prologue
    .line 980
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 981
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDefaultRotationPhoneType:Z

    .line 984
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "santos7"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "millet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 985
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDefaultRotationPhoneType:Z

    .line 987
    :cond_2
    return-void
.end method

.method private static checkDeviceType(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1860
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1861
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->TABLET:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDeviceType:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    .line 1867
    :cond_0
    :goto_0
    return-void

    .line 1862
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "gd1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "u0lte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "u0wifi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1863
    :cond_2
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->CAMERA:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDeviceType:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    goto :goto_0

    .line 1864
    :cond_3
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "mproject"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "m2alte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "m2a3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1865
    :cond_4
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->CAMERA_PHONE:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDeviceType:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    goto :goto_0
.end method

.method private static checkDisableEditShowAsAction()V
    .locals 2

    .prologue
    .line 1727
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableEditShowAsAction:Z

    .line 1730
    return-void

    .line 1727
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkDisableHide()V
    .locals 2

    .prologue
    .line 2046
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs023g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs03ltezm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kqlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-04F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL23"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    .line 2050
    return-void

    .line 2046
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkDisableHwAccelerationforImgaeNote()V
    .locals 2

    .prologue
    .line 1925
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ks01lte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hltexx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlteuc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hltevj"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableHwAccelerationforImgaeNote:Z

    .line 1927
    return-void

    .line 1925
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkDisableMenuSeaAsContact()V
    .locals 1

    .prologue
    .line 1460
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1461
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuSeaAsContact:Z

    .line 1463
    :cond_0
    return-void
.end method

.method private static checkDisableMenuShareViaMsg()V
    .locals 0

    .prologue
    .line 1467
    return-void
.end method

.method private static checkDisablePinchZoomWhenPointersOutOfThumbnail()V
    .locals 2

    .prologue
    .line 2399
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisablePinchZoomWhenPointersOutOfThumbnail:Z

    .line 2403
    return-void

    .line 2399
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkDisableShowMap()V
    .locals 1

    .prologue
    .line 1014
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1015
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableShowMap:Z

    .line 1017
    :cond_0
    return-void
.end method

.method private static checkDisableStudioShowAsAction()V
    .locals 2

    .prologue
    .line 1733
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableStudioShowAsAction:Z

    .line 1737
    return-void

    .line 1733
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkDisableTileAlphablending()V
    .locals 2

    .prologue
    .line 2072
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "wilcox"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDisableTileAlphablending:Z

    .line 2073
    return-void
.end method

.method private static checkDisableUseResizeLargeThumbnail()V
    .locals 2

    .prologue
    .line 1554
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "crater"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "serrano"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "logan"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "delos3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "garda"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "baffin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "s2ve"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "mproject"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs02"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "wilcox"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cane"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "skomerxx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableUseResizeLargeThumbnail:Z

    .line 1559
    return-void

    .line 1554
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkDisableUseXivLargeThumbnail()V
    .locals 2

    .prologue
    .line 1549
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "espresso10"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "baffinvetd3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "baffinvetd3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableUseXivLargeThumbnail:Z

    .line 1550
    return-void

    .line 1549
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkDisplaySelectAllButtonInLimitationPickMode()V
    .locals 2

    .prologue
    .line 2579
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "pateklteduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisplaySelectAllButtonInLimitationPickMode:Z

    .line 2580
    return-void
.end method

.method private static checkDragNDropInMultiwindow()V
    .locals 2

    .prologue
    .line 2013
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vienna"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "flte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ms01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gzn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gchnduoszn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kqlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "patek"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-04F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL23"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "KEW_jp_kdi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCT21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lentis"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "slte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "mega2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vasta"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kccat6"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2030
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragNDropInMultiwindow:Z

    .line 2032
    :cond_1
    return-void
.end method

.method private static checkEasymodeHelp()V
    .locals 2

    .prologue
    .line 696
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEasymodeHelp:Z

    .line 700
    return-void

    .line 696
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkEnableAirButton()V
    .locals 2

    .prologue
    .line 1934
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vienna"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirButton:Z

    .line 1937
    return-void

    .line 1934
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkEnableAlbumGrouping()V
    .locals 1

    .prologue
    .line 1962
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAlbumGrouping:Z

    .line 1963
    return-void
.end method

.method private static checkEnableAutoScrollTimeView()V
    .locals 0

    .prologue
    .line 1940
    return-void
.end method

.method private static checkEnableBurstShotSettingIcon()V
    .locals 0

    .prologue
    .line 1949
    return-void
.end method

.method private static checkEnableCreateVideoAlbum()V
    .locals 2

    .prologue
    .line 2006
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vienna"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "fltexx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "flte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateVideoAlbum:Z

    .line 2009
    return-void

    .line 2006
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkEnableEditBurstShot()V
    .locals 2

    .prologue
    .line 2062
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vienna"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "flte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEditBurstShot:Z

    .line 2065
    return-void

    .line 2062
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkEnableFixScrollingAlbum()V
    .locals 2

    .prologue
    .line 1952
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vienna"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "flte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kqlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-04F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL23"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lentis"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    .line 1959
    return-void

    .line 1952
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkEnableNote(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1967
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "delos3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "espresso"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs023gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs023gzn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs05"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isUnderWvga(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1971
    :cond_0
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNoteEditor:Z

    .line 1996
    :goto_0
    return-void

    .line 1977
    :cond_1
    invoke-static {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteUtils;->checkEnableSpen(Landroid/content/Context;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1979
    :pswitch_0
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    goto :goto_0

    .line 1984
    :pswitch_1
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    goto :goto_0

    .line 1989
    :pswitch_2
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    goto :goto_0

    .line 1977
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static checkEnableOrangeCloudOnTopInShareVia()V
    .locals 0

    .prologue
    .line 1475
    return-void
.end method

.method private static checkEnableSecretBox(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1999
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v0, :cond_0

    .line 2000
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setSecretBox(Landroid/content/Context;)V

    .line 2001
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretDirMounted(Landroid/content/Context;)V

    .line 2003
    :cond_0
    return-void
.end method

.method private static checkEnableTimeViewCache()V
    .locals 2

    .prologue
    .line 1943
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "flte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTimeViewCache:Z

    .line 1945
    return-void

    .line 1943
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkFaceTagDefaultValue()V
    .locals 0

    .prologue
    .line 1525
    return-void
.end method

.method private static checkFindoSearch(Landroid/content/pm/PackageManager;)V
    .locals 0
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 1931
    return-void
.end method

.method private static checkGlTextViewCanvas()V
    .locals 2

    .prologue
    .line 703
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jflte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mTranslateGlTextCanvas:Z

    .line 704
    return-void

    .line 703
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkHasOCR()V
    .locals 0

    .prologue
    .line 1164
    return-void
.end method

.method private static checkHideChangePlayerText()V
    .locals 2

    .prologue
    .line 2053
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "melius"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt02"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt06"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHideChangePlayerText:Z

    .line 2054
    return-void

    .line 2053
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkIsCHN()V
    .locals 3

    .prologue
    .line 1479
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1480
    .local v0, "sales":Ljava/lang/String;
    const-string v1, "CHN"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHM"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHC"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHU"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CTC"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "LHS"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "klteduoszm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "klteduosctc"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "chagallwifizcx"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "trltelduzc"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    .line 1484
    const-string v1, "PAP"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsLDU:Z

    .line 1486
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDivideMapChn:Z

    .line 1488
    return-void

    .line 1480
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static checkIsDecorAlwaysVisible()V
    .locals 2

    .prologue
    .line 2281
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "KEW_jp_kdi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCT21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsDecorAlwaysVisible:Z

    .line 2282
    return-void

    .line 2281
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkIsGuestMode(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 788
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    const/16 v2, 0x64

    if-ge v1, v2, :cond_0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsGuestMode:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 792
    :goto_1
    return-void

    .line 788
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 789
    :catch_0
    move-exception v0

    .line 790
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->TAG:Ljava/lang/String;

    const-string v2, "cannot found user manager"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static checkIsJPN()V
    .locals 2

    .prologue
    .line 2227
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "JP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsJPN:Z

    .line 2228
    return-void
.end method

.method private static checkIsMultiSIM()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2518
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsMultiSIM:Z

    .line 2519
    return-void

    .line 2518
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkIsSPCEnabled()V
    .locals 0

    .prologue
    .line 1500
    return-void
.end method

.method public static checkIsSupportSoundAndShot()V
    .locals 2

    .prologue
    .line 757
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-04F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL23"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "KEW_jp_kdi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCT21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kminilte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kmini3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "patek"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "m2alte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "m2a3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "mega2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vasta"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lentis"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "leviltetdzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kccat6"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "slte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a7lte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSupportSoundAndShot:Z

    .line 774
    return-void

    .line 757
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkIsSupportWFD(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1715
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1716
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 1717
    const/4 v1, 0x0

    .line 1722
    :goto_0
    return v1

    .line 1720
    :cond_0
    const-string v1, "com.sec.feature.wfd_support"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWfd:Z

    .line 1722
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWfd:Z

    goto :goto_0
.end method

.method private static checkIsTwHk()V
    .locals 2

    .prologue
    .line 2223
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kltezh"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kltezt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsTwHk:Z

    .line 2224
    return-void

    .line 2223
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkIsVZW()V
    .locals 3

    .prologue
    .line 2319
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2320
    .local v1, "SALES_CODE":Ljava/lang/String;
    const-string v0, "VZW"

    .line 2322
    .local v0, "OPERATOR":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsVZW:Z

    .line 2323
    return-void
.end method

.method private static checkIsWifiOnlyModel(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1212
    const-string v1, "connectivity"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1213
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1214
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsWifiOnlyModel:Z

    .line 1216
    :cond_0
    return-void
.end method

.method private static checkKNOXUse(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2118
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSupportKnoxMove:Z

    if-nez v0, :cond_0

    .line 2119
    invoke-static {p0, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setKNOXInstallMode(Landroid/content/Context;Z)V

    .line 2132
    :goto_0
    return-void

    .line 2121
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2122
    :cond_1
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseKnoxMode:Z

    .line 2123
    invoke-static {p0, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setKNOXInstallMode(Landroid/content/Context;Z)V

    goto :goto_0

    .line 2124
    :cond_2
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kqlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-04F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL23"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lentis"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2127
    :cond_3
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseKnoxMode:Z

    goto :goto_0

    .line 2129
    :cond_4
    invoke-static {p0, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setKNOXInstallMode(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method private static checkMmsEnabled()V
    .locals 2

    .prologue
    .line 1451
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagallwifizs"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagallwifizc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagallwifixx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagallltevzw"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagallltespr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagalllteatt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagalllteusc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimtwifixx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimtlteuc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimtltevzw"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubenswifi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1454
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mMmsEnabled:Z

    .line 1456
    :cond_1
    return-void
.end method

.method private static checkNearbyDemoForSProject()V
    .locals 0

    .prologue
    .line 1429
    return-void
.end method

.method private static checkNoUseBackIconInSelection()V
    .locals 2

    .prologue
    .line 2420
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mNoUseBackIconInSelection:Z

    .line 2424
    return-void

    .line 2420
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkNoUseCopytoClipboardInDetailview()V
    .locals 2

    .prologue
    .line 2510
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mCountryCode:Ljava/lang/String;

    const-string v1, "USA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mNoUseCopytoClipboardInDetailview:Z

    .line 2515
    return-void

    .line 2510
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkNoUseMovetoAlbumInTimeview()V
    .locals 2

    .prologue
    .line 2493
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mNoUseMovetoAlbumInTimeview:Z

    .line 2497
    return-void

    .line 2493
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkNoUseSortbyPhotoView()V
    .locals 2

    .prologue
    .line 2433
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mNoUseSortbyPhotoView:Z

    .line 2438
    return-void

    .line 2433
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkPortraitTablet()V
    .locals 2

    .prologue
    .line 1879
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt02"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "konawifixx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "konaltexx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "espressorfxx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "espressowifixx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "millet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt06"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsPortraitTablet:Z

    .line 1884
    return-void

    .line 1879
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkSlideSettingPopup()V
    .locals 2

    .prologue
    .line 2250
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "mega2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vasta"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2253
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideSettingPopup:Z

    .line 2257
    :goto_0
    return-void

    .line 2255
    :cond_1
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideSettingPopup:Z

    goto :goto_0
.end method

.method private static checkSlideshowVideoPlay()V
    .locals 2

    .prologue
    .line 2307
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowVideoPlay:Z

    .line 2311
    return-void

    .line 2307
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkSystemProperty()V
    .locals 3

    .prologue
    .line 683
    const-string/jumbo v1, "true"

    const-string v2, "debug.gallery.performance"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 684
    .local v0, "performanceDebug":Z
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME_ENABLE:Z

    .line 685
    return-void
.end method

.method private static checkSystemUiVisibility(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1894
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "espresso"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1895
    :cond_0
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSystemUiVisibility:I

    .line 1897
    :cond_1
    return-void
.end method

.method private static checkUse3DPanorama()V
    .locals 2

    .prologue
    .line 1372
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jflte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jalte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jactive"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ks01lte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jftdd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "flte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kqlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "patek"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-04F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL23"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "m2alte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "m2a3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vasta"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lentis"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "slte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    .line 1388
    return-void

    .line 1372
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUse3DTour()V
    .locals 2

    .prologue
    .line 753
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "KEW_jp_kdi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCT21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DTour:Z

    .line 754
    return-void

    .line 753
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUse3DViewMode(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1231
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isOverHD(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1232
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DViewMode:Z

    .line 1234
    :cond_0
    return-void
.end method

.method private static checkUseATT_DateTag()V
    .locals 1

    .prologue
    .line 1544
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isATTFamily()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    .line 1545
    return-void
.end method

.method private static checkUseATT_DefaultWidget()V
    .locals 1

    .prologue
    .line 1569
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isATTFamily()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DefaultWidget:Z

    .line 1570
    return-void
.end method

.method private static checkUseATT_PhotoEditTilteName()V
    .locals 1

    .prologue
    .line 1609
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isATTFamily()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_PhotoEditTilteName:Z

    .line 1610
    return-void
.end method

.method private static checkUseAccountSettings()V
    .locals 1

    .prologue
    .line 1224
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1225
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAccountSettings:Z

    .line 1227
    :cond_0
    return-void
.end method

.method public static checkUseAccuWeather()V
    .locals 3

    .prologue
    .line 1626
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isKORFamily()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1627
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_LiveWallpaper_WeatherWallCPName"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1628
    .local v0, "mCPName":Ljava/lang/String;
    const-string v1, "accuweather"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1629
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAccuWeather:Z

    .line 1634
    :cond_0
    :goto_0
    return-void

    .line 1631
    :cond_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAccuWeather:Z

    goto :goto_0
.end method

.method private static checkUseAdaptDisplayDialog()V
    .locals 0

    .prologue
    .line 976
    return-void
.end method

.method private static checkUseAddTagAsMoreinfo()V
    .locals 2

    .prologue
    .line 2441
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAddTagAsMoreinfo:Z

    .line 2445
    return-void

    .line 2441
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseAddTagSelectionDropDown()V
    .locals 2

    .prologue
    .line 2479
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAddTagSelectionDropDown:Z

    .line 2483
    return-void

    .line 2479
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseAirBrowse()V
    .locals 1

    .prologue
    .line 1142
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1143
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirBrowse:Z

    .line 1145
    :cond_0
    return-void
.end method

.method private static checkUseAirJump()V
    .locals 0

    .prologue
    .line 1138
    return-void
.end method

.method private static checkUseAirViewActionbar()V
    .locals 2

    .prologue
    .line 722
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirViewActionBar:Z

    .line 726
    return-void

    .line 722
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseAsyncRender()V
    .locals 0

    .prologue
    .line 1117
    return-void
.end method

.method private static checkUseBaidu()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1317
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    .line 1318
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-ne v0, v1, :cond_0

    .line 1319
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    .line 1323
    :cond_0
    return-void
.end method

.method private static checkUseBaiduPositioning()V
    .locals 2

    .prologue
    .line 2175
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->TAG:Ljava/lang/String;

    const-string v1, "checkUseBaiduPositioning"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2177
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "goyawifizc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "goya3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "v1a3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "v1awifizc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ms013gzn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ms013gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kltezn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3gduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2179
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->TAG:Ljava/lang/String;

    const-string v1, "mUseBaiduPositioning true"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2180
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaiduPositioning:Z

    .line 2182
    :cond_1
    return-void
.end method

.method private static checkUseCameraButtonAtDetailView()V
    .locals 2

    .prologue
    .line 1601
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mCountryCode:Ljava/lang/String;

    const-string v1, "USA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraButtonAtDetailView:Z

    .line 1602
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1603
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraButtonAtDetailView:Z

    .line 1605
    :cond_0
    return-void
.end method

.method private static checkUseCameraShotAndMore()V
    .locals 2

    .prologue
    .line 2587
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mCountryCode:Ljava/lang/String;

    const-string v1, "USA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraShotAndMore:Z

    .line 2588
    return-void
.end method

.method private static checkUseCameraSwitchBoost()V
    .locals 2

    .prologue
    .line 2186
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kqlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-04F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL23"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lentis"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2189
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraSwitchBoost:Z

    .line 2191
    :cond_1
    return-void
.end method

.method private static checkUseCategoryViewDefault()V
    .locals 2

    .prologue
    .line 2348
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCategoryViewDefault:Z

    .line 2352
    return-void

    .line 2348
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseChangeThumb()V
    .locals 2

    .prologue
    .line 850
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "pateklteduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vastalteduoszm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChangeThumbnail:Z

    .line 851
    return-void

    .line 850
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseChnCharacter()V
    .locals 2

    .prologue
    .line 1650
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mCountryCode:Ljava/lang/String;

    const-string v1, "CHINA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnCharacter:Z

    .line 1651
    return-void
.end method

.method private static checkUseChnUsageAlertPopup()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1345
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-ne v0, v1, :cond_0

    .line 1346
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    .line 1350
    :cond_0
    return-void
.end method

.method private static checkUseCircleFace()V
    .locals 0

    .prologue
    .line 2330
    return-void
.end method

.method private static checkUseCloud(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1105
    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudAgentExist(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    .line 1106
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    if-eqz v0, :cond_0

    .line 1107
    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudContentSyncOn(Landroid/content/Context;)Z

    .line 1109
    :cond_0
    return-void
.end method

.method private static checkUseCmcc()V
    .locals 2

    .prologue
    .line 884
    const-string v0, "CHM"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCmcc:Z

    .line 885
    return-void
.end method

.method private static checkUseCollage(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 950
    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "logan"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "ks023g"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "t0ltezm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "cs023gzc"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "cs023gzn"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "delos3gzm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "espresso"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "t0ltespr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "d2spr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "mproject"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "t0voltezm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "cs05"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "cs02ve"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "chagall"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "klimt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "rubens"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "KEW_jp_kdi"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "SCT21"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCollage:Z

    .line 955
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isOverHD(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 956
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCollage:Z

    .line 958
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCollage:Z

    if-eqz v1, :cond_1

    .line 959
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.mimage.photoretouching.multigrid"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 960
    .local v0, "useCollageIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v3, 0x10000

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-nez v1, :cond_1

    .line 961
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCollage:Z

    .line 964
    .end local v0    # "useCollageIntent":Landroid/content/Intent;
    :cond_1
    return-void

    :cond_2
    move v1, v2

    .line 950
    goto :goto_0
.end method

.method private static checkUseContextualAwareness()V
    .locals 2

    .prologue
    .line 1129
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "espresso"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    .line 1134
    return-void

    .line 1129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseCreateAGIF(Landroid/content/pm/PackageManager;)V
    .locals 4
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    const/4 v3, 0x0

    .line 856
    :try_start_0
    const-string v1, "com.sec.android.mimage.photoretouching.motionphoto"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 862
    :goto_0
    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "cane"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 863
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateAGIF:Z

    .line 865
    :cond_0
    return-void

    .line 857
    :catch_0
    move-exception v0

    .line 858
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateAGIF:Z

    goto :goto_0

    .line 859
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 860
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static checkUseCreateGif()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2355
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v2, :cond_2

    .line 2356
    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "trlte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "tblte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "a5"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "vastalte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "SC-01G"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "SCL24"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "e7ltezc"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateGIF:Z

    .line 2362
    :goto_0
    return-void

    .line 2360
    :cond_2
    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "a5ltezt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "a5ltezh"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateGIF:Z

    goto :goto_0
.end method

.method private static checkUseCubeDropEffect(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1238
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWvga(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isUnderWvga(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "espresso"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1239
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCubeDropEffect:Z

    .line 1241
    :cond_1
    return-void
.end method

.method private static checkUseCustomFilterBy()V
    .locals 2

    .prologue
    .line 2378
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCustomFilterBy:Z

    .line 2382
    return-void

    .line 2378
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseCustomMaxZoomLevel()V
    .locals 2

    .prologue
    .line 2314
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lentis"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCustomMaxZoomLevel:Z

    .line 2315
    return-void
.end method

.method private static checkUseDCIMFolderMerge()V
    .locals 0

    .prologue
    .line 1252
    return-void
.end method

.method private static checkUseDCM(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 807
    invoke-static {p0}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->isDCMAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 808
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    .line 811
    :goto_0
    return-void

    .line 810
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    goto :goto_0
.end method

.method private static checkUseDCMRelatedCategory()V
    .locals 2

    .prologue
    .line 814
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCMRelatedCategory:Z

    .line 819
    return-void

    .line 814
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseDefaultAlbumView()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 737
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "kminilte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "kmini3g"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDefaultAlbumView:Z

    .line 738
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v2, "SEC_FLOATING_FEATURE_COMMON_SUPPORT_UNPACK"

    invoke-virtual {v0, v2}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 739
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDefaultAlbumView:Z

    .line 741
    :cond_1
    return-void

    .line 737
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseDelayedDnieSetting()V
    .locals 2

    .prologue
    .line 2573
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagallhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDelayedDnieSetting:Z

    .line 2574
    return-void
.end method

.method private static checkUseDisableScrollBar()V
    .locals 2

    .prologue
    .line 2285
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lentis"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDisableScrollBar:Z

    .line 2286
    return-void
.end method

.method private static checkUseDocumentClassifier()V
    .locals 0

    .prologue
    .line 1074
    return-void
.end method

.method private static checkUseDragAndDropInExpandSplitView()V
    .locals 2

    .prologue
    .line 2364
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragAndDropInExpandSplitView:Z

    .line 2368
    return-void

    .line 2364
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseDraqHereTextColorForWQHD()V
    .locals 2

    .prologue
    .line 2392
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragHereTextColorForWQHD:Z

    .line 2396
    return-void

    .line 2392
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseDrawerIconAlpha()V
    .locals 2

    .prologue
    .line 2576
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagallhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "patek"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDrawerIconNotSetAlpha:Z

    .line 2577
    return-void

    .line 2576
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseDualBooster()V
    .locals 2

    .prologue
    .line 2205
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "m2alte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "m2a3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2206
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualBooster:Z

    .line 2208
    :cond_1
    return-void
.end method

.method private static checkUseDualSIM()V
    .locals 0

    .prologue
    .line 1504
    return-void
.end method

.method private static checkUseEVF()V
    .locals 0

    .prologue
    .line 881
    return-void
.end method

.method private static checkUseEditShowInCategory()V
    .locals 2

    .prologue
    .line 2503
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEditIconShowInCategory:Z

    .line 2507
    return-void

    .line 2503
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseEmptyScreenPopup()V
    .locals 2

    .prologue
    .line 2211
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mCountryCode:Ljava/lang/String;

    const-string v1, "USA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEmptyScreenPopup:Z

    .line 2212
    return-void
.end method

.method private static checkUseFLDRMOnly()V
    .locals 5

    .prologue
    .line 2096
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/lib/libomafldrm.so"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2098
    .local v2, "flLibPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2099
    .local v1, "file":Ljava/io/File;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2100
    const/4 v3, 0x1

    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFLDRMOnly:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2105
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 2102
    :catch_0
    move-exception v0

    .line 2103
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private static checkUseFaceBook()V
    .locals 1

    .prologue
    .line 1078
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1079
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceBook:Z

    .line 1081
    :cond_0
    return-void
.end method

.method private static checkUseFaceTag()V
    .locals 1

    .prologue
    .line 1060
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1061
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    .line 1063
    :cond_0
    return-void
.end method

.method private static checkUseFaceThumbnail()V
    .locals 1

    .prologue
    .line 1067
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1068
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    .line 1070
    :cond_0
    return-void
.end method

.method private static checkUseFestival()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 1327
    const-string v0, "2"

    .line 1328
    .local v0, "FESTIVAL_VERSION":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v5

    const-string v6, "SEC_FLOATING_FEATURE_COMMON_SUPPORT_FESTIVAL_EFFECT"

    invoke-virtual {v5, v6}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    .line 1329
    .local v3, "enableFestival":Z
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v5

    const-string v6, "SEC_FLOATING_FEATURE_COMMON_SUPPORT_FESTIVAL_EFFECT_VERSION"

    const-string v7, "1"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1332
    .local v4, "verFestivalEffect":Ljava/lang/String;
    const-string v1, "festivalv2themev2"

    .line 1333
    .local v1, "FESTIVAL_VERSION_L":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v5

    const-string v6, "SEC_FLOATING_FEATURE_COMMON_CONFIG_CHANGEABLE_UI"

    invoke-virtual {v5, v6}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1335
    .local v2, "checkFestivalEffect":Ljava/lang/String;
    if-ne v3, v8, :cond_1

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1336
    sput-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFestival:Z

    .line 1341
    :cond_0
    :goto_0
    return-void

    .line 1337
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1338
    sput-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFestival:Z

    goto :goto_0
.end method

.method private static checkUseFileInfoManager()V
    .locals 0

    .prologue
    .line 778
    return-void
.end method

.method private static checkUseFilmStripSelectionMode()V
    .locals 0

    .prologue
    .line 1433
    return-void
.end method

.method private static checkUseFlashAnnotate(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 729
    const-string v0, "com.sec.spen.flashannotate"

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkPackageExist(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 730
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlashAnnotate:Z

    .line 734
    :goto_0
    return-void

    .line 732
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlashAnnotate:Z

    goto :goto_0
.end method

.method private static checkUseFlexibleCloudAlbumFormat()V
    .locals 2

    .prologue
    .line 688
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlexibleCloudAlbumFormat:Z

    .line 693
    return-void

    .line 688
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseFlingInTalkBack()V
    .locals 2

    .prologue
    .line 2522
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    .line 2526
    return-void

    .line 2522
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseFonbletPictureFrame()V
    .locals 2

    .prologue
    .line 718
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "mega2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vasta"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFonbletPictureFrame:Z

    .line 719
    return-void

    .line 718
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseFunctionSimplification()V
    .locals 2

    .prologue
    .line 2542
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFunctionSimplification:Z

    .line 2547
    return-void

    .line 2542
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseGallerySearchUISeperatedMode()V
    .locals 2

    .prologue
    .line 2167
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "KEW_jp_kdi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCT21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2169
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    .line 2171
    :cond_1
    return-void
.end method

.method private static checkUseGapLand()V
    .locals 2

    .prologue
    .line 831
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs02ve"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGapInLand:Z

    .line 832
    return-void
.end method

.method private static checkUseGolfIcon()V
    .locals 2

    .prologue
    .line 2260
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "KEW_jp_kdi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCT21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "slte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "mega2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vasta"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "patek"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsGolfIconSupported:Z

    .line 2269
    return-void

    .line 2260
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseGroupId(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1313
    return-void
.end method

.method private static checkUseHWKey()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1409
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "espresso"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHWKey:Z

    .line 1410
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1411
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHWKey:Z

    .line 1413
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1409
    goto :goto_0
.end method

.method private static checkUseHaptic()V
    .locals 2

    .prologue
    .line 2472
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHaptic:Z

    .line 2476
    return-void

    .line 2472
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static checkUseHelp()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1662
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "espresso"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "kona"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHelp:Z

    .line 1664
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mCountryCode:Ljava/lang/String;

    const-string v2, "USA"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "klte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "k3g"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "kminilte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "kmini3g"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1667
    :cond_0
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHelp:Z

    .line 1671
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 1662
    goto :goto_0

    .line 1668
    :cond_3
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_1

    .line 1669
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHelp:Z

    goto :goto_1
.end method

.method private static checkUseHomeAsUpFromSetting()V
    .locals 2

    .prologue
    .line 822
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vastalte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kminilte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kmini3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHomeAsUpFromSetting:Z

    .line 828
    return-void

    .line 822
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseHoveringUI()V
    .locals 0

    .prologue
    .line 1149
    return-void
.end method

.method private static checkUseIdSortByASC()V
    .locals 2

    .prologue
    .line 2550
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseIdSortByASC:Z

    .line 2555
    return-void

    .line 2550
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseLargeLoadingCount()V
    .locals 2

    .prologue
    .line 2413
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLargeLoadingCount:Z

    .line 2417
    return-void

    .line 2413
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseLargeThumbnail()V
    .locals 2

    .prologue
    .line 2486
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLargeThumbnail:Z

    .line 2490
    return-void

    .line 2486
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseLaunchTranslucent()V
    .locals 2

    .prologue
    .line 1203
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "gd1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "u0lte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "u0wifi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLaunchTranslucent:Z

    .line 1204
    return-void

    .line 1203
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseLoadTask()V
    .locals 0

    .prologue
    .line 2304
    return-void
.end method

.method private static checkUseLocationActionbarPanel()V
    .locals 1

    .prologue
    .line 1591
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLocationActionbarPanel:Z

    .line 1592
    return-void
.end method

.method private static checkUseLocationInfo()V
    .locals 0

    .prologue
    .line 1437
    return-void
.end method

.method private static checkUseLongPressAlbums()V
    .locals 1

    .prologue
    .line 967
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLongPressAlbums:Z

    .line 968
    return-void
.end method

.method private static checkUseLowAnimation()V
    .locals 2

    .prologue
    .line 1445
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "crater"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "logan"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "delos3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs02ve"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs023gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs023gzn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "espresso"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs05"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLowAnimation:Z

    .line 1447
    return-void

    .line 1445
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseLowMemoryCheckDialog()V
    .locals 1

    .prologue
    .line 672
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLowMemoryCheckDialog:Z

    .line 673
    return-void
.end method

.method private static checkUseMFDMenu()V
    .locals 2

    .prologue
    .line 2108
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "flte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMFDMenu:Z

    .line 2109
    return-void
.end method

.method private static checkUseMagicShotIconEnter()V
    .locals 2

    .prologue
    .line 1270
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMagicShotIconEnter:Z

    .line 1274
    return-void

    .line 1270
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseManualMulticore()V
    .locals 2

    .prologue
    .line 2135
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2136
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseManulMulticore:Z

    .line 2138
    :cond_1
    return-void
.end method

.method private static checkUseMobilePrint(Landroid/content/pm/PackageManager;)V
    .locals 3
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 898
    :try_start_0
    const-string v1, "com.sec.android.app.mobileprint"

    const/16 v2, 0x80

    invoke-virtual {p0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 904
    :goto_0
    return-void

    .line 899
    :catch_0
    move-exception v0

    .line 900
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMobilePrint:Z

    goto :goto_0

    .line 901
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 902
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static checkUseMotion(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1021
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isGyroSensorAvailable(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    .line 1024
    return-void
.end method

.method private static checkUseMotionTutorialDialog(Landroid/content/pm/PackageManager;)V
    .locals 5
    .param p0, "packageManager"    # Landroid/content/pm/PackageManager;

    .prologue
    const/4 v4, 0x0

    .line 1030
    :try_start_0
    const-string v2, "com.samsung.helphub"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 1031
    .local v1, "info":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v2, v2, 0xa

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    .line 1032
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotionTutorialDialog:Z

    .line 1034
    :cond_0
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v2, v2, 0xa

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 1035
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOnlineHelp:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1041
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_1
    :goto_0
    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "chagall"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "lentis"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "tblte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "tr3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "KEW_jp_kdi"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "SCT21"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "tre3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "trlte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "trhlte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "patek"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "trhplte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "tbhplte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "tbelte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "patek"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "trelte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "klimt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "rubens"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "slte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "klte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "k3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "mega2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v3, "vasta"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "SC-01G"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "SCL24"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "a5"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1054
    :cond_2
    sput-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotionTutorialDialog:Z

    .line 1056
    :cond_3
    return-void

    .line 1037
    :catch_0
    move-exception v0

    .line 1038
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private static checkUseMoveFirstAlbum()V
    .locals 2

    .prologue
    .line 2468
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "KEW_jp_kdi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCT21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMoveFirstAlbum:Z

    .line 2469
    return-void

    .line 2468
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseMpo3DDisplay()V
    .locals 2

    .prologue
    .line 1004
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "u0lte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "u0wifi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMpo3DDisplay:Z

    .line 1005
    return-void

    .line 1004
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseMs01GUI()V
    .locals 2

    .prologue
    .line 2068
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ms01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMs01GUI:Z

    .line 2069
    return-void
.end method

.method private static checkUseMultiSIM()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1508
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCGGMultiSIM:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1509
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    .line 1512
    :cond_0
    const-string v0, "CTC"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klteduos"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1514
    :cond_1
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    .line 1516
    :cond_2
    return-void
.end method

.method private static checkUseMultiWindow(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1121
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1122
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-eqz v0, :cond_0

    .line 1123
    const-string v1, "com.sec.feature.multiwindow"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    .line 1125
    :cond_0
    return-void
.end method

.method public static checkUseMultiplier()V
    .locals 2

    .prologue
    .line 1654
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "santos"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "zestlteuc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "t0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "kona"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "goldenxx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "skomerxx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "m0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "d2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "c1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "m3xx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiplier:Z

    .line 1658
    return-void

    .line 1654
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseMyFilesTabletName()V
    .locals 2

    .prologue
    .line 2112
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDeviceType:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->TABLET:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "KEW_jp_kdi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCT21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2113
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMyFilesTabletName:Z

    .line 2115
    :cond_0
    return-void
.end method

.method private static checkUseNewAlbumDrop()V
    .locals 0

    .prologue
    .line 1368
    return-void
.end method

.method private static checkUseNewDragHere(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 1916
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1917
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "melius"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "hlte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "h3g"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "ha3g"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "SC-01F"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "SCL22"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "SC-02F"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "flte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewDragHere:Z

    .line 1919
    :cond_1
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWvga(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1920
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewDragHere:Z

    .line 1921
    :cond_2
    return-void

    .line 1917
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseNewFilterByHeaderForDarkTheme()V
    .locals 2

    .prologue
    .line 2219
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lentis"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewFilterByHeaderForDarkTheme:Z

    .line 2220
    return-void
.end method

.method private static checkUseNewOverlayHelp()V
    .locals 2

    .prologue
    .line 2529
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewOverlayHelp:Z

    .line 2533
    return-void

    .line 2529
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseNewQuickScroll()V
    .locals 2

    .prologue
    .line 2536
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseQuickScrollTypeWinset:Z

    .line 2540
    return-void

    .line 2536
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseNewSearchUI()V
    .locals 2

    .prologue
    .line 2457
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    .line 2461
    return-void

    .line 2457
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseOCR()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1153
    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "ks02"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1154
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    .line 1156
    :cond_0
    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "tblte"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "tre3g"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "tr3g"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "trlte"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "trhlte"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "trelte"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "trhplte"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "tbhplte"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "tbelte"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "SC-01G"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "SCL24"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCRPreloadService:Z

    .line 1160
    return-void
.end method

.method private static checkUseOMADrmSkip()V
    .locals 1

    .prologue
    .line 2091
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isKORFamily()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOMADrmSkip:Z

    .line 2092
    return-void
.end method

.method private static checkUseOfflineMenu()V
    .locals 0

    .prologue
    .line 1441
    return-void
.end method

.method private static checkUseOldEasyUX()V
    .locals 2

    .prologue
    .line 2057
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "s2ve"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "baffin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "baffinve"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "baffinlite"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "d2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "m0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "t0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "c1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOldEasyUx:Z

    .line 2059
    return-void

    .line 2057
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseOverlayHelp()V
    .locals 2

    .prologue
    .line 2563
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOverlayHelp:Z

    .line 2567
    return-void

    .line 2563
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUsePNGFullDecoding()V
    .locals 2

    .prologue
    .line 1638
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    .line 1639
    return-void
.end method

.method private static checkUsePSTouch()V
    .locals 0

    .prologue
    .line 1220
    return-void
.end method

.method private static checkUsePanoramaPlayBoost()V
    .locals 2

    .prologue
    .line 2194
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trcat6lte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2197
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePanoramaPlayBoost:Z

    .line 2199
    :cond_1
    return-void
.end method

.method private static checkUsePanoramaStopInRotate()V
    .locals 0

    .prologue
    .line 2202
    return-void
.end method

.method private static checkUsePencil()V
    .locals 0

    .prologue
    .line 1113
    return-void
.end method

.method private static checkUsePhotoEditor()V
    .locals 2

    .prologue
    .line 1259
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "delos3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cane"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoEditor:Z

    .line 1260
    return-void

    .line 1259
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUsePhotoEditorAtShareList()V
    .locals 0

    .prologue
    .line 1588
    return-void
.end method

.method private static checkUsePhotoReader()V
    .locals 2

    .prologue
    .line 2427
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoReader:Z

    .line 2431
    return-void

    .line 2427
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUsePhotoSignature(Landroid/content/pm/PackageManager;)V
    .locals 5
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 869
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v4, "hlte"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v4, "ha3g"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v4, "h3g"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v4, "SC-01F"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v4, "SCL22"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v4, "SC-02F"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v4, "flte"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoSignature:Z

    .line 873
    :try_start_0
    const-string v1, "com.sec.android.mimage.photoretouching.signature"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 877
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 869
    goto :goto_0

    .line 874
    :catch_0
    move-exception v0

    .line 875
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoSignature:Z

    goto :goto_1
.end method

.method private static checkUsePlugInDsdsService()V
    .locals 2

    .prologue
    .line 1000
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "serranods"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "delos3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs023gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePlugInDsdsService:Z

    .line 1001
    return-void

    .line 1000
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUsePopupMenuDialog()V
    .locals 1

    .prologue
    .line 2235
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePopupMenuDialog:Z

    .line 2236
    return-void
.end method

.method private static checkUsePositionControllerScale()V
    .locals 2

    .prologue
    .line 2464
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePositionControllerScale:Z

    .line 2465
    return-void
.end method

.method private static checkUsePremultipleAlphaBlend()V
    .locals 0

    .prologue
    .line 1010
    return-void
.end method

.method private static checkUseReodereredMenuForWQHD()V
    .locals 2

    .prologue
    .line 2385
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReodereredMenuForWQHD:Z

    .line 2389
    return-void

    .line 2385
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseResouceManager()V
    .locals 0

    .prologue
    .line 972
    return-void
.end method

.method private static checkUseRobotoLightFontForAlbumName()V
    .locals 2

    .prologue
    .line 2569
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseRobotoLightFontForAlbumName:Z

    .line 2570
    return-void
.end method

.method private static checkUseSCamDetails()V
    .locals 2

    .prologue
    .line 1354
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "gd1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "u0lte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "u0wifi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "flte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSCamDetails:Z

    .line 1357
    return-void

    .line 1354
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseSDL()V
    .locals 2

    .prologue
    .line 1277
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSDL:Z

    .line 1281
    return-void

    .line 1277
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static checkUseSPCDMR()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1643
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1644
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    .line 1645
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMRPinchZoom:Z

    .line 1647
    :cond_0
    return-void
.end method

.method private static checkUseSamsungLink(Landroid/content/pm/PackageManager;)V
    .locals 4
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 2158
    :try_start_0
    const-string v2, "com.samsung.android.sdk.samsunglink"

    const/16 v3, 0x4000

    invoke-virtual {p0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 2160
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v3, 0x3e8

    if-lt v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2164
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_1
    return-void

    .line 2160
    .restart local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 2161
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 2162
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->TAG:Ljava/lang/String;

    const-string v3, "SLPF isn\'t supported"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static checkUseSaveMMSImage()V
    .locals 2

    .prologue
    .line 1284
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "espresso"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSaveMMSImage:Z

    .line 1285
    return-void

    .line 1284
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseScaleDownEndEffect()V
    .locals 2

    .prologue
    .line 2333
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseScaleDownEndEffect:Z

    .line 2338
    return-void

    .line 2333
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseSecImaging()V
    .locals 2

    .prologue
    .line 2499
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecImaging:Z

    .line 2500
    return-void

    .line 2499
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseSecMMCodec()V
    .locals 1

    .prologue
    .line 2147
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-nez v0, :cond_0

    .line 2148
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecMMCodec:Z

    .line 2150
    :cond_0
    return-void
.end method

.method private static checkUseSelectAllForLimitationPickMode()V
    .locals 2

    .prologue
    .line 2448
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vasta"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "pateklteduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    .line 2454
    return-void

    .line 2448
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseSelectionAndSplitPinch()V
    .locals 2

    .prologue
    .line 744
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "KEW_jp_kdi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCT21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionAndSplitPinch:Z

    .line 750
    return-void

    .line 744
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseSelectionBar()V
    .locals 2

    .prologue
    .line 710
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    .line 715
    return-void

    .line 710
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseSelectionOnActionBar()V
    .locals 2

    .prologue
    .line 707
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    .line 708
    return-void

    .line 707
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseSequenceIcon()V
    .locals 2

    .prologue
    .line 2272
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "slte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "patek"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSequenceIconSupported:Z

    .line 2278
    return-void

    .line 2272
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseSettings()V
    .locals 1

    .prologue
    .line 1361
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1362
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSettings:Z

    .line 1364
    :cond_0
    return-void
.end method

.method private static checkUseShareLocationConfirmDialog()V
    .locals 2

    .prologue
    .line 1596
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    const-string v1, "VZW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseShareLocationConfirmDialog:Z

    .line 1597
    return-void
.end method

.method private static checkUseShowExpansionIcon()V
    .locals 2

    .prologue
    .line 2406
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseShowExpansionIcon:Z

    .line 2410
    return-void

    .line 2406
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseSideMirrorView()V
    .locals 2

    .prologue
    .line 836
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "flte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSideMirrorView:Z

    .line 837
    return-void
.end method

.method private static checkUseSimpleDetails()V
    .locals 2

    .prologue
    .line 2341
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSimpleDetails:Z

    .line 2345
    return-void

    .line 2341
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseSlideshowFilter()V
    .locals 1

    .prologue
    .line 2231
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowFilter:Z

    .line 2232
    return-void
.end method

.method private static checkUseSocialPhotoAlbum()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1675
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gduoszn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt033gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt03wifizc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ms013gzn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1677
    :cond_0
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialPhotoAlbum:Z

    .line 1681
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gduoszn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1683
    :cond_2
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsNoteModel:Z

    .line 1685
    :cond_3
    return-void
.end method

.method private static checkUseSocialTag()V
    .locals 1

    .prologue
    .line 1245
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1246
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag:Z

    .line 1248
    :cond_0
    return-void
.end method

.method private static checkUseSocialTag20()V
    .locals 1

    .prologue
    .line 1168
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 1169
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20:Z

    .line 1171
    :cond_0
    return-void
.end method

.method private static checkUseSocialTag20Chn()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1175
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gzn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gchnduoszn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "craterzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "craterzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gduoszn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "montblanc3gctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs023gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs023gzn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ms013gctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1180
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gduoszn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1181
    :cond_1
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20Chn:Z

    .line 1182
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20ChnWithWeChat:Z

    .line 1187
    :goto_0
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20:Z

    .line 1188
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag:Z

    .line 1189
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSnsSettings:Z

    .line 1192
    :cond_2
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1193
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20ChnWithWeChat:Z

    .line 1195
    :cond_3
    return-void

    .line 1184
    :cond_4
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20Chn:Z

    .line 1185
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20ChnWithWeChat:Z

    goto :goto_0
.end method

.method private static checkUseSoundScene()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1392
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "gd1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "delos3gzm"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "t0voltezm"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "espresso"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "baffin"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "baffinlite"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "cs02ve"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "s2ve"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "cs023gzc"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "cs023gzn"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "santos"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "lt02"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "skomerxx"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "lt06"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "tblte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "tre3g"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "trlte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "trhlte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "trelte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "trhplte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "tbhplte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "tr3g"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v2, "tbelte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "lentis"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "SC-01G"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "SCL24"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSoundScene:Z

    .line 1400
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSoundScene:Z

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    .line 1402
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "baffinvetd3gzm"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v2, "baffinvetd3gzc"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1403
    :cond_3
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSoundScene:Z

    .line 1405
    :cond_4
    return-void

    .line 1392
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static checkUseStoryAlbum(Landroid/content/pm/PackageManager;)V
    .locals 4
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    const/4 v3, 0x0

    .line 923
    :try_start_0
    const-string v1, "com.samsung.android.app.episodes"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 924
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseStoryAlbum:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 931
    :cond_0
    :goto_0
    return-void

    .line 926
    :catch_0
    move-exception v0

    .line 927
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseStoryAlbum:Z

    goto :goto_0

    .line 928
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 929
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static checkUseTCloud(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1492
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    const-string v1, "SKC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    const-string v1, "SKT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1493
    :cond_0
    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->isTCloudAgentExist(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloudMandatory:Z

    .line 1494
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloudMandatory:Z

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->existsAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    .line 1496
    :cond_1
    return-void

    .line 1494
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseTDataCache()V
    .locals 2

    .prologue
    .line 1520
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "gd1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTDataCache:Z

    .line 1521
    return-void
.end method

.method private static checkUseTapMenu()V
    .locals 0

    .prologue
    .line 1199
    return-void
.end method

.method private static checkUseTencentDrag()V
    .locals 2

    .prologue
    .line 1417
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jaltezm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jfltezm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jfltecsfbzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gchnduoszn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3gzn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gduoszn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3gduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "craterzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "craterzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cratertd3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "melius3gduosctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "melius3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "melius3gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt023gctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "montblanc3gctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ms013gctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTencentDrag:Z

    .line 1425
    return-void

    .line 1417
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseTileAlphaBlending()V
    .locals 1

    .prologue
    .line 2141
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-nez v0, :cond_0

    .line 2142
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    .line 2144
    :cond_0
    return-void
.end method

.method private static checkUseTouchPrediction()V
    .locals 2

    .prologue
    .line 2371
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "slte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTouchPrediction:Z

    .line 2375
    return-void

    .line 2371
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseTrimFeature(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 840
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.TRIM"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 841
    .local v0, "videoTrimIntent":Landroid/content/Intent;
    const-string v1, "com.lifevibes.trimapp"

    invoke-static {p0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkPackageExist(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 842
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNXPTrimApp:Z

    .line 844
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 845
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTrimAvailability:Z

    .line 847
    :cond_1
    return-void
.end method

.method private static checkUseTvOut()V
    .locals 0

    .prologue
    .line 894
    return-void
.end method

.method private static checkUseUMS()V
    .locals 0

    .prologue
    .line 1208
    return-void
.end method

.method private static checkUseVZW_ContextualTagPopUp()V
    .locals 2

    .prologue
    .line 1534
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    const-string v1, "VZW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVZW_ContextualTagPopUp:Z

    .line 1535
    return-void
.end method

.method private static checkUseVZW_DefaultWidget()V
    .locals 2

    .prologue
    .line 1574
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    const-string v1, "VZW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVZW_DefaultWidget:Z

    .line 1575
    return-void
.end method

.method private static checkUseVZW_MotionDialogText()V
    .locals 2

    .prologue
    .line 1529
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    const-string v1, "VZW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVZW_MotionDialogText:Z

    .line 1530
    return-void
.end method

.method private static checkUseVZW_NoItemBubble()V
    .locals 2

    .prologue
    .line 1539
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    const-string v1, "VZW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVZW_NoItemBubble:Z

    .line 1540
    return-void
.end method

.method private static checkUseVideoCallCrop()V
    .locals 1

    .prologue
    .line 2289
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isKORFamily()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoCallCrop:Z

    .line 2290
    return-void
.end method

.method public static checkUseVideoClip(Landroid/content/pm/PackageManager;)V
    .locals 4
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    const/4 v3, 0x0

    .line 936
    :try_start_0
    const-string v1, "com.sec.android.app.storycam"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 937
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoClip:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 946
    :cond_0
    :goto_0
    return-void

    .line 939
    :catch_0
    move-exception v0

    .line 940
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoClip:Z

    .line 941
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 942
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 943
    .local v0, "e":Ljava/lang/Exception;
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoClip:Z

    .line 944
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static checkUseVideoWall()V
    .locals 0

    .prologue
    .line 1256
    return-void
.end method

.method private static checkUseWaitForShrinkAnimationUntilDecodingCompleted()V
    .locals 2

    .prologue
    .line 2583
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWaitForShrinkAnimationUntilDecodingCompleted:Z

    .line 2584
    return-void
.end method

.method private static checkUseWeatherNews()V
    .locals 2

    .prologue
    .line 1579
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "JP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1580
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWeatherNews:Z

    .line 1584
    :goto_0
    return-void

    .line 1582
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWeatherNews:Z

    goto :goto_0
.end method

.method private static checkUseWhiteTheme()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 908
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 909
    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "chagall"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "klimt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "rubens"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "KEW_jp_kdi"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "SCT21"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 910
    :cond_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    .line 918
    :goto_0
    return-void

    .line 912
    :cond_1
    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    goto :goto_0

    .line 915
    :cond_2
    const-string v2, "ro.build.scafe.cream"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "white"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "a5"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "k3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v3, "klte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    goto :goto_0
.end method

.method private static checkUseWidget(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2239
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2240
    .local v1, "pm":Landroid/content/pm/PackageManager;
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.sec.android.gallery3d.gadget.PhotoAppWidgetProvider"

    invoke-direct {v0, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2245
    .local v0, "cn":Landroid/content/ComponentName;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMagazineWidget:Z

    if-eqz v2, :cond_0

    .line 2246
    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 2248
    :cond_0
    return-void
.end method

.method private static checkUseWritingPanel()V
    .locals 2

    .prologue
    .line 2077
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWritingPanel:Z

    .line 2082
    return-void

    .line 2077
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseXiv()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1085
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "KEW_jp_kdi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCT21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "mega2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "vasta"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1089
    :cond_0
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    .line 1090
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivWSTN:Z

    .line 1091
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivTileReuse:Z

    .line 1093
    :cond_1
    return-void
.end method

.method private static checkUseXivFilledReusePoolMode()V
    .locals 2

    .prologue
    .line 1563
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klimt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "KEW_jp_kdi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCT21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivFilledReusePoolMode:Z

    .line 1565
    return-void

    .line 1563
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseXivStopRDWhileZoomin()V
    .locals 2

    .prologue
    .line 1097
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "delos3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "crater"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs05"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "santos"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs023gzc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs023gzn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs02ve"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1099
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivStopRDWhileZoomin:Z

    .line 1101
    :cond_1
    return-void
.end method

.method private static checkUsemUseDialogWithMessage()V
    .locals 2

    .prologue
    .line 1263
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tr3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDialogWithMessage:Z

    .line 1267
    return-void

    .line 1263
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUsemUseEmptyScreenGalleryIcon()V
    .locals 2

    .prologue
    .line 2215
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEmptyScreenGalleryIcon:Z

    .line 2216
    return-void

    .line 2215
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkVersionOfContextProvider(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 795
    const/4 v2, -0x1

    .line 797
    .local v2, "version":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.android.providers.context"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 799
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 803
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mCheckVersionOfContextProvider:Z

    .line 804
    return-void

    .line 800
    :catch_0
    move-exception v0

    .line 801
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->TAG:Ljava/lang/String;

    const-string v4, "[SW] Could not find ContextProvider"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 803
    .end local v0    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static checkWhiteTagBuddy()V
    .locals 2

    .prologue
    .line 2086
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "lt03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTagBuddy:Z

    .line 2088
    return-void

    .line 2086
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkmUseDownloadableHelp()V
    .locals 4

    .prologue
    .line 662
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v1

    const-string v2, "SEC_FLOATING_FEATURE_HELP_HUB_APK_TYPE"

    const-string v3, "preload"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 664
    .local v0, "apkType":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Helphub apkType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const-string v1, "downloadable"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 666
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    .line 668
    :cond_0
    return-void
.end method

.method private static checkmUseFilterBG()V
    .locals 2

    .prologue
    .line 2558
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "KEW_jp_kdi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCT21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFilterBG:Z

    .line 2560
    return-void

    .line 2558
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized init(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 410
    const-class v2, Lcom/sec/android/gallery3d/util/GalleryFeature;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsInited:Z

    if-nez v1, :cond_0

    .line 411
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsInited:Z

    .line 416
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 418
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDeviceType(Landroid/content/Context;)V

    .line 419
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkCscFeature()V

    .line 420
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkSystemUiVisibility(Landroid/content/Context;)V

    .line 421
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkSystemProperty()V

    .line 426
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseMotion(Landroid/content/Context;)V

    .line 427
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseMotionTutorialDialog(Landroid/content/pm/PackageManager;)V

    .line 428
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseFaceTag()V

    .line 429
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseFaceThumbnail()V

    .line 430
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseDocumentClassifier()V

    .line 431
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseFaceBook()V

    .line 432
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseXiv()V

    .line 433
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDisableUseXivLargeThumbnail()V

    .line 434
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDisableUseResizeLargeThumbnail()V

    .line 435
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseXivFilledReusePoolMode()V

    .line 436
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseXivStopRDWhileZoomin()V

    .line 437
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePencil()V

    .line 438
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseAsyncRender()V

    .line 439
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseMultiWindow(Landroid/content/Context;)V

    .line 440
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseContextualAwareness()V

    .line 441
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseAirJump()V

    .line 442
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseAirBrowse()V

    .line 443
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseHoveringUI()V

    .line 444
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseOCR()V

    .line 445
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkHasOCR()V

    .line 446
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSocialTag20()V

    .line 447
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSocialTag20Chn()V

    .line 448
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseTapMenu()V

    .line 449
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseLaunchTranslucent()V

    .line 450
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseUMS()V

    .line 451
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkIsWifiOnlyModel(Landroid/content/Context;)V

    .line 452
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePSTouch()V

    .line 453
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseAccountSettings()V

    .line 454
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUse3DViewMode(Landroid/content/Context;)V

    .line 455
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCubeDropEffect(Landroid/content/Context;)V

    .line 456
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSocialTag()V

    .line 457
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseDCIMFolderMerge()V

    .line 458
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseVideoWall()V

    .line 459
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseGroupId(Landroid/content/Context;)V

    .line 460
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSCamDetails()V

    .line 461
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSettings()V

    .line 462
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseNewAlbumDrop()V

    .line 463
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUse3DPanorama()V

    .line 464
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSoundScene()V

    .line 465
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseHWKey()V

    .line 466
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseTencentDrag()V

    .line 467
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkNearbyDemoForSProject()V

    .line 468
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseFilmStripSelectionMode()V

    .line 469
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseLocationInfo()V

    .line 470
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseOfflineMenu()V

    .line 471
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkMmsEnabled()V

    .line 472
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDisableMenuSeaAsContact()V

    .line 473
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDisableMenuShareViaMsg()V

    .line 474
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkAddAutoRotationIcon()V

    .line 475
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkIsCHN()V

    .line 476
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseBaidu()V

    .line 477
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseFestival()V

    .line 478
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseChnUsageAlertPopup()V

    .line 479
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCloud(Landroid/content/Context;)V

    .line 480
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkEnableOrangeCloudOnTopInShareVia()V

    .line 481
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseTCloud(Landroid/content/Context;)V

    .line 482
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkIsSPCEnabled()V

    .line 483
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseDualSIM()V

    .line 484
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseMultiSIM()V

    .line 485
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseTDataCache()V

    .line 486
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkFaceTagDefaultValue()V

    .line 487
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseVZW_MotionDialogText()V

    .line 488
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseVZW_ContextualTagPopUp()V

    .line 489
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseVZW_NoItemBubble()V

    .line 490
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseATT_DateTag()V

    .line 491
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseWeatherNews()V

    .line 492
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseAccuWeather()V

    .line 493
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePNGFullDecoding()V

    .line 494
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePhotoEditorAtShareList()V

    .line 495
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseShareLocationConfirmDialog()V

    .line 496
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCameraButtonAtDetailView()V

    .line 497
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseATT_PhotoEditTilteName()V

    .line 498
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseATT_DefaultWidget()V

    .line 499
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseVZW_DefaultWidget()V

    .line 500
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDisableShowMap()V

    .line 501
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseChnCharacter()V

    .line 502
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseMultiplier()V

    .line 503
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkCMCCModel()V

    .line 504
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDefaultRotationPhoneType()V

    .line 505
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSPCDMR()V

    .line 506
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseAdaptDisplayDialog()V

    .line 507
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseResouceManager()V

    .line 508
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseStoryAlbum(Landroid/content/pm/PackageManager;)V

    .line 509
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCollage(Landroid/content/Context;)V

    .line 510
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseVideoClip(Landroid/content/pm/PackageManager;)V

    .line 511
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseWhiteTheme()V

    .line 512
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseLowAnimation()V

    .line 513
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePlugInDsdsService()V

    .line 514
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseHelp()V

    .line 515
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseMobilePrint(Landroid/content/pm/PackageManager;)V

    .line 516
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseMpo3DDisplay()V

    .line 517
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePremultipleAlphaBlend()V

    .line 518
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePhotoEditor()V

    .line 519
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsemUseDialogWithMessage()V

    .line 520
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseMagicShotIconEnter()V

    .line 521
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSDL()V

    .line 522
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseTvOut()V

    .line 523
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseEVF()V

    .line 524
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCmcc()V

    .line 525
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseNewDragHere(Landroid/content/Context;)V

    .line 526
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePhotoSignature(Landroid/content/pm/PackageManager;)V

    .line 527
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCreateAGIF(Landroid/content/pm/PackageManager;)V

    .line 528
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDisableHwAccelerationforImgaeNote()V

    .line 529
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkIsSupportWFD(Landroid/content/Context;)Z

    .line 531
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkFindoSearch(Landroid/content/pm/PackageManager;)V

    .line 532
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkEnableAirButton()V

    .line 533
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkEnableTimeViewCache()V

    .line 534
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkEnableAutoScrollTimeView()V

    .line 535
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkEnableBurstShotSettingIcon()V

    .line 536
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkEnableFixScrollingAlbum()V

    .line 537
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkEnableAlbumGrouping()V

    .line 538
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkIsSupportSoundAndShot()V

    .line 539
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSaveMMSImage()V

    .line 540
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkEnableSecretBox(Landroid/content/Context;)V

    .line 541
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDisableHide()V

    .line 542
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkEnableNote(Landroid/content/Context;)V

    .line 543
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkEnableCreateVideoAlbum()V

    .line 544
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDragNDropInMultiwindow()V

    .line 545
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkBaiduCloudDragDrop()V

    .line 546
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkHideChangePlayerText()V

    .line 547
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseOldEasyUX()V

    .line 548
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkEnableEditBurstShot()V

    .line 549
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDisableTileAlphablending()V

    .line 550
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseDCM(Landroid/content/Context;)V

    .line 551
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseDCMRelatedCategory()V

    .line 552
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseHomeAsUpFromSetting()V

    .line 553
    invoke-static {p0}, Lcom/sec/samsung/gallery/core/TabTagType;->initializeEnum(Landroid/content/Context;)V

    .line 554
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkPortraitTablet()V

    .line 555
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseTrimFeature(Landroid/content/Context;)V

    .line 556
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSocialPhotoAlbum()V

    .line 557
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSideMirrorView()V

    .line 558
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseWritingPanel()V

    .line 559
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkWhiteTagBuddy()V

    .line 560
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseOMADrmSkip()V

    .line 561
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseFLDRMOnly()V

    .line 562
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseMFDMenu()V

    .line 563
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseMyFilesTabletName()V

    .line 564
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkKNOXUse(Landroid/content/Context;)V

    .line 565
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseManualMulticore()V

    .line 566
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseGapLand()V

    .line 567
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseMs01GUI()V

    .line 568
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseTileAlphaBlending()V

    .line 569
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSecMMCodec()V

    .line 570
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSamsungLink(Landroid/content/pm/PackageManager;)V

    .line 571
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkVersionOfContextProvider(Landroid/content/Context;)V

    .line 572
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseGallerySearchUISeperatedMode()V

    .line 573
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseBaiduPositioning()V

    .line 574
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkIsGuestMode(Landroid/content/Context;)V

    .line 575
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCameraSwitchBoost()V

    .line 576
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePanoramaPlayBoost()V

    .line 577
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePanoramaStopInRotate()V

    .line 578
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseFileInfoManager()V

    .line 579
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseDualBooster()V

    .line 580
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseEmptyScreenPopup()V

    .line 581
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsemUseEmptyScreenGalleryIcon()V

    .line 582
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkIsTwHk()V

    .line 583
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkIsJPN()V

    .line 584
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseLongPressAlbums()V

    .line 585
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSlideshowFilter()V

    .line 586
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePopupMenuDialog()V

    .line 587
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseWidget(Landroid/content/Context;)V

    .line 588
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkSlideSettingPopup()V

    .line 589
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSelectionOnActionBar()V

    .line 590
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSelectionBar()V

    .line 591
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSequenceIcon()V

    .line 592
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseGolfIcon()V

    .line 593
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkIsDecorAlwaysVisible()V

    .line 594
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUse3DTour()V

    .line 595
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseFonbletPictureFrame()V

    .line 596
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseNewFilterByHeaderForDarkTheme()V

    .line 597
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseDisableScrollBar()V

    .line 598
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseVideoCallCrop()V

    .line 599
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseLocationActionbarPanel()V

    .line 600
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseLoadTask()V

    .line 601
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkSlideshowVideoPlay()V

    .line 602
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCustomMaxZoomLevel()V

    .line 603
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSelectionAndSplitPinch()V

    .line 604
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseFlexibleCloudAlbumFormat()V

    .line 605
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseAirViewActionbar()V

    .line 606
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseFlashAnnotate(Landroid/content/Context;)V

    .line 607
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseDefaultAlbumView()V

    .line 608
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseLowMemoryCheckDialog()V

    .line 609
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkIsVZW()V

    .line 610
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCircleFace()V

    .line 611
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseScaleDownEndEffect()V

    .line 612
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSimpleDetails()V

    .line 613
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCategoryViewDefault()V

    .line 614
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCreateGif()V

    .line 615
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseDragAndDropInExpandSplitView()V

    .line 616
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseTouchPrediction()V

    .line 617
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCustomFilterBy()V

    .line 618
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseReodereredMenuForWQHD()V

    .line 619
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseDraqHereTextColorForWQHD()V

    .line 620
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDisablePinchZoomWhenPointersOutOfThumbnail()V

    .line 621
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseShowExpansionIcon()V

    .line 622
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseLargeLoadingCount()V

    .line 623
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkNoUseBackIconInSelection()V

    .line 624
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePhotoReader()V

    .line 625
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkNoUseSortbyPhotoView()V

    .line 626
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseAddTagAsMoreinfo()V

    .line 627
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSelectAllForLimitationPickMode()V

    .line 628
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseNewSearchUI()V

    .line 629
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkEasymodeHelp()V

    .line 630
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUsePositionControllerScale()V

    .line 631
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseMoveFirstAlbum()V

    .line 632
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseHaptic()V

    .line 633
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseAddTagSelectionDropDown()V

    .line 634
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseLargeThumbnail()V

    .line 635
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkNoUseMovetoAlbumInTimeview()V

    .line 636
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseSecImaging()V

    .line 637
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseFlingInTalkBack()V

    .line 638
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseEditShowInCategory()V

    .line 639
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkNoUseCopytoClipboardInDetailview()V

    .line 640
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkCropInTB()V

    .line 641
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkIsMultiSIM()V

    .line 642
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseNewOverlayHelp()V

    .line 643
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseNewQuickScroll()V

    .line 644
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseChangeThumb()V

    .line 645
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseFunctionSimplification()V

    .line 646
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseIdSortByASC()V

    .line 647
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDisableStudioShowAsAction()V

    .line 648
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDisableEditShowAsAction()V

    .line 649
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkmUseFilterBG()V

    .line 650
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseOverlayHelp()V

    .line 651
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseRobotoLightFontForAlbumName()V

    .line 652
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseDelayedDnieSetting()V

    .line 653
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseDrawerIconAlpha()V

    .line 654
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkDisplaySelectAllButtonInLimitationPickMode()V

    .line 655
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseWaitForShrinkAnimationUntilDecodingCompleted()V

    .line 656
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkmUseDownloadableHelp()V

    .line 657
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseCameraShotAndMore()V

    .line 658
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkGlTextViewCanvas()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 659
    .end local v0    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_0
    monitor-exit v2

    return-void

    .line 410
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static isATTFamily()Z
    .locals 2

    .prologue
    .line 1613
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    const-string v1, "ATT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    const-string v1, "AIO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBaffinRDEnabled()Z
    .locals 2

    .prologue
    .line 1758
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1759
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "baffinrdzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1760
    const/4 v0, 0x1

    .line 1763
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCS02Enabled()Z
    .locals 2

    .prologue
    .line 1740
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1741
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs02"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1742
    const/4 v0, 0x1

    .line 1745
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCS03lteEnabled()Z
    .locals 2

    .prologue
    .line 1749
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1750
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cs03ltezm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1751
    const/4 v0, 0x1

    .line 1754
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCaneEnabled()Z
    .locals 2

    .prologue
    .line 1800
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1801
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "cane"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1802
    const/4 v0, 0x1

    .line 1805
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCaneExtraHapticEnabled(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1900
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p0, :cond_1

    .line 1901
    :cond_0
    const/4 v0, 0x0

    .line 1902
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getHapticExtraFeedback(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isDeleteIconDisabled()Z
    .locals 2

    .prologue
    .line 1700
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1701
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jflte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jalte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "serrano"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jactive"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-04E"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jftdd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "flte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "loganreltedv"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "m3dv"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "loganreltexx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "mprojectqltexx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "wilcoxltexx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1708
    :cond_0
    const/4 v0, 0x1

    .line 1711
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isEspresso10Enabled()Z
    .locals 2

    .prologue
    .line 1818
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1819
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "espresso10"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1820
    const/4 v0, 0x1

    .line 1823
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isEspressoEnabled()Z
    .locals 2

    .prologue
    .line 1809
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1810
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "espresso"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1811
    const/4 v0, 0x1

    .line 1814
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isGyroSensorAvailable(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1870
    const-string v2, "sensor"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    .line 1871
    .local v1, "sensorMgr":Landroid/hardware/SensorManager;
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    .line 1872
    .local v0, "gyroSensors":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1873
    const/4 v2, 0x1

    .line 1875
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isJEnabled()Z
    .locals 2

    .prologue
    .line 1688
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1689
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jflte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jalte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ja3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "serrano"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jactive"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "SC-04E"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "jftdd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1693
    :cond_0
    const/4 v0, 0x1

    .line 1696
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKORFamily()Z
    .locals 2

    .prologue
    .line 1618
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    const-string v1, "SKT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    const-string v1, "KTT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSalesCode:Ljava/lang/String;

    const-string v1, "LGT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mCountryCode:Ljava/lang/String;

    const-string v1, "KOREA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMEnabled()Z
    .locals 2

    .prologue
    .line 1827
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDeviceType:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->CAMERA_PHONE:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMs013gzmEnabled()Z
    .locals 2

    .prologue
    .line 1782
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1783
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "ms013gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1784
    const/4 v0, 0x1

    .line 1787
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSCAMEnabled()Z
    .locals 2

    .prologue
    .line 1852
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDeviceType:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->CAMERA:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSantosEnabled()Z
    .locals 2

    .prologue
    .line 1791
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1792
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "santos"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1793
    const/4 v0, 0x1

    .line 1796
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTablet()Z
    .locals 2

    .prologue
    .line 1856
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDeviceType:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->TABLET:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWilcoxds3gzmEnabled()Z
    .locals 2

    .prologue
    .line 1774
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1775
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "wilcox3gzm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1776
    const/4 v0, 0x1

    .line 1779
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWilcoxdsznEnabled()Z
    .locals 2

    .prologue
    .line 1766
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1767
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string/jumbo v1, "wilcoxdszn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1768
    const/4 v0, 0x1

    .line 1771
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWrongOrientationFromCamera()Z
    .locals 2

    .prologue
    .line 1887
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    const-string v1, "rubens"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1888
    const/4 v0, 0x1

    .line 1890
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static printFeatureInfoList(Landroid/content/pm/PackageManager;)V
    .locals 8
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 1907
    invoke-virtual {p0}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v2

    .line 1908
    .local v2, "fiList":[Landroid/content/pm/FeatureInfo;
    if-eqz v2, :cond_0

    array-length v5, v2

    if-nez v5, :cond_1

    .line 1913
    :cond_0
    return-void

    .line 1910
    :cond_1
    move-object v0, v2

    .local v0, "arr$":[Landroid/content/pm/FeatureInfo;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 1911
    .local v1, "fi":Landroid/content/pm/FeatureInfo;
    sget-object v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Feature Info : ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1910
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
