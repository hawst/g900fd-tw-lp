.class public Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "TCloudAlbum.java"


# static fields
.field private static final COUNT_PROJECTION:[Ljava/lang/String;

.field private static final INVALID_COUNT:I = -0x1

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private final mBaseUri:Landroid/net/Uri;

.field private final mBucketId:I

.field private final mBucketIdString:Ljava/lang/String;

.field private final mBucketName:Ljava/lang/String;

.field private mCachedCount:I

.field private final mIsImage:Z

.field private final mItemPath:Lcom/sec/android/gallery3d/data/Path;

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mOrderClause:Ljava/lang/String;

.field private final mProjection:[Ljava/lang/String;

.field private final mResolver:Landroid/content/ContentResolver;

.field private final mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

.field private final mWhereClause:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    const-class v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->TAG:Ljava/lang/String;

    .line 45
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "count(*)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)V
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "bucketId"    # I
    .param p4, "isImage"    # Z
    .param p5, "refer"    # Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .prologue
    .line 88
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p3, p5}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->getBucketName(Landroid/content/ContentResolver;ILcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p3, p5}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->getBucketIdString(Landroid/content/ContentResolver;ILcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLjava/lang/String;Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "bucketId"    # I
    .param p4, "isImage"    # Z
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "bucketIdString"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mCachedCount:I

    .line 64
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .line 65
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mResolver:Landroid/content/ContentResolver;

    .line 66
    iput p3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBucketId:I

    .line 67
    iput-object p5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBucketName:Ljava/lang/String;

    .line 68
    iput-boolean p4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mIsImage:Z

    .line 69
    iput-object p6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBucketIdString:Ljava/lang/String;

    .line 71
    if-eqz p4, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->getTCloudImageProjection()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mProjection:[Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_BUCKET_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mWhereClause:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_DATE_MODIFIED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " desc"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mOrderClause:Ljava/lang/String;

    .line 75
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseImageUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBaseUri:Landroid/net/Uri;

    .line 76
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 84
    :goto_0
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBaseUri:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 85
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->getTCloudVideoProjection()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mProjection:[Ljava/lang/String;

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_BUCKET_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mWhereClause:Ljava/lang/String;

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_DATE_MODIFIED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " desc"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mOrderClause:Ljava/lang/String;

    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseVideoUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBaseUri:Landroid/net/Uri;

    .line 82
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0
.end method

.method public static getItemCursor(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 6
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "id"    # I

    .prologue
    .line 287
    const-string v3, "_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getMediaItemById(Lcom/sec/android/gallery3d/app/GalleryApp;ZLjava/util/ArrayList;Landroid/net/Uri;[Ljava/lang/String;)[Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 19
    .param p0, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p1, "isImage"    # Z
    .param p3, "baseUri"    # Landroid/net/Uri;
    .param p4, "projection"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/GalleryApp;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            ")[",
            "Lcom/sec/android/gallery3d/data/MediaItem;"
        }
    .end annotation

    .prologue
    .line 158
    .local p2, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    .line 160
    .local v18, "result":[Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 214
    :goto_0
    return-object v18

    .line 162
    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 163
    .local v14, "idLow":I
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 167
    .local v13, "idHigh":I
    if-eqz p1, :cond_1

    .line 168
    sget-object v16, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 173
    .local v16, "itemPath":Lcom/sec/android/gallery3d/data/Path;
    :goto_1
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 174
    .local v2, "resolver":Landroid/content/ContentResolver;
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v10

    .line 176
    .local v10, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v9, 0x0

    .line 178
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v5, "_id BETWEEN ? AND ?"

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    const/4 v3, 0x1

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    const-string v7, "_id"

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 182
    if-nez v9, :cond_2

    .line 183
    sget-object v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->TAG:Ljava/lang/String;

    const-string v4, "query fail"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 170
    .end local v2    # "resolver":Landroid/content/ContentResolver;
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v10    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v16    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    sget-object v16, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .restart local v16    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    goto :goto_1

    .line 187
    .restart local v2    # "resolver":Landroid/content/ContentResolver;
    .restart local v9    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :cond_2
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 188
    .local v17, "n":I
    const/4 v11, 0x0

    .line 189
    .local v11, "i":I
    :cond_3
    :goto_2
    move/from16 v0, v17

    if-ge v11, v0, :cond_7

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 191
    const/4 v3, 0x0

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 193
    .local v12, "id":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-gt v3, v12, :cond_3

    .line 197
    :cond_4
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ge v3, v12, :cond_6

    .line 198
    add-int/lit8 v11, v11, 0x1

    move/from16 v0, v17

    if-lt v11, v0, :cond_4

    .line 199
    if-eqz v9, :cond_5

    .line 200
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 201
    :cond_5
    const/4 v9, 0x0

    .line 212
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 205
    :cond_6
    :try_start_2
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    .line 206
    .local v8, "childPath":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v8, v9, v10, v0, v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v15

    .line 208
    .local v15, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    aput-object v15, v18, v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 209
    add-int/lit8 v11, v11, 0x1

    .line 210
    goto :goto_2

    .line 212
    .end local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v12    # "id":I
    .end local v15    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_7
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .end local v11    # "i":I
    .end local v17    # "n":I
    :catchall_0
    move-exception v3

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3
.end method

.method private static loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p4, "isImage"    # Z

    .prologue
    .line 142
    invoke-virtual {p2, p0}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;

    .line 143
    .local v0, "item":Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;
    if-nez v0, :cond_1

    .line 144
    if-eqz p4, :cond_0

    .line 145
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    .end local v0    # "item":Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;
    invoke-direct {v0, p0, p3, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .line 152
    .restart local v0    # "item":Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;
    :goto_0
    return-object v0

    .line 147
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    .end local v0    # "item":Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;
    invoke-direct {v0, p0, p3, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .restart local v0    # "item":Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;
    goto :goto_0

    .line 150
    :cond_1
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->updateContent(Landroid/database/Cursor;)V

    goto :goto_0
.end method


# virtual methods
.method public delete()V
    .locals 5

    .prologue
    .line 270
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 271
    const/4 v0, -0x1

    .line 272
    .local v0, "mediaType":I
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mIsImage:Z

    if-eqz v2, :cond_0

    .line 273
    const/4 v0, 0x1

    .line 278
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Files_BUCKET_ID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBucketId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Files_MEDIA_TYPE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 281
    .local v1, "where":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseFileUri:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 283
    return-void

    .line 275
    .end local v1    # "where":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public getBucketId()I
    .locals 1

    .prologue
    .line 249
    iget v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBucketId:I

    return v0
.end method

.method public getBucketIdString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBucketIdString:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 13
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 95
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 96
    .local v8, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 98
    .local v1, "uri":Landroid/net/Uri;
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMediaItem:start:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",count:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",uri:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 100
    .local v11, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-gez p2, :cond_0

    .line 101
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "count is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :goto_0
    return-object v11

    .line 105
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 107
    const/4 v7, 0x0

    .line 109
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mBucketId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBucketId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v0

    if-ne v0, v5, :cond_1

    .line 111
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mOrderClause:Ljava/lang/String;

    const-string v2, "desc"

    const-string v3, "asc"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mOrderClause:Ljava/lang/String;

    .line 115
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mProjection:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mWhereClause:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget v12, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBucketId:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mOrderClause:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 119
    if-nez v7, :cond_2

    .line 120
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->TAG:Ljava/lang/String;

    const-string v2, "query fail: "

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 113
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mOrderClause:Ljava/lang/String;

    const-string v2, "asc"

    const-string v3, "desc"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mOrderClause:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 135
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 123
    :cond_2
    :goto_2
    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 124
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 125
    .local v9, "id":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 126
    .local v6, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mIsImage:Z

    invoke-static {v6, v7, v8, v0, v2}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v10

    .line 128
    .local v10, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v10, :cond_2

    .line 131
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 135
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v9    # "id":I
    .end local v10    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method

.method public getMediaItemCount()I
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 219
    iget v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mCachedCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 220
    const/4 v6, 0x0

    .line 222
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBaseUri:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mWhereClause:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget v9, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBucketId:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 226
    if-nez v6, :cond_0

    .line 227
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->TAG:Ljava/lang/String;

    const-string v1, "query fail"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v8

    .line 239
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v0

    .line 230
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 231
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mCachedCount:I

    .line 232
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mCachedCount:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mCachedCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 239
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    :goto_1
    iget v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mCachedCount:I

    goto :goto_0

    .line 233
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 234
    .local v7, "e":Ljava/lang/IllegalStateException;
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 236
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mBucketName:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 254
    const-wide/16 v0, 0x400

    .line 256
    .local v0, "supported":J
    return-wide v0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    invoke-static {}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mDataVersion:J

    .line 263
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mCachedCount:I

    .line 265
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->mDataVersion:J

    return-wide v0
.end method
