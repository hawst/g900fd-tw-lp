.class Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;
.super Ljava/lang/Object;
.source "UriImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/UriImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BitmapJob"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private mType:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/data/UriImage;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/data/UriImage;I)V
    .locals 0
    .param p2, "type"    # I

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234
    iput p2, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->mType:I

    .line 235
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 239
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    # invokes: Lcom/sec/android/gallery3d/data/UriImage;->prepareInputFile(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z
    invoke-static {v4, p1}, Lcom/sec/android/gallery3d/data/UriImage;->access$100(Lcom/sec/android/gallery3d/data/UriImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z

    move-result v4

    if-nez v4, :cond_1

    move-object v0, v3

    .line 273
    :cond_0
    :goto_0
    return-object v0

    .line 240
    :cond_1
    iget v4, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->mType:I

    invoke-static {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v2

    .line 241
    .local v2, "targetSize":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    # getter for: Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;
    invoke-static {v4}, Lcom/sec/android/gallery3d/data/UriImage;->access$600(Lcom/sec/android/gallery3d/data/UriImage;)Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    # getter for: Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;
    invoke-static {v4}, Lcom/sec/android/gallery3d/data/UriImage;->access$600(Lcom/sec/android/gallery3d/data/UriImage;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "content://mms/part/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->mType:I

    if-ne v4, v6, :cond_2

    .line 242
    mul-int/lit8 v2, v2, 0x2

    .line 244
    :cond_2
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 245
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v4, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 246
    const/4 v0, 0x0

    .line 247
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    iget-boolean v4, v4, Lcom/sec/android/gallery3d/data/UriImage;->isdrm:Z

    if-eqz v4, :cond_5

    .line 248
    iput-boolean v6, v1, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 249
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/UriImage;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4, v1}, Lcom/sec/android/gallery3d/data/DecodeUtils;->requestDecode(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 258
    :cond_3
    :goto_1
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_4

    if-nez v0, :cond_7

    :cond_4
    move-object v0, v3

    .line 259
    goto :goto_0

    .line 250
    :cond_5
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    # getter for: Lcom/sec/android/gallery3d/data/UriImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;
    invoke-static {v4}, Lcom/sec/android/gallery3d/data/UriImage;->access$300(Lcom/sec/android/gallery3d/data/UriImage;)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    if-nez v4, :cond_6

    .line 251
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    # getter for: Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;
    invoke-static {v4}, Lcom/sec/android/gallery3d/data/UriImage;->access$600(Lcom/sec/android/gallery3d/data/UriImage;)Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 252
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    # getter for: Lcom/sec/android/gallery3d/data/UriImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v4}, Lcom/sec/android/gallery3d/data/UriImage;->access$200(Lcom/sec/android/gallery3d/data/UriImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    # getter for: Lcom/sec/android/gallery3d/data/UriImage;->mUri:Landroid/net/Uri;
    invoke-static {v5}, Lcom/sec/android/gallery3d/data/UriImage;->access$600(Lcom/sec/android/gallery3d/data/UriImage;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {p1, v4, v5, v1, v2}, Lcom/sec/android/gallery3d/data/DecodeUtils;->requestDecode(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/graphics/BitmapFactory$Options;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 254
    :cond_6
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    # getter for: Lcom/sec/android/gallery3d/data/UriImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;
    invoke-static {v4}, Lcom/sec/android/gallery3d/data/UriImage;->access$300(Lcom/sec/android/gallery3d/data/UriImage;)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->mType:I

    invoke-static {p1, v4, v1, v2, v5}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodeThumbnail(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/io/FileDescriptor;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 262
    :cond_7
    iget v3, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->mType:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_8

    iget v3, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->mType:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_9

    .line 263
    :cond_8
    invoke-static {v0, v2, v6}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 268
    :goto_2
    if-eqz v0, :cond_0

    .line 269
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    # setter for: Lcom/sec/android/gallery3d/data/UriImage;->mWidth:I
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/UriImage;->access$402(Lcom/sec/android/gallery3d/data/UriImage;I)I

    .line 270
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriImage;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    # setter for: Lcom/sec/android/gallery3d/data/UriImage;->mHeight:I
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/UriImage;->access$502(Lcom/sec/android/gallery3d/data/UriImage;I)I

    goto/16 :goto_0

    .line 265
    :cond_9
    invoke-static {v0, v2, v6}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownBySideLength(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 230
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/UriImage$BitmapJob;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
