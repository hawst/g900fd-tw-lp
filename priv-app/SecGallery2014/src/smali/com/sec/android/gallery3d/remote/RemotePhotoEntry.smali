.class public abstract Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;
.super Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;
.source "RemotePhotoEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/RemotePhotoEntry$PhotoColumns;,
        Lcom/sec/android/gallery3d/remote/RemotePhotoEntry$Table;
    }
.end annotation


# static fields
.field private static mFields:[Ljava/lang/reflect/Field;


# instance fields
.field public album_id:Ljava/lang/String;

.field public cache_pathname:Ljava/lang/String;

.field public cache_status:I

.field public content_type:Ljava/lang/String;

.field public content_url:Ljava/lang/String;

.field public date_edited:J

.field public date_taken:J

.field public display_index:I

.field public height:I

.field public latitude:D

.field public longitude:D

.field public rotation:I

.field public screennail_url:Ljava/lang/String;

.field public size:J

.field public source_id:Ljava/lang/String;

.field public thumbnail_url:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->mFields:[Ljava/lang/reflect/Field;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;-><init>()V

    .line 53
    return-void
.end method

.method public static getProjection()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 158
    sget-object v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->mFields:[Ljava/lang/reflect/Field;

    .line 159
    .local v0, "fields":[Ljava/lang/reflect/Field;
    array-length v3, v0

    new-array v2, v3, [Ljava/lang/String;

    .line 160
    .local v2, "projection":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-eq v1, v3, :cond_0

    .line 161
    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 160
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 163
    :cond_0
    return-object v2
.end method


# virtual methods
.method public clear()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 76
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->clear()V

    .line 77
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->source_id:Ljava/lang/String;

    .line 78
    iput v1, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->cache_status:I

    .line 79
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->cache_pathname:Ljava/lang/String;

    .line 80
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->album_id:Ljava/lang/String;

    .line 81
    iput v1, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->display_index:I

    .line 82
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->title:Ljava/lang/String;

    .line 83
    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->date_edited:J

    .line 84
    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->date_taken:J

    .line 85
    iput v1, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->width:I

    .line 86
    iput v1, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->height:I

    .line 87
    iput v1, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->rotation:I

    .line 88
    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->size:J

    .line 89
    iput-wide v4, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->latitude:D

    .line 90
    iput-wide v4, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->longitude:D

    .line 91
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->thumbnail_url:Ljava/lang/String;

    .line 92
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->screennail_url:Ljava/lang/String;

    .line 93
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->content_url:Ljava/lang/String;

    .line 94
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->content_type:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public doNotMerge(Ljava/lang/String;)Z
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 131
    const-string v1, "cache_status"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 136
    :cond_0
    :goto_0
    return v0

    .line 133
    :cond_1
    const-string v1, "cache_pathname"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 99
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;

    if-nez v2, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 101
    check-cast v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;

    .line 102
    .local v0, "p":Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->source_id:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->source_id:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->cache_status:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->cache_status:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->cache_pathname:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->cache_pathname:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->album_id:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->album_id:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->display_index:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->display_index:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->title:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->title:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->date_edited:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->date_edited:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->date_taken:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->date_taken:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->width:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->width:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->height:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->height:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->rotation:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->rotation:I

    if-ne v2, v3, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->size:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->size:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->latitude:D

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->latitude:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->longitude:D

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->longitude:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->thumbnail_url:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->thumbnail_url:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->screennail_url:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->screennail_url:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->content_url:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->content_url:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public getExtraSql(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 141
    const-string v0, "cache_status"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const-string v0, "DEFAULT 0"

    .line 144
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFields()[Ljava/lang/reflect/Field;
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->mFields:[Ljava/lang/reflect/Field;

    return-object v0
.end method

.method public getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    const-string v0, "photos"

    return-object v0
.end method

.method public needIndex(Ljava/lang/String;)Z
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 123
    const-string v0, "display_index"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    const/4 v0, 0x1

    .line 126
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
