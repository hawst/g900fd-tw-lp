.class public abstract Lcom/sec/android/gallery3d/app/DetailReloadTask;
.super Ljava/lang/Thread;
.source "DetailReloadTask.java"


# instance fields
.field private volatile mActive:Z

.field private final mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

.field private final mContext:Landroid/content/Context;

.field private volatile mDirty:Z

.field private mIsLoading:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/gallery3d/app/AlbumReloader;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "threadName"    # Ljava/lang/String;
    .param p3, "albumReloader"    # Lcom/sec/android/gallery3d/app/AlbumReloader;

    .prologue
    const/4 v0, 0x1

    .line 35
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 28
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mActive:Z

    .line 29
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mDirty:Z

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mIsLoading:Z

    .line 36
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mContext:Landroid/content/Context;

    .line 37
    iput-object p3, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    .line 38
    return-void
.end method

.method private updateLoading(Z)V
    .locals 1
    .param p1, "loading"    # Z

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mIsLoading:Z

    if-ne v0, p1, :cond_0

    .line 48
    :goto_0
    return-void

    .line 46
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mIsLoading:Z

    .line 47
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/DetailReloadTask;->updateLoadingStatus(Z)V

    goto :goto_0
.end method


# virtual methods
.method public isLoading()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mIsLoading:Z

    return v0
.end method

.method public declared-synchronized notifyDirty()V
    .locals 1

    .prologue
    .line 69
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mDirty:Z

    .line 70
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    monitor-exit p0

    return-void

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract onLoadData()Z
.end method

.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    const/4 v0, 0x0

    .line 53
    .local v0, "updateComplete":Z
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mActive:Z

    if-eqz v1, :cond_3

    .line 54
    monitor-enter p0

    .line 55
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mActive:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mDirty:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AlbumReloader;->isLoading()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 56
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/DetailReloadTask;->updateLoading(Z)V

    .line 57
    invoke-static {p0}, Lcom/sec/android/gallery3d/common/Utils;->waitWithoutInterrupt(Ljava/lang/Object;)V

    .line 58
    monitor-exit p0

    goto :goto_0

    .line 60
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_2
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mDirty:Z

    .line 62
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/DetailReloadTask;->updateLoading(Z)V

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/DetailReloadTask;->onLoadData()Z

    move-result v0

    goto :goto_0

    .line 65
    :cond_3
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/DetailReloadTask;->updateLoading(Z)V

    .line 66
    return-void
.end method

.method public declared-synchronized terminate()V
    .locals 1

    .prologue
    .line 74
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/DetailReloadTask;->mActive:Z

    .line 75
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    monitor-exit p0

    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract updateLoadingStatus(Z)V
.end method
