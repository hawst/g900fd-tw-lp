.class Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;
.super Ljava/lang/Object;
.source "TileImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/TileImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TileQueue"
.end annotation


# instance fields
.field private mHead:Lcom/sec/android/gallery3d/ui/TileImageView$Tile;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/ui/TileImageView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/ui/TileImageView$1;

    .prologue
    .line 936
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;-><init>()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;)Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    .prologue
    .line 936
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->mHead:Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    return-object v0
.end method


# virtual methods
.method public clean()V
    .locals 1

    .prologue
    .line 953
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->mHead:Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .line 954
    return-void
.end method

.method public pop()Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    .locals 2

    .prologue
    .line 940
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->mHead:Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .line 941
    .local v0, "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mNext:Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->mHead:Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .line 942
    :cond_0
    return-object v0
.end method

.method public push(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)Z
    .locals 2
    .param p1, "tile"    # Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .prologue
    .line 946
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->mHead:Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 947
    .local v0, "wasEmpty":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->mHead:Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    iput-object v1, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mNext:Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .line 948
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->mHead:Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .line 949
    return v0

    .line 946
    .end local v0    # "wasEmpty":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
