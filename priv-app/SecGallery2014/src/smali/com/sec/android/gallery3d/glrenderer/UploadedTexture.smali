.class public abstract Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;
.super Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
.source "UploadedTexture.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$1;,
        Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Texture"

.field protected static final UPLOAD_LIMIT:I = 0x64

.field private static sBorderKey:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;

.field private static sBorderLines:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected mBitmap:Landroid/graphics/Bitmap;

.field private mBorder:I

.field protected mBufferData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

.field private mContentValid:Z

.field private mIsUploading:Z

.field private mOpaque:Z

.field private mThrottled:Z

.field mUseBuffer:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->sBorderLines:Ljava/util/HashMap;

    .line 55
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;-><init>(Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$1;)V

    sput-object v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->sBorderKey:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;-><init>(Z)V

    .line 73
    return-void
.end method

.method protected constructor <init>(Z)V
    .locals 3
    .param p1, "hasBorder"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;-><init>(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 59
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mIsUploading:Z

    .line 63
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mOpaque:Z

    .line 64
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mThrottled:Z

    .line 375
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mUseBuffer:Z

    .line 77
    if-eqz p1, :cond_0

    .line 78
    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->setBorder(Z)V

    .line 79
    iput v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    .line 81
    :cond_0
    return-void
.end method

.method private freeBitmap()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 166
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->onFreeBitmap(Landroid/graphics/Bitmap;)V

    .line 165
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private freeBuffer()V
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBufferData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBufferData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->onFreeBuffer(Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;)V

    .line 409
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBufferData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    .line 411
    :cond_0
    return-void
.end method

.method private getBitmap()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 143
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->onGetBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    .line 146
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 147
    const/4 v2, 0x0

    .line 155
    :goto_0
    return-object v2

    .line 149
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    mul-int/lit8 v3, v3, 0x2

    add-int v1, v2, v3

    .line 150
    .local v1, "w":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    mul-int/lit8 v3, v3, 0x2

    add-int v0, v2, v3

    .line 151
    .local v0, "h":I
    iget v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mWidth:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 152
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->setSize(II)V

    .line 155
    .end local v0    # "h":I
    .end local v1    # "w":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private static getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "vertical"    # Z
    .param p1, "config"    # Landroid/graphics/Bitmap$Config;
    .param p2, "length"    # I

    .prologue
    const/4 v3, 0x1

    .line 128
    sget-object v1, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->sBorderKey:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;

    .line 129
    .local v1, "key":Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;
    iput-boolean p0, v1, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;->vertical:Z

    .line 130
    iput-object p1, v1, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;->config:Landroid/graphics/Bitmap$Config;

    .line 131
    iput p2, v1, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;->length:I

    .line 132
    sget-object v2, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->sBorderLines:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 133
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 134
    if-eqz p0, :cond_1

    invoke-static {v3, p2, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 137
    :goto_0
    sget-object v2, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->sBorderLines:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;->clone()Lcom/sec/android/gallery3d/glrenderer/UploadedTexture$BorderKey;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    :cond_0
    return-object v0

    .line 134
    :cond_1
    invoke-static {p2, v3, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private getBuffer()Ljava/nio/ByteBuffer;
    .locals 4

    .prologue
    .line 389
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBufferData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    if-nez v2, :cond_0

    .line 390
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->onGetBuffer()Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBufferData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    .line 392
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBufferData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    if-eqz v2, :cond_0

    .line 393
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBufferData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    iget v2, v2, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->width:I

    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    mul-int/lit8 v3, v3, 0x2

    add-int v1, v2, v3

    .line 394
    .local v1, "w":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBufferData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    iget v2, v2, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->height:I

    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    mul-int/lit8 v3, v3, 0x2

    add-int v0, v2, v3

    .line 395
    .local v0, "h":I
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->setSize(II)V

    .line 399
    .end local v0    # "h":I
    .end local v1    # "w":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBufferData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    if-eqz v2, :cond_1

    .line 400
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBufferData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    iget-object v2, v2, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    .line 402
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 23
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 275
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 277
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 278
    :cond_0
    const-string v2, "Texture"

    const-string v3, "bitmap is null or recycled."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    :goto_0
    return-void

    .line 282
    :cond_1
    if-eqz v6, :cond_7

    .line 284
    :try_start_0
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v17

    .line 285
    .local v17, "bWidth":I
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    .line 286
    .local v16, "bHeight":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    mul-int/lit8 v2, v2, 0x2

    add-int v22, v17, v2

    .line 287
    .local v22, "width":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    mul-int/lit8 v2, v2, 0x2

    add-int v19, v16, v2

    .line 288
    .local v19, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getTextureWidth()I

    move-result v21

    .line 289
    .local v21, "texWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getTextureHeight()I

    move-result v20

    .line 291
    .local v20, "texHeight":I
    move/from16 v0, v17

    move/from16 v1, v21

    if-gt v0, v1, :cond_3

    move/from16 v0, v16

    move/from16 v1, v20

    if-gt v0, v1, :cond_3

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Ljunit/framework/Assert;->assertTrue(Z)V

    .line 294
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->getGLId()Lcom/sec/android/gallery3d/glrenderer/GLId;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/glrenderer/GLId;->generateTexture()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mId:I

    .line 295
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setTextureParameters(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)V

    .line 297
    move/from16 v0, v17

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    move/from16 v0, v16

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 298
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1, v6}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->initializeTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    :cond_2
    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->freeBitmap()V

    .line 333
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->setAssociatedCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 334
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mState:I

    .line 335
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    goto :goto_0

    .line 291
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 300
    :cond_4
    :try_start_1
    invoke-static {v6}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v7

    .line 301
    .local v7, "format":I
    invoke-static {v6}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v8

    .line 302
    .local v8, "type":I
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v18

    .line 304
    .local v18, "config":Landroid/graphics/Bitmap$Config;
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1, v7, v8}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->initializeTextureSize(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;II)V

    .line 305
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    move-object/from16 v2, p1

    move-object/from16 v3, p0

    invoke-interface/range {v2 .. v8}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 307
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    if-lez v2, :cond_5

    .line 309
    const/4 v2, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-static {v2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 310
    .local v13, "line":Landroid/graphics/Bitmap;
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 313
    const/4 v2, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-static {v2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 314
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 318
    .end local v13    # "line":Landroid/graphics/Bitmap;
    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    add-int v2, v2, v17

    move/from16 v0, v21

    if-ge v2, v0, :cond_6

    .line 319
    const/4 v2, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-static {v2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 320
    .restart local v13    # "line":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    add-int v11, v2, v17

    const/4 v12, 0x0

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 324
    .end local v13    # "line":Landroid/graphics/Bitmap;
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    add-int v2, v2, v16

    move/from16 v0, v20

    if-ge v2, v0, :cond_2

    .line 325
    const/4 v2, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-static {v2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 326
    .restart local v13    # "line":Landroid/graphics/Bitmap;
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    add-int v12, v2, v16

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 330
    .end local v7    # "format":I
    .end local v8    # "type":I
    .end local v13    # "line":Landroid/graphics/Bitmap;
    .end local v16    # "bHeight":I
    .end local v17    # "bWidth":I
    .end local v18    # "config":Landroid/graphics/Bitmap$Config;
    .end local v19    # "height":I
    .end local v20    # "texHeight":I
    .end local v21    # "texWidth":I
    .end local v22    # "width":I
    :catchall_0
    move-exception v2

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->freeBitmap()V

    throw v2

    .line 337
    :cond_7
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mState:I

    .line 338
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Texture load fail, no bitmap"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public getHeight()I
    .locals 2

    .prologue
    .line 184
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mHeight:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 185
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mUseBuffer:Z

    if-eqz v0, :cond_1

    .line 186
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBuffer()Ljava/nio/ByteBuffer;

    .line 191
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mHeight:I

    return v0

    .line 188
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBitmap()Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method protected getTarget()I
    .locals 1

    .prologue
    .line 350
    const/16 v0, 0xde1

    return v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 171
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mWidth:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 172
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mUseBuffer:Z

    if-eqz v0, :cond_1

    .line 173
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBuffer()Ljava/nio/ByteBuffer;

    .line 178
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mWidth:I

    return v0

    .line 175
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBitmap()Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method protected invalidateContent()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 199
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 200
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->freeBitmap()V

    .line 202
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    .line 203
    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mWidth:I

    .line 204
    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mHeight:I

    .line 205
    return-void
.end method

.method public isContentValid()Z
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 359
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mOpaque:Z

    return v0
.end method

.method public isUploading()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mIsUploading:Z

    return v0
.end method

.method protected onBind(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z
    .locals 1
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 344
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->updateContent(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->isContentValid()Z

    move-result v0

    return v0
.end method

.method protected abstract onFreeBitmap(Landroid/graphics/Bitmap;)V
.end method

.method protected onFreeBuffer(Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;)V
    .locals 0
    .param p1, "buffer"    # Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    .prologue
    .line 386
    return-void
.end method

.method protected abstract onGetBitmap()Landroid/graphics/Bitmap;
.end method

.method protected onGetBuffer()Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    .locals 1

    .prologue
    .line 382
    const/4 v0, 0x0

    return-object v0
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 364
    invoke-super {p0}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->recycle()V

    .line 366
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mUseBuffer:Z

    if-eqz v0, :cond_1

    .line 367
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBufferData:Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->freeBuffer()V

    .line 372
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->freeBitmap()V

    goto :goto_0
.end method

.method protected setIsUploading(Z)V
    .locals 0
    .param p1, "uploading"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mIsUploading:Z

    .line 85
    return-void
.end method

.method public setOpaque(Z)V
    .locals 0
    .param p1, "isOpaque"    # Z

    .prologue
    .line 354
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mOpaque:Z

    .line 355
    return-void
.end method

.method protected setThrottled(Z)V
    .locals 0
    .param p1, "throttled"    # Z

    .prologue
    .line 123
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mThrottled:Z

    .line 124
    return-void
.end method

.method protected setUseBuffer()V
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mUseBuffer:Z

    .line 379
    return-void
.end method

.method public updateContent(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 17
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 219
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->isLoaded()Z

    move-result v1

    if-nez v1, :cond_3

    .line 220
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->getUploadedCount()I

    move-result v15

    .line 221
    .local v15, "count":I
    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setUploadedCount(I)V

    .line 222
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mThrottled:Z

    if-eqz v1, :cond_1

    add-int/lit8 v15, v15, 0x1

    const/16 v1, 0x64

    if-le v15, v1, :cond_1

    .line 261
    .end local v15    # "count":I
    :cond_0
    :goto_0
    return-void

    .line 226
    .restart local v15    # "count":I
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mUseBuffer:Z

    if-eqz v1, :cond_2

    .line 228
    :try_start_0
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->uploadBufferToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 229
    :catch_0
    move-exception v16

    .line 230
    .local v16, "e":Ljava/lang/Exception;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 234
    .end local v16    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    goto :goto_0

    .line 236
    .end local v15    # "count":I
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    if-nez v1, :cond_0

    .line 237
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mUseBuffer:Z

    if-eqz v1, :cond_4

    .line 238
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 239
    .local v5, "buffer":Ljava/nio/ByteBuffer;
    if-eqz v5, :cond_0

    .line 241
    const/16 v6, 0x1908

    .line 242
    .local v6, "format":I
    const/16 v7, 0x1401

    .line 244
    .local v7, "type":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    move-object/from16 v1, p1

    move-object/from16 v2, p0

    invoke-interface/range {v1 .. v7}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILjava/nio/Buffer;II)V

    .line 246
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->freeBuffer()V

    .line 258
    .end local v5    # "buffer":Ljava/nio/ByteBuffer;
    :goto_1
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    goto :goto_0

    .line 248
    .end local v6    # "format":I
    .end local v7    # "type":I
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    .line 250
    .local v12, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v12, :cond_0

    .line 253
    invoke-static {v12}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v6

    .line 254
    .restart local v6    # "format":I
    invoke-static {v12}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v7

    .line 255
    .restart local v7    # "type":I
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    move-object/from16 v8, p1

    move-object/from16 v9, p0

    move v13, v6

    move v14, v7

    invoke-interface/range {v8 .. v14}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 256
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->freeBitmap()V

    goto :goto_1
.end method

.method protected uploadBufferToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 21
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 414
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 415
    .local v6, "buffer":Ljava/nio/ByteBuffer;
    if-eqz v6, :cond_5

    .line 417
    :try_start_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mWidth:I

    .line 418
    .local v4, "bWidth":I
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mHeight:I

    .line 419
    .local v5, "bHeight":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    mul-int/lit8 v2, v2, 0x2

    add-int v20, v4, v2

    .line 420
    .local v20, "width":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    mul-int/lit8 v2, v2, 0x2

    add-int v17, v5, v2

    .line 421
    .local v17, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getTextureWidth()I

    move-result v19

    .line 422
    .local v19, "texWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getTextureHeight()I

    move-result v18

    .line 424
    .local v18, "texHeight":I
    move/from16 v0, v19

    if-gt v4, v0, :cond_1

    move/from16 v0, v18

    if-gt v5, v0, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljunit/framework/Assert;->assertTrue(Z)V

    .line 427
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->getGLId()Lcom/sec/android/gallery3d/glrenderer/GLId;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/glrenderer/GLId;->generateTexture()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mId:I

    .line 428
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setTextureParameters(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)V

    .line 430
    const/16 v7, 0x1908

    .line 431
    .local v7, "format":I
    const/16 v8, 0x1401

    .line 432
    .local v8, "type":I
    sget-object v16, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 434
    .local v16, "config":Landroid/graphics/Bitmap$Config;
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 435
    move/from16 v0, v19

    if-ne v4, v0, :cond_2

    move/from16 v0, v18

    if-ne v5, v0, :cond_2

    move-object/from16 v2, p1

    move-object/from16 v3, p0

    .line 436
    invoke-interface/range {v2 .. v8}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->initializeTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILjava/nio/Buffer;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 473
    :cond_0
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->freeBuffer()V

    .line 476
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->setAssociatedCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 477
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mState:I

    .line 478
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    .line 483
    return-void

    .line 424
    .end local v7    # "format":I
    .end local v8    # "type":I
    .end local v16    # "config":Landroid/graphics/Bitmap$Config;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 442
    .restart local v7    # "format":I
    .restart local v8    # "type":I
    .restart local v16    # "config":Landroid/graphics/Bitmap$Config;
    :cond_2
    :try_start_1
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1, v7, v8}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->initializeTextureSize(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;II)V

    .line 444
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move-object v13, v6

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILjava/nio/Buffer;II)V

    .line 450
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    if-lez v2, :cond_3

    .line 452
    const/4 v2, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-static {v2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 453
    .local v13, "line":Landroid/graphics/Bitmap;
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 456
    const/4 v2, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-static {v2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 457
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 461
    .end local v13    # "line":Landroid/graphics/Bitmap;
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    add-int/2addr v2, v4

    move/from16 v0, v19

    if-ge v2, v0, :cond_4

    .line 462
    const/4 v2, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-static {v2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 463
    .restart local v13    # "line":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    add-int v11, v2, v4

    const/4 v12, 0x0

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 467
    .end local v13    # "line":Landroid/graphics/Bitmap;
    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    add-int/2addr v2, v5

    move/from16 v0, v18

    if-ge v2, v0, :cond_0

    .line 468
    const/4 v2, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-static {v2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 469
    .restart local v13    # "line":Landroid/graphics/Bitmap;
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    add-int v12, v2, v5

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 473
    .end local v4    # "bWidth":I
    .end local v5    # "bHeight":I
    .end local v7    # "format":I
    .end local v8    # "type":I
    .end local v13    # "line":Landroid/graphics/Bitmap;
    .end local v16    # "config":Landroid/graphics/Bitmap$Config;
    .end local v17    # "height":I
    .end local v18    # "texHeight":I
    .end local v19    # "texWidth":I
    .end local v20    # "width":I
    :catchall_0
    move-exception v2

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->freeBuffer()V

    throw v2

    .line 480
    :cond_5
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->mState:I

    .line 481
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Texture load fail, no buffer"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
