.class public final Lcom/sec/android/gallery3d/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AbsHListView:[I

.field public static final AbsHListView_android_cacheColorHint:I = 0x3

.field public static final AbsHListView_android_choiceMode:I = 0x4

.field public static final AbsHListView_android_drawSelectorOnTop:I = 0x1

.field public static final AbsHListView_android_listSelector:I = 0x0

.field public static final AbsHListView_android_scrollingCache:I = 0x2

.field public static final AbsHListView_android_smoothScrollbar:I = 0x5

.field public static final AbsHListView_stackFromRight:I = 0x6

.field public static final AbsHListView_transcriptMode:I = 0x7

.field public static final CameraPreference:[I

.field public static final CameraPreference_title:I = 0x0

.field public static final Gallery:[I

.field public static final Gallery_android_galleryItemBackground:I = 0x0

.field public static final HListView:[I

.field public static final HListView_android_divider:I = 0x1

.field public static final HListView_android_entries:I = 0x0

.field public static final HListView_dividerWidth:I = 0x2

.field public static final HListView_footerDividersEnabled:I = 0x4

.field public static final HListView_headerDividersEnabled:I = 0x3

.field public static final HListView_measureWithChild:I = 0x7

.field public static final HListView_overScrollFooter:I = 0x6

.field public static final HListView_overScrollHeader:I = 0x5

.field public static final IconIndicator:[I

.field public static final IconIndicator_icons:I = 0x0

.field public static final IconIndicator_modes:I = 0x1

.field public static final IconListPreference:[I

.field public static final IconListPreference_icons:I = 0x0

.field public static final IconListPreference_images:I = 0x3

.field public static final IconListPreference_largeIcons:I = 0x2

.field public static final IconListPreference_singleIcon:I = 0x1

.field public static final ListPreference:[I

.field public static final ListPreference_defaultValue:I = 0x1

.field public static final ListPreference_entries:I = 0x3

.field public static final ListPreference_entryValues:I = 0x2

.field public static final ListPreference_key:I = 0x0

.field public static final ListPreference_labelList:I = 0x4

.field public static final Theme:[I

.field public static final Theme_GalleryBase:[I

.field public static final Theme_GalleryBase_listPreferredItemHeightSmall:I = 0x0

.field public static final Theme_GalleryBase_switchStyle:I = 0x1

.field public static final Theme_android_galleryItemBackground:I = 0x0

.field public static final expandable:[I

.field public static final expandable_horizontalSpacing:I = 0x0

.field public static final expandable_itemHeight:I = 0x2

.field public static final expandable_verticalSpacing:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const v6, 0x101004c

    const/16 v5, 0x8

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v5, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/gallery3d/R$styleable;->AbsHListView:[I

    new-array v0, v3, [I

    const v1, 0x7f010002

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/gallery3d/R$styleable;->CameraPreference:[I

    new-array v0, v3, [I

    aput v6, v0, v2

    sput-object v0, Lcom/sec/android/gallery3d/R$styleable;->Gallery:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/gallery3d/R$styleable;->HListView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/gallery3d/R$styleable;->IconIndicator:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/gallery3d/R$styleable;->IconListPreference:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/sec/android/gallery3d/R$styleable;->ListPreference:[I

    new-array v0, v3, [I

    aput v6, v0, v2

    sput-object v0, Lcom/sec/android/gallery3d/R$styleable;->Theme:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/sec/android/gallery3d/R$styleable;->Theme_GalleryBase:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/sec/android/gallery3d/R$styleable;->expandable:[I

    return-void

    :array_0
    .array-data 4
        0x10100fb
        0x10100fc
        0x10100fe
        0x1010101
        0x101012b
        0x1010231
        0x7f010019
        0x7f01001a
    .end array-data

    :array_1
    .array-data 4
        0x10100b2
        0x1010129
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
    .end array-data

    :array_2
    .array-data 4
        0x7f010008
        0x7f010009
    .end array-data

    :array_3
    .array-data 4
        0x7f010008
        0x7f01000a
        0x7f01000b
        0x7f01000c
    .end array-data

    :array_4
    .array-data 4
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
    .end array-data

    :array_5
    .array-data 4
        0x7f010000
        0x7f010001
    .end array-data

    :array_6
    .array-data 4
        0x7f01000d
        0x7f01000e
        0x7f01000f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
