.class Lcom/sec/android/gallery3d/ui/FaceIndicatorView$6;
.super Ljava/lang/Object;
.source "FaceIndicatorView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->createSettingPopupForMe(Lcom/sec/samsung/gallery/access/contact/ContactData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)V
    .locals 0

    .prologue
    .line 781
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$6;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 785
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$6;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$6;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$6;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 786
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$6;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$6;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 788
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 789
    return-void
.end method
