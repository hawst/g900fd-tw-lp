.class public Lcom/sec/android/gallery3d/data/ClusterAlbumSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "ClusterAlbumSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;,
        Lcom/sec/android/gallery3d/data/ClusterAlbumSet$ClusterUpdateListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ClusterAlbumSet"


# instance fields
.field private locationZoomLevel:F

.field private mAlbums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/ClusterAlbum;",
            ">;"
        }
    .end annotation
.end field

.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mCancelled:Z

.field private mClustering:Lcom/sec/android/gallery3d/data/Clustering;

.field private mFaceComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/ClusterAlbum;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstReloadDone:Z

.field private mForceReload:Z

.field private mGroupComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/ClusterAlbum;",
            ">;"
        }
    .end annotation
.end field

.field private mKind:I

.field private mNeedMapThumbnailUpdate:Z

.field private mSearchArcMode:Z

.field private mSortByType:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p4, "kind"    # I

    .prologue
    const/4 v2, 0x0

    .line 58
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    .line 47
    iput v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mSortByType:I

    .line 48
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mSearchArcMode:Z

    .line 49
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mNeedMapThumbnailUpdate:Z

    .line 50
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mForceReload:Z

    .line 52
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mCancelled:Z

    .line 53
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->locationZoomLevel:F

    .line 466
    new-instance v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$2;-><init>(Lcom/sec/android/gallery3d/data/ClusterAlbumSet;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mFaceComparator:Ljava/util/Comparator;

    .line 504
    new-instance v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$3;-><init>(Lcom/sec/android/gallery3d/data/ClusterAlbumSet;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mGroupComparator:Ljava/util/Comparator;

    .line 59
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 60
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 61
    iput p4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    .line 62
    invoke-virtual {p3, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 63
    return-void
.end method

.method private IsSupportKind()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 548
    iget v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/16 v2, 0x10

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/16 v2, 0xf

    if-eq v1, v2, :cond_0

    .line 555
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/ClusterAlbumSet;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method private updateClusters()V
    .locals 18

    .prologue
    .line 149
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_2

    .line 150
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-ge v8, v14, :cond_1

    .line 152
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    if-eqz v14, :cond_0

    .line 153
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->setMediaItems(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :cond_0
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 155
    :catch_0
    move-exception v9

    .line 156
    .local v9, "ie":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v9}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1

    .line 159
    .end local v9    # "ie":Ljava/lang/IndexOutOfBoundsException;
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    .line 164
    .end local v8    # "i":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v14}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    .line 165
    .local v6, "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    packed-switch v14, :pswitch_data_0

    .line 211
    :pswitch_0
    new-instance v14, Lcom/sec/android/gallery3d/data/SizeClustering;

    invoke-direct {v14, v6}, Lcom/sec/android/gallery3d/data/SizeClustering;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    .line 215
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v14, v15}, Lcom/sec/android/gallery3d/data/Clustering;->run(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 216
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/Clustering;->getNumberOfClusters()I

    move-result v11

    .line 217
    .local v11, "n":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v14}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v7

    .line 218
    .local v7, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_3
    if-ge v8, v11, :cond_d

    .line 220
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    invoke-virtual {v14, v8}, Lcom/sec/android/gallery3d/data/Clustering;->getClusterName(I)Ljava/lang/String;

    move-result-object v3

    .line 222
    .local v3, "childName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    invoke-virtual {v14, v8}, Lcom/sec/android/gallery3d/data/Clustering;->getClusterNameID(I)Ljava/lang/String;

    move-result-object v4

    .line 223
    .local v4, "childNameID":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    invoke-virtual {v14, v8}, Lcom/sec/android/gallery3d/data/Clustering;->getClusterKey(I)Ljava/lang/String;

    move-result-object v10

    .line 225
    .local v10, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_9

    .line 226
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    .line 235
    .local v5, "childPath":Lcom/sec/android/gallery3d/data/Path;
    :goto_4
    sget-object v15, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v15

    .line 236
    :try_start_1
    invoke-virtual {v7, v5}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .line 237
    .local v2, "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    if-nez v2, :cond_3

    .line 239
    new-instance v2, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .end local v2    # "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    move-object/from16 v0, p0

    invoke-direct {v2, v5, v7, v0, v14}, Lcom/sec/android/gallery3d/data/ClusterAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 242
    .restart local v2    # "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    :cond_3
    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->isFaceCluster()Z

    move-result v14

    if-eqz v14, :cond_b

    .line 245
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    invoke-virtual {v14, v8}, Lcom/sec/android/gallery3d/data/Clustering;->getCluster(I)Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual {v2, v14}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->setMediaItemsEx(Ljava/util/ArrayList;)V

    .line 249
    :goto_5
    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->setName(Ljava/lang/String;)V

    .line 251
    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->setNameID(Ljava/lang/String;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    invoke-virtual {v14, v8}, Lcom/sec/android/gallery3d/data/Clustering;->getClusterCover(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v14

    invoke-virtual {v2, v14}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->setCoverMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 255
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->isFaceCluster()Z

    move-result v14

    if-eqz v14, :cond_c

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/4 v15, 0x5

    if-eq v14, v15, :cond_c

    .line 256
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    check-cast v14, Lcom/sec/android/gallery3d/data/FaceClustering;

    invoke-virtual {v14, v8}, Lcom/sec/android/gallery3d/data/FaceClustering;->getPersonId(I)I

    move-result v14

    iput v14, v2, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPersonId:I

    .line 257
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    check-cast v14, Lcom/sec/android/gallery3d/data/FaceClustering;

    invoke-virtual {v14, v8}, Lcom/sec/android/gallery3d/data/FaceClustering;->getGroupId(I)I

    move-result v14

    iput v14, v2, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mGroupId:I

    .line 262
    :cond_4
    :goto_6
    if-eqz v10, :cond_5

    .line 263
    invoke-virtual {v2, v10}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->setKey(Ljava/lang/String;)V

    .line 265
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->isFaceCluster()Z

    move-result v14

    if-nez v14, :cond_6

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/4 v15, 0x6

    if-eq v14, v15, :cond_6

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/16 v15, 0x10

    if-ne v14, v15, :cond_7

    .line 272
    :cond_6
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateDataVersion()V

    .line 274
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    if-eqz v14, :cond_8

    .line 275
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    :cond_8
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_3

    .line 167
    .end local v2    # "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .end local v3    # "childName":Ljava/lang/String;
    .end local v4    # "childNameID":Ljava/lang/String;
    .end local v5    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v7    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v8    # "i":I
    .end local v10    # "key":Ljava/lang/String;
    .end local v11    # "n":I
    :pswitch_1
    new-instance v14, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mSortByType:I

    invoke-direct {v14, v6, v15}, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 170
    :pswitch_2
    new-instance v14, Lcom/sec/android/gallery3d/data/LocationClustering;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->locationZoomLevel:F

    invoke-direct {v14, v6, v15}, Lcom/sec/android/gallery3d/data/LocationClustering;-><init>(Landroid/content/Context;F)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 173
    :pswitch_3
    new-instance v14, Lcom/sec/android/gallery3d/data/TagClustering;

    invoke-direct {v14, v6}, Lcom/sec/android/gallery3d/data/TagClustering;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 176
    :pswitch_4
    new-instance v14, Lcom/sec/android/gallery3d/data/FaceClustering;

    const/4 v15, 0x0

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-direct {v14, v6, v15, v0}, Lcom/sec/android/gallery3d/data/FaceClustering;-><init>(Landroid/content/Context;ZZ)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 180
    :pswitch_5
    new-instance v14, Lcom/sec/android/gallery3d/data/SearchTimeClustering;

    invoke-direct {v14, v6}, Lcom/sec/android/gallery3d/data/SearchTimeClustering;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 183
    :pswitch_6
    new-instance v14, Lcom/sec/android/gallery3d/data/GallerySearchClustering;

    invoke-direct {v14, v6}, Lcom/sec/android/gallery3d/data/GallerySearchClustering;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 187
    :pswitch_7
    new-instance v14, Lcom/sec/android/gallery3d/data/GroupClustering;

    invoke-direct {v14, v6}, Lcom/sec/android/gallery3d/data/GroupClustering;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 190
    :pswitch_8
    new-instance v14, Lcom/sec/android/gallery3d/data/FaceClustering;

    const/4 v15, 0x1

    invoke-direct {v14, v6, v15}, Lcom/sec/android/gallery3d/data/FaceClustering;-><init>(Landroid/content/Context;Z)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 193
    :pswitch_9
    new-instance v14, Lcom/sec/android/gallery3d/data/FaceClustering;

    const/4 v15, 0x1

    const/16 v16, 0x1

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v14, v6, v15, v0, v1}, Lcom/sec/android/gallery3d/data/FaceClustering;-><init>(Landroid/content/Context;ZZZ)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 196
    :pswitch_a
    new-instance v14, Lcom/sec/android/gallery3d/data/FaceClustering;

    const/4 v15, 0x1

    const/16 v16, 0x1

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v14, v6, v15, v0, v1}, Lcom/sec/android/gallery3d/data/FaceClustering;-><init>(Landroid/content/Context;ZZZ)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 200
    :pswitch_b
    new-instance v14, Lcom/sec/android/gallery3d/data/DocumentClustering;

    invoke-direct {v14, v6}, Lcom/sec/android/gallery3d/data/DocumentClustering;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 203
    :pswitch_c
    new-instance v14, Lcom/sec/android/gallery3d/data/OcrClustering;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/gallery3d/data/Path;->getSuffix(I)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v6, v15}, Lcom/sec/android/gallery3d/data/OcrClustering;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 207
    :pswitch_d
    new-instance v14, Lcom/sec/android/gallery3d/data/FestivalClustering;

    invoke-direct {v14, v6}, Lcom/sec/android/gallery3d/data/FestivalClustering;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    goto/16 :goto_2

    .line 227
    .restart local v3    # "childName":Ljava/lang/String;
    .restart local v4    # "childNameID":Ljava/lang/String;
    .restart local v7    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .restart local v8    # "i":I
    .restart local v10    # "key":Ljava/lang/String;
    .restart local v11    # "n":I
    :cond_9
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/4 v15, 0x3

    if-ne v14, v15, :cond_a

    .line 228
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    check-cast v14, Lcom/sec/android/gallery3d/data/SizeClustering;

    invoke-virtual {v14, v8}, Lcom/sec/android/gallery3d/data/SizeClustering;->getMinSize(I)J

    move-result-wide v12

    .line 229
    .local v12, "minSize":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v14, v12, v13}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    .line 230
    .restart local v5    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    goto/16 :goto_4

    .line 231
    .end local v5    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v12    # "minSize":J
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v14, v8}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    .restart local v5    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    goto/16 :goto_4

    .line 242
    :catchall_0
    move-exception v14

    :try_start_2
    monitor-exit v15
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v14

    .line 248
    .restart local v2    # "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    invoke-virtual {v14, v8}, Lcom/sec/android/gallery3d/data/Clustering;->getCluster(I)Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual {v2, v14}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->setMediaItems(Ljava/util/ArrayList;)V

    goto/16 :goto_5

    .line 258
    :cond_c
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/4 v15, 0x5

    if-ne v14, v15, :cond_4

    .line 259
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    check-cast v14, Lcom/sec/android/gallery3d/data/GroupClustering;

    invoke-virtual {v14, v8}, Lcom/sec/android/gallery3d/data/GroupClustering;->getGroupId(I)I

    move-result v14

    iput v14, v2, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mGroupId:I

    goto/16 :goto_6

    .line 278
    .end local v2    # "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .end local v3    # "childName":Ljava/lang/String;
    .end local v4    # "childNameID":Ljava/lang/String;
    .end local v5    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v10    # "key":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    if-eqz v14, :cond_f

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->isFaceCluster()Z

    move-result v14

    if-eqz v14, :cond_f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/4 v15, 0x5

    if-eq v14, v15, :cond_f

    .line 279
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mFaceComparator:Ljava/util/Comparator;

    invoke-static {v14, v15}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 288
    :cond_e
    :goto_7
    return-void

    .line 280
    :cond_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    if-eqz v14, :cond_10

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/4 v15, 0x5

    if-ne v14, v15, :cond_10

    .line 281
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mGroupComparator:Ljava/util/Comparator;

    invoke-static {v14, v15}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 282
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    check-cast v14, Lcom/sec/android/gallery3d/data/GroupClustering;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/GroupClustering;->getPersonGroups()Landroid/util/SparseArray;

    move-result-object v14

    invoke-interface {v15, v14}, Lcom/sec/android/gallery3d/app/GalleryApp;->setPersonGroupsToGlobal(Landroid/util/SparseArray;)V

    goto :goto_7

    .line 283
    :cond_10
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_e

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mFirstReloadDone:Z

    if-eqz v14, :cond_e

    .line 284
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mNeedMapThumbnailUpdate:Z

    goto :goto_7

    .line 165
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_7
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_5
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_6
        :pswitch_d
    .end packed-switch
.end method

.method private updateClustersContents()V
    .locals 10

    .prologue
    .line 291
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 292
    .local v0, "existing":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/sec/android/gallery3d/data/Path;>;"
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    new-instance v9, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$1;

    invoke-direct {v9, p0, v0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$1;-><init>(Lcom/sec/android/gallery3d/data/ClusterAlbumSet;Ljava/util/HashSet;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    .line 304
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 308
    .local v4, "n":I
    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_3

    .line 309
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v6

    .line 310
    .local v6, "oldPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 311
    .local v5, "newPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 312
    .local v3, "m":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    if-ge v2, v3, :cond_1

    .line 313
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/Path;

    .line 314
    .local v7, "p":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v0, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 315
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 318
    .end local v7    # "p":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    invoke-virtual {v8, v5}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->setMediaItems(Ljava/util/ArrayList;)V

    .line 319
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 320
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 308
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 323
    .end local v2    # "j":I
    .end local v3    # "m":I
    .end local v5    # "newPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .end local v6    # "oldPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_3
    return-void
.end method


# virtual methods
.method public cancelRunningCluster()V
    .locals 1

    .prologue
    .line 541
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mCancelled:Z

    .line 542
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mClustering:Lcom/sec/android/gallery3d/data/Clustering;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Clustering;->onCancel()V

    .line 545
    :cond_0
    return-void
.end method

.method public confirmClusterName(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;)V
    .locals 7
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    const/4 v3, 0x0

    .line 387
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v6

    new-instance v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;-><init>(Lcom/sec/android/gallery3d/data/ClusterAlbumSet;Ljava/util/ArrayList;Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v6, v0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    .line 388
    return-void
.end method

.method public getAlbums()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/ClusterAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getClusterKind()I
    .locals 1

    .prologue
    .line 375
    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 68
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :goto_0
    return-object v1

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 73
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 2

    .prologue
    .line 617
    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    if-nez v0, :cond_0

    .line 618
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 625
    :goto_0
    return-object v0

    .line 619
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 620
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    goto :goto_0

    .line 621
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_3

    .line 623
    :cond_2
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SEARCH:Lcom/sec/samsung/gallery/core/TabTagType;

    goto :goto_0

    .line 625
    :cond_3
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    goto :goto_0
.end method

.method public isFaceCluster()Z
    .locals 2

    .prologue
    .line 529
    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mKind:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNeedUpdateCluster()Z
    .locals 1

    .prologue
    .line 537
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->isFaceCluster()Z

    move-result v0

    return v0
.end method

.method public mapUpdateDone()V
    .locals 1

    .prologue
    .line 525
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mNeedMapThumbnailUpdate:Z

    .line 526
    return-void
.end method

.method public needMapThumbnailUpdate()Z
    .locals 1

    .prologue
    .line 521
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mNeedMapThumbnailUpdate:Z

    return v0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->notifyContentChanged()V

    .line 144
    return-void
.end method

.method public reload()J
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 99
    const-wide/16 v2, 0x0

    .line 100
    .local v2, "pTime":J
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_LAUNCH_FINNISH:Z

    if-nez v5, :cond_0

    .line 101
    const-string v5, "Gallery_Performance"

    const-string v6, "Gallery ClusterAlbumSet reload Start"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME_ENABLE:Z

    if-eqz v5, :cond_0

    .line 103
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 107
    :cond_0
    const/4 v0, 0x0

    .line 108
    .local v0, "isDirty":Z
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    move v0, v1

    .line 110
    :goto_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->isArcMode()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->isFaceCluster()Z

    move-result v5

    if-nez v5, :cond_1

    iget-boolean v5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mSearchArcMode:Z

    if-eqz v5, :cond_3

    .line 111
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->reloadFace(Z)J

    move-result-wide v4

    .line 137
    :goto_1
    return-wide v4

    :cond_2
    move v0, v4

    .line 108
    goto :goto_0

    .line 115
    :cond_3
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->isDocMode()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 116
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->reloadDocument(Z)J

    move-result-wide v4

    goto :goto_1

    .line 120
    :cond_4
    if-nez v0, :cond_5

    iget-boolean v5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mForceReload:Z

    if-eqz v5, :cond_6

    .line 121
    :cond_5
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mFirstReloadDone:Z

    if-eqz v5, :cond_8

    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->IsSupportKind()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 122
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateClustersContents()V

    .line 127
    :goto_2
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->nextVersionNumber()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    .line 128
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mForceReload:Z

    .line 130
    :cond_6
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_LAUNCH_FINNISH:Z

    if-nez v1, :cond_7

    .line 131
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME_ENABLE:Z

    if-eqz v1, :cond_9

    .line 132
    const-string v1, "Gallery_Performance"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Gallery ClusterAlbumSet reload End "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_7
    :goto_3
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    goto :goto_1

    .line 124
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateClusters()V

    .line 125
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mFirstReloadDone:Z

    goto :goto_2

    .line 134
    :cond_9
    const-string v1, "Gallery_Performance"

    const-string v4, "Gallery ClusterAlbumSet reload End "

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public reloadDocument(Z)J
    .locals 3
    .param p1, "isDirty"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 590
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDocumentUpdateStatus()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 591
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateClusters()V

    .line 592
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mFirstReloadDone:Z

    .line 593
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    .line 594
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0, v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->setDocumentUpdateStatus(Z)V

    .line 605
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mCancelled:Z

    if-eqz v0, :cond_1

    .line 606
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mCancelled:Z

    .line 607
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    .line 609
    :cond_1
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    return-wide v0

    .line 595
    :cond_2
    if-eqz p1, :cond_0

    .line 596
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mFirstReloadDone:Z

    if-eqz v0, :cond_3

    .line 597
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateClustersContents()V

    .line 602
    :goto_1
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    goto :goto_0

    .line 599
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateClusters()V

    .line 600
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mFirstReloadDone:Z

    goto :goto_1
.end method

.method public reloadFace(Z)J
    .locals 7
    .param p1, "isDirty"    # Z

    .prologue
    const/4 v6, 0x0

    .line 559
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->isPhotoPage()Z

    move-result v0

    .line 560
    .local v0, "isPhotoPage":Z
    if-eqz p1, :cond_1

    .line 561
    if-nez v0, :cond_0

    .line 562
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1, v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->setRefreshOperation(I)V

    .line 563
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->reloadForFR()V

    .line 565
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->nextVersionNumber()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    .line 566
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    .line 585
    :goto_0
    return-wide v2

    .line 567
    :cond_1
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mArcDataVersion:J

    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getArcVersionNumber()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_5

    .line 568
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getRefreshOperation()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 569
    if-eqz v0, :cond_3

    .line 570
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    goto :goto_0

    .line 573
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1, v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->setRefreshOperation(I)V

    .line 576
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->isTaskComplete()Z

    move-result v1

    if-nez v1, :cond_4

    .line 577
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    goto :goto_0

    .line 580
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->reloadForFR()V

    .line 581
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->nextVersionNumber()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    .line 585
    :cond_5
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mDataVersion:J

    goto :goto_0
.end method

.method public reloadForFR()V
    .locals 2

    .prologue
    .line 357
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getArcVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mArcDataVersion:J

    .line 358
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateClusters()V

    .line 359
    return-void
.end method

.method public reloadLocationData(F)V
    .locals 0
    .param p1, "zoomLevel"    # F

    .prologue
    .line 362
    iput p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->locationZoomLevel:F

    .line 363
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateClusters()V

    .line 364
    return-void
.end method

.method public removeClusterName(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;)V
    .locals 7
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    const/4 v3, 0x0

    .line 383
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v6

    new-instance v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;

    const/4 v4, 0x1

    move-object v1, p0

    move-object v2, p2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;-><init>(Lcom/sec/android/gallery3d/data/ClusterAlbumSet;Ljava/util/ArrayList;Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v6, v0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    .line 384
    return-void
.end method

.method public setArcDataVersion()V
    .locals 2

    .prologue
    .line 353
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getArcVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mArcDataVersion:J

    .line 354
    return-void
.end method

.method public setForceReload(Z)V
    .locals 0
    .param p1, "forceReload"    # Z

    .prologue
    .line 463
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mForceReload:Z

    .line 464
    return-void
.end method

.method public setSearchArcMode()V
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mSearchArcMode:Z

    .line 349
    return-void
.end method

.method public setSortByType(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 340
    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mSortByType:I

    if-ne v0, p1, :cond_0

    .line 345
    :goto_0
    return-void

    .line 343
    :cond_0
    iput p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mSortByType:I

    .line 344
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mForceReload:Z

    goto :goto_0
.end method

.method public updateClusterName(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;)V
    .locals 7
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    const/4 v3, 0x0

    .line 391
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v6

    new-instance v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;

    const/4 v4, 0x3

    move-object v1, p0

    move-object v2, p2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;-><init>(Lcom/sec/android/gallery3d/data/ClusterAlbumSet;Ljava/util/ArrayList;Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v6, v0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    .line 392
    return-void
.end method

.method public updateClusterName(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "joinedName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 379
    .local p2, "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v6

    new-instance v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;

    const/4 v4, 0x2

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;-><init>(Lcom/sec/android/gallery3d/data/ClusterAlbumSet;Ljava/util/ArrayList;Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v6, v0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    .line 380
    return-void
.end method

.method public updateMediaSet()V
    .locals 0

    .prologue
    .line 328
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->updateMediaSet()V

    .line 329
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->setVersion()V

    .line 330
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->onContentDirty()V

    .line 331
    return-void
.end method
