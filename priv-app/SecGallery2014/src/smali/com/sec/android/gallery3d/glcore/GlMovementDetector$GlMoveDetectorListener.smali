.class public interface abstract Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;
.super Ljava/lang/Object;
.source "GlMovementDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlMovementDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GlMoveDetectorListener"
.end annotation


# virtual methods
.method public abstract onClick()Z
.end method

.method public abstract onGenericMotionTouch(II)Z
.end method

.method public abstract onLongClick(II)Z
.end method

.method public abstract onMove(II)Z
.end method

.method public abstract onPress(II)Z
.end method

.method public abstract onRelease(IIII)Z
.end method

.method public abstract onScroll(IIII)Z
.end method
