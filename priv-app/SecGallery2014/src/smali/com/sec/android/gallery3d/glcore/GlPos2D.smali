.class public Lcom/sec/android/gallery3d/glcore/GlPos2D;
.super Ljava/lang/Object;
.source "GlPos2D.java"


# instance fields
.field public mX:F

.field public mY:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    .line 11
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    .line 12
    return-void
.end method


# virtual methods
.method public set(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 15
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    .line 16
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    .line 17
    return-void
.end method
