.class public Lcom/sec/android/gallery3d/provider/GalleryProvider;
.super Landroid/content/ContentProvider;
.source "GalleryProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;,
        Lcom/sec/android/gallery3d/provider/GalleryProvider$CloudImagePipeDataWriter;,
        Lcom/sec/android/gallery3d/provider/GalleryProvider$ImagePipeDataWriter;,
        Lcom/sec/android/gallery3d/provider/GalleryProvider$PipeDataWriter;,
        Lcom/sec/android/gallery3d/provider/GalleryProvider$PicasaColumns;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.gallery3d.provider"

.field public static final BASE_URI:Landroid/net/Uri;

.field private static final SUPPORTED_PICASA_COLUMNS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "GalleryProvider"

.field private static final THUMB_DATA_PATH:Ljava/lang/String; = "thumb_data_path"

.field private static final THUMB_DATA_URL:Ljava/lang/String; = "thumb_data_url"

.field private static sBaseUri:Landroid/net/Uri;

.field private static final sSyncLock:Ljava/lang/Object;


# instance fields
.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDownloadCache:Lcom/sec/android/gallery3d/data/DownloadCache;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    const-string v0, "content://com.sec.android.gallery3d.provider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->BASE_URI:Landroid/net/Uri;

    .line 72
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "orientation"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "height"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "thumb_data_path"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "thumb_data_url"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->SUPPORTED_PICASA_COLUMNS:[Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->sSyncLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 520
    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/provider/GalleryProvider;)Lcom/sec/android/gallery3d/data/DownloadCache;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/provider/GalleryProvider;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->getDownloadCache()Lcom/sec/android/gallery3d/data/DownloadCache;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 0
    .param p0, "x0"    # Ljava/io/InputStream;
    .param p1, "x1"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->dump(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    return-void
.end method

.method static synthetic access$500()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->sSyncLock:Ljava/lang/Object;

    return-object v0
.end method

.method private static dump(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 4
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 410
    const/16 v2, 0x1000

    new-array v0, v2, [B

    .line 411
    .local v0, "buffer":[B
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 418
    :cond_0
    return-void

    .line 413
    :cond_1
    array-length v2, v0

    invoke-virtual {p0, v0, v3, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 414
    .local v1, "rc":I
    :goto_0
    if-lez v1, :cond_0

    .line 415
    invoke-virtual {p1, v0, v3, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 416
    array-length v2, v0

    invoke-virtual {p0, v0, v3, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    goto :goto_0
.end method

.method public static getAuthority(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".provider"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDownloadCache()Lcom/sec/android/gallery3d/data/DownloadCache;
    .locals 2

    .prologue
    .line 308
    iget-object v1, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->mDownloadCache:Lcom/sec/android/gallery3d/data/DownloadCache;

    if-nez v1, :cond_0

    .line 309
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 310
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDownloadCache()Lcom/sec/android/gallery3d/data/DownloadCache;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->mDownloadCache:Lcom/sec/android/gallery3d/data/DownloadCache;

    .line 312
    .end local v0    # "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->mDownloadCache:Lcom/sec/android/gallery3d/data/DownloadCache;

    return-object v1
.end method

.method public static getUriFor(Landroid/content/Context;Lcom/sec/android/gallery3d/data/Path;)Landroid/net/Uri;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 106
    sget-object v0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->sBaseUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".provider"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->sBaseUri:Landroid/net/Uri;

    .line 109
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->sBaseUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static openPipeHelper(Ljava/lang/Object;Lcom/sec/android/gallery3d/provider/GalleryProvider$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/sec/android/gallery3d/provider/GalleryProvider$PipeDataWriter",
            "<TT;>;)",
            "Landroid/os/ParcelFileDescriptor;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 287
    .local p0, "args":Ljava/lang/Object;, "TT;"
    .local p1, "func":Lcom/sec/android/gallery3d/provider/GalleryProvider$PipeDataWriter;, "Lcom/sec/android/gallery3d/provider/GalleryProvider$PipeDataWriter<TT;>;"
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 288
    .local v1, "pipe":[Landroid/os/ParcelFileDescriptor;
    new-instance v2, Lcom/sec/android/gallery3d/provider/GalleryProvider$1;

    invoke-direct {v2, p1, v1, p0}, Lcom/sec/android/gallery3d/provider/GalleryProvider$1;-><init>(Lcom/sec/android/gallery3d/provider/GalleryProvider$PipeDataWriter;[Landroid/os/ParcelFileDescriptor;Ljava/lang/Object;)V

    .line 299
    .local v2, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;>;"
    const/4 v3, 0x0

    check-cast v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/AsyncTaskUtil;->executeInParallel(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 300
    const/4 v3, 0x0

    aget-object v3, v1, v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 301
    .end local v1    # "pipe":[Landroid/os/ParcelFileDescriptor;
    .end local v2    # "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;>;"
    :catch_0
    move-exception v0

    .line 302
    .local v0, "e":Ljava/io/IOException;
    new-instance v3, Ljava/io/FileNotFoundException;

    const-string v4, "failure making pipe"

    invoke-direct {v3, v4}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private queryCloudItem(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1, "image"    # Lcom/sec/android/gallery3d/remote/cloud/CloudImage;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 365
    if-nez p2, :cond_0

    sget-object p2, Lcom/sec/android/gallery3d/provider/GalleryProvider;->SUPPORTED_PICASA_COLUMNS:[Ljava/lang/String;

    .line 366
    :cond_0
    array-length v10, p2

    new-array v1, v10, [Ljava/lang/Object;

    .line 368
    .local v1, "columnValues":[Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getLatitude()D

    move-result-wide v6

    .line 369
    .local v6, "latitude":D
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getLongitude()D

    move-result-wide v8

    .line 370
    .local v8, "longitude":D
    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v4

    .line 372
    .local v4, "isValidLatlong":Z
    const/4 v3, 0x0

    .local v3, "i":I
    array-length v5, p2

    .local v5, "n":I
    :goto_0
    if-ge v3, v5, :cond_10

    .line 373
    aget-object v0, p2, v3

    .line 374
    .local v0, "column":Ljava/lang/String;
    const-string v10, "_id"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 375
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getItemId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v1, v3

    .line 372
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 376
    :cond_1
    const-string v10, "_data"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 377
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getFilePath()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 378
    :cond_2
    const-string v10, "_display_name"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 379
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 380
    :cond_3
    const-string v10, "_size"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 381
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getSize()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 382
    :cond_4
    const-string v10, "mime_type"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 383
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getMimeType()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 384
    :cond_5
    const-string v10, "picasa_id"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 385
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getSourceId()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 386
    :cond_6
    const-string v10, "datetaken"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 387
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getDateInMs()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 388
    :cond_7
    const-string v10, "latitude"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 389
    if-eqz v4, :cond_8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    :goto_2
    aput-object v10, v1, v3

    goto :goto_1

    :cond_8
    const/4 v10, 0x0

    goto :goto_2

    .line 390
    :cond_9
    const-string v10, "longitude"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 391
    if-eqz v4, :cond_a

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    :goto_3
    aput-object v10, v1, v3

    goto/16 :goto_1

    :cond_a
    const/4 v10, 0x0

    goto :goto_3

    .line 392
    :cond_b
    const-string v10, "orientation"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 393
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getRotation()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 394
    :cond_c
    const-string/jumbo v10, "width"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 395
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getWidth()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 396
    :cond_d
    const-string v10, "height"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 397
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getHeight()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 398
    :cond_e
    const-string/jumbo v10, "thumb_data_path"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 399
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getThumbPath()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 401
    :cond_f
    const-string v10, "GalleryProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "unsupported column: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 404
    .end local v0    # "column":Ljava/lang/String;
    :cond_10
    new-instance v2, Landroid/database/MatrixCursor;

    invoke-direct {v2, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 405
    .local v2, "cursor":Landroid/database/MatrixCursor;
    invoke-virtual {v2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 406
    return-object v2
.end method

.method private queryPicasaItem(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1, "image"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 179
    if-nez p2, :cond_0

    sget-object p2, Lcom/sec/android/gallery3d/provider/GalleryProvider;->SUPPORTED_PICASA_COLUMNS:[Ljava/lang/String;

    .line 180
    :cond_0
    array-length v10, p2

    new-array v1, v10, [Ljava/lang/Object;

    .line 181
    .local v1, "columnValues":[Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getLatitude()D

    move-result-wide v6

    .line 182
    .local v6, "latitude":D
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getLongitude()D

    move-result-wide v8

    .line 183
    .local v8, "longitude":D
    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v4

    .line 185
    .local v4, "isValidLatlong":Z
    const/4 v3, 0x0

    .local v3, "i":I
    array-length v5, p2

    .local v5, "n":I
    :goto_0
    if-ge v3, v5, :cond_11

    .line 186
    aget-object v0, p2, v3

    .line 192
    .local v0, "column":Ljava/lang/String;
    const-string v10, "_id"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 193
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getSourceId()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    .line 185
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 194
    :cond_1
    const-string v10, "_data"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 195
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getSourceUrl()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 198
    :cond_2
    const-string v10, "_display_name"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 199
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 200
    :cond_3
    const-string v10, "_size"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 201
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getSize()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 202
    :cond_4
    const-string v10, "mime_type"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 203
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getMimeType()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 206
    :cond_5
    const-string v10, "picasa_id"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 207
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getSourceId()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 210
    :cond_6
    const-string v10, "datetaken"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 211
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getDateInMs()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 212
    :cond_7
    const-string v10, "latitude"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 213
    if-eqz v4, :cond_8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    :goto_2
    aput-object v10, v1, v3

    goto :goto_1

    :cond_8
    const/4 v10, 0x0

    goto :goto_2

    .line 214
    :cond_9
    const-string v10, "longitude"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 215
    if-eqz v4, :cond_a

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    :goto_3
    aput-object v10, v1, v3

    goto/16 :goto_1

    :cond_a
    const/4 v10, 0x0

    goto :goto_3

    .line 216
    :cond_b
    const-string v10, "orientation"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 217
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getRotation()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 220
    :cond_c
    const-string/jumbo v10, "width"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 221
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getWidth()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 222
    :cond_d
    const-string v10, "height"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 223
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getHeight()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 224
    :cond_e
    const-string/jumbo v10, "thumb_data_path"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 225
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getThumbPath()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 226
    :cond_f
    const-string/jumbo v10, "thumb_data_url"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 227
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getThumbUrl()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 231
    :cond_10
    const-string v10, "GalleryProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "unsupported column: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 235
    .end local v0    # "column":Ljava/lang/String;
    :cond_11
    new-instance v2, Landroid/database/MatrixCursor;

    invoke-direct {v2, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 236
    .local v2, "cursor":Landroid/database/MatrixCursor;
    invoke-virtual {v2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 237
    return-object v2
.end method

.method private querySNSItem(Lcom/sec/android/gallery3d/remote/sns/SNSImage;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1, "image"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 317
    if-nez p2, :cond_0

    sget-object p2, Lcom/sec/android/gallery3d/provider/GalleryProvider;->SUPPORTED_PICASA_COLUMNS:[Ljava/lang/String;

    .line 318
    :cond_0
    array-length v10, p2

    new-array v1, v10, [Ljava/lang/Object;

    .line 320
    .local v1, "columnValues":[Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getLatitude()D

    move-result-wide v6

    .line 321
    .local v6, "latitude":D
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getLongitude()D

    move-result-wide v8

    .line 322
    .local v8, "longitude":D
    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v4

    .line 324
    .local v4, "isValidLatlong":Z
    const/4 v3, 0x0

    .local v3, "i":I
    array-length v5, p2

    .local v5, "n":I
    :goto_0
    if-ge v3, v5, :cond_11

    .line 325
    aget-object v0, p2, v3

    .line 326
    .local v0, "column":Ljava/lang/String;
    const-string v10, "_id"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 327
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getSourceId()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    .line 324
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 328
    :cond_1
    const-string v10, "_data"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 329
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getSourceUrl()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 330
    :cond_2
    const-string v10, "_display_name"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 331
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 332
    :cond_3
    const-string v10, "_size"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 333
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getSize()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 334
    :cond_4
    const-string v10, "mime_type"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 335
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getMimeType()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 336
    :cond_5
    const-string v10, "picasa_id"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 337
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getSourceId()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 338
    :cond_6
    const-string v10, "datetaken"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 339
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getDateInMs()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v1, v3

    goto :goto_1

    .line 340
    :cond_7
    const-string v10, "latitude"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 341
    if-eqz v4, :cond_8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    :goto_2
    aput-object v10, v1, v3

    goto :goto_1

    :cond_8
    const/4 v10, 0x0

    goto :goto_2

    .line 342
    :cond_9
    const-string v10, "longitude"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 343
    if-eqz v4, :cond_a

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    :goto_3
    aput-object v10, v1, v3

    goto/16 :goto_1

    :cond_a
    const/4 v10, 0x0

    goto :goto_3

    .line 344
    :cond_b
    const-string v10, "orientation"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 345
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getRotation()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 346
    :cond_c
    const-string/jumbo v10, "width"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 347
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getWidth()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 348
    :cond_d
    const-string v10, "height"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 349
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getHeight()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 350
    :cond_e
    const-string/jumbo v10, "thumb_data_path"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 351
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getThumbPath()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 352
    :cond_f
    const-string/jumbo v10, "thumb_data_url"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 353
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getThumbUrl()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    goto/16 :goto_1

    .line 355
    :cond_10
    const-string v10, "GalleryProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "unsupported column: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 358
    .end local v0    # "column":Ljava/lang/String;
    :cond_11
    new-instance v2, Landroid/database/MatrixCursor;

    invoke-direct {v2, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 359
    .local v2, "cursor":Landroid/database/MatrixCursor;
    invoke-virtual {v2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 360
    return-object v2
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 116
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 122
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 124
    .local v2, "token":J
    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 125
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v4, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 126
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 128
    :goto_0
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v4

    .line 126
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 128
    .end local v0    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v1    # "path":Lcom/sec/android/gallery3d/data/Path;
    :catchall_0
    move-exception v4

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v4
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 134
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 140
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 141
    const/4 v1, 0x1

    return v1
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 243
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 245
    .local v2, "token":J
    :try_start_0
    const-string/jumbo v4, "w"

    invoke-virtual {p2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 246
    new-instance v4, Ljava/io/FileNotFoundException;

    const-string v5, "cannot open file for write"

    invoke-direct {v4, v5}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    :catchall_0
    move-exception v4

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v4

    .line 248
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 249
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v4, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 250
    .local v0, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v0, :cond_1

    .line 251
    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 253
    :cond_1
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 256
    new-instance v4, Lcom/sec/android/gallery3d/provider/GalleryProvider$ImagePipeDataWriter;

    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .end local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v5, 0x0

    invoke-direct {v4, p0, v0, v5}, Lcom/sec/android/gallery3d/provider/GalleryProvider$ImagePipeDataWriter;-><init>(Lcom/sec/android/gallery3d/provider/GalleryProvider;Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/provider/GalleryProvider$1;)V

    invoke-static {p1, v4}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->openPipeHelper(Ljava/lang/Object;Lcom/sec/android/gallery3d/provider/GalleryProvider$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 269
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_0
    return-object v4

    .line 259
    .restart local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    :try_start_2
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->isSNSImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 260
    new-instance v4, Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;

    check-cast v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .end local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v5, 0x0

    invoke-direct {v4, p0, v0, v5}, Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;-><init>(Lcom/sec/android/gallery3d/provider/GalleryProvider;Lcom/sec/android/gallery3d/remote/sns/SNSImage;Lcom/sec/android/gallery3d/provider/GalleryProvider$1;)V

    invoke-static {p1, v4}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->openPipeHelper(Ljava/lang/Object;Lcom/sec/android/gallery3d/provider/GalleryProvider$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 269
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 261
    .restart local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    :try_start_3
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->isCloudImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 262
    new-instance v4, Lcom/sec/android/gallery3d/provider/GalleryProvider$CloudImagePipeDataWriter;

    check-cast v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    .end local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v5, 0x0

    invoke-direct {v4, p0, v0, v5}, Lcom/sec/android/gallery3d/provider/GalleryProvider$CloudImagePipeDataWriter;-><init>(Lcom/sec/android/gallery3d/provider/GalleryProvider;Lcom/sec/android/gallery3d/remote/cloud/CloudImage;Lcom/sec/android/gallery3d/provider/GalleryProvider$1;)V

    invoke-static {p1, v4}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->openPipeHelper(Ljava/lang/Object;Lcom/sec/android/gallery3d/provider/GalleryProvider$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v4

    .line 269
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 266
    .restart local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    :try_start_4
    new-instance v4, Ljava/io/FileNotFoundException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "unspported type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 148
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v10

    .line 150
    .local v10, "token":J
    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    .line 151
    .local v9, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v3, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3, v9}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v8

    .line 152
    .local v8, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v8, :cond_0

    .line 153
    const-string v3, "GalleryProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot find: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_0
    return-object v2

    .line 156
    :cond_0
    :try_start_1
    invoke-static {v8}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 157
    move-object v0, v8

    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    move-object v3, v0

    move-object v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->queryPicasaItem(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 173
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 161
    :cond_1
    :try_start_2
    instance-of v3, v8, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v3, :cond_2

    .line 162
    move-object v0, v8

    check-cast v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    move-object v3, v0

    move-object v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->querySNSItem(Lcom/sec/android/gallery3d/remote/sns/SNSImage;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 173
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 164
    :cond_2
    :try_start_3
    instance-of v3, v8, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-eqz v3, :cond_3

    .line 165
    move-object v0, v8

    check-cast v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    move-object v3, v0

    move-object v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->queryCloudItem(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    .line 173
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :cond_3
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .end local v8    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v9    # "path":Lcom/sec/android/gallery3d/data/Path;
    :catchall_0
    move-exception v2

    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 275
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
