.class final Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;
.super Ljava/lang/Object;
.source "AccountSettingManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/settings/AccountSettingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "AuthAccount"
.end annotation


# instance fields
.field public final account:Landroid/accounts/Account;

.field public final authToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/accounts/Account;)V
    .locals 0
    .param p1, "authToken"    # Ljava/lang/String;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->authToken:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->account:Landroid/accounts/Account;

    .line 42
    return-void
.end method


# virtual methods
.method public getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->account:Landroid/accounts/Account;

    return-object v0
.end method
