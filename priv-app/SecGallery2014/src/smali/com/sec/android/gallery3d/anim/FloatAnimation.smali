.class public Lcom/sec/android/gallery3d/anim/FloatAnimation;
.super Lcom/sec/android/gallery3d/anim/CanvasAnimation;
.source "FloatAnimation.java"


# instance fields
.field private mCurrent:F

.field private final mFrom:F

.field private final mTo:F


# direct methods
.method public constructor <init>(FFI)V
    .locals 0
    .param p1, "from"    # F
    .param p2, "to"    # F
    .param p3, "duration"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/gallery3d/anim/CanvasAnimation;-><init>()V

    .line 28
    iput p1, p0, Lcom/sec/android/gallery3d/anim/FloatAnimation;->mFrom:F

    .line 29
    iput p2, p0, Lcom/sec/android/gallery3d/anim/FloatAnimation;->mTo:F

    .line 30
    iput p1, p0, Lcom/sec/android/gallery3d/anim/FloatAnimation;->mCurrent:F

    .line 31
    invoke-virtual {p0, p3}, Lcom/sec/android/gallery3d/anim/FloatAnimation;->setDuration(I)V

    .line 32
    return-void
.end method


# virtual methods
.method public apply(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 0
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 53
    return-void
.end method

.method public get()F
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/android/gallery3d/anim/FloatAnimation;->mCurrent:F

    return v0
.end method

.method public getCanvasSaveFlags()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method protected onCalculate(F)V
    .locals 3
    .param p1, "progress"    # F

    .prologue
    .line 36
    iget v0, p0, Lcom/sec/android/gallery3d/anim/FloatAnimation;->mFrom:F

    iget v1, p0, Lcom/sec/android/gallery3d/anim/FloatAnimation;->mTo:F

    iget v2, p0, Lcom/sec/android/gallery3d/anim/FloatAnimation;->mFrom:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/anim/FloatAnimation;->mCurrent:F

    .line 37
    return-void
.end method
