.class public Lcom/sec/android/gallery3d/util/LightCycleHelper;
.super Ljava/lang/Object;
.source "LightCycleHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/util/LightCycleHelper$PanoramaMetadata;
    }
.end annotation


# static fields
.field public static final NOT_PANORAMA:Lcom/sec/android/gallery3d/util/LightCycleHelper$PanoramaMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    new-instance v0, Lcom/sec/android/gallery3d/util/LightCycleHelper$PanoramaMetadata;

    invoke-direct {v0, v1, v1}, Lcom/sec/android/gallery3d/util/LightCycleHelper$PanoramaMetadata;-><init>(ZZ)V

    sput-object v0, Lcom/sec/android/gallery3d/util/LightCycleHelper;->NOT_PANORAMA:Lcom/sec/android/gallery3d/util/LightCycleHelper$PanoramaMetadata;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public static getModifiedTimeFromURI(Landroid/content/ContentResolver;Landroid/net/Uri;)J
    .locals 2
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 53
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public static getPanoramaMetadata(Landroid/content/Context;Landroid/net/Uri;)Lcom/sec/android/gallery3d/util/LightCycleHelper$PanoramaMetadata;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/gallery3d/util/LightCycleHelper;->NOT_PANORAMA:Lcom/sec/android/gallery3d/util/LightCycleHelper$PanoramaMetadata;

    return-object v0
.end method

.method public static getPathFromURI(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 46
    const/4 v0, 0x0

    return-object v0
.end method
