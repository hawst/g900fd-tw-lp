.class public Lcom/sec/android/gallery3d/ui/TiledScreenNail;
.super Ljava/lang/Object;
.source "TiledScreenNail.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/ScreenNail;


# static fields
.field private static final ANIMATION_DONE:J = -0x3L

.field private static final ANIMATION_NEEDED:J = -0x2L

.field private static final ANIMATION_NOT_NEEDED:J = -0x1L

.field private static final DURATION:I = 0xb4

.field private static final TAG:Ljava/lang/String; = "TiledScreenNail"

.field private static mDrawPlaceholder:Z

.field private static mPlaceholderColor:I

.field private static sMaxSide:I


# instance fields
.field private mAnimationStartTime:J

.field public mBitmap:Landroid/graphics/Bitmap;

.field private mHeight:I

.field private mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0x280

    sput v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->sMaxSide:I

    .line 69
    const/high16 v0, -0x1000000

    sput v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mPlaceholderColor:I

    .line 70
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mDrawPlaceholder:Z

    return-void
.end method

.method public constructor <init>(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mAnimationStartTime:J

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->setSize(II)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "resource"    # Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mAnimationStartTime:J

    .line 57
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mWidth:I

    .line 58
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mHeight:I

    .line 59
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    .line 60
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;-><init>(Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    .line 61
    return-void
.end method

.method public static disableDrawPlaceholder()V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mDrawPlaceholder:Z

    .line 149
    return-void
.end method

.method public static enableDrawPlaceholder()V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mDrawPlaceholder:Z

    .line 153
    return-void
.end method

.method private getRatio()F
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 204
    invoke-static {}, Lcom/sec/android/gallery3d/ui/AnimationTime;->get()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mAnimationStartTime:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    const/high16 v2, 0x43340000    # 180.0f

    div-float v0, v1, v2

    .line 205
    .local v0, "r":F
    sub-float v1, v6, v0

    const/4 v2, 0x0

    invoke-static {v1, v2, v6}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v1

    return v1
.end method

.method public static setMaxSide(I)V
    .locals 0
    .param p0, "size"    # I

    .prologue
    .line 217
    sput p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->sMaxSide:I

    .line 218
    return-void
.end method

.method public static setPlaceholderColor(I)V
    .locals 0
    .param p0, "color"    # I

    .prologue
    .line 73
    sput p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mPlaceholderColor:I

    .line 74
    return-void
.end method

.method private setSize(II)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 77
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 78
    :cond_0
    sget p1, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->sMaxSide:I

    .line 79
    sget v1, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->sMaxSide:I

    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 p2, v1, 0x4

    .line 81
    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    sget v2, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->sMaxSide:I

    int-to-float v2, v2

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 82
    .local v0, "scale":F
    int-to-float v1, p1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mWidth:I

    .line 83
    int-to-float v1, p2

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mHeight:I

    .line 84
    return-void
.end method


# virtual methods
.method public combine(Lcom/sec/android/gallery3d/ui/ScreenNail;)Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 4
    .param p1, "other"    # Lcom/sec/android/gallery3d/ui/ScreenNail;

    .prologue
    const/4 v3, 0x0

    .line 89
    if-nez p1, :cond_0

    .line 112
    .end local p0    # "this":Lcom/sec/android/gallery3d/ui/TiledScreenNail;
    :goto_0
    return-object p0

    .line 93
    .restart local p0    # "this":Lcom/sec/android/gallery3d/ui/TiledScreenNail;
    :cond_0
    instance-of v1, p1, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    if-nez v1, :cond_1

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->recycle()V

    move-object p0, p1

    .line 95
    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 100
    check-cast v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    .line 101
    .local v0, "newer":Lcom/sec/android/gallery3d/ui/TiledScreenNail;
    iget v1, v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mWidth:I

    iput v1, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mWidth:I

    .line 102
    iget v1, v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mHeight:I

    iput v1, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mHeight:I

    .line 103
    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-eqz v1, :cond_4

    .line 104
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/sec/android/photos/data/GalleryBitmapPool;->put(Landroid/graphics/Bitmap;)Z

    .line 105
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->recycle()V

    .line 106
    :cond_3
    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    .line 107
    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    .line 108
    iput-object v3, v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    .line 109
    iput-object v3, v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    .line 111
    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->recycle()V

    goto :goto_0
.end method

.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V
    .locals 8
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-nez v0, :cond_2

    .line 158
    iget-wide v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mAnimationStartTime:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 161
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mDrawPlaceholder:Z

    if-eqz v0, :cond_1

    .line 162
    int-to-float v1, p2

    int-to-float v2, p3

    int-to-float v3, p4

    int-to-float v4, p5

    sget v5, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mPlaceholderColor:I

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 177
    :cond_1
    :goto_0
    return-void

    .line 167
    :cond_2
    iget-wide v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mAnimationStartTime:J

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 168
    invoke-static {}, Lcom/sec/android/gallery3d/ui/AnimationTime;->get()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mAnimationStartTime:J

    .line 171
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 172
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    sget v2, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mPlaceholderColor:I

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->getRatio()F

    move-result v3

    move-object v1, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->drawMixed(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IFIIII)V

    goto :goto_0

    .line 175
    :cond_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto :goto_0
.end method

.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "source"    # Landroid/graphics/RectF;
    .param p3, "dest"    # Landroid/graphics/RectF;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->isReady()Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    :cond_0
    iget v1, p3, Landroid/graphics/RectF;->left:F

    iget v2, p3, Landroid/graphics/RectF;->top:F

    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v4

    sget v5, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mPlaceholderColor:I

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 188
    :goto_0
    return-void

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mHeight:I

    return v0
.end method

.method public getTexture()Lcom/sec/android/gallery3d/glrenderer/TiledTexture;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mWidth:I

    return v0
.end method

.method public isAnimating()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 194
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->isReady()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 200
    :cond_1
    :goto_0
    return v0

    .line 195
    :cond_2
    iget-wide v2, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mAnimationStartTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 196
    invoke-static {}, Lcom/sec/android/gallery3d/ui/AnimationTime;->get()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mAnimationStartTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0xb4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    .line 197
    const-wide/16 v2, -0x3

    iput-wide v2, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mAnimationStartTime:J

    goto :goto_0

    :cond_3
    move v0, v1

    .line 200
    goto :goto_0
.end method

.method public isShowingPlaceholder()Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public noDraw()V
    .locals 0

    .prologue
    .line 133
    return-void
.end method

.method public recycle()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 137
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->recycle()V

    .line 139
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 142
    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/android/photos/data/GalleryBitmapPool;->put(Landroid/graphics/Bitmap;)Z

    .line 143
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    .line 145
    :cond_1
    return-void
.end method

.method public updatePlaceholderSize(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 118
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->setSize(II)V

    goto :goto_0
.end method
