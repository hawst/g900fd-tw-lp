.class Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;
.super Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;
.source "TiledTexture.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glrenderer/TiledTexture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Tile"
.end annotation


# instance fields
.field public bitmap:Landroid/graphics/Bitmap;

.field public contentHeight:I

.field public contentWidth:I

.field public nextFreeTile:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;

.field public offsetX:I

.field public offsetY:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;Lcom/sec/android/gallery3d/glrenderer/TiledTexture$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/glrenderer/TiledTexture;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/glrenderer/TiledTexture$1;

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;-><init>(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)V

    return-void
.end method


# virtual methods
.method protected onFreeBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 164
    return-void
.end method

.method protected onGetBitmap()Landroid/graphics/Bitmap;
    .locals 13

    .prologue
    const/16 v12, 0xfe

    const/high16 v4, 0x43800000    # 256.0f

    const/4 v2, 0x0

    .line 140
    iget-object v8, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->bitmap:Landroid/graphics/Bitmap;

    .line 143
    .local v8, "localBitmapRef":Landroid/graphics/Bitmap;
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 144
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->offsetX:I

    rsub-int/lit8 v10, v0, 0x1

    .line 145
    .local v10, "x":I
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->offsetY:I

    rsub-int/lit8 v11, v0, 0x1

    .line 146
    .local v11, "y":I
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    add-int v9, v0, v10

    .line 147
    .local v9, "r":I
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    add-int v7, v0, v11

    .line 148
    .local v7, "b":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    # getter for: Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->sCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->access$200(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)Landroid/graphics/Canvas;

    move-result-object v0

    int-to-float v1, v10

    int-to-float v3, v11

    iget-object v5, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    # getter for: Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->sBitmapPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->access$100(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual {v0, v8, v1, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 149
    const/4 v8, 0x0

    .line 152
    if-lez v10, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    # getter for: Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->sCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->access$200(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)Landroid/graphics/Canvas;

    move-result-object v0

    add-int/lit8 v1, v10, -0x1

    int-to-float v1, v1

    add-int/lit8 v3, v10, -0x1

    int-to-float v3, v3

    iget-object v5, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    # getter for: Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->sPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->access$300(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 153
    :cond_0
    if-lez v11, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    # getter for: Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->sCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->access$200(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)Landroid/graphics/Canvas;

    move-result-object v1

    add-int/lit8 v0, v11, -0x1

    int-to-float v3, v0

    add-int/lit8 v0, v11, -0x1

    int-to-float v5, v0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    # getter for: Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->sPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->access$300(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 154
    :cond_1
    if-ge v9, v12, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    # getter for: Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->sCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->access$200(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)Landroid/graphics/Canvas;

    move-result-object v0

    int-to-float v1, v9

    int-to-float v3, v9

    iget-object v5, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    # getter for: Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->sPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->access$300(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 155
    :cond_2
    if-ge v7, v12, :cond_3

    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    # getter for: Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->sCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->access$200(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)Landroid/graphics/Canvas;

    move-result-object v1

    int-to-float v3, v7

    int-to-float v5, v7

    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    # getter for: Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->sPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->access$300(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 158
    .end local v7    # "b":I
    .end local v9    # "r":I
    .end local v10    # "x":I
    .end local v11    # "y":I
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->this$0:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    # getter for: Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->sUploadBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->access$400(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public setSize(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/16 v1, 0x100

    .line 128
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->contentWidth:I

    .line 129
    iput p2, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->contentHeight:I

    .line 130
    add-int/lit8 v0, p1, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->mWidth:I

    .line 131
    add-int/lit8 v0, p2, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->mHeight:I

    .line 132
    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->mTextureWidth:I

    .line 133
    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Tile;->mTextureHeight:I

    .line 134
    return-void
.end method
