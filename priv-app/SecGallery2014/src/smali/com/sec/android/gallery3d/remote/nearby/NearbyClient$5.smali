.class Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$5;
.super Ljava/lang/Object;
.source "NearbyClient.java"

# interfaces
.implements Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->changePlayer(ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V
    .locals 0

    .prologue
    .line 551
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$5;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceChanged(Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "imgViewerState"    # Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    .param p2, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 554
    if-eqz p1, :cond_0

    .line 555
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDeviceChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq p2, v0, :cond_1

    .line 557
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "        Error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->SHOWING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    if-ne p1, v0, :cond_3

    .line 570
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$5;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refreshChangePlayerIcon()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$900(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V

    .line 571
    return-void

    .line 564
    :cond_3
    sget-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->STOPPED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    if-eq p1, v0, :cond_2

    .line 565
    sget-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->CONTENT_CHANGED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    if-ne p1, v0, :cond_2

    .line 566
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$5;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1002(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/samsung/android/allshare/media/ImageViewer;)Lcom/samsung/android/allshare/media/ImageViewer;

    .line 567
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$5;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1102(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z

    goto :goto_0
.end method
