.class final Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;
.super Ljava/lang/Object;
.source "SNSContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SyncContext"
.end annotation


# instance fields
.field public albumsChanged:Z

.field public photosChanged:Z

.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 682
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 684
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->albumsChanged:Z

    .line 687
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->photosChanged:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$1;

    .prologue
    .line 682
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;-><init>(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;)V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 700
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 701
    .local v0, "cr":Landroid/content/ContentResolver;
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->albumsChanged:Z

    if-eqz v1, :cond_0

    .line 702
    sget-object v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 704
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->photosChanged:Z

    if-eqz v1, :cond_1

    .line 705
    sget-object v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 707
    :cond_1
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->albumsChanged:Z

    .line 708
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->photosChanged:Z

    .line 709
    return-void
.end method

.method public getDb()Landroid/database/sqlite/SQLiteDatabase;
    .locals 3

    .prologue
    .line 691
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;

    # getter for: Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mDatabase:Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->access$100(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;)Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 694
    :goto_0
    return-object v1

    .line 692
    :catch_0
    move-exception v0

    .line 693
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SNSContentProvider"

    const-string v2, "Unable to get DB"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 694
    const/4 v1, 0x0

    goto :goto_0
.end method
