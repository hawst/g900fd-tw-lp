.class public Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;
.super Ljava/lang/Object;
.source "SNSSyncAdapter.java"


# static fields
.field private static final AUTHORITY_FACEBOOK:Ljava/lang/String; = "com.sec.android.app.sns3.sp.facebook"

.field private static final TAG:Ljava/lang/String; = "SNSSyncAdapter"

.field private static final sSyncLock:Ljava/lang/Object;

.field private static sSyncPending:Z

.field private static sSyncRunning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->sSyncLock:Ljava/lang/Object;

    .line 40
    sput-boolean v1, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->sSyncRunning:Z

    .line 41
    sput-boolean v1, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->sSyncPending:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getContentProvider(Landroid/content/Context;)Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 199
    const/4 v0, 0x0

    .line 201
    .local v0, "client":Landroid/content/ContentProviderClient;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 202
    .local v1, "cr":Landroid/content/ContentResolver;
    const-string v3, "com.sec.android.gallery3d.sns.contentprovider"

    invoke-virtual {v1, v3}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 203
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .end local v1    # "cr":Landroid/content/ContentResolver;
    :cond_0
    :goto_0
    return-object v3

    .line 204
    :catch_0
    move-exception v2

    .line 205
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206
    const/4 v3, 0x0

    .line 208
    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_0

    .line 208
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_1

    .line 209
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_1
    throw v3
.end method

.method public static performSync(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;ZJ)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumUri"    # Landroid/net/Uri;
    .param p2, "photoUri"    # Landroid/net/Uri;
    .param p3, "stopCurrent"    # Z
    .param p4, "id"    # J

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 60
    const-string v3, "SNSSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "perfornSync enter : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :goto_0
    sget-object v3, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->sSyncLock:Ljava/lang/Object;

    monitor-enter v3

    .line 65
    :try_start_0
    sget-boolean v4, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->sSyncRunning:Z

    if-eqz v4, :cond_2

    .line 66
    if-nez p3, :cond_0

    monitor-exit v3

    .line 93
    :goto_1
    return v1

    .line 67
    :cond_0
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->sSyncPending:Z

    .line 70
    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->getContentProvider(Landroid/content/Context;)Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;

    move-result-object v0

    .line 71
    .local v0, "provider":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;
    if-eqz v0, :cond_1

    .line 72
    invoke-virtual {v0, p3}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->setStopCurrentSyncing(Z)V

    .line 74
    :cond_1
    const-string v2, "SNSSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "current is blocked : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    monitor-exit v3

    goto :goto_1

    .line 80
    .end local v0    # "provider":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 78
    :cond_2
    const/4 v4, 0x1

    :try_start_1
    sput-boolean v4, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->sSyncRunning:Z

    .line 79
    const/4 v4, 0x0

    sput-boolean v4, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->sSyncPending:Z

    .line 80
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    :try_start_2
    invoke-static {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->syncAlbums(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 89
    :goto_2
    sget-object v3, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->sSyncLock:Ljava/lang/Object;

    monitor-enter v3

    .line 90
    const/4 v4, 0x0

    :try_start_3
    sput-boolean v4, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->sSyncRunning:Z

    .line 91
    sget-boolean v4, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->sSyncPending:Z

    if-eqz v4, :cond_3

    monitor-exit v3

    goto :goto_0

    .line 94
    :catchall_1
    move-exception v1

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    .line 92
    :cond_3
    :try_start_4
    const-string v1, "SNSSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " sync done : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move v1, v2

    goto :goto_1

    .line 85
    :catch_0
    move-exception v3

    goto :goto_2
.end method

.method public static setAlbumCachingFlag(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "flag"    # I

    .prologue
    .line 100
    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->getContentProvider(Landroid/content/Context;)Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;

    move-result-object v0

    .line 101
    .local v0, "provider":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;
    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->setAlbumCachingFlag(Ljava/lang/String;I)V

    .line 104
    :cond_0
    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->startSync(Landroid/content/Context;)V

    .line 105
    return-void
.end method

.method public static startSync(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceBook:Z

    if-nez v0, :cond_0

    .line 57
    :goto_0
    return-void

    .line 47
    :cond_0
    const-string v0, "SNSSyncAdapter"

    const-string/jumbo v1, "startSync"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    new-instance v0, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter$1;

    const-string v1, "SNSSync"

    invoke-direct {v0, v1, p0}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter$1;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter$1;->start()V

    goto :goto_0
.end method

.method public static syncAlbums(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumUri"    # Landroid/net/Uri;
    .param p2, "photoUri"    # Landroid/net/Uri;

    .prologue
    .line 109
    const-string v0, "SNSSyncAdapter"

    const-string/jumbo v1, "syncAlbums start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const/4 v8, 0x0

    .line 111
    .local v8, "cursor":Landroid/database/Cursor;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v7, "albumEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/AlbumTable;>;"
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/sec/android/gallery3d/remote/sns/SyncDatabaseInfo;->SNS_ALBUM_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 115
    if-nez v8, :cond_1

    .line 125
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    :goto_1
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 119
    new-instance v6, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    invoke-direct {v6}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;-><init>()V

    .line 120
    .local v6, "album":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->clear()V

    .line 121
    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->setProperty(Ljava/lang/Object;)V

    .line 122
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 125
    .end local v6    # "album":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    :cond_2
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 131
    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->getContentProvider(Landroid/content/Context;)Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;

    move-result-object v9

    .line 132
    .local v9, "provider":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;
    if-eqz v9, :cond_0

    .line 133
    invoke-virtual {v9, v7, p2}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->updateAlbums(Ljava/util/ArrayList;Landroid/net/Uri;)V

    .line 134
    const-string v0, "SNSSyncAdapter"

    const-string/jumbo v1, "syncAlbums end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static syncPhotos(Landroid/content/Context;Landroid/net/Uri;Lcom/sec/android/gallery3d/remote/sns/AlbumTable;)V
    .locals 25
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "parentAlbum"    # Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    .prologue
    .line 138
    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->source_id:Ljava/lang/String;

    .line 140
    .local v10, "albumId":Ljava/lang/String;
    const-string v4, "SNSSyncAdapter"

    const-string/jumbo v5, "syncPhotos start"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-static/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->getContentProvider(Landroid/content/Context;)Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;

    move-result-object v22

    .line 142
    .local v22, "provider":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;
    if-nez v22, :cond_0

    .line 143
    const-string v4, "SNSSyncAdapter"

    const-string v5, "SNS provider is null. ignore sync"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :goto_0
    return-void

    .line 147
    :cond_0
    const/4 v11, 0x0

    .line 148
    .local v11, "cursor":Landroid/database/Cursor;
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    .line 150
    .local v21, "photoMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/sec/android/gallery3d/remote/sns/PhotoTable;>;"
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    sget-object v6, Lcom/sec/android/gallery3d/remote/sns/SyncDatabaseInfo;->SNS_PHOTO_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 152
    if-nez v11, :cond_2

    .line 180
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 162
    :cond_1
    const/4 v4, 0x0

    :try_start_1
    invoke-interface {v11, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 163
    .local v18, "id":J
    const/16 v4, 0xe

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    .line 164
    .local v24, "w":I
    const/16 v4, 0xf

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 165
    .local v15, "h":I
    const/16 v4, 0x10

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 167
    .local v23, "url":Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 168
    new-instance v17, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;-><init>()V

    .line 169
    .local v17, "p":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->setProperty(Ljava/lang/Object;)V

    .line 170
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    move-object/from16 v0, v17

    move/from16 v1, v24

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v15, v2}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->addImageInfo(IILjava/lang/String;)V

    .line 154
    .end local v15    # "h":I
    .end local v17    # "p":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    .end local v18    # "id":J
    .end local v23    # "url":Ljava/lang/String;
    .end local v24    # "w":I
    :cond_2
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 155
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->isStopCurrentSyncing()Z

    move-result v4

    if-nez v4, :cond_3

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->allowDownload(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 156
    :cond_3
    if-eqz v21, :cond_4

    .line 157
    invoke-virtual/range {v21 .. v21}, Ljava/util/HashMap;->clear()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180
    :cond_4
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 173
    .restart local v15    # "h":I
    .restart local v18    # "id":J
    .restart local v23    # "url":Ljava/lang/String;
    .restart local v24    # "w":I
    :cond_5
    :try_start_2
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    .line 174
    .restart local v17    # "p":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    move-object/from16 v0, v17

    move/from16 v1, v24

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v15, v2}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->addImageInfo(IILjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 177
    .end local v15    # "h":I
    .end local v17    # "p":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    .end local v18    # "id":J
    .end local v23    # "url":Ljava/lang/String;
    .end local v24    # "w":I
    :catch_0
    move-exception v12

    .line 178
    .local v12, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 180
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 183
    .end local v12    # "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual/range {v21 .. v21}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v13

    .line 184
    .local v13, "entries":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/gallery3d/remote/sns/PhotoTable;>;"
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 185
    .local v20, "photoEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/PhotoTable;>;"
    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    .line 186
    .local v14, "entry":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    invoke-virtual {v14}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->valid()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 187
    invoke-virtual {v14}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->setMoreProperty()V

    .line 188
    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 180
    .end local v13    # "entries":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/gallery3d/remote/sns/PhotoTable;>;"
    .end local v14    # "entry":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v20    # "photoEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/PhotoTable;>;"
    :cond_7
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_2

    :catchall_0
    move-exception v4

    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v4

    .line 192
    .restart local v13    # "entries":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/gallery3d/remote/sns/PhotoTable;>;"
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v20    # "photoEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/PhotoTable;>;"
    :cond_8
    invoke-virtual/range {v21 .. v21}, Ljava/util/HashMap;->clear()V

    .line 194
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->updatePhotos(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/remote/sns/AlbumTable;)V

    .line 195
    const-string v4, "SNSSyncAdapter"

    const-string/jumbo v5, "syncPhotos end"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
