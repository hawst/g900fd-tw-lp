.class public Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaItem;
.source "PicasaImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$1;,
        Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaLargeImageRequest;,
        Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;,
        Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$DirectDecodeRequest;
    }
.end annotation


# static fields
.field public static final DOWNLOAD_DIR:Ljava/io/File;

.field private static final INVALID_VIDEO_THUMBNIAL_SIZE:I = 0x1194

.field public static final ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final TAG:Ljava/lang/String; = "PicasaImage"


# instance fields
.field private mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

.field private mIsVideo:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "download"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->DOWNLOAD_DIR:Ljava/io/File;

    .line 67
    const-string v0, "/picasa/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "entry"    # Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 76
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .line 77
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentType:Ljava/lang/String;

    const-string/jumbo v1, "video/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mIsVideo:Z

    .line 78
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/data/DownloadCache$Entry;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->rotateFullImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/data/DownloadCache$Entry;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mIsVideo:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "x2"    # Landroid/graphics/Bitmap;
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->refreshVideoThumbnail(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;I)Ljava/net/URL;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    .param p1, "x1"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getPhotoUrl(I)Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method public static find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;J)Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    .locals 2
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "id"    # J

    .prologue
    .line 81
    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, p2, p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->getPhotoEntry(Landroid/content/ContentResolver;J)Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    move-result-object v0

    .line 82
    .local v0, "entry":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    if-nez v0, :cond_0

    .line 83
    const/4 v1, 0x0

    .line 84
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    invoke-direct {v1, p0, p1, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;)V

    goto :goto_0
.end method

.method private getPhotoUrl(I)Ljava/net/URL;
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 262
    const/4 v1, 0x0

    .line 264
    .local v1, "url":Ljava/net/URL;
    packed-switch p1, :pswitch_data_0

    .line 273
    :try_start_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 275
    .end local v1    # "url":Ljava/net/URL;
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/net/MalformedURLException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 267
    .end local v0    # "e":Ljava/net/MalformedURLException;
    .restart local v1    # "url":Ljava/net/URL;
    :pswitch_0
    :try_start_1
    new-instance v1, Ljava/net/URL;

    .end local v1    # "url":Ljava/net/URL;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->thumbnailUrl:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 278
    .restart local v1    # "url":Ljava/net/URL;
    :goto_0
    return-object v1

    .line 270
    :pswitch_1
    new-instance v1, Ljava/net/URL;

    .end local v1    # "url":Ljava/net/URL;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->screennailUrl:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    .line 271
    .restart local v1    # "url":Ljava/net/URL;
    goto :goto_0

    .line 264
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getRotationFromExif(Ljava/lang/String;)I
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 351
    const/4 v4, -0x1

    .line 353
    .local v4, "roation":I
    const/4 v1, 0x0

    .line 355
    .local v1, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "exif":Landroid/media/ExifInterface;
    .local v2, "exif":Landroid/media/ExifInterface;
    move-object v1, v2

    .line 360
    .end local v2    # "exif":Landroid/media/ExifInterface;
    .restart local v1    # "exif":Landroid/media/ExifInterface;
    :goto_0
    if-eqz v1, :cond_0

    .line 361
    const-string v5, "Orientation"

    const/4 v6, -0x1

    invoke-virtual {v1, v5, v6}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v3

    .line 362
    .local v3, "exifOrientation":I
    packed-switch v3, :pswitch_data_0

    .line 376
    :pswitch_0
    const-string v5, "PicasaImage"

    const-string v6, "can\'t find proper orientation"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    .end local v3    # "exifOrientation":I
    :goto_1
    return v4

    .line 356
    :catch_0
    move-exception v0

    .line 357
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 364
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v3    # "exifOrientation":I
    :pswitch_1
    const/4 v4, 0x0

    .line 365
    goto :goto_1

    .line 367
    :pswitch_2
    const/16 v4, 0x5a

    .line 368
    goto :goto_1

    .line 370
    :pswitch_3
    const/16 v4, 0xb4

    .line 371
    goto :goto_1

    .line 373
    :pswitch_4
    const/16 v4, 0x10e

    .line 374
    goto :goto_1

    .line 379
    .end local v3    # "exifOrientation":I
    :cond_0
    const-string v5, "PicasaImage"

    const-string v6, "can\'t find exif data"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 362
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private refreshVideoThumbnail(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "type"    # I
    .param p4, "length"    # I

    .prologue
    const/16 v3, 0x1194

    const/4 v4, 0x2

    const/4 v10, 0x3

    .line 224
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mIsVideo:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 225
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    .line 226
    .local v6, "fileExsit":Z
    if-eqz v6, :cond_4

    if-gt p4, v3, :cond_4

    .line 227
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget v1, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->size:I

    if-le v1, v3, :cond_6

    .line 228
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 229
    .local v7, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v7, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 230
    if-ne p3, v10, :cond_2

    move v1, v4

    :goto_0
    invoke-static {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v9

    .line 231
    .local v9, "targetSize":I
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 232
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    .line 234
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    invoke-static {p1, v1, v7, v9, p3}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodeThumbnail(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 236
    const/4 v5, 0x0

    .line 237
    .local v5, "bitmapaArray":[B
    if-eqz p2, :cond_1

    .line 238
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->compressToBytes(Landroid/graphics/Bitmap;)[B

    move-result-object v5

    .line 239
    :cond_1
    if-nez v5, :cond_3

    .line 240
    const/4 v1, 0x0

    .line 258
    .end local v5    # "bitmapaArray":[B
    .end local v6    # "fileExsit":Z
    .end local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v9    # "targetSize":I
    :goto_1
    return-object v1

    .restart local v6    # "fileExsit":Z
    .restart local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_2
    move v1, p3

    .line 230
    goto :goto_0

    .line 241
    .restart local v5    # "bitmapaArray":[B
    .restart local v9    # "targetSize":I
    :cond_3
    array-length v1, v5

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 242
    .local v8, "target":Ljava/nio/ByteBuffer;
    invoke-virtual {v8, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 244
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v0

    .line 245
    .local v0, "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getDateInMs()J

    move-result-wide v2

    if-ne p3, v10, :cond_5

    :goto_2
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ImageCacheService;->putImageData(Lcom/sec/android/gallery3d/data/Path;JI[B)V

    .line 247
    if-ne p3, v10, :cond_4

    .line 248
    invoke-static {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-static {p2, v1, v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object p2

    .end local v0    # "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    .end local v5    # "bitmapaArray":[B
    .end local v6    # "fileExsit":Z
    .end local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v8    # "target":Ljava/nio/ByteBuffer;
    .end local v9    # "targetSize":I
    :cond_4
    :goto_3
    move-object v1, p2

    .line 258
    goto :goto_1

    .restart local v0    # "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    .restart local v5    # "bitmapaArray":[B
    .restart local v6    # "fileExsit":Z
    .restart local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v8    # "target":Ljava/nio/ByteBuffer;
    .restart local v9    # "targetSize":I
    :cond_5
    move v4, p3

    .line 245
    goto :goto_2

    .line 251
    .end local v0    # "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    .end local v5    # "bitmapaArray":[B
    .end local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v8    # "target":Ljava/nio/ByteBuffer;
    .end local v9    # "targetSize":I
    :cond_6
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_4

    .line 252
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    .line 253
    const/4 p2, 0x0

    goto :goto_3
.end method

.method private rotateFullImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/data/DownloadCache$Entry;)V
    .locals 13
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "entry"    # Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    .prologue
    .line 308
    iget-object v10, p2, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    .line 310
    .local v6, "originalFilePath":Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getRotationFromExif(Ljava/lang/String;)I

    move-result v2

    .line 311
    .local v2, "fullImageRotation":I
    if-lez v2, :cond_2

    .line 312
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 313
    .local v5, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v10, v5, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 314
    invoke-static {p1, v6, v5}, Lcom/sec/android/gallery3d/data/DecodeUtils;->requestDecode(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 315
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_5

    .line 316
    iget-object v10, p2, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v7

    .line 317
    .local v7, "rotatedFileDir":Ljava/lang/String;
    iget-object v10, p2, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, ".tmp"

    const-string v12, "_rotated.tmp"

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 318
    .local v8, "rotatedFileName":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 321
    .local v9, "rotatedFilePath":Ljava/lang/String;
    const/4 v10, 0x1

    :try_start_0
    invoke-static {v0, v2, v10}, Lcom/sec/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 322
    invoke-static {v7, v8, v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->saveBitmapFile(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 323
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 324
    .local v3, "newFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 325
    iget-object v4, p2, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    .line 326
    .local v4, "oldFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 327
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 330
    :cond_0
    invoke-virtual {v3, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 331
    const-string v10, "PicasaImage"

    const-string v11, "full image rotation is succeed"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 343
    .end local v3    # "newFile":Ljava/io/File;
    .end local v4    # "oldFile":Ljava/io/File;
    :cond_1
    :goto_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 348
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v7    # "rotatedFileDir":Ljava/lang/String;
    .end local v8    # "rotatedFileName":Ljava/lang/String;
    .end local v9    # "rotatedFilePath":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 333
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v3    # "newFile":Ljava/io/File;
    .restart local v4    # "oldFile":Ljava/io/File;
    .restart local v5    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v7    # "rotatedFileDir":Ljava/lang/String;
    .restart local v8    # "rotatedFileName":Ljava/lang/String;
    .restart local v9    # "rotatedFilePath":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v10, "PicasaImage"

    const-string v11, "rename fail!"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 339
    .end local v3    # "newFile":Ljava/io/File;
    .end local v4    # "oldFile":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 340
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 337
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_2
    const-string v10, "PicasaImage"

    const-string v11, "can\'t save rotated image"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 345
    .end local v7    # "rotatedFileDir":Ljava/lang/String;
    .end local v8    # "rotatedFileName":Ljava/lang/String;
    .end local v9    # "rotatedFilePath":Ljava/lang/String;
    :cond_5
    const-string v10, "PicasaImage"

    const-string v11, "can\'t decode full image"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public download(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 561
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 562
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/app/DownloadManager$Request;

    invoke-direct {v2, v1}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    move-result-object v2

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/app/DownloadManager$Request;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    move-result-object v0

    .line 564
    .local v0, "request":Landroid/app/DownloadManager$Request;
    invoke-static {p1, v0}, Lcom/sec/samsung/gallery/util/DownloadUtil;->download(Landroid/content/Context;Landroid/app/DownloadManager$Request;)V

    .line 565
    return-void
.end method

.method public getAlbumId()J
    .locals 2

    .prologue
    .line 470
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->albumId:J

    return-wide v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 459
    sget-object v0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDateInMs()J
    .locals 2

    .prologue
    .line 397
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateTaken:J

    return-wide v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 479
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 480
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->title:Ljava/lang/String;

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 484
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateTaken:J

    invoke-static {v2, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 486
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->width:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 487
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->height:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 488
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 489
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->size:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 490
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 491
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->extractExifInfo(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)V

    .line 493
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->summary:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->summary:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    .line 494
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->summary:Ljava/lang/String;

    invoke-virtual {v0, v7, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 496
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v2, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->latitude:D

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v4, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->longitude:D

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 497
    const/16 v1, 0x9

    new-array v2, v7, [D

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v4, v4, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->latitude:D

    aput-wide v4, v2, v3

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->longitude:D

    aput-wide v4, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 502
    :cond_2
    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 536
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 556
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    if-nez v0, :cond_0

    .line 529
    const/4 v0, 0x0

    .line 531
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->height:I

    goto :goto_0
.end method

.method public getLatLong([D)V
    .locals 4
    .param p1, "latLong"    # [D

    .prologue
    .line 402
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v2, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->latitude:D

    aput-wide v2, p1, v0

    .line 403
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v2, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->longitude:D

    aput-wide v2, p1, v0

    .line 404
    return-void
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 546
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 551
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->longitude:D

    return-wide v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 437
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mIsVideo:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentType:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoEntry()Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    return-object v0
.end method

.method public getPlayUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    return v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 512
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->size:I

    int-to-long v0, v0

    .line 515
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getSourceId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->id:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSourceUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 6

    .prologue
    .line 442
    const-wide v4, 0x400010000400L

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mIsVideo:Z

    if-eqz v2, :cond_3

    const-wide/16 v2, 0x80

    :goto_0
    or-long v0, v4, v2

    .line 446
    .local v0, "operation":J
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentType:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->isSupportedByRegionDecoder(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 447
    const-wide/16 v2, -0x41

    and-long/2addr v0, v2

    .line 449
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->latitude:D

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-wide v4, v4, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->longitude:D

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 450
    const-wide/16 v2, 0x10

    or-long/2addr v0, v2

    .line 452
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mIsVideo:Z

    if-eqz v2, :cond_2

    .line 453
    const-wide v2, -0x400000000001L

    and-long/2addr v0, v2

    .line 454
    :cond_2
    return-wide v0

    .line 442
    .end local v0    # "operation":J
    :cond_3
    const-wide v2, 0x800880000064L

    goto :goto_0
.end method

.method public getTags()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 408
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v1, v4, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->keywords:Ljava/lang/String;

    .line 409
    .local v1, "content":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 410
    const/4 v4, 0x0

    .line 418
    :goto_0
    return-object v4

    .line 412
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 413
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v4, ","

    invoke-direct {v3, v1, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    .local v3, "tokenizer":Ljava/util/StringTokenizer;
    :goto_1
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 415
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 417
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [Ljava/lang/String;

    .line 418
    .local v0, "array":[Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getThumbPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->screennailUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    if-nez v0, :cond_0

    .line 521
    const/4 v0, 0x0

    .line 523
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->width:I

    goto :goto_0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 8
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 94
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 95
    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/data/Path;I)V

    .line 130
    :goto_0
    return-object v1

    .line 97
    :cond_0
    if-eq p1, v3, :cond_1

    if-eq p1, v4, :cond_1

    if-ne p1, v2, :cond_8

    .line 98
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 99
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 100
    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/data/Path;I)V

    goto :goto_0

    .line 103
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mIsVideo:Z

    if-eqz v1, :cond_3

    .line 104
    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/data/Path;I)V

    goto :goto_0

    .line 106
    :cond_3
    if-eq p1, v3, :cond_4

    if-eq p1, v4, :cond_4

    if-ne p1, v2, :cond_6

    :cond_4
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget v1, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    if-eqz v1, :cond_6

    .line 108
    const-string v1, "PicasaImage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestImage getPath()=  mData.rotation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget v3, v3, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const-string v1, "ro.product.name"

    const-string v2, " "

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "gd1zs"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 111
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getDateInMs()J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v7, v6, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    move v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;)V

    goto :goto_0

    .line 115
    :cond_5
    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/data/Path;I)V

    goto/16 :goto_0

    .line 117
    :cond_6
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mChangePicasaSyncTypeToThumb:Z

    if-eqz v1, :cond_7

    .line 118
    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/data/Path;I)V

    goto/16 :goto_0

    .line 121
    :cond_7
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getDateInMs()J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v7, v6, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    move v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;)V

    goto/16 :goto_0

    .line 130
    .end local v0    # "file":Ljava/io/File;
    :cond_8
    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$DirectDecodeRequest;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$DirectDecodeRequest;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$1;)V

    goto/16 :goto_0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388
    :try_start_0
    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaLargeImageRequest;

    new-instance v2, Ljava/net/URL;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentUrl:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaLargeImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Ljava/net/URL;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 391
    :goto_0
    return-object v1

    .line 389
    :catch_0
    move-exception v0

    .line 390
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "PicasaImage"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 391
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected updateContent(Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;)V
    .locals 2
    .param p1, "entry"    # Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .prologue
    .line 423
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    if-eqz v0, :cond_1

    .line 425
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 426
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .line 427
    invoke-static {}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mDataVersion:J

    .line 433
    :cond_0
    :goto_0
    return-void

    .line 430
    :cond_1
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .line 431
    invoke-static {}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mDataVersion:J

    goto :goto_0
.end method
