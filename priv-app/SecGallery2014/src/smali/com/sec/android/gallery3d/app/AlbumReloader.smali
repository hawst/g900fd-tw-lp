.class public Lcom/sec/android/gallery3d/app/AlbumReloader;
.super Ljava/lang/Object;
.source "AlbumReloader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/AlbumReloader$1;,
        Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;,
        Lcom/sec/android/gallery3d/app/AlbumReloader$UpdateContent;,
        Lcom/sec/android/gallery3d/app/AlbumReloader$GetUpdateInfo;,
        Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;,
        Lcom/sec/android/gallery3d/app/AlbumReloader$MySourceListener;,
        Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    }
.end annotation


# static fields
.field public static final LOADING_FINISHED:I = 0x1

.field public static final LOADING_STARTED:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mFailedVersion:J

.field private mReloadListeners:[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;

.field private mReloadState:I

.field private mReloadTask:Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;

.field private mSize:I

.field private mSource:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mSourceListener:Lcom/sec/android/gallery3d/app/AlbumReloader$MySourceListener;

.field private mSourceVersion:J

.field private mWaitWhenResume:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/AlbumReloader;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 4
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const-wide/16 v2, -0x1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadListeners:[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;

    .line 39
    iput-wide v2, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSourceVersion:J

    .line 42
    new-instance v0, Lcom/sec/android/gallery3d/app/AlbumReloader$MySourceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/app/AlbumReloader$MySourceListener;-><init>(Lcom/sec/android/gallery3d/app/AlbumReloader;Lcom/sec/android/gallery3d/app/AlbumReloader$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSourceListener:Lcom/sec/android/gallery3d/app/AlbumReloader$MySourceListener;

    .line 46
    iput-wide v2, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mFailedVersion:J

    .line 47
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadState:I

    .line 51
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 52
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/AlbumReloader;)Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumReloader;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/AlbumReloader;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumReloader;

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mFailedVersion:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/app/AlbumReloader;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumReloader;

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSourceVersion:J

    return-wide v0
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/app/AlbumReloader;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumReloader;
    .param p1, "x1"    # J

    .prologue
    .line 24
    iput-wide p1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSourceVersion:J

    return-wide p1
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/AlbumReloader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumReloader;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSize:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/app/AlbumReloader;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumReloader;
    .param p1, "x1"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSize:I

    return p1
.end method

.method static synthetic access$502(Lcom/sec/android/gallery3d/app/AlbumReloader;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumReloader;
    .param p1, "x1"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadState:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/app/AlbumReloader;)[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumReloader;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadListeners:[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/gallery3d/app/AlbumReloader;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumReloader;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mWaitWhenResume:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/app/AlbumReloader;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumReloader;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method


# virtual methods
.method public changeSource(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 6
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AlbumReloader;->pause()V

    .line 56
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSourceVersion:J

    .line 57
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSize:I

    .line 58
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadListeners:[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 59
    .local v3, "listener":Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    invoke-interface {v3, p1}, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;->changeSource(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 58
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 61
    .end local v3    # "listener":Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AlbumReloader;->resume()V

    .line 63
    return-void
.end method

.method public getLatestUpdateInfo()Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
    .locals 4

    .prologue
    .line 66
    new-instance v0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;

    iget-wide v2, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSourceVersion:J

    iget v1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSize:I

    invoke-direct {v0, v2, v3, v1}, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;-><init>(JI)V

    return-object v0
.end method

.method public isLoading()Z
    .locals 4

    .prologue
    .line 152
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadState:I

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSourceVersion:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mWaitWhenResume:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDirty()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->notifyDirty(Z)V

    .line 159
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->terminate()V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;

    .line 83
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mWaitWhenResume:Z

    .line 84
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSourceListener:Lcom/sec/android/gallery3d/app/AlbumReloader$MySourceListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 85
    return-void
.end method

.method public resume()V
    .locals 4

    .prologue
    .line 71
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v1, :cond_0

    .line 76
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mSourceListener:Lcom/sec/android/gallery3d/app/AlbumReloader$MySourceListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 73
    const-class v1, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "threadName":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-class v3, Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;-><init>(Lcom/sec/android/gallery3d/app/AlbumReloader;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;

    .line 75
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadTask:Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->start()V

    goto :goto_0
.end method

.method public setDataListener(Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;

    .prologue
    .line 95
    instance-of v0, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadListeners:[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadListeners:[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;

    const/4 v1, 0x1

    aput-object p1, v0, v1

    goto :goto_0
.end method
