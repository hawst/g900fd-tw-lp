.class public final Lcom/sec/android/gallery3d/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Import:I = 0x7f0e009a

.field public static final My_Contacts:I = 0x7f0e00c3

.field public static final Tilt_message_1:I = 0x7f0e00e7

.field public static final Tilt_message_2:I = 0x7f0e00e8

.field public static final Tilt_message_3:I = 0x7f0e00e9

.field public static final a_face_detected:I = 0x7f0e00cb

.field public static final accessibility_pause_video:I = 0x7f0e0393

.field public static final accessibility_play_video:I = 0x7f0e0392

.field public static final accessibility_reload_video:I = 0x7f0e0394

.field public static final accessibility_time_bar:I = 0x7f0e0395

.field public static final account_logout_notice_msg:I = 0x7f0e0333

.field public static final accounts:I = 0x7f0e026b

.field public static final accounts_setting:I = 0x7f0e0309

.field public static final accumulated_time:I = 0x7f0e0105

.field public static final action_bar_text_label:I = 0x7f0e01bf

.field public static final action_bar_text_label_for_detail:I = 0x7f0e01c1

.field public static final activity_not_found:I = 0x7f0e0062

.field public static final adapt_display:I = 0x7f0e02e5

.field public static final adapt_display_message:I = 0x7f0e02e6

.field public static final add:I = 0x7f0e021a

.field public static final add_account:I = 0x7f0e00ae

.field public static final add_name:I = 0x7f0e00b0

.field public static final add_note_on_back_of_image:I = 0x7f0e0201

.field public static final add_slideshow_music:I = 0x7f0e0175

.field public static final add_tag:I = 0x7f0e0472

.field public static final add_to:I = 0x7f0e0446

.field public static final add_to_event:I = 0x7f0e0460

.field public static final add_weather_tag:I = 0x7f0e0228

.field public static final added_to_favourites:I = 0x7f0e02bf

.field public static final adding_images:I = 0x7f0e032c

.field public static final air_browse_text:I = 0x7f0e02bb

.field public static final air_command:I = 0x7f0e02d7

.field public static final airplanes:I = 0x7f0e0492

.field public static final album:I = 0x7f0e01d7

.field public static final album_created:I = 0x7f0e01f8

.field public static final album_number:I = 0x7f0e01c0

.field public static final albums:I = 0x7f0e00a4

.field public static final albums_reordered:I = 0x7f0e0363

.field public static final albumset_typeicon_scale:I = 0x7f0e0012

.field public static final albumtime_left_padding_h:I = 0x7f0e0021

.field public static final albumtime_left_padding_v:I = 0x7f0e0020

.field public static final alert:I = 0x7f0e005d

.field public static final alert_msg_invalid_name:I = 0x7f0e005e

.field public static final all:I = 0x7f0e00a5

.field public static final all_items_will_be_deleted:I = 0x7f0e01c2

.field public static final all_sim:I = 0x7f0e01a2

.field public static final allow:I = 0x7f0e049c

.field public static final allshare:I = 0x7f0e04da

.field public static final allshare_contents_location_is_not_available:I = 0x7f0e0128

.field public static final allshare_disconnect_popup:I = 0x7f0e0129

.field public static final allshare_hide_nearby_devices:I = 0x7f0e04dd

.field public static final allshare_nearby_devices:I = 0x7f0e0126

.field public static final allshare_nearby_devices_found:I = 0x7f0e012d

.field public static final allshare_nearby_my_device:I = 0x7f0e012f

.field public static final allshare_player_is_not_available:I = 0x7f0e012a

.field public static final allshare_select_playback_device:I = 0x7f0e0127

.field public static final allshare_show_nearby_devices:I = 0x7f0e04dc

.field public static final allshare_unable_scan_device:I = 0x7f0e0120

.field public static final allshare_unable_scan_device_while_alltogether_is_open:I = 0x7f0e0122

.field public static final allshare_unable_scan_device_while_group_play:I = 0x7f0e0121

.field public static final already_exists:I = 0x7f0e0110

.field public static final am:I = 0x7f0e0464

.field public static final animated_photo:I = 0x7f0e02f1

.field public static final aperture:I = 0x7f0e007d

.field public static final app_name:I = 0x7f0e002b

.field public static final application_not_found:I = 0x7f0e02bc

.field public static final appwidget_empty_text:I = 0x7f0e006b

.field public static final as_image_file:I = 0x7f0e02e9

.field public static final as_video_file:I = 0x7f0e02e8

.field public static final assign_name:I = 0x7f0e0059

.field public static final at_contextual_tag:I = 0x7f0e0220

.field public static final attention:I = 0x7f0e01a3

.field public static final auth_cancel:I = 0x7f0e0316

.field public static final auth_failed:I = 0x7f0e0315

.field public static final auth_success:I = 0x7f0e0314

.field public static final auto:I = 0x7f0e0090

.field public static final auto_play_sound:I = 0x7f0e027f

.field public static final auto_update_msg_detail:I = 0x7f0e030e

.field public static final auto_update_msg_title:I = 0x7f0e030d

.field public static final available_devices:I = 0x7f0e011f

.field public static final available_uses:I = 0x7f0e00f7

.field public static final baidu_sync:I = 0x7f0e027a

.field public static final baidu_sync_noti:I = 0x7f0e027b

.field public static final balloon_item_title:I = 0x7f0e01af

.field public static final barge_in_buddy_photo_share:I = 0x7f0e0261

.field public static final barge_in_camera:I = 0x7f0e0262

.field public static final barge_in_next:I = 0x7f0e025e

.field public static final barge_in_notification:I = 0x7f0e025d

.field public static final barge_in_play:I = 0x7f0e0263

.field public static final barge_in_previous:I = 0x7f0e025f

.field public static final barge_in_slideshow:I = 0x7f0e0260

.field public static final barge_in_stop:I = 0x7f0e0264

.field public static final best_face:I = 0x7f0e02f2

.field public static final best_pic:I = 0x7f0e016f

.field public static final best_pic_description:I = 0x7f0e0170

.field public static final bikes:I = 0x7f0e047e

.field public static final bind_account_message:I = 0x7f0e0330

.field public static final bind_account_title:I = 0x7f0e032d

.field public static final boats:I = 0x7f0e0494

.field public static final boundryeffect_h_aligintop:I = 0x7f0e0001

.field public static final boundryeffect_v_aligintop:I = 0x7f0e0000

.field public static final btn_login:I = 0x7f0e031f

.field public static final btn_logout:I = 0x7f0e0320

.field public static final buddy_add_contact_information:I = 0x7f0e00c6

.field public static final buddy_no_face_dectected:I = 0x7f0e00c4

.field public static final buddy_no_face_tagged:I = 0x7f0e00c5

.field public static final buddy_photo_sendto:I = 0x7f0e04d9

.field public static final buddy_photo_share:I = 0x7f0e04d8

.field public static final buddy_send_failed:I = 0x7f0e00c8

.field public static final buddy_send_success:I = 0x7f0e00c7

.field public static final buildings:I = 0x7f0e0490

.field public static final burstplay:I = 0x7f0e01b2

.field public static final bytes:I = 0x7f0e0344

.field public static final caching_label:I = 0x7f0e0389

.field public static final call:I = 0x7f0e00b8

.field public static final camera_connected:I = 0x7f0e009d

.field public static final camera_disconnected:I = 0x7f0e009e

.field public static final camera_label:I = 0x7f0e039e

.field public static final camera_setas_wallpaper:I = 0x7f0e003e

.field public static final cancel:I = 0x7f0e0046

.field public static final cars:I = 0x7f0e047a

.field public static final category:I = 0x7f0e00a9

.field public static final category_documents_tts:I = 0x7f0e02b1

.field public static final category_time_before_year:I = 0x7f0e0459

.field public static final category_time_last_year:I = 0x7f0e045a

.field public static final category_time_next_week:I = 0x7f0e045e

.field public static final category_time_past_month:I = 0x7f0e045c

.field public static final category_time_past_n_month_tts:I = 0x7f0e02b0

.field public static final category_time_past_six_month:I = 0x7f0e045b

.field public static final category_time_past_week:I = 0x7f0e045d

.field public static final categorychanaged:I = 0x7f0e048d

.field public static final cats:I = 0x7f0e0489

.field public static final change_category:I = 0x7f0e048c

.field public static final change_picture:I = 0x7f0e0133

.field public static final checkbox_size_factor:I = 0x7f0e001f

.field public static final cityscapes:I = 0x7f0e0491

.field public static final clear_all:I = 0x7f0e0204

.field public static final clear_night:I = 0x7f0e022e

.field public static final close:I = 0x7f0e004c

.field public static final close_filter:I = 0x7f0e017f

.field public static final cloud_album_will_be_deleted:I = 0x7f0e0238

.field public static final cloud_delete:I = 0x7f0e0239

.field public static final cloud_sync:I = 0x7f0e0277

.field public static final cloudy:I = 0x7f0e008a

.field public static final cloudy_day:I = 0x7f0e022b

.field public static final coast:I = 0x7f0e0480

.field public static final collapse:I = 0x7f0e02b9

.field public static final combined_alert:I = 0x7f0e0301

.field public static final combined_alert_title:I = 0x7f0e0300

.field public static final comment_preview_detail:I = 0x7f0e030c

.field public static final comment_preview_title:I = 0x7f0e030b

.field public static final completed_progress:I = 0x7f0e01c5

.field public static final confirm:I = 0x7f0e005a

.field public static final confirm_as:I = 0x7f0e005b

.field public static final confirm_cannot_save_with_sound_shot:I = 0x7f0e021c

.field public static final connect_WLAN_again_alert:I = 0x7f0e02fa

.field public static final connect_WLAN_alert:I = 0x7f0e02f7

.field public static final connect_WLAN_alert_title:I = 0x7f0e02f6

.field public static final connect_via_hdmi:I = 0x7f0e0143

.field public static final connected_devices:I = 0x7f0e0495

.field public static final content:I = 0x7f0e04bd

.field public static final content_to_display:I = 0x7f0e014f

.field public static final contextual_location:I = 0x7f0e0234

.field public static final contextual_tag:I = 0x7f0e021f

.field public static final contextual_tag_guide:I = 0x7f0e0266

.field public static final contextual_tag_guide_face:I = 0x7f0e026a

.field public static final contextual_tag_guide_for_k:I = 0x7f0e0265

.field public static final contextual_tag_guide_gps:I = 0x7f0e0269

.field public static final contextual_tag_guide_vzw:I = 0x7f0e0267

.field public static final contextual_tag_guide_weather:I = 0x7f0e0268

.field public static final contextual_time:I = 0x7f0e0233

.field public static final contextual_user_tags:I = 0x7f0e0235

.field public static final copy:I = 0x7f0e01ac

.field public static final copy_abb:I = 0x7f0e01ad

.field public static final copy_completed:I = 0x7f0e01c6

.field public static final copy_exist:I = 0x7f0e0284

.field public static final copy_to:I = 0x7f0e01ae

.field public static final copy_to_album:I = 0x7f0e0447

.field public static final copy_to_clipboard:I = 0x7f0e011a

.field public static final copy_to_event:I = 0x7f0e045f

.field public static final count:I = 0x7f0e0102

.field public static final count_items_selected:I = 0x7f0e01d0

.field public static final coworkers_group:I = 0x7f0e019f

.field public static final create_button:I = 0x7f0e04c4

.field public static final create_event:I = 0x7f0e0444

.field public static final create_motion_picture:I = 0x7f0e0289

.field public static final create_story_album:I = 0x7f0e0287

.field public static final create_video_album:I = 0x7f0e02ee

.field public static final crop_action:I = 0x7f0e0063

.field public static final crop_label:I = 0x7f0e0039

.field public static final crop_saved:I = 0x7f0e006c

.field public static final current_location:I = 0x7f0e01cd

.field public static final date:I = 0x7f0e0103

.field public static final date_modified:I = 0x7f0e01d4

.field public static final date_taken:I = 0x7f0e01d3

.field public static final daylight:I = 0x7f0e0085

.field public static final dcim_name:I = 0x7f0e025c

.field public static final default_exposition_value:I = 0x7f0e04c7

.field public static final delete:I = 0x7f0e0041

.field public static final delete_history:I = 0x7f0e04a2

.field public static final delete_imagenote_continue:I = 0x7f0e0210

.field public static final delete_imagenote_msg:I = 0x7f0e020f

.field public static final delete_imagenote_title:I = 0x7f0e020e

.field public static final delete_item_failed:I = 0x7f0e01f7

.field public static final delete_this_item_q:I = 0x7f0e00f3

.field public static final deleted:I = 0x7f0e01a1

.field public static final description:I = 0x7f0e0072

.field public static final deselect_all:I = 0x7f0e0049

.field public static final details:I = 0x7f0e004b

.field public static final details_hms:I = 0x7f0e0030

.field public static final details_ms:I = 0x7f0e002f

.field public static final details_size:I = 0x7f0e01d1

.field public static final details_title:I = 0x7f0e0386

.field public static final detect_faces:I = 0x7f0e005c

.field public static final dialog_msg_confirm_faces:I = 0x7f0e0060

.field public static final dialog_msg_detect_faces:I = 0x7f0e005f

.field public static final dialog_msg_remove_faces:I = 0x7f0e0061

.field public static final direct_share:I = 0x7f0e0288

.field public static final disabled:I = 0x7f0e02ac

.field public static final discard:I = 0x7f0e0337

.field public static final display:I = 0x7f0e0101

.field public static final display_now_q:I = 0x7f0e00f2

.field public static final do_not_have_enough_space_1:I = 0x7f0e028a

.field public static final do_not_have_enough_space_2:I = 0x7f0e028b

.field public static final do_not_have_enough_space_3:I = 0x7f0e028c

.field public static final do_not_have_enough_space_4:I = 0x7f0e028d

.field public static final do_not_have_enough_space_5:I = 0x7f0e028e

.field public static final do_not_have_enough_space_6:I = 0x7f0e028f

.field public static final do_not_have_enough_space_7:I = 0x7f0e0290

.field public static final do_not_show_again:I = 0x7f0e00da

.field public static final do_not_show_text:I = 0x7f0e02fe

.field public static final document:I = 0x7f0e00ac

.field public static final documents:I = 0x7f0e047c

.field public static final dogs:I = 0x7f0e0488

.field public static final done:I = 0x7f0e0070

.field public static final download:I = 0x7f0e010d

.field public static final download_form_baidu_cloud:I = 0x7f0e0305

.field public static final download_notice:I = 0x7f0e033e

.field public static final download_picture:I = 0x7f0e049d

.field public static final downloadable_dialog_popup_text:I = 0x7f0e033f

.field public static final downloadable_dialog_popup_text_chn:I = 0x7f0e0340

.field public static final downloaded:I = 0x7f0e0131

.field public static final downloading:I = 0x7f0e049e

.field public static final drag:I = 0x7f0e024e

.field public static final drag_and_drop:I = 0x7f0e02e2

.field public static final drag_and_drop_message:I = 0x7f0e02e3

.field public static final drag_and_drop_not_supported:I = 0x7f0e02e4

.field public static final drag_here:I = 0x7f0e024d

.field public static final draw_on_image:I = 0x7f0e0200

.field public static final drawer:I = 0x7f0e043c

.field public static final drawer_close:I = 0x7f0e0438

.field public static final drawer_expand:I = 0x7f0e043e

.field public static final drawer_help:I = 0x7f0e0439

.field public static final drawer_open:I = 0x7f0e0437

.field public static final drm_expired:I = 0x7f0e0109

.field public static final drm_info:I = 0x7f0e00fa

.field public static final drm_play:I = 0x7f0e0100

.field public static final drm_time_from:I = 0x7f0e00fd

.field public static final drm_time_until:I = 0x7f0e00fe

.field public static final drm_type:I = 0x7f0e00f8

.field public static final drop_show:I = 0x7f0e0196

.field public static final dropbox_Message_1:I = 0x7f0e04b2

.field public static final dropbox_Message_2:I = 0x7f0e04b3

.field public static final dropbox_Message_3:I = 0x7f0e04b4

.field public static final dropbox_Message_4:I = 0x7f0e04b5

.field public static final dropbox_popup_1:I = 0x7f0e04ae

.field public static final dropbox_popup_2:I = 0x7f0e04af

.field public static final dropbox_popup_3:I = 0x7f0e04b0

.field public static final dropbox_sync:I = 0x7f0e0278

.field public static final dropbox_sync_noti:I = 0x7f0e0279

.field public static final duration:I = 0x7f0e0079

.field public static final edit:I = 0x7f0e0058

.field public static final edit_burst_shot:I = 0x7f0e02f0

.field public static final edit_category:I = 0x7f0e048b

.field public static final edit_location:I = 0x7f0e0473

.field public static final edit_name:I = 0x7f0e00b1

.field public static final edit_weather_tag:I = 0x7f0e0227

.field public static final editing:I = 0x7f0e02e1

.field public static final effect:I = 0x7f0e0171

.field public static final effect_backdropper_gallery:I = 0x7f0e0434

.field public static final effect_backdropper_space:I = 0x7f0e0432

.field public static final effect_backdropper_sunset:I = 0x7f0e0433

.field public static final effect_goofy_face_big_eyes:I = 0x7f0e042d

.field public static final effect_goofy_face_big_mouth:I = 0x7f0e042e

.field public static final effect_goofy_face_big_nose:I = 0x7f0e0430

.field public static final effect_goofy_face_small_eyes:I = 0x7f0e0431

.field public static final effect_goofy_face_small_mouth:I = 0x7f0e042f

.field public static final effect_goofy_face_squeeze:I = 0x7f0e042c

.field public static final effect_none:I = 0x7f0e042b

.field public static final email:I = 0x7f0e00ba

.field public static final empty_album:I = 0x7f0e006d

.field public static final empty_set_description:I = 0x7f0e015a

.field public static final empty_set_description_baidu:I = 0x7f0e016d

.field public static final empty_set_description_center_for_people:I = 0x7f0e0167

.field public static final empty_set_description_dropbox:I = 0x7f0e016b

.field public static final empty_set_description_facebook:I = 0x7f0e016c

.field public static final empty_set_description_favorites:I = 0x7f0e016a

.field public static final empty_set_description_image:I = 0x7f0e015b

.field public static final empty_set_description_image_tap_here_for_camera:I = 0x7f0e015e

.field public static final empty_set_description_image_tap_here_for_camera_vzw:I = 0x7f0e0161

.field public static final empty_set_description_no_albums:I = 0x7f0e0169

.field public static final empty_set_description_no_items:I = 0x7f0e0168

.field public static final empty_set_description_tap_here_for_camera:I = 0x7f0e015d

.field public static final empty_set_description_tap_here_for_camera_vzw:I = 0x7f0e0160

.field public static final empty_set_description_tap_here_for_location:I = 0x7f0e0163

.field public static final empty_set_description_tap_here_for_location_vzw:I = 0x7f0e0165

.field public static final empty_set_description_tap_here_for_people:I = 0x7f0e0164

.field public static final empty_set_description_tap_here_for_people_vzw:I = 0x7f0e0166

.field public static final empty_set_description_video:I = 0x7f0e015c

.field public static final empty_set_description_video_tap_here_for_camera:I = 0x7f0e015f

.field public static final empty_set_description_video_tap_here_for_camera_vzw:I = 0x7f0e0162

.field public static final empty_tag_event:I = 0x7f0e0455

.field public static final empty_tag_location:I = 0x7f0e0453

.field public static final empty_tag_people:I = 0x7f0e0454

.field public static final empty_tag_user_def:I = 0x7f0e0452

.field public static final eraser:I = 0x7f0e0206

.field public static final eraser_settings:I = 0x7f0e0203

.field public static final error:I = 0x7f0e013a

.field public static final error_device_not_found:I = 0x7f0e013d

.field public static final error_dlna_cause_hdmi:I = 0x7f0e014e

.field public static final error_download_exceeds_storage:I = 0x7f0e0142

.field public static final error_failed_to_download:I = 0x7f0e0141

.field public static final error_file_already_exists:I = 0x7f0e013b

.field public static final error_file_error:I = 0x7f0e013c

.field public static final error_network_error_occurred:I = 0x7f0e013f

.field public static final error_not_enough_memory:I = 0x7f0e013e

.field public static final error_unknown:I = 0x7f0e0140

.field public static final error_wifi_display_cause_group_play:I = 0x7f0e014a

.field public static final error_wifi_display_cause_hotspot:I = 0x7f0e0144

.field public static final error_wifi_display_cause_hotspot_metropcs:I = 0x7f0e0145

.field public static final error_wifi_display_cause_hotspot_tmobile:I = 0x7f0e0146

.field public static final error_wifi_display_cause_mhl:I = 0x7f0e014d

.field public static final error_wifi_display_cause_portable_hotspot:I = 0x7f0e04de

.field public static final error_wifi_display_cause_portable_hotspot_chn:I = 0x7f0e014b

.field public static final error_wifi_display_cause_power_saving_mode:I = 0x7f0e0148

.field public static final error_wifi_display_cause_sydesync:I = 0x7f0e0149

.field public static final error_wifi_display_cause_wifi_direct:I = 0x7f0e0147

.field public static final error_wifi_display_cause_wifi_direct_chn:I = 0x7f0e014c

.field public static final event:I = 0x7f0e0441

.field public static final event_created:I = 0x7f0e0448

.field public static final evf_lcd_touch_screen:I = 0x7f0e02ec

.field public static final exceed_max_items_video_edit:I = 0x7f0e02ef

.field public static final exit_confirmation:I = 0x7f0e0236

.field public static final exit_withno_save:I = 0x7f0e0336

.field public static final expand:I = 0x7f0e02ba

.field public static final expand_close:I = 0x7f0e043f

.field public static final expand_open:I = 0x7f0e0440

.field public static final exposure_time:I = 0x7f0e0080

.field public static final face_reco:I = 0x7f0e0197

.field public static final face_tag:I = 0x7f0e00af

.field public static final face_tag_dialog_message_1:I = 0x7f0e00e5

.field public static final fail_to_load:I = 0x7f0e0033

.field public static final fail_to_load_invaild_file:I = 0x7f0e0035

.field public static final fail_to_load_video:I = 0x7f0e0034

.field public static final fail_to_move:I = 0x7f0e04c1

.field public static final family_group:I = 0x7f0e019d

.field public static final fast:I = 0x7f0e00e2

.field public static final fast_scroll_bar:I = 0x7f0e04bc

.field public static final favorite:I = 0x7f0e02bd

.field public static final favorites:I = 0x7f0e00aa

.field public static final filck_to_navigate:I = 0x7f0e02d0

.field public static final file_size:I = 0x7f0e0388

.field public static final file_type_not_supported:I = 0x7f0e010a

.field public static final filter_by_tts:I = 0x7f0e02ae

.field public static final filter_event:I = 0x7f0e0451

.field public static final filter_location:I = 0x7f0e044f

.field public static final filter_person:I = 0x7f0e0450

.field public static final filter_select_tts:I = 0x7f0e0456

.field public static final filter_tag:I = 0x7f0e044d

.field public static final filter_time:I = 0x7f0e044c

.field public static final filter_user_def:I = 0x7f0e044e

.field public static final filterby:I = 0x7f0e043a

.field public static final filters:I = 0x7f0e0179

.field public static final fine_weather:I = 0x7f0e0089

.field public static final first_time_view:I = 0x7f0e00f6

.field public static final flash:I = 0x7f0e007c

.field public static final flash_annotate:I = 0x7f0e04c0

.field public static final flash_off:I = 0x7f0e0092

.field public static final flash_on:I = 0x7f0e0091

.field public static final flowers:I = 0x7f0e047f

.field public static final fluorescent:I = 0x7f0e0086

.field public static final fluorescent_D:I = 0x7f0e008e

.field public static final fluorescent_H:I = 0x7f0e008c

.field public static final fluorescent_L:I = 0x7f0e008d

.field public static final focal_length:I = 0x7f0e007e

.field public static final focus_shot_help:I = 0x7f0e04c5

.field public static final folder_download:I = 0x7f0e036c

.field public static final folder_edited_online_photos:I = 0x7f0e0365

.field public static final folder_screenshot:I = 0x7f0e0364

.field public static final food:I = 0x7f0e0479

.field public static final forest:I = 0x7f0e0485

.field public static final format:I = 0x7f0e04be

.field public static final forwarding:I = 0x7f0e00fc

.field public static final free_space_format:I = 0x7f0e0380

.field public static final friends_group:I = 0x7f0e019e

.field public static final gadget_title:I = 0x7f0e002c

.field public static final gadget_title_simple:I = 0x7f0e002d

.field public static final geo_tag_alert_message:I = 0x7f0e01a4

.field public static final get_text:I = 0x7f0e01fd

.field public static final get_text_description:I = 0x7f0e019c

.field public static final gif_maker:I = 0x7f0e04c3

.field public static final gif_maker_unsupport_format:I = 0x7f0e04c6

.field public static final giga_byte:I = 0x7f0e0341

.field public static final glboundaryanim_dec_factor:I = 0x7f0e001e

.field public static final glboundaryanim_speed_ratio:I = 0x7f0e001d

.field public static final glgrid_glscroller_hsize_r_p:I = 0x7f0e000d

.field public static final glgrid_glscroller_hsize_r_w:I = 0x7f0e000c

.field public static final glgrid_glscroller_padding_bottom:I = 0x7f0e0007

.field public static final glgrid_glscroller_padding_left_h:I = 0x7f0e0005

.field public static final glgrid_glscroller_padding_left_v:I = 0x7f0e0004

.field public static final glgrid_glscroller_padding_right:I = 0x7f0e0006

.field public static final glgrid_glscroller_padding_top_h:I = 0x7f0e0002

.field public static final glgrid_glscroller_padding_top_v:I = 0x7f0e0003

.field public static final glgrid_glscroller_vgap_2:I = 0x7f0e0008

.field public static final glgrid_glscroller_vgap_3:I = 0x7f0e0009

.field public static final glgrid_glscroller_vgap_4:I = 0x7f0e000a

.field public static final glgrid_glscroller_vgap_5:I = 0x7f0e000b

.field public static final glgrid_glscroller_vsize_r_h:I = 0x7f0e000f

.field public static final glgrid_glscroller_vsize_r_v:I = 0x7f0e000e

.field public static final glphoto_glscroller_scroll_bar_width_land:I = 0x7f0e0010

.field public static final glphoto_glscroller_scroll_bar_width_port:I = 0x7f0e0011

.field public static final glphotoview_split_ratio_land:I = 0x7f0e0018

.field public static final glphotoview_split_ratio_land_tablet_pick:I = 0x7f0e0019

.field public static final glphotoview_split_ratio_port:I = 0x7f0e0017

.field public static final grey_filter:I = 0x7f0e017b

.field public static final grid_view:I = 0x7f0e01b0

.field public static final group_by:I = 0x7f0e0371

.field public static final group_by_album:I = 0x7f0e0373

.field public static final group_by_faces:I = 0x7f0e0378

.field public static final group_by_location:I = 0x7f0e0375

.field public static final group_by_size:I = 0x7f0e0377

.field public static final group_by_tags:I = 0x7f0e0376

.field public static final group_by_time:I = 0x7f0e0374

.field public static final header:I = 0x7f0e043d

.field public static final header_accounts:I = 0x7f0e04d2

.field public static final header_cloud_sync:I = 0x7f0e04d4

.field public static final header_connected_devices:I = 0x7f0e04ca

.field public static final header_filter:I = 0x7f0e04cf

.field public static final header_filter_by:I = 0x7f0e04cb

.field public static final header_music:I = 0x7f0e04d1

.field public static final header_nearby_devices:I = 0x7f0e04c9

.field public static final header_registered_devices:I = 0x7f0e04ce

.field public static final header_samsung_Link:I = 0x7f0e04d6

.field public static final header_social_network_data_management:I = 0x7f0e04d5

.field public static final header_sort_by:I = 0x7f0e04cc

.field public static final header_speed:I = 0x7f0e04d0

.field public static final header_tags:I = 0x7f0e04d3

.field public static final header_transition_effect:I = 0x7f0e04cd

.field public static final header_video:I = 0x7f0e04d7

.field public static final height:I = 0x7f0e0077

.field public static final help:I = 0x7f0e02d9

.field public static final help_air_button_complete:I = 0x7f0e02d6

.field public static final help_air_button_press:I = 0x7f0e02d5

.field public static final help_icon_labels_completed:I = 0x7f0e02d8

.field public static final help_invlaid_action:I = 0x7f0e02da

.field public static final help_picker:I = 0x7f0e02db

.field public static final here:I = 0x7f0e024f

.field public static final hidden_item:I = 0x7f0e01d9

.field public static final hide_item:I = 0x7f0e01d8

.field public static final hide_item_failed:I = 0x7f0e01dc

.field public static final hide_item_success:I = 0x7f0e01db

.field public static final hide_items_failed:I = 0x7f0e01dd

.field public static final hint_location_tag:I = 0x7f0e0458

.field public static final hint_user_tag:I = 0x7f0e0457

.field public static final home:I = 0x7f0e01fa

.field public static final hover_here:I = 0x7f0e02cd

.field public static final hover_your_pen:I = 0x7f0e02cf

.field public static final hover_your_pen_or_finger:I = 0x7f0e02ce

.field public static final i_will_never_gonna:I = 0x7f0e04a9

.field public static final image_too_small:I = 0x7f0e0282

.field public static final imagenote:I = 0x7f0e01fe

.field public static final import_complete:I = 0x7f0e009b

.field public static final import_fail:I = 0x7f0e009c

.field public static final impossible:I = 0x7f0e00fb

.field public static final in_contextual_tag:I = 0x7f0e0221

.field public static final information:I = 0x7f0e00d4

.field public static final information_preview_completed:I = 0x7f0e02d1

.field public static final ingest_empty_device:I = 0x7f0e0397

.field public static final ingest_importing:I = 0x7f0e039b

.field public static final ingest_no_device:I = 0x7f0e0398

.field public static final ingest_scanning:I = 0x7f0e0399

.field public static final ingest_scanning_done:I = 0x7f0e039c

.field public static final ingest_sorting:I = 0x7f0e039a

.field public static final interval:I = 0x7f0e0104

.field public static final invalid_character:I = 0x7f0e0111

.field public static final iso:I = 0x7f0e0081

.field public static final items_moved_and_other_item_exist:I = 0x7f0e01f6

.field public static final items_moved_and_other_items_exist:I = 0x7f0e01f5

.field public static final kilo_byte:I = 0x7f0e0343

.field public static final knox_not_ready:I = 0x7f0e0350

.field public static final last_month:I = 0x7f0e0241

.field public static final last_synced_on:I = 0x7f0e026e

.field public static final last_three_days:I = 0x7f0e023d

.field public static final last_week:I = 0x7f0e023f

.field public static final last_year:I = 0x7f0e0243

.field public static final latest:I = 0x7f0e023b

.field public static final latitude:I = 0x7f0e01d5

.field public static final learn_about_pan:I = 0x7f0e00d2

.field public static final learn_about_peek:I = 0x7f0e00d3

.field public static final learn_about_tilt:I = 0x7f0e00d1

.field public static final license_aquisition_failed:I = 0x7f0e0108

.field public static final list_scrolling_completed:I = 0x7f0e02d2

.field public static final load_contents:I = 0x7f0e02ff

.field public static final loading:I = 0x7f0e0032

.field public static final loading_image:I = 0x7f0e0031

.field public static final loading_title:I = 0x7f0e0219

.field public static final loading_video:I = 0x7f0e036f

.field public static final location:I = 0x7f0e0074

.field public static final locations:I = 0x7f0e00a6

.field public static final longitude:I = 0x7f0e01d6

.field public static final magazine_widget_description:I = 0x7f0e04b8

.field public static final make_available_offline:I = 0x7f0e0370

.field public static final make_collage:I = 0x7f0e0286

.field public static final maker:I = 0x7f0e007a

.field public static final manual:I = 0x7f0e0083

.field public static final manual_fd_title:I = 0x7f0e00c9

.field public static final mapview_marker_bitmap_leftmargin_factor:I = 0x7f0e0022

.field public static final mapview_marker_resize_album_count_text_height:I = 0x7f0e0023

.field public static final mapview_marker_resize_album_count_text_width:I = 0x7f0e0024

.field public static final max_character:I = 0x7f0e01cc

.field public static final maximum_selection_number:I = 0x7f0e0114

.field public static final maximum_selection_number_exceeded:I = 0x7f0e0117

.field public static final me:I = 0x7f0e00c0

.field public static final mega_byte:I = 0x7f0e0342

.field public static final menu_camera:I = 0x7f0e038e

.field public static final menu_search:I = 0x7f0e038f

.field public static final messaging:I = 0x7f0e00b9

.field public static final mimetype:I = 0x7f0e0387

.field public static final minimum_selection_number:I = 0x7f0e0116

.field public static final mobile_network_charge_alert:I = 0x7f0e02f5

.field public static final mobile_network_charge_alert_title:I = 0x7f0e02f4

.field public static final model:I = 0x7f0e007b

.field public static final more:I = 0x7f0e00b7

.field public static final more_comments:I = 0x7f0e0329

.field public static final more_options:I = 0x7f0e02de

.field public static final moreinfo:I = 0x7f0e0468

.field public static final moreinfo_all_tags:I = 0x7f0e046c

.field public static final moreinfo_edit_tag_hint:I = 0x7f0e0471

.field public static final moreinfo_location_editor_gps_popup_body:I = 0x7f0e0477

.field public static final moreinfo_location_editor_gps_popup_title:I = 0x7f0e0476

.field public static final moreinfo_tag_already_in_use:I = 0x7f0e046d

.field public static final moreinfo_tag_delete:I = 0x7f0e046e

.field public static final moreinfo_tags_delete:I = 0x7f0e046f

.field public static final moreinfo_tags_for_this_contents:I = 0x7f0e046b

.field public static final moreinfo_unused_tag_delete:I = 0x7f0e0470

.field public static final moreinfo_user_tags:I = 0x7f0e046a

.field public static final moreinfo_usertags_none:I = 0x7f0e0469

.field public static final motion:I = 0x7f0e00cf

.field public static final mountain:I = 0x7f0e0484

.field public static final move:I = 0x7f0e01a8

.field public static final move_abb:I = 0x7f0e01a9

.field public static final move_album_success:I = 0x7f0e01e5

.field public static final move_albums_success:I = 0x7f0e01e6

.field public static final move_albums_to_knox:I = 0x7f0e0354

.field public static final move_albums_to_knox_success:I = 0x7f0e034d

.field public static final move_completed:I = 0x7f0e01c7

.field public static final move_device:I = 0x7f0e00eb

.field public static final move_device_right:I = 0x7f0e00ec

.field public static final move_exist:I = 0x7f0e0285

.field public static final move_item_failed:I = 0x7f0e01e7

.field public static final move_item_success:I = 0x7f0e01e3

.field public static final move_items_failed:I = 0x7f0e01e8

.field public static final move_items_success:I = 0x7f0e01e4

.field public static final move_items_to_knox:I = 0x7f0e0352

.field public static final move_items_to_knox_success:I = 0x7f0e0349

.field public static final move_one_album_to_knox:I = 0x7f0e0353

.field public static final move_one_item_to_knox:I = 0x7f0e0351

.field public static final move_singlealbum_to_knox_success:I = 0x7f0e034c

.field public static final move_singleitem_to_knox_success:I = 0x7f0e0348

.field public static final move_to:I = 0x7f0e01ab

.field public static final move_to_album:I = 0x7f0e0498

.field public static final move_to_knox:I = 0x7f0e0346

.field public static final move_to_secretbox:I = 0x7f0e01e1

.field public static final movie_view_label:I = 0x7f0e039d

.field public static final moving:I = 0x7f0e01aa

.field public static final msg_sending_multi:I = 0x7f0e031e

.field public static final msg_sending_single:I = 0x7f0e031d

.field public static final multi_cube:I = 0x7f0e0198

.field public static final multi_picture_already_upload:I = 0x7f0e033b

.field public static final multiface_crop_help:I = 0x7f0e0036

.field public static final multiple_albums_will_be_deleted:I = 0x7f0e01c4

.field public static final music:I = 0x7f0e0172

.field public static final mute_action:I = 0x7f0e038d

.field public static final my_album:I = 0x7f0e01a6

.field public static final my_contact_group:I = 0x7f0e01a0

.field public static final my_tablet:I = 0x7f0e04b9

.field public static final myphone:I = 0x7f0e043b

.field public static final n_items_will_be_deleted:I = 0x7f0e0043

.field public static final n_items_will_be_removed:I = 0x7f0e0045

.field public static final name1:I = 0x7f0e02d3

.field public static final name2:I = 0x7f0e02d4

.field public static final name_cannot_empty:I = 0x7f0e01ca

.field public static final name_tag_removed:I = 0x7f0e048f

.field public static final need_to_max_items_for_collage:I = 0x7f0e0115

.field public static final network_info_noti:I = 0x7f0e0311

.field public static final new_album:I = 0x7f0e01a5

.field public static final new_album_button:I = 0x7f0e01a7

.field public static final new_album_message:I = 0x7f0e0237

.field public static final new_event:I = 0x7f0e0445

.field public static final new_label:I = 0x7f0e04ad

.field public static final night_scene:I = 0x7f0e0481

.field public static final no_app_for_action:I = 0x7f0e0258

.field public static final no_connectivity:I = 0x7f0e033c

.field public static final no_devices_found_scanning_etc:I = 0x7f0e012e

.field public static final no_document:I = 0x7f0e006a

.field public static final no_images_available:I = 0x7f0e006e

.field public static final no_location:I = 0x7f0e0069

.field public static final no_longer_available:I = 0x7f0e00f4

.field public static final no_network:I = 0x7f0e032e

.field public static final no_selection_items:I = 0x7f0e0113

.field public static final no_such_item:I = 0x7f0e0057

.field public static final no_tags:I = 0x7f0e0229

.field public static final no_title:I = 0x7f0e0461

.field public static final no_usb_storage:I = 0x7f0e016e

.field public static final no_videos_available:I = 0x7f0e006f

.field public static final none:I = 0x7f0e0174

.field public static final not_now:I = 0x7f0e04b1

.field public static final not_selectable_in_scene_mode:I = 0x7f0e03e9

.field public static final not_support_image:I = 0x7f0e033d

.field public static final not_supported:I = 0x7f0e02eb

.field public static final noti_date:I = 0x7f0e0231

.field public static final noti_date_att:I = 0x7f0e0232

.field public static final noti_people:I = 0x7f0e0230

.field public static final noti_weather:I = 0x7f0e022f

.field public static final notice_low_storage:I = 0x7f0e0139

.field public static final number_and_count_of_item_selected:I = 0x7f0e0050

.field public static final number_and_count_of_item_selected_text:I = 0x7f0e0051

.field public static final number_and_count_of_item_selected_tts:I = 0x7f0e0052

.field public static final number_of_albums_selected:I = 0x7f0e0053

.field public static final number_of_item_selected:I = 0x7f0e004f

.field public static final number_of_items_selected:I = 0x7f0e004e

.field public static final ocr_add_bookmark:I = 0x7f0e0255

.field public static final ocr_add_contact:I = 0x7f0e0252

.field public static final ocr_error:I = 0x7f0e0248

.field public static final ocr_image_is_too_large:I = 0x7f0e0246

.field public static final ocr_no_result:I = 0x7f0e024c

.field public static final ocr_no_text:I = 0x7f0e0249

.field public static final ocr_open_url:I = 0x7f0e0254

.field public static final ocr_search_result:I = 0x7f0e024b

.field public static final ocr_searching_text:I = 0x7f0e024a

.field public static final ocr_send_mail:I = 0x7f0e0251

.field public static final ocr_send_msg:I = 0x7f0e0253

.field public static final ocr_title:I = 0x7f0e0247

.field public static final ocr_unsupported_image_format:I = 0x7f0e0245

.field public static final ocr_web_search:I = 0x7f0e0250

.field public static final off:I = 0x7f0e0187

.field public static final ok:I = 0x7f0e00db

.field public static final on:I = 0x7f0e00bc

.field public static final one_album_will_be_deleted:I = 0x7f0e01c3

.field public static final one_item_moved_and_other_item_exist:I = 0x7f0e01f4

.field public static final one_item_moved_and_other_items_exist:I = 0x7f0e01f3

.field public static final one_item_will_be_deleted:I = 0x7f0e0042

.field public static final one_item_will_be_removed:I = 0x7f0e0044

.field public static final online_pic_download:I = 0x7f0e010b

.field public static final only_share_image:I = 0x7f0e0338

.field public static final open:I = 0x7f0e0209

.field public static final open_country:I = 0x7f0e0482

.field public static final open_filter:I = 0x7f0e017e

.field public static final orientation:I = 0x7f0e0078

.field public static final overwrite:I = 0x7f0e04e1

.field public static final pan_complete_msg:I = 0x7f0e00ed

.field public static final panning_dialog_message_1:I = 0x7f0e00d8

.field public static final pano_progress_text:I = 0x7f0e036d

.field public static final panorama:I = 0x7f0e02f3

.field public static final paper_artist:I = 0x7f0e011e

.field public static final parallel_itemstarty_land:I = 0x7f0e0016

.field public static final parallel_tagdist:I = 0x7f0e0015

.field public static final path:I = 0x7f0e0075

.field public static final pd_faces_detected:I = 0x7f0e00cc

.field public static final pd_seconds:I = 0x7f0e0138

.field public static final peek_complete_msg:I = 0x7f0e00ee

.field public static final peeking_dialog_message_1:I = 0x7f0e00d9

.field public static final pen:I = 0x7f0e0205

.field public static final pen_settings:I = 0x7f0e0202

.field public static final pen_settings_already_exists:I = 0x7f0e0217

.field public static final pen_settings_preset_delete_msg:I = 0x7f0e0216

.field public static final pen_settings_preset_delete_title:I = 0x7f0e0215

.field public static final pen_settings_preset_empty:I = 0x7f0e0214

.field public static final pen_settings_preset_maximum_msg:I = 0x7f0e0218

.field public static final people:I = 0x7f0e00a7

.field public static final person:I = 0x7f0e0222

.field public static final personal_image_setas_alert_message:I = 0x7f0e01f2

.field public static final personal_image_unsupported_setas:I = 0x7f0e01f1

.field public static final personal_mode:I = 0x7f0e0345

.field public static final pets:I = 0x7f0e047b

.field public static final photo_editor:I = 0x7f0e011b

.field public static final photo_editor_description:I = 0x7f0e019b

.field public static final photo_navigation:I = 0x7f0e01fc

.field public static final photo_wizard:I = 0x7f0e011c

.field public static final picasa_web_albums:I = 0x7f0e00ef

.field public static final picture_added:I = 0x7f0e0134

.field public static final pictures_added:I = 0x7f0e0135

.field public static final play_from_newest:I = 0x7f0e0192

.field public static final play_from_oldest:I = 0x7f0e0191

.field public static final play_speed:I = 0x7f0e01b5

.field public static final play_speed_tts:I = 0x7f0e01ba

.field public static final play_video:I = 0x7f0e0176

.field public static final playback_pictures:I = 0x7f0e01b3

.field public static final playing_order:I = 0x7f0e0178

.field public static final pm:I = 0x7f0e0465

.field public static final polaroidframe:I = 0x7f0e01ff

.field public static final pref_camcorder_settings_category:I = 0x7f0e03aa

.field public static final pref_camera_countdown_label:I = 0x7f0e03e3

.field public static final pref_camera_countdown_label_fifteen:I = 0x7f0e03e8

.field public static final pref_camera_countdown_label_off:I = 0x7f0e03e4

.field public static final pref_camera_countdown_label_one:I = 0x7f0e03e5

.field public static final pref_camera_countdown_label_ten:I = 0x7f0e03e7

.field public static final pref_camera_countdown_label_three:I = 0x7f0e03e6

.field public static final pref_camera_flashmode_default:I = 0x7f0e03be

.field public static final pref_camera_flashmode_entry_auto:I = 0x7f0e03c2

.field public static final pref_camera_flashmode_entry_off:I = 0x7f0e03c4

.field public static final pref_camera_flashmode_entry_on:I = 0x7f0e03c3

.field public static final pref_camera_flashmode_label:I = 0x7f0e03c1

.field public static final pref_camera_flashmode_label_auto:I = 0x7f0e03c5

.field public static final pref_camera_flashmode_label_off:I = 0x7f0e03c7

.field public static final pref_camera_flashmode_label_on:I = 0x7f0e03c6

.field public static final pref_camera_flashmode_no_flash:I = 0x7f0e03bf

.field public static final pref_camera_flashmode_title:I = 0x7f0e03c0

.field public static final pref_camera_focusmode_entry_auto:I = 0x7f0e03b8

.field public static final pref_camera_focusmode_entry_infinity:I = 0x7f0e03b9

.field public static final pref_camera_focusmode_entry_macro:I = 0x7f0e03ba

.field public static final pref_camera_focusmode_label_auto:I = 0x7f0e03bb

.field public static final pref_camera_focusmode_label_infinity:I = 0x7f0e03bc

.field public static final pref_camera_focusmode_label_macro:I = 0x7f0e03bd

.field public static final pref_camera_focusmode_title:I = 0x7f0e03b7

.field public static final pref_camera_hdr_default:I = 0x7f0e03ed

.field public static final pref_camera_hdr_label:I = 0x7f0e03ee

.field public static final pref_camera_id_default:I = 0x7f0e041f

.field public static final pref_camera_id_entry_back:I = 0x7f0e0420

.field public static final pref_camera_id_entry_front:I = 0x7f0e0421

.field public static final pref_camera_id_label_back:I = 0x7f0e03ef

.field public static final pref_camera_id_label_front:I = 0x7f0e03f0

.field public static final pref_camera_id_title:I = 0x7f0e041e

.field public static final pref_camera_location_label:I = 0x7f0e0424

.field public static final pref_camera_picturesize_entry_13mp:I = 0x7f0e03ac

.field public static final pref_camera_picturesize_entry_1_3mp:I = 0x7f0e03b3

.field public static final pref_camera_picturesize_entry_1mp:I = 0x7f0e03b4

.field public static final pref_camera_picturesize_entry_2mp:I = 0x7f0e03b1

.field public static final pref_camera_picturesize_entry_2mp_wide:I = 0x7f0e03b2

.field public static final pref_camera_picturesize_entry_3mp:I = 0x7f0e03b0

.field public static final pref_camera_picturesize_entry_4mp:I = 0x7f0e03af

.field public static final pref_camera_picturesize_entry_5mp:I = 0x7f0e03ae

.field public static final pref_camera_picturesize_entry_8mp:I = 0x7f0e03ad

.field public static final pref_camera_picturesize_entry_qvga:I = 0x7f0e03b6

.field public static final pref_camera_picturesize_entry_vga:I = 0x7f0e03b5

.field public static final pref_camera_picturesize_title:I = 0x7f0e03ab

.field public static final pref_camera_recordlocation_default:I = 0x7f0e0423

.field public static final pref_camera_recordlocation_title:I = 0x7f0e0422

.field public static final pref_camera_scenemode_default:I = 0x7f0e03d6

.field public static final pref_camera_scenemode_entry_action:I = 0x7f0e03da

.field public static final pref_camera_scenemode_entry_auto:I = 0x7f0e03d8

.field public static final pref_camera_scenemode_entry_hdr:I = 0x7f0e03d9

.field public static final pref_camera_scenemode_entry_night:I = 0x7f0e03db

.field public static final pref_camera_scenemode_entry_party:I = 0x7f0e03dd

.field public static final pref_camera_scenemode_entry_sunset:I = 0x7f0e03dc

.field public static final pref_camera_scenemode_label_action:I = 0x7f0e03df

.field public static final pref_camera_scenemode_label_auto:I = 0x7f0e03de

.field public static final pref_camera_scenemode_label_night:I = 0x7f0e03e0

.field public static final pref_camera_scenemode_label_party:I = 0x7f0e03e2

.field public static final pref_camera_scenemode_label_sunset:I = 0x7f0e03e1

.field public static final pref_camera_scenemode_title:I = 0x7f0e03d7

.field public static final pref_camera_settings_category:I = 0x7f0e03a9

.field public static final pref_camera_timer_default:I = 0x7f0e0426

.field public static final pref_camera_timer_sound_default:I = 0x7f0e0427

.field public static final pref_camera_timer_sound_title:I = 0x7f0e0428

.field public static final pref_camera_timer_title:I = 0x7f0e0425

.field public static final pref_camera_video_flashmode_default:I = 0x7f0e03c8

.field public static final pref_camera_whitebalance_default:I = 0x7f0e03c9

.field public static final pref_camera_whitebalance_entry_auto:I = 0x7f0e03cc

.field public static final pref_camera_whitebalance_entry_cloudy:I = 0x7f0e03d0

.field public static final pref_camera_whitebalance_entry_daylight:I = 0x7f0e03ce

.field public static final pref_camera_whitebalance_entry_fluorescent:I = 0x7f0e03cf

.field public static final pref_camera_whitebalance_entry_incandescent:I = 0x7f0e03cd

.field public static final pref_camera_whitebalance_label:I = 0x7f0e03cb

.field public static final pref_camera_whitebalance_label_auto:I = 0x7f0e03d1

.field public static final pref_camera_whitebalance_label_cloudy:I = 0x7f0e03d5

.field public static final pref_camera_whitebalance_label_daylight:I = 0x7f0e03d3

.field public static final pref_camera_whitebalance_label_fluorescent:I = 0x7f0e03d4

.field public static final pref_camera_whitebalance_label_incandescent:I = 0x7f0e03d2

.field public static final pref_camera_whitebalance_title:I = 0x7f0e03ca

.field public static final pref_exposure_default:I = 0x7f0e03eb

.field public static final pref_exposure_label:I = 0x7f0e03ec

.field public static final pref_exposure_title:I = 0x7f0e03ea

.field public static final pref_video_effect_default:I = 0x7f0e0429

.field public static final pref_video_effect_title:I = 0x7f0e042a

.field public static final pref_video_quality_default:I = 0x7f0e03a1

.field public static final pref_video_quality_entry_1080p:I = 0x7f0e03a2

.field public static final pref_video_quality_entry_480p:I = 0x7f0e03a4

.field public static final pref_video_quality_entry_720p:I = 0x7f0e03a3

.field public static final pref_video_quality_entry_high:I = 0x7f0e03a5

.field public static final pref_video_quality_entry_low:I = 0x7f0e03a6

.field public static final pref_video_quality_title:I = 0x7f0e03a0

.field public static final pref_video_time_lapse_frame_interval_1000:I = 0x7f0e03f3

.field public static final pref_video_time_lapse_frame_interval_10000:I = 0x7f0e03fb

.field public static final pref_video_time_lapse_frame_interval_10800000:I = 0x7f0e0411

.field public static final pref_video_time_lapse_frame_interval_12000:I = 0x7f0e03fc

.field public static final pref_video_time_lapse_frame_interval_120000:I = 0x7f0e0402

.field public static final pref_video_time_lapse_frame_interval_1440000:I = 0x7f0e040b

.field public static final pref_video_time_lapse_frame_interval_14400000:I = 0x7f0e0412

.field public static final pref_video_time_lapse_frame_interval_1500:I = 0x7f0e03f4

.field public static final pref_video_time_lapse_frame_interval_15000:I = 0x7f0e03fd

.field public static final pref_video_time_lapse_frame_interval_150000:I = 0x7f0e0403

.field public static final pref_video_time_lapse_frame_interval_180000:I = 0x7f0e0404

.field public static final pref_video_time_lapse_frame_interval_1800000:I = 0x7f0e040c

.field public static final pref_video_time_lapse_frame_interval_18000000:I = 0x7f0e0413

.field public static final pref_video_time_lapse_frame_interval_2000:I = 0x7f0e03f5

.field public static final pref_video_time_lapse_frame_interval_21600000:I = 0x7f0e0414

.field public static final pref_video_time_lapse_frame_interval_24000:I = 0x7f0e03fe

.field public static final pref_video_time_lapse_frame_interval_240000:I = 0x7f0e0405

.field public static final pref_video_time_lapse_frame_interval_2500:I = 0x7f0e03f6

.field public static final pref_video_time_lapse_frame_interval_3000:I = 0x7f0e03f7

.field public static final pref_video_time_lapse_frame_interval_30000:I = 0x7f0e03ff

.field public static final pref_video_time_lapse_frame_interval_300000:I = 0x7f0e0406

.field public static final pref_video_time_lapse_frame_interval_360000:I = 0x7f0e0407

.field public static final pref_video_time_lapse_frame_interval_3600000:I = 0x7f0e040d

.field public static final pref_video_time_lapse_frame_interval_36000000:I = 0x7f0e0415

.field public static final pref_video_time_lapse_frame_interval_4000:I = 0x7f0e03f8

.field public static final pref_video_time_lapse_frame_interval_43200000:I = 0x7f0e0416

.field public static final pref_video_time_lapse_frame_interval_500:I = 0x7f0e03f2

.field public static final pref_video_time_lapse_frame_interval_5000:I = 0x7f0e03f9

.field public static final pref_video_time_lapse_frame_interval_5400000:I = 0x7f0e040e

.field public static final pref_video_time_lapse_frame_interval_54000000:I = 0x7f0e0417

.field public static final pref_video_time_lapse_frame_interval_6000:I = 0x7f0e03fa

.field public static final pref_video_time_lapse_frame_interval_60000:I = 0x7f0e0400

.field public static final pref_video_time_lapse_frame_interval_600000:I = 0x7f0e0408

.field public static final pref_video_time_lapse_frame_interval_720000:I = 0x7f0e0409

.field public static final pref_video_time_lapse_frame_interval_7200000:I = 0x7f0e040f

.field public static final pref_video_time_lapse_frame_interval_86400000:I = 0x7f0e0418

.field public static final pref_video_time_lapse_frame_interval_90000:I = 0x7f0e0401

.field public static final pref_video_time_lapse_frame_interval_900000:I = 0x7f0e040a

.field public static final pref_video_time_lapse_frame_interval_9000000:I = 0x7f0e0410

.field public static final pref_video_time_lapse_frame_interval_default:I = 0x7f0e03a8

.field public static final pref_video_time_lapse_frame_interval_off:I = 0x7f0e03f1

.field public static final pref_video_time_lapse_frame_interval_title:I = 0x7f0e03a7

.field public static final printer:I = 0x7f0e0112

.field public static final process_caching_requests:I = 0x7f0e037e

.field public static final processing:I = 0x7f0e00ca

.field public static final quick_scroll_help:I = 0x7f0e04a6

.field public static final quick_scroll_help_people:I = 0x7f0e04a7

.field public static final quick_scroll_item_pitch:I = 0x7f0e0027

.field public static final quick_scroll_root_pitch_ratio:I = 0x7f0e0028

.field public static final quick_scroll_root_y:I = 0x7f0e0029

.field public static final quick_scroll_root_z:I = 0x7f0e002a

.field public static final rainy_day:I = 0x7f0e022c

.field public static final random:I = 0x7f0e019a

.field public static final reach_max:I = 0x7f0e032a

.field public static final redo:I = 0x7f0e0208

.field public static final refresh:I = 0x7f0e012b

.field public static final registered_devices:I = 0x7f0e0497

.field public static final remove:I = 0x7f0e0335

.field public static final remove_album_success:I = 0x7f0e01eb

.field public static final remove_albums_from_knox_success:I = 0x7f0e034f

.field public static final remove_albums_success:I = 0x7f0e01ec

.field public static final remove_from:I = 0x7f0e0463

.field public static final remove_from_category:I = 0x7f0e0462

.field public static final remove_from_event:I = 0x7f0e0449

.field public static final remove_from_knox:I = 0x7f0e0347

.field public static final remove_from_private_album_success:I = 0x7f0e01ed

.field public static final remove_from_private_albums_success:I = 0x7f0e01ee

.field public static final remove_from_secretbox:I = 0x7f0e01e2

.field public static final remove_item_failed:I = 0x7f0e01ef

.field public static final remove_item_success:I = 0x7f0e01e9

.field public static final remove_items_failed:I = 0x7f0e01f0

.field public static final remove_items_from_knox_success:I = 0x7f0e034b

.field public static final remove_items_success:I = 0x7f0e01ea

.field public static final remove_name_tag:I = 0x7f0e00be

.field public static final remove_singlealbum_from_knox_success:I = 0x7f0e034e

.field public static final remove_singleitem_from_knox_success:I = 0x7f0e034a

.field public static final remove_sound:I = 0x7f0e04a3

.field public static final remove_sound_massage:I = 0x7f0e04a4

.field public static final remove_tag:I = 0x7f0e00bd

.field public static final removed_from_favourites:I = 0x7f0e02c0

.field public static final rename:I = 0x7f0e010e

.field public static final render_info_can_use_1_time:I = 0x7f0e00f1

.field public static final render_info_can_use_n_times:I = 0x7f0e00f0

.field public static final reorder:I = 0x7f0e0362

.field public static final replace:I = 0x7f0e0283

.field public static final reply_management:I = 0x7f0e030a

.field public static final resolution:I = 0x7f0e01d2

.field public static final resume_playing_message:I = 0x7f0e0382

.field public static final resume_playing_restart:I = 0x7f0e0384

.field public static final resume_playing_resume:I = 0x7f0e0383

.field public static final resume_playing_title:I = 0x7f0e0381

.field public static final rotate_left:I = 0x7f0e0055

.field public static final rotate_right:I = 0x7f0e0056

.field public static final s_studio:I = 0x7f0e049f

.field public static final sa_perspective:I = 0x7f0e0199

.field public static final sa_rotate:I = 0x7f0e0190

.field public static final samsung_gallery:I = 0x7f0e032b

.field public static final samsung_social_album:I = 0x7f0e0308

.field public static final save:I = 0x7f0e020a

.field public static final save_as:I = 0x7f0e020d

.field public static final save_error:I = 0x7f0e0038

.field public static final save_imagenote_current_msg:I = 0x7f0e0212

.field public static final save_imagenote_msg:I = 0x7f0e0211

.field public static final save_into:I = 0x7f0e036e

.field public static final save_photoframe_current_msg:I = 0x7f0e0213

.field public static final saved:I = 0x7f0e020b

.field public static final saved_in:I = 0x7f0e020c

.field public static final saving_image:I = 0x7f0e0037

.field public static final scan_for_nearby_devices:I = 0x7f0e04db

.field public static final scan_started_device_list_etc:I = 0x7f0e012c

.field public static final scenery:I = 0x7f0e0478

.field public static final screen_mirroring:I = 0x7f0e0132

.field public static final scroll_left_or_right:I = 0x7f0e02c3

.field public static final search:I = 0x7f0e01f9

.field public static final search_hint:I = 0x7f0e01fb

.field public static final search_results:I = 0x7f0e0474

.field public static final search_results_single:I = 0x7f0e0475

.field public static final search_text:I = 0x7f0e021d

.field public static final search_view_guide_text:I = 0x7f0e04b6

.field public static final sec:I = 0x7f0e0189

.field public static final sec_1:I = 0x7f0e018a

.field public static final sec_3:I = 0x7f0e018b

.field public static final sec_5:I = 0x7f0e018c

.field public static final secret_box:I = 0x7f0e00ab

.field public static final see_your_old_photos:I = 0x7f0e04a0

.field public static final select:I = 0x7f0e003c

.field public static final select_a_picture:I = 0x7f0e02c2

.field public static final select_a_picture_face:I = 0x7f0e02c9

.field public static final select_album:I = 0x7f0e003b

.field public static final select_all:I = 0x7f0e0048

.field public static final select_an_album:I = 0x7f0e02c1

.field public static final select_channel:I = 0x7f0e00bf

.field public static final select_device:I = 0x7f0e0123

.field public static final select_event:I = 0x7f0e0443

.field public static final select_from:I = 0x7f0e009f

.field public static final select_image:I = 0x7f0e038a

.field public static final select_item:I = 0x7f0e003a

.field public static final select_video:I = 0x7f0e038b

.field public static final seleted_image_over:I = 0x7f0e0334

.field public static final send:I = 0x7f0e0318

.field public static final send_multi_result:I = 0x7f0e0319

.field public static final send_single_result_fail:I = 0x7f0e031b

.field public static final send_single_result_success:I = 0x7f0e031a

.field public static final send_to_other_devices:I = 0x7f0e0496

.field public static final sending:I = 0x7f0e00cd

.field public static final sensitivity:I = 0x7f0e00e1

.field public static final sepia_filter:I = 0x7f0e017c

.field public static final set_Wallpaper:I = 0x7f0e0119

.field public static final set_as:I = 0x7f0e0064

.field public static final set_as_caller_id:I = 0x7f0e00b5

.field public static final set_as_caller_id_menu:I = 0x7f0e00b4

.field public static final set_as_contact_icon:I = 0x7f0e00b6

.field public static final set_callerid_message:I = 0x7f0e00b2

.field public static final set_contacticon_message:I = 0x7f0e00b3

.field public static final set_image:I = 0x7f0e0379

.field public static final set_label_all_albums:I = 0x7f0e0093

.field public static final set_label_local_albums:I = 0x7f0e0094

.field public static final set_label_mtp_devices:I = 0x7f0e0095

.field public static final set_label_picasa_albums:I = 0x7f0e0096

.field public static final set_up_your_email_account:I = 0x7f0e00c1

.field public static final setas_both:I = 0x7f0e0040

.field public static final setas_lockscreen:I = 0x7f0e003f

.field public static final setting_off:I = 0x7f0e041c

.field public static final setting_off_value:I = 0x7f0e0435

.field public static final setting_on:I = 0x7f0e041d

.field public static final setting_on_value:I = 0x7f0e0436

.field public static final settings:I = 0x7f0e00ad

.field public static final shade:I = 0x7f0e008b

.field public static final share:I = 0x7f0e0047

.field public static final share_as_photo:I = 0x7f0e0367

.field public static final share_golf_file:I = 0x7f0e02ea

.field public static final share_panorama:I = 0x7f0e0366

.field public static final share_picture:I = 0x7f0e0312

.field public static final share_soundshot:I = 0x7f0e02e7

.field public static final show_all:I = 0x7f0e037c

.field public static final show_hidden_item:I = 0x7f0e01da

.field public static final show_images_only:I = 0x7f0e037a

.field public static final show_item:I = 0x7f0e0256

.field public static final show_on_map:I = 0x7f0e0054

.field public static final show_videos_only:I = 0x7f0e037b

.field public static final signature:I = 0x7f0e02ed

.field public static final signin_slink_account:I = 0x7f0e0499

.field public static final signout_notice:I = 0x7f0e032f

.field public static final simple_edit:I = 0x7f0e038c

.field public static final simple_filter:I = 0x7f0e017a

.field public static final single_picture_already_upload:I = 0x7f0e033a

.field public static final size_above:I = 0x7f0e0098

.field public static final size_below:I = 0x7f0e0097

.field public static final size_between:I = 0x7f0e0099

.field public static final skyscraper:I = 0x7f0e0486

.field public static final slide:I = 0x7f0e0193

.field public static final slideshow:I = 0x7f0e004a

.field public static final slideshow_interval:I = 0x7f0e0137

.field public static final slideshow_music:I = 0x7f0e0272

.field public static final slideshow_music_count:I = 0x7f0e0281

.field public static final slideshow_notibar_playback_on_tv:I = 0x7f0e018d

.field public static final slideshow_notibar_tap_here_to_stop_slideshow:I = 0x7f0e018f

.field public static final slideshow_notibar_tap_to_enable_mirroring:I = 0x7f0e04df

.field public static final slideshow_on_connected_screen:I = 0x7f0e018e

.field public static final slideshow_pause:I = 0x7f0e0180

.field public static final slideshow_play:I = 0x7f0e0184

.field public static final slideshow_settings:I = 0x7f0e0181

.field public static final slideshow_stop:I = 0x7f0e0183

.field public static final slideshow_video_clip:I = 0x7f0e0182

.field public static final slideshowsettings:I = 0x7f0e0186

.field public static final slink_charges:I = 0x7f0e049b

.field public static final slink_load_the_content:I = 0x7f0e049a

.field public static final slow:I = 0x7f0e00e3

.field public static final smooth:I = 0x7f0e0194

.field public static final snow:I = 0x7f0e0483

.field public static final snowy_day:I = 0x7f0e022d

.field public static final sns_app_install_notice:I = 0x7f0e0324

.field public static final sns_data_control:I = 0x7f0e027c

.field public static final sns_edit_comment_hint:I = 0x7f0e0325

.field public static final sns_qzone:I = 0x7f0e0322

.field public static final sns_renren:I = 0x7f0e0323

.field public static final sns_sina_weibo:I = 0x7f0e0321

.field public static final social:I = 0x7f0e00bb

.field public static final social_photo_album:I = 0x7f0e0307

.field public static final sort_by_latest:I = 0x7f0e02df

.field public static final sort_by_oldest:I = 0x7f0e02e0

.field public static final sort_by_tts:I = 0x7f0e02af

.field public static final sound_removed:I = 0x7f0e04a5

.field public static final sound_shot:I = 0x7f0e027e

.field public static final sound_shot_noti:I = 0x7f0e0280

.field public static final spanish_question_mark:I = 0x7f0e00ce

.field public static final speak_about_to_release_drag_event:I = 0x7f0e0296

.field public static final speak_add_face_as_name:I = 0x7f0e02a0

.field public static final speak_add_name:I = 0x7f0e02a1

.field public static final speak_burst_shot_pictures:I = 0x7f0e029f

.field public static final speak_button:I = 0x7f0e02ab

.field public static final speak_edit_photo_note:I = 0x7f0e02a6

.field public static final speak_face_detected:I = 0x7f0e02a4

.field public static final speak_face_name:I = 0x7f0e02a5

.field public static final speak_face_recognized:I = 0x7f0e02a3

.field public static final speak_folder_name_1_item:I = 0x7f0e0292

.field public static final speak_folder_name_n_items:I = 0x7f0e0293

.field public static final speak_item_selected:I = 0x7f0e029d

.field public static final speak_item_unselected:I = 0x7f0e029e

.field public static final speak_location:I = 0x7f0e02aa

.field public static final speak_new_folder_name:I = 0x7f0e0297

.field public static final speak_new_photo_detected:I = 0x7f0e0298

.field public static final speak_not_selected:I = 0x7f0e02b3

.field public static final speak_now:I = 0x7f0e0257

.field public static final speak_pick_select:I = 0x7f0e029b

.field public static final speak_picture_name:I = 0x7f0e0299

.field public static final speak_remove_tag:I = 0x7f0e02a2

.field public static final speak_selected:I = 0x7f0e02b2

.field public static final speak_selective_focus:I = 0x7f0e02a9

.field public static final speak_shot_and_more:I = 0x7f0e02a8

.field public static final speak_sound_scene:I = 0x7f0e02a7

.field public static final speak_start_drag_event_1_item:I = 0x7f0e0294

.field public static final speak_start_drag_event_n_items:I = 0x7f0e0295

.field public static final speak_tickbox:I = 0x7f0e029c

.field public static final speak_video_name:I = 0x7f0e029a

.field public static final speed:I = 0x7f0e0173

.field public static final spen_description_advanced_settings:I = 0x7f0e035f

.field public static final spen_description_color:I = 0x7f0e035e

.field public static final spen_description_palette:I = 0x7f0e0361

.field public static final spen_description_pen_preview:I = 0x7f0e0360

.field public static final spen_description_pentype_desc_artbrush:I = 0x7f0e0356

.field public static final spen_description_pentype_desc_calligraphybrush:I = 0x7f0e0357

.field public static final spen_description_pentype_desc_calligraphypen:I = 0x7f0e035b

.field public static final spen_description_pentype_desc_fountainpen:I = 0x7f0e035a

.field public static final spen_description_pentype_desc_magicpen:I = 0x7f0e0359

.field public static final spen_description_pentype_desc_pencil:I = 0x7f0e0358

.field public static final spen_description_pentype_pen:I = 0x7f0e0355

.field public static final spen_description_thickness:I = 0x7f0e035c

.field public static final spen_description_transparency:I = 0x7f0e035d

.field public static final spiral:I = 0x7f0e04e0

.field public static final split_close:I = 0x7f0e0259

.field public static final split_screen:I = 0x7f0e025a

.field public static final split_switch:I = 0x7f0e025b

.field public static final start:I = 0x7f0e0188

.field public static final stms_appgroup:I = 0x7f0e04c8

.field public static final street:I = 0x7f0e0487

.field public static final suggestion:I = 0x7f0e0442

.field public static final sunny_day:I = 0x7f0e022a

.field public static final surround_shot:I = 0x7f0e04ac

.field public static final swiping:I = 0x7f0e02c7

.field public static final switch_photo_filmstrip:I = 0x7f0e036a

.field public static final switch_photo_fullscreen:I = 0x7f0e0396

.field public static final switch_photo_grid:I = 0x7f0e036b

.field public static final switch_to_camera:I = 0x7f0e004d

.field public static final sync_album_error:I = 0x7f0e0368

.field public static final sync_before_sharing:I = 0x7f0e04bb

.field public static final sync_disabled:I = 0x7f0e026c

.field public static final sync_error:I = 0x7f0e026d

.field public static final sync_error_msg:I = 0x7f0e0328

.field public static final sync_in_progress:I = 0x7f0e026f

.field public static final sync_on_wifi_only:I = 0x7f0e0270

.field public static final sync_on_wifi_only_help:I = 0x7f0e0271

.field public static final sync_on_wlan_only:I = 0x7f0e0273

.field public static final sync_on_wlan_only_help:I = 0x7f0e0274

.field public static final sync_picasa_albums:I = 0x7f0e0372

.field public static final sync_with_dropbox:I = 0x7f0e0158

.field public static final sync_with_dropbox_description:I = 0x7f0e0159

.field public static final sync_with_server:I = 0x7f0e0327

.field public static final tab:I = 0x7f0e0291

.field public static final tab_albums:I = 0x7f0e0391

.field public static final tab_photos:I = 0x7f0e0390

.field public static final tag_added:I = 0x7f0e04a8

.field public static final tags:I = 0x7f0e00a8

.field public static final tags_removed:I = 0x7f0e048e

.field public static final tap_and_hold:I = 0x7f0e00ea

.field public static final tap_the_edit_icon:I = 0x7f0e02c8

.field public static final tap_the_name:I = 0x7f0e02cc

.field public static final tap_the_white_box:I = 0x7f0e02cb

.field public static final tap_the_yellow_box:I = 0x7f0e02ca

.field public static final tap_to_add_images:I = 0x7f0e002e

.field public static final tap_to_start:I = 0x7f0e04b7

.field public static final tap_to_sync_content:I = 0x7f0e027d

.field public static final tap_to_view:I = 0x7f0e02b8

.field public static final tapping_and_holding:I = 0x7f0e02c6

.field public static final text_no_comment:I = 0x7f0e0313

.field public static final text_shadowDx:I = 0x7f0e001a

.field public static final text_shadowDy:I = 0x7f0e001b

.field public static final text_shadowRadius:I = 0x7f0e001c

.field public static final the_same_wallpaper_will_be_applied_to_the_lock_screen:I = 0x7f0e0118

.field public static final there_is_no_album:I = 0x7f0e01cf

.field public static final there_is_no_events:I = 0x7f0e044a

.field public static final there_is_only_one_album:I = 0x7f0e01ce

.field public static final this_month:I = 0x7f0e0240

.field public static final this_week:I = 0x7f0e023e

.field public static final this_year:I = 0x7f0e0242

.field public static final thumbnail_view_split_album_seprator_ratio:I = 0x7f0e0025

.field public static final thumbnail_view_split_album_seprator_ratio_divider_height:I = 0x7f0e0026

.field public static final tilting_help_dialog_message:I = 0x7f0e00e6

.field public static final time:I = 0x7f0e0073

.field public static final time_filter:I = 0x7f0e044b

.field public static final time_lapse_hours:I = 0x7f0e041b

.field public static final time_lapse_minutes:I = 0x7f0e041a

.field public static final time_lapse_seconds:I = 0x7f0e0419

.field public static final timeline_view:I = 0x7f0e01b1

.field public static final times:I = 0x7f0e037d

.field public static final timeview_decFactor:I = 0x7f0e0014

.field public static final timeview_speedRatio:I = 0x7f0e0013

.field public static final title:I = 0x7f0e0071

.field public static final to:I = 0x7f0e0331

.field public static final today:I = 0x7f0e023a

.field public static final trains:I = 0x7f0e0493

.field public static final transition_effect:I = 0x7f0e0185

.field public static final trim:I = 0x7f0e01b4

.field public static final trim_action:I = 0x7f0e0369

.field public static final trim_label:I = 0x7f0e039f

.field public static final try_air_motion:I = 0x7f0e00dc

.field public static final try_air_motion_jpn:I = 0x7f0e00dd

.field public static final try_panning:I = 0x7f0e00df

.field public static final try_peek:I = 0x7f0e00e0

.field public static final try_tilt:I = 0x7f0e00de

.field public static final try_to_set_local_album_available_offline:I = 0x7f0e037f

.field public static final tts_private:I = 0x7f0e02ad

.field public static final tungsten:I = 0x7f0e0087

.field public static final tutorial_complete:I = 0x7f0e02dc

.field public static final two_years_ago:I = 0x7f0e0244

.field public static final unable_reordered_remote_album:I = 0x7f0e04c2

.field public static final unable_share_unsynced_images:I = 0x7f0e04ba

.field public static final unable_to_copy:I = 0x7f0e01c8

.field public static final unable_to_create_folder:I = 0x7f0e01cb

.field public static final unable_to_move:I = 0x7f0e01c9

.field public static final unable_to_play_during_call:I = 0x7f0e0066

.field public static final unable_to_rename:I = 0x7f0e010f

.field public static final undo:I = 0x7f0e0207

.field public static final unfavourite:I = 0x7f0e02be

.field public static final ungrouped:I = 0x7f0e0068

.field public static final unhide_item_failed:I = 0x7f0e01df

.field public static final unhide_item_success:I = 0x7f0e01de

.field public static final unhide_items_failed:I = 0x7f0e01e0

.field public static final unit_mm:I = 0x7f0e0082

.field public static final unknown:I = 0x7f0e00ff

.field public static final unlimited:I = 0x7f0e0106

.field public static final unlock_it_now_q:I = 0x7f0e00f5

.field public static final unnamed:I = 0x7f0e00c2

.field public static final unselect_account_notice:I = 0x7f0e0326

.field public static final unsupported_file:I = 0x7f0e0107

.field public static final untagged:I = 0x7f0e0067

.field public static final up_button:I = 0x7f0e021e

.field public static final up_to_limit:I = 0x7f0e0332

.field public static final upload:I = 0x7f0e010c

.field public static final upload_again_notice:I = 0x7f0e0339

.field public static final upload_to_baidu_cloud:I = 0x7f0e0306

.field public static final use_data_network:I = 0x7f0e0303

.field public static final use_motion:I = 0x7f0e00d0

.field public static final use_motion_after_calibration:I = 0x7f0e00e4

.field public static final use_permission_alert:I = 0x7f0e02f9

.field public static final use_permission_alert_title:I = 0x7f0e02f8

.field public static final use_personal_information:I = 0x7f0e0304

.field public static final use_wlan:I = 0x7f0e0302

.field public static final validity:I = 0x7f0e00f9

.field public static final vehicles:I = 0x7f0e047d

.field public static final via_home_network:I = 0x7f0e0124

.field public static final via_screen_mirroring:I = 0x7f0e0125

.field public static final video:I = 0x7f0e0177

.field public static final video_err:I = 0x7f0e0065

.field public static final video_maker:I = 0x7f0e011d

.field public static final view_by:I = 0x7f0e04aa

.field public static final view_by_all:I = 0x7f0e0150

.field public static final view_by_baidu:I = 0x7f0e0156

.field public static final view_by_dropbox:I = 0x7f0e0153

.field public static final view_by_facebook:I = 0x7f0e0154

.field public static final view_by_format:I = 0x7f0e04bf

.field public static final view_by_local:I = 0x7f0e0151

.field public static final view_by_local_vzw:I = 0x7f0e0152

.field public static final view_by_picasa:I = 0x7f0e0155

.field public static final view_by_tcloud:I = 0x7f0e0157

.field public static final viewing_picture_tutorial_complete:I = 0x7f0e02dd

.field public static final vintage_filter:I = 0x7f0e017d

.field public static final virtual_tour:I = 0x7f0e04ab

.field public static final voice_input:I = 0x7f0e0467

.field public static final voice_search:I = 0x7f0e0466

.field public static final wallpaper:I = 0x7f0e003d

.field public static final warning:I = 0x7f0e021b

.field public static final waterfall:I = 0x7f0e048a

.field public static final wb_auto:I = 0x7f0e0084

.field public static final wb_custom:I = 0x7f0e008f

.field public static final wb_flash:I = 0x7f0e0088

.field public static final weather:I = 0x7f0e0223

.field public static final weibo_showcomment_fail:I = 0x7f0e0317

.field public static final weibosdk_send_failed:I = 0x7f0e031c

.field public static final white_balance:I = 0x7f0e007f

.field public static final widget_frametype_multi_left_rightupdown:I = 0x7f0e02b5

.field public static final widget_frametype_multi_up_down:I = 0x7f0e02b6

.field public static final widget_frametype_multi_up_downleftright:I = 0x7f0e02b7

.field public static final widget_frametype_single:I = 0x7f0e02b4

.field public static final widget_layout_type:I = 0x7f0e0136

.field public static final widget_type:I = 0x7f0e0385

.field public static final widget_type_album:I = 0x7f0e00a0

.field public static final widget_type_person:I = 0x7f0e00a2

.field public static final widget_type_photos:I = 0x7f0e00a3

.field public static final widget_type_shuffle:I = 0x7f0e00a1

.field public static final width:I = 0x7f0e0076

.field public static final wifi_dialog_body:I = 0x7f0e0275

.field public static final wifi_dialog_title:I = 0x7f0e0276

.field public static final with_person:I = 0x7f0e0224

.field public static final with_person_and_another:I = 0x7f0e0225

.field public static final with_person_and_others:I = 0x7f0e0226

.field public static final wlan_update_msg_detail:I = 0x7f0e0310

.field public static final wlan_update_msg_title:I = 0x7f0e030f

.field public static final x02:I = 0x7f0e01b6

.field public static final x02_tts:I = 0x7f0e01bb

.field public static final x05:I = 0x7f0e01b7

.field public static final x05_tts:I = 0x7f0e01bc

.field public static final x10:I = 0x7f0e01b8

.field public static final x10_tts:I = 0x7f0e01bd

.field public static final x20:I = 0x7f0e01b9

.field public static final x20_tts:I = 0x7f0e01be

.field public static final years_ago:I = 0x7f0e04a1

.field public static final yesterday:I = 0x7f0e023c

.field public static final your_contacts:I = 0x7f0e02fc

.field public static final your_item_will_be_downloaded:I = 0x7f0e0130

.field public static final your_location:I = 0x7f0e02fd

.field public static final your_phone_number:I = 0x7f0e02fb

.field public static final zoom_in:I = 0x7f0e02c4

.field public static final zoom_out:I = 0x7f0e02c5

.field public static final zoomin:I = 0x7f0e0195

.field public static final zooming_dialog_message_1:I = 0x7f0e00d5

.field public static final zooming_dialog_message_2:I = 0x7f0e00d6

.field public static final zooming_dialog_message_3:I = 0x7f0e00d7


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
