.class Lcom/sec/android/gallery3d/data/UriSource;
.super Lcom/sec/android/gallery3d/data/MediaSource;
.source "UriSource.java"


# static fields
.field private static final CHARSET_UTF_8:Ljava/lang/String; = "utf-8"

.field private static final IMAGE_TYPE_ANY:Ljava/lang/String; = "image/*"

.field private static final IMAGE_TYPE_PREFIX:Ljava/lang/String; = "image/"

.field private static final TAG:Ljava/lang/String; = "UriSource"


# instance fields
.field private final DOWNLOADABLE_HELP_ITEMSET:I

.field private final HELP_ITEMSET:I

.field private final MMS_AUTHORITY:Ljava/lang/String;

.field private final NO_MATCH:I

.field private final URI_ITEMSET:I

.field private final URI_URI_ITEMSET:I

.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 6
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 54
    const-string/jumbo v0, "uri"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    .line 41
    iput v2, p0, Lcom/sec/android/gallery3d/data/UriSource;->URI_ITEMSET:I

    .line 42
    iput v3, p0, Lcom/sec/android/gallery3d/data/UriSource;->HELP_ITEMSET:I

    .line 43
    iput v4, p0, Lcom/sec/android/gallery3d/data/UriSource;->DOWNLOADABLE_HELP_ITEMSET:I

    .line 44
    iput v5, p0, Lcom/sec/android/gallery3d/data/UriSource;->URI_URI_ITEMSET:I

    .line 46
    iput v1, p0, Lcom/sec/android/gallery3d/data/UriSource;->NO_MATCH:I

    .line 49
    new-instance v0, Landroid/content/UriMatcher;

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/UriSource;->mUriMatcher:Landroid/content/UriMatcher;

    .line 50
    const-string v0, "mms"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/UriSource;->MMS_AUTHORITY:Ljava/lang/String;

    .line 55
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/UriSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 57
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/UriSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 58
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/uri/mediaset/*"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/uri/help/mediaset"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/uri/downloadablehelp/mediaset"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "mms"

    const-string v2, "part"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 63
    return-void
.end method

.method private getMimeType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 109
    const-string v2, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 110
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "extension":Ljava/lang/String;
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 114
    .local v1, "type":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 120
    .end local v0    # "extension":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 118
    .end local v1    # "type":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/UriSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 119
    .restart local v1    # "type":Ljava/lang/String;
    if-nez v1, :cond_0

    const-string v1, "image/*"

    goto :goto_0
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 67
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v2

    .line 79
    .local v2, "segment":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 81
    .local v0, "decoded":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 93
    array-length v3, v2

    if-eq v3, v5, :cond_1

    .line 94
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bad path: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 83
    :pswitch_0
    array-length v3, v2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 84
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bad path: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 87
    :cond_0
    new-instance v3, Lcom/sec/android/gallery3d/data/UriMediaSet;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    aget-object v5, v2, v5

    invoke-direct {v3, v4, p1, v5}, Lcom/sec/android/gallery3d/data/UriMediaSet;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;)V

    .line 102
    :goto_0
    return-object v3

    .line 89
    :pswitch_1
    new-instance v3, Lcom/sec/android/gallery3d/data/HelpMediaSet;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    aget-object v5, v2, v5

    const/4 v6, 0x0

    invoke-direct {v3, v4, p1, v5, v6}, Lcom/sec/android/gallery3d/data/HelpMediaSet;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;Z)V

    goto :goto_0

    .line 91
    :pswitch_2
    new-instance v3, Lcom/sec/android/gallery3d/data/HelpMediaSet;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    aget-object v5, v2, v5

    invoke-direct {v3, v4, p1, v5, v6}, Lcom/sec/android/gallery3d/data/HelpMediaSet;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;Z)V

    goto :goto_0

    .line 98
    :cond_1
    const/4 v3, 0x1

    :try_start_0
    aget-object v3, v2, v3

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 102
    :goto_1
    new-instance v3, Lcom/sec/android/gallery3d/data/UriImage;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, p1, v5}, Lcom/sec/android/gallery3d/data/UriImage;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Landroid/net/Uri;)V

    goto :goto_0

    .line 99
    :catch_0
    move-exception v1

    .line 100
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 145
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriSource;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 152
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 155
    .local v1, "mimeType":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v4, "image/"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "*/*"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 157
    :cond_0
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/uri/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-static {v5, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 164
    .end local v1    # "mimeType":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 147
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    check-cast v4, Landroid/content/Context;

    invoke-static {v4, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getMidFromMmsList(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v2

    .line 149
    .local v2, "mid":J
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/uri/mediaset/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    goto :goto_0

    .line 158
    .end local v2    # "mid":J
    .restart local v1    # "mimeType":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 164
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
