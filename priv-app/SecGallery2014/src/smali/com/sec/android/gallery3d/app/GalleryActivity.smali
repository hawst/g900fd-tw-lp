.class public Lcom/sec/android/gallery3d/app/GalleryActivity;
.super Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
.source "GalleryActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# static fields
.field public static final ACTION_MULTIPLE_PICK:Ljava/lang/String; = "android.intent.action.MULTIPLE_PICK"

.field public static final ACTION_PERSON_PICK:Ljava/lang/String; = "android.intent.action.PERSON_PICK"

.field public static final ACTION_REVIEW:Ljava/lang/String; = "com.android.camera.action.REVIEW"

.field public static final ACTION_SEARCH_VIEW:Ljava/lang/String; = "com.android.gallery.action.SEARCH_VIEW"

.field public static final ACTION_VIEW:Ljava/lang/String; = "android.intent.action.VIEW"

.field public static final ACTION_VIEW_MEMO:Ljava/lang/String; = "android.intent.action.VIEW_GALLERY_TO_MEMO"

.field public static APP_CAMERA:Ljava/lang/String; = null

.field private static final CANDIDATES_CNT:I = 0x3

.field public static final EXTRA_DREAM:Ljava/lang/String; = "dream"

.field public static final EXTRA_SLIDESHOW:Ljava/lang/String; = "slideshow"

.field public static FACE_IMAGE_MODIFIED_DATE:Ljava/lang/String; = null

.field public static FACE_IMAGE_PATH:Ljava/lang/String; = null

.field public static FACE_IMAGE_SIZE:Ljava/lang/String; = null

.field public static FACE_IMAGE_URI:Ljava/lang/String; = null

.field public static FACE_START_APP:Ljava/lang/String; = null

.field public static final FROM_SETUP_WIDZARD:Ljava/lang/String; = "from-sw"

.field private static final ID_RANDOMER:Ljava/util/Random;

.field public static final KEY_CONTACT_CALLER_ID:Ljava/lang/String; = "caller_id_pick"

.field public static final KEY_FACE_NAME:Ljava/lang/String; = "faceTagedName"

.field public static final KEY_FROM_CAMERA:Ljava/lang/String; = "from-Camera"

.field public static final KEY_FROM_MYFILES:Ljava/lang/String; = "from-myfiles"

.field public static final KEY_FROM_TOGETHER:Ljava/lang/String; = "from-WeTogether"

.field public static final KEY_GET_ALBUM:Ljava/lang/String; = "get-album"

.field public static final KEY_GET_CONTENT:Ljava/lang/String; = "get-content"

.field public static final KEY_INCLUDE_RECOMMEND:Ljava/lang/String; = "include-recommend"

.field public static final KEY_IS_ALBUM_PICK:Ljava/lang/String; = "album-pick"

.field public static final KEY_IS_GALLERY_WIDGET:Ljava/lang/String; = "photo-pick"

.field public static final KEY_IS_GIF_MAKER:Ljava/lang/String; = "photo-pick-gifmaker"

.field public static final KEY_IS_INSIDE_GALLERY:Ljava/lang/String; = "pick-from-gallery"

.field public static final KEY_IS_MULTI_PICK:Ljava/lang/String; = "multi-pick"

.field public static final KEY_IS_ONLY_ALBUM_PICK:Ljava/lang/String; = "only-album-pick"

.field public static final KEY_IS_PERSON_PICK:Ljava/lang/String; = "person-pick"

.field public static final KEY_IS_PHOTOWALL:Ljava/lang/String; = "from-photowall"

.field public static final KEY_IS_QUERY_URI:Ljava/lang/String; = "query_uri"

.field public static final KEY_MAX_PICK_ITEM:Ljava/lang/String; = "pick-max-item"

.field public static final KEY_MEDIA_TYPES:Ljava/lang/String; = "mediaTypes"

.field public static final KEY_MIN_PICK_ITEM:Ljava/lang/String; = "pick-min-item"

.field public static final KEY_MOTION_TILT:Ljava/lang/String; = "from-motiontilt"

.field public static final KEY_SINGLE_ALBUM:Ljava/lang/String; = "single-album"

.field public static final KEY_TYPE_BITS:Ljava/lang/String; = "type-bits"

.field private static final MSG_FINISH_PENDING:I = 0x2

.field private static final MSG_PAUSE_PENDING:I = 0x1

.field private static final MSG_RESUME_PENDING:I = 0x3

.field public static final REQUEST_VIDEO_PLAY_COVER_MODE:I

.field private static final TAG:Ljava/lang/String;

.field public static volatile mIsContactDBAvailable:Z

.field public static volatile mIsDocumentScanning:Z


# instance fields
.field private isBackPressed:Z

.field private mAGIF:Lcom/quramsoft/agif/QuramAGIF;

.field private mActivityCreateTime:J

.field private mAutoRecommendTime:J

.field private mBaiduCloudDragDropFinishReceiver:Landroid/content/BroadcastReceiver;

.field private mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

.field private mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mDragListener:Landroid/view/View$OnDragListener;

.field private mFaceScannerReceiver:Landroid/content/BroadcastReceiver;

.field private mFeatureManager:Lcom/sec/android/gallery3d/util/FeatureManager;

.field private mFestivalDataCount:I

.field private mFestivalEndTime:J

.field private mFestivalStartTime:J

.field private mFinishAtSecrureLock:Z

.field private mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private final mGalleryId:I

.field private mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

.field private final mHandler:Landroid/os/Handler;

.field private mHardKeyboardHidden:I

.field private mIsActive:Z

.field private mIsContactsChanged:Z

.field private mIsFestival:Z

.field private mIsFromCamera:Z

.field private mIsFromMyFiles:Z

.field public mIsFromQuickStart:Z

.field public mIsInTouchMode:Z

.field public mIsInViewMode:Z

.field public mIsSingleAlbumForPick:Z

.field private mMainLayout:Landroid/widget/RelativeLayout;

.field private mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

.field public mNeedFirstUpOfDetailView:Z

.field private mPaused:Z

.field private mPausedByHomePress:Z

.field private mPreDisplay:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

.field private mSDCardRemovedReceiver:Landroid/content/BroadcastReceiver;

.field private mSimilarPersonsReceiver:Landroid/content/BroadcastReceiver;

.field private mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

.field private mUpdateBackscreenInPauseState:Z

.field private mVersionCheckDialog:Landroid/app/Dialog;

.field private nFaceRecommendationObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 108
    const-class v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;

    .line 153
    const-string v0, "FACE_IMAGE_URI"

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->FACE_IMAGE_URI:Ljava/lang/String;

    .line 154
    const-string v0, "FACE_IMAGE_MODIFIED_DATE"

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->FACE_IMAGE_MODIFIED_DATE:Ljava/lang/String;

    .line 155
    const-string v0, "FACE_IMAGE_PATH"

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->FACE_IMAGE_PATH:Ljava/lang/String;

    .line 156
    const-string v0, "FACE_IMAGE_SIZE"

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->FACE_IMAGE_SIZE:Ljava/lang/String;

    .line 157
    const-string/jumbo v0, "startApp"

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->FACE_START_APP:Ljava/lang/String;

    .line 158
    const-string v0, "camera"

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->APP_CAMERA:Ljava/lang/String;

    .line 172
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsDocumentScanning:Z

    .line 176
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsContactDBAvailable:Z

    .line 179
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->ID_RANDOMER:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;-><init>()V

    .line 159
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsContactsChanged:Z

    .line 161
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->isBackPressed:Z

    .line 162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsInTouchMode:Z

    .line 163
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFromCamera:Z

    .line 164
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFromMyFiles:Z

    .line 165
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsSingleAlbumForPick:Z

    .line 166
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsInViewMode:Z

    .line 167
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mNeedFirstUpOfDetailView:Z

    .line 168
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPausedByHomePress:Z

    .line 169
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPaused:Z

    .line 170
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFromQuickStart:Z

    .line 177
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHardKeyboardHidden:I

    .line 180
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->ID_RANDOMER:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryId:I

    .line 181
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    .line 182
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    .line 183
    iput-wide v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mAutoRecommendTime:J

    .line 186
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    .line 192
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mUpdateBackscreenInPauseState:Z

    .line 193
    iput-wide v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mActivityCreateTime:J

    .line 195
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFestival:Z

    .line 206
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFinishAtSecrureLock:Z

    .line 207
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsActive:Z

    .line 387
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/GalleryActivity$1;-><init>(Lcom/sec/android/gallery3d/app/GalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mSDCardRemovedReceiver:Landroid/content/BroadcastReceiver;

    .line 767
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->nFaceRecommendationObserver:Landroid/database/ContentObserver;

    .line 770
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/GalleryActivity$2;-><init>(Lcom/sec/android/gallery3d/app/GalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mSimilarPersonsReceiver:Landroid/content/BroadcastReceiver;

    .line 801
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/GalleryActivity$3;-><init>(Lcom/sec/android/gallery3d/app/GalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFaceScannerReceiver:Landroid/content/BroadcastReceiver;

    .line 1050
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/GalleryActivity$4;-><init>(Lcom/sec/android/gallery3d/app/GalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mDragListener:Landroid/view/View$OnDragListener;

    .line 1227
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryActivity$6;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity$6;-><init>(Lcom/sec/android/gallery3d/app/GalleryActivity;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHandler:Landroid/os/Handler;

    .line 1349
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/GalleryActivity$7;-><init>(Lcom/sec/android/gallery3d/app/GalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mBaiduCloudDragDropFinishReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/GalleryActivity;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryActivity;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/GalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryActivity;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/app/GalleryActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryActivity;

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mAutoRecommendTime:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/GalleryActivity;)Landroid/database/ContentObserver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryActivity;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->nFaceRecommendationObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/app/GalleryActivity;Landroid/database/ContentObserver;)Landroid/database/ContentObserver;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryActivity;
    .param p1, "x1"    # Landroid/database/ContentObserver;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->nFaceRecommendationObserver:Landroid/database/ContentObserver;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/app/GalleryActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryActivity;

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsActive:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/app/GalleryActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryActivity;

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mUpdateBackscreenInPauseState:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/gallery3d/app/GalleryActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mUpdateBackscreenInPauseState:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/app/GalleryActivity;)Lcom/sec/android/gallery3d/app/GalleryCoverMode;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryActivity;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/app/GalleryActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryActivity;

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFinishAtSecrureLock:Z

    return v0
.end method

.method private checkLowMemoryAndFinishActivity()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1187
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkLowStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1188
    const/4 v0, 0x0

    .line 1202
    :goto_0
    return v0

    .line 1190
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUMS:Z

    if-eqz v1, :cond_1

    const-string/jumbo v1, "sys.usb.config"

    const-string v2, "none"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mass_storage"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1191
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e016e

    invoke-static {v1, v2, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;II)V

    .line 1192
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->finish()V

    goto :goto_0

    .line 1194
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    if-eqz v1, :cond_2

    .line 1195
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->dismissDialog()V

    .line 1196
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    .line 1199
    :cond_2
    new-instance v1, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;Landroid/content/Intent;Z)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    .line 1200
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->showDialog()V

    goto :goto_0
.end method

.method private enableFinishingAtSecureLock()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1409
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFinishAtSecrureLock:Z

    .line 1410
    invoke-virtual {p0, v1, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->convertToTranslucent(Landroid/app/Activity$TranslucentConversionListener;Landroid/app/ActivityOptions;)Z

    .line 1411
    return-void
.end method

.method private fromCamera(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x0

    .line 1274
    const-string v1, "from-Camera"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryActivity;->APP_CAMERA:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryActivity;->FACE_START_APP:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private hideBargeInNotification()V
    .locals 3

    .prologue
    .line 1308
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;->HIDE:Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    aput-object v2, v0, v1

    .line 1311
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SHOW_BARGEIN_NOTIFICATION"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1312
    return-void
.end method

.method private hideRootView()V
    .locals 1

    .prologue
    .line 1279
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->fromCamera(Landroid/content/Intent;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->hideRootView(Z)V

    .line 1280
    return-void
.end method

.method private hideRootView(Z)V
    .locals 3
    .param p1, "isHide"    # Z

    .prologue
    .line 1283
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v2, 0x1

    if-eqz p1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->HIDE:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    :goto_0
    aput-object v1, v0, v2

    .line 1286
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "HIDE_ROOT_VIEW"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1287
    return-void

    .line 1283
    .end local v0    # "params":[Ljava/lang/Object;
    :cond_0
    sget-object v1, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->SHOW:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    goto :goto_0
.end method

.method private initActionBarForSCamera(Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;)V
    .locals 5
    .param p1, "motionType"    # Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .prologue
    const/16 v4, 0x1000

    const/4 v3, 0x1

    .line 1290
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 1291
    .local v0, "actionBar":Landroid/app/ActionBar;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1292
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 1293
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1294
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1295
    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 1297
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne p1, v2, :cond_1

    .line 1298
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1299
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1300
    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 1302
    :cond_0
    const v2, 0x7f0203b3

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setIcon(I)V

    .line 1304
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setArbitarySizeForSCamera()V

    .line 1305
    return-void
.end method

.method private isValidDataUri(Landroid/net/Uri;)Z
    .locals 4
    .param p1, "dataUri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 602
    if-nez p1, :cond_0

    .line 608
    :goto_0
    return v1

    .line 604
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "r"

    invoke-virtual {v2, p1, v3}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 605
    const/4 v1, 0x1

    goto :goto_0

    .line 606
    :catch_0
    move-exception v0

    .line 607
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;

    const-string v3, "cannot open uri: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V
    .locals 3
    .param p1, "cmd"    # Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    .prologue
    .line 1419
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 1422
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "POST_GALLERY"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1423
    return-void
.end method

.method private removeSelectionPopup()V
    .locals 4

    .prologue
    .line 1370
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v2

    .line 1372
    .local v2, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v3, v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-nez v3, :cond_0

    instance-of v3, v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v3, :cond_2

    .line 1373
    :cond_0
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/ActivityState;->getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 1374
    .local v0, "actionBarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    instance-of v3, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;

    if-eqz v3, :cond_1

    .line 1375
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->getPopUpMenu()Landroid/widget/ListPopupWindow;

    move-result-object v1

    .line 1376
    .local v1, "menu":Landroid/widget/ListPopupWindow;
    if-eqz v1, :cond_1

    .line 1377
    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 1389
    .end local v0    # "actionBarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    .end local v1    # "menu":Landroid/widget/ListPopupWindow;
    .end local v2    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_1
    :goto_0
    return-void

    .line 1380
    .restart local v2    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_2
    instance-of v3, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-eqz v3, :cond_1

    .line 1381
    check-cast v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    .end local v2    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 1382
    .restart local v0    # "actionBarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    instance-of v3, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    if-eqz v3, :cond_1

    .line 1383
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->getPopUpMenu()Landroid/widget/ListPopupWindow;

    move-result-object v1

    .line 1384
    .restart local v1    # "menu":Landroid/widget/ListPopupWindow;
    if-eqz v1, :cond_1

    .line 1385
    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->dismiss()V

    goto :goto_0
.end method

.method private restoreViewState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->restoreFromState(Landroid/os/Bundle;)V

    .line 1104
    return-void
.end method

.method private setArbitarySizeForSCamera()V
    .locals 5

    .prologue
    .line 1264
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1265
    .local v1, "LCDSize":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 1266
    .local v0, "LCDHeight":I
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 1268
    .local v2, "LCDWidth":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mMainLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 1269
    .local v3, "lp":Landroid/view/ViewGroup$LayoutParams;
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1270
    iput v0, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1271
    return-void
.end method

.method private setOnDragListener()V
    .locals 2

    .prologue
    .line 1074
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    .line 1075
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1077
    :cond_0
    return-void
.end method

.method private setStatusBarManager(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1315
    const-string/jumbo v1, "statusbar"

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 1316
    .local v0, "statusBar":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_0

    .line 1317
    invoke-virtual {v0, p1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 1318
    :cond_0
    return-void
.end method

.method private startView(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1080
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 1081
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1082
    .local v0, "extras":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 1083
    .local v1, "helpMode":Z
    if-eqz v0, :cond_0

    .line 1084
    const-string v4, "IsHelpMode"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1087
    :cond_0
    if-eqz p1, :cond_1

    if-eqz v1, :cond_2

    .line 1088
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v5, "START_GALLERY_VIEW"

    invoke-virtual {v4, v5, p0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1100
    :goto_0
    return-void

    .line 1090
    :cond_2
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-eqz v4, :cond_4

    .line 1091
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v3

    .line 1092
    .local v3, "tutorialType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v4, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v3, v4, :cond_3

    sget-object v4, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v3, v4, :cond_3

    sget-object v4, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v3, v4, :cond_4

    .line 1095
    :cond_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/app/GalleryMotion;->setMotionHelpView(Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;)V

    .line 1098
    .end local v3    # "tutorialType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    :cond_4
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->restoreViewState(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private unregisterMediaScannerReceiver()V
    .locals 3

    .prologue
    .line 1180
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$MediaScanner;->UNREGISTER:Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$MediaScanner;

    aput-object v2, v0, v1

    .line 1183
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "MEDIASCANNER_RECEIVER"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1184
    return-void
.end method

.method private unregisterSecretModeReceiver()V
    .locals 3

    .prologue
    .line 1173
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$CmdType;->UNREGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$CmdType;

    aput-object v2, v0, v1

    .line 1176
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SECRET_MODE_RECEIVER"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1177
    return-void
.end method

.method private updatePickerModeWindow(Landroid/view/WindowManager$LayoutParams;)V
    .locals 3
    .param p1, "lp"    # Landroid/view/WindowManager$LayoutParams;

    .prologue
    const/4 v2, -0x1

    .line 1392
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    .line 1393
    .local v0, "o":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1394
    const/16 v1, 0x50

    iput v1, p1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1395
    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1396
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1397
    const v1, 0x7f04001f

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/gallery3d/app/GalleryActivity;->overridePendingTransition(II)V

    .line 1405
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1406
    return-void

    .line 1399
    :cond_0
    const/4 v1, 0x5

    iput v1, p1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1400
    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1401
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v1

    iput v1, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1402
    const v1, 0x7f040020

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/gallery3d/app/GalleryActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method


# virtual methods
.method public disableFinishingAtSecureLock()V
    .locals 1

    .prologue
    .line 1414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFinishAtSecrureLock:Z

    .line 1415
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->convertFromTranslucent()V

    .line 1416
    return-void
.end method

.method public dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 20
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 620
    invoke-super/range {p0 .. p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v10

    .line 621
    .local v10, "result":Z
    if-eqz v10, :cond_4

    .line 622
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 623
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 624
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusableInTouchMode(Z)V

    .line 626
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContentPane()Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v17

    if-eqz v17, :cond_3

    .line 627
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContentPane()Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/ui/GLView;->onGenericMotionCancel()V

    .line 684
    :cond_1
    :goto_0
    sget-boolean v17, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v17, :cond_2

    .line 685
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/app/ActionBar;->getHeight()I

    move-result v3

    .line 686
    .local v3, "actionBarHeight":I
    invoke-static/range {p0 .. p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v5

    .line 687
    .local v5, "actionBarWidth":I
    new-instance v4, Landroid/graphics/Rect;

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v4, v0, v1, v5, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 688
    .local v4, "actionBarRect":Landroid/graphics/Rect;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v17

    move/from16 v0, v17

    float-to-int v15, v0

    .line 689
    .local v15, "x":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v16, v0

    .line 690
    .local v16, "y":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v11

    .line 691
    .local v11, "state":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v0, v11, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move/from16 v17, v0

    if-eqz v17, :cond_2

    move/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Landroid/graphics/Rect;->contains(II)Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/app/ActionBar;->isShowing()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 692
    check-cast v11, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .end local v11    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-virtual {v11}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->removeHidingMessage()V

    .line 696
    .end local v3    # "actionBarHeight":I
    .end local v4    # "actionBarRect":Landroid/graphics/Rect;
    .end local v5    # "actionBarWidth":I
    .end local v15    # "x":I
    .end local v16    # "y":I
    :cond_2
    return v10

    .line 629
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getRootLayer()Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v7

    .line 630
    .local v7, "layer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    if-eqz v7, :cond_1

    .line 631
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchGenericMotionCancel()V

    goto :goto_0

    .line 634
    .end local v7    # "layer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 635
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v14

    .line 636
    .local v14, "viewStateProxy":Lcom/sec/android/gallery3d/app/StateManager;
    const/4 v6, 0x0

    .line 637
    .local v6, "isMoreInfoInFront":Z
    if-eqz v14, :cond_5

    .line 638
    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v12

    .line 639
    .local v12, "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    if-eqz v12, :cond_5

    instance-of v0, v12, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move/from16 v17, v0

    if-eqz v17, :cond_5

    .line 640
    check-cast v12, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .end local v12    # "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v9, v12, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .line 641
    .local v9, "moreInfo":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEnabled()Z

    move-result v17

    if-eqz v17, :cond_5

    .line 642
    const/4 v6, 0x1

    .line 647
    .end local v9    # "moreInfo":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;
    :cond_5
    sget-boolean v17, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSDL:Z

    if-eqz v17, :cond_6

    .line 648
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mMainLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v17, v0

    const/16 v18, 0x40

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/widget/RelativeLayout;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 649
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mMainLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v17, v0

    const/16 v18, 0x80

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/widget/RelativeLayout;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 650
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mMainLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v17, v0

    const/16 v18, 0x4

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/widget/RelativeLayout;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    goto/16 :goto_0

    .line 652
    :cond_6
    if-nez v6, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    .line 653
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v13

    .line 654
    .local v13, "viewRootImpl":Landroid/view/ViewRootImpl;
    if-eqz v13, :cond_1

    .line 655
    invoke-virtual {v13}, Landroid/view/ViewRootImpl;->getAccessibilityFocusedHost()Landroid/view/View;

    move-result-object v2

    .line 656
    .local v2, "accessibilityFocusedHost":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 657
    const/16 v17, 0x80

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    goto/16 :goto_0

    .line 664
    .end local v2    # "accessibilityFocusedHost":Landroid/view/View;
    .end local v6    # "isMoreInfoInFront":Z
    .end local v13    # "viewRootImpl":Landroid/view/ViewRootImpl;
    .end local v14    # "viewStateProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :cond_7
    const/4 v8, 0x1

    .line 665
    .local v8, "mFocusable":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v12

    .line 666
    .restart local v12    # "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v0, v12, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    move/from16 v17, v0

    if-nez v17, :cond_1

    .line 667
    if-eqz v12, :cond_9

    instance-of v0, v12, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move/from16 v17, v0

    if-eqz v17, :cond_9

    .line 668
    check-cast v12, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .end local v12    # "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v9, v12, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .line 669
    .restart local v9    # "moreInfo":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;
    if-eqz v9, :cond_8

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEnabled()Z

    move-result v17

    if-eqz v17, :cond_8

    .line 670
    const/4 v8, 0x0

    .line 675
    .end local v9    # "moreInfo":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;
    :cond_8
    :goto_1
    if-eqz v8, :cond_1

    .line 676
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 677
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusableInTouchMode(Z)V

    .line 678
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestFocus()Z

    goto/16 :goto_0

    .line 672
    .restart local v12    # "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_9
    instance-of v0, v12, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    move/from16 v17, v0

    if-eqz v17, :cond_8

    .line 673
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->dispatchTouchEvent(Landroid/view/MotionEvent;)V

    .line 1011
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public getActivityCreateTime()J
    .locals 2

    .prologue
    .line 1457
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mActivityCreateTime:J

    return-wide v0
.end method

.method public getAndroidContext()Landroid/content/Context;
    .locals 0

    .prologue
    .line 884
    return-object p0
.end method

.method public getDataManager()Lcom/sec/android/gallery3d/data/DataManager;
    .locals 1

    .prologue
    .line 879
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    return-object v0
.end method

.method public getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;
    .locals 1

    .prologue
    .line 1222
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    if-nez v0, :cond_0

    .line 1223
    new-instance v0, Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 1224
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    return-object v0
.end method

.method public getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;
    .locals 1

    .prologue
    .line 1113
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v0

    return-object v0
.end method

.method public getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1

    .prologue
    .line 1108
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method public getGalleryCoverMode()Lcom/sec/android/gallery3d/app/GalleryCoverMode;
    .locals 1

    .prologue
    .line 1366
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    return-object v0
.end method

.method public getGalleryId()I
    .locals 1

    .prologue
    .line 1133
    iget v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryId:I

    return v0
.end method

.method public getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;
    .locals 1

    .prologue
    .line 1128
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    return-object v0
.end method

.method public getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;
    .locals 1

    .prologue
    .line 1118
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v0

    return-object v0
.end method

.method public getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    .locals 1

    .prologue
    .line 1213
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    return-object v0
.end method

.method public getMultiWindowActivity()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1

    .prologue
    .line 1217
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->getMultiWindowActivity()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v0

    return-object v0
.end method

.method public getQuramAGIF()Lcom/quramsoft/agif/QuramAGIF;
    .locals 1

    .prologue
    .line 1322
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mAGIF:Lcom/quramsoft/agif/QuramAGIF;

    if-nez v0, :cond_0

    .line 1323
    invoke-static {}, Lcom/quramsoft/agif/QuramAGIF;->create()Lcom/quramsoft/agif/QuramAGIF;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mAGIF:Lcom/quramsoft/agif/QuramAGIF;

    .line 1325
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mAGIF:Lcom/quramsoft/agif/QuramAGIF;

    return-object v0
.end method

.method public getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;
    .locals 1

    .prologue
    .line 889
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    return-object v0
.end method

.method public hidePreDisplayScreen()V
    .locals 1

    .prologue
    .line 1329
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPreDisplay:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    if-eqz v0, :cond_0

    .line 1330
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPreDisplay:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->hideScreenNail()V

    .line 1332
    :cond_0
    return-void
.end method

.method public isContactsChanged()Z
    .locals 1

    .prologue
    .line 1155
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsContactsChanged:Z

    return v0
.end method

.method public isCoverCloseMode()Z
    .locals 1

    .prologue
    .line 1447
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1448
    const/4 v0, 0x1

    .line 1449
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromCamera()Z
    .locals 1

    .prologue
    .line 1435
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFromCamera:Z

    return v0
.end method

.method public isFromMyFiles()Z
    .locals 1

    .prologue
    .line 1443
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFromMyFiles:Z

    return v0
.end method

.method public isInTouchMode()Z
    .locals 1

    .prologue
    .line 1123
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsInTouchMode:Z

    return v0
.end method

.method public isPreDisplayScreenNailVisible()Z
    .locals 1

    .prologue
    .line 1335
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPreDisplay:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    if-eqz v0, :cond_0

    .line 1336
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPreDisplay:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->isScreenNailVisible()Z

    move-result v0

    .line 1338
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1464
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1465
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isVideoStarted(Z)V

    .line 1466
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 1467
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->destroy()V

    .line 1469
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1470
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mVersionCheckDialog:Landroid/app/Dialog;

    if-ne p1, v0, :cond_0

    .line 553
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mVersionCheckDialog:Landroid/app/Dialog;

    .line 555
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v6, 0x1

    .line 725
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mUpdateBackscreenInPauseState:Z

    if-eqz v3, :cond_1

    .line 726
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->onResume()V

    .line 727
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mUpdateBackscreenInPauseState:Z

    .line 728
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 729
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->disableFinishingAtSecureLock()V

    .line 731
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v3, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 733
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 735
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 736
    .local v2, "tutorialType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-eqz v3, :cond_2

    .line 737
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v2

    .line 740
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 742
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 743
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_3

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v2, v3, :cond_3

    .line 744
    const v3, 0x7f0203b3

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setIcon(I)V

    .line 746
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setArbitarySizeForSCamera()V

    .line 749
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_4
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-eqz v3, :cond_6

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v2, v3, :cond_5

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v2, v3, :cond_5

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v2, v3, :cond_5

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v2, v3, :cond_6

    .line 753
    :cond_5
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/GalleryMotion;->setMotionTutorialView()V

    .line 756
    :cond_6
    iget v3, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHardKeyboardHidden:I

    iget v4, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-eq v3, v4, :cond_7

    .line 757
    iget v3, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v3, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHardKeyboardHidden:I

    .line 758
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->removeSelectionPopup()V

    .line 761
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 762
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 763
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->updatePickerModeWindow(Landroid/view/WindowManager$LayoutParams;)V

    .line 765
    .end local v1    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_8
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v9, 0x0

    const/16 v4, 0x400

    .line 230
    const-string v1, "VerificationLog"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    const-string v1, "Gallery_Performance"

    const-string v2, "Gallery onCreate Start"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    const-wide/16 v10, 0x0

    .line 234
    .local v10, "pTime":J
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME_ENABLE:Z

    if-eqz v1, :cond_0

    .line 235
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .end local v10    # "pTime":J
    sput-wide v10, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME:J

    .line 237
    .restart local v10    # "pTime":J
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mActivityCreateTime:J

    .line 238
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCoverMode(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->convertFromTranslucent()V

    .line 241
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPreDisplay:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    .line 242
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onCreate(Landroid/os/Bundle;)V

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    .line 246
    invoke-static {p0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 247
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    invoke-virtual {v1, p0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->startup(Landroid/app/Activity;)V

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    .line 250
    .local v8, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v8, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v8, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 252
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 253
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 255
    invoke-direct {p0, v8}, Lcom/sec/android/gallery3d/app/GalleryActivity;->updatePickerModeWindow(Landroid/view/WindowManager$LayoutParams;)V

    .line 262
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 264
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 266
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-eqz v1, :cond_3

    .line 267
    new-instance v1, Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    .line 270
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 271
    const v1, 0x7f11002d

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setTheme(I)V

    .line 277
    :goto_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 278
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setRequestedOrientation(I)V

    .line 280
    :cond_4
    const v1, 0x7f030084

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setContentView(I)V

    .line 281
    const v1, 0x7f0f0175

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mMainLayout:Landroid/widget/RelativeLayout;

    .line 283
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 284
    invoke-virtual {p0, v12, v12}, Lcom/sec/android/gallery3d/app/GalleryActivity;->convertToTranslucent(Landroid/app/Activity$TranslucentConversionListener;Landroid/app/ActivityOptions;)Z

    .line 285
    invoke-virtual {p0, v13}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setFinishOnTouchOutside(Z)V

    .line 288
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 289
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 290
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 292
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setOnDragListener()V

    .line 293
    new-instance v1, Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    .line 295
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 296
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->initActionBarForSCamera(Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;)V

    .line 298
    :cond_6
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLaunchTranslucent:Z

    if-eqz v1, :cond_7

    .line 299
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->hideRootView()V

    .line 301
    :cond_7
    new-instance v1, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    .line 302
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPreDisplay:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    if-eqz v1, :cond_8

    .line 303
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPreDisplay:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->checkApplyScreenNail()Z

    .line 305
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->setAppIntent(Landroid/content/Intent;)V

    .line 306
    new-instance v1, Lcom/sec/android/gallery3d/util/FeatureManager;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/util/FeatureManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFeatureManager:Lcom/sec/android/gallery3d/util/FeatureManager;

    .line 308
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFestival:Z

    if-eqz v1, :cond_9

    .line 309
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFestival:Z

    iget v3, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFestivalDataCount:I

    iget-wide v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFestivalStartTime:J

    iget-wide v6, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFestivalEndTime:J

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setImplFestivalData(ZIJJ)V

    .line 312
    :cond_9
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->startView(Landroid/os/Bundle;)V

    .line 314
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    invoke-static {p0, v1}, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->registerKnoxModeReceiver(Landroid/content/Context;Lcom/sec/samsung/gallery/core/GalleryFacade;)V

    .line 316
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->registerMediaScannerReceiver()V

    .line 317
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->registerFaceScanReceiver()V

    .line 318
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->registerBaiduCloudDragDropReceiver()V

    .line 319
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->registerSecretModeReceiver()V

    .line 321
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirButton:Z

    if-eqz v1, :cond_a

    invoke-static {}, Lcom/samsung/android/sdk/look/SlookImpl;->getVersionCode()I

    move-result v1

    if-lt v1, v13, :cond_a

    .line 322
    new-instance v0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, v2, v12}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 323
    .local v0, "abv":Lcom/sec/samsung/gallery/view/common/AirButtonViewer;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setAirButtonViewer(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)V

    .line 324
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->getAirButtonImpl()Lcom/samsung/android/airbutton/AirButtonImpl;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setAirButtonImpl(Lcom/samsung/android/airbutton/AirButtonImpl;)V

    .line 327
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v9}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setShowAirButton(Z)V

    .line 328
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAirButtonImpl()Lcom/samsung/android/airbutton/AirButtonImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/airbutton/AirButtonImpl;->unlinkWithParentView()V

    .line 331
    .end local v0    # "abv":Lcom/sec/samsung/gallery/view/common/AirButtonViewer;
    :cond_a
    iget v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryId:I

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setGalleryID(I)V

    .line 333
    sget-object v1, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->CREATE:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    .line 334
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME_ENABLE:Z

    if-eqz v1, :cond_e

    .line 335
    const-string v1, "Gallery_Performance"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Gallery onCreate End "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v10

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    :goto_2
    return-void

    .line 257
    :cond_b
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    goto/16 :goto_0

    .line 272
    :cond_c
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 273
    const v1, 0x7f11002e

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setTheme(I)V

    goto/16 :goto_1

    .line 275
    :cond_d
    const v1, 0x7f11002c

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setTheme(I)V

    goto/16 :goto_1

    .line 337
    :cond_e
    const-string v1, "Gallery_Performance"

    const-string v2, "Gallery onCreate End "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 814
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mUpdateBackscreenInPauseState:Z

    if-eqz v2, :cond_0

    .line 815
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 816
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 817
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 818
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mUpdateBackscreenInPauseState:Z

    .line 821
    :cond_0
    const-string v2, "Gallery_Performance"

    const-string v3, "Gallery onDestroy Start"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 823
    .local v1, "glRootView":Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onDestroy()V

    .line 824
    sget-object v2, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->DESTROY:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/GalleryActivity;->postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    .line 826
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 828
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->destroy()V

    .line 829
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onDestroy()V

    .line 830
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->clearStatus()V

    .line 831
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->releaseInstance()V

    .line 832
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->unregisterMediaScannerReceiver()V

    .line 833
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->unregisterSecretModeReceiver()V

    .line 834
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    invoke-static {p0, v2}, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->unregisterKnoxModeReceiver(Landroid/content/Context;Lcom/sec/samsung/gallery/core/GalleryFacade;)V

    .line 836
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v2, :cond_3

    .line 837
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mSimilarPersonsReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 838
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mSimilarPersonsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/app/GalleryActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 840
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFaceScannerReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_2

    .line 841
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFaceScannerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/app/GalleryActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 843
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mSimilarPersonsReceiver:Landroid/content/BroadcastReceiver;

    .line 844
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFaceScannerReceiver:Landroid/content/BroadcastReceiver;

    .line 846
    :cond_3
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaiduCloudDragDrop:Z

    if-eqz v2, :cond_5

    .line 847
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mBaiduCloudDragDropFinishReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_4

    .line 848
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mBaiduCloudDragDropFinishReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/app/GalleryActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 850
    :cond_4
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mBaiduCloudDragDropFinishReceiver:Landroid/content/BroadcastReceiver;

    .line 853
    :cond_5
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    if-eqz v2, :cond_6

    .line 854
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->dismissDialog()V

    .line 857
    :cond_6
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 858
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->hideBargeInNotification()V

    .line 860
    :cond_7
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onDestroy()V

    .line 861
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "DESTROY"

    invoke-virtual {v2, v3, p0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 863
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 864
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_8

    const-string v2, "android.intent.action.PICK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 867
    :cond_8
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->stopPresentation(Landroid/content/Context;)V

    .line 869
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 871
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 874
    const-string v2, "Gallery_Performance"

    const-string v3, "Gallery onDestroy End"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    return-void

    .line 871
    .end local v0    # "action":Ljava/lang/String;
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v2
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 13
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v12, 0x15

    const/16 v11, 0x13

    const/16 v10, 0x14

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 894
    if-eq p1, v10, :cond_0

    if-eq p1, v11, :cond_0

    if-eq p1, v12, :cond_0

    const/16 v7, 0x16

    if-ne p1, v7, :cond_3

    .line 896
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    .line 897
    .local v6, "viewStateProxy":Lcom/sec/android/gallery3d/app/StateManager;
    if-eqz v6, :cond_3

    .line 898
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v5

    .line 899
    .local v5, "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    if-eqz v5, :cond_1

    instance-of v7, v5, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v7, :cond_1

    move-object v7, v5

    .line 900
    check-cast v7, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v2, v7, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .line 901
    .local v2, "moreInfo":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 902
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    .line 1005
    .end local v2    # "moreInfo":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;
    .end local v5    # "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    .end local v6    # "viewStateProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :goto_0
    return v7

    .line 906
    .restart local v5    # "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    .restart local v6    # "viewStateProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :cond_1
    if-eqz v5, :cond_3

    instance-of v7, v5, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    if-eqz v7, :cond_3

    move-object v3, v5

    .line 907
    check-cast v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .line 908
    .local v3, "searchViewState":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isFilterShow()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isLastRowKeyboardfocus()Z

    move-result v7

    if-eqz v7, :cond_2

    if-eq p1, v10, :cond_3

    .line 909
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto :goto_0

    .line 915
    .end local v3    # "searchViewState":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .end local v5    # "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    .end local v6    # "viewStateProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :cond_3
    const/4 v7, 0x4

    if-ne p1, v7, :cond_5

    .line 916
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v7

    if-nez v7, :cond_4

    .line 917
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->isBackPressed:Z

    .line 918
    :cond_4
    sget-object v7, Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onKeyDown isBackPressed = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->isBackPressed:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto :goto_0

    .line 921
    :cond_5
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v7, :cond_6

    .line 922
    sget-object v7, Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onKeyDown mGLRootView == null, key = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v8

    .line 923
    goto :goto_0

    .line 927
    :cond_6
    const/16 v7, 0xa8

    if-eq p1, v7, :cond_7

    const/16 v7, 0xa9

    if-ne p1, v7, :cond_8

    .line 928
    :cond_7
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v7

    if-eqz v7, :cond_b

    move v7, v8

    .line 929
    goto :goto_0

    .line 931
    :cond_8
    const/16 v7, 0x1b

    if-ne p1, v7, :cond_a

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMEnabled()Z

    move-result v7

    if-eqz v7, :cond_a

    iget-boolean v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFromCamera:Z

    if-eqz v7, :cond_a

    .line 933
    iget-boolean v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsSingleAlbumForPick:Z

    if-nez v7, :cond_9

    .line 934
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v8, "START_CAMERA"

    invoke-virtual {v7, v8, p0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_9
    move v7, v9

    .line 936
    goto/16 :goto_0

    .line 937
    :cond_a
    const/16 v7, 0x119

    if-ne p1, v7, :cond_b

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v7

    if-eqz v7, :cond_b

    move v7, v9

    .line 938
    goto/16 :goto_0

    .line 941
    :cond_b
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasFocus()Z

    move-result v7

    if-eqz v7, :cond_14

    .line 942
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 944
    .local v0, "code":I
    const v7, 0x7f0f020c

    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/app/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 945
    .local v4, "selectionBarCheckBox":Landroid/widget/CheckBox;
    if-ne v0, v11, :cond_d

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v7

    if-eqz v7, :cond_d

    if-eqz v4, :cond_d

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v7

    if-nez v7, :cond_d

    invoke-virtual {v4}, Landroid/widget/CheckBox;->hasFocus()Z

    move-result v7

    if-nez v7, :cond_d

    .line 947
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 948
    invoke-virtual {v4}, Landroid/widget/CheckBox;->requestFocus()Z

    .line 950
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getRootLayer()Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v1

    .line 951
    .local v1, "layer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    if-eqz v1, :cond_c

    .line 952
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchGenericMotionCancel()V

    :cond_c
    move v7, v9

    .line 953
    goto/16 :goto_0

    .line 956
    .end local v1    # "layer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    :cond_d
    if-eq v0, v10, :cond_e

    if-eq v0, v11, :cond_e

    if-eq v0, v12, :cond_e

    const/16 v7, 0x16

    if-ne v0, v7, :cond_13

    .line 958
    :cond_e
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v7

    instance-of v7, v7, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    if-eqz v7, :cond_f

    .line 959
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 962
    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    if-eqz v7, :cond_10

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_10

    .line 963
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 964
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->requestFocus()Z

    .line 966
    :cond_10
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getRootLayer()Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v1

    .line 967
    .restart local v1    # "layer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    if-eqz v1, :cond_11

    .line 968
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchGenericMotionCancel()V

    .line 995
    .end local v0    # "code":I
    .end local v1    # "layer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    .end local v4    # "selectionBarCheckBox":Landroid/widget/CheckBox;
    :cond_11
    :goto_1
    const/16 v7, 0x71

    if-eq p1, v7, :cond_12

    const/16 v7, 0x72

    if-ne p1, v7, :cond_18

    .line 996
    :cond_12
    sget-object v7, Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;

    const-string v8, "onKeyDown KEYCODE_CTRL_LEFT || KEYCODE_CTRL_RIGHT"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto/16 :goto_0

    .line 969
    .restart local v0    # "code":I
    .restart local v4    # "selectionBarCheckBox":Landroid/widget/CheckBox;
    :cond_13
    const/16 v7, 0x3d

    if-ne v0, v7, :cond_11

    .line 970
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getRootLayer()Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v1

    .line 971
    .restart local v1    # "layer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    if-eqz v1, :cond_11

    .line 972
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchGenericMotionCancel()V

    goto :goto_1

    .line 974
    .end local v0    # "code":I
    .end local v1    # "layer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    .end local v4    # "selectionBarCheckBox":Landroid/widget/CheckBox;
    :cond_14
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isSlideShow()Z

    move-result v7

    if-nez v7, :cond_11

    .line 975
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v7

    if-eq v7, v10, :cond_15

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v7

    if-eqz v7, :cond_11

    const/16 v7, 0x118

    if-eq p1, v7, :cond_15

    const/16 v7, 0x117

    if-eq p1, v7, :cond_15

    const/16 v7, 0x119

    if-ne p1, v7, :cond_11

    .line 979
    :cond_15
    const v7, 0x7f0f020c

    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/app/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 980
    .restart local v4    # "selectionBarCheckBox":Landroid/widget/CheckBox;
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isDrawerVisible()Z

    move-result v7

    if-eqz v7, :cond_16

    move v7, v8

    .line 982
    goto/16 :goto_0

    .line 983
    :cond_16
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v7

    if-eqz v7, :cond_17

    if-eqz v4, :cond_17

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v7

    if-nez v7, :cond_17

    invoke-virtual {v4}, Landroid/widget/CheckBox;->hasFocus()Z

    move-result v7

    if-nez v7, :cond_17

    .line 985
    invoke-virtual {v4}, Landroid/widget/CheckBox;->requestFocus()Z

    goto :goto_1

    .line 988
    :cond_17
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7, v9}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 989
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestFocus()Z

    .line 990
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto/16 :goto_0

    .line 1000
    .end local v4    # "selectionBarCheckBox":Landroid/widget/CheckBox;
    :cond_18
    const/16 v7, 0x54

    if-ne p1, v7, :cond_19

    .line 1001
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v7, :cond_19

    .line 1002
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    const-class v8, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 1005
    :cond_19
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto/16 :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1016
    const/4 v2, 0x4

    if-ne p1, v2, :cond_7

    .line 1017
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->isBackPressed:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1018
    :cond_0
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKeyUp isBackPressed = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->isBackPressed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", key = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1047
    :goto_0
    return v0

    .line 1022
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v2, :cond_2

    .line 1023
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKeyUp mGLRootView == null, key = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1026
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isDrawerVisible()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1027
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->closeDrawerMenu()V

    move v0, v1

    .line 1028
    goto :goto_0

    .line 1030
    :cond_3
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1031
    :cond_4
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKeyUp event.isLongPress() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", event.isCanceled() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1034
    :cond_5
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverOpen()Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1035
    goto :goto_0

    .line 1037
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->onBackPressed()V

    .line 1038
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->isBackPressed:Z

    move v0, v1

    .line 1039
    goto :goto_0

    .line 1040
    :cond_7
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa8

    if-eq p1, v0, :cond_8

    const/16 v0, 0xa9

    if-eq p1, v0, :cond_8

    const/16 v0, 0x118

    if-eq p1, v0, :cond_8

    const/16 v0, 0x117

    if-ne p1, v0, :cond_9

    .line 1042
    :cond_8
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 1043
    :cond_9
    const/16 v0, 0x114

    if-eq p1, v0, :cond_a

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x71

    if-eq v0, v1, :cond_a

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x72

    if-ne v0, v1, :cond_b

    .line 1045
    :cond_a
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 1047
    :cond_b
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 582
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 584
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isValidDataUri(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 585
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 586
    .local v0, "startIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 587
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 588
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 589
    const/high16 v1, 0x10000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 591
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->startActivity(Landroid/content/Intent;)V

    .line 599
    .end local v0    # "startIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 592
    :cond_1
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 595
    const v1, 0x7f0e0057

    invoke-static {p0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 500
    const-string v2, "Gallery_Performance"

    const-string v3, "Gallery onPause Start"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsActive:Z

    .line 502
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mUpdateBackscreenInPauseState:Z

    .line 503
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPausedByHomePress:Z

    if-eqz v2, :cond_0

    .line 505
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->closeCameraLens(Landroid/content/Context;)V

    .line 506
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPausedByHomePress:Z

    .line 509
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 510
    .local v1, "glRootView":Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAirButtonViewer()Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    move-result-object v0

    .line 511
    .local v0, "av":Lcom/sec/samsung/gallery/view/common/AirButtonViewer;
    if-eqz v0, :cond_1

    .line 512
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->dismissProgressDialog()V

    .line 514
    :cond_1
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUMS:Z

    if-eqz v2, :cond_2

    .line 515
    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setStatusBarManager(I)V

    .line 518
    :cond_2
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onPause()V

    .line 519
    sget-object v2, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->PAUSE:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/GalleryActivity;->postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    .line 520
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onPause()V

    .line 522
    invoke-static {}, Lcom/sec/android/gallery3d/util/LocationUtils;->getInstance()Lcom/sec/android/gallery3d/util/LocationUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/util/LocationUtils;->onDestroy()V

    .line 524
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->unregisterEventViewObserver()V

    .line 526
    const-string v2, "Gallery_Performance"

    const-string v3, "Gallery onPause End"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    return-void
.end method

.method protected onResume()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 416
    const-string v1, "VerificationLog"

    const-string v2, "onResume"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    const-string v1, "Gallery_Performance"

    const-string v2, "Gallery onResume Start"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    const-wide/16 v8, 0x0

    .line 419
    .local v8, "pTime":J
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME_ENABLE:Z

    if-eqz v1, :cond_0

    .line 420
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 423
    :cond_0
    iput-boolean v11, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsActive:Z

    .line 424
    iput-boolean v12, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mUpdateBackscreenInPauseState:Z

    .line 425
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v11}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 426
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 427
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onResume()V

    .line 495
    :goto_0
    return-void

    .line 431
    :cond_1
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFestival:Z

    if-eqz v1, :cond_2

    .line 432
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFestival:Z

    iget v3, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFestivalDataCount:I

    iget-wide v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFestivalStartTime:J

    iget-wide v6, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFestivalEndTime:J

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setImplFestivalData(ZIJJ)V

    .line 435
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->resume()V

    .line 437
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getDecoderInterface()Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->initDecoder(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 439
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getCacheInterface()Lcom/sec/samsung/gallery/decoder/CacheInterface;

    .line 441
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onResume()V

    .line 443
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onResume()V

    .line 444
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->updateMultiWindowSize(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 446
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_3

    .line 447
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onResume()V

    .line 448
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getDecoderInterface()Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setDecoderInterface(Lcom/sec/samsung/gallery/decoder/DecoderInterface;)V

    .line 452
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_4

    .line 453
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAirButtonViewer()Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    move-result-object v0

    .line 454
    .local v0, "av":Lcom/sec/samsung/gallery/view/common/AirButtonViewer;
    if-eqz v0, :cond_4

    .line 455
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->dismissProgressDialog()V

    .line 458
    .end local v0    # "av":Lcom/sec/samsung/gallery/view/common/AirButtonViewer;
    :cond_4
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUMS:Z

    if-eqz v1, :cond_5

    .line 459
    const/high16 v1, 0x10000

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setStatusBarManager(I)V

    .line 462
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHardKeyboardHidden:I

    .line 464
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLowMemoryCheckDialog:Z

    if-eqz v1, :cond_7

    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->checkLowMemoryAndFinishActivity()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 465
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;

    const-string v2, "onResume : checkLowMemoryAndFinishActivity() == true, return"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 446
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v2, :cond_6

    .line 447
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onResume()V

    .line 448
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getDecoderInterface()Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setDecoderInterface(Lcom/sec/samsung/gallery/decoder/DecoderInterface;)V

    :cond_6
    throw v1

    .line 469
    :cond_7
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloudMandatory:Z

    if-eqz v1, :cond_8

    .line 470
    new-instance v10, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v10}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    .line 471
    .local v10, "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-nez v1, :cond_8

    iget-boolean v1, v10, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mInit:Z

    if-eqz v1, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->existsAccount(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 472
    sput-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    .line 475
    .end local v10    # "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    :cond_8
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v1, :cond_9

    .line 476
    new-instance v10, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v10}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    .line 477
    .restart local v10    # "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->initTCloud(Landroid/content/Context;)V

    .line 479
    .end local v10    # "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    :cond_9
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPaused:Z

    if-eqz v1, :cond_a

    .line 480
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1, v11}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 481
    iput-boolean v12, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPaused:Z

    .line 483
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->registerEventViewObserver()V

    .line 485
    sget-object v1, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->RESUME:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    .line 487
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME_ENABLE:Z

    if-eqz v1, :cond_b

    .line 488
    const-string v1, "Gallery_Performance"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Gallery onResume End "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v8

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    :goto_1
    const-string v1, "VerificationLog"

    const-string v2, "Executed"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 490
    :cond_b
    const-string v1, "Gallery_Performance"

    const-string v2, "Gallery onResume End "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 615
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->saveState(Landroid/os/Bundle;)V

    .line 616
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 1344
    const/4 v0, 0x0

    return v0
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 717
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onStart()V

    .line 718
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 719
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 721
    :cond_0
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 702
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onUserLeaveHint()V

    .line 703
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;

    const-string v1, "Home Button is Pressed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPausedByHomePress:Z

    .line 705
    return-void
.end method

.method protected onViewPause()V
    .locals 4

    .prologue
    .line 541
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onViewPause()V

    .line 542
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->releaseInstance()V

    .line 543
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mPaused:Z

    .line 545
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 548
    :cond_1
    return-void
.end method

.method protected onViewResume()V
    .locals 1

    .prologue
    .line 533
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMiniModeServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 534
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->enableFinishingAtSecureLock()V

    .line 536
    :cond_0
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onViewResume()V

    .line 537
    return-void
.end method

.method public registerBaiduCloudDragDropReceiver()V
    .locals 2

    .prologue
    .line 362
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaiduCloudDragDrop:Z

    if-eqz v1, :cond_0

    .line 363
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 364
    .local v0, "dragDropFinishFilter":Landroid/content/IntentFilter;
    const-string v1, "com.baidu.xcloud.netdisk.ACTION_FINISH_BRODCAST"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 365
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mBaiduCloudDragDropFinishReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 367
    .end local v0    # "dragDropFinishFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public registerEventViewObserver()V
    .locals 2

    .prologue
    .line 343
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mSDCardRemovedReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getSDCardRemovedIntentFilter()Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 347
    :cond_0
    return-void
.end method

.method public registerFaceRecommendationObserver(Landroid/net/Uri;Z)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "showDialog"    # Z

    .prologue
    .line 1138
    const-string v0, "reload"

    const-string v1, "auto recommend start"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mAutoRecommendTime:J

    .line 1140
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryActivity$5;

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity$5;-><init>(Lcom/sec/android/gallery3d/app/GalleryActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->nFaceRecommendationObserver:Landroid/database/ContentObserver;

    .line 1150
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->nFaceRecommendationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1151
    return-void
.end method

.method public registerFaceScanReceiver()V
    .locals 3

    .prologue
    .line 370
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v2, :cond_0

    .line 371
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 372
    .local v1, "similarPersonFilter":Landroid/content/IntentFilter;
    const-string v2, "com.android.media.FACE_GET_SIMILAR_PERSONS_FINISHED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 373
    const-string v2, "content"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 374
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mSimilarPersonsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 376
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 377
    .local v0, "scannerFilter":Landroid/content/IntentFilter;
    const-string v2, "com.android.media.FACE_SCANNER_STARTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 378
    const-string v2, "com.android.media.FACE_SCANNER_PROGRESS"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 379
    const-string v2, "com.android.media.FACE_SCANNER_FINISHED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 380
    const-string v2, "com.android.media.FACE_SCANNER_STOPPED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 381
    const-string v2, "content"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 382
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFaceScannerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 384
    .end local v0    # "scannerFilter":Landroid/content/IntentFilter;
    .end local v1    # "similarPersonFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public registerMediaScannerReceiver()V
    .locals 3

    .prologue
    .line 1159
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$MediaScanner;->REGISTER:Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$MediaScanner;

    aput-object v2, v0, v1

    .line 1162
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "MEDIASCANNER_RECEIVER"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1163
    return-void
.end method

.method public registerSecretModeReceiver()V
    .locals 3

    .prologue
    .line 1166
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$CmdType;->REGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$CmdType;

    aput-object v2, v0, v1

    .line 1169
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SECRET_MODE_RECEIVER"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1170
    return-void
.end method

.method public setFestivalData(ZIJJ)V
    .locals 1
    .param p1, "mode"    # Z
    .param p2, "count"    # I
    .param p3, "start"    # J
    .param p5, "end"    # J

    .prologue
    .line 1426
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFestival:Z

    .line 1427
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1428
    iput p2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFestivalDataCount:I

    .line 1429
    iput-wide p3, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFestivalStartTime:J

    .line 1430
    iput-wide p5, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mFestivalEndTime:J

    .line 1432
    :cond_0
    return-void
.end method

.method public setFromCamera(Z)V
    .locals 0
    .param p1, "isFromCamera"    # Z

    .prologue
    .line 1439
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFromCamera:Z

    .line 1440
    return-void
.end method

.method public setFromMyFiles(Z)V
    .locals 0
    .param p1, "isFromMyFiles"    # Z

    .prologue
    .line 1453
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFromMyFiles:Z

    .line 1454
    return-void
.end method

.method public startDefaultPage()V
    .locals 0

    .prologue
    .line 411
    return-void
.end method

.method public unregisterEventViewObserver()V
    .locals 4

    .prologue
    .line 350
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v1, :cond_0

    .line 352
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mSDCardRemovedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 353
    :catch_0
    move-exception v0

    .line 354
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unregisterEventViewObserver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 355
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 356
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
