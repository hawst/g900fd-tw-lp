.class Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;
.super Ljava/lang/Object;
.source "FaceIndicatorView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->addContactPopupView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelCurIndex()V
    .locals 2

    .prologue
    .line 431
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    const/4 v1, -0x1

    # setter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1102(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;I)I

    .line 432
    return-void
.end method

.method public onClicked(I)V
    .locals 5
    .param p1, "which"    # I

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 390
    const/16 v0, 0x10

    if-ne p1, v0, :cond_1

    .line 391
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$700(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContentPane()Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/GLView;->getComponent(I)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->resume()V

    .line 393
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->show(Z)V

    .line 394
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # setter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$802(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Z)Z

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 399
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$900(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    # invokes: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->updateFaceData(Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    invoke-static {v0, v1, v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 400
    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v0

    if-eq v0, v4, :cond_3

    .line 401
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # setter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z
    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1302(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Z)Z

    .line 403
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 404
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # setter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$802(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Z)Z

    .line 405
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->show(Z)V

    .line 407
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->show()V

    goto :goto_0

    .line 409
    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 410
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 422
    :cond_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 423
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # setter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$802(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Z)Z

    .line 424
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->show(Z)V

    .line 426
    :cond_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    goto/16 :goto_0

    .line 411
    :cond_6
    const/16 v0, 0x20

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v0

    if-eq v0, v4, :cond_4

    .line 412
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 415
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # setter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z
    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$802(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Z)Z

    .line 416
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->show(Z)V

    .line 418
    :cond_7
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->show()V

    goto/16 :goto_0
.end method
