.class public Lcom/sec/android/gallery3d/util/MotionDetector;
.super Ljava/lang/Object;
.source "MotionDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;,
        Lcom/sec/android/gallery3d/util/MotionDetector$MotionListener;
    }
.end annotation


# static fields
.field public static final MOTION_MOVE_DEFAULT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "MotionDetector"


# instance fields
.field public final MOTION_MOVE_MAX:I

.field public final MOTION_MOVE_MIN:I

.field public mBaseTilt:I

.field public mBaseX:F

.field public mBaseY:F

.field private mContext:Landroid/content/Context;

.field private mGallerymotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

.field private mListener:Lcom/samsung/android/motion/MRListener;

.field public mMotionCallCount:J

.field public mMotionDegreeX:I

.field public mMotionDegreeY:I

.field private final mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

.field mState:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

.field public mZoomoutStart:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/GalleryMotion;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "galleryMotion"    # Lcom/sec/android/gallery3d/app/GalleryMotion;

    .prologue
    const-wide/16 v2, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->MOTION_MOVE_MAX:I

    .line 30
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->MOTION_MOVE_MIN:I

    .line 38
    sget-object v0, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_NONE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mState:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    .line 39
    iput-wide v2, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mZoomoutStart:J

    .line 40
    iput-wide v2, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mMotionCallCount:J

    .line 43
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mContext:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mGallerymotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    .line 45
    sget-object v0, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_NONE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mState:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    .line 46
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mContext:Landroid/content/Context;

    const-string v1, "motion_recognition"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/motion/MotionRecognitionManager;

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    .line 48
    iput-wide v2, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mZoomoutStart:J

    .line 49
    return-void
.end method


# virtual methods
.method public checkState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)Z
    .locals 1
    .param p1, "state"    # Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mState:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getState()Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mState:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    return-object v0
.end method

.method public registerMotionListener(Lcom/samsung/android/motion/MRListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/motion/MRListener;

    .prologue
    .line 61
    if-nez p1, :cond_0

    .line 68
    :goto_0
    return-void

    .line 64
    :cond_0
    const-string v0, "MotionDetector"

    const-string v1, "registerMotionListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mListener:Lcom/samsung/android/motion/MRListener;

    .line 66
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mListener:Lcom/samsung/android/motion/MRListener;

    const/16 v2, 0x30

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/motion/MotionRecognitionManager;->registerListenerEvent(Lcom/samsung/android/motion/MRListener;I)V

    goto :goto_0
.end method

.method public setDefaultMotionCenterPosition(FF)V
    .locals 4
    .param p1, "touchPosX"    # F
    .param p2, "touchPosY"    # F

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 52
    iput p1, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mBaseX:F

    .line 53
    iput p2, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mBaseY:F

    .line 54
    iput v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mMotionDegreeX:I

    .line 55
    iput v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mMotionDegreeY:I

    .line 56
    iput-wide v2, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mZoomoutStart:J

    .line 57
    iput-wide v2, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mMotionCallCount:J

    .line 58
    return-void
.end method

.method public setState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)V
    .locals 2
    .param p1, "state"    # Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mState:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    .line 85
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-nez v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mState:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    sget-object v1, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_MOVE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mGallerymotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mGallerymotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionEngineUse()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mGallerymotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionPanningUse()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mGallerymotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionPeekUse()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->deviceRotationLock(Landroid/content/Context;)V

    .line 95
    :cond_1
    :goto_0
    return-void

    .line 89
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionPeekSeries(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 90
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->deviceRotationLock(Landroid/content/Context;I)V

    goto :goto_0

    .line 92
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->deviceRotationUnLock(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public unregisterMotionListener()V
    .locals 2

    .prologue
    .line 76
    const-string v0, "MotionDetector"

    const-string/jumbo v1, "unregisterMotionListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mListener:Lcom/samsung/android/motion/MRListener;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mListener:Lcom/samsung/android/motion/MRListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/motion/MotionRecognitionManager;->unregisterListener(Lcom/samsung/android/motion/MRListener;)V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mListener:Lcom/samsung/android/motion/MRListener;

    .line 81
    :cond_0
    return-void
.end method

.method public updateFocusPoint(FF)V
    .locals 0
    .param p1, "mTouchPosX"    # F
    .param p2, "mTouchPosY"    # F

    .prologue
    .line 71
    iput p1, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mBaseX:F

    .line 72
    iput p2, p0, Lcom/sec/android/gallery3d/util/MotionDetector;->mBaseY:F

    .line 73
    return-void
.end method
