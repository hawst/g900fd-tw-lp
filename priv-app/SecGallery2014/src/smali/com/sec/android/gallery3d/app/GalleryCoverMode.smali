.class public Lcom/sec/android/gallery3d/app/GalleryCoverMode;
.super Ljava/lang/Object;
.source "GalleryCoverMode.java"


# static fields
.field public static final ACTION_COVER_OPEN:Ljava/lang/String; = "com.samsung.cover.OPEN"

.field public static final ACTION_START_COVER_GALLERY:Ljava/lang/String; = "com.sec.android.app.camera.ACTION_START_COVER_CAMERA"

.field public static final ACTION_STOP_COVER_GALLERY:Ljava/lang/String; = "com.sec.android.app.camera.ACTION_STOP_COVER_CAMERA"

.field public static final CAPTURE_COUNT_IN_LOCKSCREEN:Ljava/lang/String; = "CaptureCountInSecuremode"

.field private static final COVER_SCREEN_OFF_TIMEOUT:J = 0x1770L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCover:Lcom/samsung/android/sdk/cover/Scover;

.field private mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mCoverStateListenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mFinishByBtn:Z

.field private mIsStarted:Z

.field private mMainLayout:Landroid/view/ViewGroup;

.field private mSettingTimeOut:J

.field private mWindwowManager:Landroid/view/IWindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverStateListenerList:Ljava/util/ArrayList;

    .line 259
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;-><init>(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 58
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    .line 59
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    const v0, 0x7f0f0175

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mMainLayout:Landroid/view/ViewGroup;

    .line 60
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mWindwowManager:Landroid/view/IWindowManager;

    .line 61
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->setSettingTimeOut()V

    .line 62
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->setCoverMode()V

    .line 63
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/app/GalleryCoverMode;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryCoverMode;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mFinishByBtn:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/GalleryCoverMode;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryCoverMode;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->setActivityResult(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/GalleryCoverMode;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryCoverMode;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->requestSystemKeyEvent(IZ)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->getSettingTimeOut()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/app/GalleryCoverMode;JZ)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryCoverMode;
    .param p1, "x1"    # J
    .param p3, "x2"    # Z

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->setScreenOffTime(JZ)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->setLayoutParams()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverStateListenerList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private finishedByBtn()Z
    .locals 1

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mFinishByBtn:Z

    return v0
.end method

.method private getSettingTimeOut()J
    .locals 2

    .prologue
    .line 208
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mSettingTimeOut:J

    return-wide v0
.end method

.method private requestSystemKeyEvent(IZ)V
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "request"    # Z

    .prologue
    .line 193
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mWindwowManager:Landroid/view/IWindowManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-interface {v2, p1, v1, p2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :goto_0
    return-void

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private setActivityResult(I)V
    .locals 2
    .param p1, "result"    # I

    .prologue
    .line 182
    const/4 v0, 0x0

    .line 183
    .local v0, "finalResult":I
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverOpen()Z

    move-result v1

    if-nez v1, :cond_0

    .line 185
    const/4 v0, -0x1

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setResult(I)V

    .line 189
    return-void
.end method

.method private setCoverMode()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 66
    new-instance v1, Lcom/samsung/android/sdk/cover/Scover;

    invoke-direct {v1}, Lcom/samsung/android/sdk/cover/Scover;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCover:Lcom/samsung/android/sdk/cover/Scover;

    .line 69
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCover:Lcom/samsung/android/sdk/cover/Scover;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/Scover;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 76
    :goto_0
    new-instance v1, Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 90
    :goto_1
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 72
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 73
    .local v0, "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/SsdkUnsupportedException;->printStackTrace()V

    goto :goto_0

    .line 81
    .end local v0    # "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    :cond_0
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "start CoverMode"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mFinishByBtn:Z

    .line 83
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.camera.ACTION_START_COVER_CAMERA"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 85
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->setWindowLaoutParams()V

    .line 87
    const/4 v1, 0x3

    invoke-direct {p0, v1, v4}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->requestSystemKeyEvent(IZ)V

    .line 88
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->setLayoutParams()V

    .line 89
    const-wide/16 v2, 0x1770

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->setScreenOffTime(JZ)V

    goto :goto_1
.end method

.method private setLayoutParams()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 146
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mMainLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 147
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverOpen()Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0247

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 150
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0248

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 152
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 157
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mMainLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 158
    return-void

    .line 154
    :cond_0
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 155
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    goto :goto_0
.end method

.method private setScreenOffTime(JZ)V
    .locals 5
    .param p1, "timeOut"    # J
    .param p3, "isDimOff"    # Z

    .prologue
    .line 212
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 213
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iput-wide p1, v0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    .line 214
    if-eqz p3, :cond_0

    .line 215
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Landroid/view/WindowManager$LayoutParams;->screenDimDuration:J

    .line 219
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 220
    return-void

    .line 217
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Landroid/view/WindowManager$LayoutParams;->screenDimDuration:J

    goto :goto_0
.end method

.method private setSettingTimeOut()V
    .locals 4

    .prologue
    .line 201
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_off_timeout"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mSettingTimeOut:J
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :goto_0
    return-void

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private setWindowLaoutParams()V
    .locals 3

    .prologue
    .line 223
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 224
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 225
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 227
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 228
    return-void
.end method

.method private showCoverCloseBtn()V
    .locals 5

    .prologue
    .line 161
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030043

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 162
    .local v1, "closeBtnView":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mMainLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 163
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0f00be

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 165
    .local v0, "closeBtn":Landroid/widget/ImageButton;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 166
    new-instance v2, Lcom/sec/android/gallery3d/app/GalleryCoverMode$1;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode$1;-><init>(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    return-void
.end method

.method private switchToNormal()V
    .locals 2

    .prologue
    .line 236
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 237
    .local v0, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    if-eqz v0, :cond_0

    .line 238
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/ActivityState;->onCoverModeChanged()V

    .line 240
    :cond_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 4

    .prologue
    .line 114
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->TAG:Ljava/lang/String;

    const-string v2, "Cover Gallery: Destroy"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 131
    :goto_0
    return-void

    .line 121
    :cond_0
    const/4 v0, 0x0

    .line 123
    .local v0, "result":I
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->finishedByBtn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    const/4 v0, -0x1

    .line 127
    :cond_1
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->requestSystemKeyEvent(IZ)V

    .line 128
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.camera.ACTION_STOP_COVER_CAMERA"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 129
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->setActivityResult(I)V

    .line 130
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public getCapturedCount()I
    .locals 5

    .prologue
    .line 243
    const/4 v0, 0x0

    .line 245
    .local v0, "caputreCount":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CaptureCountInSecuremode"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 249
    :goto_0
    return v0

    .line 246
    :catch_0
    move-exception v1

    .line 247
    .local v1, "e":Ljava/lang/NullPointerException;
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getCurrentColor()I
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    if-nez v0, :cond_1

    .line 254
    :cond_0
    const/4 v0, 0x0

    .line 256
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getColor()I

    move-result v0

    goto :goto_0
.end method

.method public isCoverMode()Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCoverMode(Landroid/app/Activity;)Z

    move-result v0

    return v0
.end method

.method public isCoverOpen()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    if-nez v0, :cond_1

    .line 139
    :cond_0
    const/4 v0, 0x1

    .line 142
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    goto :goto_0
.end method

.method public isVideoStarted(Z)V
    .locals 0
    .param p1, "started"    # Z

    .prologue
    .line 298
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mIsStarted:Z

    .line 299
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->TAG:Ljava/lang/String;

    const-string v1, "Cover Gallery: Pause"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mIsStarted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isIncomingCall(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .prologue
    .line 286
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverStateListenerList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 287
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverStateListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 288
    monitor-exit v1

    .line 289
    return-void

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 93
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->TAG:Ljava/lang/String;

    const-string v1, "Cover Gallery: Resume"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    :goto_0
    return-void

    .line 99
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->showCoverCloseBtn()V

    goto :goto_0
.end method

.method public unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .prologue
    .line 292
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverStateListenerList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 293
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverStateListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 294
    monitor-exit v1

    .line 295
    return-void

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
