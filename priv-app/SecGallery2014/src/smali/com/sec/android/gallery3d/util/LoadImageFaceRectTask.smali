.class public Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;
.super Ljava/lang/Thread;
.source "LoadImageFaceRectTask.java"


# static fields
.field public static mFaceRectMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private static mLoadImageFaceRectTask:Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;


# instance fields
.field final mContext:Landroid/content/Context;

.field private mIsDirty:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mFaceRectMap:Ljava/util/HashMap;

    .line 20
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mLoadImageFaceRectTask:Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    const-string v0, "load-face-rect"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mIsDirty:Z

    .line 50
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mContext:Landroid/content/Context;

    .line 51
    return-void
.end method

.method public static getAllFacesRect(I)Landroid/graphics/RectF;
    .locals 3
    .param p0, "imageId"    # I

    .prologue
    .line 34
    sget-object v1, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mFaceRectMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 35
    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mFaceRectMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    monitor-exit v1

    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private processImageFaceRectDB()V
    .locals 24

    .prologue
    .line 65
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 66
    .local v12, "faceRectMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/RectF;>;"
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    .line 67
    .local v21, "rectListMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Landroid/graphics/Rect;>;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 68
    .local v2, "resolver":Landroid/content/ContentResolver;
    const/4 v10, 0x0

    .line 70
    .local v10, "faceCursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "pos_left"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "pos_top"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "pos_right"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "pos_bottom"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "image_id"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 75
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 77
    :cond_0
    new-instance v19, Landroid/graphics/Rect;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Rect;-><init>()V

    .line 78
    .local v19, "rect":Landroid/graphics/Rect;
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 79
    const/4 v3, 0x1

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 80
    const/4 v3, 0x2

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/Rect;->right:I

    .line 81
    const/4 v3, 0x3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 82
    const/4 v3, 0x4

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 83
    .local v16, "imageId":I
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/ArrayList;

    .line 84
    .local v20, "rectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    if-nez v20, :cond_6

    .line 85
    new-instance v20, Ljava/util/ArrayList;

    .end local v20    # "rectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 86
    .restart local v20    # "rectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 94
    .end local v16    # "imageId":I
    .end local v19    # "rect":Landroid/graphics/Rect;
    .end local v20    # "rectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    :cond_1
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 96
    invoke-virtual/range {v21 .. v21}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 97
    .local v9, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/ArrayList<Landroid/graphics/Rect;>;>;"
    const/16 v17, 0x0

    .local v17, "left":I
    const/16 v23, 0x0

    .local v23, "top":I
    const/16 v22, 0x0

    .local v22, "right":I
    const/4 v8, 0x0

    .line 98
    .local v8, "bottom":I
    new-instance v19, Landroid/graphics/RectF;

    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, -0x40800000    # -1.0f

    const/high16 v5, -0x40800000    # -1.0f

    const/high16 v6, -0x40800000    # -1.0f

    move-object/from16 v0, v19

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 101
    .local v19, "rect":Landroid/graphics/RectF;
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/util/ArrayList;

    .line 102
    .local v18, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 103
    .local v15, "id":I
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Rect;

    .line 104
    .local v11, "faceRect":Landroid/graphics/Rect;
    iget v0, v11, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    .line 105
    iget v0, v11, Landroid/graphics/Rect;->top:I

    move/from16 v23, v0

    .line 106
    iget v0, v11, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    .line 107
    iget v8, v11, Landroid/graphics/Rect;->bottom:I

    .line 108
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/RectF;->left:F

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_7

    .line 109
    move/from16 v0, v17

    int-to-float v3, v0

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/RectF;->left:F

    .line 114
    :cond_3
    :goto_3
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/RectF;->top:F

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_8

    .line 115
    move/from16 v0, v23

    int-to-float v3, v0

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/RectF;->top:F

    .line 119
    :cond_4
    :goto_4
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/RectF;->right:F

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_9

    .line 120
    move/from16 v0, v22

    int-to-float v3, v0

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/RectF;->right:F

    .line 124
    :cond_5
    :goto_5
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_a

    .line 125
    int-to-float v3, v8

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_2

    .line 89
    .end local v8    # "bottom":I
    .end local v9    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/ArrayList<Landroid/graphics/Rect;>;>;"
    .end local v11    # "faceRect":Landroid/graphics/Rect;
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "id":I
    .end local v17    # "left":I
    .end local v18    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    .end local v22    # "right":I
    .end local v23    # "top":I
    .restart local v16    # "imageId":I
    .local v19, "rect":Landroid/graphics/Rect;
    .restart local v20    # "rectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    :cond_6
    :try_start_1
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 94
    .end local v16    # "imageId":I
    .end local v19    # "rect":Landroid/graphics/Rect;
    .end local v20    # "rectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    :catchall_0
    move-exception v3

    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3

    .line 110
    .restart local v8    # "bottom":I
    .restart local v9    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/ArrayList<Landroid/graphics/Rect;>;>;"
    .restart local v11    # "faceRect":Landroid/graphics/Rect;
    .restart local v14    # "i$":Ljava/util/Iterator;
    .restart local v15    # "id":I
    .restart local v17    # "left":I
    .restart local v18    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    .local v19, "rect":Landroid/graphics/RectF;
    .restart local v22    # "right":I
    .restart local v23    # "top":I
    :cond_7
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/RectF;->left:F

    move/from16 v0, v17

    int-to-float v4, v0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3

    .line 111
    move/from16 v0, v17

    int-to-float v3, v0

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/RectF;->left:F

    goto :goto_3

    .line 116
    :cond_8
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/RectF;->top:F

    move/from16 v0, v23

    int-to-float v4, v0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    .line 117
    move/from16 v0, v23

    int-to-float v3, v0

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/RectF;->top:F

    goto :goto_4

    .line 121
    :cond_9
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/RectF;->right:F

    move/from16 v0, v22

    int-to-float v4, v0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_5

    .line 122
    move/from16 v0, v22

    int-to-float v3, v0

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/RectF;->right:F

    goto :goto_5

    .line 126
    :cond_a
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    int-to-float v4, v8

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 127
    int-to-float v3, v8

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_2

    .line 130
    .end local v11    # "faceRect":Landroid/graphics/Rect;
    :cond_b
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/RectF;->top:F

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v15}, Lcom/sec/android/gallery3d/data/LocalImage;->getFaceCount(Landroid/content/ContentResolver;I)I

    move-result v3

    if-ltz v3, :cond_c

    .line 132
    const/high16 v3, -0x40000000    # -2.0f

    move-object/from16 v0, v19

    iput v3, v0, Landroid/graphics/RectF;->top:F

    .line 134
    :cond_c
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v12, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 137
    .end local v8    # "bottom":I
    .end local v9    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/ArrayList<Landroid/graphics/Rect;>;>;"
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "id":I
    .end local v17    # "left":I
    .end local v18    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    .end local v19    # "rect":Landroid/graphics/RectF;
    .end local v22    # "right":I
    .end local v23    # "top":I
    :cond_d
    sget-object v4, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mFaceRectMap:Ljava/util/HashMap;

    monitor-enter v4

    .line 138
    :try_start_2
    sget-object v3, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mFaceRectMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 139
    sget-object v3, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mFaceRectMap:Ljava/util/HashMap;

    invoke-virtual {v3, v12}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 140
    monitor-exit v4

    .line 141
    return-void

    .line 140
    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3
.end method

.method public static start(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-nez v0, :cond_0

    .line 31
    :goto_0
    return-void

    .line 25
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mLoadImageFaceRectTask:Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mLoadImageFaceRectTask:Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->isAlive()Z

    move-result v0

    if-nez v0, :cond_2

    .line 26
    :cond_1
    new-instance v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mLoadImageFaceRectTask:Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;

    .line 27
    sget-object v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mLoadImageFaceRectTask:Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->start()V

    goto :goto_0

    .line 29
    :cond_2
    sget-object v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mLoadImageFaceRectTask:Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->notifyDirty()V

    goto :goto_0
.end method

.method public static updateAllFacesRectInCache(ILandroid/graphics/RectF;)V
    .locals 3
    .param p0, "id"    # I
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 40
    sget-object v1, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mFaceRectMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 41
    if-nez p1, :cond_0

    .line 42
    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mFaceRectMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    :goto_0
    monitor-exit v1

    .line 47
    return-void

    .line 44
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mFaceRectMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public notifyDirty()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mIsDirty:Z

    .line 55
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 58
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mIsDirty:Z

    if-eqz v0, :cond_0

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->mIsDirty:Z

    .line 60
    invoke-direct {p0}, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->processImageFaceRectDB()V

    goto :goto_0

    .line 62
    :cond_0
    return-void
.end method
