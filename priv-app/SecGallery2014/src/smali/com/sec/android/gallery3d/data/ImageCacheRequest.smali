.class public abstract Lcom/sec/android/gallery3d/data/ImageCacheRequest;
.super Ljava/lang/Object;
.source "ImageCacheRequest.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ImageCacheRequest"


# instance fields
.field protected mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mNeedBitmap:Z

.field private mPath:Lcom/sec/android/gallery3d/data/Path;

.field private mTargetSize:I

.field private mTimeModified:J

.field private mType:I

.field private mUseCache:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JII)V
    .locals 1
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "timeModified"    # J
    .param p5, "type"    # I
    .param p6, "targetSize"    # I

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mUseCache:Z

    .line 56
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 57
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 58
    iput p5, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    .line 59
    iput p6, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTargetSize:I

    .line 60
    iput-wide p3, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTimeModified:J

    .line 61
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JIIZ)V
    .locals 1
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "timeModified"    # J
    .param p5, "type"    # I
    .param p6, "targetSize"    # I
    .param p7, "needBitmap"    # Z

    .prologue
    .line 65
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/gallery3d/data/ImageCacheRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JII)V

    .line 66
    iput-wide p3, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTimeModified:J

    .line 67
    iput-boolean p7, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mNeedBitmap:Z

    .line 68
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mPath:Lcom/sec/android/gallery3d/data/Path;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getObject()Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 70
    :cond_0
    return-void
.end method

.method private debugTag()Ljava/lang/String;
    .locals 4

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTimeModified:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const-string v0, "THUMB"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const-string v0, "MICROTHUMB"

    goto :goto_0

    :cond_1
    const-string v0, "?"

    goto :goto_0
.end method

.method private writeBitmapToByteBuffer(Landroid/graphics/Bitmap;)Ljava/nio/ByteBuffer;
    .locals 14
    .param p1, "srcBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 256
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/BitmapUtils;->compressToBytes(Landroid/graphics/Bitmap;)[B

    move-result-object v0

    .line 257
    .local v0, "bitmaparray":[B
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v11, :cond_1

    .line 258
    const/4 v10, 0x0

    .line 294
    :cond_0
    :goto_0
    return-object v10

    .line 259
    :cond_1
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v12, 0x10

    invoke-virtual {v11, v12, v13}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v5

    .line 260
    .local v5, "isSound":Z
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v11

    const-string v12, "gif"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    .line 261
    .local v3, "isGif":Z
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/32 v12, 0x80000

    invoke-virtual {v11, v12, v13}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v4

    .line 262
    .local v4, "isPhotoNote":Z
    const/4 v6, 0x0

    .line 265
    .local v6, "metadataarray":[B
    :try_start_0
    new-instance v9, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v9}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 266
    .local v9, "stream":Ljava/io/ByteArrayOutputStream;
    new-instance v8, Ljava/io/DataOutputStream;

    invoke-direct {v8, v9}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 268
    .local v8, "out":Ljava/io/DataOutputStream;
    invoke-virtual {v8, v5}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 269
    invoke-virtual {v8, v3}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 270
    invoke-virtual {v8, v4}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 272
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    .line 274
    invoke-virtual {v8}, Ljava/io/DataOutputStream;->close()V

    .line 275
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    .end local v8    # "out":Ljava/io/DataOutputStream;
    .end local v9    # "stream":Ljava/io/ByteArrayOutputStream;
    :goto_1
    array-length v1, v0

    .line 281
    .local v1, "bitmaplength":I
    const/4 v7, 0x0

    .line 282
    .local v7, "metadatalength":I
    if-eqz v6, :cond_2

    .line 283
    array-length v7, v6

    .line 285
    :cond_2
    const-string v11, "ImageCacheRequest"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "bitmaplength = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", metadatalength = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/data/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    const-string v11, "ImageCacheRequest"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "isSound = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", isGif = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", isPhotoNote = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/data/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    add-int v11, v1, v7

    invoke-static {v11}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v10

    .line 289
    .local v10, "target":Ljava/nio/ByteBuffer;
    invoke-virtual {v10, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 290
    if-eqz v6, :cond_0

    .line 291
    invoke-virtual {v10, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    goto/16 :goto_0

    .line 276
    .end local v1    # "bitmaplength":I
    .end local v7    # "metadatalength":I
    .end local v10    # "target":Ljava/nio/ByteBuffer;
    :catch_0
    move-exception v2

    .line 277
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public abstract onDecodeOriginal(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;I)Landroid/graphics/Bitmap;
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 20
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 129
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v3, :cond_3

    .line 130
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_3

    .line 131
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    .line 132
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->onDecodeOriginal(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;I)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 133
    .local v14, "bitmap":Landroid/graphics/Bitmap;
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 134
    const/4 v14, 0x0

    .line 238
    .end local v14    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object v14

    .line 136
    .restart local v14    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    if-nez v14, :cond_2

    .line 137
    const/4 v14, 0x0

    goto :goto_0

    .line 140
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTargetSize:I

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 142
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 143
    const/4 v14, 0x0

    goto :goto_0

    .line 149
    .end local v14    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v2

    .line 150
    .local v2, "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->get()Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    move-result-object v7

    .line 152
    .local v7, "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    const/16 v16, 0x0

    .line 153
    .local v16, "found":Z
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mUseCache:Z

    if-eqz v3, :cond_4

    .line 154
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mPath:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTimeModified:J

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/gallery3d/data/ImageCacheService;->getImageData(Lcom/sec/android/gallery3d/data/Path;JILcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z

    move-result v16

    .line 156
    :cond_4
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_5

    .line 157
    const/4 v14, 0x0

    .line 171
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    goto :goto_0

    .line 159
    :cond_5
    if-eqz v16, :cond_8

    .line 160
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mNeedBitmap:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v3, :cond_6

    .line 161
    const/4 v14, 0x0

    .line 171
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    goto :goto_0

    .line 162
    :cond_6
    :try_start_2
    new-instance v17, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 163
    .local v17, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v17

    iput-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 164
    iget-object v3, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    iget v4, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    iget v5, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v3, v4, v5, v1}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodeUsingPool(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;[BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 165
    .restart local v14    # "bitmap":Landroid/graphics/Bitmap;
    if-nez v14, :cond_7

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_7

    .line 166
    const-string v3, "ImageCacheRequest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "decode cached failed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->debugTag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 171
    :cond_7
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    goto/16 :goto_0

    .end local v14    # "bitmap":Landroid/graphics/Bitmap;
    .end local v17    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_8
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 174
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->onDecodeOriginal(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;I)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 175
    .restart local v14    # "bitmap":Landroid/graphics/Bitmap;
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 176
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 171
    .end local v14    # "bitmap":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    throw v3

    .line 178
    .restart local v14    # "bitmap":Landroid/graphics/Bitmap;
    :cond_9
    if-nez v14, :cond_a

    .line 179
    const-string v3, "ImageCacheRequest"

    const-string v4, "decode orig failed "

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 183
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSignatureFile(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v3

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_b

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_c

    .line 185
    :cond_b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTargetSize:I

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndFitToSize(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 212
    :goto_1
    if-nez v14, :cond_14

    .line 213
    const-string v3, "ImageCacheRequest"

    const-string v4, "bitmap is null!"

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 186
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v3

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_d

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_d

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_e

    .line 188
    :cond_d
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTargetSize:I

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    goto :goto_1

    .line 189
    :cond_e
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_13

    .line 190
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v3, v3, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v3, :cond_12

    .line 191
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v3, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalImage;->getFaceCount()I

    move-result v15

    .line 192
    .local v15, "faceCount":I
    if-nez v15, :cond_f

    .line 193
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTargetSize:I

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    goto :goto_1

    .line 194
    :cond_f
    if-lez v15, :cond_11

    .line 195
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v3, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalImage;->loadRectOfAllFaces()V

    .line 196
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v3, Lcom/sec/android/gallery3d/data/LocalImage;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces(Z)Landroid/graphics/RectF;

    move-result-object v18

    .line 197
    .local v18, "rect":Landroid/graphics/RectF;
    if-eqz v18, :cond_10

    .line 198
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTargetSize:I

    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-static {v14, v3, v0, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropFace(Landroid/graphics/Bitmap;ILandroid/graphics/RectF;Z)Landroid/graphics/Bitmap;

    move-result-object v14

    goto :goto_1

    .line 200
    :cond_10
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTargetSize:I

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    goto/16 :goto_1

    .line 203
    .end local v18    # "rect":Landroid/graphics/RectF;
    :cond_11
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTargetSize:I

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    goto/16 :goto_1

    .line 206
    .end local v15    # "faceCount":I
    :cond_12
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTargetSize:I

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    goto/16 :goto_1

    .line 209
    :cond_13
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTargetSize:I

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownBySideLength(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    goto/16 :goto_1

    .line 217
    :cond_14
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 218
    const-string v3, "ImageCacheRequest"

    const-string v4, "jc isCancelled!"

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 222
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v3, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    const-string v4, "png"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    const-string v4, "gif"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 223
    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSignatureFile(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 224
    const v3, -0x777778

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->setBGColor(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 230
    :cond_17
    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->writeBitmapToByteBuffer(Landroid/graphics/Bitmap;)Ljava/nio/ByteBuffer;

    move-result-object v19

    .line 231
    .local v19, "target":Ljava/nio/ByteBuffer;
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_19

    .line 232
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 226
    .end local v19    # "target":Ljava/nio/ByteBuffer;
    :cond_18
    const/high16 v3, -0x1000000

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->setBGColor(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    goto :goto_2

    .line 234
    .restart local v19    # "target":Ljava/nio/ByteBuffer;
    :cond_19
    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mUseCache:Z

    if-eqz v3, :cond_0

    .line 235
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mPath:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mTimeModified:J

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mType:I

    invoke-virtual/range {v19 .. v19}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v13

    move-object v8, v2

    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/gallery3d/data/ImageCacheService;->putImageData(Lcom/sec/android/gallery3d/data/Path;JI[B)V

    goto/16 :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected setUseCache(Z)V
    .locals 0
    .param p1, "useCache"    # Z

    .prologue
    .line 250
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->mUseCache:Z

    .line 251
    return-void
.end method
