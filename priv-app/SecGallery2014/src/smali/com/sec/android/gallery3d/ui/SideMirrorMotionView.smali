.class public Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "SideMirrorMotionView.java"


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mBg:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mIsShowing:Z

.field private mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

.field private final mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mIsShowing:Z

    .line 74
    new-instance v0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView$1;-><init>(Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 20
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 21
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0202e0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mBg:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    return-object v0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mIsShowing:Z

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "configuration"    # Landroid/content/res/Configuration;

    .prologue
    .line 112
    if-eqz p1, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 113
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0202e0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mBg:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 116
    :goto_0
    return-void

    .line 115
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0202e1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mBg:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 42
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    .line 34
    const/16 v0, 0xc8

    .line 35
    .local v0, "height":I
    invoke-static {p0}, Lcom/sec/android/gallery3d/ui/MeasureHelper;->getInstance(Lcom/sec/android/gallery3d/ui/GLView;)Lcom/sec/android/gallery3d/ui/MeasureHelper;

    move-result-object v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/ui/MeasureHelper;->setPreferredContentSize(II)Lcom/sec/android/gallery3d/ui/MeasureHelper;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/ui/MeasureHelper;->measure(II)V

    .line 38
    return-void
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 49
    const/4 v0, 0x1

    return v0
.end method

.method protected onVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->onVisibilityChanged(I)V

    .line 27
    if-nez p1, :cond_0

    .line 28
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserInteraction()V

    .line 30
    :cond_0
    return-void
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 3
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    const/4 v2, 0x0

    .line 64
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-interface {p1, v2, v2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->clipRect(IIII)Z

    .line 66
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 67
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 68
    return-void
.end method

.method public renderBackground(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    const/4 v2, 0x0

    .line 59
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mBg:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v5

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 60
    return-void
.end method

.method public setIsShowing(Z)V
    .locals 0
    .param p1, "isShowing"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mIsShowing:Z

    .line 105
    return-void
.end method

.method public setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 72
    return-void
.end method
