.class public final Lcom/sec/android/gallery3d/homesync/DMRViewController;
.super Ljava/lang/Object;
.source "DMRViewController.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DMRViewController"

.field static excutor:Ljava/util/concurrent/ExecutorService;

.field private static objDetailViewDMRViewControllerInstance:Lcom/sec/android/gallery3d/homesync/DMRViewController;


# instance fields
.field private ReceiverInstanceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/homesync/DMRReceiver;",
            ">;"
        }
    .end annotation
.end field

.field mViewControllerHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

.field private serverPort:I

.field serverSocket:Ljava/net/ServerSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->objDetailViewDMRViewControllerInstance:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    .line 50
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->excutor:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverPort:I

    .line 48
    iput-object v1, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->mViewControllerHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->ReceiverInstanceList:Ljava/util/ArrayList;

    .line 54
    iput-object v1, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverSocket:Ljava/net/ServerSocket;

    .line 58
    return-void
.end method

.method public static declared-synchronized GetInstance()Lcom/sec/android/gallery3d/homesync/DMRViewController;
    .locals 2

    .prologue
    .line 62
    const-class v1, Lcom/sec/android/gallery3d/homesync/DMRViewController;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_1

    .line 63
    sget-object v0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->objDetailViewDMRViewControllerInstance:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/sec/android/gallery3d/homesync/DMRViewController;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/homesync/DMRViewController;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->objDetailViewDMRViewControllerInstance:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    .line 67
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->objDetailViewDMRViewControllerInstance:Lcom/sec/android/gallery3d/homesync/DMRViewController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/homesync/DMRViewController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/homesync/DMRViewController;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverPort:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/homesync/DMRViewController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/homesync/DMRViewController;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverPort:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/homesync/DMRViewController;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/homesync/DMRViewController;
    .param p1, "x1"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->sendServerPort(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/homesync/DMRViewController;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/homesync/DMRViewController;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->ReceiverInstanceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static isDetailViewDMRViewControllerInstance()Z
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->objDetailViewDMRViewControllerInstance:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    if-nez v0, :cond_0

    .line 74
    const/4 v0, 0x0

    .line 76
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private sendServerPort(I)I
    .locals 3
    .param p1, "port"    # I

    .prologue
    .line 179
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 180
    const-string v0, "DMRViewController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "server port: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->mViewControllerHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->mViewControllerHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;->setZoomPort(I)I

    move-result v0

    .line 185
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public startViewControllerServer(Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;)V
    .locals 2
    .param p1, "handler"    # Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    .prologue
    .line 88
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 89
    iput-object p1, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->mViewControllerHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    .line 90
    sget-object v0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->excutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;-><init>(Lcom/sec/android/gallery3d/homesync/DMRViewController;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 147
    :cond_0
    return-void
.end method

.method public stopViewControllerServer()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 154
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v2, :cond_2

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v2, :cond_2

    .line 156
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverSocket:Ljava/net/ServerSocket;

    if-eqz v2, :cond_0

    .line 157
    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->close()V

    .line 159
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverSocket:Ljava/net/ServerSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->ReceiverInstanceList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 167
    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController;->ReceiverInstanceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/homesync/DMRReceiver;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->unRegisterControllerActionHandler()V

    .line 166
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 160
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 169
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "i":I
    :cond_1
    sput-object v3, Lcom/sec/android/gallery3d/homesync/DMRViewController;->objDetailViewDMRViewControllerInstance:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    .line 171
    .end local v1    # "i":I
    :cond_2
    return-void
.end method
