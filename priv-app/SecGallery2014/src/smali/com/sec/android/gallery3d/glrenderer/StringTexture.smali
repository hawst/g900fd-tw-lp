.class public Lcom/sec/android/gallery3d/glrenderer/StringTexture;
.super Lcom/sec/android/gallery3d/glrenderer/CanvasTexture;
.source "StringTexture.java"


# instance fields
.field private final mMetrics:Landroid/graphics/Paint$FontMetricsInt;

.field private final mPaint:Landroid/text/TextPaint;

.field private final mText:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Landroid/text/TextPaint;Landroid/graphics/Paint$FontMetricsInt;II)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "paint"    # Landroid/text/TextPaint;
    .param p3, "metrics"    # Landroid/graphics/Paint$FontMetricsInt;
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 46
    invoke-direct {p0, p4, p5}, Lcom/sec/android/gallery3d/glrenderer/CanvasTexture;-><init>(II)V

    .line 47
    iput-object p1, p0, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->mText:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->mPaint:Landroid/text/TextPaint;

    .line 49
    iput-object p3, p0, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->mMetrics:Landroid/graphics/Paint$FontMetricsInt;

    .line 50
    return-void
.end method

.method public static getCharacterDimension(Ljava/lang/String;FI)Landroid/graphics/Point;
    .locals 4
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "textSize"    # F
    .param p2, "color"    # I

    .prologue
    .line 205
    if-nez p0, :cond_0

    .line 206
    const/4 v1, 0x0

    .line 209
    :goto_0
    return-object v1

    .line 208
    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    .line 209
    .local v0, "sTexture":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0
.end method

.method public static getDefaultPaint(FI)Landroid/text/TextPaint;
    .locals 1
    .param p0, "textSize"    # F
    .param p1, "color"    # I

    .prologue
    .line 53
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getDefaultPaint(FIZ)Landroid/text/TextPaint;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultPaint(FIZ)Landroid/text/TextPaint;
    .locals 4
    .param p0, "textSize"    # F
    .param p1, "color"    # I
    .param p2, "showShadow"    # Z

    .prologue
    const/4 v3, 0x0

    .line 58
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    .line 59
    .local v0, "paint":Landroid/text/TextPaint;
    invoke-virtual {v0, p0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 60
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 61
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 62
    if-eqz p2, :cond_0

    .line 63
    const/high16 v1, 0x40000000    # 2.0f

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 64
    :cond_0
    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 1
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "textSize"    # F
    .param p2, "color"    # I

    .prologue
    .line 70
    invoke-static {p1, p2}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getDefaultPaint(FI)Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;Landroid/text/TextPaint;)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;FIFZ)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "textSize"    # F
    .param p2, "color"    # I
    .param p3, "lengthLimit"    # F
    .param p4, "isBold"    # Z

    .prologue
    const/4 v1, 0x1

    .line 76
    invoke-static {p1, p2, v1}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getDefaultPaint(FIZ)Landroid/text/TextPaint;

    move-result-object v0

    .line 77
    .local v0, "paint":Landroid/text/TextPaint;
    if-eqz p4, :cond_0

    .line 78
    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 80
    :cond_0
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 81
    const/4 v1, 0x0

    cmpl-float v1, p3, v1

    if-lez v1, :cond_1

    .line 82
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p0, v0, p3, v1}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    .line 85
    :cond_1
    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;Landroid/text/TextPaint;)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v1

    return-object v1
.end method

.method public static newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 5
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "textSize"    # F
    .param p2, "color"    # I
    .param p3, "lengthLimit"    # F
    .param p4, "isBold"    # Z
    .param p5, "TagBuddyType"    # I

    .prologue
    .line 216
    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getDefaultPaint(FIZ)Landroid/text/TextPaint;

    move-result-object v0

    .line 217
    .local v0, "paint":Landroid/text/TextPaint;
    const/4 v1, 0x1

    if-ne p5, v1, :cond_0

    .line 218
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 219
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 222
    :goto_0
    const v1, 0x3dcccccd    # 0.1f

    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v4, -0x5a000000

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 226
    :cond_0
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p0, v0, p3, v1}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    .line 228
    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;Landroid/text/TextPaint;)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v1

    return-object v1

    .line 221
    :cond_1
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRegularFont()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;FIFZZ)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "textSize"    # F
    .param p2, "color"    # I
    .param p3, "lengthLimit"    # F
    .param p4, "isBold"    # Z
    .param p5, "showShadow"    # Z

    .prologue
    .line 234
    invoke-static {p1, p2, p5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getDefaultPaint(FIZ)Landroid/text/TextPaint;

    move-result-object v0

    .line 235
    .local v0, "paint":Landroid/text/TextPaint;
    if-eqz p4, :cond_0

    .line 236
    const/4 v1, 0x1

    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 238
    :cond_0
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p0, v0, p3, v1}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    .line 240
    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;Landroid/text/TextPaint;)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v1

    return-object v1
.end method

.method public static newInstance(Ljava/lang/String;FIII)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 1
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "textSize"    # F
    .param p2, "color"    # I
    .param p3, "fontHeight"    # I
    .param p4, "style"    # I

    .prologue
    .line 190
    invoke-static {p1, p2}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getDefaultPaint(FI)Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {p0, v0, p3, p4}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;Landroid/text/TextPaint;II)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;FIZ)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 1
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "textSize"    # F
    .param p2, "color"    # I
    .param p3, "isBold"    # Z

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIZZ)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;FIZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 9
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "textSize"    # F
    .param p2, "color"    # I
    .param p3, "isBold"    # Z
    .param p4, "limitTextWidth"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 128
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 129
    .local v1, "paint":Landroid/text/TextPaint;
    invoke-virtual {v1, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 130
    invoke-virtual {v1, v5}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 131
    invoke-virtual {v1, p2}, Landroid/text/TextPaint;->setColor(I)V

    .line 132
    if-eqz p3, :cond_0

    .line 133
    invoke-static {v5}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 135
    :cond_0
    invoke-virtual {v1, p0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v4, v6

    .line 136
    .local v4, "textWidth":I
    const/16 v5, 0x14

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v5

    sub-int v2, p4, v5

    .line 137
    .local v2, "realLimitTextWidth":I
    if-le v4, v2, :cond_2

    .line 138
    const/4 v0, 0x1

    .line 139
    .local v0, "index":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 140
    .local v3, "strLength":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 141
    invoke-virtual {p0, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    if-le v5, v2, :cond_3

    .line 142
    add-int/lit8 v0, v0, -0x2

    .line 146
    :cond_1
    if-lez v0, :cond_2

    .line 147
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 150
    .end local v0    # "index":I
    .end local v3    # "strLength":I
    :cond_2
    invoke-static {p0, v1}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;Landroid/text/TextPaint;)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v5

    return-object v5

    .line 140
    .restart local v0    # "index":I
    .restart local v3    # "strLength":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;FIZZ)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 11
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "textSize"    # F
    .param p2, "color"    # I
    .param p3, "isBold"    # Z
    .param p4, "isFadeOut"    # Z

    .prologue
    const/4 v4, 0x2

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 111
    new-instance v8, Landroid/text/TextPaint;

    invoke-direct {v8}, Landroid/text/TextPaint;-><init>()V

    .line 112
    .local v8, "paint":Landroid/text/TextPaint;
    invoke-virtual {v8, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 113
    invoke-virtual {v8, v10}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 114
    invoke-virtual {v8, p2}, Landroid/text/TextPaint;->setColor(I)V

    .line 115
    if-eqz p4, :cond_0

    .line 116
    invoke-virtual {v8, p0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v9, v2

    .line 117
    .local v9, "textWidth":I
    new-instance v0, Landroid/graphics/LinearGradient;

    add-int/lit8 v2, v9, -0x1

    int-to-float v3, v2

    new-array v5, v4, [I

    const/4 v2, 0x0

    aput p2, v5, v2

    const v2, 0xffffff

    and-int/2addr v2, p2

    aput v2, v5, v10

    new-array v6, v4, [F

    fill-array-data v6, :array_0

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v4, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v8, v0}, Landroid/text/TextPaint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 121
    .end local v9    # "textWidth":I
    :cond_0
    if-eqz p3, :cond_1

    .line 122
    invoke-static {v10}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 124
    :cond_1
    invoke-static {p0, v8}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;Landroid/text/TextPaint;)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    return-object v0

    .line 117
    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3f7ae148    # 0.98f
    .end array-data
.end method

.method private static newInstance(Ljava/lang/String;Landroid/text/TextPaint;)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 6
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "paint"    # Landroid/text/TextPaint;

    .prologue
    .line 89
    invoke-virtual {p1}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v3

    .line 90
    .local v3, "metrics":Landroid/graphics/Paint$FontMetricsInt;
    invoke-virtual {p1, p0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v4, v0

    .line 91
    .local v4, "width":I
    iget v0, v3, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v1, v3, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int v5, v0, v1

    .line 93
    .local v5, "height":I
    if-gtz v4, :cond_0

    const/4 v4, 0x1

    .line 94
    :cond_0
    if-gtz v5, :cond_1

    const/4 v5, 0x1

    .line 95
    :cond_1
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;-><init>(Ljava/lang/String;Landroid/text/TextPaint;Landroid/graphics/Paint$FontMetricsInt;II)V

    return-object v0
.end method

.method private static newInstance(Ljava/lang/String;Landroid/text/TextPaint;I)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 6
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "paint"    # Landroid/text/TextPaint;
    .param p2, "textWidth"    # I

    .prologue
    .line 177
    invoke-virtual {p1}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v3

    .line 178
    .local v3, "metrics":Landroid/graphics/Paint$FontMetricsInt;
    move v4, p2

    .line 179
    .local v4, "width":I
    iget v0, v3, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v1, v3, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int v5, v0, v1

    .line 181
    .local v5, "height":I
    if-gtz v4, :cond_0

    const/4 v4, 0x1

    .line 182
    :cond_0
    if-gtz v5, :cond_1

    const/4 v5, 0x1

    .line 183
    :cond_1
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;-><init>(Ljava/lang/String;Landroid/text/TextPaint;Landroid/graphics/Paint$FontMetricsInt;II)V

    return-object v0
.end method

.method private static newInstance(Ljava/lang/String;Landroid/text/TextPaint;II)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 6
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "paint"    # Landroid/text/TextPaint;
    .param p2, "fontHeight"    # I
    .param p3, "style"    # I

    .prologue
    .line 194
    invoke-virtual {p1}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v3

    .line 195
    .local v3, "metrics":Landroid/graphics/Paint$FontMetricsInt;
    invoke-virtual {p1, p0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v4, v0

    .line 196
    .local v4, "width":I
    if-gtz v4, :cond_0

    const/4 v4, 0x1

    .line 197
    :cond_0
    const-string v0, "?"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    add-int/lit8 v4, v4, 0x2

    .line 199
    :cond_1
    invoke-static {p3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 200
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object v1, p0

    move-object v2, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;-><init>(Ljava/lang/String;Landroid/text/TextPaint;Landroid/graphics/Paint$FontMetricsInt;II)V

    return-object v0
.end method

.method public static newInstanceWithAutoFadeOut(Ljava/lang/String;FIZLjava/lang/String;I)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 13
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "textSize"    # F
    .param p2, "color"    # I
    .param p3, "isBold"    # Z
    .param p4, "baseChar"    # Ljava/lang/String;
    .param p5, "maxCharCount"    # I

    .prologue
    .line 156
    new-instance v10, Landroid/text/TextPaint;

    invoke-direct {v10}, Landroid/text/TextPaint;-><init>()V

    .line 157
    .local v10, "paint":Landroid/text/TextPaint;
    invoke-virtual {v10, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 158
    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 159
    invoke-virtual {v10, p2}, Landroid/text/TextPaint;->setColor(I)V

    .line 160
    if-eqz p3, :cond_0

    .line 161
    const/4 v2, 0x1

    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v10, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 164
    :cond_0
    invoke-virtual {v10, p0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v12, v2

    .line 165
    .local v12, "textWidth":I
    move-object/from16 v0, p4

    invoke-virtual {v10, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    move/from16 v0, p5

    int-to-double v4, v0

    mul-double/2addr v2, v4

    double-to-int v11, v2

    .line 166
    .local v11, "textMaxWidth":I
    if-le v12, v11, :cond_1

    .line 167
    move v12, v11

    .line 168
    new-instance v2, Landroid/graphics/LinearGradient;

    const/4 v3, 0x0

    const/4 v4, 0x0

    add-int/lit8 v5, v12, -0x1

    int-to-float v5, v5

    const/4 v6, 0x0

    const/4 v7, 0x2

    new-array v7, v7, [I

    const/4 v8, 0x0

    aput p2, v7, v8

    const/4 v8, 0x1

    const v9, 0xffffff

    and-int/2addr v9, p2

    aput v9, v7, v8

    const/4 v8, 0x2

    new-array v8, v8, [F

    fill-array-data v8, :array_0

    sget-object v9, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v2 .. v9}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v10, v2}, Landroid/text/TextPaint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 173
    :cond_1
    invoke-static {p0, v10, v12}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;Landroid/text/TextPaint;I)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v2

    return-object v2

    .line 168
    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3f7ae148    # 0.98f
    .end array-data
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "backing"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v2, 0x0

    .line 100
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->mMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->mText:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 102
    return-void
.end method
