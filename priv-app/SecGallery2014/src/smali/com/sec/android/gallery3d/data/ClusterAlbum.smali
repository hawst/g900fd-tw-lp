.class public Lcom/sec/android/gallery3d/data/ClusterAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "ClusterAlbum.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ClusterAlbum"


# instance fields
.field private mAlbumKey:Ljava/lang/String;

.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mContext:Landroid/content/Context;

.field private mCover:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field public mGroupId:I

.field private mIsDirty:Z

.field private mKind:I

.field private mLocalImagePaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaSetType:I

.field private mName:Ljava/lang/String;

.field private mNameID:Ljava/lang/String;

.field private mPathHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field public mPersonId:I

.field private mSupportShare:Z

.field private mUntaggedAlbum:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "clusterAlbumSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p4, "kind"    # I

    .prologue
    const/4 v2, 0x0

    .line 69
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mName:Ljava/lang/String;

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mSupportShare:Z

    .line 53
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mUntaggedAlbum:Z

    .line 57
    iput v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mKind:I

    .line 59
    iput v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPersonId:I

    .line 60
    iput v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mGroupId:I

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mNameID:Ljava/lang/String;

    .line 62
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mIsDirty:Z

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mLocalImagePaths:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPathHashMap:Ljava/util/HashMap;

    .line 328
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mMediaSetType:I

    .line 70
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 72
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mContext:Landroid/content/Context;

    .line 74
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 75
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 77
    :cond_0
    iput p4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mKind:I

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/ClusterAlbum;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/ClusterAlbum;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateSelectedFaces(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private checkMediaSetType()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 305
    iget v6, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mKind:I

    const/4 v7, 0x4

    if-eq v6, v7, :cond_0

    iget v6, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mKind:I

    const/4 v7, 0x5

    if-eq v6, v7, :cond_0

    iget v6, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mKind:I

    const/16 v7, 0xb

    if-eq v6, v7, :cond_0

    iget v6, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mKind:I

    const/16 v7, 0xc

    if-eq v6, v7, :cond_0

    iget v6, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mKind:I

    const/16 v7, 0xd

    if-ne v6, v7, :cond_1

    .line 310
    :cond_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 311
    .local v4, "size":I
    if-lez v4, :cond_1

    .line 312
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    add-int/lit8 v7, v4, -0x1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/Path;

    .line 313
    .local v0, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    .line 314
    .local v1, "pathS":Ljava/lang/String;
    const-string v6, "/"

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 315
    .local v5, "values":[Ljava/lang/String;
    const/16 v6, 0x9

    aget-object v6, v5, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 316
    .local v3, "recommendedId":I
    const/16 v6, 0xa

    aget-object v6, v5, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 318
    .local v2, "personId":I
    if-le v2, v8, :cond_2

    .line 319
    iput v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mMediaSetType:I

    .line 326
    .end local v0    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v1    # "pathS":Ljava/lang/String;
    .end local v2    # "personId":I
    .end local v3    # "recommendedId":I
    .end local v4    # "size":I
    .end local v5    # "values":[Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 320
    .restart local v0    # "path":Lcom/sec/android/gallery3d/data/Path;
    .restart local v1    # "pathS":Ljava/lang/String;
    .restart local v2    # "personId":I
    .restart local v3    # "recommendedId":I
    .restart local v4    # "size":I
    .restart local v5    # "values":[Ljava/lang/String;
    :cond_2
    if-le v3, v8, :cond_3

    if-eq v3, v2, :cond_3

    .line 321
    const/4 v6, 0x0

    iput v6, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mMediaSetType:I

    goto :goto_0

    .line 323
    :cond_3
    const/4 v6, -0x1

    iput v6, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mMediaSetType:I

    goto :goto_0
.end method

.method public static getMediaItemFromPath(Ljava/util/ArrayList;IILcom/sec/android/gallery3d/data/DataManager;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "start"    # I
    .param p2, "count"    # I
    .param p3, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;II",
            "Lcom/sec/android/gallery3d/data/DataManager;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    .local p0, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt p1, v6, :cond_1

    .line 140
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 157
    :cond_0
    return-object v4

    .line 142
    :cond_1
    add-int v6, p1, p2

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 143
    .local v2, "end":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {p0, p1, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 144
    .local v5, "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    sub-int v6, v2, p1

    new-array v0, v6, [Lcom/sec/android/gallery3d/data/MediaItem;

    .line 145
    .local v0, "buf":[Lcom/sec/android/gallery3d/data/MediaItem;
    new-instance v1, Lcom/sec/android/gallery3d/data/ClusterAlbum$1;

    invoke-direct {v1, v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum$1;-><init>([Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 152
    .local v1, "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    const/4 v6, 0x0

    invoke-virtual {p3, v5, v1, v6}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 153
    new-instance v4, Ljava/util/ArrayList;

    sub-int v6, v2, p1

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 154
    .local v4, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, v0

    if-ge v3, v6, :cond_0

    .line 155
    aget-object v6, v0, v3

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private updateSelectedFaces(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "joinedName"    # Ljava/lang/String;
    .param p4, "personId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 600
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p3, :cond_0

    .line 601
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mContext:Landroid/content/Context;

    invoke-static {v3, p4, p3}, Lcom/sec/samsung/gallery/access/face/PersonList;->updateJoinedName(Landroid/content/Context;ILjava/lang/String;)V

    .line 604
    :cond_0
    const/4 v2, 0x0

    .line 605
    .local v2, "recommendPath":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/Path;

    .line 606
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v3, v1, p4}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateOneFace(Landroid/content/Context;Lcom/sec/android/gallery3d/data/Path;I)V

    .line 608
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xd

    aget-object v3, v3, v4

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 609
    move-object v2, v1

    goto :goto_0

    .line 612
    .end local v1    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_2
    if-eqz v2, :cond_3

    .line 613
    invoke-virtual {p0, v2, p4}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->autoRecommend(Lcom/sec/android/gallery3d/data/Path;I)V

    .line 617
    :goto_1
    return-void

    .line 615
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateMediaSet()V

    goto :goto_1
.end method


# virtual methods
.method public addNewItem(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 727
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 728
    return-void
.end method

.method public autoRecommend(Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "personId"    # I

    .prologue
    .line 620
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v1

    .line 621
    .local v1, "values":[Ljava/lang/String;
    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 622
    .local v0, "id":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3, v0, p2}, Lcom/sec/samsung/gallery/access/face/FaceList;->recommendFaces(Landroid/content/Context;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;II)V

    .line 623
    return-void
.end method

.method public confirmFaces()V
    .locals 8

    .prologue
    .line 582
    const/4 v3, 0x0

    .line 583
    .local v3, "pathS":Ljava/lang/String;
    const/4 v6, 0x0

    .line 584
    .local v6, "values":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 585
    .local v1, "id":I
    const/4 v5, 0x0

    .line 586
    .local v5, "recommendedId":I
    const/4 v4, 0x0

    .line 588
    .local v4, "personId":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/Path;

    .line 589
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    .line 590
    const-string v7, "/"

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 591
    const/4 v7, 0x3

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 592
    const/16 v7, 0x9

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 593
    const/16 v7, 0xa

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 594
    if-eq v5, v4, :cond_0

    .line 595
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mContext:Landroid/content/Context;

    invoke-static {v7, v1, v5}, Lcom/sec/samsung/gallery/access/face/FaceList;->setPerson(Landroid/content/Context;II)V

    goto :goto_0

    .line 597
    .end local v2    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    return-void
.end method

.method public delete()V
    .locals 4

    .prologue
    .line 264
    new-instance v0, Lcom/sec/android/gallery3d/data/ClusterAlbum$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/ClusterAlbum$2;-><init>(Lcom/sec/android/gallery3d/data/ClusterAlbum;)V

    .line 273
    .local v0, "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 274
    return-void
.end method

.method protected enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)I
    .locals 2
    .param p1, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .param p2, "startIndex"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mCover:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mCover:Lcom/sec/android/gallery3d/data/MediaItem;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mAlbumKey:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalMediaItemFromPath(Ljava/util/ArrayList;IILcom/sec/android/gallery3d/data/DataManager;)Ljava/util/ArrayList;
    .locals 20
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;II",
            "Lcom/sec/android/gallery3d/data/DataManager;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 641
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p2

    if-lt v0, v2, :cond_0

    .line 642
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 723
    :goto_0
    return-object v17

    .line 644
    :cond_0
    add-int v2, p2, p3

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 645
    .local v12, "end":I
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 647
    .local v18, "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :try_start_0
    monitor-enter p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 648
    :try_start_1
    new-instance v19, Ljava/util/ArrayList;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v0, v1, v12}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 649
    .end local v18    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .local v19, "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object/from16 v18, v19

    .line 653
    .end local v19    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v18    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :goto_1
    sub-int v2, v12, p2

    new-array v8, v2, [Lcom/sec/android/gallery3d/data/MediaItem;

    .line 654
    .local v8, "buf":[Lcom/sec/android/gallery3d/data/MediaItem;
    new-instance v9, Lcom/sec/android/gallery3d/data/ClusterAlbum$4;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v8}, Lcom/sec/android/gallery3d/data/ClusterAlbum$4;-><init>(Lcom/sec/android/gallery3d/data/ClusterAlbum;[Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 662
    .local v9, "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    const/4 v2, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v9, v2}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 663
    new-instance v17, Ljava/util/ArrayList;

    sub-int v2, v12, p2

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 665
    .local v17, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/16 v16, 0x0

    .line 666
    .local v16, "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    const/4 v15, 0x0

    .line 668
    .local v15, "isFileModified":Z
    const/4 v14, 0x0

    .line 670
    .local v14, "isDelete":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getRefreshOperation()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    .line 671
    const/4 v14, 0x1

    .line 674
    :cond_1
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    array-length v2, v8

    if-ge v13, v2, :cond_9

    .line 675
    aget-object v2, v8, v13

    instance-of v2, v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v2, :cond_8

    .line 676
    aget-object v2, v8, v13

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v16

    .line 677
    if-nez v16, :cond_3

    .line 678
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    aget-object v2, v8, v13

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 674
    :cond_2
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 649
    .end local v8    # "buf":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v9    # "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .end local v13    # "i":I
    .end local v14    # "isDelete":Z
    .end local v15    # "isFileModified":Z
    .end local v16    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    .end local v17    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :catchall_0
    move-exception v2

    :goto_4
    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 650
    :catch_0
    move-exception v11

    .line 651
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 682
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v8    # "buf":[Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v9    # "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .restart local v13    # "i":I
    .restart local v14    # "isDelete":Z
    .restart local v15    # "isFileModified":Z
    .restart local v16    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    .restart local v17    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->setFacePaths(Ljava/util/ArrayList;)V

    .line 684
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->isPhotoPage()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 685
    const/4 v15, 0x0

    .line 687
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/data/LocalImage;->isFaceInfoDeleted()Z

    move-result v2

    if-nez v2, :cond_2

    .line 688
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mIsDirty:Z

    if-eqz v2, :cond_5

    if-nez v14, :cond_5

    .line 689
    const/4 v10, 0x0

    .line 690
    .local v10, "cursor":Landroid/database/Cursor;
    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    iget v4, v0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 691
    .local v3, "uri":Landroid/net/Uri;
    const-string v7, "datetaken DESC, _id DESC"

    .line 693
    .local v7, "strOrderClause":Ljava/lang/String;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 694
    if-eqz v10, :cond_4

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 695
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/data/LocalImage;->isFileModified(Landroid/database/Cursor;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v15

    .line 698
    :cond_4
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 702
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v7    # "strOrderClause":Ljava/lang/String;
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_5
    if-eqz v15, :cond_6

    .line 703
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/data/LocalImage;->deleteFaceInfo()V

    goto :goto_3

    .line 698
    .restart local v3    # "uri":Landroid/net/Uri;
    .restart local v7    # "strOrderClause":Ljava/lang/String;
    .restart local v10    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v2

    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 705
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v7    # "strOrderClause":Ljava/lang/String;
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_6
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 709
    :cond_7
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 712
    :cond_8
    aget-object v2, v8, v13

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 717
    :cond_9
    if-eqz v14, :cond_a

    .line 718
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->setRefreshOperation(I)V

    .line 721
    :cond_a
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mIsDirty:Z

    goto/16 :goto_0

    .line 649
    .end local v8    # "buf":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v9    # "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .end local v13    # "i":I
    .end local v14    # "isDelete":Z
    .end local v15    # "isFileModified":Z
    .end local v16    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    .end local v17    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v18    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v19    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :catchall_2
    move-exception v2

    move-object/from16 v18, v19

    .end local v19    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v18    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    goto/16 :goto_4
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 8
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {p0, v3, p1, p2, v4}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItemFromPathEx(Ljava/util/ArrayList;IILcom/sec/android/gallery3d/data/DataManager;)Ljava/util/ArrayList;

    move-result-object v2

    .line 124
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-nez v2, :cond_1

    const/4 v2, 0x0

    .line 132
    .end local v2    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    :goto_0
    return-object v2

    .line 125
    .restart local v2    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 126
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_2

    .line 127
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v4

    const-wide/16 v6, 0x4

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    .line 128
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mSupportShare:Z

    goto :goto_0
.end method

.method public getMediaItemCount()I
    .locals 2

    .prologue
    .line 114
    sget-object v1, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 115
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMediaItemEx(II)Ljava/util/ArrayList;
    .locals 2
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 636
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getLocalMediaItemFromPath(Ljava/util/ArrayList;IILcom/sec/android/gallery3d/data/DataManager;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMediaItemFromPathEx(Ljava/util/ArrayList;IILcom/sec/android/gallery3d/data/DataManager;)Ljava/util/ArrayList;
    .locals 20
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;II",
            "Lcom/sec/android/gallery3d/data/DataManager;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 337
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p2

    if-lt v0, v2, :cond_0

    .line 338
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 421
    :goto_0
    return-object v17

    .line 340
    :cond_0
    add-int v2, p2, p3

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 341
    .local v12, "end":I
    const/16 v18, 0x0

    .line 343
    .local v18, "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :try_start_0
    monitor-enter p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    :try_start_1
    new-instance v19, Ljava/util/ArrayList;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v0, v1, v12}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345
    .end local v18    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .local v19, "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object/from16 v18, v19

    .line 349
    .end local v19    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v18    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :goto_1
    if-nez v18, :cond_1

    .line 350
    const/16 v17, 0x0

    goto :goto_0

    .line 345
    :catchall_0
    move-exception v2

    :goto_2
    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 346
    :catch_0
    move-exception v11

    .line 347
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 351
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_1
    sub-int v2, v12, p2

    new-array v8, v2, [Lcom/sec/android/gallery3d/data/MediaItem;

    .line 352
    .local v8, "buf":[Lcom/sec/android/gallery3d/data/MediaItem;
    new-instance v9, Lcom/sec/android/gallery3d/data/ClusterAlbum$3;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v8}, Lcom/sec/android/gallery3d/data/ClusterAlbum$3;-><init>(Lcom/sec/android/gallery3d/data/ClusterAlbum;[Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 358
    .local v9, "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    const/4 v2, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v9, v2}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 359
    new-instance v17, Ljava/util/ArrayList;

    sub-int v2, v12, p2

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 361
    .local v17, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/16 v16, 0x0

    .line 362
    .local v16, "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    const/4 v15, 0x0

    .line 364
    .local v15, "isFileModified":Z
    const/4 v14, 0x0

    .line 366
    .local v14, "isDelete":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getRefreshOperation()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_2

    .line 367
    const/4 v14, 0x1

    .line 370
    :cond_2
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_3
    array-length v2, v8

    if-ge v13, v2, :cond_a

    .line 371
    aget-object v2, v8, v13

    instance-of v2, v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v2, :cond_9

    .line 372
    aget-object v2, v8, v13

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->setFacePaths(Ljava/util/ArrayList;)V

    .line 373
    aget-object v2, v8, v13

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mLocalImagePaths:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPathHashMap:Ljava/util/HashMap;

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->setPathMap(Ljava/util/ArrayList;Ljava/util/HashMap;)V

    .line 375
    aget-object v2, v8, v13

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v16

    .line 376
    if-nez v16, :cond_4

    .line 377
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    aget-object v2, v8, v13

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 370
    :cond_3
    :goto_4
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 381
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->setFacePaths(Ljava/util/ArrayList;)V

    .line 383
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->isPhotoPage()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 384
    const/4 v15, 0x0

    .line 386
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/data/LocalImage;->isFaceInfoDeleted()Z

    move-result v2

    if-nez v2, :cond_3

    .line 387
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mIsDirty:Z

    if-eqz v2, :cond_6

    if-nez v14, :cond_6

    .line 388
    const/4 v10, 0x0

    .line 389
    .local v10, "cursor":Landroid/database/Cursor;
    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    iget v4, v0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 390
    .local v3, "uri":Landroid/net/Uri;
    const-string v7, "datetaken DESC, _id DESC"

    .line 392
    .local v7, "strOrderClause":Ljava/lang/String;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 393
    if-eqz v10, :cond_5

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 394
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/data/LocalImage;->isFileModified(Landroid/database/Cursor;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v15

    .line 397
    :cond_5
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 401
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v7    # "strOrderClause":Ljava/lang/String;
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_6
    if-eqz v15, :cond_7

    .line 402
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/data/LocalImage;->deleteFaceInfo()V

    goto :goto_4

    .line 397
    .restart local v3    # "uri":Landroid/net/Uri;
    .restart local v7    # "strOrderClause":Ljava/lang/String;
    .restart local v10    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v2

    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 404
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v7    # "strOrderClause":Ljava/lang/String;
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_7
    aget-object v2, v8, v13

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 408
    :cond_8
    aget-object v2, v8, v13

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 411
    :cond_9
    aget-object v2, v8, v13

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 415
    :cond_a
    if-eqz v14, :cond_b

    .line 416
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->setRefreshOperation(I)V

    .line 419
    :cond_b
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mIsDirty:Z

    goto/16 :goto_0

    .line 345
    .end local v8    # "buf":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v9    # "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .end local v13    # "i":I
    .end local v14    # "isDelete":Z
    .end local v15    # "isFileModified":Z
    .end local v16    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    .end local v17    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v18    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v19    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :catchall_2
    move-exception v2

    move-object/from16 v18, v19

    .end local v19    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v18    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    goto/16 :goto_2
.end method

.method public getMediaItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMediaSetType()I
    .locals 1

    .prologue
    .line 330
    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mMediaSetType:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNameID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mNameID:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 10

    .prologue
    const-wide/high16 v8, 0x800000000000000L

    const-wide/high16 v6, 0x200000000000000L

    .line 241
    const-wide/16 v0, 0x405

    .line 242
    .local v0, "operation":J
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mSupportShare:Z

    if-nez v2, :cond_0

    const-wide/16 v2, -0x5

    and-long/2addr v0, v2

    .line 245
    :cond_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v2, :cond_1

    .line 246
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v2, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getClusterKind()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    .line 247
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0068

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 258
    .end local v0    # "operation":J
    :cond_1
    :goto_0
    return-wide v0

    .line 249
    .restart local v0    # "operation":J
    :cond_2
    iget v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mMediaSetType:I

    if-nez v2, :cond_3

    .line 250
    or-long v2, v0, v8

    or-long/2addr v2, v6

    const-wide/high16 v4, 0x400000000000000L

    or-long v0, v2, v4

    goto :goto_0

    .line 252
    :cond_3
    or-long v2, v0, v8

    or-long v0, v2, v6

    goto :goto_0

    .line 253
    :cond_4
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v2, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getClusterKind()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    goto :goto_0
.end method

.method public getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 2

    .prologue
    .line 788
    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mKind:I

    const/16 v1, 0x104

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mKind:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 790
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 795
    :goto_0
    return-object v0

    .line 791
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mKind:I

    const/16 v1, 0x100

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mKind:I

    if-nez v0, :cond_3

    .line 793
    :cond_2
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    goto :goto_0

    .line 795
    :cond_3
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    goto :goto_0
.end method

.method public getTotalMediaItemCount()I
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 170
    const/4 v0, 0x0

    .line 173
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public isAbleToDeleteItem(II)Z
    .locals 5
    .param p1, "start"    # I
    .param p2, "count"    # I

    .prologue
    .line 776
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {p0, v3, p1, p2, v4}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItemFromPathEx(Ljava/util/ArrayList;IILcom/sec/android/gallery3d/data/DataManager;)Ljava/util/ArrayList;

    move-result-object v2

    .line 777
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 778
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v3, v1, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-nez v3, :cond_0

    instance-of v3, v1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v3, :cond_0

    .line 779
    const/4 v3, 0x1

    .line 782
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public isDirty()Z
    .locals 4

    .prologue
    .line 447
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mArcDataVersion:J

    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getArcVersionNumber()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x1

    return v0
.end method

.method public isUntaggedAlbum()Z
    .locals 1

    .prologue
    .line 429
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mUntaggedAlbum:Z

    return v0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->notifyContentChanged()V

    .line 235
    return-void
.end method

.method public reload()J
    .locals 10

    .prologue
    const/4 v3, 0x1

    .line 179
    const/4 v0, 0x0

    .line 180
    .local v0, "bArcMode":Z
    const/4 v2, 0x0

    .line 182
    .local v2, "isPhotoPage":Z
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->isArcMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 183
    const/4 v0, 0x1

    .line 186
    :cond_0
    if-eqz v0, :cond_3

    .line 187
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->isPhotoPage()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 188
    const/4 v2, 0x1

    .line 189
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 190
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataVersion:J

    .line 229
    :goto_0
    return-wide v4

    .line 194
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->isTaskComplete()Z

    move-result v4

    if-nez v4, :cond_2

    .line 195
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataVersion:J

    goto :goto_0

    .line 197
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 198
    const-wide/16 v4, -0x1

    goto :goto_0

    .line 203
    :cond_3
    const/4 v1, 0x0

    .line 204
    .local v1, "isDirty":Z
    sget-object v4, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v4

    .line 205
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataVersion:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_4

    move v1, v3

    .line 206
    :goto_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    if-eqz v1, :cond_8

    .line 210
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v4, :cond_7

    .line 211
    if-eqz v0, :cond_7

    .line 212
    if-eqz v2, :cond_5

    .line 213
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mIsDirty:Z

    .line 214
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->nextVersionNumber()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataVersion:J

    goto :goto_0

    .line 205
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 206
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 217
    :cond_5
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mArcDataVersion:J

    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getArcVersionNumber()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_6

    .line 218
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->nextVersionNumber()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataVersion:J

    goto :goto_0

    .line 221
    :cond_6
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getArcVersionNumber()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mArcDataVersion:J

    .line 222
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataVersion:J

    goto :goto_0

    .line 227
    :cond_7
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->nextVersionNumber()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataVersion:J

    .line 229
    :cond_8
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataVersion:J

    goto :goto_0
.end method

.method public removeCategory(Ljava/lang/String;)V
    .locals 1
    .param p1, "categoryType"    # Ljava/lang/String;

    .prologue
    .line 565
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->deleteCategoryType(Landroid/content/Context;Ljava/lang/String;)Z

    .line 566
    return-void
.end method

.method public removeFaces()V
    .locals 6

    .prologue
    .line 569
    const/4 v3, 0x0

    .line 570
    .local v3, "pathS":Ljava/lang/String;
    const/4 v4, 0x0

    .line 571
    .local v4, "values":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 573
    .local v1, "id":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/Path;

    .line 574
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    .line 575
    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 576
    const/4 v5, 0x3

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 577
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mContext:Landroid/content/Context;

    invoke-static {v5, v1}, Lcom/sec/samsung/gallery/access/face/FaceList;->setFaceUnknown(Landroid/content/Context;I)V

    goto :goto_0

    .line 579
    .end local v2    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_0
    return-void
.end method

.method public setArcDataVersion()V
    .locals 2

    .prologue
    .line 443
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getArcVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mArcDataVersion:J

    .line 444
    return-void
.end method

.method public setCoverMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p1, "cover"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mCover:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 82
    return-void
.end method

.method public setKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 631
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mAlbumKey:Ljava/lang/String;

    .line 632
    return-void
.end method

.method setMediaItems(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-nez p1, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 95
    :goto_0
    return-void

    .line 94
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public setMediaItemsEx(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 284
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p1, :cond_1

    .line 285
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    .line 289
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mLocalImagePaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 290
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPathHashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 291
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v0, :cond_0

    .line 292
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->checkMediaSetType()V

    .line 294
    :cond_0
    return-void

    .line 287
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mName:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public setNameID(Ljava/lang/String;)V
    .locals 0
    .param p1, "nameId"    # Ljava/lang/String;

    .prologue
    .line 297
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mNameID:Ljava/lang/String;

    .line 298
    return-void
.end method

.method public setUntaggedAlbum(Z)V
    .locals 0
    .param p1, "untaggedAlbum"    # Z

    .prologue
    .line 425
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mUntaggedAlbum:Z

    .line 426
    return-void
.end method

.method public updateDataVersion()V
    .locals 2

    .prologue
    .line 749
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataVersion:J

    .line 750
    return-void
.end method

.method public updateFaces(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "joinedName"    # Ljava/lang/String;

    .prologue
    .line 731
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/access/face/PersonList;->addPerson(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 732
    .local v0, "personId":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-direct {p0, v1, p1, p2, v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateSelectedFaces(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    .line 733
    return-void
.end method

.method public updateMediaItems(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 753
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v8, 0x0

    .line 754
    .local v8, "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    const/4 v7, 0x0

    .local v7, "i":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .local v9, "n":I
    :goto_0
    if-ge v7, v9, :cond_2

    .line 756
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_1

    .line 757
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    check-cast v8, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 758
    .restart local v8    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    const/4 v6, 0x0

    .line 759
    .local v6, "cursor":Landroid/database/Cursor;
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget v2, v8, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 760
    .local v1, "uri":Landroid/net/Uri;
    const-string v5, "datetaken DESC, _id DESC"

    .line 762
    .local v5, "strOrderClause":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 763
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 764
    invoke-virtual {v8, v6}, Lcom/sec/android/gallery3d/data/LocalImage;->updateContent(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 767
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 754
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v5    # "strOrderClause":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 767
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v5    # "strOrderClause":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 771
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v5    # "strOrderClause":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    return-void
.end method

.method public updateMediaSet()V
    .locals 0

    .prologue
    .line 743
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->updateMediaSet()V

    .line 744
    invoke-static {}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->setVersion()V

    .line 745
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->onContentDirty()V

    .line 746
    return-void
.end method

.method public updateOneFace(Landroid/content/Context;Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "personId"    # I

    .prologue
    .line 736
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    .line 737
    .local v0, "pathS":Ljava/lang/String;
    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 738
    .local v1, "values":[Ljava/lang/String;
    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {p1, v2, p3}, Lcom/sec/samsung/gallery/access/face/FaceList;->setPerson(Landroid/content/Context;II)V

    .line 739
    return-void
.end method

.method public updateOperation(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 9
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p3, "operationType"    # I
    .param p4, "joinedName"    # Ljava/lang/String;
    .param p5, "parentMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;I",
            "Ljava/lang/String;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ")V"
        }
    .end annotation

    .prologue
    .line 452
    .local p2, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const-string v0, "reload"

    const-string v1, "clusteralbum showProcessingDialog"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/ArcLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    .line 454
    .local v4, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v8

    new-instance v0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mName:Ljava/lang/String;

    move-object v1, p0

    move-object v3, p2

    move v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;-><init>(Lcom/sec/android/gallery3d/data/ClusterAlbum;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Context;ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;)V

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    .line 459
    return-void
.end method

.method public updateSlotName(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 6
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "joinedName"    # Ljava/lang/String;
    .param p5, "parentMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ")V"
        }
    .end annotation

    .prologue
    .line 436
    .local p2, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p3, :cond_0

    .line 437
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mName:Ljava/lang/String;

    .line 439
    :cond_0
    const/4 v3, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateOperation(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 440
    return-void
.end method
