.class Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;
.super Ljava/lang/Object;
.source "HelpImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/HelpImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RegionDecoderJob"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/HelpImage;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/data/HelpImage;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/data/HelpImage;Lcom/sec/android/gallery3d/data/HelpImage$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/data/HelpImage;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/data/HelpImage$1;

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;-><init>(Lcom/sec/android/gallery3d/data/HelpImage;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 3
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v2, 0x0

    .line 209
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    # invokes: Lcom/sec/android/gallery3d/data/HelpImage;->prepareInputFile(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z
    invoke-static {v1, p1}, Lcom/sec/android/gallery3d/data/HelpImage;->access$100(Lcom/sec/android/gallery3d/data/HelpImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 210
    const/4 v0, 0x0

    .line 224
    :cond_0
    :goto_0
    return-object v0

    .line 211
    :cond_1
    const/4 v0, 0x0

    .line 212
    .local v0, "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/data/HelpImage;->isdrm:Z

    if-eqz v1, :cond_2

    .line 213
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/HelpImage;->getFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v2}, Lcom/sec/android/gallery3d/data/DecodeUtils;->createBitmapRegionDecoder(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    .line 214
    if-nez v0, :cond_3

    .line 215
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    # getter for: Lcom/sec/android/gallery3d/data/HelpImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v1}, Lcom/sec/android/gallery3d/data/HelpImage;->access$200(Lcom/sec/android/gallery3d/data/HelpImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getBrokenImageRegionDecoder(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    goto :goto_0

    .line 218
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    # getter for: Lcom/sec/android/gallery3d/data/HelpImage;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;
    invoke-static {v1}, Lcom/sec/android/gallery3d/data/HelpImage;->access$300(Lcom/sec/android/gallery3d/data/HelpImage;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-static {p1, v1, v2}, Lcom/sec/android/gallery3d/data/DecodeUtils;->createBitmapRegionDecoder(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/io/FileDescriptor;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    .line 220
    :cond_3
    if-eqz v0, :cond_0

    .line 221
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getWidth()I

    move-result v2

    # setter for: Lcom/sec/android/gallery3d/data/HelpImage;->mWidth:I
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/data/HelpImage;->access$402(Lcom/sec/android/gallery3d/data/HelpImage;I)I

    .line 222
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getHeight()I

    move-result v2

    # setter for: Lcom/sec/android/gallery3d/data/HelpImage;->mHeight:I
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/data/HelpImage;->access$502(Lcom/sec/android/gallery3d/data/HelpImage;I)I

    goto :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 206
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/HelpImage$RegionDecoderJob;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    return-object v0
.end method
