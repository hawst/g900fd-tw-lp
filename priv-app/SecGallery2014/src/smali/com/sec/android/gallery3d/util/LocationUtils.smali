.class public Lcom/sec/android/gallery3d/util/LocationUtils;
.super Ljava/lang/Object;
.source "LocationUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mLocationUtils:Lcom/sec/android/gallery3d/util/LocationUtils;


# instance fields
.field private mCurLocation:Landroid/location/Location;

.field private mLocationListener:Landroid/location/LocationListener;

.field private mLocationMgr:Landroid/location/LocationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/gallery3d/util/LocationUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/LocationUtils;->TAG:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationUtils:Lcom/sec/android/gallery3d/util/LocationUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mCurLocation:Landroid/location/Location;

    .line 31
    iput-object v0, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationMgr:Landroid/location/LocationManager;

    .line 58
    new-instance v0, Lcom/sec/android/gallery3d/util/LocationUtils$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/util/LocationUtils$1;-><init>(Lcom/sec/android/gallery3d/util/LocationUtils;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationListener:Landroid/location/LocationListener;

    .line 36
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/util/LocationUtils;Landroid/location/Location;)Landroid/location/Location;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/LocationUtils;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mCurLocation:Landroid/location/Location;

    return-object p1
.end method

.method public static getInstance()Lcom/sec/android/gallery3d/util/LocationUtils;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationUtils:Lcom/sec/android/gallery3d/util/LocationUtils;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/sec/android/gallery3d/util/LocationUtils;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/util/LocationUtils;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationUtils:Lcom/sec/android/gallery3d/util/LocationUtils;

    .line 55
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationUtils:Lcom/sec/android/gallery3d/util/LocationUtils;

    return-object v0
.end method


# virtual methods
.method public getLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mCurLocation:Landroid/location/Location;

    return-object v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationMgr:Landroid/location/LocationManager;

    if-nez v1, :cond_0

    .line 41
    const-string v1, "location"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    iput-object v1, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationMgr:Landroid/location/LocationManager;

    .line 42
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationMgr:Landroid/location/LocationManager;

    const-string v2, "network"

    iget-object v3, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationListener:Landroid/location/LocationListener;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 44
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationMgr:Landroid/location/LocationManager;

    const-string v2, "network"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mCurLocation:Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/sec/android/gallery3d/util/LocationUtils;->TAG:Ljava/lang/String;

    const-string v2, "network provider doesn\'t exists by some reason"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationMgr:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationMgr:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 84
    iput-object v2, p0, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationMgr:Landroid/location/LocationManager;

    .line 86
    :cond_0
    sput-object v2, Lcom/sec/android/gallery3d/util/LocationUtils;->mLocationUtils:Lcom/sec/android/gallery3d/util/LocationUtils;

    .line 87
    return-void
.end method
