.class Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$4;
.super Ljava/lang/Object;
.source "WidgetPreferenceActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$4;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x1

    .line 159
    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$4;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    # getter for: Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mLayoutChanger:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;
    invoke-static {v1}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->access$100(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->setListView(Landroid/widget/AdapterView;)V

    .line 160
    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$4;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    # getter for: Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mLayoutChanger:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;
    invoke-static {v1}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->access$100(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;

    move-result-object v1

    if-ne p3, v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->setSelected(Z)V

    .line 161
    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 164
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
