.class public Lcom/sec/android/gallery3d/data/EventAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "EventAlbum.java"


# static fields
.field private static final COUNT_PROJECTION:[Ljava/lang/String;

.field private static final INVALID:I = -0x1

.field private static final ORDER_CLAUSE_ASC:Ljava/lang/String; = "datetaken ASC, _id ASC"

.field private static final ORDER_CLAUSE_DESC:Ljava/lang/String; = "datetaken DESC, _id DESC"

.field private static final TAG:Ljava/lang/String; = "EventAlbum"


# instance fields
.field private final data:Lcom/sec/android/gallery3d/data/EventAlbumManager;

.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mBucketId:I

.field private mCachedCount:I

.field private final mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

.field private mEventAlbumTimeInfo:Ljava/lang/String;

.field private final mEventInfoProjection:[Ljava/lang/String;

.field private mHasLocation:Z

.field private final mIsImage:Z

.field private mIsSuggestion:I

.field private final mItemPath:Lcom/sec/android/gallery3d/data/Path;

.field private mName:Ljava/lang/String;

.field private mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mOrderClause:Ljava/lang/String;

.field private final mProjection:[Ljava/lang/String;

.field private final mTable:Ljava/lang/String;

.field private final mWatchUris:[Landroid/net/Uri;

.field private final mWhereClause:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "count(*)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/data/EventAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "bucketId"    # I
    .param p4, "isImage"    # Z

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    invoke-static {}, Lcom/sec/android/gallery3d/data/EventAlbum;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    .line 56
    const-string v0, "datetaken DESC, _id DESC"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mOrderClause:Ljava/lang/String;

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mCachedCount:I

    .line 68
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 69
    invoke-static {p2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->data:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    .line 70
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 71
    iput p3, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mBucketId:I

    .line 72
    iput-boolean p4, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mIsImage:Z

    .line 74
    if-eqz p4, :cond_0

    .line 75
    const-string v0, "images_event_album"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mTable:Ljava/lang/String;

    .line 76
    const-string v0, "bucket_id = ?"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mWhereClause:Ljava/lang/String;

    .line 77
    new-array v0, v4, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->EVENT_ALBUM_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mWatchUris:[Landroid/net/Uri;

    .line 78
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mProjection:[Ljava/lang/String;

    .line 79
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "bucket_display_name"

    aput-object v1, v0, v2

    const-string/jumbo v1, "suggestion"

    aput-object v1, v0, v3

    const-string v1, "latitude"

    aput-object v1, v0, v4

    const-string v1, "longitude"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mEventInfoProjection:[Ljava/lang/String;

    .line 81
    sget-object v0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    iget v1, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mBucketId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 92
    :goto_0
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mWatchUris:[Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;[Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 93
    return-void

    .line 83
    :cond_0
    const-string/jumbo v0, "video_event_album"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mTable:Ljava/lang/String;

    .line 84
    const-string v0, "bucket_id = ?"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mWhereClause:Ljava/lang/String;

    .line 85
    new-array v0, v4, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->EVENT_ALBUM_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mWatchUris:[Landroid/net/Uri;

    .line 86
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalVideo;->PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mProjection:[Ljava/lang/String;

    .line 87
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "bucket_display_name"

    aput-object v1, v0, v2

    const-string/jumbo v1, "suggestion"

    aput-object v1, v0, v3

    const-string v1, "latitude"

    aput-object v1, v0, v4

    const-string v1, "longitude"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mEventInfoProjection:[Ljava/lang/String;

    .line 89
    sget-object v0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    iget v1, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mBucketId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0
.end method

.method private getOrderClause()Ljava/lang/String;
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 272
    const-string v0, "datetaken ASC, _id ASC"

    .line 274
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "datetaken DESC, _id DESC"

    goto :goto_0
.end method

.method private updateInfo()V
    .locals 9

    .prologue
    .line 248
    const/4 v6, 0x0

    .line 250
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mTable:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mEventInfoProjection:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mWhereClause:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget v8, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mBucketId:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 253
    if-eqz v6, :cond_1

    .line 254
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mName:Ljava/lang/String;

    .line 255
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mIsSuggestion:I

    .line 257
    :cond_0
    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    const/4 v2, 0x3

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mHasLocation:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    :cond_1
    :goto_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 268
    :goto_1
    return-void

    .line 261
    :cond_2
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 263
    :catch_0
    move-exception v7

    .line 264
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 266
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method


# virtual methods
.method public getBucketId()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mBucketId:I

    return v0
.end method

.method public getEventAlbumTimeInfo()Ljava/lang/String;
    .locals 3

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->data:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/EventAlbum;->getMediaItemCount()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/gallery3d/data/EventAlbum;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getTimeString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 16
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .local v14, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-ltz p1, :cond_0

    if-gtz p2, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-object v14

    .line 102
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/data/EventAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v9

    .line 103
    .local v9, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " limit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 104
    .local v13, "limit":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/EventAlbum;->getOrderClause()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/gallery3d/data/EventAlbum;->mOrderClause:Ljava/lang/String;

    .line 106
    const/4 v8, 0x0

    .line 108
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/data/EventAlbum;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/EventAlbum;->mTable:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/EventAlbum;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/EventAlbum;->mWhereClause:Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/gallery3d/data/EventAlbum;->mBucketId:I

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/EventAlbum;->mOrderClause:Ljava/lang/String;

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 115
    if-nez v8, :cond_2

    .line 116
    const-string v1, "EventAlbum"

    const-string v2, "query fail: "

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 120
    :cond_2
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 121
    .local v11, "id":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/data/EventAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1, v11}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    .line 122
    .local v7, "childPath":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v9, v7}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 123
    .local v12, "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    if-nez v12, :cond_4

    .line 124
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/data/EventAlbum;->mIsImage:Z

    if-eqz v1, :cond_3

    .line 125
    new-instance v12, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v12    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/data/EventAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v12, v7, v1, v8}, Lcom/sec/android/gallery3d/data/LocalImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .line 132
    .restart local v12    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :goto_1
    invoke-virtual {v14, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    .line 139
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 127
    :cond_3
    :try_start_2
    new-instance v12, Lcom/sec/android/gallery3d/data/LocalVideo;

    .end local v12    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/data/EventAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v12, v7, v1, v8}, Lcom/sec/android/gallery3d/data/LocalVideo;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .restart local v12    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    goto :goto_1

    .line 130
    :cond_4
    invoke-virtual {v12, v8}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->updateContent(Landroid/database/Cursor;)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 134
    .end local v7    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v11    # "id":I
    .end local v12    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :catch_0
    move-exception v10

    .line 135
    .local v10, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 139
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 136
    .end local v10    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v10

    .line 137
    .local v10, "e":Landroid/database/CursorWindowAllocationException;
    :try_start_4
    invoke-virtual {v10}, Landroid/database/CursorWindowAllocationException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 139
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .end local v10    # "e":Landroid/database/CursorWindowAllocationException;
    :catchall_0
    move-exception v1

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method public getMediaItemCount()I
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 146
    iget v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mCachedCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 147
    const/4 v6, 0x0

    .line 149
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mTable:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/gallery3d/data/EventAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mWhereClause:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget v8, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mBucketId:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 156
    if-nez v6, :cond_0

    .line 157
    const-string v0, "EventAlbum"

    const-string v1, "query fail"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v7

    .line 165
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v0

    .line 160
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mCachedCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 165
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mCachedCount:I

    goto :goto_0

    .line 162
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPathOnFileSystem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 4

    .prologue
    .line 213
    const-wide v0, 0x4030400000600405L    # 16.250000022355398

    .line 215
    .local v0, "supported":J
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/EventAlbum;->isSuggestionEvent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 216
    const-wide v2, -0x4000000000000001L    # -1.9999999999999998

    and-long/2addr v0, v2

    .line 219
    :cond_0
    return-wide v0
.end method

.method public getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 1

    .prologue
    .line 244
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    return-object v0
.end method

.method public hasLocation()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mHasLocation:Z

    return v0
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x1

    return v0
.end method

.method public isSuggestionEvent()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 170
    iget v1, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mIsSuggestion:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reload()J
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 198
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-static {}, Lcom/sec/android/gallery3d/data/EventAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mDataVersion:J

    .line 200
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mCachedCount:I

    .line 201
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->data:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mIsImage:Z

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mTable:Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mBucketId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->updateLocalDB(ZLjava/lang/String;Ljava/lang/Integer;)Ljava/util/List;

    .line 203
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mName:Ljava/lang/String;

    .line 204
    iput v4, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mIsSuggestion:I

    .line 205
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mHasLocation:Z

    .line 206
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/EventAlbum;->updateInfo()V

    .line 208
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mDataVersion:J

    return-wide v0
.end method

.method public setSuggestionEvent(I)V
    .locals 0
    .param p1, "isSuggestion"    # I

    .prologue
    .line 175
    iput p1, p0, Lcom/sec/android/gallery3d/data/EventAlbum;->mIsSuggestion:I

    .line 176
    return-void
.end method
