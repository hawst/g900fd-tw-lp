.class Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;
.super Ljava/lang/Object;
.source "GlRootView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnStylusButtonEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlRootView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StylusEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 0

    .prologue
    .line 2415
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlRootView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/glcore/GlRootView$1;

    .prologue
    .line 2415
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    return-void
.end method


# virtual methods
.method public onStylusButtonEvent(Landroid/view/MotionEvent;I)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "clipboardId"    # I

    .prologue
    .line 2418
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$1100(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2419
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$1100(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onStylusButtonEvent(Landroid/view/MotionEvent;I)V

    .line 2421
    :cond_0
    return-void
.end method
