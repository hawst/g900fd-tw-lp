.class Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;
.super Ljava/lang/Object;
.source "GalleryAppImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/app/GalleryAppImpl;->syncContactsOnThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 332
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    # setter for: Lcom/sec/android/gallery3d/app/GalleryAppImpl;->bNeedSync:Z
    invoke-static {v2, v5}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->access$202(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Z)Z

    .line 333
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->access$000(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->syncAllContactInfoList(Landroid/content/Context;)V

    .line 334
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    # setter for: Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsContactsChanged:Z
    invoke-static {v2, v4}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->access$302(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Z)Z

    .line 335
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactsChangeListener:Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->access$400(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 336
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactsChangeListener:Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->access$400(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;->onContactsChange()V

    .line 339
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mListeners:Ljava/util/WeakHashMap;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->access$500(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)Ljava/util/WeakHashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;

    .line 340
    .local v1, "observer":Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;
    invoke-interface {v1, v4}, Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;->onChange(Z)V

    goto :goto_0

    .line 342
    .end local v1    # "observer":Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    # setter for: Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsChanged:Z
    invoke-static {v2, v4}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->access$602(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Z)Z

    .line 343
    const-string/jumbo v2, "syncContacts"

    const-string v3, "onChange"

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryAppImpl;->bNeedSync:Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->access$200(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 345
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    # setter for: Lcom/sec/android/gallery3d/app/GalleryAppImpl;->bNowSync:Z
    invoke-static {v2, v5}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->access$702(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Z)Z

    .line 346
    return-void
.end method
