.class Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;
.super Lcom/sec/android/gallery3d/ui/BitmapLoader;
.source "AlbumSetSlidingWindow.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$EntryUpdater;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbumLabelLoader"
.end annotation


# instance fields
.field private final mSlotIndex:I

.field private final mSourceType:I

.field private final mTitle:Ljava/lang/String;

.field private final mTotalCount:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;ILjava/lang/String;II)V
    .locals 0
    .param p2, "slotIndex"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "totalCount"    # I
    .param p5, "sourceType"    # I

    .prologue
    .line 491
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/BitmapLoader;-><init>()V

    .line 492
    iput p2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->mSlotIndex:I

    .line 493
    iput-object p3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->mTitle:Ljava/lang/String;

    .line 494
    iput p4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->mTotalCount:I

    .line 495
    iput p5, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->mSourceType:I

    .line 496
    return-void
.end method


# virtual methods
.method protected onLoadComplete(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 506
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->access$300(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 507
    return-void
.end method

.method protected submitBitmapTask(Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 500
    .local p1, "l":Lcom/sec/android/gallery3d/util/FutureListener;, "Lcom/sec/android/gallery3d/util/FutureListener<Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->access$200(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelMaker:Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->access$1000(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->mTitle:Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->mTotalCount:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->mSourceType:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;->requestLabel(Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    return-object v0
.end method

.method public updateEntry()V
    .locals 6

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 512
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 527
    :cond_0
    :goto_0
    return-void

    .line 514
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->access$400(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->mSlotIndex:I

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->access$400(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    move-result-object v5

    array-length v5, v5

    rem-int/2addr v4, v5

    aget-object v1, v3, v4

    .line 515
    .local v1, "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    new-instance v2, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-direct {v2, v0}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;-><init>(Landroid/graphics/Bitmap;)V

    .line 516
    .local v2, "texture":Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;->setOpaque(Z)V

    .line 517
    iput-object v2, v1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    .line 519
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->mSlotIndex:I

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->isActiveSlot(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 520
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelUploader:Lcom/sec/android/gallery3d/glrenderer/TextureUploader;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->access$1100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/glrenderer/TextureUploader;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/glrenderer/TextureUploader;->addFgTexture(Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;)V

    .line 521
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    # --operator for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveRequestCount:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->access$706(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)I

    .line 522
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveRequestCount:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->access$700(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    # invokes: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->requestNonactiveImages()V
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->access$800(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)V

    .line 523
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->access$900(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->access$900(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;->onContentChanged()V

    goto :goto_0

    .line 525
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelUploader:Lcom/sec/android/gallery3d/glrenderer/TextureUploader;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->access$1100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/glrenderer/TextureUploader;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/glrenderer/TextureUploader;->addBgTexture(Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;)V

    goto :goto_0
.end method
