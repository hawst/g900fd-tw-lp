.class Lcom/sec/android/gallery3d/app/ContextualTagSetting$1;
.super Landroid/os/Handler;
.source "ContextualTagSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/ContextualTagSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$1;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x1

    .line 85
    iget v3, p1, Landroid/os/Message;->what:I

    .line 87
    const/4 v1, 0x1

    .line 88
    .local v1, "ischecked":Z
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$1;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$000(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 89
    .local v2, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "ContextualTagEnable"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 91
    .local v0, "isCommited":Z
    if-eqz v0, :cond_0

    .line 92
    # setter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->isContextualTagEnabledInited:Z
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$102(Z)Z

    .line 93
    # setter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->isContextualTagEnabled:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$202(Z)Z

    .line 95
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$1;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$300(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Landroid/widget/ListView;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 96
    if-eqz v1, :cond_2

    .line 97
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$1;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$300(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Landroid/widget/ListView;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAlpha(F)V

    .line 98
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$1;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$1;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$400(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)I

    move-result v4

    # invokes: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setVisiblePreviewText(I)V
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$500(Lcom/sec/android/gallery3d/app/ContextualTagSetting;I)V

    .line 111
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$1;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$700(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Landroid/widget/Switch;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 114
    return-void

    .line 107
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$1;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$300(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Landroid/widget/ListView;

    move-result-object v3

    const v4, 0x3e99999a    # 0.3f

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAlpha(F)V

    .line 108
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$1;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    const/4 v4, 0x0

    # invokes: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->updatePreviewTextVisible(IZ)V
    invoke-static {v3, v4, v5}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$600(Lcom/sec/android/gallery3d/app/ContextualTagSetting;IZ)V

    goto :goto_0
.end method
