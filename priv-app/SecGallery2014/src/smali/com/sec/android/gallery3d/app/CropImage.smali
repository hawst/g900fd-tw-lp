.class public Lcom/sec/android/gallery3d/app/CropImage;
.super Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
.source "CropImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/CropImage$LoadBitmapDataTask;,
        Lcom/sec/android/gallery3d/app/CropImage$LoadDataTask;,
        Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;
    }
.end annotation


# static fields
.field public static final ACTION_CHANGE_COVER_BACKGROUND:Ljava/lang/String; = "com.sec.android.sviewcover.CHANGE_COVER_BACKGROUND"

.field public static final ACTION_CROP:Ljava/lang/String; = "com.android.camera.action.CROP"

.field public static final ACTION_CROP_SEC_ONLY:Ljava/lang/String; = "com.sed.android.gallery3d.CROP_SEC_ONLY"

.field private static final BACKUP_PIXEL_COUNT:I = 0x75300

.field private static final CONTACT_PACKAGE_NAME:Ljava/lang/String; = "com.android.contacts"

.field public static final COVER_WALLPAPER_NAME:Ljava/lang/String; = "cover_wallpaper"

.field public static final CROP_ACTION:Ljava/lang/String; = "com.android.camera.action.CROP"

.field public static final CROP_WIDTH_TB:I = 0x640

.field private static final DEFAULT_COMPRESS_QUALITY:I = 0x64

.field public static final DOWNLOAD_BUCKET:Ljava/io/File;

.field private static final EXIF_SOFTWARE_VALUE:Ljava/lang/String; = "Android Gallery"

.field private static final EXIF_TAGS:[I

.field public static final KEY_ASPECT_X:Ljava/lang/String; = "aspectX"

.field public static final KEY_ASPECT_Y:Ljava/lang/String; = "aspectY"

.field public static final KEY_CROPPED_RECT:Ljava/lang/String; = "cropped-rect"

.field public static final KEY_CROP_AND_DELETE:Ljava/lang/String; = "crop_and_delete"

.field public static final KEY_DATA:Ljava/lang/String; = "data"

.field public static final KEY_FACE_POSITION:Ljava/lang/String; = "face-position"

.field public static final KEY_IS_CALLER_ID:Ljava/lang/String; = "is-caller-id"

.field public static final KEY_IS_MANUAL_FD:Ljava/lang/String; = "is-manual-fd"

.field public static final KEY_IS_SLINK_CROP:Ljava/lang/String; = "is_slink"

.field private static final KEY_LATITUDE:Ljava/lang/String; = "latitude"

.field private static final KEY_LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final KEY_NEED_WALLPAPER_LAYOUT:Ljava/lang/String; = "need_wallpaper_layout"

.field public static final KEY_NO_FACE_DETECTION:Ljava/lang/String; = "noFaceDetection"

.field public static final KEY_OUTPUT_FORMAT:Ljava/lang/String; = "outputFormat"

.field public static final KEY_OUTPUT_X:Ljava/lang/String; = "outputX"

.field public static final KEY_OUTPUT_Y:Ljava/lang/String; = "outputY"

.field public static final KEY_PHOTO_FRAME:Ljava/lang/String; = "photo-frame"

.field public static final KEY_RESIZE_HEIGHT:Ljava/lang/String; = "resizeH"

.field public static final KEY_RESIZE_WIDTH:Ljava/lang/String; = "resizeW"

.field public static final KEY_RETURN_DATA:Ljava/lang/String; = "return-data"

.field public static final KEY_SCALE:Ljava/lang/String; = "scale"

.field public static final KEY_SCALE_UP_IF_NEEDED:Ljava/lang/String; = "scaleUpIfNeeded"

.field public static final KEY_SET_AS_BOTHSCREEN:Ljava/lang/String; = "set-as-bothscreen"

.field public static final KEY_SET_AS_CONTACT_PHOTO:Ljava/lang/String; = "set-as-contactphoto"

.field public static final KEY_SET_AS_IMAGE:Ljava/lang/String; = "set-as-image"

.field public static final KEY_SET_AS_LOCKSCREEN:Ljava/lang/String; = "set-as-lockscreen"

.field public static final KEY_SET_AS_MUSIC_ALBUM:Ljava/lang/String; = "fromMusicPlayer"

.field public static final KEY_SET_AS_WALLPAPER:Ljava/lang/String; = "set-as-wallpaper"

.field public static final KEY_SET_SNOTE:Ljava/lang/String; = "set_snote"

.field public static final KEY_SET_SVIEW:Ljava/lang/String; = "set_sview"

.field public static final KEY_SHOW_WHEN_LOCKED:Ljava/lang/String; = "showWhenLocked"

.field private static final KEY_SIM_SELECTION:Ljava/lang/String; = "sim_selection"

.field public static final KEY_SKIP_DS_SELECTION:Ljava/lang/String; = "SkipDualSimSelection"

.field public static final KEY_SPOTLIGHT_RECT:Ljava/lang/String; = "spotlightRect"

.field public static final KEY_SPOTLIGHT_X:Ljava/lang/String; = "spotlightX"

.field public static final KEY_SPOTLIGHT_Y:Ljava/lang/String; = "spotlightY"

.field private static final KEY_STATE:Ljava/lang/String; = "state"

.field private static final KEY_TAKENTIME:Ljava/lang/String; = "takenTime"

.field private static final MAX_BACKUP_IMAGE_SIZE:I = 0x140

.field private static final MAX_CONTACTPHOTO_SIZE:I = 0x19000

.field private static final MAX_FACE_PIXEL_COUNT:I = 0x9c40

.field private static final MAX_FILE_INDEX:I = 0x3e8

.field private static final MAX_PIXEL_COUNT:I = 0x4c4b40

.field private static final MAX_VIDEOCALL_PHOTO_SIZE:I = 0x7d000

.field private static final MIN_CROP_SIZE:I = 0x10

.field private static final MSG_BITMAP:I = 0x2

.field private static final MSG_CANCEL_DIALOG:I = 0x5

.field private static final MSG_LARGE_BITMAP:I = 0x1

.field private static final MSG_MENU_CANCEL_SELECTED:I = 0x68

.field private static final MSG_MENU_CHANGE_SELECTED:I = 0x6a

.field private static final MSG_MENU_HOME_SELECTED:I = 0x69

.field public static final MSG_MENU_SAVE_DISABLE:I = 0x6c

.field public static final MSG_MENU_SAVE_ENABLE:I = 0x6b

.field private static final MSG_MENU_SAVE_SELECTED:I = 0x67

.field private static final MSG_SAVE_COMPLETE:I = 0x3

.field private static final MSG_SAVE_COMPLETE_SLINK:I = 0x6

.field private static final MSG_SHOW_SAVE_ERROR:I = 0x4

.field private static final MSG_TIMEOUT_BITMAP:I = 0x65

.field private static final MSG_TIMEOUT_LARGE_BITMAP:I = 0x66

.field private static final STATE_INIT:I = 0x0

.field private static final STATE_LOADED:I = 0x1

.field private static final STATE_SAVING:I = 0x2

.field private static final SVIEWCOVER_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.sviewcover"

.field public static final S_VIEW_COVER_PKG_NAME:Ljava/lang/String; = "com.sec.android.sviewcover"

.field private static final TAG:Ljava/lang/String; = "CropImage"

.field private static final TARGET_PIXELS:I = 0x6400

.field private static final TILE_SIZE:I = 0x200

.field private static final TIMEOUT_MAX:I = 0x3a98

.field private static final TIME_STAMP_NAME:Ljava/lang/String; = "\'IMG\'_yyyyMMdd_HHmmss"

.field private static final VIDEOCALL_TARGET_PIXELS:I = 0x1f400


# instance fields
.field private CROP:Ljava/lang/String;

.field private final MODE_HOMESCREEN_WALLPAPER:I

.field private final MODE_LOCKSCREEN_WALLPAPER:I

.field private final SIM_SELECTION_ALL:I

.field private final SIM_SELECTION_SIM1:I

.field private final SIM_SELECTION_SIM2:I

.field private final SUCCESS_MAKE_CONTACT_AGIF:I

.field private VIDEO_CALL_RH:I

.field private VIDEO_CALL_RW:I

.field private final VIDEO_CALL_SELECTION_RATIO:F

.field WALLPAPER_URI:Ljava/lang/String;

.field private lookUpKeyAfterRecreate:Ljava/lang/String;

.field private lookupkeytoPass:Ljava/lang/String;

.field private mActionBarButtons:Landroid/view/View;

.field private final mActionBarListener:Landroid/view/View$OnClickListener;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapInIntent:Landroid/graphics/Bitmap;

.field private mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

.field private mBitmapTileProvider:Lcom/sec/android/gallery3d/ui/BitmapTileProvider;

.field private mBothScreen:Z

.field private mCropView:Lcom/sec/android/gallery3d/ui/CropView;

.field private mDoFaceDetection:Z

.field private mDoneActionView:Landroid/view/View;

.field private mFaceRect:Landroid/graphics/RectF;

.field private mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

.field private mIsCallerId:Z

.field private mIsCloudItem:Z

.field private mIsCropAndDelete:Z

.field private mIsDoneActionViewEnabled:Z

.field private mIsFromVideoCall:Z

.field private mIsManualFD:Z

.field private mIsMultiFrameMode:Z

.field private mIsSaveEnable:Z

.field private mIsSlinkCrop:Z

.field private mIsTablet:Z

.field private mKnox:Z

.field private mKnoxWallpaper:Landroid/graphics/Bitmap;

.field private mLoadBitmapTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private mLockScreen:Z

.field private mMainHandler:Landroid/os/Handler;

.field private mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mMenu:Landroid/view/Menu;

.field private mMenuHandler:Landroid/os/Handler;

.field private mNeedWallpaperLayout:Z

.field private mOffsetX:I

.field private mOffsetY:I

.field private mPausedWhileSaving:Z

.field private mPhotoFrame:Z

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

.field private mSNote:Z

.field private mSView:Z

.field private mSaveTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private mSetWallpaperDialog:Landroid/app/AlertDialog;

.field private mSkipDualSIMSelection:Z

.field private mSpotlightRect:Landroid/graphics/RectF;

.field private mSrcUri:Ljava/lang/String;

.field private mState:I

.field private mUseRegionDecoder:Z

.field private mWallpaper:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 247
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "download"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    .line 2009
    const/16 v0, 0x13

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_DATE_TIME:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_MAKE:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_MODEL:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_FLASH:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_GPS_LATITUDE:I

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_GPS_LONGITUDE:I

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_GPS_LATITUDE_REF:I

    aput v2, v0, v1

    const/4 v1, 0x7

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_GPS_LONGITUDE_REF:I

    aput v2, v0, v1

    const/16 v1, 0x8

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_GPS_ALTITUDE:I

    aput v2, v0, v1

    const/16 v1, 0x9

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_GPS_ALTITUDE_REF:I

    aput v2, v0, v1

    const/16 v1, 0xa

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_GPS_TIME_STAMP:I

    aput v2, v0, v1

    const/16 v1, 0xb

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_GPS_DATE_STAMP:I

    aput v2, v0, v1

    const/16 v1, 0xc

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_WHITE_BALANCE:I

    aput v2, v0, v1

    const/16 v1, 0xd

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_FOCAL_LENGTH:I

    aput v2, v0, v1

    const/16 v1, 0xe

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_GPS_PROCESSING_METHOD:I

    aput v2, v0, v1

    const/16 v1, 0xf

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_F_NUMBER:I

    aput v2, v0, v1

    const/16 v1, 0x10

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_EXPOSURE_BIAS_VALUE:I

    aput v2, v0, v1

    const/16 v1, 0x11

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_EXPOSURE_TIME:I

    aput v2, v0, v1

    const/16 v1, 0x12

    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_ISO_SPEED_RATINGS:I

    aput v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/app/CropImage;->EXIF_TAGS:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 127
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;-><init>()V

    .line 215
    iput v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->MODE_HOMESCREEN_WALLPAPER:I

    .line 216
    iput v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->MODE_LOCKSCREEN_WALLPAPER:I

    .line 218
    iput v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->SIM_SELECTION_ALL:I

    .line 219
    iput v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->SIM_SELECTION_SIM1:I

    .line 220
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->SIM_SELECTION_SIM2:I

    .line 222
    iput v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->SUCCESS_MAKE_CONTACT_AGIF:I

    .line 224
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_SELECTION_RATIO:F

    .line 225
    iput v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RW:I

    .line 226
    iput v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RH:I

    .line 252
    const-string v0, "CROP"

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->CROP:Ljava/lang/String;

    .line 254
    iput v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mState:I

    .line 258
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoFaceDetection:Z

    .line 270
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mUseRegionDecoder:Z

    .line 280
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z

    .line 281
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mPhotoFrame:Z

    .line 282
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLockScreen:Z

    .line 283
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBothScreen:Z

    .line 284
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSNote:Z

    .line 285
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSView:Z

    .line 286
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mNeedWallpaperLayout:Z

    .line 287
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z

    .line 288
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSkipDualSIMSelection:Z

    .line 293
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsSaveEnable:Z

    .line 298
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsDoneActionViewEnabled:Z

    .line 302
    iput-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    .line 304
    new-instance v0, Lcom/sec/android/gallery3d/app/CropImage$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/CropImage$1;-><init>(Lcom/sec/android/gallery3d/app/CropImage;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMenuHandler:Landroid/os/Handler;

    .line 353
    iput-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSpotlightRect:Landroid/graphics/RectF;

    .line 354
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsCloudItem:Z

    .line 356
    iput-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSetWallpaperDialog:Landroid/app/AlertDialog;

    .line 357
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mPausedWhileSaving:Z

    .line 359
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsCallerId:Z

    .line 360
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    .line 363
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsFromVideoCall:Z

    .line 365
    new-instance v0, Lcom/sec/android/gallery3d/app/CropImage$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/CropImage$2;-><init>(Lcom/sec/android/gallery3d/app/CropImage;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    .line 384
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mKnox:Z

    .line 385
    iput-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage;->mKnoxWallpaper:Landroid/graphics/Bitmap;

    .line 658
    new-instance v0, Lcom/sec/android/gallery3d/app/CropImage$4;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/CropImage$4;-><init>(Lcom/sec/android/gallery3d/app/CropImage;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mActionBarListener:Landroid/view/View$OnClickListener;

    .line 2244
    const-string v0, "content://com.sec.knox.provider/RestrctionPolicy4"

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->WALLPAPER_URI:Ljava/lang/String;

    .line 2492
    iput v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mOffsetX:I

    .line 2493
    iput v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mOffsetY:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/CropImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mState:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/CropImage;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->onSaveClicked()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/app/CropImage;Landroid/app/Dialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/CropImage;->dismissDialog(Landroid/app/Dialog;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/CropImage;->onBitmapRegionDecoderAvailable(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/CropImage;->onBitmapAvailable(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLockScreen:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBothScreen:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/gallery3d/app/CropImage;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->setLockScreenSettingAndBroadCast()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mKnox:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mKnoxWallpaper:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mKnoxWallpaper:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/gallery3d/app/CropImage;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->sendResultToSCover()V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/app/CropImage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Z

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/gallery3d/app/CropImage;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSrcUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsCropAndDelete:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/util/Future;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLoadBitmapTask:Lcom/sec/android/gallery3d/util/Future;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/Future;)Lcom/sec/android/gallery3d/util/Future;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/Future;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLoadBitmapTask:Lcom/sec/android/gallery3d/util/Future;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/Future;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/Future;

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/CropImage;->cancelTaskAndDismissProgressDialog(Lcom/sec/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/util/Future;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/Future;)Lcom/sec/android/gallery3d/util/Future;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/Future;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMenuHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/ui/CropView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    return-object v0
.end method

.method static synthetic access$2800(Landroid/graphics/Rect;III)V
    .locals 0
    .param p0, "x0"    # Landroid/graphics/Rect;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 127
    invoke-static {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/app/CropImage;->rotateRectangle(Landroid/graphics/Rect;III)V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->isAGIFCrop()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/app/CropImage;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/CropImage;->changeFrame(ZZ)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Landroid/graphics/Rect;
    .param p2, "x2"    # Z

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/CropImage;->getCroppedImage(Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsCallerId:Z

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "x2"    # Landroid/graphics/Bitmap;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 127
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/app/CropImage;->saveCallerId(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3300(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mNeedWallpaperLayout:Z

    return v0
.end method

.method static synthetic access$3400(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/CropImage;->changeBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsFromVideoCall:Z

    return v0
.end method

.method static synthetic access$3600(Lcom/sec/android/gallery3d/app/CropImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RW:I

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/gallery3d/app/CropImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RH:I

    return v0
.end method

.method static synthetic access$3800(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Landroid/net/Uri;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "x2"    # Landroid/graphics/Bitmap;
    .param p3, "x3"    # Landroid/net/Uri;

    .prologue
    .line 127
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/app/CropImage;->saveBitmapToUri(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3900(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsSaveEnable:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "x2"    # Landroid/graphics/Bitmap;

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/CropImage;->setAsWallpaper(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/app/CropImage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Z

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsSaveEnable:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsTablet:Z

    return v0
.end method

.method static synthetic access$4200(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "x2"    # Landroid/graphics/Bitmap;
    .param p3, "x3"    # Z

    .prologue
    .line 127
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/app/CropImage;->saveLockScreenImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method static synthetic access$4300(Lcom/sec/android/gallery3d/app/CropImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getSimSelectionID()I

    move-result v0

    return v0
.end method

.method static synthetic access$4400(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "x2"    # Landroid/graphics/Bitmap;

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/CropImage;->saveToMediaProvider(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/util/Future;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getLoadBitmapTask()Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/android/gallery3d/app/CropImage;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->saveCropImageFile()V

    return-void
.end method

.method static synthetic access$4802(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/Future;)Lcom/sec/android/gallery3d/util/Future;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/Future;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSaveTask:Lcom/sec/android/gallery3d/util/Future;

    return-object p1
.end method

.method static synthetic access$4900(Lcom/sec/android/gallery3d/app/CropImage;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->lookUpKeyAfterRecreate:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/gallery3d/app/CropImage;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->lookupkeytoPass:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/android/gallery3d/app/CropImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsSlinkCrop:Z

    return v0
.end method

.method static synthetic access$5200(Lcom/sec/android/gallery3d/app/CropImage;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->checkSim()V

    return-void
.end method

.method static synthetic access$5300(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSetWallpaperDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/app/CropImage;Landroid/view/MenuItem;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Landroid/view/MenuItem;
    .param p2, "x2"    # Z

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/CropImage;->setMenuEnable(Landroid/view/MenuItem;Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/app/CropImage;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoneActionView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/gallery3d/app/CropImage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;
    .param p1, "x1"    # Z

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsDoneActionViewEnabled:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/app/CropImage;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CropImage;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method private cancelTaskAndDismissProgressDialog(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2634
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<*>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2635
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 2636
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->waitDone()V

    .line 2637
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/CropImage;->dismissDialog(Landroid/app/Dialog;)V

    .line 2639
    :cond_0
    return-void
.end method

.method private changeBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "ori"    # Landroid/graphics/Bitmap;

    .prologue
    .line 2755
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 2756
    .local v5, "width":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 2757
    .local v1, "height":I
    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 2759
    .local v3, "size":I
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v3, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2760
    .local v4, "target":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2761
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/high16 v6, -0x1000000

    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 2762
    new-instance v2, Landroid/graphics/Paint;

    const/4 v6, 0x2

    invoke-direct {v2, v6}, Landroid/graphics/Paint;-><init>(I)V

    .line 2763
    .local v2, "paint":Landroid/graphics/Paint;
    iget v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mOffsetX:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/android/gallery3d/app/CropImage;->mOffsetY:I

    int-to-float v7, v7

    invoke-virtual {v0, p1, v6, v7, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2765
    return-object v4
.end method

.method private changeExifData(Ljava/lang/String;II)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 1086
    new-instance v1, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 1087
    .local v1, "exifInterface":Lcom/sec/android/gallery3d/exif/ExifInterface;
    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_IMAGE_WIDTH:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/exif/ExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/exif/ExifInterface;->setTag(Lcom/sec/android/gallery3d/exif/ExifTag;)Lcom/sec/android/gallery3d/exif/ExifTag;

    .line 1088
    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_IMAGE_LENGTH:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/exif/ExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/exif/ExifInterface;->setTag(Lcom/sec/android/gallery3d/exif/ExifTag;)Lcom/sec/android/gallery3d/exif/ExifTag;

    .line 1089
    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_SOFTWARE:I

    const-string v3, "Android Gallery"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/exif/ExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/exif/ExifInterface;->setTag(Lcom/sec/android/gallery3d/exif/ExifTag;)Lcom/sec/android/gallery3d/exif/ExifTag;

    .line 1090
    sget v2, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_DATE_TIME:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/exif/ExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/exif/ExifInterface;->setTag(Lcom/sec/android/gallery3d/exif/ExifTag;)Lcom/sec/android/gallery3d/exif/ExifTag;

    .line 1093
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/exif/ExifInterface;->removeCompressedThumbnail()V

    .line 1095
    :try_start_0
    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/exif/ExifInterface;->forceRewriteExif(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1103
    :goto_0
    return-void

    .line 1096
    :catch_0
    move-exception v0

    .line 1097
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v2, "CropImage"

    const-string v3, "cannot find file to set exif: "

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1098
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1099
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 1100
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "CropImage"

    const-string v3, "cannot set exif data - write orientation"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private changeFrame(ZZ)V
    .locals 3
    .param p1, "isMultiFrame"    # Z
    .param p2, "setVisible"    # Z

    .prologue
    .line 2677
    if-eqz p1, :cond_1

    .line 2678
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/CropView;->setIsWallpaper(Z)V

    .line 2679
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/CropView;->setMultiFrame()V

    .line 2680
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2681
    .local v0, "extras":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 2690
    .end local v0    # "extras":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 2683
    .restart local v0    # "extras":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/app/CropImage;->drawMultiFrame(Z)V

    .line 2689
    .end local v0    # "extras":Landroid/os/Bundle;
    :goto_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/CropView;->invalidate()V

    goto :goto_0

    .line 2685
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/CropView;->setIsWallpaper(Z)V

    .line 2686
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/CropView;->setSingleFrame()V

    .line 2687
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/app/CropImage;->drawSingleFrame(Z)V

    goto :goto_1
.end method

.method private checkSim()V
    .locals 2

    .prologue
    .line 2155
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v1, :cond_2

    .line 2156
    :cond_0
    const/4 v0, 0x0

    .line 2158
    .local v0, "isSelected":Z
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->selectSimByNetwork(Landroid/content/Context;)Z

    move-result v0

    .line 2159
    if-nez v0, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSkipDualSIMSelection:Z

    if-eqz v1, :cond_3

    .line 2160
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->saveCropImageFile()V

    .line 2170
    .end local v0    # "isSelected":Z
    :cond_2
    :goto_0
    return-void

    .line 2162
    .restart local v0    # "isSelected":Z
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBothScreen:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLockScreen:Z

    if-eqz v1, :cond_5

    .line 2163
    :cond_4
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->selectSimByUser(I)V

    .line 2164
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->saveCropImageFile()V

    goto :goto_0

    .line 2166
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->displaySimSelectorDialog()V

    goto :goto_0
.end method

.method private convertExtensionToCompressFormat(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;
    .locals 1
    .param p1, "extension"    # Ljava/lang/String;

    .prologue
    .line 1325
    const-string v0, "png"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_0
.end method

.method private static copyExif(Lcom/sec/android/gallery3d/data/MediaItem;II)Lcom/sec/android/gallery3d/exif/ExifInterface;
    .locals 5
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p1, "newWidth"    # I
    .param p2, "newHeight"    # I

    .prologue
    .line 2034
    :try_start_0
    new-instance v0, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 2035
    .local v0, "newExif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->extractExifValues(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/exif/ExifInterface;)V

    .line 2036
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2037
    .local v2, "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/exif/ExifTag;>;"
    sget v3, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_IMAGE_WIDTH:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/gallery3d/exif/ExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2038
    sget v3, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_IMAGE_LENGTH:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/gallery3d/exif/ExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2039
    sget v3, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_ORIENTATION:I

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/gallery3d/exif/ExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2040
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/exif/ExifInterface;->setTags(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2045
    .end local v0    # "newExif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .end local v2    # "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/exif/ExifTag;>;"
    :goto_0
    return-object v0

    .line 2042
    :catch_0
    move-exception v1

    .line 2043
    .local v1, "t":Ljava/lang/Throwable;
    const-string v3, "CropImage"

    const-string v4, "cannot copy exif: "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2045
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static copyExif(Ljava/lang/String;II)Lcom/sec/android/gallery3d/exif/ExifInterface;
    .locals 11
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "newWidth"    # I
    .param p2, "newHeight"    # I

    .prologue
    .line 2049
    new-instance v5, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 2051
    .local v5, "oldExif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    :try_start_0
    invoke-virtual {v5, p0}, Lcom/sec/android/gallery3d/exif/ExifInterface;->readExif(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2058
    :goto_0
    new-instance v4, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 2059
    .local v4, "newExif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2060
    .local v8, "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/exif/ExifTag;>;"
    sget v9, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_IMAGE_WIDTH:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lcom/sec/android/gallery3d/exif/ExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2061
    sget v9, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_IMAGE_LENGTH:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lcom/sec/android/gallery3d/exif/ExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2062
    sget v9, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_ORIENTATION:I

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lcom/sec/android/gallery3d/exif/ExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2065
    sget-object v0, Lcom/sec/android/gallery3d/app/CropImage;->EXIF_TAGS:[I

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_1

    aget v7, v0, v2

    .line 2066
    .local v7, "tagId":I
    invoke-virtual {v5, v7}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v6

    .line 2067
    .local v6, "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    if-eqz v6, :cond_0

    .line 2068
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2065
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2052
    .end local v0    # "arr$":[I
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "newExif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .end local v6    # "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    .end local v7    # "tagId":I
    .end local v8    # "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/exif/ExifTag;>;"
    :catch_0
    move-exception v1

    .line 2053
    .local v1, "e1":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2054
    .end local v1    # "e1":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 2055
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 2071
    .end local v1    # "e1":Ljava/io/IOException;
    .restart local v0    # "arr$":[I
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "newExif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .restart local v8    # "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/exif/ExifTag;>;"
    :cond_1
    invoke-virtual {v4, v8}, Lcom/sec/android/gallery3d/exif/ExifInterface;->setTags(Ljava/util/Collection;)V

    .line 2072
    return-object v4
.end method

.method public static determineCompressFormat(Lcom/sec/android/gallery3d/data/MediaObject;)Ljava/lang/String;
    .locals 3
    .param p0, "obj"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 959
    const-string v0, "JPEG"

    .line 960
    .local v0, "compressFormat":Ljava/lang/String;
    instance-of v2, p0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_1

    .line 961
    check-cast p0, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p0    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v1

    .line 962
    .local v1, "mime":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 963
    const-string v2, "png"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "gif"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 966
    :cond_0
    const-string v0, "PNG"

    .line 970
    .end local v1    # "mime":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method private dismissDialog(Landroid/app/Dialog;)V
    .locals 0
    .param p1, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 2799
    if-eqz p1, :cond_0

    .line 2800
    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    .line 2802
    :cond_0
    return-void
.end method

.method private dismissProgressDialogIfShown()V
    .locals 1

    .prologue
    .line 1938
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 1939
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1940
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1942
    :cond_0
    return-void
.end method

.method private displaySetWallpaperDialog()V
    .locals 3

    .prologue
    .line 2642
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSetWallpaperDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSetWallpaperDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2643
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2644
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0e0119

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2645
    const v1, 0x7f0e0118

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 2647
    const v1, 0x7f0e00db

    new-instance v2, Lcom/sec/android/gallery3d/app/CropImage$11;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/app/CropImage$11;-><init>(Lcom/sec/android/gallery3d/app/CropImage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2664
    const v1, 0x7f0e0046

    new-instance v2, Lcom/sec/android/gallery3d/app/CropImage$12;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/app/CropImage$12;-><init>(Lcom/sec/android/gallery3d/app/CropImage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2672
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSetWallpaperDialog:Landroid/app/AlertDialog;

    .line 2674
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_1
    return-void
.end method

.method private displaySimSelectorDialog()V
    .locals 3

    .prologue
    .line 2138
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v1, :cond_1

    .line 2139
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter;-><init>(Landroid/content/Context;)V

    .line 2142
    .local v0, "simSelectorDataAdapter":Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e0119

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/gallery3d/app/CropImage$8;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/gallery3d/app/CropImage$8;-><init>(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2152
    .end local v0    # "simSelectorDataAdapter":Lcom/sec/android/gallery3d/ds/SimSelectorDataAdapter;
    :cond_1
    return-void
.end method

.method private drawInTiles(Landroid/graphics/Canvas;Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;Landroid/graphics/Rect;Landroid/graphics/Rect;I)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "decoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "dest"    # Landroid/graphics/Rect;
    .param p5, "sample"    # I

    .prologue
    .line 1554
    move/from16 v0, p5

    mul-int/lit16 v5, v0, 0x200

    .line 1555
    .local v5, "tileSize":I
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1556
    .local v4, "tileRect":Landroid/graphics/Rect;
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1557
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v10, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1558
    move/from16 v0, p5

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1559
    move-object/from16 v0, p4

    iget v10, v0, Landroid/graphics/Rect;->left:I

    int-to-float v10, v10

    move-object/from16 v0, p4

    iget v11, v0, Landroid/graphics/Rect;->top:I

    int-to-float v11, v11

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1560
    move/from16 v0, p5

    int-to-float v10, v0

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Rect;->width()I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v10, v11

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->width()I

    move-result v11

    int-to-float v11, v11

    div-float/2addr v10, v11

    move/from16 v0, p5

    int-to-float v11, v0

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Rect;->height()I

    move-result v12

    int-to-float v12, v12

    mul-float/2addr v11, v12

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v11, v12

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->scale(FF)V

    .line 1562
    new-instance v3, Landroid/graphics/Paint;

    const/4 v10, 0x2

    invoke-direct {v3, v10}, Landroid/graphics/Paint;-><init>(I)V

    .line 1563
    .local v3, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, p3

    iget v6, v0, Landroid/graphics/Rect;->left:I

    .local v6, "tx":I
    const/4 v8, 0x0

    .local v8, "x":I
    :goto_0
    move-object/from16 v0, p3

    iget v10, v0, Landroid/graphics/Rect;->right:I

    if-ge v6, v10, :cond_2

    .line 1564
    move-object/from16 v0, p3

    iget v7, v0, Landroid/graphics/Rect;->top:I

    .local v7, "ty":I
    const/4 v9, 0x0

    .local v9, "y":I
    :goto_1
    move-object/from16 v0, p3

    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    if-ge v7, v10, :cond_1

    .line 1565
    add-int v10, v6, v5

    add-int v11, v7, v5

    invoke-virtual {v4, v6, v7, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 1566
    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1570
    monitor-enter p2

    .line 1571
    :try_start_0
    invoke-virtual {p2, v4, v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1572
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1573
    int-to-float v10, v8

    int-to-float v11, v9

    invoke-virtual {p1, v1, v10, v11, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1574
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1564
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    add-int/2addr v7, v5

    add-int/lit16 v9, v9, 0x200

    goto :goto_1

    .line 1572
    :catchall_0
    move-exception v10

    :try_start_1
    monitor-exit p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v10

    .line 1563
    :cond_1
    add-int/2addr v6, v5

    add-int/lit16 v8, v8, 0x200

    goto :goto_0

    .line 1578
    .end local v7    # "ty":I
    .end local v9    # "y":I
    :cond_2
    return-void
.end method

.method private drawMultiFrame(Z)V
    .locals 9
    .param p1, "setVisible"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 2693
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 2694
    .local v2, "extras":Landroid/os/Bundle;
    const-string v5, "aspectX"

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2695
    .local v0, "aspectX":I
    const-string v5, "aspectY"

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 2696
    .local v1, "aspectY":I
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 2697
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    int-to-float v6, v0

    int-to-float v7, v1

    div-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/ui/CropView;->setAspectRatio(F)V

    .line 2699
    :cond_0
    const-string/jumbo v5, "spotlightX"

    invoke-virtual {v2, v5, v8}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v3

    .line 2700
    .local v3, "spotlightX":F
    const-string/jumbo v5, "spotlightY"

    invoke-virtual {v2, v5, v8}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v4

    .line 2701
    .local v4, "spotlightY":F
    cmpl-float v5, v3, v8

    if-eqz v5, :cond_1

    cmpl-float v5, v4, v8

    if-eqz v5, :cond_1

    .line 2702
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v5, v3, v4}, Lcom/sec/android/gallery3d/ui/CropView;->setSpotlightRatio(FF)V

    .line 2704
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSpotlightRect:Landroid/graphics/RectF;

    invoke-virtual {v5, v6, p1}, Lcom/sec/android/gallery3d/ui/CropView;->initializeHighlightRectangle(Landroid/graphics/RectF;Z)V

    .line 2705
    return-void
.end method

.method private drawSingleFrame(Z)V
    .locals 9
    .param p1, "setVisible"    # Z

    .prologue
    const/4 v7, 0x0

    .line 2708
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v6, v7, v7}, Lcom/sec/android/gallery3d/ui/CropView;->setSpotlightRatio(FF)V

    .line 2709
    const/4 v2, 0x0

    .line 2710
    .local v2, "bFlag":Z
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v5

    .line 2711
    .local v5, "width":I
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v3

    .line 2712
    .local v3, "height":I
    const/4 v0, 0x1

    .line 2713
    .local v0, "aspectX":I
    const/4 v1, 0x1

    .line 2715
    .local v1, "aspectY":I
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsCropInTB:Z

    if-eqz v6, :cond_0

    .line 2716
    if-ge v5, v3, :cond_4

    .line 2717
    const/16 v5, 0x640

    .line 2722
    :cond_0
    :goto_0
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsTablet:Z

    if-nez v6, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v6

    if-eqz v6, :cond_6

    :cond_1
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z

    if-eqz v6, :cond_6

    .line 2723
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->isLandscape()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->isPortrait()Z

    move-result v6

    if-eqz v6, :cond_5

    :cond_2
    const/4 v2, 0x1

    .line 2728
    :goto_1
    if-eqz v2, :cond_9

    .line 2729
    if-ge v5, v3, :cond_7

    move v0, v3

    .line 2730
    :goto_2
    if-ge v5, v3, :cond_8

    move v1, v5

    .line 2735
    :goto_3
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    int-to-float v7, v0

    int-to-float v8, v1

    div-float/2addr v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/ui/CropView;->setAspectRatio(F)V

    .line 2736
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 2737
    .local v4, "intent":Landroid/content/Intent;
    if-eqz v4, :cond_3

    .line 2738
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "outputX"

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2739
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "outputY"

    invoke-virtual {v6, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2741
    :cond_3
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v7, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSpotlightRect:Landroid/graphics/RectF;

    invoke-virtual {v6, v7, p1}, Lcom/sec/android/gallery3d/ui/CropView;->initializeHighlightRectangle(Landroid/graphics/RectF;Z)V

    .line 2742
    return-void

    .line 2719
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_4
    const/16 v3, 0x640

    goto :goto_0

    .line 2723
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 2725
    :cond_6
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isLandScapeModel(Landroid/content/Context;)Z

    move-result v2

    goto :goto_1

    :cond_7
    move v0, v5

    .line 2729
    goto :goto_2

    :cond_8
    move v1, v3

    .line 2730
    goto :goto_3

    .line 2732
    :cond_9
    if-ge v5, v3, :cond_a

    move v0, v5

    .line 2733
    :goto_4
    if-ge v5, v3, :cond_b

    move v1, v3

    :goto_5
    goto :goto_3

    :cond_a
    move v0, v3

    .line 2732
    goto :goto_4

    :cond_b
    move v1, v5

    .line 2733
    goto :goto_5
.end method

.method public static getCallerIdSavePath(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2178
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/tempCropPicture"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2179
    .local v0, "path":Ljava/lang/String;
    return-object v0
.end method

.method private getCroppedImage(Landroid/graphics/Rect;)Landroid/graphics/Bitmap;
    .locals 22
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 1416
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-lez v2, :cond_6

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-lez v2, :cond_6

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 1418
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    .line 1420
    .local v8, "extras":Landroid/os/Bundle;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v10

    .line 1421
    .local v10, "outputX":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v11

    .line 1422
    .local v11, "outputY":I
    if-eqz v8, :cond_0

    .line 1423
    const-string v2, "outputX"

    invoke-virtual {v8, v2, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    .line 1424
    const-string v2, "outputY"

    invoke-virtual {v8, v2, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v11

    .line 1427
    :cond_0
    mul-int v2, v10, v11

    const v4, 0x4c4b40

    if-le v2, v4, :cond_1

    .line 1428
    const v2, 0x4a989680    # 5000000.0f

    int-to-float v4, v10

    div-float/2addr v2, v4

    int-to-float v4, v11

    div-float/2addr v2, v4

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v16

    .line 1429
    .local v16, "scale":F
    const-string v2, "CropImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "scale down the cropped image: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1430
    int-to-float v2, v10

    mul-float v2, v2, v16

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v10

    .line 1431
    int-to-float v2, v11

    mul-float v2, v2, v16

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v11

    .line 1436
    .end local v16    # "scale":F
    :cond_1
    const/high16 v17, 0x3f800000    # 1.0f

    .line 1437
    .local v17, "scaleX":F
    const/high16 v18, 0x3f800000    # 1.0f

    .line 1438
    .local v18, "scaleY":F
    new-instance v6, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-direct {v6, v2, v4, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1439
    .local v6, "dest":Landroid/graphics/Rect;
    if-eqz v8, :cond_2

    const-string v2, "scale"

    const/4 v4, 0x1

    invoke-virtual {v8, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1440
    :cond_2
    int-to-float v2, v10

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float v17, v2, v4

    .line 1441
    int-to-float v2, v11

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float v18, v2, v4

    .line 1442
    if-eqz v8, :cond_3

    const-string v2, "scaleUpIfNeeded"

    const/4 v4, 0x0

    invoke-virtual {v8, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1444
    :cond_3
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v17, v2

    if-lez v2, :cond_4

    const/high16 v17, 0x3f800000    # 1.0f

    .line 1445
    :cond_4
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v18, v2

    if-lez v2, :cond_5

    const/high16 v18, 0x3f800000    # 1.0f

    .line 1450
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v17

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v13

    .line 1451
    .local v13, "rectWidth":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v18

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v12

    .line 1452
    .local v12, "rectHeight":I
    sub-int v2, v10, v13

    int-to-float v2, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    sub-int v4, v11, v12

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    add-int v5, v10, v13

    int-to-float v5, v5

    const/high16 v20, 0x40000000    # 2.0f

    div-float v5, v5, v20

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    add-int v20, v11, v12

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    div-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v6, v2, v4, v5, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 1457
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapInIntent:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_7

    .line 1458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapInIntent:Landroid/graphics/Bitmap;

    move-object/from16 v19, v0

    .line 1459
    .local v19, "source":Landroid/graphics/Bitmap;
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v11, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 1461
    .local v14, "result":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v14}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1462
    .local v3, "canvas":Landroid/graphics/Canvas;
    const/4 v2, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v3, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1506
    .end local v3    # "canvas":Landroid/graphics/Canvas;
    .end local v14    # "result":Landroid/graphics/Bitmap;
    .end local v19    # "source":Landroid/graphics/Bitmap;
    :goto_1
    return-object v14

    .line 1416
    .end local v6    # "dest":Landroid/graphics/Rect;
    .end local v8    # "extras":Landroid/os/Bundle;
    .end local v10    # "outputX":I
    .end local v11    # "outputY":I
    .end local v12    # "rectHeight":I
    .end local v13    # "rectWidth":I
    .end local v17    # "scaleX":F
    .end local v18    # "scaleY":F
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1466
    .restart local v6    # "dest":Landroid/graphics/Rect;
    .restart local v8    # "extras":Landroid/os/Bundle;
    .restart local v10    # "outputX":I
    .restart local v11    # "outputY":I
    .restart local v12    # "rectHeight":I
    .restart local v13    # "rectWidth":I
    .restart local v17    # "scaleX":F
    .restart local v18    # "scaleY":F
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/app/CropImage;->mUseRegionDecoder:Z

    if-eqz v2, :cond_9

    .line 1467
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v15

    .line 1468
    .local v15, "rotation":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView;->getImageWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/CropView;->getImageHeight()I

    move-result v4

    rsub-int v5, v15, 0x168

    move-object/from16 v0, p1

    invoke-static {v0, v2, v4, v5}, Lcom/sec/android/gallery3d/app/CropImage;->rotateRectangle(Landroid/graphics/Rect;III)V

    .line 1470
    rsub-int v2, v15, 0x168

    invoke-static {v6, v10, v11, v2}, Lcom/sec/android/gallery3d/app/CropImage;->rotateRectangle(Landroid/graphics/Rect;III)V

    .line 1472
    new-instance v9, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v9}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1473
    .local v9, "options":Landroid/graphics/BitmapFactory$Options;
    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->computeSampleSizeLarger(F)I

    move-result v7

    .line 1475
    .local v7, "sample":I
    iput v7, v9, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1481
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/2addr v2, v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-ne v2, v4, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/2addr v2, v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-ne v2, v4, :cond_8

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-ne v10, v2, :cond_8

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-ne v11, v2, :cond_8

    if-nez v15, :cond_8

    .line 1486
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/app/CropImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    monitor-enter v4

    .line 1487
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/app/CropImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v9}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v14

    monitor-exit v4

    goto :goto_1

    .line 1488
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1490
    :cond_8
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v11, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 1492
    .restart local v14    # "result":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v14}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1493
    .restart local v3    # "canvas":Landroid/graphics/Canvas;
    invoke-static {v3, v10, v11, v15}, Lcom/sec/android/gallery3d/app/CropImage;->rotateCanvas(Landroid/graphics/Canvas;III)V

    .line 1494
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/app/CropImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-object/from16 v2, p0

    move-object/from16 v5, p1

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/gallery3d/app/CropImage;->drawInTiles(Landroid/graphics/Canvas;Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;Landroid/graphics/Rect;Landroid/graphics/Rect;I)V

    goto/16 :goto_1

    .line 1497
    .end local v3    # "canvas":Landroid/graphics/Canvas;
    .end local v7    # "sample":I
    .end local v9    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v14    # "result":Landroid/graphics/Bitmap;
    .end local v15    # "rotation":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v15

    .line 1498
    .restart local v15    # "rotation":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView;->getImageWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/CropView;->getImageHeight()I

    move-result v4

    rsub-int v5, v15, 0x168

    move-object/from16 v0, p1

    invoke-static {v0, v2, v4, v5}, Lcom/sec/android/gallery3d/app/CropImage;->rotateRectangle(Landroid/graphics/Rect;III)V

    .line 1500
    rsub-int v2, v15, 0x168

    invoke-static {v6, v10, v11, v2}, Lcom/sec/android/gallery3d/app/CropImage;->rotateRectangle(Landroid/graphics/Rect;III)V

    .line 1501
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v11, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 1502
    .restart local v14    # "result":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v14}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1503
    .restart local v3    # "canvas":Landroid/graphics/Canvas;
    invoke-static {v3, v10, v11, v15}, Lcom/sec/android/gallery3d/app/CropImage;->rotateCanvas(Landroid/graphics/Canvas;III)V

    .line 1504
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmap:Landroid/graphics/Bitmap;

    new-instance v4, Landroid/graphics/Paint;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Landroid/graphics/Paint;-><init>(I)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v2, v0, v6, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_1
.end method

.method private getCroppedImage(Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;
    .locals 30
    .param p1, "rectOri"    # Landroid/graphics/Rect;
    .param p2, "isHomeWallpaper"    # Z

    .prologue
    .line 2496
    new-instance v13, Landroid/graphics/Rect;

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p1

    iget v7, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v13, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2498
    .local v13, "rect":Landroid/graphics/Rect;
    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v5

    if-lez v5, :cond_11

    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v5

    if-lez v5, :cond_11

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 2500
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v16

    .line 2502
    .local v16, "extras":Landroid/os/Bundle;
    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v18

    .line 2503
    .local v18, "outputX":I
    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v19

    .line 2504
    .local v19, "outputY":I
    if-eqz v16, :cond_0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mUseRegionDecoder:Z

    if-eqz v5, :cond_0

    .line 2505
    const-string v5, "outputX"

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v18

    .line 2506
    const-string v5, "outputY"

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v19

    .line 2509
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z

    if-nez v5, :cond_1

    if-nez p2, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mNeedWallpaperLayout:Z

    if-eqz v5, :cond_a

    .line 2510
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/gallery3d/app/CropImage;->getExtendedRect(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v13

    .line 2511
    iget v5, v13, Landroid/graphics/Rect;->left:I

    if-gez v5, :cond_3

    .line 2512
    iget v5, v13, Landroid/graphics/Rect;->left:I

    neg-int v5, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mOffsetX:I

    .line 2513
    const/4 v5, 0x0

    iput v5, v13, Landroid/graphics/Rect;->left:I

    .line 2515
    :cond_3
    iget v5, v13, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/CropView;->getImageWidth()I

    move-result v6

    if-le v5, v6, :cond_4

    .line 2516
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/CropView;->getImageWidth()I

    move-result v5

    iput v5, v13, Landroid/graphics/Rect;->right:I

    .line 2517
    :cond_4
    iget v5, v13, Landroid/graphics/Rect;->top:I

    if-gez v5, :cond_5

    .line 2518
    iget v5, v13, Landroid/graphics/Rect;->top:I

    neg-int v5, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mOffsetY:I

    .line 2519
    const/4 v5, 0x0

    iput v5, v13, Landroid/graphics/Rect;->top:I

    .line 2521
    :cond_5
    iget v5, v13, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/CropView;->getImageHeight()I

    move-result v6

    if-le v5, v6, :cond_6

    .line 2522
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/CropView;->getImageHeight()I

    move-result v5

    iput v5, v13, Landroid/graphics/Rect;->bottom:I

    .line 2523
    :cond_6
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_12

    .line 2524
    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v5

    mul-int v5, v5, v19

    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v6

    div-int/2addr v5, v6

    int-to-float v5, v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v18

    .line 2528
    :goto_1
    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v5

    move/from16 v0, v19

    if-gt v5, v0, :cond_7

    if-eqz v16, :cond_8

    const-string v5, "scaleUpIfNeeded"

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 2529
    :cond_7
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mOffsetX:I

    mul-int v5, v5, v19

    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v6

    div-int/2addr v5, v6

    int-to-float v5, v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mOffsetX:I

    .line 2530
    :cond_8
    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v5

    move/from16 v0, v18

    if-gt v5, v0, :cond_9

    if-eqz v16, :cond_a

    const-string v5, "scaleUpIfNeeded"

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 2531
    :cond_9
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mOffsetY:I

    mul-int v5, v5, v18

    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v6

    div-int/2addr v5, v6

    int-to-float v5, v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mOffsetY:I

    .line 2534
    :cond_a
    mul-int v5, v18, v19

    const v6, 0x4c4b40

    if-le v5, v6, :cond_b

    .line 2535
    const-wide v6, 0x415312d000000000L    # 5000000.0

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v6, v6, v28

    move/from16 v0, v19

    int-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v6, v6, v28

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v0, v6

    move/from16 v24, v0

    .line 2537
    .local v24, "scale":F
    const-string v5, "CropImage"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "scale down the cropped image: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2538
    move/from16 v0, v18

    int-to-float v5, v0

    mul-float v5, v5, v24

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v18

    .line 2539
    move/from16 v0, v19

    int-to-float v5, v0

    mul-float v5, v5, v24

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v19

    .line 2544
    .end local v24    # "scale":F
    :cond_b
    const/high16 v25, 0x3f800000    # 1.0f

    .line 2545
    .local v25, "scaleX":F
    const/high16 v26, 0x3f800000    # 1.0f

    .line 2546
    .local v26, "scaleY":F
    new-instance v14, Landroid/graphics/Rect;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v14, v5, v6, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2547
    .local v14, "dest":Landroid/graphics/Rect;
    if-eqz v16, :cond_c

    const-string v5, "scale"

    const/4 v6, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 2548
    :cond_c
    move/from16 v0, v18

    int-to-float v5, v0

    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    div-float v25, v5, v6

    .line 2549
    move/from16 v0, v19

    int-to-float v5, v0

    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    div-float v26, v5, v6

    .line 2550
    if-eqz v16, :cond_d

    const-string v5, "scaleUpIfNeeded"

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_f

    .line 2552
    :cond_d
    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, v25, v5

    if-lez v5, :cond_e

    .line 2553
    const/high16 v25, 0x3f800000    # 1.0f

    .line 2554
    :cond_e
    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, v26, v5

    if-lez v5, :cond_f

    .line 2555
    const/high16 v26, 0x3f800000    # 1.0f

    .line 2560
    :cond_f
    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    mul-float v5, v5, v25

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v21

    .line 2561
    .local v21, "rectWidth":I
    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    mul-float v5, v5, v26

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v20

    .line 2562
    .local v20, "rectHeight":I
    sub-int v5, v18, v21

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    sub-int v6, v19, v20

    int-to-float v6, v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    add-int v7, v18, v21

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    add-int v8, v19, v20

    int-to-float v8, v8

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v8, v10

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-virtual {v14, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 2567
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapInIntent:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_13

    .line 2568
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapInIntent:Landroid/graphics/Bitmap;

    move-object/from16 v27, v0

    .line 2569
    .local v27, "source":Landroid/graphics/Bitmap;
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v0, v1, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2571
    .local v4, "result":Landroid/graphics/Bitmap;
    new-instance v11, Landroid/graphics/Canvas;

    invoke-direct {v11, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2572
    .local v11, "canvas":Landroid/graphics/Canvas;
    const/4 v5, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v11, v0, v13, v14, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2612
    .end local v4    # "result":Landroid/graphics/Bitmap;
    .end local v11    # "canvas":Landroid/graphics/Canvas;
    .end local v27    # "source":Landroid/graphics/Bitmap;
    :cond_10
    :goto_2
    return-object v4

    .line 2498
    .end local v14    # "dest":Landroid/graphics/Rect;
    .end local v16    # "extras":Landroid/os/Bundle;
    .end local v18    # "outputX":I
    .end local v19    # "outputY":I
    .end local v20    # "rectHeight":I
    .end local v21    # "rectWidth":I
    .end local v25    # "scaleX":F
    .end local v26    # "scaleY":F
    :cond_11
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 2526
    .restart local v16    # "extras":Landroid/os/Bundle;
    .restart local v18    # "outputX":I
    .restart local v19    # "outputY":I
    :cond_12
    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v5

    mul-int v5, v5, v18

    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v6

    div-int/2addr v5, v6

    int-to-float v5, v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v19

    goto/16 :goto_1

    .line 2576
    .restart local v14    # "dest":Landroid/graphics/Rect;
    .restart local v20    # "rectHeight":I
    .restart local v21    # "rectWidth":I
    .restart local v25    # "scaleX":F
    .restart local v26    # "scaleY":F
    :cond_13
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v23

    .line 2577
    .local v23, "rotation":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/CropView;->getImageWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/CropView;->getImageHeight()I

    move-result v6

    move/from16 v0, v23

    rsub-int v7, v0, 0x168

    invoke-static {v13, v5, v6, v7}, Lcom/sec/android/gallery3d/app/CropImage;->rotateRectangle(Landroid/graphics/Rect;III)V

    .line 2578
    move/from16 v0, v23

    rsub-int v5, v0, 0x168

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v14, v0, v1, v5}, Lcom/sec/android/gallery3d/app/CropImage;->rotateRectangle(Landroid/graphics/Rect;III)V

    .line 2579
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mUseRegionDecoder:Z

    if-eqz v5, :cond_15

    .line 2580
    new-instance v17, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2581
    .local v17, "options":Landroid/graphics/BitmapFactory$Options;
    invoke-static/range {v25 .. v26}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/common/BitmapUtils;->computeSampleSizeLarger(F)I

    move-result v15

    .line 2582
    .local v15, "sample":I
    move-object/from16 v0, v17

    iput v15, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 2583
    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v5

    div-int/2addr v5, v15

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v6

    if-ne v5, v6, :cond_14

    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v5

    div-int/2addr v5, v15

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v6

    if-ne v5, v6, :cond_14

    .line 2586
    const/4 v4, 0x0

    .line 2587
    .restart local v4    # "result":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/app/CropImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    monitor-enter v6

    .line 2588
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-object/from16 v0, v17

    invoke-virtual {v5, v13, v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2589
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2591
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    if-nez v5, :cond_10

    .line 2595
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 2596
    .local v9, "matrix":Landroid/graphics/Matrix;
    move/from16 v0, v23

    int-to-float v5, v0

    invoke-virtual {v9, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 2597
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    const/4 v10, 0x1

    invoke-static/range {v4 .. v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v22

    .local v22, "rotateBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v4, v22

    .line 2600
    goto/16 :goto_2

    .line 2589
    .end local v9    # "matrix":Landroid/graphics/Matrix;
    .end local v22    # "rotateBitmap":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 2602
    .end local v4    # "result":Landroid/graphics/Bitmap;
    :cond_14
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v0, v1, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2603
    .restart local v4    # "result":Landroid/graphics/Bitmap;
    new-instance v11, Landroid/graphics/Canvas;

    invoke-direct {v11, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2604
    .restart local v11    # "canvas":Landroid/graphics/Canvas;
    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v23

    invoke-static {v11, v0, v1, v2}, Lcom/sec/android/gallery3d/app/CropImage;->rotateCanvas(Landroid/graphics/Canvas;III)V

    .line 2605
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/app/CropImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-object/from16 v10, p0

    invoke-direct/range {v10 .. v15}, Lcom/sec/android/gallery3d/app/CropImage;->drawInTiles(Landroid/graphics/Canvas;Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;Landroid/graphics/Rect;Landroid/graphics/Rect;I)V

    goto/16 :goto_2

    .line 2608
    .end local v4    # "result":Landroid/graphics/Bitmap;
    .end local v11    # "canvas":Landroid/graphics/Canvas;
    .end local v15    # "sample":I
    .end local v17    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_15
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v0, v1, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2609
    .restart local v4    # "result":Landroid/graphics/Bitmap;
    new-instance v11, Landroid/graphics/Canvas;

    invoke-direct {v11, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2610
    .restart local v11    # "canvas":Landroid/graphics/Canvas;
    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v23

    invoke-static {v11, v0, v1, v2}, Lcom/sec/android/gallery3d/app/CropImage;->rotateCanvas(Landroid/graphics/Canvas;III)V

    .line 2611
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmap:Landroid/graphics/Bitmap;

    new-instance v6, Landroid/graphics/Paint;

    const/4 v7, 0x2

    invoke-direct {v6, v7}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v11, v5, v13, v14, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_2
.end method

.method private getExifData(Ljava/lang/String;)Lcom/sec/android/gallery3d/exif/ExifData;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1080
    const/4 v0, 0x0

    return-object v0
.end method

.method private getExtendedRect(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 2473
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 2474
    .local v2, "resultRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    int-to-float v0, v3

    .line 2475
    .local v0, "centerX":F
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v1, v3

    .line 2477
    .local v1, "centerY":F
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 2478
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    sub-float/2addr v3, v1

    sub-float v3, v0, v3

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 2479
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    sub-float/2addr v3, v1

    add-float/2addr v3, v0

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 2480
    iget v3, p1, Landroid/graphics/Rect;->top:I

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 2481
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 2489
    :goto_0
    return-object v2

    .line 2483
    :cond_0
    iget v3, p1, Landroid/graphics/Rect;->left:I

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 2484
    iget v3, p1, Landroid/graphics/Rect;->right:I

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 2485
    iget v3, p1, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    sub-float/2addr v3, v0

    sub-float v3, v1, v3

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 2486
    iget v3, p1, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    sub-float/2addr v3, v0

    add-float/2addr v3, v1

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method

.method private getFileExtension()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1335
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "outputFormat"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1336
    .local v1, "requestFormat":Ljava/lang/String;
    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v2}, Lcom/sec/android/gallery3d/app/CropImage;->determineCompressFormat(Lcom/sec/android/gallery3d/data/MediaObject;)Ljava/lang/String;

    move-result-object v0

    .line 1340
    .local v0, "outputFormat":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 1341
    const-string v2, "png"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "gif"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const-string v2, "png"

    :goto_1
    return-object v2

    .end local v0    # "outputFormat":Ljava/lang/String;
    :cond_1
    move-object v0, v1

    .line 1336
    goto :goto_0

    .line 1341
    .restart local v0    # "outputFormat":Ljava/lang/String;
    :cond_2
    const-string v2, "jpg"

    goto :goto_1
.end method

.method private getLoadBitmapTask()Lcom/sec/android/gallery3d/util/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2617
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    new-instance v1, Lcom/sec/android/gallery3d/app/CropImage$LoadBitmapDataTask;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/app/CropImage$LoadBitmapDataTask;-><init>(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/data/MediaItem;)V

    new-instance v2, Lcom/sec/android/gallery3d/app/CropImage$10;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/app/CropImage$10;-><init>(Lcom/sec/android/gallery3d/app/CropImage;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    return-object v0
.end method

.method private getMediaEjectFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 2793
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 2794
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 2795
    return-object v0
.end method

.method private getMediaItemFromIntentData()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1945
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 1947
    .local v2, "uri":Landroid/net/Uri;
    if-nez v2, :cond_0

    .line 1948
    const-string v4, "CropImage"

    const-string v5, "no data given"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1960
    :goto_0
    return-object v3

    .line 1952
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 1953
    .local v0, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 1954
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    if-nez v1, :cond_1

    .line 1955
    const-string v4, "CropImage"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cannot get path for: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", or no data given"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1960
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;Z)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0
.end method

.method private getOutputMimeType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1331
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getFileExtension()Ljava/lang/String;

    move-result-object v0

    const-string v1, "png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "image/png"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "image/jpeg"

    goto :goto_0
.end method

.method private getSetAsLockScreenFlag()I
    .locals 5

    .prologue
    .line 2197
    const/4 v1, 0x1

    .line 2199
    .local v1, "flag":I
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2203
    :goto_0
    return v1

    .line 2200
    :catch_0
    move-exception v0

    .line 2201
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "CropImage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getSimSelectionID()I
    .locals 3

    .prologue
    .line 2825
    const/4 v0, -0x1

    .line 2826
    .local v0, "simSelection_id":I
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-nez v2, :cond_0

    move v1, v0

    .line 2838
    .end local v0    # "simSelection_id":I
    .local v1, "simSelection_id":I
    :goto_0
    return v1

    .line 2828
    .end local v1    # "simSelection_id":I
    .restart local v0    # "simSelection_id":I
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->isSetAsSIM1()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->isSetAsSIM2()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2829
    const/4 v0, 0x0

    :cond_1
    :goto_1
    move v1, v0

    .line 2838
    .end local v0    # "simSelection_id":I
    .restart local v1    # "simSelection_id":I
    goto :goto_0

    .line 2831
    .end local v1    # "simSelection_id":I
    .restart local v0    # "simSelection_id":I
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->isSetAsSIM1()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2832
    const/4 v0, 0x1

    goto :goto_1

    .line 2833
    :cond_3
    invoke-static {}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->isSetAsSIM2()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2834
    const/4 v0, 0x2

    goto :goto_1
.end method

.method private hasSaveableSpace()Z
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2372
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2373
    .local v1, "extra":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 2374
    .local v2, "path":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v4, :cond_0

    .line 2375
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 2378
    :cond_0
    if-eqz v1, :cond_1

    .line 2379
    const-string v4, "output"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 2380
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_2

    .line 2381
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 2388
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v4, :cond_5

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2390
    :try_start_0
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z

    if-nez v4, :cond_5

    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLockScreen:Z

    if-nez v4, :cond_5

    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBothScreen:Z

    if-nez v4, :cond_5

    .line 2391
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v4, p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->hasSaveAbleSDCardStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_4

    move v4, v5

    .line 2404
    :goto_0
    return v4

    .line 2382
    .restart local v3    # "uri":Landroid/net/Uri;
    :cond_2
    const-string v4, "return-data"

    invoke-virtual {v1, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "data"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    if-eqz v4, :cond_1

    :cond_3
    move v4, v5

    .line 2384
    goto :goto_0

    .end local v3    # "uri":Landroid/net/Uri;
    :cond_4
    move v4, v6

    .line 2394
    goto :goto_0

    .line 2396
    :catch_0
    move-exception v0

    .line 2397
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 2401
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v4, p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->hasSaveAbleStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v5

    .line 2402
    goto :goto_0

    :cond_6
    move v4, v6

    .line 2404
    goto :goto_0
.end method

.method private initializeData()V
    .locals 13

    .prologue
    .line 1711
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 1713
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_9

    .line 1714
    const-string v8, "noFaceDetection"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1715
    const-string v8, "noFaceDetection"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_7

    const/4 v8, 0x1

    :goto_0
    iput-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoFaceDetection:Z

    .line 1718
    :cond_0
    const-string v8, "set-as-bothscreen"

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    iput-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBothScreen:Z

    .line 1719
    const-string v8, "set-as-lockscreen"

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    iput-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLockScreen:Z

    .line 1720
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v8, :cond_1

    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v8, :cond_2

    .line 1721
    :cond_1
    const-string v8, "SkipDualSimSelection"

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    iput-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSkipDualSIMSelection:Z

    .line 1725
    :cond_2
    const-string v8, "data"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/graphics/Bitmap;

    iput-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapInIntent:Landroid/graphics/Bitmap;

    .line 1727
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSpotlightRect:Landroid/graphics/RectF;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSpotlightRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    .line 1728
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSpotlightRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->setEmpty()V

    .line 1729
    :cond_3
    const-string/jumbo v8, "spotlightRect"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/graphics/RectF;

    iput-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSpotlightRect:Landroid/graphics/RectF;

    .line 1732
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapInIntent:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_9

    .line 1733
    new-instance v8, Lcom/sec/android/gallery3d/ui/BitmapTileProvider;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapInIntent:Landroid/graphics/Bitmap;

    const/16 v10, 0x140

    invoke-direct {v8, v9, v10}, Lcom/sec/android/gallery3d/ui/BitmapTileProvider;-><init>(Landroid/graphics/Bitmap;I)V

    iput-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapTileProvider:Lcom/sec/android/gallery3d/ui/BitmapTileProvider;

    .line 1735
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapTileProvider:Lcom/sec/android/gallery3d/ui/BitmapTileProvider;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/ui/CropView;->setDataModel(Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;I)V

    .line 1736
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoFaceDetection:Z

    if-eqz v8, :cond_5

    .line 1740
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsCallerId:Z

    if-nez v8, :cond_4

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    if-eqz v8, :cond_8

    .line 1741
    :cond_4
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapInIntent:Landroid/graphics/Bitmap;

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/CropImage;->mFaceRect:Landroid/graphics/RectF;

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/ui/CropView;->detectFaces(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)V

    .line 1750
    :cond_5
    :goto_1
    const/4 v8, 0x1

    iput v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mState:I

    .line 1848
    :cond_6
    :goto_2
    return-void

    .line 1715
    :cond_7
    const/4 v8, 0x0

    goto :goto_0

    .line 1743
    :cond_8
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapInIntent:Landroid/graphics/Bitmap;

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMenuHandler:Landroid/os/Handler;

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/ui/CropView;->detectFaces(Landroid/graphics/Bitmap;Landroid/os/Handler;)V

    goto :goto_1

    .line 1755
    :cond_9
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsDoneActionViewEnabled:Z

    .line 1758
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v8, :cond_a

    .line 1759
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-direct {p0, v8}, Lcom/sec/android/gallery3d/app/CropImage;->dismissDialog(Landroid/app/Dialog;)V

    .line 1761
    :cond_a
    const/4 v8, 0x0

    const v9, 0x7f0e0031

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/app/CropImage;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x1

    invoke-static {p0, v8, v9, v10, v11}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1763
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 1764
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMainHandler:Landroid/os/Handler;

    const/4 v10, 0x5

    invoke-virtual {v9, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/ProgressDialog;->setCancelMessage(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1770
    :goto_3
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getMediaItemFromIntentData()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1771
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v8, :cond_b

    .line 1773
    const v8, 0x7f0e0140

    invoke-static {p0, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1774
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->finish()V

    goto :goto_2

    .line 1765
    :catch_0
    move-exception v0

    .line 1766
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    invoke-virtual {v0}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V

    .line 1767
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    goto :goto_3

    .line 1779
    .end local v0    # "e":Landroid/view/WindowManager$BadTokenException;
    :cond_b
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v8, :cond_c

    .line 1780
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/CropView;->addItem(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1783
    :cond_c
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v8}, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->isCloudImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 1784
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsCloudItem:Z

    .line 1788
    :cond_d
    if-eqz v1, :cond_10

    const-string v8, "senderIsVideoCall"

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 1789
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoFaceDetection:Z

    .line 1790
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoCallCrop:Z

    if-eqz v8, :cond_10

    .line 1791
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsFromVideoCall:Z

    .line 1792
    const-string v8, "resizeW"

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RW:I

    .line 1793
    const-string v8, "resizeH"

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RH:I

    .line 1794
    iget v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RW:I

    if-eqz v8, :cond_10

    iget v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RH:I

    if-eqz v8, :cond_10

    .line 1795
    const v7, 0x3e99999a    # 0.3f

    .line 1796
    .local v7, "w":F
    const v2, 0x3e99999a    # 0.3f

    .line 1797
    .local v2, "h":F
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v4

    .line 1798
    .local v4, "imageWidth":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v3

    .line 1799
    .local v3, "imageHeight":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v8

    div-int/lit8 v8, v8, 0x5a

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_e

    .line 1800
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v4

    .line 1801
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v3

    .line 1803
    :cond_e
    const/high16 v6, 0x3f800000    # 1.0f

    .line 1804
    .local v6, "target_ratio":F
    iget v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RW:I

    if-eqz v8, :cond_f

    iget v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RH:I

    if-eqz v8, :cond_f

    .line 1805
    iget v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RW:I

    int-to-float v8, v8

    iget v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->VIDEO_CALL_RH:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    int-to-float v9, v3

    mul-float/2addr v8, v9

    int-to-float v9, v4

    div-float v6, v8, v9

    .line 1807
    :cond_f
    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v8, v6, v8

    if-lez v8, :cond_12

    .line 1808
    div-float v2, v7, v6

    .line 1812
    :goto_4
    new-instance v8, Landroid/graphics/RectF;

    const/high16 v9, 0x3f000000    # 0.5f

    sub-float/2addr v9, v7

    const/high16 v10, 0x3f000000    # 0.5f

    sub-float/2addr v10, v2

    const/high16 v11, 0x3f000000    # 0.5f

    add-float/2addr v11, v7

    const/high16 v12, 0x3f000000    # 0.5f

    add-float/2addr v12, v2

    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSpotlightRect:Landroid/graphics/RectF;

    .line 1817
    .end local v2    # "h":F
    .end local v3    # "imageHeight":I
    .end local v4    # "imageWidth":I
    .end local v6    # "target_ratio":F
    .end local v7    # "w":F
    :cond_10
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v8

    const-wide/16 v10, 0x40

    and-long/2addr v8, v10

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_13

    const/4 v5, 0x1

    .line 1820
    .local v5, "supportedByBitmapRegionDecoder":Z
    :goto_5
    if-eqz v5, :cond_14

    .line 1821
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v8

    new-instance v9, Lcom/sec/android/gallery3d/app/CropImage$LoadDataTask;

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v9, p0, v10}, Lcom/sec/android/gallery3d/app/CropImage$LoadDataTask;-><init>(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/data/MediaItem;)V

    new-instance v10, Lcom/sec/android/gallery3d/app/CropImage$7;

    invoke-direct {v10, p0}, Lcom/sec/android/gallery3d/app/CropImage$7;-><init>(Lcom/sec/android/gallery3d/app/CropImage;)V

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;

    .line 1845
    :goto_6
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z

    if-nez v8, :cond_11

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mNeedWallpaperLayout:Z

    if-eqz v8, :cond_6

    .line 1846
    :cond_11
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z

    const/4 v9, 0x0

    invoke-direct {p0, v8, v9}, Lcom/sec/android/gallery3d/app/CropImage;->changeFrame(ZZ)V

    goto/16 :goto_2

    .line 1810
    .end local v5    # "supportedByBitmapRegionDecoder":Z
    .restart local v2    # "h":F
    .restart local v3    # "imageHeight":I
    .restart local v4    # "imageWidth":I
    .restart local v6    # "target_ratio":F
    .restart local v7    # "w":F
    :cond_12
    mul-float v7, v2, v6

    goto :goto_4

    .line 1817
    .end local v2    # "h":F
    .end local v3    # "imageHeight":I
    .end local v4    # "imageWidth":I
    .end local v6    # "target_ratio":F
    .end local v7    # "w":F
    :cond_13
    const/4 v5, 0x0

    goto :goto_5

    .line 1841
    .restart local v5    # "supportedByBitmapRegionDecoder":Z
    :cond_14
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMainHandler:Landroid/os/Handler;

    const/16 v9, 0x65

    const-wide/16 v10, 0x3a98

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1842
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getLoadBitmapTask()Lcom/sec/android/gallery3d/util/Future;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLoadBitmapTask:Lcom/sec/android/gallery3d/util/Future;

    goto :goto_6
.end method

.method private isAGIFCrop()Z
    .locals 6

    .prologue
    const/16 v5, 0x2d0

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 2769
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2770
    .local v0, "extra":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v1

    .line 2771
    .local v1, "mimeType":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, "image/gif"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2789
    :cond_0
    :goto_0
    return v2

    .line 2773
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v3, v3, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v3, :cond_0

    .line 2775
    const-string v3, "output"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2777
    const-string v3, "scale"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2779
    const-string v3, "aspectX"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v4, :cond_0

    .line 2781
    const-string v3, "aspectY"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v4, :cond_0

    .line 2783
    const-string v3, "outputX"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 2785
    const-string v3, "outputY"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 2789
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {p0, v2}, Lcom/quramsoft/agif/QURAMWINKUTIL;->isAGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v2

    goto :goto_0
.end method

.method private isLandscape()Z
    .locals 2

    .prologue
    .line 2745
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v0

    const/16 v1, 0xb4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPortrait()Z
    .locals 2

    .prologue
    .line 2750
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v0

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v0

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onBitmapAvailable(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x0

    .line 1647
    if-nez p1, :cond_0

    .line 1648
    const v0, 0x7f0e0033

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1649
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->finish()V

    .line 1683
    :goto_0
    return-void

    .line 1653
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x10

    if-gt v0, v1, :cond_1

    .line 1654
    const v0, 0x7f0e0282

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1655
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->finish()V

    goto :goto_0

    .line 1659
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/app/CropImage;->mUseRegionDecoder:Z

    .line 1660
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mState:I

    .line 1662
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 1664
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    new-instance v1, Lcom/sec/android/gallery3d/ui/BitmapTileProvider;

    const/16 v2, 0x200

    invoke-direct {v1, p1, v2}, Lcom/sec/android/gallery3d/ui/BitmapTileProvider;-><init>(Landroid/graphics/Bitmap;I)V

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/CropView;->setDataModel(Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;I)V

    .line 1666
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoFaceDetection:Z

    if-eqz v0, :cond_4

    .line 1670
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsCallerId:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    if-eqz v0, :cond_3

    .line 1671
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mFaceRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/CropView;->detectFaces(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)V

    goto :goto_0

    .line 1673
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMenuHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/CropView;->detectFaces(Landroid/graphics/Bitmap;Landroid/os/Handler;)V

    goto :goto_0

    .line 1678
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mNeedWallpaperLayout:Z

    if-eqz v0, :cond_6

    .line 1679
    :cond_5
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z

    invoke-direct {p0, v0, v3}, Lcom/sec/android/gallery3d/app/CropImage;->changeFrame(ZZ)V

    .line 1681
    :cond_6
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSpotlightRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->initializeHighlightRectangle(Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method private onBitmapRegionDecoderAvailable(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V
    .locals 14
    .param p1, "regionDecoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .prologue
    const/4 v9, 0x1

    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 1581
    if-nez p1, :cond_0

    .line 1582
    const v9, 0x7f0e0033

    invoke-static {p0, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1583
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->finish()V

    .line 1644
    :goto_0
    return-void

    .line 1586
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 1587
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mUseRegionDecoder:Z

    .line 1588
    iput v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mState:I

    .line 1590
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1591
    .local v6, "options":Landroid/graphics/BitmapFactory$Options;
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getWidth()I

    move-result v8

    .line 1592
    .local v8, "width":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getHeight()I

    move-result v4

    .line 1594
    .local v4, "height":I
    invoke-static {v8, v4}, Ljava/lang/Math;->min(II)I

    move-result v9

    const/16 v10, 0x10

    if-gt v9, v10, :cond_1

    .line 1595
    const v9, 0x7f0e0282

    invoke-static {p0, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1596
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->finish()V

    goto :goto_0

    .line 1600
    :cond_1
    const/4 v9, -0x1

    const v10, 0x75300

    invoke-static {v8, v4, v9, v10}, Lcom/sec/android/gallery3d/common/BitmapUtils;->computeSampleSize(IIII)I

    move-result v9

    iput v9, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1602
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9, v12, v12, v8, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p1, v9, v6}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 1605
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v5

    .line 1606
    .local v5, "mimeType":Ljava/lang/String;
    if-eqz v5, :cond_3

    const-string v9, "image/png"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSignatureFile(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1607
    iget v9, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v9, v8, v9

    iget v10, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v10, v4, v10

    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v10, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1608
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1609
    .local v2, "canvas":Landroid/graphics/Canvas;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1610
    .local v7, "paint":Landroid/graphics/Paint;
    sget-object v9, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v9}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1611
    const v9, -0x777778

    invoke-virtual {v7, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 1612
    new-instance v3, Landroid/graphics/Rect;

    iget v9, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v9, v8, v9

    iget v10, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v10, v4, v10

    invoke-direct {v3, v12, v12, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1613
    .local v3, "fillRect":Landroid/graphics/Rect;
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1614
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v13, v13, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1615
    new-instance v9, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    invoke-direct {v9, v1}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    .line 1620
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "canvas":Landroid/graphics/Canvas;
    .end local v3    # "fillRect":Landroid/graphics/Rect;
    .end local v7    # "paint":Landroid/graphics/Paint;
    :goto_1
    new-instance v0, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;-><init>()V

    .line 1621
    .local v0, "adapter":Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    invoke-virtual {v0, v9, v8, v4}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;II)V

    .line 1622
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->setRegionDecoder(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    .line 1623
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v10

    invoke-virtual {v9, v0, v10}, Lcom/sec/android/gallery3d/ui/CropView;->setDataModel(Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;I)V

    .line 1625
    iget-boolean v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoFaceDetection:Z

    if-eqz v9, :cond_5

    .line 1629
    iget-boolean v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsCallerId:Z

    if-nez v9, :cond_2

    iget-boolean v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    if-eqz v9, :cond_4

    .line 1630
    :cond_2
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v11, p0, Lcom/sec/android/gallery3d/app/CropImage;->mFaceRect:Landroid/graphics/RectF;

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/ui/CropView;->detectFaces(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)V

    goto/16 :goto_0

    .line 1617
    .end local v0    # "adapter":Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;
    :cond_3
    new-instance v9, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v9, v10}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    goto :goto_1

    .line 1632
    .restart local v0    # "adapter":Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;
    :cond_4
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v11, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMenuHandler:Landroid/os/Handler;

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/ui/CropView;->detectFaces(Landroid/graphics/Bitmap;Landroid/os/Handler;)V

    goto/16 :goto_0

    .line 1638
    :cond_5
    iget-boolean v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z

    if-nez v9, :cond_6

    iget-boolean v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mNeedWallpaperLayout:Z

    if-eqz v9, :cond_7

    .line 1639
    :cond_6
    iget-boolean v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsMultiFrameMode:Z

    invoke-direct {p0, v9, v12}, Lcom/sec/android/gallery3d/app/CropImage;->changeFrame(ZZ)V

    .line 1641
    :cond_7
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSpotlightRect:Landroid/graphics/RectF;

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/ui/CropView;->initializeHighlightRectangle(Landroid/graphics/RectF;)V

    goto/16 :goto_0
.end method

.method private onSaveClicked()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1372
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->hasSaveableSpace()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1413
    :goto_0
    return-void

    .line 1379
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mKnox:Z

    if-nez v0, :cond_6

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v0, :cond_6

    .line 1380
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mPausedWhileSaving:Z

    if-eqz v0, :cond_2

    .line 1381
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->saveCropImageFile()V

    goto :goto_0

    .line 1382
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBothScreen:Z

    if-eqz v0, :cond_3

    .line 1383
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "set-as-wallpaper"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1384
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "set-as-lockscreen"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1385
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->checkSim()V

    goto :goto_0

    .line 1387
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLockScreen:Z

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getSetAsLockScreenFlag()I

    move-result v0

    if-nez v0, :cond_4

    .line 1388
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->displaySetWallpaperDialog()V

    goto :goto_0

    .line 1390
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z

    if-eqz v0, :cond_5

    .line 1391
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->checkSim()V

    goto :goto_0

    .line 1393
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->saveCropImageFile()V

    goto :goto_0

    .line 1399
    :cond_6
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBothScreen:Z

    if-eqz v0, :cond_7

    .line 1400
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "set-as-wallpaper"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1401
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "set-as-lockscreen"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1402
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->saveCropImageFile()V

    goto :goto_0

    .line 1404
    :cond_7
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLockScreen:Z

    if-nez v0, :cond_8

    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getSetAsLockScreenFlag()I

    move-result v0

    if-nez v0, :cond_8

    .line 1405
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->displaySetWallpaperDialog()V

    goto :goto_0

    .line 1407
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->saveCropImageFile()V

    goto :goto_0
.end method

.method private static rotateCanvas(Landroid/graphics/Canvas;III)V
    .locals 3
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "rotation"    # I

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1511
    int-to-float v0, p1

    div-float/2addr v0, v2

    int-to-float v1, p2

    div-float/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1512
    int-to-float v0, p3

    invoke-virtual {p0, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1513
    div-int/lit8 v0, p3, 0x5a

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    .line 1514
    neg-int v0, p1

    int-to-float v0, v0

    div-float/2addr v0, v2

    neg-int v1, p2

    int-to-float v1, v1

    div-float/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1518
    :goto_0
    return-void

    .line 1516
    :cond_0
    neg-int v0, p2

    int-to-float v0, v0

    div-float/2addr v0, v2

    neg-int v1, p1

    int-to-float v1, v1

    div-float/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0
.end method

.method private static rotateRectangle(Landroid/graphics/Rect;III)V
    .locals 3
    .param p0, "rect"    # Landroid/graphics/Rect;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "rotation"    # I

    .prologue
    .line 1521
    if-eqz p3, :cond_0

    const/16 v2, 0x168

    if-ne p3, v2, :cond_1

    .line 1546
    :cond_0
    :goto_0
    return-void

    .line 1524
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 1525
    .local v1, "w":I
    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 1526
    .local v0, "h":I
    sparse-switch p3, :sswitch_data_0

    .line 1549
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1528
    :sswitch_0
    iget v2, p0, Landroid/graphics/Rect;->left:I

    iput v2, p0, Landroid/graphics/Rect;->top:I

    .line 1529
    iget v2, p0, Landroid/graphics/Rect;->bottom:I

    sub-int v2, p2, v2

    iput v2, p0, Landroid/graphics/Rect;->left:I

    .line 1530
    iget v2, p0, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v0

    iput v2, p0, Landroid/graphics/Rect;->right:I

    .line 1531
    iget v2, p0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v1

    iput v2, p0, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 1535
    :sswitch_1
    iget v2, p0, Landroid/graphics/Rect;->right:I

    sub-int v2, p1, v2

    iput v2, p0, Landroid/graphics/Rect;->left:I

    .line 1536
    iget v2, p0, Landroid/graphics/Rect;->bottom:I

    sub-int v2, p2, v2

    iput v2, p0, Landroid/graphics/Rect;->top:I

    .line 1537
    iget v2, p0, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v1

    iput v2, p0, Landroid/graphics/Rect;->right:I

    .line 1538
    iget v2, p0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v0

    iput v2, p0, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 1542
    :sswitch_2
    iget v2, p0, Landroid/graphics/Rect;->top:I

    iput v2, p0, Landroid/graphics/Rect;->left:I

    .line 1543
    iget v2, p0, Landroid/graphics/Rect;->right:I

    sub-int v2, p1, v2

    iput v2, p0, Landroid/graphics/Rect;->top:I

    .line 1544
    iget v2, p0, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v0

    iput v2, p0, Landroid/graphics/Rect;->right:I

    .line 1545
    iget v2, p0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v1

    iput v2, p0, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 1526
    nop

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch
.end method

.method private saveBitmapToOutputStream(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/io/OutputStream;)Z
    .locals 5
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "format"    # Landroid/graphics/Bitmap$CompressFormat;
    .param p4, "os"    # Ljava/io/OutputStream;

    .prologue
    const/4 v4, 0x0

    .line 1260
    new-instance v0, Lcom/sec/android/gallery3d/util/InterruptableOutputStream;

    invoke-direct {v0, p4}, Lcom/sec/android/gallery3d/util/InterruptableOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1261
    .local v0, "ios":Lcom/sec/android/gallery3d/util/InterruptableOutputStream;
    new-instance v2, Lcom/sec/android/gallery3d/app/CropImage$6;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/gallery3d/app/CropImage$6;-><init>(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/util/InterruptableOutputStream;)V

    invoke-interface {p1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setCancelListener(Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;)V

    .line 1273
    const/16 v2, 0x64

    :try_start_0
    invoke-virtual {p2, p3, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v1

    .line 1274
    .local v1, "result":Z
    if-nez v1, :cond_0

    .line 1275
    const-string v2, "CropImage"

    const-string v3, "Bitmap write errror!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1278
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    .line 1280
    :goto_0
    invoke-interface {p1, v4}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setCancelListener(Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;)V

    .line 1281
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    return v2

    .line 1278
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1280
    .end local v1    # "result":Z
    :catchall_0
    move-exception v2

    invoke-interface {p1, v4}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setCancelListener(Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;)V

    .line 1281
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v2
.end method

.method private saveBitmapToUri(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Landroid/net/Uri;)Z
    .locals 12
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x0

    .line 1289
    :try_start_0
    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1291
    .local v6, "strUri":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v7

    const-string v9, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v7, v9}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1293
    .local v5, "strContactPackageName":Ljava/lang/String;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1294
    :cond_0
    const-string v5, "com.android.contacts"

    .line 1297
    :cond_1
    invoke-virtual {v6, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "com.sec.android.sviewcover"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1298
    :cond_2
    const-string v2, ""

    .line 1299
    .local v2, "folderPath":Ljava/lang/String;
    const-string v7, "file://"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1300
    .local v4, "str":[Ljava/lang/String;
    if-eqz v4, :cond_3

    array-length v7, v4

    const/4 v9, 0x2

    if-ne v7, v9, :cond_3

    .line 1301
    const/4 v7, 0x1

    aget-object v7, v4, v7

    const/4 v9, 0x0

    const/4 v10, 0x1

    aget-object v10, v4, v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v7, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1303
    :cond_3
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1304
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_4

    .line 1305
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 1308
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "folderPath":Ljava/lang/String;
    .end local v4    # "str":[Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-virtual {v7, p3}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1310
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getFileExtension()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/gallery3d/app/CropImage;->convertExtensionToCompressFormat(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v7

    invoke-direct {p0, p1, p2, v7, v3}, Lcom/sec/android/gallery3d/app/CropImage;->saveBitmapToOutputStream(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/io/OutputStream;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    .line 1313
    :try_start_2
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 1320
    .end local v3    # "out":Ljava/io/OutputStream;
    .end local v5    # "strContactPackageName":Ljava/lang/String;
    .end local v6    # "strUri":Ljava/lang/String;
    :goto_0
    return v7

    .line 1313
    .restart local v3    # "out":Ljava/io/OutputStream;
    .restart local v5    # "strContactPackageName":Ljava/lang/String;
    .restart local v6    # "strUri":Ljava/lang/String;
    :catchall_0
    move-exception v7

    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v7
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1315
    .end local v3    # "out":Ljava/io/OutputStream;
    .end local v5    # "strContactPackageName":Ljava/lang/String;
    .end local v6    # "strUri":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1316
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v7, "CropImage"

    const-string v9, "cannot write output"

    invoke-static {v7, v9, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v7, v8

    .line 1320
    goto :goto_0
.end method

.method private saveCallerId(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/lang/String;)Z
    .locals 9
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "croped"    # Landroid/graphics/Bitmap;
    .param p3, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 2207
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2208
    .local v1, "dir":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2210
    .local v0, "candidate":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 2211
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2217
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v7

    if-nez v7, :cond_2

    .line 2218
    :cond_0
    const-string v7, "CropImage"

    const-string v8, "created file not exist: "

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v6

    .line 2242
    :cond_1
    :goto_0
    return v5

    .line 2212
    :catch_0
    move-exception v2

    .line 2213
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "CropImage"

    const-string v8, "fail to create new file: "

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v5, v6

    .line 2214
    goto :goto_0

    .line 2222
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    invoke-virtual {v0, v8, v6}, Ljava/io/File;->setReadable(ZZ)Z

    .line 2223
    invoke-virtual {v0, v8, v6}, Ljava/io/File;->setWritable(ZZ)Z

    .line 2226
    const/4 v3, 0x0

    .line 2228
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2229
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-direct {p0, p1, p2, v7, v4}, Lcom/sec/android/gallery3d/app/CropImage;->saveBitmapToOutputStream(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v5

    .line 2235
    .local v5, "result":Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 2238
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2239
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move v5, v6

    .line 2240
    goto :goto_0

    .line 2230
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .end local v5    # "result":Z
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v2

    .line 2231
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v7, "CropImage"

    const-string v8, "fail to save image: "

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2232
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2235
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move v5, v6

    goto :goto_0

    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_2
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v6

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 2230
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v2

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method private saveCloudImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Landroid/net/Uri;
    .locals 12
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "cropped"    # Landroid/graphics/Bitmap;
    .param p3, "image"    # Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    .prologue
    const/4 v11, 0x0

    const/4 v5, 0x0

    .line 2337
    sget-object v0, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2338
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "cannot create download folder"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2341
    :cond_0
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getName()Ljava/lang/String;

    move-result-object v4

    .line 2342
    .local v4, "filename":Ljava/lang/String;
    if-nez v4, :cond_2

    .line 2367
    :cond_1
    :goto_0
    return-object v5

    .line 2345
    :cond_2
    const/16 v0, 0x2e

    invoke-virtual {v4, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v9

    .line 2346
    .local v9, "pos":I
    if-ltz v9, :cond_3

    .line 2347
    invoke-virtual {v4, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 2349
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/gallery3d/app/CropImage;->changeExifData(Ljava/lang/String;II)V

    .line 2351
    sget-object v3, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/app/CropImage;->saveMedia(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/io/File;Ljava/lang/String;Lcom/sec/android/gallery3d/exif/ExifData;)Ljava/io/File;

    move-result-object v8

    .line 2352
    .local v8, "output":Ljava/io/File;
    if-eqz v8, :cond_1

    .line 2355
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long v6, v0, v2

    .line 2356
    .local v6, "now":J
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 2357
    .local v10, "values":Landroid/content/ContentValues;
    const-string/jumbo v0, "title"

    invoke-virtual {p3}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2358
    const-string v0, "_display_name"

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2359
    const-string v0, "datetaken"

    invoke-virtual {p3}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getDateInMs()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2360
    const-string v0, "date_modified"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2361
    const-string v0, "date_added"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2362
    const-string v0, "mime_type"

    const-string v1, "image/jpeg"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2363
    const-string v0, "orientation"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2364
    const-string v0, "_data"

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2365
    const-string v0, "_size"

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2367
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_0
.end method

.method private saveCropImageFile()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2408
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 2410
    .local v2, "extra":Landroid/os/Bundle;
    const/4 v0, 0x0

    .line 2411
    .local v0, "cropRect":Landroid/graphics/RectF;
    const/4 v1, 0x0

    .line 2412
    .local v1, "cropRectFace":Landroid/graphics/RectF;
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    if-eqz v4, :cond_1

    .line 2413
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v4, v4, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v4, :cond_0

    .line 2414
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v4, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalImage;->getArcWidth()I

    move-result v6

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v4, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalImage;->getArcHeight()I

    move-result v4

    invoke-virtual {v5, v6, v4}, Lcom/sec/android/gallery3d/ui/CropView;->getCropRectangleEx(II)Landroid/graphics/RectF;

    move-result-object v1

    .line 2417
    if-nez v1, :cond_1

    .line 2470
    :cond_0
    :goto_0
    return-void

    .line 2422
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/CropView;->getCropRectangle()Landroid/graphics/RectF;

    move-result-object v0

    .line 2424
    if-eqz v0, :cond_0

    .line 2427
    const/4 v4, 0x2

    iput v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mState:I

    .line 2428
    if-eqz v2, :cond_3

    const-string v4, "set-as-wallpaper"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "set-as-lockscreen"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "set-as-bothscreen"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    const v3, 0x7f0e003d

    .line 2433
    .local v3, "messageId":I
    :goto_1
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mPausedWhileSaving:Z

    if-eqz v4, :cond_4

    .line 2434
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/app/CropImage;->mPausedWhileSaving:Z

    .line 2440
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v4

    new-instance v5, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;

    invoke-direct {v5, p0, v0, v1}, Lcom/sec/android/gallery3d/app/CropImage$SaveOutput;-><init>(Lcom/sec/android/gallery3d/app/CropImage;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    new-instance v6, Lcom/sec/android/gallery3d/app/CropImage$9;

    invoke-direct {v6, p0}, Lcom/sec/android/gallery3d/app/CropImage$9;-><init>(Lcom/sec/android/gallery3d/app/CropImage;)V

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSaveTask:Lcom/sec/android/gallery3d/util/Future;

    goto :goto_0

    .line 2428
    .end local v3    # "messageId":I
    :cond_3
    const v3, 0x7f0e0037

    goto :goto_1

    .line 2436
    .restart local v3    # "messageId":I
    :cond_4
    const/4 v4, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/app/CropImage;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {p0, v4, v5, v6, v7}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    goto :goto_2
.end method

.method private saveFile(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "lockscreen"    # Landroid/graphics/Bitmap;
    .param p3, "savePath"    # Ljava/lang/String;
    .param p4, "fileName"    # Ljava/lang/String;
    .param p5, "settingsPath"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2273
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p3, p4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2275
    .local v0, "candidate":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2281
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2282
    :cond_0
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "cannot create file: saveLockScreenImage"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2276
    :catch_0
    move-exception v1

    .line 2277
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "CropImage"

    const-string v5, "fail to create new file: "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2306
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    :goto_0
    return-void

    .line 2285
    :cond_2
    invoke-virtual {v0, v6, v5}, Ljava/io/File;->setReadable(ZZ)Z

    .line 2286
    invoke-virtual {v0, v6, v5}, Ljava/io/File;->setWritable(ZZ)Z

    .line 2288
    const/4 v2, 0x0

    .line 2290
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2291
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-direct {p0, p1, p2, v4, v3}, Lcom/sec/android/gallery3d/app/CropImage;->saveBitmapToOutputStream(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/io/OutputStream;)Z

    .line 2292
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, p5, v5}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2299
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 2302
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2303
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 2294
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v1

    .line 2295
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "CropImage"

    const-string v5, "fail to save image: "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2296
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2299
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_2
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v4

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 2294
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v1

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method private saveGenericImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Landroid/net/Uri;
    .locals 12
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "cropped"    # Landroid/graphics/Bitmap;

    .prologue
    const-wide/16 v10, 0x3e8

    const/4 v5, 0x0

    .line 1229
    sget-object v0, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1230
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "cannot create download folder"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1233
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1234
    .local v6, "now":J
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "\'IMG\'_yyyyMMdd_HHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 1237
    .local v4, "filename":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/app/CropImage;->saveMedia(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/io/File;Ljava/lang/String;Lcom/sec/android/gallery3d/exif/ExifData;)Ljava/io/File;

    move-result-object v8

    .line 1238
    .local v8, "output":Ljava/io/File;
    if-nez v8, :cond_1

    .line 1253
    :goto_0
    return-object v5

    .line 1240
    :cond_1
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 1241
    .local v9, "values":Landroid/content/ContentValues;
    const-string/jumbo v0, "title"

    invoke-virtual {v9, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1242
    const-string v0, "_display_name"

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1243
    const-string v0, "datetaken"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1244
    const-string v0, "date_modified"

    div-long v2, v6, v10

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1245
    const-string v0, "date_added"

    div-long v2, v6, v10

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1246
    const-string v0, "mime_type"

    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getOutputMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1247
    const-string v0, "orientation"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1248
    const-string v0, "_data"

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1249
    const-string v0, "_size"

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1251
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v9, v0, v1}, Lcom/sec/android/gallery3d/app/CropImage;->setImageSize(Landroid/content/ContentValues;II)V

    .line 1253
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    goto :goto_0
.end method

.method private saveLocalImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Landroid/net/Uri;
    .locals 18
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "cropped"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1172
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 1174
    .local v8, "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    new-instance v12, Ljava/io/File;

    iget-object v2, v8, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-direct {v12, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1175
    .local v12, "oldPath":Ljava/io/File;
    new-instance v5, Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1177
    .local v5, "directory":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 1178
    .local v6, "filename":Ljava/lang/String;
    const/16 v2, 0x2e

    invoke-virtual {v6, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v14

    .line 1179
    .local v14, "pos":I
    if-ltz v14, :cond_0

    .line 1180
    const/4 v2, 0x0

    invoke-virtual {v6, v2, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 1182
    :cond_0
    const/4 v13, 0x0

    .line 1184
    .local v13, "output":Ljava/io/File;
    const/4 v7, 0x0

    .local v7, "exifData":Lcom/sec/android/gallery3d/exif/ExifData;
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    .line 1190
    invoke-direct/range {v2 .. v7}, Lcom/sec/android/gallery3d/app/CropImage;->saveMedia(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/io/File;Ljava/lang/String;Lcom/sec/android/gallery3d/exif/ExifData;)Ljava/io/File;

    move-result-object v13

    .line 1191
    if-nez v13, :cond_1

    const/4 v2, 0x0

    .line 1224
    :goto_0
    return-object v2

    .line 1195
    :cond_1
    iget-object v2, v8, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->copySoundDataToFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1197
    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    .line 1198
    .local v9, "newTitle":Ljava/lang/String;
    const/16 v2, 0x2e

    invoke-virtual {v9, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v14

    .line 1199
    if-ltz v14, :cond_2

    .line 1200
    const/4 v2, 0x0

    invoke-virtual {v9, v2, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 1203
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v16, 0x3e8

    div-long v10, v2, v16

    .line 1204
    .local v10, "now":J
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 1207
    .local v15, "values":Landroid/content/ContentValues;
    const-string/jumbo v2, "title"

    invoke-virtual {v15, v2, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1209
    const-string v2, "_display_name"

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    const-string v2, "datetaken"

    iget-wide v0, v8, Lcom/sec/android/gallery3d/data/LocalImage;->dateTakenInMs:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1211
    const-string v2, "date_modified"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1212
    const-string v2, "date_added"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1213
    const-string v2, "mime_type"

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/CropImage;->getOutputMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214
    const-string v2, "orientation"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1215
    const-string v2, "_data"

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    const-string v2, "_size"

    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1218
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v15, v2, v3}, Lcom/sec/android/gallery3d/app/CropImage;->setImageSize(Landroid/content/ContentValues;II)V

    .line 1220
    iget-wide v2, v8, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    iget-wide v0, v8, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-static {v2, v3, v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1221
    const-string v2, "latitude"

    iget-wide v0, v8, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1222
    const-string v2, "longitude"

    iget-wide v0, v8, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1224
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v15}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_0
.end method

.method private saveLockScreenImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)V
    .locals 10
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "lockscreen"    # Landroid/graphics/Bitmap;
    .param p3, "isRipple"    # Z

    .prologue
    .line 2246
    const-string v0, "CropImage"

    const-string v2, "save lockscreen image"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2247
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2248
    .local v9, "savePath":Ljava/lang/String;
    if-eqz p3, :cond_1

    const-string v6, "lockscreen_wallpaper_ripple.jpg"

    .line 2250
    .local v6, "fileName":Ljava/lang/String;
    :goto_0
    if-eqz p3, :cond_2

    const-string v7, "lockscreen_wallpaper_path_ripple"

    .line 2253
    .local v7, "settingsPath":Ljava/lang/String;
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->WALLPAPER_URI:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2254
    .local v1, "uri":Landroid/net/Uri;
    const/4 v8, 0x0

    .line 2256
    .local v8, "cr":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const-string v3, "isWallpaperChangeAllowed"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2257
    if-eqz v8, :cond_3

    .line 2258
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2259
    const-string v0, "isWallpaperChangeAllowed"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "false"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_3

    .line 2264
    if-eqz v8, :cond_0

    .line 2265
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2269
    :cond_0
    :goto_2
    return-void

    .line 2248
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "fileName":Ljava/lang/String;
    .end local v7    # "settingsPath":Ljava/lang/String;
    .end local v8    # "cr":Landroid/database/Cursor;
    :cond_1
    const-string v6, "lockscreen_wallpaper.jpg"

    goto :goto_0

    .line 2250
    .restart local v6    # "fileName":Ljava/lang/String;
    :cond_2
    const-string v7, "lockscreen_wallpaper_path"

    goto :goto_1

    .line 2264
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v7    # "settingsPath":Ljava/lang/String;
    .restart local v8    # "cr":Landroid/database/Cursor;
    :cond_3
    if-eqz v8, :cond_4

    .line 2265
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v9

    .line 2268
    invoke-direct/range {v2 .. v7}, Lcom/sec/android/gallery3d/app/CropImage;->saveFile(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2264
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_5

    .line 2265
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private saveMedia(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 6
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "cropped"    # Landroid/graphics/Bitmap;
    .param p3, "directory"    # Ljava/io/File;
    .param p4, "filename"    # Ljava/lang/String;

    .prologue
    .line 995
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/app/CropImage;->saveMedia(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/io/File;Ljava/lang/String;Lcom/sec/android/gallery3d/exif/ExifData;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private saveMedia(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/io/File;Ljava/lang/String;Lcom/sec/android/gallery3d/exif/ExifData;)Ljava/io/File;
    .locals 10
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "cropped"    # Landroid/graphics/Bitmap;
    .param p3, "directory"    # Ljava/io/File;
    .param p4, "filename"    # Ljava/lang/String;
    .param p5, "exifData"    # Lcom/sec/android/gallery3d/exif/ExifData;

    .prologue
    .line 1002
    const/4 v0, 0x0

    .line 1003
    .local v0, "candidate":Ljava/io/File;
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getFileExtension()Ljava/lang/String;

    move-result-object v3

    .line 1004
    .local v3, "fileExtension":Ljava/lang/String;
    const/4 v6, 0x1

    .local v6, "i":I
    :goto_0
    const/16 v7, 0x3e8

    if-ge v6, v7, :cond_0

    .line 1005
    new-instance v0, Ljava/io/File;

    .end local v0    # "candidate":Ljava/io/File;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, p3, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1008
    .restart local v0    # "candidate":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-eqz v7, :cond_2

    .line 1025
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v7

    if-nez v7, :cond_3

    .line 1026
    :cond_1
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "cannot create file: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1011
    :catch_0
    move-exception v1

    .line 1012
    .local v1, "e":Ljava/io/IOException;
    const-string v7, "CropImage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "fail to create new file: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1015
    new-instance v7, Lcom/sec/android/gallery3d/app/CropImage$5;

    invoke-direct {v7, p0}, Lcom/sec/android/gallery3d/app/CropImage$5;-><init>(Lcom/sec/android/gallery3d/app/CropImage;)V

    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/app/CropImage;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1022
    const/4 v7, 0x0

    .line 1064
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    return-object v7

    .line 1004
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1029
    :cond_3
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Ljava/io/File;->setReadable(ZZ)Z

    .line 1030
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Ljava/io/File;->setWritable(ZZ)Z

    .line 1032
    const/4 v4, 0x0

    .line 1035
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1036
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .local v5, "fos":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 1044
    .local v2, "eos":Lcom/sec/android/gallery3d/exif/ExifOutputStream;
    :try_start_2
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/app/CropImage;->convertExtensionToCompressFormat(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v7

    invoke-direct {p0, p1, p2, v7, v5}, Lcom/sec/android/gallery3d/app/CropImage;->saveBitmapToOutputStream(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/io/OutputStream;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1048
    :try_start_3
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1056
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 1059
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1060
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1061
    const/4 v7, 0x0

    goto :goto_1

    .line 1048
    :catchall_0
    move-exception v7

    :try_start_4
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v7
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1050
    :catch_1
    move-exception v1

    move-object v4, v5

    .line 1051
    .end local v2    # "eos":Lcom/sec/android/gallery3d/exif/ExifOutputStream;
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "e":Ljava/io/IOException;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :goto_2
    :try_start_5
    const-string v7, "CropImage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "fail to save image: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1053
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1054
    const/4 v7, 0x0

    .line 1056
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v1    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v7

    :goto_3
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v7

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "eos":Lcom/sec/android/gallery3d/exif/ExifOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :cond_4
    move-object v7, v0

    .line 1064
    goto :goto_1

    .line 1056
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 1050
    .end local v2    # "eos":Lcom/sec/android/gallery3d/exif/ExifOutputStream;
    :catch_2
    move-exception v1

    goto :goto_2
.end method

.method private savePicasaImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Landroid/net/Uri;
    .locals 15
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "cropped"    # Landroid/graphics/Bitmap;
    .param p3, "image"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .prologue
    .line 1131
    sget-object v0, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1132
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "cannot create download folder"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1134
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getName()Ljava/lang/String;

    move-result-object v4

    .line 1135
    .local v4, "filename":Ljava/lang/String;
    const/16 v0, 0x2e

    invoke-virtual {v4, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v13

    .line 1136
    .local v13, "pos":I
    if-ltz v13, :cond_1

    .line 1137
    const/4 v0, 0x0

    invoke-virtual {v4, v0, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1139
    :cond_1
    const/4 v5, 0x0

    .line 1145
    .local v5, "exifData":Lcom/sec/android/gallery3d/exif/ExifData;
    sget-object v3, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/app/CropImage;->saveMedia(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/io/File;Ljava/lang/String;Lcom/sec/android/gallery3d/exif/ExifData;)Ljava/io/File;

    move-result-object v12

    .line 1146
    .local v12, "output":Ljava/io/File;
    if-nez v12, :cond_2

    const/4 v0, 0x0

    .line 1167
    :goto_0
    return-object v0

    .line 1148
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long v10, v0, v2

    .line 1149
    .local v10, "now":J
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1150
    .local v14, "values":Landroid/content/ContentValues;
    const-string/jumbo v0, "title"

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1151
    const-string v0, "_display_name"

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    const-string v0, "datetaken"

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getDateInMs()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1153
    const-string v0, "date_modified"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1154
    const-string v0, "date_added"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1155
    const-string v0, "mime_type"

    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getOutputMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    const-string v0, "orientation"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1157
    const-string v0, "_data"

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    const-string v0, "_size"

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1159
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v14, v0, v1}, Lcom/sec/android/gallery3d/app/CropImage;->setImageSize(Landroid/content/ContentValues;II)V

    .line 1161
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getLatitude()D

    move-result-wide v6

    .line 1162
    .local v6, "latitude":D
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getLongitude()D

    move-result-wide v8

    .line 1163
    .local v8, "longitude":D
    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1164
    const-string v0, "latitude"

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1165
    const-string v0, "longitude"

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1167
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v14}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private saveSNSImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Landroid/net/Uri;
    .locals 12
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "cropped"    # Landroid/graphics/Bitmap;
    .param p3, "image"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .prologue
    const/4 v11, 0x0

    const/4 v5, 0x0

    .line 2309
    sget-object v0, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2310
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "cannot create download folder"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2313
    :cond_0
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getName()Ljava/lang/String;

    move-result-object v4

    .line 2314
    .local v4, "filename":Ljava/lang/String;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2332
    :cond_1
    :goto_0
    return-object v5

    .line 2315
    :cond_2
    const/16 v0, 0x2e

    invoke-virtual {v4, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v9

    .line 2316
    .local v9, "pos":I
    if-ltz v9, :cond_3

    invoke-virtual {v4, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 2317
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/gallery3d/app/CropImage;->changeExifData(Ljava/lang/String;II)V

    .line 2318
    sget-object v3, Lcom/sec/android/gallery3d/app/CropImage;->DOWNLOAD_BUCKET:Ljava/io/File;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/app/CropImage;->saveMedia(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Ljava/io/File;Ljava/lang/String;Lcom/sec/android/gallery3d/exif/ExifData;)Ljava/io/File;

    move-result-object v8

    .line 2319
    .local v8, "output":Ljava/io/File;
    if-eqz v8, :cond_1

    .line 2321
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long v6, v0, v2

    .line 2322
    .local v6, "now":J
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 2323
    .local v10, "values":Landroid/content/ContentValues;
    const-string/jumbo v0, "title"

    invoke-virtual {p3}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2324
    const-string v0, "_display_name"

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325
    const-string v0, "datetaken"

    invoke-virtual {p3}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getDateInMs()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2326
    const-string v0, "date_modified"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2327
    const-string v0, "date_added"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2328
    const-string v0, "mime_type"

    const-string v1, "image/jpeg"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2329
    const-string v0, "orientation"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2330
    const-string v0, "_data"

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2331
    const-string v0, "_size"

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2332
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_0
.end method

.method private saveToMediaProvider(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Landroid/net/Uri;
    .locals 1
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "cropped"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1106
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1107
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/gallery3d/app/CropImage;->savePicasaImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Landroid/net/Uri;

    move-result-object v0

    .line 1117
    :goto_0
    return-object v0

    .line 1108
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_1

    .line 1109
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/CropImage;->saveLocalImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 1111
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->isSNSImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1112
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/gallery3d/app/CropImage;->saveSNSImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 1113
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->isCloudImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1114
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/gallery3d/app/CropImage;->saveCloudImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 1117
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/CropImage;->saveGenericImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private sendResultToSCover()V
    .locals 5

    .prologue
    .line 2814
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "sview_color_wallpaper"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2815
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "s_view_cover_skin_color"

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->getCurrentColor()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2819
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.sviewcover.CHANGE_COVER_BACKGROUND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2821
    .local v1, "resultIntent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/CropImage;->sendBroadcast(Landroid/content/Intent;)V

    .line 2822
    return-void

    .line 2816
    .end local v1    # "resultIntent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 2817
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "CropImage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setActionBar()V
    .locals 12

    .prologue
    const v11, 0x7f0e02ab

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 628
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    if-eqz v6, :cond_0

    .line 629
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 630
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 656
    :goto_0
    return-void

    .line 632
    :cond_0
    const-string v6, "layout_inflater"

    invoke-virtual {p0, v6}, Lcom/sec/android/gallery3d/app/CropImage;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 633
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030012

    new-instance v7, Landroid/widget/LinearLayout;

    invoke-direct {v7, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v6, v7, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mActionBarButtons:Landroid/view/View;

    .line 634
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mActionBarButtons:Landroid/view/View;

    const v7, 0x7f0f0022

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 635
    .local v1, "cancelActionView":Landroid/view/View;
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mActionBarButtons:Landroid/view/View;

    const v7, 0x7f0f0023

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 636
    .local v0, "cancel":Landroid/widget/TextView;
    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 637
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mActionBarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 638
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mActionBarButtons:Landroid/view/View;

    const v7, 0x7f0f0024

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoneActionView:Landroid/view/View;

    .line 639
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mActionBarButtons:Landroid/view/View;

    const v7, 0x7f0f0025

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 640
    .local v2, "done":Landroid/widget/TextView;
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 641
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoneActionView:Landroid/view/View;

    iget-object v7, p0, Lcom/sec/android/gallery3d/app/CropImage;->mActionBarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 643
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getAndroidContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0e020a

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getAndroidContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 645
    .local v4, "sb":Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoneActionView:Landroid/view/View;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 647
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getAndroidContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0e0046

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getAndroidContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 649
    .local v5, "sb1":Ljava/lang/StringBuilder;
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 651
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/gallery3d/app/CropImage;->mActionBarButtons:Landroid/view/View;

    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 652
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 653
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 654
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    goto/16 :goto_0
.end method

.method private setAsWallpaper(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;)Z
    .locals 3
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "wallpaper"    # Landroid/graphics/Bitmap;

    .prologue
    .line 978
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mKnox:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsMultiSIM:Z

    if-eqz v1, :cond_1

    .line 979
    :cond_0
    invoke-static {p0, p2}, Lcom/sec/android/gallery3d/ds/DsWallpaperSetting;->setAsDsWallpaper(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    .line 991
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 982
    :cond_1
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/WallpaperManager;->setBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 985
    :catch_0
    move-exception v0

    .line 986
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "CropImage"

    const-string v2, "fail to set wall paper"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 988
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private setCropParameters()V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 1686
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 1687
    .local v2, "extras":Landroid/os/Bundle;
    if-nez v2, :cond_0

    .line 1708
    :goto_0
    return-void

    .line 1689
    :cond_0
    const-string v5, "aspectX"

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1690
    .local v0, "aspectX":I
    const-string v5, "aspectY"

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1691
    .local v1, "aspectY":I
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 1692
    if-eq v0, v1, :cond_1

    .line 1693
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoFaceDetection:Z

    .line 1694
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    int-to-float v6, v0

    int-to-float v7, v1

    div-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/ui/CropView;->setAspectRatio(F)V

    .line 1697
    :cond_2
    const-string/jumbo v5, "spotlightX"

    invoke-virtual {v2, v5, v8}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v3

    .line 1698
    .local v3, "spotlightX":F
    const-string/jumbo v5, "spotlightY"

    invoke-virtual {v2, v5, v8}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v4

    .line 1699
    .local v4, "spotlightY":F
    cmpl-float v5, v3, v8

    if-eqz v5, :cond_3

    cmpl-float v5, v4, v8

    if-eqz v5, :cond_3

    .line 1700
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v5, v3, v4}, Lcom/sec/android/gallery3d/ui/CropView;->setSpotlightRatio(FF)V

    .line 1703
    :cond_3
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-boolean v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/ui/CropView;->setIsWallpaper(Z)V

    .line 1704
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-boolean v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSNote:Z

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/ui/CropView;->setIsSNote(Z)V

    .line 1705
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-boolean v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mPhotoFrame:Z

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/ui/CropView;->setIsPhotoFrame(Z)V

    .line 1706
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-boolean v6, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSView:Z

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/ui/CropView;->setIsSView(Z)V

    goto :goto_0
.end method

.method private static setImageSize(Landroid/content/ContentValues;II)V
    .locals 2
    .param p0, "values"    # Landroid/content/ContentValues;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1124
    sget-boolean v0, Lcom/sec/android/gallery3d/common/ApiHelper;->HAS_MEDIA_COLUMNS_WIDTH_AND_HEIGHT:Z

    if-eqz v0, :cond_0

    .line 1125
    const-string/jumbo v0, "width"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1126
    const-string v0, "height"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1128
    :cond_0
    return-void
.end method

.method private setLockScreenSettingAndBroadCast()V
    .locals 5

    .prologue
    .line 2184
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2186
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper_transparent"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2189
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.gallery3d.LOCKSCREEN_IMAGE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2190
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/CropImage;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2194
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 2191
    :catch_0
    move-exception v0

    .line 2192
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "CropImage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setMenuEnable(Landroid/view/MenuItem;Z)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;
    .param p2, "enable"    # Z

    .prologue
    .line 2126
    if-nez p1, :cond_1

    .line 2134
    :cond_0
    :goto_0
    return-void

    .line 2129
    :cond_1
    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2130
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2131
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 2132
    if-eqz p2, :cond_2

    const/16 v1, 0xff

    :goto_1
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    :cond_2
    const/16 v1, 0x64

    goto :goto_1
.end method

.method private setTextOnlyButton(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 2111
    const v0, 0x7f0f0025

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/app/CropImage;->setTextOnlyButton(Landroid/view/Menu;IZ)V

    .line 2112
    return-void
.end method

.method private setTextOnlyButton(Landroid/view/Menu;IZ)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "itemId"    # I
    .param p3, "isTextOnly"    # Z

    .prologue
    .line 2115
    invoke-interface {p1, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2116
    .local v0, "menuItem":Landroid/view/MenuItem;
    if-nez v0, :cond_0

    .line 2123
    :goto_0
    return-void

    .line 2119
    :cond_0
    if-eqz p3, :cond_1

    .line 2120
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 2122
    :cond_1
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0
.end method


# virtual methods
.method public getCropRectangle(Landroid/graphics/RectF;)Landroid/graphics/Rect;
    .locals 8
    .param p1, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 2805
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v2

    .line 2806
    .local v2, "w":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v0

    .line 2807
    .local v0, "h":I
    new-instance v1, Landroid/graphics/Rect;

    iget v3, p1, Landroid/graphics/RectF;->left:F

    int-to-float v4, v2

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget v4, p1, Landroid/graphics/RectF;->top:F

    int-to-float v5, v0

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iget v5, p1, Landroid/graphics/RectF;->right:F

    int-to-float v6, v2

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    iget v6, p1, Landroid/graphics/RectF;->bottom:F

    int-to-float v7, v0

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-direct {v1, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2809
    .local v1, "result":Landroid/graphics/Rect;
    return-object v1
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 717
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->finish()V

    .line 718
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2078
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2080
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsCallerId:Z

    if-eqz v0, :cond_2

    .line 2088
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mActionBarButtons:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    if-nez v0, :cond_0

    .line 2089
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->setActionBar()V

    .line 2091
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsDoneActionViewEnabled:Z

    if-nez v0, :cond_1

    .line 2092
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoneActionView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2093
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mDoneActionView:Landroid/view/View;

    const v1, 0x3f19999a    # 0.6f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 2096
    :cond_1
    return-void

    .line 2082
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    if-eqz v0, :cond_3

    .line 2083
    const v0, 0x7f0e00c9

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/CropImage;->setTitle(I)V

    goto :goto_0

    .line 2085
    :cond_3
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/CropImage;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 391
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onCreate(Landroid/os/Bundle;)V

    .line 393
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v2, :cond_b

    .line 394
    const v2, 0x7f110039

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/app/CropImage;->setTheme(I)V

    .line 399
    :goto_0
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/app/CropImage;->requestWindowFeature(I)Z

    .line 402
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 403
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v5, 0x400

    const/16 v6, 0x400

    invoke-virtual {v2, v5, v6}, Landroid/view/Window;->setFlags(II)V

    .line 405
    :cond_0
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 407
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getMediaEjectFilter()Landroid/content/IntentFilter;

    move-result-object v5

    invoke-virtual {p0, v2, v5}, Lcom/sec/android/gallery3d/app/CropImage;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 409
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsTablet:Z

    .line 412
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 413
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/app/CropImage;->mKnox:Z

    .line 417
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 418
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSrcUri:Ljava/lang/String;

    .line 423
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 424
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_5

    .line 425
    const-string v2, "set-as-wallpaper"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "set-as-lockscreen"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "set-as-bothscreen"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_c

    :cond_3
    move v2, v4

    :goto_1
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mWallpaper:Z

    .line 429
    const-string v2, "photo-frame"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mPhotoFrame:Z

    .line 431
    const-string/jumbo v2, "set_snote"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "set-as-image"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_4
    move v2, v4

    :goto_2
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSNote:Z

    .line 434
    const-string/jumbo v2, "set_sview"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSView:Z

    .line 435
    const-string v2, "need_wallpaper_layout"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mNeedWallpaperLayout:Z

    .line 436
    const-string v2, "crop_and_delete"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsCropAndDelete:Z

    .line 437
    const-string v2, "is_slink"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsSlinkCrop:Z

    .line 440
    :cond_5
    const v2, 0x7f030044

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/app/CropImage;->setContentView(I)V

    .line 441
    new-instance v2, Lcom/sec/android/gallery3d/ui/CropView;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/ui/CropView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    .line 442
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-interface {v2, v5}, Lcom/sec/android/gallery3d/ui/GLRoot;->setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 453
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v2, :cond_8

    if-eqz v0, :cond_8

    .line 454
    const-string v2, "is-caller-id"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsCallerId:Z

    .line 455
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsCallerId:Z

    if-eqz v2, :cond_6

    .line 456
    const-string v2, "face-position"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v1

    .line 457
    .local v1, "ia":[F
    if-eqz v1, :cond_6

    array-length v2, v1

    if-ne v2, v11, :cond_6

    .line 458
    new-instance v2, Landroid/graphics/RectF;

    aget v5, v1, v3

    aget v6, v1, v4

    aget v7, v1, v9

    aget v8, v1, v10

    invoke-direct {v2, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mFaceRect:Landroid/graphics/RectF;

    .line 462
    .end local v1    # "ia":[F
    :cond_6
    const-string v2, "is-manual-fd"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    .line 463
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    if-eqz v2, :cond_8

    .line 464
    const v2, 0x7f0e00c9

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/app/CropImage;->setTitle(I)V

    .line 466
    const-string v2, "face-position"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v1

    .line 467
    .restart local v1    # "ia":[F
    if-eqz v1, :cond_7

    array-length v2, v1

    if-ne v2, v11, :cond_7

    .line 468
    new-instance v2, Landroid/graphics/RectF;

    aget v3, v1, v3

    aget v4, v1, v4

    aget v5, v1, v9

    aget v6, v1, v10

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mFaceRect:Landroid/graphics/RectF;

    .line 470
    :cond_7
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/CropView;->setManualFD(Z)V

    .line 474
    .end local v1    # "ia":[F
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->setActionBar()V

    .line 477
    new-instance v2, Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    .line 479
    new-instance v2, Lcom/sec/android/gallery3d/app/CropImage$3;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/sec/android/gallery3d/app/CropImage$3;-><init>(Lcom/sec/android/gallery3d/app/CropImage;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMainHandler:Landroid/os/Handler;

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 618
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "lookupKeyFromContactPopup"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->lookupkeytoPass:Ljava/lang/String;

    .line 619
    :cond_9
    if-eqz p1, :cond_a

    .line 620
    const-string v2, "lookUpOnRecreate"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->lookUpKeyAfterRecreate:Ljava/lang/String;

    .line 622
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->setCropParameters()V

    .line 624
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage;->CROP:Ljava/lang/String;

    invoke-static {v2, v3, p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 625
    return-void

    .line 396
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_b
    const v2, 0x7f11002f

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/app/CropImage;->setTheme(I)V

    goto/16 :goto_0

    .restart local v0    # "extras":Landroid/os/Bundle;
    :cond_c
    move v2, v3

    .line 425
    goto/16 :goto_1

    :cond_d
    move v2, v3

    .line 431
    goto/16 :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x1

    .line 681
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 682
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    if-nez v0, :cond_0

    .line 688
    :goto_0
    return v2

    .line 684
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120004

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 685
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMenu:Landroid/view/Menu;

    .line 686
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f0025

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsSaveEnable:Z

    invoke-direct {p0, v0, v1}, Lcom/sec/android/gallery3d/app/CropImage;->setMenuEnable(Landroid/view/MenuItem;Z)V

    .line 687
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMenu:Landroid/view/Menu;

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/CropImage;->setTextOnlyButton(Landroid/view/Menu;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1919
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onDestroy()V

    .line 1920
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    if-eqz v0, :cond_0

    .line 1921
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;->recycle()V

    .line 1922
    iput-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    .line 1925
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    if-eqz v0, :cond_1

    .line 1926
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->recycle()V

    .line 1927
    iput-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 1930
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->destroy()V

    .line 1932
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/CropImage;->dismissDialog(Landroid/app/Dialog;)V

    .line 1933
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/CropImage;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1935
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2100
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x14

    if-ne v1, v2, :cond_0

    .line 2101
    const v1, 0x7f0f00c0

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2102
    .local v0, "btnSave":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 2103
    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 2104
    const/4 v1, 0x1

    .line 2107
    .end local v0    # "btnSave":Landroid/widget/TextView;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 699
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mIsManualFD:Z

    if-nez v0, :cond_0

    .line 712
    :goto_0
    return v2

    .line 701
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 703
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMenuHandler:Landroid/os/Handler;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 706
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage;->mMenuHandler:Landroid/os/Handler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 701
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f0025 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 1876
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onPause()V

    .line 1892
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/CropImage;->cancelTaskAndDismissProgressDialog(Lcom/sec/android/gallery3d/util/Future;)V

    .line 1893
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mLoadBitmapTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/CropImage;->cancelTaskAndDismissProgressDialog(Lcom/sec/android/gallery3d/util/Future;)V

    .line 1896
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mSaveTask:Lcom/sec/android/gallery3d/util/Future;

    .line 1897
    .local v1, "saveTask":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Landroid/content/Intent;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/sec/android/gallery3d/util/Future;->isDone()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1899
    invoke-interface {v1}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 1900
    invoke-interface {v1}, Lcom/sec/android/gallery3d/util/Future;->waitDone()V

    .line 1902
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mPausedWhileSaving:Z

    .line 1905
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    .line 1906
    .local v0, "root":Lcom/sec/android/gallery3d/ui/GLRoot;
    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->lockRenderThread()V

    .line 1908
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->releaseInstance()V

    .line 1911
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView;->pause()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1913
    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    .line 1915
    return-void

    .line 1913
    :catchall_0
    move-exception v2

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    throw v2
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 693
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 694
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 1852
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onResume()V

    .line 1853
    iget v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mState:I

    if-nez v2, :cond_0

    .line 1854
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->initializeData()V

    .line 1856
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1857
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CropImage;->onSaveClicked()V

    .line 1860
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->resume()V

    .line 1861
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-interface {v2, v3}, Lcom/sec/android/gallery3d/ui/GLRoot;->setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 1863
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    .line 1864
    .local v1, "root":Lcom/sec/android/gallery3d/ui/GLRoot;
    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->lockRenderThread()V

    .line 1866
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CropImage;->mCropView:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView;->resume()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1870
    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    .line 1872
    :goto_0
    return-void

    .line 1867
    :catch_0
    move-exception v0

    .line 1868
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1870
    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    throw v2
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "saveState"    # Landroid/os/Bundle;

    .prologue
    .line 675
    const-string/jumbo v0, "state"

    iget v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 676
    const-string v0, "lookUpOnRecreate"

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CropImage;->lookupkeytoPass:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    return-void
.end method
