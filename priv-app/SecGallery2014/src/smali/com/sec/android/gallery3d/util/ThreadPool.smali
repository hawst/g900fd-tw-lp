.class public Lcom/sec/android/gallery3d/util/ThreadPool;
.super Ljava/lang/Object;
.source "ThreadPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/util/ThreadPool$1;,
        Lcom/sec/android/gallery3d/util/ThreadPool$Worker;,
        Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;,
        Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;,
        Lcom/sec/android/gallery3d/util/ThreadPool$JobContextStub;,
        Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;,
        Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    }
.end annotation


# static fields
.field private static final CORE_POOL_SIZE:I = 0x4

.field public static final JOB_CONTEXT_STUB:Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

.field private static final KEEP_ALIVE_TIME:I = 0xa

.field private static final MAX_POOL_SIZE:I = 0x6

.field public static final MODE_CPU:I = 0x1

.field public static final MODE_NETWORK:I = 0x2

.field public static final MODE_NONE:I = 0x0

.field private static final NETWORK_POOL_SIZE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ThreadPool"


# instance fields
.field mCpuCounter:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field mNetworkCounter:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

.field private final mNetworkExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/sec/android/gallery3d/util/ThreadPool$JobContextStub;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContextStub;-><init>(Lcom/sec/android/gallery3d/util/ThreadPool$1;)V

    sput-object v0, Lcom/sec/android/gallery3d/util/ThreadPool;->JOB_CONTEXT_STUB:Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 91
    const/4 v0, 0x4

    const/4 v1, 0x6

    invoke-direct {p0, v0, v1}, Lcom/sec/android/gallery3d/util/ThreadPool;-><init>(II)V

    .line 92
    return-void
.end method

.method public constructor <init>(II)V
    .locals 11
    .param p1, "initPoolSize"    # I
    .param p2, "maxPoolSize"    # I

    .prologue
    const-wide/16 v4, 0xa

    const/16 v10, 0xa

    const/4 v9, 0x2

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool;->mCpuCounter:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    .line 41
    new-instance v0, Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    invoke-direct {v0, v9}, Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool;->mNetworkCounter:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    .line 95
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, Lcom/sec/android/gallery3d/util/PriorityThreadFactory;

    const-string/jumbo v0, "thread-pool"

    invoke-direct {v8, v0, v10}, Lcom/sec/android/gallery3d/util/PriorityThreadFactory;-><init>(Ljava/lang/String;I)V

    move v2, p1

    move v3, p2

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/util/ThreadPool;->mExecutor:Ljava/util/concurrent/Executor;

    .line 101
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, Lcom/sec/android/gallery3d/util/PriorityThreadFactory;

    const-string v0, "network-thread-pool"

    invoke-direct {v8, v0, v10}, Lcom/sec/android/gallery3d/util/PriorityThreadFactory;-><init>(Ljava/lang/String;I)V

    move v2, v9

    move v3, v9

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/util/ThreadPool;->mNetworkExecutor:Ljava/util/concurrent/Executor;

    .line 106
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/util/ThreadPool;Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/util/FutureListener;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submitToNetworkPool(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    return-object v0
.end method

.method private submitToNetworkPool(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<TT;>;",
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<TT;>;)",
            "Lcom/sec/android/gallery3d/util/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 157
    .local p1, "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<TT;>;"
    .local p2, "listener":Lcom/sec/android/gallery3d/util/FutureListener;, "Lcom/sec/android/gallery3d/util/FutureListener<TT;>;"
    new-instance v0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;-><init>(Lcom/sec/android/gallery3d/util/ThreadPool;Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;Z)V

    .line 158
    .local v0, "w":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ThreadPool;->mNetworkExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 159
    return-object v0
.end method


# virtual methods
.method public getQueuedCount()I
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool;->mExecutor:Ljava/util/concurrent/Executor;

    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    .line 151
    .local v0, "excutor":Ljava/util/concurrent/ThreadPoolExecutor;
    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v1

    .line 153
    .local v1, "queue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Ljava/lang/Runnable;>;"
    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v2

    return v2
.end method

.method public submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<TT;>;)",
            "Lcom/sec/android/gallery3d/util/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    return-object v0
.end method

.method public submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<TT;>;",
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<TT;>;)",
            "Lcom/sec/android/gallery3d/util/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<TT;>;"
    .local p2, "listener":Lcom/sec/android/gallery3d/util/FutureListener;, "Lcom/sec/android/gallery3d/util/FutureListener<TT;>;"
    new-instance v0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;-><init>(Lcom/sec/android/gallery3d/util/ThreadPool;Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)V

    .line 112
    .local v0, "w":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ThreadPool;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 113
    return-object v0
.end method

.method public submitToLast(Lcom/sec/android/gallery3d/util/Future;)Lcom/sec/android/gallery3d/util/Future;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<TT;>;)",
            "Lcom/sec/android/gallery3d/util/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<TT;>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool;->mExecutor:Ljava/util/concurrent/Executor;

    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    .line 139
    .local v0, "excutor":Ljava/util/concurrent/ThreadPoolExecutor;
    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v1

    .local v1, "queue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Ljava/lang/Runnable;>;"
    move-object v2, p1

    .line 140
    check-cast v2, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;

    .line 142
    .local v2, "w":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    invoke-interface {v1, v2}, Ljava/util/concurrent/BlockingQueue;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 143
    invoke-virtual {v0, v2}, Ljava/util/concurrent/ThreadPoolExecutor;->remove(Ljava/lang/Runnable;)Z

    .line 144
    invoke-virtual {v0, v2}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 146
    :cond_0
    return-object v2
.end method

.method public submitUrgent(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<TT;>;",
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<TT;>;)",
            "Lcom/sec/android/gallery3d/util/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<TT;>;"
    .local p2, "listener":Lcom/sec/android/gallery3d/util/FutureListener;, "Lcom/sec/android/gallery3d/util/FutureListener<TT;>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool;->mExecutor:Ljava/util/concurrent/Executor;

    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    .line 122
    .local v0, "excutor":Ljava/util/concurrent/ThreadPoolExecutor;
    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v2

    .line 125
    .local v2, "queue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Ljava/lang/Runnable;>;"
    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v4

    .line 126
    .local v4, "size":I
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v3

    .line 127
    .local v3, "res":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<TT;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_1

    .line 128
    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;

    .line 129
    .local v5, "w":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    if-eqz v5, :cond_0

    .line 130
    invoke-virtual {v0, v5}, Ljava/util/concurrent/ThreadPoolExecutor;->remove(Ljava/lang/Runnable;)Z

    .line 131
    invoke-virtual {v0, v5}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 127
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 134
    .end local v5    # "w":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    :cond_1
    return-object v3
.end method
