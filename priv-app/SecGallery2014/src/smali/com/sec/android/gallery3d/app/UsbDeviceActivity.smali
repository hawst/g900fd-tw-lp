.class public final Lcom/sec/android/gallery3d/app/UsbDeviceActivity;
.super Landroid/app/Activity;
.source "UsbDeviceActivity.java"


# static fields
.field public static final ACTION:Ljava/lang/String; = "dummyaction.MTP_CONNECTED"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/android/gallery3d/app/UsbDeviceActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/UsbDeviceActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    sget-object v2, Lcom/sec/android/gallery3d/app/UsbDeviceActivity;->TAG:Ljava/lang/String;

    const-string v3, "onCreate() - received usb device attached"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "dummyaction.MTP_CONNECTED"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x14000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    .line 44
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/UsbDeviceActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "mtpMode"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 45
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/UsbDeviceActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/UsbDeviceActivity;->finish()V

    .line 50
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/android/gallery3d/app/UsbDeviceActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "unable to start Gallery activity"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
