.class public Lcom/sec/android/gallery3d/ui/SlotView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "SlotView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/SlotView$IntegerAnimation;,
        Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;,
        Lcom/sec/android/gallery3d/ui/SlotView$Layout;,
        Lcom/sec/android/gallery3d/ui/SlotView$Spec;,
        Lcom/sec/android/gallery3d/ui/SlotView$ScatteringAnimation;,
        Lcom/sec/android/gallery3d/ui/SlotView$RisingAnimation;,
        Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;,
        Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;,
        Lcom/sec/android/gallery3d/ui/SlotView$SimpleListener;,
        Lcom/sec/android/gallery3d/ui/SlotView$Listener;
    }
.end annotation


# static fields
.field public static final INDEX_NONE:I = -0x1

.field public static final OVERSCROLL_3D:I = 0x0

.field public static final OVERSCROLL_NONE:I = 0x2

.field public static final OVERSCROLL_SYSTEM:I = 0x1

.field public static final RENDER_MORE_FRAME:I = 0x2

.field public static final RENDER_MORE_PASS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SlotView"


# instance fields
.field protected WIDE:Z

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field protected mAlignCenter:Z

.field private mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

.field private mDownInScrolling:Z

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private final mHandler:Landroid/os/Handler;

.field private final mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

.field private mListener:Lcom/sec/android/gallery3d/ui/SlotView$Listener;

.field private mMoreAnimation:Z

.field private mOverscrollEffect:I

.field private final mPaper:Lcom/sec/android/gallery3d/ui/Paper;

.field private mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

.field private mRequestRenderSlots:[I

.field private final mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

.field private mStartIndex:I

.field private final mTempRect:Landroid/graphics/Rect;

.field private mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/SlotView$Spec;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "spec"    # Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 108
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 35
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    .line 45
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mAlignCenter:Z

    .line 79
    new-instance v0, Lcom/sec/android/gallery3d/ui/Paper;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/Paper;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mPaper:Lcom/sec/android/gallery3d/ui/Paper;

    .line 84
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mMoreAnimation:Z

    .line 85
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    .line 86
    new-instance v0, Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;-><init>(Lcom/sec/android/gallery3d/ui/SlotView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    .line 87
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mStartIndex:I

    .line 91
    iput v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mOverscrollEffect:I

    .line 96
    const/16 v0, 0x10

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mRequestRenderSlots:[I

    .line 103
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mTempRect:Landroid/graphics/Rect;

    .line 109
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;-><init>(Lcom/sec/android/gallery3d/ui/SlotView;Lcom/sec/android/gallery3d/ui/SlotView$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 110
    new-instance v0, Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    .line 111
    new-instance v0, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;-><init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mHandler:Landroid/os/Handler;

    .line 112
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/ui/SlotView;->setSlotSpec(Lcom/sec/android/gallery3d/ui/SlotView$Spec;)V

    .line 114
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 116
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/ScrollerHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/gallery3d/ui/SlotView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mOverscrollEffect:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/Paper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mPaper:Lcom/sec/android/gallery3d/ui/Paper;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/gallery3d/ui/SlotView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView;

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mDownInScrolling:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$Layout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mListener:Lcom/sec/android/gallery3d/ui/SlotView$Listener;

    return-object v0
.end method

.method private static expandIntArray([II)[I
    .locals 1
    .param p0, "array"    # [I
    .param p1, "capacity"    # I

    .prologue
    .line 252
    :goto_0
    array-length v0, p0

    if-ge v0, p1, :cond_0

    .line 253
    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    new-array p0, v0, [I

    goto :goto_0

    .line 255
    :cond_0
    return-object p0
.end method

.method private renderItem(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIZ)I
    .locals 8
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "index"    # I
    .param p3, "pass"    # I
    .param p4, "paperActive"    # Z

    .prologue
    .line 345
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p2, v1}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getSlotRect(ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v6

    .line 347
    .local v6, "rect":Landroid/graphics/Rect;
    if-eqz p4, :cond_1

    .line 348
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mPaper:Lcom/sec/android/gallery3d/ui/Paper;

    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollX:I

    int-to-float v1, v1

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/gallery3d/ui/Paper;->getTransform(Landroid/graphics/Rect;F)[F

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->multiplyMatrix([FI)V

    .line 352
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    invoke-virtual {v0, p1, p2, v6}, Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;->apply(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ILandroid/graphics/Rect;)V

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    iget v1, v6, Landroid/graphics/Rect;->right:I

    iget v2, v6, Landroid/graphics/Rect;->left:I

    sub-int v4, v1, v2

    iget v1, v6, Landroid/graphics/Rect;->bottom:I

    iget v2, v6, Landroid/graphics/Rect;->top:I

    sub-int v5, v1, v2

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;->renderSlot(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)I

    move-result v7

    .line 357
    .local v7, "result":I
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 358
    return v7

    .line 350
    .end local v7    # "result":I
    :cond_1
    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget v1, v6, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FFF)V

    goto :goto_0
.end method

.method private updateScrollPosition(IZ)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "force"    # Z

    .prologue
    .line 202
    if-nez p2, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollX:I

    if-ne p1, v0, :cond_2

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollY:I

    if-eq p1, v0, :cond_0

    .line 203
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v0, :cond_3

    .line 204
    iput p1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollX:I

    .line 208
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->setScrollPosition(I)V

    .line 209
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/SlotView;->onScrollPositionChanged(I)V

    goto :goto_0

    .line 206
    :cond_3
    iput p1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollY:I

    goto :goto_1
.end method


# virtual methods
.method public addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V
    .locals 1
    .param p1, "view"    # Lcom/sec/android/gallery3d/ui/GLView;

    .prologue
    .line 170
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getScrollX()I
    .locals 1

    .prologue
    .line 805
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollX:I

    return v0
.end method

.method public getScrollY()I
    .locals 1

    .prologue
    .line 809
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollY:I

    return v0
.end method

.method public getSlotIndexByPosition(FF)I
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 866
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getSlotIndexByPosition(FF)I

    move-result v0

    return v0
.end method

.method public getSlotRect(I)Landroid/graphics/Rect;
    .locals 2
    .param p1, "slotIndex"    # I

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getSlotRect(ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getSlotRect(ILcom/sec/android/gallery3d/ui/GLView;)Landroid/graphics/Rect;
    .locals 5
    .param p1, "slotIndex"    # I
    .param p2, "rootPane"    # Lcom/sec/android/gallery3d/ui/GLView;

    .prologue
    .line 814
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 815
    .local v0, "offset":Landroid/graphics/Rect;
    invoke-virtual {p2, p0, v0}, Lcom/sec/android/gallery3d/ui/GLView;->getBoundsOf(Lcom/sec/android/gallery3d/ui/GLView;Landroid/graphics/Rect;)Z

    .line 816
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/SlotView;->getSlotRect(I)Landroid/graphics/Rect;

    move-result-object v1

    .line 817
    .local v1, "r":Landroid/graphics/Rect;
    iget v2, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SlotView;->getScrollX()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SlotView;->getScrollY()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 819
    return-object v1
.end method

.method public getVisibleEnd()I
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getVisibleEnd()I

    move-result v0

    return v0
.end method

.method public getVisibleStart()I
    .locals 1

    .prologue
    .line 797
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getVisibleStart()I

    move-result v0

    return v0
.end method

.method public makeSlotVisible(I)V
    .locals 9
    .param p1, "index"    # I

    .prologue
    .line 139
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v7, p1, v8}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getSlotRect(ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v1

    .line 140
    .local v1, "rect":Landroid/graphics/Rect;
    iget-boolean v7, p0, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v7, :cond_1

    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollX:I

    .line 141
    .local v4, "visibleBegin":I
    :goto_0
    iget-boolean v7, p0, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SlotView;->getWidth()I

    move-result v6

    .line 142
    .local v6, "visibleLength":I
    :goto_1
    add-int v5, v4, v6

    .line 143
    .local v5, "visibleEnd":I
    iget-boolean v7, p0, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v7, :cond_3

    iget v2, v1, Landroid/graphics/Rect;->left:I

    .line 144
    .local v2, "slotBegin":I
    :goto_2
    iget-boolean v7, p0, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v7, :cond_4

    iget v3, v1, Landroid/graphics/Rect;->right:I

    .line 146
    .local v3, "slotEnd":I
    :goto_3
    move v0, v4

    .line 147
    .local v0, "position":I
    sub-int v7, v3, v2

    if-ge v6, v7, :cond_5

    .line 148
    move v0, v4

    .line 155
    :cond_0
    :goto_4
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/SlotView;->setScrollPosition(I)V

    .line 156
    return-void

    .line 140
    .end local v0    # "position":I
    .end local v2    # "slotBegin":I
    .end local v3    # "slotEnd":I
    .end local v4    # "visibleBegin":I
    .end local v5    # "visibleEnd":I
    .end local v6    # "visibleLength":I
    :cond_1
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollY:I

    goto :goto_0

    .line 141
    .restart local v4    # "visibleBegin":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SlotView;->getHeight()I

    move-result v6

    goto :goto_1

    .line 143
    .restart local v5    # "visibleEnd":I
    .restart local v6    # "visibleLength":I
    :cond_3
    iget v2, v1, Landroid/graphics/Rect;->top:I

    goto :goto_2

    .line 144
    .restart local v2    # "slotBegin":I
    :cond_4
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_3

    .line 149
    .restart local v0    # "position":I
    .restart local v3    # "slotEnd":I
    :cond_5
    if-ge v2, v4, :cond_6

    .line 150
    move v0, v2

    goto :goto_4

    .line 151
    :cond_6
    if-le v3, v5, :cond_0

    .line 152
    sub-int v0, v3, v6

    goto :goto_4
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changeSize"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 175
    if-nez p1, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getVisibleStart()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getVisibleEnd()I

    move-result v2

    add-int/2addr v1, v2

    div-int/lit8 v0, v1, 0x2

    .line 182
    .local v0, "visibleIndex":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    sub-int v2, p4, p2

    sub-int v3, p5, p3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->setSize(II)V

    .line 183
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/SlotView;->makeSlotVisible(I)V

    .line 184
    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mOverscrollEffect:I

    if-nez v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mPaper:Lcom/sec/android/gallery3d/ui/Paper;

    sub-int v2, p4, p2

    sub-int v3, p5, p3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/Paper;->setSize(II)V

    goto :goto_0
.end method

.method protected onScrollPositionChanged(I)V
    .locals 2
    .param p1, "newPosition"    # I

    .prologue
    .line 213
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getScrollLimit()I

    move-result v0

    .line 214
    .local v0, "limit":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mListener:Lcom/sec/android/gallery3d/ui/SlotView$Listener;

    invoke-interface {v1, p1, v0}, Lcom/sec/android/gallery3d/ui/SlotView$Listener;->onScrollPositionChanged(II)V

    .line 215
    return-void
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 223
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserInteraction()V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 225
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 235
    :goto_0
    return v1

    .line 227
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mDownInScrolling:Z

    .line 228
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->forceFinished()V

    goto :goto_0

    .line 227
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 231
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mPaper:Lcom/sec/android/gallery3d/ui/Paper;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/Paper;->onRelease()V

    .line 232
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    goto :goto_0

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 26
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 262
    invoke-super/range {p0 .. p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    move-object/from16 v23, v0

    if-nez v23, :cond_0

    .line 341
    :goto_0
    return-void

    .line 265
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;->prepareDrawing()V

    .line 267
    invoke-static {}, Lcom/sec/android/gallery3d/ui/AnimationTime;->get()J

    move-result-wide v6

    .line 268
    .local v6, "animTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->advanceAnimation(J)Z

    move-result v10

    .line 269
    .local v10, "more":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->advanceAnimation(J)Z

    move-result v23

    or-int v10, v10, v23

    .line 270
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollX:I

    .line 271
    .local v14, "oldX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->getPosition()I

    move-result v23

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/SlotView;->updateScrollPosition(IZ)V

    .line 272
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->getPosition()I

    move-result v17

    .line 273
    .local v17, "position":I
    if-ltz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getScrollLimit()I

    move-result v23

    move/from16 v0, v23

    move/from16 v1, v17

    if-ge v0, v1, :cond_2

    .line 274
    :cond_1
    if-gez v17, :cond_b

    const/4 v4, 0x0

    .line 275
    .local v4, "adjust":I
    :goto_1
    const/16 v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v4, v1}, Lcom/sec/android/gallery3d/ui/SlotView;->updateScrollPosition(IZ)V

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    move-object/from16 v23, v0

    if-eqz v23, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;->isActive()Z

    move-result v23

    if-eqz v23, :cond_2

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;->forceStop()V

    .line 281
    .end local v4    # "adjust":I
    :cond_2
    const/4 v15, 0x0

    .line 282
    .local v15, "paperActive":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mOverscrollEffect:I

    move/from16 v23, v0

    if-nez v23, :cond_7

    .line 284
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollX:I

    .line 285
    .local v13, "newX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getScrollLimit()I

    move-result v8

    .line 286
    .local v8, "limit":I
    if-lez v14, :cond_3

    if-eqz v13, :cond_4

    :cond_3
    if-ge v14, v8, :cond_6

    if-ne v13, v8, :cond_6

    .line 287
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->getCurrVelocity()F

    move-result v22

    .line 288
    .local v22, "v":F
    if-ne v13, v8, :cond_5

    move/from16 v0, v22

    neg-float v0, v0

    move/from16 v22, v0

    .line 291
    :cond_5
    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->isNaN(F)Z

    move-result v23

    if-nez v23, :cond_6

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mPaper:Lcom/sec/android/gallery3d/ui/Paper;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/Paper;->edgeReached(F)V

    .line 295
    .end local v22    # "v":F
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mPaper:Lcom/sec/android/gallery3d/ui/Paper;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/Paper;->advanceAnimation()Z

    move-result v15

    .line 298
    .end local v8    # "limit":I
    .end local v13    # "newX":I
    :cond_7
    or-int/2addr v10, v15

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    move-object/from16 v23, v0

    if-eqz v23, :cond_8

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;->calculate(J)Z

    move-result v23

    or-int v10, v10, v23

    .line 304
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollX:I

    move/from16 v23, v0

    move/from16 v0, v23

    neg-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollY:I

    move/from16 v24, v0

    move/from16 v0, v24

    neg-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-interface {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 306
    const/16 v19, 0x0

    .line 307
    .local v19, "requestCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mRequestRenderSlots:[I

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleEnd:I
    invoke-static/range {v24 .. v24}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->access$400(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleStart:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->access$500(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I

    move-result v25

    sub-int v24, v24, v25

    invoke-static/range {v23 .. v24}, Lcom/sec/android/gallery3d/ui/SlotView;->expandIntArray([II)[I

    move-result-object v21

    .line 310
    .local v21, "requestedSlot":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleEnd:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->access$400(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I

    move-result v23

    add-int/lit8 v5, v23, -0x1

    .local v5, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mVisibleStart:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->access$500(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I

    move-result v23

    move/from16 v0, v23

    if-lt v5, v0, :cond_c

    .line 311
    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v23

    invoke-direct {v0, v1, v5, v2, v15}, Lcom/sec/android/gallery3d/ui/SlotView;->renderItem(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIZ)I

    move-result v18

    .line 312
    .local v18, "r":I
    and-int/lit8 v23, v18, 0x2

    if-eqz v23, :cond_9

    const/4 v10, 0x1

    .line 313
    :cond_9
    and-int/lit8 v23, v18, 0x1

    if-eqz v23, :cond_a

    add-int/lit8 v20, v19, 0x1

    .end local v19    # "requestCount":I
    .local v20, "requestCount":I
    aput v5, v21, v19

    move/from16 v19, v20

    .line 310
    .end local v20    # "requestCount":I
    .restart local v19    # "requestCount":I
    :cond_a
    add-int/lit8 v5, v5, -0x1

    goto :goto_2

    .line 274
    .end local v5    # "i":I
    .end local v15    # "paperActive":Z
    .end local v18    # "r":I
    .end local v19    # "requestCount":I
    .end local v21    # "requestedSlot":[I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getScrollLimit()I

    move-result v4

    goto/16 :goto_1

    .line 316
    .restart local v5    # "i":I
    .restart local v15    # "paperActive":Z
    .restart local v19    # "requestCount":I
    .restart local v21    # "requestedSlot":[I
    :cond_c
    const/16 v16, 0x1

    .local v16, "pass":I
    :goto_3
    if-eqz v19, :cond_f

    .line 317
    const/4 v11, 0x0

    .line 318
    .local v11, "newCount":I
    const/4 v5, 0x0

    move v12, v11

    .end local v11    # "newCount":I
    .local v12, "newCount":I
    :goto_4
    move/from16 v0, v19

    if-ge v5, v0, :cond_e

    .line 319
    aget v23, v21, v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v23

    move/from16 v3, v16

    invoke-direct {v0, v1, v2, v3, v15}, Lcom/sec/android/gallery3d/ui/SlotView;->renderItem(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIZ)I

    move-result v18

    .line 321
    .restart local v18    # "r":I
    and-int/lit8 v23, v18, 0x2

    if-eqz v23, :cond_d

    const/4 v10, 0x1

    .line 322
    :cond_d
    and-int/lit8 v23, v18, 0x1

    if-eqz v23, :cond_12

    add-int/lit8 v11, v12, 0x1

    .end local v12    # "newCount":I
    .restart local v11    # "newCount":I
    aput v5, v21, v12

    .line 318
    :goto_5
    add-int/lit8 v5, v5, 0x1

    move v12, v11

    .end local v11    # "newCount":I
    .restart local v12    # "newCount":I
    goto :goto_4

    .line 324
    .end local v18    # "r":I
    :cond_e
    move/from16 v19, v12

    .line 316
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 327
    .end local v12    # "newCount":I
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollX:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollY:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-interface {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 329
    if-eqz v10, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    .line 331
    :cond_10
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 332
    .local v9, "listener":Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mMoreAnimation:Z

    move/from16 v23, v0

    if-eqz v23, :cond_11

    if-nez v10, :cond_11

    if-eqz v9, :cond_11

    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    new-instance v24, Lcom/sec/android/gallery3d/ui/SlotView$1;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v9}, Lcom/sec/android/gallery3d/ui/SlotView$1;-><init>(Lcom/sec/android/gallery3d/ui/SlotView;Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V

    invoke-virtual/range {v23 .. v24}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 340
    :cond_11
    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/sec/android/gallery3d/ui/SlotView;->mMoreAnimation:Z

    goto/16 :goto_0

    .end local v9    # "listener":Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    .restart local v12    # "newCount":I
    .restart local v18    # "r":I
    :cond_12
    move v11, v12

    .end local v12    # "newCount":I
    .restart local v11    # "newCount":I
    goto :goto_5
.end method

.method public scrollTo(I)V
    .locals 1
    .param p1, "slotIndex"    # I

    .prologue
    .line 870
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/ui/SlotView;->scrollTo(IZ)V

    .line 871
    return-void
.end method

.method public scrollTo(IZ)V
    .locals 9
    .param p1, "slotIndex"    # I
    .param p2, "animated"    # Z

    .prologue
    .line 874
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 875
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 876
    .local v3, "w":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->access$100(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I

    move-result v1

    .line 877
    .local v1, "slotWidth":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotGap:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->access$1500(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I

    move-result v4

    add-int/2addr v4, v1

    mul-int/2addr v4, p1

    div-int/lit8 v5, v3, 0x2

    sub-int/2addr v4, v5

    div-int/lit8 v5, v1, 0x2

    add-int v2, v4, v5

    .line 878
    .local v2, "target":I
    if-eqz p2, :cond_0

    .line 879
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->getPosition()I

    move-result v5

    sub-int v5, v2, v5

    neg-int v6, v3

    div-int/lit8 v6, v6, 0x2

    div-int/lit8 v7, v1, 0x2

    add-int/2addr v6, v7

    const v7, 0x7fffffff

    const/16 v8, 0x320

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->startScroll(IIII)I

    .line 883
    :goto_0
    return-void

    .line 881
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->setPosition(I)V

    goto :goto_0
.end method

.method public setCenterIndex(I)V
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 127
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotCount:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->access$300(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I

    move-result v2

    .line 128
    .local v2, "slotCount":I
    if-ltz p1, :cond_0

    if-lt p1, v2, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, v4}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getSlotRect(ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v1

    .line 132
    .local v1, "rect":Landroid/graphics/Rect;
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v3, :cond_2

    iget v3, v1, Landroid/graphics/Rect;->left:I

    iget v4, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SlotView;->getWidth()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v0, v3, 0x2

    .line 135
    .local v0, "position":I
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/SlotView;->setScrollPosition(I)V

    goto :goto_0

    .line 132
    .end local v0    # "position":I
    :cond_2
    iget v3, v1, Landroid/graphics/Rect;->top:I

    iget v4, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SlotView;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v0, v3, 0x2

    goto :goto_1
.end method

.method public setListener(Lcom/sec/android/gallery3d/ui/SlotView$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/SlotView$Listener;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mListener:Lcom/sec/android/gallery3d/ui/SlotView$Listener;

    .line 240
    return-void
.end method

.method public setOverscrollEffect(I)V
    .locals 2
    .param p1, "kind"    # I

    .prologue
    const/4 v0, 0x1

    .line 247
    iput p1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mOverscrollEffect:I

    .line 248
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    if-ne p1, v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->setOverfling(Z)V

    .line 249
    return-void

    .line 248
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setScrollPosition(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 159
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getScrollLimit()I

    move-result v0

    invoke-static {p1, v1, v0}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result p1

    .line 160
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->setPosition(I)V

    .line 161
    invoke-direct {p0, p1, v1}, Lcom/sec/android/gallery3d/ui/SlotView;->updateScrollPosition(IZ)V

    .line 162
    return-void
.end method

.method public setSlotCount(I)Z
    .locals 3
    .param p1, "slotCount"    # I

    .prologue
    const/4 v2, -0x1

    .line 784
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->setSlotCount(I)Z

    move-result v0

    .line 787
    .local v0, "changed":Z
    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mStartIndex:I

    if-eq v1, v2, :cond_0

    .line 788
    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mStartIndex:I

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SlotView;->setCenterIndex(I)V

    .line 789
    iput v2, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mStartIndex:I

    .line 792
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollX:I

    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SlotView;->setScrollPosition(I)V

    .line 793
    return v0

    .line 792
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mScrollY:I

    goto :goto_0
.end method

.method public setSlotRenderer(Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;)V
    .locals 3
    .param p1, "slotDrawer"    # Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    .line 120
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotWidth:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->access$100(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotHeight:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->access$200(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;->onSlotSizeChanged(II)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mRenderer:Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SlotView;->getVisibleStart()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SlotView;->getVisibleEnd()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;->onVisibleRangeChanged(II)V

    .line 124
    :cond_0
    return-void
.end method

.method public setSlotSpec(Lcom/sec/android/gallery3d/ui/SlotView$Spec;)V
    .locals 1
    .param p1, "spec"    # Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->setSlotSpec(Lcom/sec/android/gallery3d/ui/SlotView$Spec;)V

    .line 166
    return-void
.end method

.method public setStartIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 779
    iput p1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mStartIndex:I

    .line 780
    return-void
.end method

.method public setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 244
    return-void
.end method

.method public setWide(Z)V
    .locals 0
    .param p1, "wide"    # Z

    .prologue
    .line 862
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    .line 863
    return-void
.end method

.method public startRisingAnimation()V
    .locals 1

    .prologue
    .line 196
    new-instance v0, Lcom/sec/android/gallery3d/ui/SlotView$RisingAnimation;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/SlotView$RisingAnimation;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    .line 197
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;->start()V

    .line 198
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotCount:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->access$300(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    .line 199
    :cond_0
    return-void
.end method

.method public startScatteringAnimation(Lcom/sec/android/gallery3d/ui/RelativePosition;)V
    .locals 1
    .param p1, "position"    # Lcom/sec/android/gallery3d/ui/RelativePosition;

    .prologue
    .line 190
    new-instance v0, Lcom/sec/android/gallery3d/ui/SlotView$ScatteringAnimation;

    invoke-direct {v0, p1}, Lcom/sec/android/gallery3d/ui/SlotView$ScatteringAnimation;-><init>(Lcom/sec/android/gallery3d/ui/RelativePosition;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    .line 191
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mAnimation:Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView$SlotAnimation;->start()V

    .line 192
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView$Layout;->mSlotCount:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->access$300(Lcom/sec/android/gallery3d/ui/SlotView$Layout;)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    .line 193
    :cond_0
    return-void
.end method
