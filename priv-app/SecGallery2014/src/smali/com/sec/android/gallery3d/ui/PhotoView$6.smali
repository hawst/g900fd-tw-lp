.class Lcom/sec/android/gallery3d/ui/PhotoView$6;
.super Ljava/lang/Object;
.source "PhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PhotoView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0

    .prologue
    .line 3443
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$6;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHdmiConnect(Z)V
    .locals 1
    .param p1, "connected"    # Z

    .prologue
    .line 3446
    if-eqz p1, :cond_0

    .line 3447
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$6;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->updateHdmi3D()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$7700(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    .line 3452
    :goto_0
    return-void

    .line 3449
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/HdmiUtils;->setExternal3Dmode(Z)V

    .line 3450
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$6;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$7800(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/Hdmi3D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->stopPresentation()V

    goto :goto_0
.end method
