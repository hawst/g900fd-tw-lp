.class public Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
.super Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;
.source "AlbumTable.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;-><init>()V

    return-void
.end method

.method private setPropertyFromCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->source_id:Ljava/lang/String;

    .line 37
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->num_photos:I

    .line 38
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->title:Ljava/lang/String;

    .line 39
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->date_edited:J

    .line 40
    return-void
.end method


# virtual methods
.method public setProperty(Ljava/lang/Object;)V
    .locals 2
    .param p1, "source"    # Ljava/lang/Object;

    .prologue
    .line 27
    instance-of v0, p1, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 28
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "source":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->setPropertyFromCursor(Landroid/database/Cursor;)V

    .line 32
    return-void

    .line 30
    .restart local p1    # "source":Ljava/lang/Object;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Source is wrong"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
