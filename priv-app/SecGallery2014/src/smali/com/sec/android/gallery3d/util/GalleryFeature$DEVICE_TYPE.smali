.class final enum Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;
.super Ljava/lang/Enum;
.source "GalleryFeature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/util/GalleryFeature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "DEVICE_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

.field public static final enum CAMERA:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

.field public static final enum CAMERA_PHONE:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

.field public static final enum PHONE:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

.field public static final enum TABLET:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 339
    new-instance v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->PHONE:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    new-instance v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    const-string v1, "TABLET"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->TABLET:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    new-instance v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    const-string v1, "CAMERA"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->CAMERA:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    new-instance v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    const-string v1, "CAMERA_PHONE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->CAMERA_PHONE:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    .line 338
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->PHONE:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->TABLET:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->CAMERA:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->CAMERA_PHONE:Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->$VALUES:[Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 338
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 338
    const-class v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;
    .locals 1

    .prologue
    .line 338
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->$VALUES:[Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    invoke-virtual {v0}, [Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/gallery3d/util/GalleryFeature$DEVICE_TYPE;

    return-object v0
.end method
