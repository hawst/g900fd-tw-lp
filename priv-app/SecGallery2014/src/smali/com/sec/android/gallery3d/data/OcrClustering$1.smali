.class Lcom/sec/android/gallery3d/data/OcrClustering$1;
.super Ljava/lang/Object;
.source "OcrClustering.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/data/OcrClustering;->run(Lcom/sec/android/gallery3d/data/MediaSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/OcrClustering;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/OcrClustering;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/OcrClustering$1;->this$0:Lcom/sec/android/gallery3d/data/OcrClustering;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 3
    .param p1, "index"    # I
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 104
    invoke-static {p2}, Lcom/sec/android/gallery3d/data/OcrClustering;->isOCRMedia(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering$1;->this$0:Lcom/sec/android/gallery3d/data/OcrClustering;

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/OcrClustering$1;->this$0:Lcom/sec/android/gallery3d/data/OcrClustering;

    # getter for: Lcom/sec/android/gallery3d/data/OcrClustering;->mKeyword:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/gallery3d/data/OcrClustering;->access$000(Lcom/sec/android/gallery3d/data/OcrClustering;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/OcrClustering;->searchImgText(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/OcrClustering$1;->this$0:Lcom/sec/android/gallery3d/data/OcrClustering;

    # getter for: Lcom/sec/android/gallery3d/data/OcrClustering;->mItems:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/gallery3d/data/OcrClustering;->access$100(Lcom/sec/android/gallery3d/data/OcrClustering;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
