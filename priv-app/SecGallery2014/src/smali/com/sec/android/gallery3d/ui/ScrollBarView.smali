.class public Lcom/sec/android/gallery3d/ui/ScrollBarView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "ScrollBarView.java"


# instance fields
.field private mBarHeight:I

.field private mContentPosition:I

.field private mContentTotal:I

.field private mGivenGripWidth:I

.field private mGripHeight:I

.field private mGripPosition:I

.field private mGripWidth:I

.field private mScrollBarTexture:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mScrollBarVisible:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "gripHeight"    # I
    .param p3, "gripWidth"    # I

    .prologue
    const/4 v4, 0x0

    .line 37
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 34
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mScrollBarVisible:Z

    .line 38
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 39
    .local v0, "outValue":Landroid/util/TypedValue;
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010064

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 41
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    const v2, 0x7f020182

    invoke-direct {v1, p1, v2}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mScrollBarTexture:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 42
    iput v4, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripPosition:I

    .line 43
    iput v4, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripWidth:I

    .line 44
    iput p3, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGivenGripWidth:I

    .line 45
    iput p2, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripHeight:I

    .line 46
    return-void
.end method


# virtual methods
.method public hideScrollBar()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mScrollBarVisible:Z

    .line 99
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 50
    if-nez p1, :cond_0

    .line 52
    :goto_0
    return-void

    .line 51
    :cond_0
    sub-int v0, p5, p3

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mBarHeight:I

    goto :goto_0
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 86
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mScrollBarVisible:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripWidth:I

    if-nez v0, :cond_1

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mBarHeight:I

    iget v1, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripHeight:I

    sub-int/2addr v0, v1

    div-int/lit8 v3, v0, 0x2

    .line 90
    .local v3, "y":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mScrollBarTexture:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget v2, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripPosition:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripWidth:I

    iget v5, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripHeight:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto :goto_0
.end method

.method public setContentPosition(II)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "total"    # I

    .prologue
    const/4 v1, 0x0

    .line 57
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mContentPosition:I

    if-ne p1, v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mContentTotal:I

    if-ne p2, v2, :cond_0

    .line 81
    :goto_0
    return-void

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->invalidate()V

    .line 63
    iput p1, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mContentPosition:I

    .line 64
    iput p2, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mContentTotal:I

    .line 67
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mContentTotal:I

    if-gtz v2, :cond_1

    .line 68
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripPosition:I

    .line 69
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripWidth:I

    goto :goto_0

    .line 77
    :cond_1
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGivenGripWidth:I

    iput v2, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripWidth:I

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripWidth:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mContentTotal:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 79
    .local v0, "r":F
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mContentPosition:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripPosition:I

    .line 80
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripPosition:I

    if-gez v2, :cond_2

    :goto_1
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripPosition:I

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripPosition:I

    goto :goto_1
.end method

.method public setGripWidth(I)V
    .locals 1
    .param p1, "width"    # I

    .prologue
    .line 102
    iput p1, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGivenGripWidth:I

    .line 103
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGivenGripWidth:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mGripWidth:I

    .line 104
    return-void
.end method

.method public showScrollBar()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ScrollBarView;->mScrollBarVisible:Z

    .line 95
    return-void
.end method
