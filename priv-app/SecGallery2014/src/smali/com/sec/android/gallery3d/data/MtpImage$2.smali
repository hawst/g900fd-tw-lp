.class Lcom/sec/android/gallery3d/data/MtpImage$2;
.super Ljava/lang/Object;
.source "MtpImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/data/MtpImage;->requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/MtpImage;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/MtpImage;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/MtpImage$2;->this$0:Lcom/sec/android/gallery3d/data/MtpImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 6
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v5, 0x0

    .line 111
    const-string v1, "MtpImage"

    const-string v2, "requestLargeImage is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/MtpImage$2;->this$0:Lcom/sec/android/gallery3d/data/MtpImage;

    # getter for: Lcom/sec/android/gallery3d/data/MtpImage;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;
    invoke-static {v1}, Lcom/sec/android/gallery3d/data/MtpImage;->access$200(Lcom/sec/android/gallery3d/data/MtpImage;)Lcom/sec/android/gallery3d/data/MtpContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MtpContext;->getMtpClient()Lcom/sec/android/gallery3d/data/MtpClient;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpImage$2;->this$0:Lcom/sec/android/gallery3d/data/MtpImage;

    # getter for: Lcom/sec/android/gallery3d/data/MtpImage;->mDeviceId:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/data/MtpImage;->access$000(Lcom/sec/android/gallery3d/data/MtpImage;)I

    move-result v2

    invoke-static {v2}, Landroid/hardware/usb/UsbDevice;->getDeviceName(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/MtpImage$2;->this$0:Lcom/sec/android/gallery3d/data/MtpImage;

    # getter for: Lcom/sec/android/gallery3d/data/MtpImage;->mObjectId:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/data/MtpImage;->access$100(Lcom/sec/android/gallery3d/data/MtpImage;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/MtpImage$2;->this$0:Lcom/sec/android/gallery3d/data/MtpImage;

    # getter for: Lcom/sec/android/gallery3d/data/MtpImage;->mObjectSize:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/data/MtpImage;->access$300(Lcom/sec/android/gallery3d/data/MtpImage;)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/data/MtpClient;->getObject(Ljava/lang/String;II)[B

    move-result-object v0

    .line 115
    .local v0, "bytes":[B
    if-nez v0, :cond_0

    .line 116
    const-string v1, "MtpImage"

    const-string/jumbo v2, "thumbnail is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const-string v1, "MtpImage"

    const-string v2, "failed to getting large image via MTP"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const/4 v1, 0x0

    .line 121
    :goto_0
    return-object v1

    :cond_0
    array-length v1, v0

    const/4 v2, 0x1

    invoke-static {v0, v5, v1, v5, v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->newInstance([BIIZZ)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v1

    goto :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 109
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/MtpImage$2;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    return-object v0
.end method
