.class public Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "SNSAlbum.java"


# static fields
.field private static CACHED_PATH:[Ljava/lang/String; = null

.field private static final INVALID_COUNT:I = -0x1

.field private static final TAG:Ljava/lang/String; = "SNSAlbum"

.field private static WHERE:Ljava/lang/String;

.field private static final sIdIndex:I


# instance fields
.field private mCachedCount:I

.field private mData:Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

.field private mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const-string v0, "album_id = ? AND cache_status >= ?"

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->WHERE:Ljava/lang/String;

    .line 44
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "cache_pathname"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->CACHED_PATH:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/sns/AlbumTable;I)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "entry"    # Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    .param p4, "type"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mCachedCount:I

    .line 61
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mResolver:Landroid/content/ContentResolver;

    .line 62
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mData:Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    .line 63
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 65
    return-void
.end method

.method private static WHERE_CACHE_STATUS(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p0, "albumId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x2

    .line 46
    new-array v0, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public static find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;JI)Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;
    .locals 2
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "id"    # J
    .param p4, "type"    # I

    .prologue
    .line 69
    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, p2, p3}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->getAlbumEntry(Landroid/content/ContentResolver;J)Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    move-result-object v0

    .line 71
    .local v0, "entry":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 72
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    invoke-direct {v1, p0, p1, v0, p4}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/sns/AlbumTable;I)V

    goto :goto_0
.end method

.method public static getMediaItemById(Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/util/ArrayList;)[Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 23
    .param p0, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/GalleryApp;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)[",
            "Lcom/sec/android/gallery3d/data/MediaItem;"
        }
    .end annotation

    .prologue
    .line 123
    .local p1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v20, v0

    .line 124
    .local v20, "result":[Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 177
    :goto_0
    return-object v20

    .line 125
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 126
    .local v16, "idLow":J
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 128
    .local v14, "idHigh":J
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 129
    .local v2, "resolver":Landroid/content/ContentResolver;
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v10

    .line 130
    .local v10, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    sget-object v3, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    .line 131
    .local v3, "uri":Landroid/net/Uri;
    const/4 v9, 0x0

    .line 133
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->getProjection()[Ljava/lang/String;

    move-result-object v4

    const-string v5, "cache_status = ? AND _id BETWEEN ? AND ?"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const/16 v22, 0x2

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v6, v7

    const/4 v7, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v6, v7

    const/4 v7, 0x2

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v6, v7

    const-string v7, "_id"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 145
    if-nez v9, :cond_1

    .line 146
    const-string v4, "SNSAlbum"

    const-string v5, "query fail"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 150
    :cond_1
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v19

    .line 151
    .local v19, "n":I
    const/4 v11, 0x0

    .line 153
    .local v11, "i":I
    :cond_2
    :goto_1
    move/from16 v0, v19

    if-ge v11, v0, :cond_5

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 154
    const/4 v4, 0x0

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 157
    .local v12, "id":J
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v12

    if-gtz v4, :cond_2

    .line 161
    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    cmp-long v4, v4, v12

    if-gez v4, :cond_4

    .line 162
    add-int/lit8 v11, v11, 0x1

    move/from16 v0, v19

    if-lt v11, v0, :cond_3

    .line 177
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 167
    :cond_4
    :try_start_2
    new-instance v21, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    invoke-direct/range {v21 .. v21}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;-><init>()V

    .line 168
    .local v21, "table":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    move-object/from16 v0, v21

    move-object/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;

    .line 169
    sget-object v4, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v4, v12, v13}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    .line 170
    .local v8, "childPath":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v8, v0, v10, v1}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/remote/sns/PhotoTable;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v18

    .line 172
    .local v18, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    aput-object v18, v20, v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 173
    add-int/lit8 v11, v11, 0x1

    .line 174
    goto :goto_1

    .line 177
    .end local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v12    # "id":J
    .end local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v21    # "table":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    :cond_5
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .end local v11    # "i":I
    .end local v19    # "n":I
    :catchall_0
    move-exception v4

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v4
.end method

.method public static getPhotoEntry(Landroid/content/ContentResolver;J)Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    .locals 9
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "id"    # J

    .prologue
    const/4 v8, 0x0

    .line 182
    const/4 v6, 0x0

    .line 184
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 189
    new-instance v7, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    invoke-direct {v7}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;-><init>()V

    .line 190
    .local v7, "table":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v7, v6, v7}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :goto_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    return-object v0

    :cond_0
    move-object v0, v8

    .line 190
    goto :goto_0

    .line 194
    .end local v7    # "table":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/remote/sns/PhotoTable;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "entry"    # Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 110
    invoke-virtual {p2, p0}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .line 111
    .local v0, "item":Lcom/sec/android/gallery3d/remote/sns/SNSImage;
    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .end local v0    # "item":Lcom/sec/android/gallery3d/remote/sns/SNSImage;
    invoke-direct {v0, p0, p3, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/sns/PhotoTable;)V

    .line 116
    .restart local v0    # "item":Lcom/sec/android/gallery3d/remote/sns/SNSImage;
    :goto_0
    return-object v0

    .line 114
    :cond_0
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->updateContent(Lcom/sec/android/gallery3d/remote/sns/PhotoTable;)V

    goto :goto_0
.end method


# virtual methods
.method public cache(I)V
    .locals 0
    .param p1, "inFlag"    # I

    .prologue
    .line 248
    packed-switch p1, :pswitch_data_0

    .line 258
    :pswitch_0
    return-void

    .line 248
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getBucketId()I
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mPath:Lcom/sec/android/gallery3d/data/Path;

    if-nez v0, :cond_0

    .line 297
    const/4 v0, -0x1

    .line 299
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public getCacheFlag()I
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 237
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mData:Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    iget v2, v2, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->cache_flag:I

    if-ne v2, v0, :cond_0

    .line 242
    :goto_0
    return v0

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mData:Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->cache_flag:I

    if-ne v0, v1, :cond_1

    move v0, v1

    .line 240
    goto :goto_0

    .line 242
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCacheSize()J
    .locals 2

    .prologue
    .line 275
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getCacheStatus()I
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mData:Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->cache_status:I

    packed-switch v0, :pswitch_data_0

    .line 268
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 264
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 266
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 262
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 12
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    sget-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 80
    .local v1, "uri":Landroid/net/Uri;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 81
    .local v10, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v7, 0x0

    .line 83
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v5, "display_index"

    .line 84
    .local v5, "sortOrder":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 85
    const-string v5, "display_index DESC"

    .line 89
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mResolver:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->getProjection()[Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->WHERE:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mData:Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    iget-object v4, v4, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->source_id:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->WHERE_CACHE_STATUS(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 91
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 92
    .local v8, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    if-eqz v7, :cond_1

    .line 93
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    new-instance v11, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    invoke-direct {v11}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;-><init>()V

    .line 95
    .local v11, "table":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    invoke-virtual {v11, v7, v11}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;

    .line 96
    sget-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iget-wide v2, v11, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->_id:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 97
    .local v6, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v6, v11, v8, v0}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/remote/sns/PhotoTable;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v9

    .line 99
    .local v9, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 103
    .end local v5    # "sortOrder":Ljava/lang/String;
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v8    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v9    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v11    # "table":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 87
    .restart local v5    # "sortOrder":Ljava/lang/String;
    :cond_0
    :try_start_1
    const-string v5, "display_index ASC"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 103
    .restart local v8    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :cond_1
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 105
    return-object v10
.end method

.method public getMediaItemCount()I
    .locals 7

    .prologue
    .line 207
    iget v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mCachedCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 208
    const/4 v6, 0x0

    .line 210
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->CACHED_PATH:[Ljava/lang/String;

    sget-object v3, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->WHERE:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mData:Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    iget-object v4, v4, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->source_id:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->WHERE_CACHE_STATUS(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 214
    if-eqz v6, :cond_0

    .line 215
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mCachedCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 221
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mCachedCount:I

    return v0

    .line 218
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mData:Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 231
    const-wide v0, 0x400000000504L

    .line 232
    .local v0, "operation":J
    return-wide v0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mDataVersion:J

    .line 282
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mCachedCount:I

    .line 284
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mDataVersion:J

    return-wide v0
.end method

.method protected updateContent(Lcom/sec/android/gallery3d/remote/sns/AlbumTable;)V
    .locals 2
    .param p1, "entry"    # Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mData:Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mData:Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    .line 201
    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->mDataVersion:J

    .line 203
    :cond_0
    return-void
.end method
