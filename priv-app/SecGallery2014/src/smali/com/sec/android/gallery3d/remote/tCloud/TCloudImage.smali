.class public Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;
.super Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;
.source "TCloudImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$TCloudLargeImageRequest;,
        Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;
    }
.end annotation


# static fields
.field private static final INDEX_AVAILABLE_THUMB:I = 0xc

.field private static final INDEX_BUCKET_ID:I = 0x6

.field private static final INDEX_BUCKET_NAME:I = 0x7

.field private static final INDEX_CACHED:I = 0xb

.field private static final INDEX_CACHE_PATH:I = 0x8

.field private static final INDEX_CAPTION:I = 0x3

.field private static final INDEX_DATA:I = 0x1

.field private static final INDEX_DATE_MODIFIED:I = 0x5

.field private static final INDEX_ID:I = 0x0

.field private static final INDEX_MIME_TYPE:I = 0x4

.field private static final INDEX_ORIENTATION:I = 0xa

.field private static final INDEX_SIZE:I = 0x2

.field private static final INDEX_THUMBPATH:I = 0x9

.field public static final ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private final mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->TAG:Ljava/lang/String;

    .line 54
    const-string v0, "/tCloud/image/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V
    .locals 6
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "id"    # I

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 71
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mHandler:Landroid/os/Handler;

    .line 83
    new-instance v4, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    iput-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .line 84
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 85
    .local v2, "resolver":Landroid/content/ContentResolver;
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v3, v4, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseFileUri:Landroid/net/Uri;

    .line 87
    .local v3, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->getTCloudImageProjection()[Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4, p3}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->getItemCursor(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    .line 88
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 89
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "cannot get cursor for: "

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 93
    :cond_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-gtz v4, :cond_1

    .line 94
    sget-object v4, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->TAG:Ljava/lang/String;

    const-string v5, "Cursor getCount is zero"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 98
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->loadFromCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 107
    :goto_0
    return-void

    .line 100
    :cond_2
    :try_start_1
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "cannot find data for: "

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :catch_0
    move-exception v1

    .line 103
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 105
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v4
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 71
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mHandler:Landroid/os/Handler;

    .line 76
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .line 77
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->loadFromCursor(Landroid/database/Cursor;)V

    .line 78
    return-void
.end method

.method private UpdateOrientation(I)V
    .locals 2
    .param p1, "ori"    # I

    .prologue
    .line 110
    iget v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 111
    const/16 v0, 0x5a

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    .line 119
    :goto_0
    return-void

    .line 112
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 113
    const/16 v0, 0xb4

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    goto :goto_0

    .line 114
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 115
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    goto :goto_0

    .line 117
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method private loadFromCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->id:J

    .line 123
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->data:Ljava/lang/String;

    .line 124
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->fileSize:J

    .line 125
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->caption:Ljava/lang/String;

    .line 126
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mimeType:Ljava/lang/String;

    .line 127
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->dateModifiedInSec:J

    .line 128
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->bucketId:I

    .line 129
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->bucketName:Ljava/lang/String;

    .line 130
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->cachedPath:Ljava/lang/String;

    .line 131
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->thumbPath:Ljava/lang/String;

    .line 132
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    .line 133
    iget v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->UpdateOrientation(I)V

    .line 134
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->isCached:I

    .line 135
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->availablethumb:I

    .line 136
    return-void
.end method


# virtual methods
.method public delete()V
    .locals 0

    .prologue
    .line 319
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->delete()V

    .line 320
    return-void
.end method

.method public download(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 325
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->download(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    :goto_0
    return-void

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseImageUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "id"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->id:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 6

    .prologue
    .line 270
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 271
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->dateModifiedInSec:J

    invoke-static {v2, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 273
    const/16 v1, 0x10

    iget v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 274
    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getFullImageRotation()I
    .locals 1

    .prologue
    .line 265
    iget v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x2

    return v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x0

    return v0
.end method

.method public getSourceId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 333
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->id:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSourceUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 4

    .prologue
    .line 284
    const-wide/16 v0, 0x400

    .line 286
    .local v0, "operation":J
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mimeType:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->isSupportedByRegionDecoder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 287
    const-wide/16 v2, 0x40

    or-long/2addr v0, v2

    .line 290
    :cond_0
    return-wide v0
.end method

.method public getThumbPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->thumbPath:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x0

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$ThumbDecodeRequest;-><init>(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;I)V

    return-object v0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 211
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$TCloudLargeImageRequest;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$TCloudLargeImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)V

    return-object v0
.end method

.method public rotate(I)V
    .locals 10
    .param p1, "degrees"    # I

    .prologue
    .line 295
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 296
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 297
    .local v1, "values":Landroid/content/ContentValues;
    iget v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->orientation:I

    add-int/2addr v2, p1

    rem-int/lit16 v0, v2, 0x168

    .line 298
    .local v0, "orientation":I
    if-gez v0, :cond_0

    .line 299
    add-int/lit16 v0, v0, 0x168

    .line 300
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mimeType:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isJpeg(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 301
    const-string v2, "_size"

    iget-wide v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->fileSize:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 302
    const-string v2, "orientation"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 303
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseImageUri:Landroid/net/Uri;

    const-string v4, "_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-wide v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->id:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 315
    :goto_0
    return-void

    .line 308
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$1;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$1;-><init>(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected updateFromCursor(Landroid/database/Cursor;)Z
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 140
    new-instance v0, Lcom/sec/android/gallery3d/util/UpdateHelper;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/util/UpdateHelper;-><init>()V

    .line 141
    .local v0, "uh":Lcom/sec/android/gallery3d/util/UpdateHelper;
    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->id:J

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->id:J

    .line 142
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->data:Ljava/lang/String;

    .line 143
    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->fileSize:J

    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->fileSize:J

    .line 144
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->caption:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->caption:Ljava/lang/String;

    .line 145
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mimeType:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mimeType:Ljava/lang/String;

    .line 146
    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->dateModifiedInSec:J

    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->dateModifiedInSec:J

    .line 147
    iget v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->bucketId:I

    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->bucketId:I

    .line 148
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->bucketName:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->bucketName:Ljava/lang/String;

    .line 149
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->cachedPath:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->cachedPath:Ljava/lang/String;

    .line 150
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->thumbPath:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->thumbPath:Ljava/lang/String;

    .line 152
    iget v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->isCached:I

    const/16 v2, 0xb

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->isCached:I

    .line 153
    iget v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->availablethumb:I

    const/16 v2, 0xc

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->availablethumb:I

    .line 154
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/UpdateHelper;->isUpdated()Z

    move-result v1

    return v1
.end method
