.class public Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
.super Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;
.source "PhotoTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;,
        Lcom/sec/android/gallery3d/remote/sns/PhotoTable$SnsPhotoColumns;
    }
.end annotation


# static fields
.field private static final INVALID_INDEX:I = -0x1

.field private static final MAX_SCREENNAIL_SIZE:I = 0x3c0

.field private static final MIN_THUMBNAIL_SIZE:I = 0xc8

.field private static mFields:[Ljava/lang/reflect/Field;


# instance fields
.field public comments_count:I

.field public likes_count:I

.field private mImageInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mFields:[Ljava/lang/reflect/Field;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;-><init>()V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    .line 205
    return-void
.end method

.method public static getProjection()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 67
    sget-object v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mFields:[Ljava/lang/reflect/Field;

    .line 68
    .local v0, "fields":[Ljava/lang/reflect/Field;
    array-length v3, v0

    new-array v2, v3, [Ljava/lang/String;

    .line 69
    .local v2, "projection":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-eq v1, v3, :cond_0

    .line 70
    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 72
    :cond_0
    return-object v2
.end method

.method private getType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 190
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    :cond_0
    const-string v0, ""

    .line 202
    :goto_0
    return-object v0

    .line 193
    :cond_1
    const-string v0, ".jpg"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 194
    const-string v0, "image/jpeg"

    goto :goto_0

    .line 195
    :cond_2
    const-string v0, ".png"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 196
    const-string v0, "image/png"

    goto :goto_0

    .line 197
    :cond_3
    const-string v0, ".gif"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 198
    const-string v0, "image/gif"

    goto :goto_0

    .line 199
    :cond_4
    const-string v0, ".bmp"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 200
    const-string v0, "image/bmp"

    goto :goto_0

    .line 202
    :cond_5
    const-string v0, "image/jpeg"

    goto :goto_0
.end method

.method private setPropertyFromCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 86
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->source_id:Ljava/lang/String;

    .line 87
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->album_id:Ljava/lang/String;

    .line 88
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->date_taken:J

    .line 89
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->date_edited:J

    .line 90
    iput v2, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->rotation:I

    .line 96
    return-void
.end method


# virtual methods
.method public addImageInfo(IILjava/lang/String;)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "s"    # Ljava/lang/String;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;-><init>(Lcom/sec/android/gallery3d/remote/sns/PhotoTable;IILjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->clear()V

    .line 47
    iput v0, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->likes_count:I

    .line 48
    iput v0, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->comments_count:I

    .line 49
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 53
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    if-nez v2, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 55
    check-cast v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    .line 56
    .local v0, "p":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->likes_count:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->likes_count:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->comments_count:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->comments_count:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getFields()[Ljava/lang/reflect/Field;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mFields:[Ljava/lang/reflect/Field;

    return-object v0
.end method

.method public setMoreProperty()V
    .locals 14

    .prologue
    .line 99
    const/4 v3, -0x1

    .line 100
    .local v3, "indexFullImage":I
    const/4 v4, -0x1

    .line 101
    .local v4, "indexScreenNail":I
    const/4 v5, -0x1

    .line 102
    .local v5, "indexScreenNail_candidate":I
    const/4 v6, -0x1

    .line 103
    .local v6, "indexThumbnail":I
    const/4 v10, 0x0

    .line 106
    .local v10, "valid":Z
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 107
    .local v2, "imageInfoSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_5

    .line 108
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget v11, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->width:I

    .line 109
    .local v11, "w":I
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget v0, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->height:I

    .line 110
    .local v0, "h":I
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget-object v9, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->url:Ljava/lang/String;

    .line 112
    .local v9, "url":Ljava/lang/String;
    if-lez v11, :cond_0

    if-lez v0, :cond_0

    if-eqz v9, :cond_0

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 107
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 116
    :cond_1
    const/4 v10, 0x1

    .line 119
    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 122
    .local v7, "largeSideLength":I
    const/4 v12, -0x1

    if-ne v3, v12, :cond_2

    .line 123
    move v3, v1

    .line 127
    :cond_2
    const/4 v12, -0x1

    if-ne v4, v12, :cond_4

    const/16 v12, 0x3c0

    if-gt v7, v12, :cond_4

    .line 129
    move v4, v1

    .line 135
    :cond_3
    :goto_2
    const/16 v12, 0xc8

    if-lt v7, v12, :cond_0

    .line 136
    move v6, v1

    goto :goto_1

    .line 130
    :cond_4
    const/16 v12, 0x3c0

    if-lt v7, v12, :cond_3

    .line 131
    move v5, v1

    goto :goto_2

    .line 141
    .end local v0    # "h":I
    .end local v7    # "largeSideLength":I
    .end local v9    # "url":Ljava/lang/String;
    .end local v11    # "w":I
    :cond_5
    if-eqz v10, :cond_6

    const/4 v12, -0x1

    if-ne v3, v12, :cond_8

    .line 142
    :cond_6
    const/4 v12, 0x0

    iput-boolean v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->isValid:Z

    .line 183
    :cond_7
    :goto_3
    return-void

    .line 146
    :cond_8
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget-object v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->url:Ljava/lang/String;

    iput-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_url:Ljava/lang/String;

    .line 147
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget-object v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->url:Ljava/lang/String;

    iput-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->screennail_url:Ljava/lang/String;

    .line 148
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget-object v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->url:Ljava/lang/String;

    iput-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->thumbnail_url:Ljava/lang/String;

    .line 150
    const/4 v12, -0x1

    if-eq v4, v12, :cond_d

    .line 151
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget-object v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->url:Ljava/lang/String;

    iput-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->screennail_url:Ljava/lang/String;

    .line 152
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget-object v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->url:Ljava/lang/String;

    iput-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->thumbnail_url:Ljava/lang/String;

    .line 153
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->width:I

    iput v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->width:I

    .line 154
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->height:I

    iput v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->height:I

    .line 164
    :cond_9
    :goto_4
    const/4 v12, -0x1

    if-eq v6, v12, :cond_a

    .line 165
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget-object v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->url:Ljava/lang/String;

    iput-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->thumbnail_url:Ljava/lang/String;

    .line 168
    :cond_a
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_url:Ljava/lang/String;

    const-string v13, "_o."

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 169
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->width:I

    iput v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->width:I

    .line 170
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->height:I

    iput v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->height:I

    .line 173
    :cond_b
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_url:Ljava/lang/String;

    invoke-direct {p0, v12}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->getType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_type:Ljava/lang/String;

    .line 174
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_type:Ljava/lang/String;

    if-eqz v12, :cond_c

    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_type:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_e

    .line 175
    :cond_c
    const/4 v12, 0x0

    iput-boolean v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->isValid:Z

    goto/16 :goto_3

    .line 156
    :cond_d
    const/4 v12, -0x1

    if-eq v5, v12, :cond_9

    .line 157
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget-object v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->url:Ljava/lang/String;

    iput-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->screennail_url:Ljava/lang/String;

    .line 158
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget-object v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->url:Ljava/lang/String;

    iput-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->thumbnail_url:Ljava/lang/String;

    .line 159
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->width:I

    iput v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->width:I

    .line 160
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->mImageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;

    iget v12, v12, Lcom/sec/android/gallery3d/remote/sns/PhotoTable$ImageInfo;->height:I

    iput v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->height:I

    goto :goto_4

    .line 179
    :cond_e
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->title:Ljava/lang/String;

    if-eqz v12, :cond_f

    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->title:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_7

    .line 180
    :cond_f
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_url:Ljava/lang/String;

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 181
    .local v8, "split":[Ljava/lang/String;
    array-length v12, v8

    add-int/lit8 v12, v12, -0x1

    aget-object v12, v8, v12

    iput-object v12, p0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->title:Ljava/lang/String;

    goto/16 :goto_3
.end method

.method public setProperty(Ljava/lang/Object;)V
    .locals 2
    .param p1, "source"    # Ljava/lang/Object;

    .prologue
    .line 77
    instance-of v0, p1, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 78
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "source":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->setPropertyFromCursor(Landroid/database/Cursor;)V

    .line 82
    return-void

    .line 80
    .restart local p1    # "source":Ljava/lang/Object;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Source is wrong"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
