.class public Lcom/sec/android/gallery3d/homesync/HomeSyncController;
.super Ljava/lang/Object;
.source "HomeSyncController.java"


# instance fields
.field private final ACTION_PLAY_NEXT:Ljava/lang/String;

.field private final ACTION_PLAY_PREVIOUS:Ljava/lang/String;

.field private final ACTION_START_SLIDESHOW:Ljava/lang/String;

.field private final ACTION_STOP_PLAYER:Ljava/lang/String;

.field private final ACTION_STOP_SLIDESHOW:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const-class v0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->TAG:Ljava/lang/String;

    .line 11
    const-string v0, "com.android.spc.image.PLAY_NEXT"

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->ACTION_PLAY_NEXT:Ljava/lang/String;

    .line 12
    const-string v0, "com.android.spc.image.PLAY_PREVIOUS"

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->ACTION_PLAY_PREVIOUS:Ljava/lang/String;

    .line 13
    const-string v0, "com.android.spc.image.START_SLIDESHOW"

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->ACTION_START_SLIDESHOW:Ljava/lang/String;

    .line 14
    const-string v0, "com.android.spc.image.STOP_SLIDESHOW"

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->ACTION_STOP_SLIDESHOW:Ljava/lang/String;

    .line 15
    const-string v0, "com.android.spc.image.STOP_PLAYER"

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->ACTION_STOP_PLAYER:Ljava/lang/String;

    .line 16
    iput-object v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mContext:Landroid/content/Context;

    .line 17
    iput-object v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    .line 19
    new-instance v0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;-><init>(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 54
    iput-object p1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mContext:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public registerBroadCastReceiver()V
    .locals 3

    .prologue
    .line 60
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 61
    .local v0, "contentReceiverFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.spc.image.PLAY_NEXT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 62
    const-string v1, "com.android.spc.image.PLAY_PREVIOUS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 63
    const-string v1, "com.android.spc.image.START_SLIDESHOW"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 64
    const-string v1, "com.android.spc.image.STOP_SLIDESHOW"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 65
    const-string v1, "com.android.spc.image.STOP_PLAYER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 68
    return-void
.end method

.method public unregisterBroadCastReceiver()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 74
    :cond_0
    return-void
.end method
