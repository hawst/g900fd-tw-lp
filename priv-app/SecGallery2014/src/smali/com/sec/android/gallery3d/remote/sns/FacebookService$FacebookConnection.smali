.class Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;
.super Ljava/lang/Object;
.source "FacebookService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/sns/FacebookService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FacebookConnection"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/sns/FacebookService;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/sns/FacebookService;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;->this$0:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;->this$0:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    invoke-static {p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;

    move-result-object v1

    # setter for: Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mSnsFacebook:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->access$102(Lcom/sec/android/gallery3d/remote/sns/FacebookService;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;)Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;

    .line 242
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;->this$0:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mSnsFacebook:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->access$102(Lcom/sec/android/gallery3d/remote/sns/FacebookService;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;)Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;

    .line 247
    return-void
.end method
