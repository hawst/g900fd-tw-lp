.class Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;
.super Ljava/lang/Object;
.source "MagazineWidgetService.java"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PhotoRVFactory"
.end annotation


# instance fields
.field private QUEUE_COUNT_FUTURE_BMP:I

.field private mAlbumPath:Ljava/lang/String;

.field private final mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mAppWidgetId:I

.field private mFrameType:I

.field private mFutureBmpMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mFuturePosition:I

.field private mFutureThread:Ljava/lang/Thread;

.field private mIsEmptyViewNow:Z

.field private mIsInit:Z

.field private mIsThreadStop:Z

.field private mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

.field private mSourceSize:I

.field private mTempContext:Landroid/content/Context;

.field private mType:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;IILjava/lang/String;I)V
    .locals 3
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "id"    # I
    .param p3, "type"    # I
    .param p4, "albumPath"    # Ljava/lang/String;
    .param p5, "frameType"    # I

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    .line 167
    iput v2, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFuturePosition:I

    .line 168
    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mTempContext:Landroid/content/Context;

    .line 169
    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    .line 170
    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 171
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->QUEUE_COUNT_FUTURE_BMP:I

    .line 172
    iput v2, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSourceSize:I

    .line 174
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsInit:Z

    .line 175
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsThreadStop:Z

    .line 178
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 179
    iput p2, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    .line 180
    iput p3, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mType:I

    .line 181
    iput-object p4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAlbumPath:Ljava/lang/String;

    .line 182
    iput p5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFrameType:I

    .line 183
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v0, :cond_0

    .line 184
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 186
    :cond_0
    return-void
.end method

.method static synthetic access$1002(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;
    .param p1, "x1"    # Ljava/lang/Thread;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    .prologue
    .line 158
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFuturePosition:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsInit:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;
    .param p1, "x1"    # Z

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsInit:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Lcom/sec/android/gallery3d/gadget/WidgetSource;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mTempContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    .prologue
    .line 158
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->QUEUE_COUNT_FUTURE_BMP:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    .prologue
    .line 158
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSourceSize:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsThreadStop:Z

    return v0
.end method

.method private clearAlbumPath(I)V
    .locals 4
    .param p1, "appWidgetId"    # I

    .prologue
    .line 565
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 566
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 567
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    iget v2, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 568
    const-string v2, ""

    iput-object v2, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    .line 571
    :cond_0
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 573
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 575
    return-void

    .line 573
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v2
.end method

.method private initSource()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 196
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v5, :cond_0

    .line 197
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->close()V

    .line 199
    :cond_0
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v5, :cond_3

    .line 200
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    if-eqz v5, :cond_2

    .line 201
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->isAlive()Z

    move-result v5

    if-ne v5, v6, :cond_1

    .line 202
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 203
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsThreadStop:Z

    .line 205
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    .line 206
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 207
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 209
    :cond_2
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsInit:Z

    .line 212
    :cond_3
    iget v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mType:I

    packed-switch v5, :pswitch_data_0

    .line 246
    :cond_4
    :goto_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v5, :cond_5

    .line 247
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v5, p0}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->setContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 248
    :cond_5
    return-void

    .line 214
    :pswitch_0
    new-instance v5, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$EmptySource;

    invoke-direct {v5}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$EmptySource;-><init>()V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    goto :goto_0

    .line 217
    :pswitch_1
    new-instance v5, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    iget-object v6, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-direct {v5, v6, v7}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    goto :goto_0

    .line 220
    :pswitch_2
    const/4 v4, 0x0

    .line 222
    .local v4, "succeed":Z
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAlbumPath:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 223
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 224
    .local v1, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 225
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_6

    .line 226
    new-instance v5, Lcom/sec/android/gallery3d/gadget/MediaSetSource;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v1, v6}, Lcom/sec/android/gallery3d/gadget/MediaSetSource;-><init>(Lcom/sec/android/gallery3d/data/DataManager;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    const/4 v4, 0x1

    .line 235
    .end local v1    # "manager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_6
    :goto_1
    if-nez v4, :cond_4

    .line 236
    new-instance v5, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$EmptySource;

    invoke-direct {v5}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$EmptySource;-><init>()V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    goto :goto_0

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "e":Ljava/lang/NullPointerException;
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v5

    const-string v6, "exception while initiating album set path : "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 239
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .end local v4    # "succeed":Z
    :pswitch_3
    new-instance v5, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    iget-object v6, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-direct {v5, v6, v7}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    goto :goto_0

    .line 242
    :pswitch_4
    new-instance v5, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;

    iget-object v6, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    goto :goto_0

    .line 212
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private updateEmptyView(Z)V
    .locals 11
    .param p1, "showEmpty"    # Z

    .prologue
    const v10, 0x7f0f016c

    const v9, 0x7f0f0169

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 284
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 287
    .local v2, "manager":Landroid/appwidget/AppWidgetManager;
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 288
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    if-eqz p1, :cond_0

    .line 289
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "show empty view"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    new-instance v3, Landroid/widget/RemoteViews;

    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f030080

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 292
    .local v3, "views":Landroid/widget/RemoteViews;
    invoke-virtual {v3, v9, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 293
    invoke-virtual {v3, v10, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 294
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    .line 295
    iput v7, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 296
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-static {v4, v0, v3, v5}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->updateDefaultImage(Landroid/content/Context;Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;Landroid/widget/RemoteViews;I)V

    .line 308
    :goto_0
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 309
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "appWidgetId"

    iget v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 310
    const-string/jumbo v4, "widget-type"

    iget v5, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 311
    const-string v4, "frame-type"

    iget v5, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 312
    const-string v4, "album-path"

    iget-object v5, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 313
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "widget://gallery/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 315
    const v4, 0x7f0f0170

    invoke-virtual {v3, v4, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 316
    const v4, 0x7f0f01e8

    const/4 v5, 0x4

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 317
    const v4, 0x7f0f016e

    invoke-virtual {v3, v4, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 318
    const v4, 0x7f0f016a

    invoke-virtual {v3, v4, v1}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    .line 319
    const v4, 0x7f0f016f

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->createMagazineCameraPIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 321
    const v4, 0x7f0f01ed

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->createMagazinePreferencePIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 324
    iget v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-virtual {v2, v4, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 325
    return-void

    .line 298
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "views":Landroid/widget/RemoteViews;
    :cond_0
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "show content view"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    new-instance v3, Landroid/widget/RemoteViews;

    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getIntervalLayoutResId(Landroid/content/Context;I)I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 302
    .restart local v3    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {v3, v10, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 303
    invoke-virtual {v3, v9, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 304
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    goto/16 :goto_0
.end method


# virtual methods
.method protected createClickPIntent(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/high16 v3, 0x8000000

    .line 509
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/gallery3d/gadget/WidgetClickHandler;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 513
    .local v0, "clickIntent":Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-static {p1, v1, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public getCount()I
    .locals 7

    .prologue
    .line 266
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v2

    .line 267
    .local v2, "size":I
    if-lez v2, :cond_1

    .line 269
    iget v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFrameType:I

    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getSubframeCount(I)I

    move-result v3

    .line 270
    .local v3, "subframeCount":I
    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    .line 271
    div-int v0, v2, v3

    .line 272
    .local v0, "dividend":I
    rem-int v1, v2, v3

    .line 273
    .local v1, "remain":I
    if-lez v1, :cond_0

    .line 274
    add-int/lit8 v0, v0, 0x1

    .line 276
    :cond_0
    move v2, v0

    .line 279
    .end local v0    # "dividend":I
    .end local v1    # "remain":I
    .end local v3    # "subframeCount":I
    :cond_1
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "page count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    return v2
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 328
    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 3

    .prologue
    .line 340
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0300b5

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 341
    .local v0, "rv":Landroid/widget/RemoteViews;
    return-object v0
.end method

.method public getSingleFrame(I)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "position"    # I

    .prologue
    .line 345
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v10}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    .line 346
    .local v1, "context":Landroid/content/Context;
    const/4 v8, 0x0

    .line 347
    .local v8, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v0, 0x0

    .line 349
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v10, :cond_0

    .line 350
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v10, p1}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    .line 351
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v10}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v10

    iput v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSourceSize:I

    .line 355
    :cond_0
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    move-result v10

    if-gtz v10, :cond_3

    if-eqz v8, :cond_3

    .line 356
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v10

    const-string v11, "mFutureBitmap == null && position == 0"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    const/4 v10, 0x4

    const/4 v11, 0x0

    invoke-static {v1, v8, v10, v11}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 395
    :cond_1
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsInit:Z

    const/4 v11, 0x1

    if-ne v10, v11, :cond_b

    const/4 v10, 0x0

    iput v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFuturePosition:I

    .line 398
    :goto_0
    iput-object v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mTempContext:Landroid/content/Context;

    .line 400
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    if-nez v10, :cond_2

    .line 401
    new-instance v10, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;

    invoke-direct {v10, p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;-><init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)V

    iput-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    .line 434
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 435
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 437
    :cond_2
    return-object v0

    .line 359
    :cond_3
    if-eqz v8, :cond_1

    .line 360
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 361
    const/4 v10, 0x4

    const/4 v11, 0x0

    invoke-static {v1, v8, v10, v11}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 373
    :cond_4
    :goto_1
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;

    if-nez v10, :cond_1

    iget-boolean v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsInit:Z

    if-nez v10, :cond_1

    .line 374
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 375
    .local v4, "futureIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 376
    .local v9, "removeIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    iget v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->QUEUE_COUNT_FUTURE_BMP:I

    if-ge v5, v10, :cond_7

    add-int v10, p1, v5

    iget v11, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSourceSize:I

    rem-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 362
    .end local v4    # "futureIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v5    # "i":I
    .end local v9    # "removeIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_5
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 363
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_6

    .line 364
    const/4 v10, 0x4

    const/4 v11, 0x0

    invoke-static {v1, v8, v10, v11}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 365
    iget-object v11, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    monitor-enter v11

    .line 366
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    monitor-exit v11

    goto :goto_1

    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 370
    :cond_6
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    iget-object v11, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_1

    .line 378
    .restart local v4    # "futureIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v5    # "i":I
    .restart local v9    # "removeIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_7
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 379
    .local v6, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;>;"
    :cond_8
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 380
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 381
    .local v3, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 382
    .local v7, "key":I
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 384
    .end local v3    # "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    .end local v7    # "key":I
    :cond_9
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 385
    .local v2, "count":I
    const/4 v7, 0x0

    .restart local v7    # "key":I
    :goto_4
    if-ge v7, v2, :cond_1

    .line 386
    iget-object v11, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    monitor-enter v11

    .line 387
    :try_start_1
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_a

    .line 388
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 389
    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    :cond_a
    monitor-exit v11

    .line 385
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 391
    :catchall_1
    move-exception v10

    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v10

    .line 396
    .end local v2    # "count":I
    .end local v4    # "futureIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v5    # "i":I
    .end local v6    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;>;"
    .end local v7    # "key":I
    .end local v9    # "removeIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_b
    add-int/lit8 v10, p1, 0x1

    iget v11, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSourceSize:I

    rem-int/2addr v10, v11

    iput v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFuturePosition:I

    goto/16 :goto_0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 21
    .param p1, "position"    # I

    .prologue
    .line 443
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "WidgetId : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", getViewAt : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v16

    .line 446
    .local v16, "token":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    .line 447
    .local v3, "context":Landroid/content/Context;
    new-instance v6, Landroid/widget/RemoteViews;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v18

    const v19, 0x7f0300b1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v6, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 449
    .local v6, "flipperFrame":Landroid/widget/RemoteViews;
    :try_start_0
    new-instance v5, Landroid/widget/RemoteViews;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFrameType:I

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getLayoutResId(I)I

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v5, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 452
    .local v5, "flipperContent":Landroid/widget/RemoteViews;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFrameType:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getSubframeCount(I)I

    move-result v14

    .line 453
    .local v14, "subframeCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v13

    .line 455
    .local v13, "size":I
    if-lez v13, :cond_9

    .line 457
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v14, :cond_9

    .line 458
    mul-int v18, p1, v14

    add-int v18, v18, v7

    rem-int v11, v18, v13

    .line 460
    .local v11, "newPosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getContentUri(I)Landroid/net/Uri;

    move-result-object v15

    .line 461
    .local v15, "uri":Landroid/net/Uri;
    if-nez v15, :cond_1

    .line 457
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 464
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->isDrm(I)Z

    move-result v9

    .line 466
    .local v9, "isdrm":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v10

    .line 467
    .local v10, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v10, :cond_6

    const-wide/32 v18, 0x40000

    move-wide/from16 v0, v18

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v18

    if-nez v18, :cond_2

    const-wide/16 v18, 0x80

    move-wide/from16 v0, v18

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v18

    if-eqz v18, :cond_6

    :cond_2
    const/4 v8, 0x1

    .line 470
    .local v8, "isSequenceShot":Z
    :goto_2
    const/4 v2, 0x0

    .line 471
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v15}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v18

    const-string v19, "/picasa/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_3

    if-eqz v9, :cond_7

    .line 472
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getImage(I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 481
    :cond_4
    :goto_3
    if-eqz v2, :cond_0

    .line 483
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v15}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExifOrientation(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v12

    .line 484
    .local v12, "rotation":I
    if-eqz v12, :cond_5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v18

    if-nez v18, :cond_5

    .line 485
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-static {v2, v12, v0}, Lcom/sec/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 487
    :cond_5
    const v18, 0x7f0f0173

    add-int v18, v18, v7

    move/from16 v0, v18

    invoke-virtual {v5, v0, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 490
    const v18, 0x7f0f0173

    add-int v18, v18, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v15}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->createClickPIntent(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v19

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 498
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "flipperContent":Landroid/widget/RemoteViews;
    .end local v7    # "i":I
    .end local v8    # "isSequenceShot":Z
    .end local v9    # "isdrm":Z
    .end local v10    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v11    # "newPosition":I
    .end local v12    # "rotation":I
    .end local v13    # "size":I
    .end local v14    # "subframeCount":I
    .end local v15    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v4

    .line 499
    .local v4, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_1
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v18

    const-string v19, "caught array index out of bound exception. data might be changed while sleep mode"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 503
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 505
    .end local v4    # "e":Ljava/lang/IndexOutOfBoundsException;
    :goto_4
    return-object v6

    .line 467
    .restart local v5    # "flipperContent":Landroid/widget/RemoteViews;
    .restart local v7    # "i":I
    .restart local v9    # "isdrm":Z
    .restart local v10    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v11    # "newPosition":I
    .restart local v13    # "size":I
    .restart local v14    # "subframeCount":I
    .restart local v15    # "uri":Landroid/net/Uri;
    :cond_6
    const/4 v8, 0x0

    goto :goto_2

    .line 474
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v8    # "isSequenceShot":Z
    :cond_7
    :try_start_2
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v18, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFrameType:I

    move/from16 v18, v0

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 475
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->getSingleFrame(I)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_3

    .line 476
    :cond_8
    if-eqz v10, :cond_4

    .line 477
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFrameType:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-static {v3, v10, v0, v7}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_3

    .line 497
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v7    # "i":I
    .end local v8    # "isSequenceShot":Z
    .end local v9    # "isdrm":Z
    .end local v10    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v11    # "newPosition":I
    .end local v15    # "uri":Landroid/net/Uri;
    :cond_9
    const v18, 0x7f0f01e9

    move/from16 v0, v18

    invoke-virtual {v6, v0, v5}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 503
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_4

    .line 500
    .end local v5    # "flipperContent":Landroid/widget/RemoteViews;
    .end local v13    # "size":I
    .end local v14    # "subframeCount":I
    :catch_1
    move-exception v4

    .line 501
    .local v4, "e":Ljava/lang/NullPointerException;
    :try_start_3
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v18

    const-string v19, "caught null pointer exception. check source is deleted"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 503
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_4

    .end local v4    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v18

    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v18
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 332
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x1

    return v0
.end method

.method public onContentDirty()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 547
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onContentDirty"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mType:I

    if-ne v0, v2, :cond_2

    .line 550
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v0

    if-ge v0, v2, :cond_1

    .line 551
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->updateEmptyView(Z)V

    .line 560
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    const v2, 0x7f0f016a

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 562
    return-void

    .line 553
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->updateEmptyView(Z)V

    goto :goto_0

    .line 555
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsEmptyViewNow:Z

    if-eqz v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v0

    if-ge v0, v2, :cond_0

    .line 557
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->updateEmptyView(Z)V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 190
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->initSource()V

    .line 193
    return-void
.end method

.method public onDataSetChanged()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 518
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string v5, "onDataSetChanged"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 521
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 522
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    iget v4, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    iput v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mType:I

    .line 523
    iget v4, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    iput v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFrameType:I

    .line 524
    iget-object v4, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAlbumPath:Ljava/lang/String;

    .line 525
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 527
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->initSource()V

    .line 529
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 531
    .local v2, "token":J
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v4, :cond_0

    .line 532
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->reload()V

    .line 535
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v4

    if-ge v4, v6, :cond_1

    .line 536
    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->updateEmptyView(Z)V

    .line 537
    iget v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mAppWidgetId:I

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->clearAlbumPath(I)V

    .line 542
    :cond_0
    :goto_0
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 543
    return-void

    .line 539
    :cond_1
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->updateEmptyView(Z)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 252
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->close()V

    .line 254
    iput-object v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;

    .line 257
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 260
    iput-object v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;

    .line 263
    :cond_1
    return-void
.end method
