.class public Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "MagazineWidgetProvider.java"


# static fields
.field private static final ACTION_SEC_WIDGET_RESIZE:Ljava/lang/String; = "com.sec.android.widgetapp.APPWIDGET_RESIZE"

.field private static final ACTION_SEC_WIDGET_UPDATE:Ljava/lang/String; = "android.appwidget.action.APPWIDGET_UPDATE"

.field private static final TAG:Ljava/lang/String; = "MagazineWidgetProvider"

.field private static mAppWidgetId:I

.field private static mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

.field private static spanX:I

.field private static spanY:I

.field private static targetWidgetId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    sput v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->spanX:I

    sput v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->spanY:I

    sput v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->targetWidgetId:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method public static buildAndUpdateWidget(Landroid/content/Context;ILcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .param p2, "entry"    # Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    .prologue
    .line 162
    sput-object p2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    .line 163
    sput p1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    .line 164
    sget v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->spanX:I

    sget v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->spanY:I

    invoke-static {p0, v2, v3}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->updateViews(Landroid/content/Context;II)Landroid/widget/RemoteViews;

    move-result-object v1

    .line 165
    .local v1, "views":Landroid/widget/RemoteViews;
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 166
    .local v0, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v0, p1, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 167
    const v2, 0x7f0f016a

    invoke-virtual {v0, p1, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 168
    return-void
.end method

.method public static magazineWidgetRotation(Landroid/content/Context;III)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "widgetId"    # I
    .param p2, "spanX"    # I
    .param p3, "spanY"    # I

    .prologue
    .line 151
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 152
    .local v0, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 153
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    sget v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v3

    sput-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    .line 154
    invoke-static {p0, p2, p3}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->updateViews(Landroid/content/Context;II)Landroid/widget/RemoteViews;

    move-result-object v2

    .line 155
    .local v2, "views":Landroid/widget/RemoteViews;
    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 156
    sget v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->targetWidgetId:I

    invoke-virtual {v0, v3, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 157
    sget v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    const v4, 0x7f0f016a

    invoke-virtual {v0, v3, v4}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 158
    const/4 v3, 0x1

    return v3
.end method

.method public static updateDefaultImage(Landroid/content/Context;Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;Landroid/widget/RemoteViews;I)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "entry"    # Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    .param p2, "views"    # Landroid/widget/RemoteViews;
    .param p3, "widgetId"    # I

    .prologue
    .line 124
    sget v4, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->spanX:I

    sget v5, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->spanY:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->updateDefaultImage(Landroid/content/Context;Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;Landroid/widget/RemoteViews;III)V

    .line 125
    return-void
.end method

.method private static updateDefaultImage(Landroid/content/Context;Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;Landroid/widget/RemoteViews;III)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "entry"    # Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    .param p2, "views"    # Landroid/widget/RemoteViews;
    .param p3, "widgetId"    # I
    .param p4, "SpanX"    # I
    .param p5, "SpanY"    # I

    .prologue
    const v6, 0x7f0f016d

    const/4 v4, 0x2

    const/4 v5, 0x0

    .line 128
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0203a4

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 129
    .local v0, "bm":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .line 130
    .local v1, "cropedImage":Landroid/graphics/Bitmap;
    sput p4, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->spanX:I

    .line 131
    sput p5, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->spanY:I

    .line 132
    if-le p4, v4, :cond_0

    .line 133
    const/4 v2, 0x5

    iput v2, p1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    .line 134
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    invoke-static {v0, v5, v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 146
    :goto_0
    invoke-virtual {p2, v6, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 147
    invoke-static {p0, p3}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->createMagazineWidgetPIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {p2, v6, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 148
    return-void

    .line 136
    :cond_0
    if-le p5, v4, :cond_1

    .line 137
    const/4 v2, 0x6

    iput v2, p1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    .line 138
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v0, v2, v5, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    .line 141
    :cond_1
    const/4 v2, 0x4

    iput v2, p1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    .line 142
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    invoke-static {v0, v2, v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method private static updateViews(Landroid/content/Context;II)Landroid/widget/RemoteViews;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "SpanX"    # I
    .param p2, "SpanY"    # I

    .prologue
    const v8, 0x7f0f016c

    const v5, 0x7f0f0169

    const/16 v4, 0x8

    const/4 v7, 0x0

    .line 85
    const-string v0, "MagazineWidgetProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "build Multiframe for widget : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    new-instance v6, Landroid/content/Intent;

    const-class v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;

    invoke-direct {v6, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 87
    .local v6, "intent":Landroid/content/Intent;
    const-string v0, "appWidgetId"

    sget v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 88
    const-string/jumbo v0, "widget-type"

    sget-object v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    iget v1, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 89
    const-string v0, "frame-type"

    sget-object v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    iget v1, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 90
    const-string v0, "album-path"

    sget-object v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "widget://gallery/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 96
    sget-object v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    iget v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    iget v0, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->isEmpty(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    const/4 v1, 0x2

    iput v1, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->showInterval:I

    .line 99
    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f030080

    invoke-direct {v2, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 101
    .local v2, "views":Landroid/widget/RemoteViews;
    invoke-virtual {v2, v5, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 102
    invoke-virtual {v2, v8, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 104
    sget v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->createMagazineWidgetPIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;

    .line 111
    :goto_0
    sget-object v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    sget v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    move-object v0, p0

    move v4, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->updateDefaultImage(Landroid/content/Context;Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;Landroid/widget/RemoteViews;III)V

    .line 112
    const-string v0, "frame-type"

    sget-object v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    iget v1, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 114
    const v0, 0x7f0f0170

    invoke-virtual {v2, v0, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 115
    const v0, 0x7f0f016a

    invoke-virtual {v2, v0, v6}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    .line 116
    const v0, 0x7f0f016e

    invoke-virtual {v2, v0, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 117
    const v0, 0x7f0f016f

    sget v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    invoke-static {p0, v1}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->createMagazineCameraPIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 120
    return-object v2

    .line 106
    .end local v2    # "views":Landroid/widget/RemoteViews;
    :cond_1
    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    invoke-static {p0, v1}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getIntervalLayoutResId(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v2, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 107
    .restart local v2    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {v2, v8, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 108
    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method


# virtual methods
.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetId"    # I
    .param p4, "newOptions"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1, p2, p3, p4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    .line 78
    return-void
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 82
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 70
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 66
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    .line 29
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_0

    .line 61
    :goto_0
    return-void

    .line 32
    :cond_0
    const-string/jumbo v8, "widgetId"

    const/4 v9, -0x1

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    sput v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->targetWidgetId:I

    .line 33
    const-string v8, "MagazineWidgetProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "GalleryWidget "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->targetWidgetId:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    const-string v9, "com.sec.android.widgetapp.APPWIDGET_RESIZE"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    const-string v9, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 37
    :cond_1
    const-string/jumbo v8, "widgetspanx"

    invoke-virtual {p2, v8, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 38
    .local v6, "x":I
    const-string/jumbo v8, "widgetspany"

    invoke-virtual {p2, v8, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 40
    .local v7, "y":I
    sput v6, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->spanX:I

    .line 41
    sput v7, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->spanY:I

    .line 42
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v5

    .line 43
    .local v5, "widgetMgr":Landroid/appwidget/AppWidgetManager;
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-class v9, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .local v0, "cn":Landroid/content/ComponentName;
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v1, p1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 46
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    invoke-virtual {v5, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v4

    .line 47
    .local v4, "widgetIds":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v8, v4

    if-ge v2, v8, :cond_3

    .line 48
    const-string v8, "MagazineWidgetProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "GalleryWidget "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget v10, v4, v2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    aget v8, v4, v2

    sget v9, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->targetWidgetId:I

    if-ne v8, v9, :cond_2

    .line 50
    aget v8, v4, v2

    sput v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    .line 51
    sget v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    invoke-virtual {v1, v8}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v8

    sput-object v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    .line 52
    sget v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->spanX:I

    sget v9, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->spanY:I

    invoke-static {p1, v8, v9}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->updateViews(Landroid/content/Context;II)Landroid/widget/RemoteViews;

    move-result-object v3

    .line 53
    .local v3, "views":Landroid/widget/RemoteViews;
    sget-object v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mEntry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    invoke-virtual {v1, v8}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 54
    sget v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    invoke-virtual {v5, v8, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 55
    sget v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->mAppWidgetId:I

    const v9, 0x7f0f016a

    invoke-virtual {v5, v8, v9}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 47
    .end local v3    # "views":Landroid/widget/RemoteViews;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 60
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v1    # "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    .end local v2    # "i":I
    .end local v4    # "widgetIds":[I
    .end local v5    # "widgetMgr":Landroid/appwidget/AppWidgetManager;
    .end local v6    # "x":I
    .end local v7    # "y":I
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 73
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 74
    return-void
.end method
