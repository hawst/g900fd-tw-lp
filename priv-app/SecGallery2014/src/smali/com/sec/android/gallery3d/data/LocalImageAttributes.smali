.class public Lcom/sec/android/gallery3d/data/LocalImageAttributes;
.super Ljava/lang/Object;
.source "LocalImageAttributes.java"


# static fields
.field public static final COLUMN_SEF_FILE_TPYE:Ljava/lang/String; = "sef_file_type"

.field public static final SEF_FILE_TYPE_3DTOUR:I = 0x850

.field public static final SEF_FILE_TYPE_BESTFACE:I = 0x832

.field public static final SEF_FILE_TYPE_BESTPHOTO:I = 0x831

.field public static final SEF_FILE_TYPE_DRAMASHOT:I = 0x834

.field public static final SEF_FILE_TYPE_ERASER:I = 0x833

.field public static final SEF_FILE_TYPE_MAGICSHOT:I = 0x830

.field public static final SEF_FILE_TYPE_NONE:I = -0x1

.field public static final SEF_FILE_TYPE_OUTOFFOCUS:I = 0x840

.field public static final SEF_FILE_TYPE_PICMOTION:I = 0x835

.field public static final SEF_FILE_TYPE_SEQUENCE:I = 0x870

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/LocalImage;)V
    .locals 0
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "localImage"    # Lcom/sec/android/gallery3d/data/LocalImage;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 44
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    .line 45
    return-void
.end method

.method private readAttributFromByteBuffer(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z
    .locals 12
    .param p1, "buffer"    # Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    .prologue
    const-wide/32 v10, 0x80000

    const-wide/16 v8, 0x20

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 206
    iget v3, p1, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    if-gtz v3, :cond_0

    .line 240
    :goto_0
    return v1

    .line 210
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p1, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    if-ge v0, v3, :cond_4

    .line 211
    packed-switch v0, :pswitch_data_0

    .line 210
    :goto_2
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 219
    :pswitch_1
    iget-object v3, p1, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    iget v4, p1, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    if-nez v3, :cond_1

    .line 220
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v3, v10, v11, v1}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    goto :goto_2

    .line 222
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v3, v10, v11, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    goto :goto_2

    .line 225
    :pswitch_2
    iget-object v3, p1, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    iget v4, p1, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    if-nez v3, :cond_2

    .line 226
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iput-boolean v1, v3, Lcom/sec/android/gallery3d/data/LocalImage;->is3dpanorama:Z

    .line 229
    :goto_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v4, 0x8

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-boolean v6, v6, Lcom/sec/android/gallery3d/data/LocalImage;->is3dpanorama:Z

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    goto :goto_2

    .line 228
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iput-boolean v2, v3, Lcom/sec/android/gallery3d/data/LocalImage;->is3dpanorama:Z

    goto :goto_3

    .line 232
    :pswitch_3
    iget-object v3, p1, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    iget v4, p1, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    if-nez v3, :cond_3

    .line 233
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v3, v8, v9, v1}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    goto :goto_2

    .line 235
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v3, v8, v9, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    goto :goto_2

    :cond_4
    move v1, v2

    .line 240
    goto :goto_0

    .line 211
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private udpatePhotonoteN3DPanoramaAttribute()V
    .locals 8

    .prologue
    .line 175
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    if-nez v4, :cond_0

    .line 202
    :goto_0
    return-void

    .line 178
    :cond_0
    const/4 v1, 0x0

    .line 179
    .local v1, "file":Ljava/io/RandomAccessFile;
    const/4 v3, 0x0

    .line 181
    .local v3, "note_info":I
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    const-string v5, "r"

    invoke-direct {v2, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    .end local v1    # "file":Ljava/io/RandomAccessFile;
    .local v2, "file":Ljava/io/RandomAccessFile;
    :try_start_1
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    if-eqz v4, :cond_1

    .line 185
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->check3DPanorama(Ljava/io/RandomAccessFile;)Z

    move-result v5

    iput-boolean v5, v4, Lcom/sec/android/gallery3d/data/LocalImage;->is3dpanorama:Z

    .line 186
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v6, 0x8

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-boolean v5, v5, Lcom/sec/android/gallery3d/data/LocalImage;->is3dpanorama:Z

    invoke-virtual {v4, v6, v7, v5}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 189
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v4, :cond_2

    .line 190
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteUtils;->getPhotoNoteType(Ljava/io/RandomAccessFile;)I

    move-result v3

    .line 191
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/32 v6, 0x80000

    if-lez v3, :cond_3

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v5, v6, v7, v4}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 196
    :cond_2
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v1, v2

    .line 197
    .end local v2    # "file":Ljava/io/RandomAccessFile;
    .restart local v1    # "file":Ljava/io/RandomAccessFile;
    goto :goto_0

    .line 191
    .end local v1    # "file":Ljava/io/RandomAccessFile;
    .restart local v2    # "file":Ljava/io/RandomAccessFile;
    :cond_3
    const/4 v4, 0x0

    goto :goto_1

    .line 193
    .end local v2    # "file":Ljava/io/RandomAccessFile;
    .restart local v1    # "file":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 196
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v4

    :goto_3
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v4

    .end local v1    # "file":Ljava/io/RandomAccessFile;
    .restart local v2    # "file":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "file":Ljava/io/RandomAccessFile;
    .restart local v1    # "file":Ljava/io/RandomAccessFile;
    goto :goto_3

    .line 193
    .end local v1    # "file":Ljava/io/RandomAccessFile;
    .restart local v2    # "file":Ljava/io/RandomAccessFile;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "file":Ljava/io/RandomAccessFile;
    .restart local v1    # "file":Ljava/io/RandomAccessFile;
    goto :goto_2
.end method

.method private update3DMPOAttribute()V
    .locals 4

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v2, 0x2

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v1, v1, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/BitmapUtils;->isMpoSupported(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 88
    return-void
.end method

.method private updateDrmAttribute()V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    if-nez v0, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v2, v2, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isDrm(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/data/LocalImage;->isdrm:Z

    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->isdrm:Z

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v2, v2, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDRMInfo(Ljava/lang/String;)Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/gallery3d/data/LocalImage;->drmInfo:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;

    goto :goto_0
.end method

.method private updateGolfAttribute()V
    .locals 4

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->isGolf()Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/data/LocalImage;->mIsGolf:Z

    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v2, 0x80

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/data/LocalImage;->mIsGolf:Z

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 84
    return-void
.end method

.method private updatePendingAttribute()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 91
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v2, 0x20

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/android/gallery3d/data/LocalImage;->setPendingAttribute(JZ)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->groupId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v2, 0x200

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/android/gallery3d/data/LocalImage;->setPendingAttribute(JZ)V

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v2, 0x8

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/android/gallery3d/data/LocalImage;->setPendingAttribute(JZ)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/32 v2, 0x80000

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/android/gallery3d/data/LocalImage;->setPendingAttribute(JZ)V

    .line 97
    return-void
.end method

.method private updateSeffileAttribute()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x400

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 100
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DTour:Z

    if-eqz v0, :cond_2

    .line 101
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    const/16 v4, 0x850

    if-ne v0, v4, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v6, v7, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 105
    :goto_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v4, 0x800

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    const/16 v6, 0x830

    if-ne v0, v6, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v4, v5, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 106
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraShotAndMore:Z

    if-eqz v0, :cond_0

    .line 107
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v4, 0x1000

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    const/16 v6, 0x831

    if-ne v0, v6, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v3, v4, v5, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 108
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v4, 0x2000

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    const/16 v6, 0x832

    if-ne v0, v6, :cond_5

    move v0, v1

    :goto_4
    invoke-virtual {v3, v4, v5, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 109
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v4, 0x4000

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    const/16 v6, 0x833

    if-ne v0, v6, :cond_6

    move v0, v1

    :goto_5
    invoke-virtual {v3, v4, v5, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 110
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/32 v4, 0x8000

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    const/16 v6, 0x834

    if-ne v0, v6, :cond_7

    move v0, v1

    :goto_6
    invoke-virtual {v3, v4, v5, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 111
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/32 v4, 0x10000

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    const/16 v6, 0x835

    if-ne v0, v6, :cond_8

    move v0, v1

    :goto_7
    invoke-virtual {v3, v4, v5, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 113
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/32 v4, 0x20000

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    const/16 v6, 0x840

    if-ne v0, v6, :cond_9

    move v0, v1

    :goto_8
    invoke-virtual {v3, v4, v5, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/32 v4, 0x40000

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget v3, v3, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    const/16 v6, 0x870

    if-ne v3, v6, :cond_a

    :goto_9
    invoke-virtual {v0, v4, v5, v1}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 115
    return-void

    :cond_1
    move v0, v2

    .line 101
    goto/16 :goto_0

    .line 103
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v0, v6, v7, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    goto/16 :goto_1

    :cond_3
    move v0, v2

    .line 105
    goto/16 :goto_2

    :cond_4
    move v0, v2

    .line 107
    goto :goto_3

    :cond_5
    move v0, v2

    .line 108
    goto :goto_4

    :cond_6
    move v0, v2

    .line 109
    goto :goto_5

    :cond_7
    move v0, v2

    .line 110
    goto :goto_6

    :cond_8
    move v0, v2

    .line 111
    goto :goto_7

    :cond_9
    move v0, v2

    .line 113
    goto :goto_8

    :cond_a
    move v1, v2

    .line 114
    goto :goto_9
.end method

.method private updateSoundShotAttribute()V
    .locals 4

    .prologue
    .line 125
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSoundScene:Z

    if-nez v0, :cond_0

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v1, v1, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundScene(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/data/LocalImage;->isSoundScene:Z

    .line 129
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v2, 0x10

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/data/LocalImage;->isSoundScene:Z

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    goto :goto_0
.end method

.method private updateSphericalMosaicAttribute()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->sphericalMosaic:I

    if-eqz v0, :cond_0

    .line 119
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v4, 0x8

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->sphericalMosaic:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v4, v5, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 122
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 119
    goto :goto_0
.end method

.method private writeAttributeToByteBuffer()Ljava/nio/ByteBuffer;
    .locals 12

    .prologue
    .line 245
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v10, 0x10

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v3

    .line 246
    .local v3, "isSound":Z
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/32 v10, 0x80000

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v2

    .line 247
    .local v2, "isPhotoNote":Z
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v10, 0x20

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v1

    .line 248
    .local v1, "isAGIF":Z
    const/4 v4, 0x0

    .line 249
    .local v4, "metadataarray":[B
    const/4 v8, 0x0

    .line 252
    .local v8, "target":Ljava/nio/ByteBuffer;
    :try_start_0
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 253
    .local v7, "stream":Ljava/io/ByteArrayOutputStream;
    new-instance v6, Ljava/io/DataOutputStream;

    invoke-direct {v6, v7}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 255
    .local v6, "out":Ljava/io/DataOutputStream;
    invoke-virtual {v6, v3}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 256
    invoke-virtual {v6, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 257
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-boolean v9, v9, Lcom/sec/android/gallery3d/data/LocalImage;->is3dpanorama:Z

    invoke-virtual {v6, v9}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 258
    invoke-virtual {v6, v1}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 259
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 261
    invoke-virtual {v6}, Ljava/io/DataOutputStream;->close()V

    .line 262
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    .end local v6    # "out":Ljava/io/DataOutputStream;
    .end local v7    # "stream":Ljava/io/ByteArrayOutputStream;
    :goto_0
    const/4 v5, 0x0

    .line 268
    .local v5, "metadatalength":I
    if-eqz v4, :cond_1

    .line 269
    array-length v5, v4

    .line 271
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 272
    if-eqz v4, :cond_0

    .line 273
    invoke-virtual {v8, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 279
    :cond_0
    :goto_1
    return-object v8

    .line 263
    .end local v5    # "metadatalength":I
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 276
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v5    # "metadatalength":I
    :cond_1
    sget-object v9, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "writeAttributeToByteBuffer() metadataarray is null"

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method updateAttributes()V
    .locals 2

    .prologue
    .line 48
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->resetPendingAttribute()V

    .line 50
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "filePath":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 53
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->updateDrmAttribute()V

    .line 55
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->updateGolfAttribute()V

    .line 57
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->updateSoundShotAttribute()V

    .line 59
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->update3DMPOAttribute()V

    .line 62
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->updatePendingAttribute()V

    .line 64
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->updateSeffileAttribute()V

    .line 66
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->updateSphericalMosaicAttribute()V

    .line 69
    :cond_0
    return-void
.end method

.method public updateFileAccessAttribute()V
    .locals 15

    .prologue
    const-wide/16 v6, 0x20

    const/4 v4, 0x6

    .line 133
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v1, v1, Lcom/sec/android/gallery3d/data/LocalImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v1, v1, Lcom/sec/android/gallery3d/data/LocalImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 135
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v0

    .line 136
    .local v0, "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getAttributeBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->get()Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    move-result-object v5

    .line 137
    .local v5, "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v1, v1, Lcom/sec/android/gallery3d/data/LocalImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-wide v2, v2, Lcom/sec/android/gallery3d/data/LocalImage;->dateModifiedInSec:J

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ImageCacheService;->getImageData(Lcom/sec/android/gallery3d/data/Path;JILcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z

    move-result v12

    .line 139
    .local v12, "found":Z
    const/4 v13, 0x0

    .line 142
    .local v13, "result":Z
    if-eqz v12, :cond_0

    .line 143
    :try_start_0
    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->readAttributFromByteBuffer(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v13

    .line 146
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getAttributeBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 149
    if-eqz v12, :cond_1

    if-nez v13, :cond_2

    .line 151
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->udpatePhotonoteN3DPanoramaAttribute()V

    .line 153
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-static {v2, v3}, Lcom/quramsoft/agif/QURAMWINKUTIL;->isAGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v2

    invoke-virtual {v1, v6, v7, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    .line 155
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->writeAttributeToByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v14

    .line 156
    .local v14, "target":Ljava/nio/ByteBuffer;
    if-eqz v14, :cond_2

    .line 157
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v7, v1, Lcom/sec/android/gallery3d/data/LocalImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-wide v8, v1, Lcom/sec/android/gallery3d/data/LocalImage;->dateModifiedInSec:J

    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v11

    move-object v6, v0

    move v10, v4

    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/gallery3d/data/ImageCacheService;->putImageData(Lcom/sec/android/gallery3d/data/Path;JI[B)V

    .line 171
    .end local v0    # "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    .end local v5    # "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    .end local v12    # "found":Z
    .end local v13    # "result":Z
    .end local v14    # "target":Ljava/nio/ByteBuffer;
    :cond_2
    :goto_0
    return-void

    .line 146
    .restart local v0    # "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    .restart local v5    # "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    .restart local v12    # "found":Z
    .restart local v13    # "result":Z
    :catchall_0
    move-exception v1

    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getAttributeBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    throw v1

    .line 167
    .end local v0    # "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    .end local v5    # "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    .end local v12    # "found":Z
    .end local v13    # "result":Z
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->udpatePhotonoteN3DPanoramaAttribute()V

    .line 169
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-static {v2, v3}, Lcom/quramsoft/agif/QURAMWINKUTIL;->isAGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v2

    invoke-virtual {v1, v6, v7, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->setAttribute(JZ)V

    goto :goto_0
.end method
