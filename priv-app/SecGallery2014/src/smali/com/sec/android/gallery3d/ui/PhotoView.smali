.class public Lcom/sec/android/gallery3d/ui/PhotoView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "PhotoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;,
        Lcom/sec/android/gallery3d/ui/PhotoView$ImageData;,
        Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;,
        Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;,
        Lcom/sec/android/gallery3d/ui/PhotoView$ZInterpolator;,
        Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;,
        Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;,
        Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;,
        Lcom/sec/android/gallery3d/ui/PhotoView$Picture;,
        Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;,
        Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;,
        Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;,
        Lcom/sec/android/gallery3d/ui/PhotoView$Listener;,
        Lcom/sec/android/gallery3d/ui/PhotoView$Model;,
        Lcom/sec/android/gallery3d/ui/PhotoView$Size;
    }
.end annotation


# static fields
.field private static final CARD_EFFECT:Z = false

.field private static final COLOR_OUTLINE:I = -0xff7501

.field private static DEFAULT_TEXT_SIZE:F = 0.0f

.field private static final DELAY_SHOW_LOADING:J = 0xfaL

.field private static final DOUBLE_TAP_TIMEOUT:I

.field public static final ENTRY_NEXT:I = 0x1

.field public static final ENTRY_PREVIOUS:I = 0x0

.field private static final FACE_RECT_DIMENSION:I = 0x96

.field private static final FACE_RECT_DIMENSION_SMALL:I = 0x4b

.field private static final HELP_ANIMATION_HEAD_START_OFFSET:I = 0x384

.field private static final HELP_ANIMATION_START_OFFSET:I = 0x2bc

.field private static final HELP_TILT_THRESHOLD:J = 0xfL

.field public static final HELP_VIEWING_PICTURES_SCROLL_STATE:I = 0x0

.field public static final HELP_VIEWING_PICTURES_ZOOM_IN_STATE:I = 0x1

.field public static final HELP_VIEWING_PICTURES_ZOOM_OUT_STATE:I = 0x2

.field private static final HOLD_CAPTURE_ANIMATION:I = 0x2

.field private static final HOLD_DELETE:I = 0x4

.field private static final HOLD_TOUCH_DOWN:I = 0x1

.field public static final IGNORE_FLING_EVENT_THRESHOLD_TIME:J = 0x12cL

.field public static final INVALID_DATA_VERSION:J = -0x1L

.field public static final INVALID_SIZE:I = -0x1

.field private static final LOADING_COMPLETE:I = 0x2

.field private static final LOADING_FAIL:I = 0x3

.field private static final LOADING_INIT:I = 0x0

.field private static LOADING_PROGRESS_NONE:I = 0x0

.field private static LOADING_PROGRESS_RUNNING:I = 0x0

.field private static final LOADING_TIMEOUT:I = 0x1

.field private static final MAX_DISMISS_VELOCITY:I = 0x9c4

.field private static final MIN_PANNING_DISTANCE:I = 0x28

.field private static final MOTION_PANNING_THRESHOLD:J = 0x3L

.field private static final MOTION_TILT_THRESHOLD:J = 0x3L

.field private static final MSG_CANCEL_EXTRA_SCALING:I = 0x2

.field private static final MSG_CAPTURE_ANIMATION_DONE:I = 0x4

.field private static final MSG_DELETE_ANIMATION_DONE:I = 0x5

.field private static final MSG_DELETE_DONE:I = 0x6

.field private static final MSG_HIDE_CAMERA_PREVIEW:I = 0x69

.field private static final MSG_HIDE_PREVIEW:I = 0x68

.field private static final MSG_SHOW_FAIL:I = 0x67

.field private static final MSG_SHOW_LOADING:I = 0x66

.field private static final MSG_SWITCH_FOCUS:I = 0x3

.field private static final MSG_TRANSITION_COMPLETE:I = 0x65

.field private static final MSG_UNDO_BAR_FULL_CAMERA:I = 0x8

.field private static final MSG_UNDO_BAR_TIMEOUT:I = 0x7

.field private static final OFFSET_EFFECT:Z = true

.field private static final OUTLINE_WIDTH:F = 30.0f

.field public static final SCREEN_NAIL_MAX:I = 0x3

.field private static final SCROLL_THRESHOLD_RATIO:F = 0.016f

.field private static final SLIDE_NEXT_IMAGE:I = 0x1

.field private static final SLIDE_PREV_IMAGE:I = -0x1

.field private static final SWIPE_ESCAPE_VELOCITY:I = 0x1f4

.field private static final SWIPE_MIN_THRESHOLD:I

.field private static final SWIPE_THRESHOLD:F = 300.0f

.field private static final TAG:Ljava/lang/String;

.field private static TRANSITION_SCALE_FACTOR:F = 0.0f

.field public static final TRANS_NONE:I = 0x0

.field public static final TRANS_OPEN_ANIMATION:I = 0x5

.field public static final TRANS_SLIDE_IN_LEFT:I = 0x2

.field public static final TRANS_SLIDE_IN_RIGHT:I = 0x1

.field private static final TRANS_SWITCH_NEXT:I = 0x3

.field private static final TRANS_SWITCH_PREVIOUS:I = 0x4

.field private static final TUTORIAL_FINISH:I = 0x64

.field private static final TUTORIAL_FINISH_TIME:I = 0x7d0

.field private static final UNDO_BAR_DELETE_LAST:I = 0x10

.field private static final UNDO_BAR_FULL_CAMERA:I = 0x8

.field private static final UNDO_BAR_SHOW:I = 0x1

.field private static final UNDO_BAR_TIMEOUT:I = 0x2

.field private static final UNDO_BAR_TOUCHED:I = 0x4

.field public static final UNSPECIFIED:F = -1.0f

.field private static final ZOOM_OUT_EFFECT:Z = true

.field private static final ZOOM_OUT_THRESHOLD_MAX:F = 1.2f

.field private static mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private static mDragUri:Landroid/net/Uri;

.field private static mGetPrevFocusX:F

.field private static mGetPrevFocusY:F


# instance fields
.field private final TIME_TEXT_SIZE:F

.field public isCtrlKeyPressed:Z

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mAdaptDisplayDialog:Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;

.field private mAgifMode:Z

.field private mAlphaInterpolator:Landroid/view/animation/AccelerateInterpolator;

.field private mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field private mBubbleHeadAnimation:Landroid/view/animation/Animation;

.field private mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

.field private mCameraRect:Landroid/graphics/Rect;

.field private mCameraRelativeFrame:Landroid/graphics/Rect;

.field private mCancelExtraScalingPending:Z

.field private mCenterX:F

.field private mCenterY:F

.field private mCompensation:I

.field private mContext:Landroid/content/Context;

.field mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

.field private mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mCurrentPointAnimation_1:I

.field private mCurrentPointAnimation_2:I

.field private mDecodeImageFinished:Z

.field private mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

.field private mDisplay:Landroid/view/Display;

.field private mDisplayHeight:I

.field private mDisplayRotation:I

.field private mDisplayWidth:I

.field private mDragAndDropDialog:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

.field private mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

.field private mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

.field private mFilmMode:Z

.field private mFilmstripJump:Z

.field private mFinishHandler:Landroid/os/Handler;

.field private mFirst:Z

.field private mFirstFaceTagDialog:Z

.field private mFirstShowDialog:Z

.field private mFullScreenCamera:Z

.field private mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

.field private final mGestureListener:Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;

.field private final mGestureRecognizer:Lcom/sec/android/gallery3d/ui/GestureRecognizer;

.field private mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

.field private mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

.field private mHdmiListener:Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;

.field private mHelpMode:I

.field private mHelpViewingMode:I

.field private mHolding:I

.field private mIgnoreMenuItemPressed:Z

.field private mIgnoreScroll:Z

.field private mIgnoreUpEvent:Z

.field private mImageRotation:I

.field private mIsBackkeyPressed:Z

.field private mIsDmrEnabled:Z

.field private mIsOffContextualTag:Z

.field private mIsOnScale:Z

.field private mIsOperatePanning:Z

.field private mIsOperatePeek:Z

.field private mIsPaused:Z

.field private mIsPressed:Z

.field private mIsRotated:Z

.field private mIsSelectionMode:Z

.field private mIsShowBars:Z

.field private mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

.field private mLoadImageFinished:Z

.field private mLoadingProgressState:I

.field private mLoadingSpinner:Lcom/sec/android/gallery3d/ui/ProgressSpinner;

.field private mLoadingState:I

.field private mLoadingText:Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;

.field private mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

.field private mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

.field private mMotionPanningXSum:I

.field private mMotionPanningYSum:I

.field private mMotionScaleBegin:Z

.field private mMotionTiltSum:I

.field private mMpfExtract:Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;

.field private mMultiTouch:Z

.field private mMyGestureLongPressDetected:Z

.field private mMyScaleListener:Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;

.field private mNextBound:I

.field private mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

.field private mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

.field private mPanningStartTime:J

.field private mPeekStartTime:J

.field private mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

.field private final mPictures:Lcom/sec/android/gallery3d/util/RangeArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/RangeArray",
            "<",
            "Lcom/sec/android/gallery3d/ui/PhotoView$Picture;",
            ">;"
        }
    .end annotation
.end field

.field private final mPlaceholderColor:I

.field private mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mPointAnimations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/animation/Animation;",
            ">;"
        }
    .end annotation
.end field

.field private final mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

.field private mPrevBound:I

.field private mPrevMoveX:F

.field private mPrevMoveY:F

.field private mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

.field private mScaleEndStartTime:J

.field private mScaleInterpolator:Lcom/sec/android/gallery3d/ui/PhotoView$ZInterpolator;

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSideMirrorViewOnShowListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;

.field private mSizes:[Lcom/sec/android/gallery3d/ui/PhotoView$Size;

.field private mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

.field private mTakenTimeLable:Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;

.field private mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

.field private mTiltZoomIn:I

.field private mTiltZoomOut:I

.field private mTouchBoxDeletable:Z

.field private mTouchBoxIndex:I

.field private mTransitionMode:I

.field private mUndoBar:Lcom/sec/android/gallery3d/ui/UndoBarView;

.field private mUndoBarState:I

.field private mUndoIndexHint:I

.field private mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

.field private mWantPictureCenterCallbacks:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 137
    const-class v0, Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;

    .line 288
    const/high16 v0, 0x41a00000    # 20.0f

    sput v0, Lcom/sec/android/gallery3d/ui/PhotoView;->DEFAULT_TEXT_SIZE:F

    .line 290
    const v0, 0x3f3d70a4    # 0.74f

    sput v0, Lcom/sec/android/gallery3d/ui/PhotoView;->TRANSITION_SCALE_FACTOR:F

    .line 397
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lcom/sec/android/gallery3d/ui/PhotoView;->DOUBLE_TAP_TIMEOUT:I

    .line 424
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_NONE:I

    .line 425
    sput v2, Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_RUNNING:I

    .line 445
    sput v1, Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusX:F

    .line 446
    sput v1, Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusY:F

    .line 523
    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v0

    sput v0, Lcom/sec/android/gallery3d/ui/PhotoView;->SWIPE_MIN_THRESHOLD:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 13
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v12, 0x0

    const-wide/16 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 558
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 303
    new-instance v4, Lcom/sec/android/gallery3d/ui/PhotoView$ZInterpolator;

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-direct {v4, v5}, Lcom/sec/android/gallery3d/ui/PhotoView$ZInterpolator;-><init>(F)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mScaleInterpolator:Lcom/sec/android/gallery3d/ui/PhotoView$ZInterpolator;

    .line 306
    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    const v5, 0x3f666666    # 0.9f

    invoke-direct {v4, v5}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAlphaInterpolator:Landroid/view/animation/AccelerateInterpolator;

    .line 319
    new-instance v4, Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v5, -0x3

    const/4 v6, 0x3

    invoke-direct {v4, v5, v6}, Lcom/sec/android/gallery3d/util/RangeArray;-><init>(II)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    .line 321
    const/4 v4, 0x7

    new-array v4, v4, [Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSizes:[Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    .line 337
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    .line 338
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mWantPictureCenterCallbacks:Z

    .line 339
    iput v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayRotation:I

    .line 340
    iput v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCompensation:I

    .line 342
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRelativeFrame:Landroid/graphics/Rect;

    .line 343
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRect:Landroid/graphics/Rect;

    .line 344
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirst:Z

    .line 364
    const v4, 0x7fffffff

    iput v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I

    .line 371
    const v4, 0x7fffffff

    iput v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoIndexHint:I

    .line 401
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAdaptDisplayDialog:Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;

    .line 403
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOnScale:Z

    .line 404
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsPressed:Z

    .line 406
    iput v12, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevMoveX:F

    .line 407
    iput v12, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevMoveY:F

    .line 413
    iput v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpMode:I

    .line 414
    iput v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpViewingMode:I

    .line 419
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    .line 423
    iput v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I

    .line 426
    sget v4, Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_NONE:I

    iput v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingProgressState:I

    .line 430
    const/4 v4, 0x2

    iput v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    .line 437
    new-instance v4, Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;-><init>()V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    .line 449
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    .line 450
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirstShowDialog:Z

    .line 453
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirstFaceTagDialog:Z

    .line 456
    iput-wide v10, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPanningStartTime:J

    .line 457
    iput-wide v10, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPeekStartTime:J

    .line 458
    iput-wide v10, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mScaleEndStartTime:J

    .line 466
    iput v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionTiltSum:I

    .line 467
    iput v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningXSum:I

    .line 468
    iput v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningYSum:I

    .line 471
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadImageFinished:Z

    .line 472
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDecodeImageFinished:Z

    .line 476
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    .line 477
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMyGestureLongPressDetected:Z

    .line 483
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    .line 486
    new-instance v4, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;

    invoke-direct {v4, p0, v8}, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/ui/PhotoView$1;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMyScaleListener:Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;

    .line 492
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionScaleBegin:Z

    .line 499
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOperatePanning:Z

    .line 500
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOperatePeek:Z

    .line 501
    iput v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    .line 502
    iput v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_2:I

    .line 505
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 506
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleHeadAnimation:Landroid/view/animation/Animation;

    .line 507
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    .line 508
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMpfExtract:Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;

    .line 510
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsDmrEnabled:Z

    .line 511
    iput v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomIn:I

    .line 512
    iput v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomOut:I

    .line 513
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsSelectionMode:Z

    .line 514
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsRotated:Z

    .line 516
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOffContextualTag:Z

    .line 517
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMultiTouch:Z

    .line 520
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreScroll:Z

    .line 521
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreUpEvent:Z

    .line 522
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreMenuItemPressed:Z

    .line 525
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmstripJump:Z

    .line 545
    new-instance v4, Lcom/sec/android/gallery3d/ui/PhotoView$1;

    invoke-direct {v4, p0}, Lcom/sec/android/gallery3d/ui/PhotoView$1;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFinishHandler:Landroid/os/Handler;

    .line 3443
    new-instance v4, Lcom/sec/android/gallery3d/ui/PhotoView$6;

    invoke-direct {v4, p0}, Lcom/sec/android/gallery3d/ui/PhotoView$6;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmiListener:Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;

    .line 3516
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->isCtrlKeyPressed:Z

    .line 4772
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsShowBars:Z

    .line 4774
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsBackkeyPressed:Z

    .line 4989
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 559
    new-instance v4, Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-direct {v4, p1}, Lcom/sec/android/gallery3d/ui/TileImageView;-><init>(Lcom/sec/android/gallery3d/app/GalleryContext;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    .line 560
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 561
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    .line 562
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b000c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPlaceholderColor:I

    .line 564
    new-instance v4, Lcom/sec/android/gallery3d/ui/EdgeView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/sec/android/gallery3d/ui/EdgeView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    .line 565
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 566
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    invoke-virtual {v4, v9}, Lcom/sec/android/gallery3d/ui/EdgeView;->setVisibility(I)V

    .line 567
    new-instance v4, Lcom/sec/android/gallery3d/ui/UndoBarView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/sec/android/gallery3d/ui/UndoBarView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBar:Lcom/sec/android/gallery3d/ui/UndoBarView;

    .line 568
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBar:Lcom/sec/android/gallery3d/ui/UndoBarView;

    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 569
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBar:Lcom/sec/android/gallery3d/ui/UndoBarView;

    invoke-virtual {v4, v9}, Lcom/sec/android/gallery3d/ui/UndoBarView;->setVisibility(I)V

    .line 570
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBar:Lcom/sec/android/gallery3d/ui/UndoBarView;

    new-instance v5, Lcom/sec/android/gallery3d/ui/PhotoView$2;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/ui/PhotoView$2;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/UndoBarView;->setOnClickListener(Lcom/sec/android/gallery3d/ui/GLView$OnClickListener;)V

    .line 578
    new-instance v4, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    .line 580
    new-instance v4, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;

    invoke-direct {v4, p0, v8}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/ui/PhotoView$1;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGestureListener:Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;

    .line 581
    new-instance v4, Lcom/sec/android/gallery3d/ui/GestureRecognizer;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGestureListener:Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;

    invoke-direct {v4, v5, v6}, Lcom/sec/android/gallery3d/ui/GestureRecognizer;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/GestureRecognizer$Listener;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGestureRecognizer:Lcom/sec/android/gallery3d/ui/GestureRecognizer;

    .line 583
    new-instance v4, Lcom/sec/android/gallery3d/ui/PositionController;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    new-instance v6, Lcom/sec/android/gallery3d/ui/PhotoView$3;

    invoke-direct {v6, p0}, Lcom/sec/android/gallery3d/ui/PhotoView$3;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    invoke-direct {v4, p1, v5, p0, v6}, Lcom/sec/android/gallery3d/ui/PositionController;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/content/Context;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/ui/PositionController$Listener;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    .line 618
    const/4 v1, -0x3

    .local v1, "i":I
    :goto_0
    const/4 v4, 0x3

    if-gt v1, v4, :cond_1

    .line 619
    if-nez v1, :cond_0

    .line 620
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    new-instance v5, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    invoke-virtual {v4, v1, v5}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    .line 618
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 622
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    new-instance v5, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;

    invoke-direct {v5, p0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;I)V

    invoke-virtual {v4, v1, v5}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 627
    :cond_1
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 628
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 629
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    .line 631
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 632
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 634
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    const v5, -0xff7501

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setColor(I)V

    .line 635
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    const/high16 v5, 0x41f00000    # 30.0f

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setLineWidth(F)V

    .line 638
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v4, :cond_7

    .line 639
    new-instance v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .line 640
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    new-instance v5, Lcom/sec/android/gallery3d/ui/PhotoView$4;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/ui/PhotoView$4;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setUpdateFaceListener(Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;)V

    .line 661
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 667
    :goto_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    if-eqz v4, :cond_2

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotionTutorialDialog:Z

    if-nez v4, :cond_2

    .line 668
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v4, v9}, Lcom/sec/android/gallery3d/app/GalleryMotion;->setMotionDialogOff(Z)V

    .line 672
    :cond_2
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v4, :cond_8

    .line 673
    new-instance v4, Lcom/sec/android/gallery3d/ui/ContextualTagView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5, v6}, Lcom/sec/android/gallery3d/ui/ContextualTagView;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    .line 674
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 675
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    if-eqz v4, :cond_4

    .line 676
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v4

    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionDialogOff()Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 677
    :cond_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->disableHelpDialog()V

    .line 685
    :cond_4
    :goto_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/dmc/ocr/OcrUtils;->isOCRAvailable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 686
    new-instance v4, Lcom/sec/android/gallery3d/ui/SpenOcrView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/android/gallery3d/ui/SpenOcrView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    .line 687
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 692
    :goto_4
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d011d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    sput v4, Lcom/sec/android/gallery3d/ui/PhotoView;->DEFAULT_TEXT_SIZE:F

    .line 693
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d011e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    iput v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->TIME_TEXT_SIZE:F

    .line 695
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->initDisplaySize(Landroid/content/Context;)V

    .line 696
    iget v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayWidth:I

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayHeight:I

    if-ge v4, v5, :cond_a

    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayWidth:I

    .line 698
    .local v2, "maxTextWidth":I
    :goto_5
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    const v5, 0x7f0e0032

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/sec/android/gallery3d/ui/PhotoView;->DEFAULT_TEXT_SIZE:F

    const/4 v6, -0x1

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-static {v4, v2, v5, v6, v7}, Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;->newInstance(Ljava/lang/String;IFILandroid/text/Layout$Alignment;)Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingText:Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;

    .line 702
    const-string v4, ""

    sget v5, Lcom/sec/android/gallery3d/ui/PhotoView;->DEFAULT_TEXT_SIZE:F

    float-to-int v5, v5

    iget v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->TIME_TEXT_SIZE:F

    const/4 v7, -0x1

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    invoke-static {v4, v5, v6, v7, v8}, Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;->newInstance(Ljava/lang/String;IFILandroid/text/Layout$Alignment;)Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTakenTimeLable:Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;

    .line 707
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 708
    .local v3, "wManager":Landroid/view/WindowManager;
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplay:Landroid/view/Display;

    .line 710
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAnimationInterface()Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    .line 711
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getCacheInterface()Lcom/sec/samsung/gallery/decoder/CacheInterface;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    .line 712
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDecoderInterface()Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .line 714
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelp(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 715
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->helpTapAnimationInit(Landroid/content/Context;)V

    .line 716
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->helpPopupAnimationInit()V

    .line 718
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->cpuBoostInit()V

    .line 719
    new-instance v4, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDragAndDropDialog:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

    .line 721
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v4, :cond_6

    .line 722
    new-instance v4, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

    invoke-direct {v4}, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;-><init>()V

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGLViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

    .line 723
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->initAccessibilityListener()V

    .line 724
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGLViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGLViewAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;->setAccessibilityNodeInfoListener(Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;)V

    .line 727
    :cond_6
    return-void

    .line 663
    .end local v2    # "maxTextWidth":I
    .end local v3    # "wManager":Landroid/view/WindowManager;
    :cond_7
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    goto/16 :goto_2

    .line 681
    :cond_8
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    goto/16 :goto_3

    .line 689
    :cond_9
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    goto/16 :goto_4

    .line 696
    :cond_a
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayHeight:I

    goto/16 :goto_5
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/ui/PhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCancelExtraScalingPending:Z

    return v0
.end method

.method static synthetic access$10000()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDragUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCancelExtraScalingPending:Z

    return p1
.end method

.method static synthetic access$10100()Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$10200(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    return v0
.end method

.method static synthetic access$10208(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    return v0
.end method

.method static synthetic access$10300(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_2:I

    return v0
.end method

.method static synthetic access$10308(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_2:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_2:I

    return v0
.end method

.method static synthetic access$10400(Lcom/sec/android/gallery3d/ui/PhotoView;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchFocus()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/gallery3d/ui/PhotoView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->captureAnimationDone(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mNextBound:I

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevBound:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->snapback()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/gallery3d/ui/PhotoView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->checkHideUndoBar(I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->onTransitionComplete()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/gallery3d/ui/PhotoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/ProgressSpinner;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getLoadingSpinner()Lcom/sec/android/gallery3d/ui/ProgressSpinner;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/TileImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPanoramaRotation()I

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getCameraRotation()I

    move-result v0

    return v0
.end method

.method static synthetic access$2400(III)I
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 136
    invoke-static {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->getRotated(III)I

    move-result v0

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/gallery3d/ui/PhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mWantPictureCenterCallbacks:Z

    return v0
.end method

.method static synthetic access$2600(III)F
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 136
    invoke-static {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->calculateMoveOutProgress(III)F

    move-result v0

    return v0
.end method

.method static synthetic access$2700(Lcom/sec/android/gallery3d/ui/PhotoView;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # F

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getScrollScale(F)F

    move-result v0

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/android/gallery3d/ui/PhotoView;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # F

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getScrollAlpha(F)F

    move-result v0

    return v0
.end method

.method static synthetic access$2900(FFF)F
    .locals 1
    .param p0, "x0"    # F
    .param p1, "x1"    # F
    .param p2, "x2"    # F

    .prologue
    .line 136
    invoke-static {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->interpolate(FFF)F

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideUndoBar()V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/gallery3d/ui/PhotoView;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # F

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getOffsetAlpha(F)F

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingProgressState:I

    return v0
.end method

.method static synthetic access$3302(Lcom/sec/android/gallery3d/ui/PhotoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingProgressState:I

    return p1
.end method

.method static synthetic access$3400()I
    .locals 1

    .prologue
    .line 136
    sget v0, Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_RUNNING:I

    return v0
.end method

.method static synthetic access$3500()I
    .locals 1

    .prologue
    .line 136
    sget v0, Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_NONE:I

    return v0
.end method

.method static synthetic access$3600(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "x2"    # Landroid/graphics/Rect;

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->drawPlaceHolder(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V

    return-void
.end method

.method static synthetic access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    return v0
.end method

.method static synthetic access$3800(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/CacheInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/DecoderInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/gallery3d/ui/PhotoView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToHitPicture(II)V

    return-void
.end method

.method static synthetic access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/gallery3d/ui/PhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirstFaceTagDialog:Z

    return v0
.end method

.method static synthetic access$4300(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->startFaceTagDialog()V

    return-void
.end method

.method static synthetic access$4400(Lcom/sec/android/gallery3d/ui/PhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsShowBars:Z

    return v0
.end method

.method static synthetic access$4500(Lcom/sec/android/gallery3d/ui/PhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreMenuItemPressed:Z

    return v0
.end method

.method static synthetic access$4502(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreMenuItemPressed:Z

    return p1
.end method

.method static synthetic access$4600(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SpenOcrView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/RangeArray;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/sec/android/gallery3d/ui/PhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->isHelpViewingScrollMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    return v0
.end method

.method static synthetic access$5000(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I

    return v0
.end method

.method static synthetic access$5100(Lcom/sec/android/gallery3d/ui/PhotoView;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-wide v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPanningStartTime:J

    return-wide v0
.end method

.method static synthetic access$5102(Lcom/sec/android/gallery3d/ui/PhotoView;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # J

    .prologue
    .line 136
    iput-wide p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPanningStartTime:J

    return-wide p1
.end method

.method static synthetic access$5200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/anim/FloatAnimation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/android/gallery3d/ui/PhotoView;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHelpTextAndState(IZ)V

    return-void
.end method

.method static synthetic access$5400(Lcom/sec/android/gallery3d/ui/PhotoView;FF)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # F
    .param p2, "x2"    # F

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->checkFlingCondition(FF)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5500(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpViewingMode:I

    return v0
.end method

.method static synthetic access$5600(Lcom/sec/android/gallery3d/ui/PhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMultiTouch:Z

    return v0
.end method

.method static synthetic access$5700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreScroll:Z

    return v0
.end method

.method static synthetic access$5702(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreScroll:Z

    return p1
.end method

.method static synthetic access$572(Lcom/sec/android/gallery3d/ui/PhotoView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    return v0
.end method

.method static synthetic access$576(Lcom/sec/android/gallery3d/ui/PhotoView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    return v0
.end method

.method static synthetic access$5800(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I

    return v0
.end method

.method static synthetic access$5802(Lcom/sec/android/gallery3d/ui/PhotoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I

    return p1
.end method

.method static synthetic access$5900(Lcom/sec/android/gallery3d/ui/PhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxDeletable:Z

    return v0
.end method

.method static synthetic access$5902(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxDeletable:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/EdgeView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    return-object v0
.end method

.method static synthetic access$6000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6100(Lcom/sec/android/gallery3d/ui/PhotoView;FF)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # F
    .param p2, "x2"    # F

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->swipeImages(FF)Z

    move-result v0

    return v0
.end method

.method static synthetic access$6200(Lcom/sec/android/gallery3d/ui/PhotoView;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-wide v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mScaleEndStartTime:J

    return-wide v0
.end method

.method static synthetic access$6202(Lcom/sec/android/gallery3d/ui/PhotoView;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # J

    .prologue
    .line 136
    iput-wide p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mScaleEndStartTime:J

    return-wide p1
.end method

.method static synthetic access$6302(Lcom/sec/android/gallery3d/ui/PhotoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoIndexHint:I

    return p1
.end method

.method static synthetic access$6400(Lcom/sec/android/gallery3d/ui/PhotoView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    return-object v0
.end method

.method static synthetic access$6600(Lcom/sec/android/gallery3d/ui/PhotoView;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-wide v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPeekStartTime:J

    return-wide v0
.end method

.method static synthetic access$6602(Lcom/sec/android/gallery3d/ui/PhotoView;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # J

    .prologue
    .line 136
    iput-wide p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPeekStartTime:J

    return-wide p1
.end method

.method static synthetic access$6700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->snapToNeighborImage()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6802(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMyGestureLongPressDetected:Z

    return p1
.end method

.method static synthetic access$6900(Lcom/sec/android/gallery3d/ui/PhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsSelectionMode:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    return-object v0
.end method

.method static synthetic access$7000(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$7100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$7200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDragAndDropDialog:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

    return-object v0
.end method

.method static synthetic access$7300(Lcom/sec/android/gallery3d/ui/PhotoView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->showDragAndDropDialog(II)V

    return-void
.end method

.method static synthetic access$7400(Lcom/sec/android/gallery3d/ui/PhotoView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->startManualFD(II)V

    return-void
.end method

.method static synthetic access$7500(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->startDrag()V

    return-void
.end method

.method static synthetic access$7700(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->updateHdmi3D()V

    return-void
.end method

.method static synthetic access$7800(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/Hdmi3D;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    return-object v0
.end method

.method static synthetic access$7900(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToPrevImage()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/GestureRecognizer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGestureRecognizer:Lcom/sec/android/gallery3d/ui/GestureRecognizer;

    return-object v0
.end method

.method static synthetic access$8000(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToNextImage()V

    return-void
.end method

.method static synthetic access$8100()F
    .locals 1

    .prologue
    .line 136
    sget v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusX:F

    return v0
.end method

.method static synthetic access$8102(F)F
    .locals 0
    .param p0, "x0"    # F

    .prologue
    .line 136
    sput p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusX:F

    return p0
.end method

.method static synthetic access$8200()F
    .locals 1

    .prologue
    .line 136
    sget v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusY:F

    return v0
.end method

.method static synthetic access$8202(F)F
    .locals 0
    .param p0, "x0"    # F

    .prologue
    .line 136
    sput p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusY:F

    return p0
.end method

.method static synthetic access$8302(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOnScale:Z

    return p1
.end method

.method static synthetic access$8402(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreUpEvent:Z

    return p1
.end method

.method static synthetic access$8500(Lcom/sec/android/gallery3d/ui/PhotoView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCenterX:F

    return v0
.end method

.method static synthetic access$8600(Lcom/sec/android/gallery3d/ui/PhotoView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCenterY:F

    return v0
.end method

.method static synthetic access$8702(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionScaleBegin:Z

    return p1
.end method

.method static synthetic access$8800()I
    .locals 1

    .prologue
    .line 136
    sget v0, Lcom/sec/android/gallery3d/ui/PhotoView;->DOUBLE_TAP_TIMEOUT:I

    return v0
.end method

.method static synthetic access$8900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/GalleryMotion;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    return-object v0
.end method

.method static synthetic access$9000(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSideMirrorViewOnShowListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;

    return-object v0
.end method

.method static synthetic access$9102(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOperatePeek:Z

    return p1
.end method

.method static synthetic access$9200(Lcom/sec/android/gallery3d/ui/PhotoView;)Landroid/view/Display;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplay:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic access$9300(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningXSum:I

    return v0
.end method

.method static synthetic access$9302(Lcom/sec/android/gallery3d/ui/PhotoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningXSum:I

    return p1
.end method

.method static synthetic access$9312(Lcom/sec/android/gallery3d/ui/PhotoView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningXSum:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningXSum:I

    return v0
.end method

.method static synthetic access$9400(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningYSum:I

    return v0
.end method

.method static synthetic access$9402(Lcom/sec/android/gallery3d/ui/PhotoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningYSum:I

    return p1
.end method

.method static synthetic access$9412(Lcom/sec/android/gallery3d/ui/PhotoView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningYSum:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningYSum:I

    return v0
.end method

.method static synthetic access$9502(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOperatePanning:Z

    return p1
.end method

.method static synthetic access$9600(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomIn:I

    return v0
.end method

.method static synthetic access$9608(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomIn:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomIn:I

    return v0
.end method

.method static synthetic access$9700(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomOut:I

    return v0
.end method

.method static synthetic access$9708(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomOut:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomOut:I

    return v0
.end method

.method static synthetic access$9800(Lcom/sec/android/gallery3d/ui/PhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionTiltSum:I

    return v0
.end method

.method static synthetic access$9802(Lcom/sec/android/gallery3d/ui/PhotoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionTiltSum:I

    return p1
.end method

.method static synthetic access$9812(Lcom/sec/android/gallery3d/ui/PhotoView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionTiltSum:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionTiltSum:I

    return v0
.end method

.method static synthetic access$9900(Lcom/sec/android/gallery3d/ui/PhotoView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFinishHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private static calculateMoveOutProgress(III)F
    .locals 4
    .param p0, "left"    # I
    .param p1, "right"    # I
    .param p2, "viewWidth"    # I

    .prologue
    .line 3109
    sub-int v0, p1, p0

    .line 3116
    .local v0, "w":I
    if-ge v0, p2, :cond_1

    .line 3117
    div-int/lit8 v2, p2, 0x2

    div-int/lit8 v3, v0, 0x2

    sub-int v1, v2, v3

    .line 3118
    .local v1, "zx":I
    if-le p0, v1, :cond_0

    .line 3119
    sub-int v2, p0, v1

    neg-int v2, v2

    int-to-float v2, v2

    sub-int v3, p2, v1

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 3140
    .end local v1    # "zx":I
    :goto_0
    return v2

    .line 3122
    .restart local v1    # "zx":I
    :cond_0
    sub-int v2, p0, v1

    int-to-float v2, v2

    neg-int v3, v0

    sub-int/2addr v3, v1

    int-to-float v3, v3

    div-float/2addr v2, v3

    goto :goto_0

    .line 3132
    .end local v1    # "zx":I
    :cond_1
    if-lez p0, :cond_2

    .line 3133
    neg-int v2, p0

    int-to-float v2, v2

    int-to-float v3, p2

    div-float/2addr v2, v3

    goto :goto_0

    .line 3136
    :cond_2
    if-ge p1, p2, :cond_3

    .line 3137
    sub-int v2, p2, p1

    int-to-float v2, v2

    int-to-float v3, p2

    div-float/2addr v2, v3

    goto :goto_0

    .line 3140
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private captureAnimationDone(I)V
    .locals 2
    .param p1, "offset"    # I

    .prologue
    const/4 v1, 0x1

    .line 3084
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    .line 3085
    if-ne p1, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    if-nez v0, :cond_0

    .line 3087
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onActionBarAllowed(Z)V

    .line 3088
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onActionBarWanted()V

    .line 3090
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->snapback()V

    .line 3091
    return-void
.end method

.method private checkFlingCondition(FF)Z
    .locals 8
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1718
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1734
    :cond_0
    :goto_0
    return v5

    .line 1719
    :cond_1
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 1720
    .local v1, "deltaX":F
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1721
    .local v2, "deltaY":F
    const/4 v7, 0x0

    cmpl-float v7, v1, v7

    if-nez v7, :cond_2

    move v5, v6

    goto :goto_0

    .line 1723
    :cond_2
    const v0, 0x3fddb3d7

    .line 1725
    .local v0, "TANMAX":F
    div-float v4, v2, v1

    .line 1726
    .local v4, "tangent":F
    cmpl-float v7, v4, v0

    if-lez v7, :cond_0

    move v5, v6

    .line 1727
    goto :goto_0

    .line 1732
    .end local v0    # "TANMAX":F
    .end local v1    # "deltaX":F
    .end local v2    # "deltaY":F
    .end local v4    # "tangent":F
    :catch_0
    move-exception v3

    .line 1733
    .local v3, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkFocusSwitching()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 2793
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    if-nez v0, :cond_1

    .line 2800
    :cond_0
    :goto_0
    return-void

    .line 2795
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2797
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchPosition()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2798
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private checkGuideDialogState()Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;
    .locals 2

    .prologue
    .line 3297
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->NONE_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    .line 3298
    .local v0, "guideDialogState":Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdaptDisplayDialog:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAdaptDisplayDialog:Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAdaptDisplayDialog:Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->isAdaptDisplayDialogOff()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3299
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->ADAPT_DISPLAY_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    .line 3307
    :goto_0
    return-object v0

    .line 3300
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionDialogOff()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3301
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->MOTION_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    goto :goto_0

    .line 3302
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->isContextualTagHelpOff()Z

    move-result v1

    if-nez v1, :cond_2

    .line 3303
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->CONTEXTUAL_TAG_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    goto :goto_0

    .line 3305
    :cond_2
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->NONE_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    goto :goto_0
.end method

.method private checkHideUndoBar(I)V
    .locals 7
    .param p1, "addition"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2597
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBarState:I

    or-int/2addr v6, p1

    iput v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBarState:I

    .line 2598
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBarState:I

    and-int/lit8 v6, v6, 0x1

    if-nez v6, :cond_1

    .line 2607
    :cond_0
    :goto_0
    return-void

    .line 2600
    :cond_1
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBarState:I

    and-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_4

    move v2, v4

    .line 2601
    .local v2, "timeout":Z
    :goto_1
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBarState:I

    and-int/lit8 v6, v6, 0x4

    if-eqz v6, :cond_5

    move v3, v4

    .line 2602
    .local v3, "touched":Z
    :goto_2
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBarState:I

    and-int/lit8 v6, v6, 0x8

    if-eqz v6, :cond_6

    move v1, v4

    .line 2603
    .local v1, "fullCamera":Z
    :goto_3
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBarState:I

    and-int/lit8 v6, v6, 0x10

    if-eqz v6, :cond_7

    move v0, v4

    .line 2604
    .local v0, "deleteLast":Z
    :goto_4
    if-eqz v2, :cond_2

    if-nez v0, :cond_3

    :cond_2
    if-nez v1, :cond_3

    if-eqz v3, :cond_0

    .line 2605
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideUndoBar()V

    goto :goto_0

    .end local v0    # "deleteLast":Z
    .end local v1    # "fullCamera":Z
    .end local v2    # "timeout":Z
    .end local v3    # "touched":Z
    :cond_4
    move v2, v5

    .line 2600
    goto :goto_1

    .restart local v2    # "timeout":Z
    :cond_5
    move v3, v5

    .line 2601
    goto :goto_2

    .restart local v3    # "touched":Z
    :cond_6
    move v1, v5

    .line 2602
    goto :goto_3

    .restart local v1    # "fullCamera":Z
    :cond_7
    move v0, v5

    .line 2603
    goto :goto_4
.end method

.method public static combineBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "background"    # Landroid/graphics/Bitmap;
    .param p1, "foreground"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 4675
    if-nez p0, :cond_0

    move-object v5, v6

    .line 4688
    :goto_0
    return-object v5

    .line 4678
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 4679
    .local v1, "bgWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 4680
    .local v0, "bgHeight":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 4681
    .local v4, "fgWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 4682
    .local v3, "fgHeight":I
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 4683
    .local v5, "newmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 4684
    .local v2, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v2, p0, v8, v8, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 4685
    sub-int v7, v1, v4

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    sub-int v8, v0, v3

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    invoke-virtual {v2, p1, v7, v8, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 4686
    const/16 v6, 0x1f

    invoke-virtual {v2, v6}, Landroid/graphics/Canvas;->save(I)I

    .line 4687
    invoke-virtual {v2}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method private cpuBoost()V
    .locals 3

    .prologue
    .line 4913
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;->BOOST_CPU:Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    aput-object v2, v0, v1

    .line 4916
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "CPU_BOOST"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4917
    return-void
.end method

.method private cpuBoost200MS()V
    .locals 3

    .prologue
    .line 4920
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;->BOOST_CPU_200MS:Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    aput-object v2, v0, v1

    .line 4923
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "CPU_BOOST"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4924
    return-void
.end method

.method private cpuBoost600MS()V
    .locals 3

    .prologue
    .line 4927
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;->BOOST_CPU_600MS:Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    aput-object v2, v0, v1

    .line 4930
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "CPU_BOOST"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4931
    return-void
.end method

.method private cpuBoostCancel()V
    .locals 3

    .prologue
    .line 4934
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;->BOOST_CPU_CANCEL:Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    aput-object v2, v0, v1

    .line 4937
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "CPU_BOOST"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4938
    return-void
.end method

.method private cpuBoostInit()V
    .locals 3

    .prologue
    .line 4906
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;->INITIALIZE:Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    aput-object v2, v0, v1

    .line 4909
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "CPU_BOOST"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4910
    return-void
.end method

.method private cpuBoostRelease()V
    .locals 3

    .prologue
    .line 4941
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;->BOOST_RELEASE:Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    aput-object v2, v0, v1

    .line 4944
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "CPU_BOOST"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4945
    return-void
.end method

.method private createPointAnimationListener(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 4991
    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$10;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView$10;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 5037
    return-void
.end method

.method private drawPlaceHolder(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 1493
    iget v0, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget v0, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v4, v0

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPlaceholderColor:I

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 1494
    return-void
.end method

.method private static gapToSide(II)I
    .locals 2
    .param p0, "imageWidth"    # I
    .param p1, "viewWidth"    # I

    .prologue
    .line 2968
    const/4 v0, 0x0

    sub-int v1, p1, p0

    div-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private getCameraRotation()I
    .locals 2

    .prologue
    .line 1049
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCompensation:I

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayRotation:I

    sub-int/2addr v0, v1

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    return v0
.end method

.method private getFixedFaceRectOnImage(II)Landroid/graphics/RectF;
    .locals 25
    .param p1, "centerX"    # I
    .param p2, "centerY"    # I

    .prologue
    .line 4521
    const/4 v7, 0x0

    .line 4523
    .local v7, "faceRTonImageF":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v9

    .line 4524
    .local v9, "imageBounds":Landroid/graphics/Rect;
    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 4525
    new-instance v7, Landroid/graphics/RectF;

    .end local v7    # "faceRTonImageF":Landroid/graphics/RectF;
    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    .line 4527
    .restart local v7    # "faceRTonImageF":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getImageWidth()I

    move-result v19

    .line 4528
    .local v19, "w":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getImageHeight()I

    move-result v8

    .line 4529
    .local v8, "h":I
    if-lez v19, :cond_0

    if-gtz v8, :cond_1

    .line 4530
    :cond_0
    const/16 v20, 0x0

    .line 4567
    .end local v8    # "h":I
    .end local v19    # "w":I
    :goto_0
    return-object v20

    .line 4533
    .restart local v8    # "h":I
    .restart local v19    # "w":I
    :cond_1
    const/16 v16, 0x0

    .line 4534
    .local v16, "rotation":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v20, v0

    if-eqz v20, :cond_2

    .line 4535
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v16

    .line 4537
    :cond_2
    const/16 v20, 0x5a

    move/from16 v0, v16

    move/from16 v1, v20

    if-eq v0, v1, :cond_3

    const/16 v20, 0x10e

    move/from16 v0, v16

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 4538
    :cond_3
    move/from16 v16, v19

    .line 4539
    move/from16 v19, v8

    .line 4540
    move/from16 v8, v16

    .line 4542
    :cond_4
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v15, v20, v21

    .line 4544
    .local v15, "ratio":F
    iget v0, v9, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    sub-int v20, p1, v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v20, v20, v15

    move/from16 v0, v20

    float-to-int v11, v0

    .line 4545
    .local v11, "imgCenterX":I
    iget v0, v9, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    sub-int v20, p2, v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v20, v20, v15

    move/from16 v0, v20

    float-to-int v12, v0

    .line 4547
    .local v12, "imgCenterY":I
    const/high16 v20, 0x3f800000    # 1.0f

    cmpl-float v20, v15, v20

    if-lez v20, :cond_a

    const/16 v5, 0x96

    .line 4548
    .local v5, "dimension":I
    :goto_1
    new-instance v6, Landroid/graphics/Rect;

    sub-int v20, v11, v5

    sub-int v21, v12, v5

    add-int v22, v11, v5

    add-int v23, v12, v5

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v6, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4549
    .local v6, "faceRT":Landroid/graphics/Rect;
    new-instance v10, Landroid/graphics/Rect;

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v19

    invoke-direct {v10, v0, v1, v2, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4550
    .local v10, "imageRT":Landroid/graphics/Rect;
    invoke-virtual {v10, v6}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    .line 4552
    iget v13, v10, Landroid/graphics/Rect;->left:I

    .line 4553
    .local v13, "l":I
    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    .line 4554
    .local v18, "t":I
    iget v14, v10, Landroid/graphics/Rect;->right:I

    .line 4555
    .local v14, "r":I
    iget v4, v10, Landroid/graphics/Rect;->bottom:I

    .line 4558
    .local v4, "b":I
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v20

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v21

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 4559
    .local v17, "side":I
    if-nez v13, :cond_5

    add-int v14, v13, v17

    .line 4560
    :cond_5
    if-nez v18, :cond_6

    add-int v4, v18, v17

    .line 4561
    :cond_6
    move/from16 v0, v19

    if-ne v0, v14, :cond_7

    sub-int v13, v19, v17

    .line 4562
    :cond_7
    if-ne v8, v4, :cond_8

    sub-int v18, v4, v17

    .line 4564
    :cond_8
    int-to-float v0, v13

    move/from16 v20, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v8

    move/from16 v22, v0

    div-float v21, v21, v22

    int-to-float v0, v14

    move/from16 v22, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    int-to-float v0, v4

    move/from16 v23, v0

    int-to-float v0, v8

    move/from16 v24, v0

    div-float v23, v23, v24

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .end local v4    # "b":I
    .end local v5    # "dimension":I
    .end local v6    # "faceRT":Landroid/graphics/Rect;
    .end local v8    # "h":I
    .end local v10    # "imageRT":Landroid/graphics/Rect;
    .end local v11    # "imgCenterX":I
    .end local v12    # "imgCenterY":I
    .end local v13    # "l":I
    .end local v14    # "r":I
    .end local v15    # "ratio":F
    .end local v16    # "rotation":I
    .end local v17    # "side":I
    .end local v18    # "t":I
    .end local v19    # "w":I
    :cond_9
    move-object/from16 v20, v7

    .line 4567
    goto/16 :goto_0

    .line 4547
    .restart local v8    # "h":I
    .restart local v11    # "imgCenterX":I
    .restart local v12    # "imgCenterY":I
    .restart local v15    # "ratio":F
    .restart local v16    # "rotation":I
    .restart local v19    # "w":I
    :cond_a
    const/16 v5, 0x4b

    goto/16 :goto_1
.end method

.method private getImageView()Landroid/widget/ImageView;
    .locals 10

    .prologue
    const v9, 0x7f0f00df

    .line 4574
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 4575
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 4577
    .local v4, "imageView":Landroid/widget/ImageView;
    if-nez v4, :cond_0

    .line 4578
    const v6, 0x7f0f0175

    invoke-virtual {v0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 4579
    .local v5, "mainView":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030052

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 4581
    .local v2, "dragView":Landroid/view/View;
    new-instance v1, Lcom/sec/android/gallery3d/ui/PhotoView$9;

    invoke-direct {v1, p0, v5, v2}, Lcom/sec/android/gallery3d/ui/PhotoView$9;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 4591
    .local v1, "addViewOnUiThread":Ljava/lang/Runnable;
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 4593
    invoke-virtual {v0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .end local v4    # "imageView":Landroid/widget/ImageView;
    check-cast v4, Landroid/widget/ImageView;

    .line 4594
    .restart local v4    # "imageView":Landroid/widget/ImageView;
    if-nez v4, :cond_0

    .line 4596
    :try_start_0
    sget-object v6, Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;

    const-string v7, "addViewOnUiThread wait"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4597
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 4598
    sget-object v6, Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;

    const-string v7, "addViewOnUiThread resume"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4603
    :goto_0
    invoke-virtual {v0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .end local v4    # "imageView":Landroid/widget/ImageView;
    check-cast v4, Landroid/widget/ImageView;

    .line 4606
    .end local v1    # "addViewOnUiThread":Ljava/lang/Runnable;
    .end local v2    # "dragView":Landroid/view/View;
    .end local v5    # "mainView":Landroid/view/ViewGroup;
    .restart local v4    # "imageView":Landroid/widget/ImageView;
    :cond_0
    return-object v4

    .line 4599
    .restart local v1    # "addViewOnUiThread":Ljava/lang/Runnable;
    .restart local v2    # "dragView":Landroid/view/View;
    .restart local v5    # "mainView":Landroid/view/ViewGroup;
    :catch_0
    move-exception v3

    .line 4600
    .local v3, "e":Ljava/lang/InterruptedException;
    sget-object v6, Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;

    const-string v7, "ImageView was not added to main layout"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4601
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private getLoadingSpinner()Lcom/sec/android/gallery3d/ui/ProgressSpinner;
    .locals 2

    .prologue
    .line 5226
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingSpinner:Lcom/sec/android/gallery3d/ui/ProgressSpinner;

    if-nez v0, :cond_0

    .line 5227
    new-instance v0, Lcom/sec/android/gallery3d/ui/ProgressSpinner;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/ProgressSpinner;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingSpinner:Lcom/sec/android/gallery3d/ui/ProgressSpinner;

    .line 5229
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingSpinner:Lcom/sec/android/gallery3d/ui/ProgressSpinner;

    return-object v0
.end method

.method private getOffsetAlpha(F)F
    .locals 3
    .param p1, "offset"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 3186
    const/high16 v1, 0x3f000000    # 0.5f

    div-float/2addr p1, v1

    .line 3187
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-lez v1, :cond_0

    sub-float v0, v2, p1

    .line 3188
    .local v0, "alpha":F
    :goto_0
    const v1, 0x3cf5c28f    # 0.03f

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v1

    return v1

    .line 3187
    .end local v0    # "alpha":F
    :cond_0
    add-float v0, v2, p1

    goto :goto_0
.end method

.method private getPanoramaRotation()I
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1060
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v2, v5, Landroid/content/res/Configuration;->orientation:I

    .line 1061
    .local v2, "orientation":I
    if-ne v2, v3, :cond_1

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayRotation:I

    const/16 v6, 0x5a

    if-eq v5, v6, :cond_0

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayRotation:I

    const/16 v6, 0x10e

    if-ne v5, v6, :cond_1

    :cond_0
    move v1, v3

    .line 1063
    .local v1, "invertPortrait":Z
    :goto_0
    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayRotation:I

    const/16 v6, 0xb4

    if-lt v5, v6, :cond_2

    move v0, v3

    .line 1064
    .local v0, "invert":Z
    :goto_1
    if-eq v0, v1, :cond_3

    .line 1065
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCompensation:I

    add-int/lit16 v3, v3, 0xb4

    rem-int/lit16 v3, v3, 0x168

    .line 1067
    :goto_2
    return v3

    .end local v0    # "invert":Z
    .end local v1    # "invertPortrait":Z
    :cond_1
    move v1, v4

    .line 1061
    goto :goto_0

    .restart local v1    # "invertPortrait":Z
    :cond_2
    move v0, v4

    .line 1063
    goto :goto_1

    .line 1067
    .restart local v0    # "invert":Z
    :cond_3
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCompensation:I

    goto :goto_2
.end method

.method private static getRotated(III)I
    .locals 1
    .param p0, "degree"    # I
    .param p1, "original"    # I
    .param p2, "theother"    # I

    .prologue
    .line 1497
    rem-int/lit16 v0, p0, 0xb4

    if-nez v0, :cond_0

    .end local p1    # "original":I
    :goto_0
    return p1

    .restart local p1    # "original":I
    :cond_0
    move p1, p2

    goto :goto_0
.end method

.method private getScrollAlpha(F)F
    .locals 3
    .param p1, "scrollProgress"    # F

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 3146
    const/4 v1, 0x0

    cmpg-float v1, p1, v1

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAlphaInterpolator:Landroid/view/animation/AccelerateInterpolator;

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sub-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/animation/AccelerateInterpolator;->getInterpolation(F)F

    move-result v0

    :cond_0
    return v0
.end method

.method private getScrollScale(F)F
    .locals 4
    .param p1, "scrollProgress"    # F

    .prologue
    .line 3153
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mScaleInterpolator:Lcom/sec/android/gallery3d/ui/PhotoView$ZInterpolator;

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView$ZInterpolator;->getInterpolation(F)F

    move-result v0

    .line 3154
    .local v0, "interpolatedProgress":F
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, v0

    sget v3, Lcom/sec/android/gallery3d/ui/PhotoView;->TRANSITION_SCALE_FACTOR:F

    mul-float/2addr v3, v0

    add-float v1, v2, v3

    .line 3155
    .local v1, "scale":F
    return v1
.end method

.method private helpPopupAnimationInit()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x384

    const v5, 0x7f040003

    const-wide/16 v6, 0x2bc

    const v4, 0x7f040002

    .line 3258
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpTilt(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3259
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f01ce

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 3261
    .local v1, "text":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 3262
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 3263
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 3264
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 3293
    .end local v1    # "text":Landroid/widget/TextView;
    :cond_0
    :goto_0
    return-void

    .line 3267
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPanning(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3268
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f01c4

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 3269
    .restart local v1    # "text":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f01c5

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 3271
    .local v0, "head":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 3272
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 3273
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-static {v2, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleHeadAnimation:Landroid/view/animation/Animation;

    .line 3274
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 3275
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleHeadAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v8, v9}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 3276
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 3277
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleHeadAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 3279
    .end local v0    # "head":Landroid/widget/ImageView;
    .end local v1    # "text":Landroid/widget/TextView;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPeek(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3280
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f01ca

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 3281
    .restart local v1    # "text":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f01cb

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 3283
    .restart local v0    # "head":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 3284
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 3285
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-static {v2, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleHeadAnimation:Landroid/view/animation/Animation;

    .line 3286
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 3287
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleHeadAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v8, v9}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 3289
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 3290
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleHeadAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method

.method private helpTapAnimationInit(Landroid/content/Context;)V
    .locals 3
    .param p1, "mActivity"    # Landroid/content/Context;

    .prologue
    .line 4949
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->createPointAnimationListener(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 4952
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    .line 4954
    const v1, 0x7f040015

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 4955
    .local v0, "animation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4956
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4958
    const v1, 0x7f040016

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 4959
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4960
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4962
    const v1, 0x7f040017

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 4963
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4964
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4966
    const v1, 0x7f040018

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 4967
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4968
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4970
    const v1, 0x7f040019

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 4971
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4972
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4973
    return-void
.end method

.method private hideUndoBar()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2580
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->removeMessages(I)V

    .line 2581
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onCommitDeleteImage()V

    .line 2582
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBar:Lcom/sec/android/gallery3d/ui/UndoBarView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/UndoBarView;->animateVisibility(I)V

    .line 2583
    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBarState:I

    .line 2584
    const v0, 0x7fffffff

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoIndexHint:I

    .line 2585
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    invoke-interface {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onUndoBarVisibilityChanged(Z)V

    .line 2586
    return-void
.end method

.method private initDisplaySize(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 3311
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayWidth:I

    .line 3312
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayHeight:I

    .line 3313
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayWidth:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCenterX:F

    .line 3314
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayHeight:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCenterY:F

    .line 3315
    return-void
.end method

.method private static interpolate(FFF)F
    .locals 1
    .param p0, "ratio"    # F
    .param p1, "from"    # F
    .param p2, "to"    # F

    .prologue
    .line 3180
    sub-float v0, p2, p1

    mul-float/2addr v0, p0

    mul-float/2addr v0, p0

    add-float/2addr v0, p1

    return v0
.end method

.method private isHelpViewingScrollMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4983
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpMode:I

    if-ne v1, v0, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpViewingMode:I

    if-nez v1, :cond_0

    .line 4986
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isScaned()Z
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 4738
    const/4 v9, 0x0

    .line 4739
    .local v9, "isScaned":Z
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_1

    .line 4740
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v12

    .line 4741
    .local v12, "itemUri":Landroid/net/Uri;
    if-eqz v12, :cond_1

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "content"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4744
    :try_start_0
    invoke-static {v12}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v10

    .line 4750
    .local v10, "id":J
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4752
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceData;->RAW_SQL_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "main"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 4753
    .local v6, "RAW_SQL_MAIN":Landroid/net/Uri;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "select face_count from files where _id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 4754
    .local v13, "sqlString":Ljava/lang/String;
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v13}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 4755
    .local v1, "sqlUri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 4757
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_1
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 4758
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4759
    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    const/4 v9, 0x1

    .line 4762
    :cond_0
    :goto_0
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "sqlUri":Landroid/net/Uri;
    .end local v6    # "RAW_SQL_MAIN":Landroid/net/Uri;
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v10    # "id":J
    .end local v12    # "itemUri":Landroid/net/Uri;
    .end local v13    # "sqlString":Ljava/lang/String;
    :cond_1
    move v14, v9

    .line 4766
    :goto_1
    return v14

    .line 4745
    .restart local v12    # "itemUri":Landroid/net/Uri;
    :catch_0
    move-exception v8

    .line 4746
    .local v8, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v1    # "sqlUri":Landroid/net/Uri;
    .restart local v6    # "RAW_SQL_MAIN":Landroid/net/Uri;
    .restart local v7    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "id":J
    .restart local v13    # "sqlString":Ljava/lang/String;
    :cond_2
    move v9, v14

    .line 4759
    goto :goto_0

    .line 4762
    :catchall_0
    move-exception v2

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method private onMotionMoveEnd()V
    .locals 2

    .prologue
    .line 4878
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    if-eqz v0, :cond_0

    .line 4879
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    sget-object v1, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_NONE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/MotionDetector;->setState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)V

    .line 4881
    :cond_0
    return-void
.end method

.method private onTransitionComplete()V
    .locals 1

    .prologue
    .line 4180
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I

    .line 4181
    return-void
.end method

.method private scrollAfterLongPress(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 3664
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v2, :cond_0

    .line 3667
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMyGestureLongPressDetected:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOnScale:Z

    if-nez v2, :cond_3

    .line 3668
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevMoveX:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sub-float v0, v2, v3

    .line 3669
    .local v0, "dx":F
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevMoveY:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sub-float v1, v2, v3

    .line 3671
    .local v1, "dy":F
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I

    if-eqz v2, :cond_1

    .line 3685
    .end local v0    # "dx":F
    .end local v1    # "dy":F
    :cond_0
    :goto_0
    return-void

    .line 3674
    .restart local v0    # "dx":F
    .restart local v1    # "dy":F
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreScroll:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v2

    if-nez v2, :cond_0

    .line 3678
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    float-to-int v3, v0

    float-to-int v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->scrollPage(II)V

    .line 3679
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreUpEvent:Z

    .line 3682
    .end local v0    # "dx":F
    .end local v1    # "dy":F
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevMoveX:F

    .line 3683
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevMoveY:F

    goto :goto_0
.end method

.method private sendTitleInfo()V
    .locals 3

    .prologue
    .line 5080
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    if-eqz v1, :cond_0

    .line 5081
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    check-cast v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 5082
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;

    if-eqz v1, :cond_0

    .line 5083
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->SendTitleInformation(Landroid/content/Context;Ljava/lang/String;)V

    .line 5086
    .end local v0    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    return-void
.end method

.method private set3dDisplayEnable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 3429
    const/4 v0, 0x0

    .line 3430
    .local v0, "displayEnabled":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v1, :cond_0

    .line 3431
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->is3dDisplayEnabled()Z

    move-result v0

    .line 3432
    :cond_0
    if-ne p1, v0, :cond_1

    .line 3441
    :goto_0
    return-void

    .line 3435
    :cond_1
    move v0, p1

    .line 3436
    if-eqz v0, :cond_2

    .line 3437
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->updateHdmi3D()V

    goto :goto_0

    .line 3439
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHdmiPresentation(Z)V

    goto :goto_0
.end method

.method private setAnimationListenerForSlide()V
    .locals 2

    .prologue
    .line 2947
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->existAnimationListener()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2948
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->setAnimationListener(Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;)V

    .line 2949
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->completeSlideAnimation()V

    .line 2952
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideFaceIndicatorView()V

    .line 2953
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetAgifMode(ZLcom/sec/android/gallery3d/data/MediaItem;)V

    .line 2954
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v0, :cond_1

    .line 2955
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->hideIcon(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 2957
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    new-instance v1, Lcom/sec/android/gallery3d/ui/PhotoView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/ui/PhotoView$5;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->setAnimationListener(Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;)V

    .line 2965
    return-void
.end method

.method private setHelpTextAndState(IZ)V
    .locals 13
    .param p1, "action"    # I
    .param p2, "isMinScale"    # Z

    .prologue
    const v12, 0x7f0202f1

    const v11, 0x7f0202f0

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x4

    .line 5113
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpMode:I

    if-ne v6, v10, :cond_3

    .line 5114
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpViewingMode:I

    if-nez v6, :cond_5

    iget v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpViewingMode:I

    if-ne p1, v6, :cond_5

    .line 5115
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0f01c4

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 5116
    .local v4, "text_pan_text":Landroid/widget/TextView;
    if-eqz v4, :cond_0

    .line 5117
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 5119
    :cond_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    const v7, 0x7f0f01c5

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 5120
    .local v1, "head":Landroid/widget/ImageView;
    if-eqz v1, :cond_1

    .line 5121
    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5123
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0f01c6

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 5124
    .local v3, "text":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    const v7, 0x7f0f01c3

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 5125
    .local v0, "cue_img":Landroid/widget/ImageView;
    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    .line 5126
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 5127
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5128
    const v6, 0x7f0e02c4

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    .line 5129
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v6, :cond_4

    .line 5130
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v7, "gallery_help_zoominout_cue"

    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->getHelpDrawable(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 5135
    :cond_2
    :goto_0
    iput v10, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpViewingMode:I

    .line 5182
    .end local v0    # "cue_img":Landroid/widget/ImageView;
    .end local v1    # "head":Landroid/widget/ImageView;
    .end local v3    # "text":Landroid/widget/TextView;
    .end local v4    # "text_pan_text":Landroid/widget/TextView;
    :cond_3
    :goto_1
    return-void

    .line 5132
    .restart local v0    # "cue_img":Landroid/widget/ImageView;
    .restart local v1    # "head":Landroid/widget/ImageView;
    .restart local v3    # "text":Landroid/widget/TextView;
    .restart local v4    # "text_pan_text":Landroid/widget/TextView;
    :cond_4
    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 5136
    .end local v0    # "cue_img":Landroid/widget/ImageView;
    .end local v1    # "head":Landroid/widget/ImageView;
    .end local v3    # "text":Landroid/widget/TextView;
    .end local v4    # "text_pan_text":Landroid/widget/TextView;
    :cond_5
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpViewingMode:I

    if-ne v6, v10, :cond_3

    iget v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpViewingMode:I

    if-ne p1, v6, :cond_3

    .line 5137
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0f01c4

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 5138
    .restart local v4    # "text_pan_text":Landroid/widget/TextView;
    if-eqz v4, :cond_6

    .line 5139
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 5141
    :cond_6
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    const v7, 0x7f0f01c5

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 5142
    .restart local v1    # "head":Landroid/widget/ImageView;
    if-eqz v1, :cond_7

    .line 5143
    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5145
    :cond_7
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0f01c6

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 5146
    .restart local v3    # "text":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    const v7, 0x7f0f01c3

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 5147
    .restart local v0    # "cue_img":Landroid/widget/ImageView;
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 5148
    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    .line 5149
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 5150
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5151
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e02c5

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 5152
    .local v2, "str":Ljava/lang/String;
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v6, :cond_8

    .line 5153
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v7, "gallery_help_zoomoutin_cue"

    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->getHelpDrawable(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 5157
    :goto_2
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 5158
    .local v5, "text_str":Ljava/lang/String;
    if-eqz p2, :cond_9

    if-eqz v2, :cond_9

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 5159
    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5160
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 5161
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5162
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->showCompletedPopup()V

    .line 5163
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFinishHandler:Landroid/os/Handler;

    const/16 v7, 0x64

    const-wide/16 v8, 0x7d0

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 5155
    .end local v5    # "text_str":Ljava/lang/String;
    :cond_8
    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 5164
    .restart local v5    # "text_str":Ljava/lang/String;
    :cond_9
    if-eqz p2, :cond_b

    .line 5165
    const v6, 0x7f0e02c4

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    .line 5166
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v6, :cond_a

    .line 5167
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v7, "gallery_help_zoominout_cue"

    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->getHelpDrawable(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 5169
    :cond_a
    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 5172
    :cond_b
    const v6, 0x7f0e02c5

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    .line 5173
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v6, :cond_c

    .line 5174
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v7, "gallery_help_zoomoutin_cue"

    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->getHelpDrawable(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 5176
    :cond_c
    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1
.end method

.method private setPictureSize(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 958
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    .line 959
    .local v0, "p":Lcom/sec/android/gallery3d/ui/PhotoView$Picture;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->getSize()Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    move-result-object v3

    if-nez p1, :cond_0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->isCamera()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRect:Landroid/graphics/Rect;

    :goto_0
    invoke-virtual {v2, p1, v3, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->setImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;Landroid/graphics/Rect;)V

    .line 961
    return-void

    .line 959
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private showCompletedPopup()V
    .locals 3

    .prologue
    .line 5105
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    const v2, 0x7f040002

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 5106
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f01c6

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 5107
    .local v0, "completePopup":Landroid/widget/TextView;
    const v1, 0x7f0e02dd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 5108
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 5109
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 5110
    return-void
.end method

.method private showDragAndDropDialog(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 5194
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDragAndDropDialog:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

    new-instance v1, Lcom/sec/android/gallery3d/ui/PhotoView$11;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView$11;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;II)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->setOkButtonClickListener(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;)V

    .line 5200
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDragAndDropDialog:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->show(II)V

    .line 5201
    return-void
.end method

.method private slideToNextPicture()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2927
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mNextBound:I

    if-gtz v1, :cond_0

    .line 2928
    const/4 v0, 0x0

    .line 2933
    :goto_0
    return v0

    .line 2930
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->setAnimationListenerForSlide()V

    .line 2931
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToImageFastest(I)V

    .line 2932
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->startHorizontalSlide()V

    goto :goto_0
.end method

.method private slideToPrevPicture()Z
    .locals 1

    .prologue
    .line 2937
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevBound:I

    if-ltz v0, :cond_0

    .line 2938
    const/4 v0, 0x0

    .line 2943
    :goto_0
    return v0

    .line 2940
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->setAnimationListenerForSlide()V

    .line 2941
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToImageFastest(I)V

    .line 2942
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->startHorizontalSlide()V

    .line 2943
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private snapToNeighborImage()Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2910
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v5, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v1

    .line 2911
    .local v1, "r":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v3

    .line 2913
    .local v3, "viewW":I
    sget v0, Lcom/sec/android/gallery3d/ui/PhotoView;->SWIPE_MIN_THRESHOLD:I

    .line 2914
    .local v0, "moveThreshold":I
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-static {v5, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->gapToSide(II)I

    move-result v5

    add-int v2, v0, v5

    .line 2917
    .local v2, "threshold":I
    iget v5, v1, Landroid/graphics/Rect;->right:I

    sub-int v5, v3, v5

    if-le v5, v2, :cond_1

    .line 2918
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->slideToNextPicture()Z

    move-result v4

    .line 2923
    :cond_0
    :goto_0
    return v4

    .line 2919
    :cond_1
    iget v5, v1, Landroid/graphics/Rect;->left:I

    if-le v5, v2, :cond_0

    .line 2920
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->slideToPrevPicture()Z

    move-result v4

    goto :goto_0
.end method

.method private snapback()V
    .locals 1

    .prologue
    .line 2902
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    and-int/lit8 v0, v0, -0x5

    if-eqz v0, :cond_1

    .line 2907
    :cond_0
    :goto_0
    return-void

    .line 2904
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->snapToNeighborImage()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2905
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->snapback()V

    goto :goto_0
.end method

.method private startArrayAnimationHelp()V
    .locals 10

    .prologue
    .line 3531
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01bc

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 3532
    .local v6, "startNearLeft":Landroid/widget/ImageView;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/widget/ImageView;->getVisibility()I

    move-result v8

    if-nez v8, :cond_3

    .line 3533
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01bb

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 3534
    .local v4, "startFarLeft":Landroid/widget/ImageView;
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01be

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 3535
    .local v7, "startNearRight":Landroid/widget/ImageView;
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01bd

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 3537
    .local v5, "startFarRight":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    .line 3538
    .local v1, "leftNearAni":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 3539
    .local v0, "leftFarAni":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v7}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/AnimationDrawable;

    .line 3540
    .local v3, "rightNearAni":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/AnimationDrawable;

    .line 3542
    .local v2, "rightFarAni":Landroid/graphics/drawable/AnimationDrawable;
    if-eqz v1, :cond_0

    .line 3543
    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 3544
    :cond_0
    if-eqz v0, :cond_1

    .line 3545
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 3546
    :cond_1
    if-eqz v3, :cond_2

    .line 3547
    invoke-virtual {v3}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 3548
    :cond_2
    if-eqz v2, :cond_3

    .line 3549
    invoke-virtual {v2}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 3551
    .end local v0    # "leftFarAni":Landroid/graphics/drawable/AnimationDrawable;
    .end local v1    # "leftNearAni":Landroid/graphics/drawable/AnimationDrawable;
    .end local v2    # "rightFarAni":Landroid/graphics/drawable/AnimationDrawable;
    .end local v3    # "rightNearAni":Landroid/graphics/drawable/AnimationDrawable;
    .end local v4    # "startFarLeft":Landroid/widget/ImageView;
    .end local v5    # "startFarRight":Landroid/widget/ImageView;
    .end local v7    # "startNearRight":Landroid/widget/ImageView;
    :cond_3
    return-void
.end method

.method private startDrag()V
    .locals 6

    .prologue
    .line 4610
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v3, :cond_0

    .line 4621
    :goto_0
    return-void

    .line 4611
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    .line 4612
    .local v1, "imageView":Landroid/widget/ImageView;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "galleryUri"

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v0

    .line 4613
    .local v0, "dragData":Landroid/content/ClipData;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    sput-object v3, Lcom/sec/android/gallery3d/ui/PhotoView;->mDragUri:Landroid/net/Uri;

    .line 4614
    sget-object v3, Lcom/sec/android/gallery3d/ui/PhotoView;->mDragUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "content://mms/part/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4615
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0e02e4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 4618
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryApp;

    sput-object v3, Lcom/sec/android/gallery3d/ui/PhotoView;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 4619
    new-instance v2, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;-><init>(Landroid/view/View;Landroid/content/Context;)V

    .line 4620
    .local v2, "myShadow":Landroid/view/View$DragShadowBuilder;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/ImageView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    goto :goto_0
.end method

.method private startFaceTagDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4264
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirstFaceTagDialog:Z

    if-nez v1, :cond_1

    .line 4272
    :cond_0
    :goto_0
    return-void

    .line 4267
    :cond_1
    new-instance v0, Lcom/sec/android/gallery3d/ui/FacetagDialog;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/FacetagDialog;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 4268
    .local v0, "facetagDialog":Lcom/sec/android/gallery3d/ui/FacetagDialog;
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/FacetagDialog;->isFaceTagDialogOff(Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4270
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FacetagDialog;->faceTagDialogInitial()V

    .line 4271
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirstFaceTagDialog:Z

    goto :goto_0
.end method

.method private startManualFD(II)V
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v7, 0x1

    .line 4699
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v5, :cond_1

    .line 4723
    :cond_0
    :goto_0
    return-void

    .line 4700
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v5, v5, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v5, :cond_0

    .line 4702
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->getFixedFaceRectOnImage(II)Landroid/graphics/RectF;

    move-result-object v3

    .line 4703
    .local v3, "rtOnImageF":Landroid/graphics/RectF;
    if-eqz v3, :cond_0

    .line 4704
    const/4 v5, 0x4

    new-array v4, v5, [F

    const/4 v5, 0x0

    iget v6, v3, Landroid/graphics/RectF;->left:F

    aput v6, v4, v5

    iget v5, v3, Landroid/graphics/RectF;->top:F

    aput v5, v4, v7

    const/4 v5, 0x2

    iget v6, v3, Landroid/graphics/RectF;->right:F

    aput v6, v4, v5

    const/4 v5, 0x3

    iget v6, v3, Landroid/graphics/RectF;->bottom:F

    aput v6, v4, v5

    .line 4706
    .local v4, "values":[F
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    .line 4707
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Landroid/content/Intent;

    const-string v5, "com.android.camera.action.CROP"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4709
    .local v2, "intent":Landroid/content/Intent;
    const-class v5, Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 4710
    const-string v5, "is-manual-fd"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4711
    const-string v5, "aspectX"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4712
    const-string v5, "aspectY"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4713
    const-string v5, "face-position"

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[F)Landroid/content/Intent;

    .line 4714
    const-string v5, "return-data"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4715
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    .line 4716
    .local v1, "data":Landroid/net/Uri;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 4717
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsBackkeyPressed:Z

    if-nez v5, :cond_0

    .line 4718
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/16 v6, 0x306

    invoke-virtual {v5, v2, v6}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 4719
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    if-eqz v5, :cond_0

    .line 4720
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onLongPress()V

    goto :goto_0
.end method

.method private startTapAnimationHelp()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 3579
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPanning(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3580
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v5, 0x7f0f01c2

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 3581
    .local v1, "tap":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 3582
    iput v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    .line 3583
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    .line 3584
    .local v0, "animation":Landroid/view/animation/Animation;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3607
    .end local v0    # "animation":Landroid/view/animation/Animation;
    .end local v1    # "tap":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    return-void

    .line 3586
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpTilt(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3587
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v5, 0x7f0f01d1

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 3588
    .local v2, "tap_1":Landroid/widget/ImageView;
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v5, 0x7f0f01d2

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 3589
    .local v3, "tap_2":Landroid/widget/ImageView;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_2

    .line 3590
    iput v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    .line 3591
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    .line 3592
    .restart local v0    # "animation":Landroid/view/animation/Animation;
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3594
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_2
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 3595
    iput v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_2:I

    .line 3596
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_2:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    .line 3597
    .restart local v0    # "animation":Landroid/view/animation/Animation;
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 3599
    .end local v0    # "animation":Landroid/view/animation/Animation;
    .end local v2    # "tap_1":Landroid/widget/ImageView;
    .end local v3    # "tap_2":Landroid/widget/ImageView;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPeek(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3600
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v5, 0x7f0f01c9

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 3601
    .restart local v1    # "tap":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 3602
    iput v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    .line 3603
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    .line 3604
    .restart local v0    # "animation":Landroid/view/animation/Animation;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private stopAniamtionHelp()V
    .locals 1

    .prologue
    .line 3649
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPanning(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3650
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopBubbleAnimationHelp()V

    .line 3651
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopArrayAnimationHelp()V

    .line 3652
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopTapAnimationHelp()V

    .line 3661
    :cond_0
    :goto_0
    return-void

    .line 3653
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpTilt(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3654
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopBubbleAnimationHelp()V

    .line 3655
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopTapAnimationHelp()V

    goto :goto_0

    .line 3656
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPeek(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3657
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopBubbleAnimationHelp()V

    .line 3658
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopArrayAnimationHelp()V

    .line 3659
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopTapAnimationHelp()V

    goto :goto_0
.end method

.method private stopArrayAnimationHelp()V
    .locals 10

    .prologue
    .line 3555
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01bc

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 3556
    .local v6, "startNearLeft":Landroid/widget/ImageView;
    if-eqz v6, :cond_3

    .line 3557
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01bb

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 3558
    .local v4, "startFarLeft":Landroid/widget/ImageView;
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01be

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 3559
    .local v7, "startNearRight":Landroid/widget/ImageView;
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01bd

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 3561
    .local v5, "startFarRight":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    .line 3562
    .local v1, "leftNearAni":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 3563
    .local v0, "leftFarAni":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v7}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/AnimationDrawable;

    .line 3564
    .local v3, "rightNearAni":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/AnimationDrawable;

    .line 3566
    .local v2, "rightFarAni":Landroid/graphics/drawable/AnimationDrawable;
    if-eqz v1, :cond_0

    .line 3567
    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 3568
    :cond_0
    if-eqz v0, :cond_1

    .line 3569
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 3570
    :cond_1
    if-eqz v3, :cond_2

    .line 3571
    invoke-virtual {v3}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 3572
    :cond_2
    if-eqz v2, :cond_3

    .line 3573
    invoke-virtual {v2}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 3575
    .end local v0    # "leftFarAni":Landroid/graphics/drawable/AnimationDrawable;
    .end local v1    # "leftNearAni":Landroid/graphics/drawable/AnimationDrawable;
    .end local v2    # "rightFarAni":Landroid/graphics/drawable/AnimationDrawable;
    .end local v3    # "rightNearAni":Landroid/graphics/drawable/AnimationDrawable;
    .end local v4    # "startFarLeft":Landroid/widget/ImageView;
    .end local v5    # "startFarRight":Landroid/widget/ImageView;
    .end local v7    # "startNearRight":Landroid/widget/ImageView;
    :cond_3
    return-void
.end method

.method private stopBubbleAnimationHelp()V
    .locals 1

    .prologue
    .line 3644
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 3645
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 3646
    :cond_0
    return-void
.end method

.method private stopTapAnimationHelp()V
    .locals 6

    .prologue
    const/4 v5, 0x5

    .line 3610
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPanning(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3611
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0f01c2

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 3612
    .local v0, "tap":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 3613
    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 3614
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    if-ltz v3, :cond_0

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    if-ge v3, v5, :cond_0

    .line 3615
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/animation/Animation;

    invoke-virtual {v3}, Landroid/view/animation/Animation;->cancel()V

    .line 3641
    .end local v0    # "tap":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    return-void

    .line 3617
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpTilt(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 3618
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0f01d1

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 3619
    .local v1, "tap_1":Landroid/widget/ImageView;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0f01d2

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 3621
    .local v2, "tap_2":Landroid/widget/ImageView;
    if-eqz v1, :cond_2

    .line 3622
    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 3623
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    if-ltz v3, :cond_2

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    if-ge v3, v5, :cond_2

    .line 3624
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/animation/Animation;

    invoke-virtual {v3}, Landroid/view/animation/Animation;->cancel()V

    .line 3627
    :cond_2
    if-eqz v2, :cond_0

    .line 3628
    invoke-virtual {v2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 3629
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_2:I

    if-ltz v3, :cond_0

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_2:I

    if-ge v3, v5, :cond_0

    .line 3630
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_2:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/animation/Animation;

    invoke-virtual {v3}, Landroid/view/animation/Animation;->cancel()V

    goto :goto_0

    .line 3633
    .end local v1    # "tap_1":Landroid/widget/ImageView;
    .end local v2    # "tap_2":Landroid/widget/ImageView;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPeek(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3634
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0f01c9

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 3635
    .restart local v0    # "tap":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 3636
    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 3637
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    if-ltz v3, :cond_0

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    if-ge v3, v5, :cond_0

    .line 3638
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPointAnimations:Ljava/util/List;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurrentPointAnimation_1:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/animation/Animation;

    invoke-virtual {v3}, Landroid/view/animation/Animation;->cancel()V

    goto/16 :goto_0
.end method

.method private swipeImages(FF)Z
    .locals 7
    .param p1, "velocityX"    # F
    .param p2, "velocityY"    # F

    .prologue
    const/4 v4, 0x0

    .line 2870
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    if-eqz v5, :cond_1

    .line 2898
    :cond_0
    :goto_0
    return v4

    .line 2875
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    .line 2876
    .local v0, "controller":Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v2

    .line 2877
    .local v2, "isMinimal":Z
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageAtEdges()I

    move-result v1

    .line 2878
    .local v1, "edges":I
    if-nez v2, :cond_2

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_2

    .line 2879
    and-int/lit8 v5, v1, 0x4

    if-eqz v5, :cond_0

    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_0

    .line 2885
    :cond_2
    const/4 v3, 0x0

    .line 2886
    .local v3, "threshold":F
    if-eqz v2, :cond_4

    .line 2887
    const v3, 0x44bb8000    # 1500.0f

    .line 2890
    :goto_1
    neg-float v5, v3

    cmpg-float v5, p1, v5

    if-gez v5, :cond_5

    if-nez v2, :cond_3

    and-int/lit8 v5, v1, 0x2

    if-eqz v5, :cond_5

    .line 2892
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->slideToNextPicture()Z

    move-result v4

    goto :goto_0

    .line 2889
    :cond_4
    const v3, 0x453b8000    # 3000.0f

    goto :goto_1

    .line 2893
    :cond_5
    cmpl-float v5, p1, v3

    if-lez v5, :cond_0

    if-nez v2, :cond_6

    and-int/lit8 v5, v1, 0x1

    if-eqz v5, :cond_0

    .line 2895
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->slideToPrevPicture()Z

    move-result v4

    goto :goto_0
.end method

.method private switchFocus()V
    .locals 1

    .prologue
    .line 2804
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    if-eqz v0, :cond_0

    .line 2814
    :goto_0
    return-void

    .line 2806
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchPosition()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 2808
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToPrevImage()V

    goto :goto_0

    .line 2811
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToNextImage()V

    goto :goto_0

    .line 2806
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private switchPosition()I
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v7, -0x1

    .line 2819
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v10, v9}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v1

    .line 2820
    .local v1, "curr":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v10

    div-int/lit8 v0, v10, 0x2

    .line 2822
    .local v0, "center":I
    iget v10, v1, Landroid/graphics/Rect;->left:I

    if-le v10, v0, :cond_0

    iget v10, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevBound:I

    if-gez v10, :cond_0

    .line 2823
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v8, v7}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v5

    .line 2824
    .local v5, "prev":Landroid/graphics/Rect;
    iget v8, v1, Landroid/graphics/Rect;->left:I

    sub-int v2, v8, v0

    .line 2825
    .local v2, "currDist":I
    iget v8, v5, Landroid/graphics/Rect;->right:I

    sub-int v6, v0, v8

    .line 2826
    .local v6, "prevDist":I
    if-ge v6, v2, :cond_1

    .line 2838
    .end local v2    # "currDist":I
    .end local v5    # "prev":Landroid/graphics/Rect;
    .end local v6    # "prevDist":I
    :goto_0
    return v7

    .line 2829
    :cond_0
    iget v7, v1, Landroid/graphics/Rect;->right:I

    if-ge v7, v0, :cond_1

    iget v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mNextBound:I

    if-lez v7, :cond_1

    .line 2830
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v3

    .line 2831
    .local v3, "next":Landroid/graphics/Rect;
    iget v7, v1, Landroid/graphics/Rect;->right:I

    sub-int v2, v0, v7

    .line 2832
    .restart local v2    # "currDist":I
    iget v7, v3, Landroid/graphics/Rect;->left:I

    sub-int v4, v7, v0

    .line 2833
    .local v4, "nextDist":I
    if-ge v4, v2, :cond_1

    move v7, v8

    .line 2834
    goto :goto_0

    .end local v2    # "currDist":I
    .end local v3    # "next":Landroid/graphics/Rect;
    .end local v4    # "nextDist":I
    :cond_1
    move v7, v9

    .line 2838
    goto :goto_0
.end method

.method private switchToFirstImage()V
    .locals 2

    .prologue
    .line 3020
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->moveTo(I)V

    .line 3021
    return-void
.end method

.method private switchToHitPicture(II)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2844
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevBound:I

    if-gez v1, :cond_1

    .line 2845
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 2846
    .local v0, "r":Landroid/graphics/Rect;
    iget v1, v0, Landroid/graphics/Rect;->right:I

    if-lt v1, p1, :cond_1

    .line 2847
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->slideToPrevPicture()Z

    .line 2859
    .end local v0    # "r":Landroid/graphics/Rect;
    :cond_0
    :goto_0
    return-void

    .line 2852
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mNextBound:I

    if-lez v1, :cond_0

    .line 2853
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 2854
    .restart local v0    # "r":Landroid/graphics/Rect;
    iget v1, v0, Landroid/graphics/Rect;->left:I

    if-gt v1, p1, :cond_0

    .line 2855
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->slideToNextPicture()Z

    goto :goto_0
.end method

.method private switchToImageFastest(I)V
    .locals 3
    .param p1, "slideDirection"    # I

    .prologue
    const/4 v2, 0x1

    .line 3005
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setImageChanging(Z)V

    .line 3007
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->checkSliding()V

    .line 3008
    if-ne p1, v2, :cond_0

    .line 3009
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->switchToNext()V

    .line 3015
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getCurrentIndex()I

    move-result v1

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->moveToFastest(I)V

    .line 3016
    const/4 v0, 0x0

    invoke-direct {p0, v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHelpTextAndState(IZ)V

    .line 3017
    return-void

    .line 3011
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->switchToPrev()V

    goto :goto_0
.end method

.method private switchToNextImage()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2981
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setImageChanging(Z)V

    .line 2983
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->checkSliding()V

    .line 2984
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->switchToNext()V

    .line 2987
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getCurrentIndex()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->moveTo(I)V

    .line 2988
    const/4 v0, 0x0

    invoke-direct {p0, v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHelpTextAndState(IZ)V

    .line 2989
    return-void
.end method

.method private switchToPrevImage()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2993
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setImageChanging(Z)V

    .line 2995
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->checkSliding()V

    .line 2996
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->switchToPrev()V

    .line 2999
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getCurrentIndex()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->moveTo(I)V

    .line 3000
    const/4 v0, 0x0

    invoke-direct {p0, v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHelpTextAndState(IZ)V

    .line 3001
    return-void
.end method

.method private switchWithCaptureAnimationLocked(I)Z
    .locals 6
    .param p1, "offset"    # I

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3047
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    if-eqz v3, :cond_0

    .line 3080
    :goto_0
    return v1

    .line 3049
    :cond_0
    if-ne p1, v1, :cond_3

    .line 3050
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mNextBound:I

    if-gtz v3, :cond_1

    move v1, v2

    .line 3051
    goto :goto_0

    .line 3053
    :cond_1
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    if-nez v3, :cond_2

    .line 3054
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    invoke-interface {v3, v2}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onActionBarAllowed(Z)V

    .line 3055
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToNextImage()V

    .line 3056
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->startCaptureAnimationSlide(I)V

    .line 3077
    :goto_1
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    .line 3078
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const/4 v4, 0x4

    invoke-virtual {v3, v4, p1, v2}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 3079
    .local v0, "m":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const-wide/16 v4, 0x2bc

    invoke-virtual {v2, v0, v4, v5}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 3057
    .end local v0    # "m":Landroid/os/Message;
    :cond_3
    if-ne p1, v4, :cond_7

    .line 3058
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevBound:I

    if-ltz v3, :cond_4

    move v1, v2

    .line 3059
    goto :goto_0

    .line 3060
    :cond_4
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    if-eqz v3, :cond_5

    .line 3061
    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setFilmMode(Z)V

    .line 3066
    :cond_5
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getCurrentIndex()I

    move-result v3

    const/4 v4, 0x3

    if-le v3, v4, :cond_6

    .line 3067
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToFirstImage()V

    .line 3068
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->skipToFinalPosition()V

    goto :goto_0

    .line 3072
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToFirstImage()V

    .line 3073
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->startCaptureAnimationSlide(I)V

    goto :goto_1

    :cond_7
    move v1, v2

    .line 3075
    goto :goto_0
.end method

.method private updateActionBar()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2426
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->isCamera()Z

    move-result v0

    .line 2427
    .local v0, "isCamera":Z
    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    if-nez v1, :cond_1

    .line 2429
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onActionBarAllowed(Z)V

    .line 2434
    :cond_0
    :goto_0
    return-void

    .line 2431
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onActionBarAllowed(Z)V

    .line 2432
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onActionBarWanted()V

    goto :goto_0
.end method

.method private updateCameraRect()V
    .locals 12

    .prologue
    .line 1011
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v6

    .line 1012
    .local v6, "w":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v1

    .line 1013
    .local v1, "h":I
    iget v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCompensation:I

    rem-int/lit16 v7, v7, 0xb4

    if-eqz v7, :cond_0

    .line 1014
    move v5, v6

    .line 1015
    .local v5, "tmp":I
    move v6, v1

    .line 1016
    move v1, v5

    .line 1018
    .end local v5    # "tmp":I
    :cond_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRelativeFrame:Landroid/graphics/Rect;

    iget v2, v7, Landroid/graphics/Rect;->left:I

    .line 1019
    .local v2, "l":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRelativeFrame:Landroid/graphics/Rect;

    iget v4, v7, Landroid/graphics/Rect;->top:I

    .line 1020
    .local v4, "t":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRelativeFrame:Landroid/graphics/Rect;

    iget v3, v7, Landroid/graphics/Rect;->right:I

    .line 1021
    .local v3, "r":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRelativeFrame:Landroid/graphics/Rect;

    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    .line 1024
    .local v0, "b":I
    iget v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCompensation:I

    sparse-switch v7, :sswitch_data_0

    .line 1034
    :goto_0
    return-void

    .line 1025
    :sswitch_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v2, v4, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 1026
    :sswitch_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRect:Landroid/graphics/Rect;

    sub-int v8, v1, v0

    sub-int v9, v1, v4

    invoke-virtual {v7, v8, v2, v9, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 1027
    :sswitch_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRect:Landroid/graphics/Rect;

    sub-int v8, v6, v3

    sub-int v9, v1, v0

    sub-int v10, v6, v2

    sub-int v11, v1, v4

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 1028
    :sswitch_3
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRect:Landroid/graphics/Rect;

    sub-int v8, v6, v3

    sub-int v9, v6, v2

    invoke-virtual {v7, v4, v8, v0, v9}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 1024
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method private updateHdmi3D()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3398
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v1, :cond_0

    .line 3399
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->set3dDisplayAvailable(Z)V

    .line 3400
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/HdmiUtils;->is3DTvConnected()Z

    move-result v1

    if-nez v1, :cond_2

    .line 3426
    :cond_1
    :goto_0
    return-void

    .line 3402
    :cond_2
    const/4 v0, 0x0

    .line 3403
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    if-eqz v1, :cond_3

    .line 3404
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    check-cast v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 3405
    :cond_3
    if-eqz v0, :cond_4

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    if-nez v1, :cond_5

    .line 3406
    :cond_4
    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHdmiPresentation(Z)V

    goto :goto_0

    .line 3409
    :cond_5
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMpfExtract:Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;

    if-nez v1, :cond_6

    .line 3410
    new-instance v1, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMpfExtract:Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;

    .line 3412
    :cond_6
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMpfExtract:Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->setMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 3413
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMpfExtract:Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->isAvailable()Z

    move-result v1

    if-nez v1, :cond_7

    .line 3414
    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHdmiPresentation(Z)V

    goto :goto_0

    .line 3417
    :cond_7
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v1, :cond_8

    .line 3418
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->set3dDisplayAvailable(Z)V

    .line 3419
    :cond_8
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->is3dDisplayEnabled()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 3420
    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHdmiPresentation(Z)V

    .line 3421
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMpfExtract:Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->getBitmap(Z)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2, v5}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->setImage(Landroid/graphics/Bitmap;Z)V

    .line 3422
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMpfExtract:Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->getBitmap(Z)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->setImage(Landroid/graphics/Bitmap;Z)V

    goto :goto_0

    .line 3424
    :cond_9
    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHdmiPresentation(Z)V

    goto :goto_0
.end method

.method private updateLoadingState()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1f4

    const/16 v1, 0x66

    .line 3462
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    if-eqz v0, :cond_2

    .line 3463
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getLevelCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 3465
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->removeMessages(I)V

    .line 3466
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    .line 3468
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->sendTitleInfo()V

    .line 3470
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isPreDisplayScreenNailVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3471
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const/16 v1, 0x68

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 3473
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3474
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const/16 v1, 0x69

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 3484
    :cond_2
    :goto_0
    return-void

    .line 3476
    :cond_3
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    if-eqz v0, :cond_2

    .line 3477
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    .line 3478
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->removeMessages(I)V

    .line 3479
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isPreDisplayScreenNailVisible()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3480
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private updateTakenTimeLable()V
    .locals 8

    .prologue
    .line 4845
    iget v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayWidth:I

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayHeight:I

    if-ge v4, v5, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayWidth:I

    .line 4846
    .local v1, "defaultTextWidth":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    check-cast v4, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 4847
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v3, :cond_1

    .line 4855
    :goto_1
    return-void

    .line 4845
    .end local v1    # "defaultTextWidth":I
    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayHeight:I

    goto :goto_0

    .line 4849
    .restart local v1    # "defaultTextWidth":I
    .restart local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 4851
    .local v0, "date":Ljava/lang/String;
    :try_start_0
    iget v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->TIME_TEXT_SIZE:F

    const/4 v5, -0x1

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    invoke-static {v0, v1, v4, v5, v6}, Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;->newInstance(Ljava/lang/String;IFILandroid/text/Layout$Alignment;)Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTakenTimeLable:Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 4852
    :catch_0
    move-exception v2

    .line 4853
    .local v2, "e":Ljava/lang/NullPointerException;
    sget-object v4, Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;

    const-string v5, "TakenTimeLable is null while checking this item is null"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public ZoomInOutByShortCutKey(Z)Z
    .locals 11
    .param p1, "zoomIn"    # Z

    .prologue
    const/4 v4, 0x1

    const/high16 v10, 0x40000000    # 2.0f

    .line 3739
    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I

    if-nez v5, :cond_1

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    if-eq v5, v4, :cond_1

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    const/4 v6, 0x3

    if-eq v5, v6, :cond_1

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isVideoPlayIcon()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    iget v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingProgressState:I

    sget v6, Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_RUNNING:I

    if-ne v5, v6, :cond_2

    .line 3742
    :cond_1
    const/4 v4, 0x0

    .line 3758
    :goto_0
    return v4

    .line 3745
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x1f4

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPanningStartTime:J

    .line 3747
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    .line 3748
    .local v2, "controller":Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v3

    .line 3750
    .local v3, "scale":F
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v5

    div-int/lit8 v1, v5, 0x2

    .line 3751
    .local v1, "cexterX":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v5

    div-int/lit8 v0, v5, 0x2

    .line 3753
    .local v0, "centerY":I
    if-eqz p1, :cond_3

    .line 3754
    int-to-float v5, v1

    int-to-float v6, v0

    mul-float v7, v3, v10

    invoke-virtual {v2, v5, v6, v7}, Lcom/sec/android/gallery3d/ui/PositionController;->zoomIn(FFF)V

    goto :goto_0

    .line 3756
    :cond_3
    int-to-float v5, v1

    int-to-float v6, v0

    div-float v7, v3, v10

    invoke-virtual {v2, v5, v6, v7}, Lcom/sec/android/gallery3d/ui/PositionController;->zoomIn(FFF)V

    goto :goto_0
.end method

.method public assignName(Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;)V
    .locals 1
    .param p1, "type"    # Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    .prologue
    .line 4380
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4381
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->assignName(Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;)V

    .line 4382
    :cond_0
    return-void
.end method

.method public buildFallbackEffect(Lcom/sec/android/gallery3d/ui/GLView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Lcom/sec/android/gallery3d/ui/PhotoFallbackEffect;
    .locals 17
    .param p1, "root"    # Lcom/sec/android/gallery3d/ui/GLView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 3204
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    .line 3205
    .local v12, "location":Landroid/graphics/Rect;
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v12}, Lcom/sec/android/gallery3d/ui/GLView;->getBoundsOf(Lcom/sec/android/gallery3d/ui/GLView;Landroid/graphics/Rect;)Z

    move-result v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 3207
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->bounds()Landroid/graphics/Rect;

    move-result-object v9

    .line 3208
    .local v9, "fullRect":Landroid/graphics/Rect;
    new-instance v8, Lcom/sec/android/gallery3d/ui/PhotoFallbackEffect;

    invoke-direct {v8}, Lcom/sec/android/gallery3d/ui/PhotoFallbackEffect;-><init>()V

    .line 3209
    .local v8, "effect":Lcom/sec/android/gallery3d/ui/PhotoFallbackEffect;
    const/4 v10, -0x3

    .local v10, "i":I
    :goto_0
    const/4 v3, 0x3

    if-gt v10, v3, :cond_5

    .line 3210
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v3, v10}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v11

    .line 3211
    .local v11, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v11, :cond_1

    .line 3209
    :cond_0
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 3213
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v3, v10}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getScreenNail(I)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v2

    .line 3215
    .local v2, "sc":Lcom/sec/android/gallery3d/ui/ScreenNail;
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseBufferThumbnailMode()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 3216
    instance-of v3, v2, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    if-eqz v3, :cond_0

    move-object v3, v2

    check-cast v3, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->isShowingPlaceholder()Z

    move-result v3

    if-nez v3, :cond_0

    .line 3225
    :cond_2
    new-instance v13, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPhotoRect(I)Landroid/graphics/Rect;

    move-result-object v3

    invoke-direct {v13, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 3226
    .local v13, "rect":Landroid/graphics/Rect;
    invoke-static {v9, v13}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3228
    iget v3, v12, Landroid/graphics/Rect;->left:I

    iget v4, v12, Landroid/graphics/Rect;->top:I

    invoke-virtual {v13, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 3230
    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v6

    .line 3231
    .local v6, "width":I
    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v7

    .line 3233
    .local v7, "height":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v3, v10}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getImageRotation(I)I

    move-result v14

    .line 3235
    .local v14, "rotation":I
    rem-int/lit16 v3, v14, 0xb4

    if-nez v3, :cond_4

    .line 3236
    new-instance v15, Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    const/4 v3, 0x1

    invoke-direct {v15, v6, v7, v3}, Lcom/sec/android/gallery3d/glrenderer/RawTexture;-><init>(IIZ)V

    .line 3237
    .local v15, "texture":Lcom/sec/android/gallery3d/glrenderer/RawTexture;
    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->beginRenderTarget(Lcom/sec/android/gallery3d/glrenderer/RawTexture;)V

    .line 3238
    int-to-float v3, v6

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    int-to-float v4, v7

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p2

    invoke-interface {v0, v3, v4}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 3245
    :goto_2
    int-to-float v3, v14

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v16, 0x3f800000    # 1.0f

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v3, v4, v5, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->rotate(FFFF)V

    .line 3246
    neg-int v3, v6

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    neg-int v4, v7

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p2

    invoke-interface {v0, v3, v4}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 3247
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v3, p2

    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 3248
    invoke-interface/range {p2 .. p2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->endRenderTarget()V

    .line 3249
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v8, v3, v13, v15}, Lcom/sec/android/gallery3d/ui/PhotoFallbackEffect;->addEntry(Lcom/sec/android/gallery3d/data/Path;Landroid/graphics/Rect;Lcom/sec/android/gallery3d/glrenderer/RawTexture;)V

    goto/16 :goto_1

    .line 3219
    .end local v6    # "width":I
    .end local v7    # "height":I
    .end local v13    # "rect":Landroid/graphics/Rect;
    .end local v14    # "rotation":I
    .end local v15    # "texture":Lcom/sec/android/gallery3d/glrenderer/RawTexture;
    :cond_3
    instance-of v3, v2, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    if-eqz v3, :cond_0

    move-object v3, v2

    check-cast v3, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->isShowingPlaceholder()Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_1

    .line 3240
    .restart local v6    # "width":I
    .restart local v7    # "height":I
    .restart local v13    # "rect":Landroid/graphics/Rect;
    .restart local v14    # "rotation":I
    :cond_4
    new-instance v15, Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    const/4 v3, 0x1

    invoke-direct {v15, v7, v6, v3}, Lcom/sec/android/gallery3d/glrenderer/RawTexture;-><init>(IIZ)V

    .line 3241
    .restart local v15    # "texture":Lcom/sec/android/gallery3d/glrenderer/RawTexture;
    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->beginRenderTarget(Lcom/sec/android/gallery3d/glrenderer/RawTexture;)V

    .line 3242
    int-to-float v3, v7

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    int-to-float v4, v6

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p2

    invoke-interface {v0, v3, v4}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    goto :goto_2

    .line 3251
    .end local v2    # "sc":Lcom/sec/android/gallery3d/ui/ScreenNail;
    .end local v6    # "width":I
    .end local v7    # "height":I
    .end local v11    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v13    # "rect":Landroid/graphics/Rect;
    .end local v14    # "rotation":I
    .end local v15    # "texture":Lcom/sec/android/gallery3d/glrenderer/RawTexture;
    :cond_5
    return-object v8
.end method

.method public canUndo()Z
    .locals 1

    .prologue
    .line 2610
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBarState:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkManualFD(FFZ)Z
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "faceTag"    # Z

    .prologue
    .line 2400
    const/4 v2, 0x0

    .line 2402
    .local v2, "result":Z
    const/4 v1, 0x0

    .line 2403
    .local v1, "isFaceIndicator":Z
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v3, :cond_0

    .line 2404
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    float-to-int v4, p1

    float-to-int v5, p2

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->isFaceIndicatorArea(II)Z

    move-result v1

    .line 2407
    :cond_0
    const/4 v0, 0x1

    .line 2408
    .local v0, "contactPopupButtonState":Z
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v3, :cond_1

    .line 2409
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getContactPopupButtonState()Z

    move-result v3

    if-nez v3, :cond_4

    const/4 v0, 0x1

    .line 2412
    :cond_1
    :goto_0
    if-eqz p3, :cond_2

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->isScaned()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsBackkeyPressed:Z

    if-nez v3, :cond_2

    .line 2413
    const/4 v2, 0x1

    .line 2415
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->IsPressedIcon()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2416
    const/4 v2, 0x0

    .line 2418
    :cond_3
    return v2

    .line 2409
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearCurFace()V
    .locals 1

    .prologue
    .line 4424
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4425
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->clearCurFace()V

    .line 4427
    :cond_0
    return-void
.end method

.method public deleteCurrentImage()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5185
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v1, v2, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->flingFilmY(II)I

    move-result v0

    .line 5186
    .local v0, "duration":I
    if-ltz v0, :cond_0

    .line 5187
    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I

    .line 5188
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->setPopFromTop(Z)V

    .line 5189
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGestureListener:Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->deleteAfterAnimation(I)V
    invoke-static {v1, v0}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->access$10500(Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;I)V

    .line 5191
    :cond_0
    return-void
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 3520
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x71

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x72

    if-ne v0, v1, :cond_2

    .line 3522
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->isCtrlKeyPressed:Z

    .line 3526
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 3523
    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 3524
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->isCtrlKeyPressed:Z

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 4885
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v2, 0x3e8

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_3

    .line 4886
    :cond_0
    const/4 v0, 0x0

    .line 4887
    .local v0, "consumed":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v1, :cond_1

    .line 4888
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->onGenericMotionEnter(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 4889
    :cond_1
    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v1, :cond_2

    .line 4890
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->onGenericMotion(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 4891
    :cond_2
    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    if-eqz v1, :cond_3

    .line 4892
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v2, p1}, Landroid/view/View$OnGenericMotionListener;->onGenericMotion(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 4894
    .end local v0    # "consumed":Z
    :cond_3
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1
.end method

.method public doDMRPinchZoom(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "scale"    # F

    .prologue
    .line 5056
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMRPinchZoom:Z

    if-eqz v0, :cond_0

    .line 5057
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    if-eqz v0, :cond_0

    .line 5058
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/gallery3d/ui/PositionController;->zoomIn(FFF)V

    .line 5060
    :cond_0
    return-void
.end method

.method public endScale()V
    .locals 2

    .prologue
    .line 3790
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMyScaleListener:Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;

    if-eqz v0, :cond_0

    .line 3791
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMyScaleListener:Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->onScaleEnd(Landroid/view/ScaleGestureDetector;)V

    .line 3793
    :cond_0
    return-void
.end method

.method public getAssignedName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4385
    const/4 v0, 0x0

    .line 4386
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v1, :cond_0

    .line 4387
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getAssignedName()Ljava/lang/String;

    move-result-object v0

    .line 4389
    :cond_0
    return-object v0
.end method

.method public getCurContactLookupKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4479
    const/4 v0, 0x0

    .line 4480
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v1, :cond_0

    .line 4481
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getCurContactLookupKey()Ljava/lang/String;

    move-result-object v0

    .line 4483
    :cond_0
    return-object v0
.end method

.method public getCurFaceId()I
    .locals 1

    .prologue
    .line 4361
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4362
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getCurFaceId()I

    move-result v0

    .line 4364
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCurrentScale()F
    .locals 1

    .prologue
    .line 4227
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v0

    return v0
.end method

.method public getFilmMode()Z
    .locals 1

    .prologue
    .line 2448
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    return v0
.end method

.method public getIsRotated()Z
    .locals 1

    .prologue
    .line 5093
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsRotated:Z

    return v0
.end method

.method public getLoadingState()I
    .locals 1

    .prologue
    .line 5052
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    return v0
.end method

.method public getMediaSetKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4393
    const/4 v0, 0x0

    .line 4394
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v1, :cond_0

    .line 4395
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getMediaSetKey()Ljava/lang/String;

    move-result-object v0

    .line 4397
    :cond_0
    return-object v0
.end method

.method public getMotionDetector()Lcom/sec/android/gallery3d/util/MotionDetector;
    .locals 1

    .prologue
    .line 4123
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    return-object v0
.end method

.method public getPhotoRect(I)Landroid/graphics/Rect;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 3200
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;
    .locals 1

    .prologue
    .line 4276
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    return-object v0
.end method

.method public getRectOfImage()Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 3692
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v1

    .line 3693
    .local v1, "bounds":Landroid/graphics/Rect;
    const/16 v0, 0xc

    .line 3694
    .local v0, "OUTLINE":I
    iget v2, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v2, v2, 0xc

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 3695
    iget v2, v1, Landroid/graphics/Rect;->right:I

    add-int/lit8 v2, v2, -0xc

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 3696
    return-object v1
.end method

.method public final getScaleMin()F
    .locals 1

    .prologue
    .line 4231
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getMinScale()F

    move-result v0

    return v0
.end method

.method public getScaledFaceRectForCrop()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 4493
    const/4 v0, 0x0

    .line 4494
    .local v0, "r":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v1, :cond_0

    .line 4495
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getScaledFaceRectForCrop()Landroid/graphics/RectF;

    move-result-object v0

    .line 4498
    :cond_0
    return-object v0
.end method

.method public getScrollThresholdRatio()F
    .locals 1

    .prologue
    .line 536
    const v0, 0x3c83126f    # 0.016f

    return v0
.end method

.method public getShowBarState()Z
    .locals 1

    .prologue
    .line 4794
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsShowBars:Z

    return v0
.end method

.method public getTileImageView()Lcom/sec/android/gallery3d/ui/TileImageView;
    .locals 1

    .prologue
    .line 4280
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    return-object v0
.end method

.method public getTransitionMode()I
    .locals 1

    .prologue
    .line 4214
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I

    return v0
.end method

.method public hideContactPopupView()V
    .locals 1

    .prologue
    .line 4437
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4438
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->hideContactPopupView()V

    .line 4440
    :cond_0
    return-void
.end method

.method public hideFaceIndicatorView()V
    .locals 1

    .prologue
    .line 4449
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getNameMenuPopupVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4450
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->hide()V

    .line 4452
    :cond_0
    return-void
.end method

.method protected initAccessibilityListener()V
    .locals 1

    .prologue
    .line 5233
    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$12;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/PhotoView$12;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGLViewAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    .line 5300
    return-void
.end method

.method public invalidateFaceIndicatorView(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "photo"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 4511
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4512
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4513
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setPositionController(Lcom/sec/android/gallery3d/ui/PositionController;)V

    .line 4514
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 4517
    :cond_0
    return-void
.end method

.method public isCurFaceExistName()Z
    .locals 2

    .prologue
    .line 4502
    const/4 v0, 0x0

    .line 4503
    .local v0, "existName":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v1, :cond_0

    .line 4504
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->isCurFaceExistName()Z

    move-result v0

    .line 4507
    :cond_0
    return v0
.end method

.method public isDeleting()Z
    .locals 1

    .prologue
    .line 915
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->hasDeletingBox()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDmrEnabled()Z
    .locals 1

    .prologue
    .line 5071
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 5072
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsDmrEnabled:Z

    .line 5074
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInTransition()Z
    .locals 1

    .prologue
    .line 4222
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOffContextualTag()Z
    .locals 1

    .prologue
    .line 5101
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOffContextualTag:Z

    return v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 4206
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsPaused:Z

    return v0
.end method

.method public jumpTo(I)Z
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x1

    .line 3889
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I

    if-eqz v1, :cond_0

    .line 3890
    const/4 v0, 0x0

    .line 3895
    :goto_0
    return v0

    .line 3891
    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmstripJump:Z

    .line 3892
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setImageChanging(Z)V

    .line 3893
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v1, p1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->moveTo(I)V

    .line 3894
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->moveTo(I)V

    goto :goto_0
.end method

.method public lockNotify()V
    .locals 1

    .prologue
    .line 4473
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4474
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->lockNotify()V

    .line 4476
    :cond_0
    return-void
.end method

.method public nameChanged(Z)V
    .locals 1
    .param p1, "changed"    # Z

    .prologue
    .line 4413
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4414
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->nameChanged(Z)V

    .line 4416
    :cond_0
    return-void
.end method

.method public nextImage()Z
    .locals 1

    .prologue
    .line 3700
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->slideToNextPicture()Z

    move-result v0

    return v0
.end method

.method public nextImageForAirMotion()V
    .locals 3

    .prologue
    .line 3722
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mNextBound:I

    if-gtz v0, :cond_1

    .line 3723
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v1

    neg-int v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->scrollPage(II)V

    .line 3736
    :cond_0
    :goto_0
    return-void

    .line 3726
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsPressed:Z

    if-nez v0, :cond_0

    .line 3727
    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$8;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/PhotoView$8;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView(Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;)V

    .line 3734
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    const v1, -0x3c698000    # -301.0f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->checkFlingVelocity(FF)V

    goto :goto_0
.end method

.method public notifyCurrentImageInvalidated()V
    .locals 5

    .prologue
    .line 3349
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/TileImageView;->notifyModelInvalidated()V

    .line 3350
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getImageRotation()I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mImageRotation:I

    .line 3351
    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Size;-><init>()V

    .line 3352
    .local v0, "s":Lcom/sec/android/gallery3d/ui/PhotoView$Size;
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mImageRotation:I

    div-int/lit8 v1, v1, 0x5a

    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_1

    .line 3354
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 3355
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    .line 3361
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->setImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;Landroid/graphics/Rect;)V

    .line 3363
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    if-eqz v1, :cond_0

    .line 3364
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/app/GalleryMotion;->initImageScale(Lcom/sec/android/gallery3d/ui/PositionController;II)V

    .line 3367
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->updateLoadingState()V

    .line 3369
    return-void

    .line 3358
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 3359
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    goto :goto_0
.end method

.method public notifyCurrentImageInvalidated(Z)V
    .locals 4
    .param p1, "noRotate"    # Z

    .prologue
    const/4 v3, 0x0

    .line 3329
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/TileImageView;->notifyModelInvalidated()V

    .line 3330
    iput v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mImageRotation:I

    .line 3331
    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Size;-><init>()V

    .line 3332
    .local v0, "s":Lcom/sec/android/gallery3d/ui/PhotoView$Size;
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mImageRotation:I

    div-int/lit8 v1, v1, 0x5a

    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_0

    .line 3334
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 3335
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    .line 3341
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v0, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->setImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;Landroid/graphics/Rect;)V

    .line 3342
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->updateLoadingState()V

    .line 3344
    return-void

    .line 3338
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 3339
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    goto :goto_0
.end method

.method public notifyDataChange([III)V
    .locals 15
    .param p1, "fromIndex"    # [I
    .param p2, "prevBound"    # I
    .param p3, "nextBound"    # I

    .prologue
    .line 861
    move/from16 v0, p2

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevBound:I

    .line 862
    move/from16 v0, p3

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mNextBound:I

    .line 865
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I

    const v3, 0x7fffffff

    if-eq v2, v3, :cond_0

    .line 866
    iget v11, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I

    .line 867
    .local v11, "k":I
    const v2, 0x7fffffff

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I

    .line 868
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    const/4 v2, 0x7

    if-ge v9, v2, :cond_0

    .line 869
    aget v2, p1, v9

    if-ne v2, v11, :cond_2

    .line 870
    add-int/lit8 v2, v9, -0x3

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTouchBoxIndex:I

    .line 877
    .end local v9    # "i":I
    .end local v11    # "k":I
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoIndexHint:I

    const v3, 0x7fffffff

    if-eq v2, v3, :cond_1

    .line 878
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoIndexHint:I

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getCurrentIndex()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_1

    .line 879
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideUndoBar()V

    .line 884
    :cond_1
    const/4 v9, -0x3

    .restart local v9    # "i":I
    :goto_1
    const/4 v2, 0x3

    if-gt v9, v2, :cond_3

    .line 885
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v2, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    .line 886
    .local v13, "p":Lcom/sec/android/gallery3d/ui/PhotoView$Picture;
    invoke-interface {v13}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->reload()V

    .line 887
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSizes:[Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    add-int/lit8 v3, v9, 0x3

    invoke-interface {v13}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->getSize()Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    move-result-object v4

    aput-object v4, v2, v3

    .line 884
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 868
    .end local v13    # "p":Lcom/sec/android/gallery3d/ui/PhotoView$Picture;
    .restart local v11    # "k":I
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 890
    .end local v11    # "k":I
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->hasDeletingBox()Z

    move-result v14

    .line 893
    .local v14, "wasDeleting":Z
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevBound:I

    if-gez v3, :cond_4

    const/4 v4, 0x1

    :goto_2
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mNextBound:I

    if-lez v3, :cond_5

    const/4 v5, 0x1

    :goto_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isCamera(I)Z

    move-result v6

    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSizes:[Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmstripJump:Z

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v8}, Lcom/sec/android/gallery3d/ui/PositionController;->moveBox([IZZZ[Lcom/sec/android/gallery3d/ui/PhotoView$Size;Z)V

    .line 896
    const/4 v9, -0x3

    :goto_4
    const/4 v2, 0x3

    if-gt v9, v2, :cond_6

    .line 897
    invoke-direct {p0, v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->setPictureSize(I)V

    .line 896
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 893
    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    :cond_5
    const/4 v5, 0x0

    goto :goto_3

    .line 900
    :cond_6
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->hasDeletingBox()Z

    move-result v10

    .line 904
    .local v10, "isDeleting":Z
    if-eqz v14, :cond_7

    if-nez v10, :cond_7

    .line 905
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->removeMessages(I)V

    .line 906
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v12

    .line 907
    .local v12, "m":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const-wide/16 v4, 0x258

    invoke-virtual {v2, v12, v4, v5}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 910
    .end local v12    # "m":Landroid/os/Message;
    :cond_7
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onSizeChanged()V

    .line 911
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    .line 912
    return-void
.end method

.method public notifyImageChange(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 919
    if-nez p1, :cond_3

    .line 920
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onCurrentImageUpdated()V

    .line 923
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/TileImageView;->notifyModelInvalidated()V

    .line 924
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    if-eqz v1, :cond_0

    .line 925
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurrentPhoto()V

    .line 926
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getImageRotation()I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mImageRotation:I

    .line 928
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Size;-><init>()V

    .line 929
    .local v0, "s":Lcom/sec/android/gallery3d/ui/PhotoView$Size;
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mImageRotation:I

    div-int/lit8 v1, v1, 0x5a

    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_2

    .line 931
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 932
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    .line 938
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->setImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;Landroid/graphics/Rect;)V

    .line 939
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->updateHdmi3D()V

    .line 940
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->updateLoadingState()V

    .line 952
    .end local v0    # "s":Lcom/sec/android/gallery3d/ui/PhotoView$Size;
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->reload()V

    .line 953
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->setPictureSize(I)V

    .line 954
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    .line 955
    return-void

    .line 935
    .restart local v0    # "s":Lcom/sec/android/gallery3d/ui/PhotoView$Size;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 936
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    goto :goto_0

    .line 944
    .end local v0    # "s":Lcom/sec/android/gallery3d/ui/PhotoView$Size;
    :cond_3
    if-ltz p1, :cond_1

    .line 947
    if-lez p1, :cond_1

    goto :goto_1
.end method

.method public notifyOnNewImage()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4127
    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Size;-><init>()V

    .line 4128
    .local v0, "s":Lcom/sec/android/gallery3d/ui/PhotoView$Size;
    iput v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 4129
    iput v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    .line 4131
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v0, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->setImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;Landroid/graphics/Rect;)V

    .line 4133
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v1, :cond_0

    .line 4134
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v1, :cond_0

    .line 4135
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->resetView()V

    .line 4139
    :cond_0
    return-void
.end method

.method public notifyTransitionComplete()V
    .locals 2

    .prologue
    .line 4173
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isPanoramaScrolling()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4174
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 4176
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessage(I)Z

    .line 4177
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 4841
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsBackkeyPressed:Z

    .line 4842
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 4297
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPeek(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4317
    :cond_0
    :goto_0
    return-void

    .line 4299
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v1

    .line 4300
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v0

    .line 4302
    .local v0, "height":I
    if-lez v1, :cond_2

    if-lez v0, :cond_2

    .line 4303
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v2

    int-to-float v3, v1

    int-to-float v4, v0

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 4305
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->initDisplaySize(Landroid/content/Context;)V

    .line 4307
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    if-eqz v2, :cond_3

    .line 4308
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    sget-object v3, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_NONE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/util/MotionDetector;->setState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)V

    .line 4311
    :cond_3
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v2, :cond_0

    .line 4312
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    if-eqz v2, :cond_0

    .line 4313
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method public onContactsChange()V
    .locals 1

    .prologue
    .line 4798
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v0, :cond_0

    .line 4799
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4800
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->onContactsChange()V

    .line 4803
    :cond_0
    return-void
.end method

.method public onDecodeImageFinished()V
    .locals 1

    .prologue
    .line 4823
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDecodeImageFinished:Z

    .line 4824
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadImageFinished:Z

    if-eqz v0, :cond_0

    .line 4825
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->onUpdateImageFinished()V

    .line 4827
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 3488
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x71

    if-eq v1, v2, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x72

    if-ne v1, v2, :cond_1

    .line 3489
    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->isCtrlKeyPressed:Z

    .line 3492
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3497
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getNameMenuPopupVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3499
    sparse-switch p1, :sswitch_data_0

    .line 3509
    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x71

    if-eq v2, v3, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x72

    if-ne v2, v3, :cond_2

    .line 3510
    :cond_1
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->isCtrlKeyPressed:Z

    move v0, v1

    .line 3513
    :cond_2
    :goto_0
    return v0

    .line 3501
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->movePopupFocus(Z)Z

    move-result v0

    goto :goto_0

    .line 3503
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->movePopupFocus(Z)Z

    move-result v0

    goto :goto_0

    .line 3506
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->clickPopupFocus()Z

    move-result v0

    goto :goto_0

    .line 3499
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x17 -> :sswitch_2
        0x42 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 11
    .param p1, "changeSize"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 965
    sub-int v6, p4, p2

    .line 966
    .local v6, "w":I
    sub-int v2, p5, p3

    .line 967
    .local v2, "h":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v6, v2}, Lcom/sec/android/gallery3d/ui/TileImageView;->layout(IIII)V

    .line 968
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v6, v2}, Lcom/sec/android/gallery3d/ui/EdgeView;->layout(IIII)V

    .line 969
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBar:Lcom/sec/android/gallery3d/ui/UndoBarView;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/gallery3d/ui/UndoBarView;->measure(II)V

    .line 970
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBar:Lcom/sec/android/gallery3d/ui/UndoBarView;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBar:Lcom/sec/android/gallery3d/ui/UndoBarView;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/UndoBarView;->getMeasuredHeight()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBar:Lcom/sec/android/gallery3d/ui/UndoBarView;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/UndoBarView;->getMeasuredHeight()I

    move-result v10

    mul-int/lit8 v10, v10, 0x2

    invoke-virtual {v7, v8, v9, v6, v10}, Lcom/sec/android/gallery3d/ui/UndoBarView;->layout(IIII)V

    .line 972
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v5

    .line 973
    .local v5, "root":Lcom/sec/android/gallery3d/ui/GLRoot;
    invoke-interface {v5}, Lcom/sec/android/gallery3d/ui/GLRoot;->getDisplayRotation()I

    move-result v1

    .line 974
    .local v1, "displayRotation":I
    invoke-interface {v5}, Lcom/sec/android/gallery3d/ui/GLRoot;->getCompensation()I

    move-result v0

    .line 975
    .local v0, "compensation":I
    iget v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayRotation:I

    if-ne v7, v1, :cond_0

    iget v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCompensation:I

    if-eq v7, v0, :cond_2

    .line 976
    :cond_0
    iput v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayRotation:I

    .line 977
    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCompensation:I

    .line 982
    const/4 v3, -0x3

    .local v3, "i":I
    :goto_0
    const/4 v7, 0x3

    if-gt v3, v7, :cond_2

    .line 983
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v7, v3}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    .line 984
    .local v4, "p":Lcom/sec/android/gallery3d/ui/PhotoView$Picture;
    invoke-interface {v4}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->isCamera()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 985
    invoke-interface {v4}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->forceSize()V

    .line 982
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 990
    .end local v3    # "i":I
    .end local v4    # "p":Lcom/sec/android/gallery3d/ui/PhotoView$Picture;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->updateCameraRect()V

    .line 991
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/ui/PositionController;->setConstrainedFrame(Landroid/graphics/Rect;)V

    .line 992
    if-eqz p1, :cond_3

    .line 993
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/gallery3d/ui/PositionController;->setViewSize(II)V

    .line 997
    :cond_3
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->setViewSize(II)V

    .line 998
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGestureRecognizer:Lcom/sec/android/gallery3d/ui/GestureRecognizer;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->setViewSize(II)V

    .line 1001
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    if-eqz v7, :cond_4

    .line 1002
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v9

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v10

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/android/gallery3d/app/GalleryMotion;->initImageScale(Lcom/sec/android/gallery3d/ui/PositionController;II)V

    .line 1004
    :cond_4
    return-void
.end method

.method public onLoadImageFinished()V
    .locals 1

    .prologue
    .line 4816
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadImageFinished:Z

    .line 4817
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDecodeImageFinished:Z

    if-eqz v0, :cond_0

    .line 4818
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->onUpdateImageFinished()V

    .line 4820
    :cond_0
    return-void
.end method

.method public onMWLayoutChanged()V
    .locals 1

    .prologue
    .line 4732
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    if-eqz v0, :cond_0

    .line 4733
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->onMWLayoutChanged()V

    .line 4735
    :cond_0
    return-void
.end method

.method public onOCRRecognizeComplete(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4858
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    if-eqz v0, :cond_0

    .line 4859
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->onRecognizeComplet(Landroid/content/Intent;)V

    .line 4861
    :cond_0
    return-void
.end method

.method public onStylusButtonEvent(Landroid/view/MotionEvent;I)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "clipboardId"    # I

    .prologue
    .line 4836
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    if-eqz v0, :cond_0

    .line 4837
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->onStylusButtonEvent(Landroid/view/MotionEvent;I)V

    .line 4838
    :cond_0
    return-void
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v13, 0x7f0f01ba

    const/16 v12, 0x9

    const/4 v7, 0x1

    const/4 v11, 0x4

    const/4 v8, 0x0

    .line 1507
    sget-object v6, Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onTouch Action ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1508
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    if-eqz v6, :cond_1

    .line 1713
    :cond_0
    :goto_0
    return v7

    .line 1511
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGestureRecognizer:Lcom/sec/android/gallery3d/ui/GestureRecognizer;

    invoke-virtual {v6, p1}, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 1513
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePositionControllerScale:Z

    if-eqz v6, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    const/4 v9, 0x2

    if-lt v6, v9, :cond_2

    .line 1514
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v9

    add-float/2addr v6, v9

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float v1, v6, v9

    .line 1515
    .local v1, "focusX":F
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v9

    add-float/2addr v6, v9

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float v2, v6, v9

    .line 1516
    .local v2, "focusY":F
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v6, v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->beginScale(FF)V

    .line 1519
    .end local v1    # "focusX":F
    .end local v2    # "focusY":F
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-nez v6, :cond_3

    .line 1520
    iput-boolean v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOnScale:Z

    .line 1522
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    goto :goto_0

    .line 1628
    :sswitch_0
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsPressed:Z

    .line 1629
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualBooster:Z

    if-eqz v6, :cond_17

    .line 1630
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->cpuBoost200MS()V

    .line 1634
    :goto_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v6, :cond_4

    .line 1635
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6, p1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->touchAction(Landroid/view/MotionEvent;)V

    .line 1637
    :cond_4
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPanning(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 1638
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01c1

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 1639
    .local v4, "tapLayout":Landroid/widget/RelativeLayout;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1640
    .local v0, "arrowLayout":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01bf

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1641
    .local v5, "text":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01c0

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 1642
    .local v3, "head":Landroid/widget/ImageView;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v6

    if-nez v6, :cond_0

    .line 1643
    invoke-virtual {v4, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1644
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopTapAnimationHelp()V

    .line 1645
    if-eqz v0, :cond_5

    .line 1646
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1647
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->startArrayAnimationHelp()V

    .line 1649
    :cond_5
    if-eqz v5, :cond_6

    .line 1650
    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1651
    :cond_6
    if-eqz v3, :cond_0

    .line 1652
    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1524
    .end local v0    # "arrowLayout":Landroid/widget/LinearLayout;
    .end local v3    # "head":Landroid/widget/ImageView;
    .end local v4    # "tapLayout":Landroid/widget/RelativeLayout;
    .end local v5    # "text":Landroid/widget/TextView;
    :sswitch_1
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMultiTouch:Z

    goto/16 :goto_0

    .line 1528
    :sswitch_2
    iput-boolean v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMultiTouch:Z

    goto/16 :goto_0

    .line 1531
    :sswitch_3
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v6, :cond_8

    .line 1532
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->setSoundSceneIconPressed(Z)V

    .line 1533
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->setPanoramaIconPressed(Z)V

    .line 1534
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->set3DPanoramaIconPressed(Z)V

    .line 1535
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->setOutOfFocusIconPressed(Z)V

    .line 1536
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->setSequenceIconPressed(Z)V

    .line 1537
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->setMagicShotIconPressed(Z)V

    .line 1538
    iput-boolean v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsPressed:Z

    .line 1540
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->is3DPanoramaIconPressed()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1541
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->set3dDisplayIconPressed(Z)V

    .line 1542
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->is3dDisplayEnabled()Z

    move-result v6

    if-nez v6, :cond_14

    move v6, v7

    :goto_2
    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->set3dDisplayEnable(Z)V

    .line 1545
    :cond_7
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isBurstShotPlayIconPressed()Z

    move-result v6

    if-eqz v6, :cond_15

    .line 1546
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->setBurstShotPlayIconPressed(Z)V

    .line 1552
    :cond_8
    :goto_3
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPanning(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1553
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01c1

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 1554
    .restart local v4    # "tapLayout":Landroid/widget/RelativeLayout;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1555
    .restart local v0    # "arrowLayout":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01bf

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1556
    .restart local v5    # "text":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01c0

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 1557
    .restart local v3    # "head":Landroid/widget/ImageView;
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v6

    if-nez v6, :cond_b

    .line 1558
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1559
    if-eqz v5, :cond_9

    .line 1560
    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1561
    :cond_9
    if-eqz v3, :cond_a

    .line 1562
    invoke-virtual {v3, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1563
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopArrayAnimationHelp()V

    .line 1564
    if-eqz v4, :cond_b

    iget-boolean v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOperatePanning:Z

    if-nez v6, :cond_b

    .line 1565
    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1566
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->startTapAnimationHelp()V

    .line 1570
    :cond_b
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOperatePanning:Z

    if-eqz v6, :cond_c

    .line 1571
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFinishHandler:Landroid/os/Handler;

    const/16 v9, 0x64

    invoke-virtual {v6, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1575
    .end local v0    # "arrowLayout":Landroid/widget/LinearLayout;
    .end local v3    # "head":Landroid/widget/ImageView;
    .end local v4    # "tapLayout":Landroid/widget/RelativeLayout;
    .end local v5    # "text":Landroid/widget/TextView;
    :cond_c
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpTilt(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 1576
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01ce

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1577
    .restart local v5    # "text":Landroid/widget/TextView;
    if-eqz v5, :cond_d

    .line 1578
    const v6, 0x7f0e00e6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1579
    :cond_d
    iput v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomIn:I

    .line 1580
    iput v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomOut:I

    .line 1583
    .end local v5    # "text":Landroid/widget/TextView;
    :cond_e
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPeek(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 1584
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01c1

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 1585
    .restart local v4    # "tapLayout":Landroid/widget/RelativeLayout;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1586
    .restart local v0    # "arrowLayout":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01c7

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1587
    .restart local v5    # "text":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01c8

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 1588
    .restart local v3    # "head":Landroid/widget/ImageView;
    if-eqz v0, :cond_11

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v6

    if-nez v6, :cond_11

    .line 1589
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1590
    if-eqz v5, :cond_f

    .line 1591
    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1592
    :cond_f
    if-eqz v3, :cond_10

    .line 1593
    invoke-virtual {v3, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1594
    :cond_10
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopArrayAnimationHelp()V

    .line 1595
    if-eqz v4, :cond_11

    iget-boolean v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOperatePeek:Z

    if-nez v6, :cond_11

    .line 1596
    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1597
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->startTapAnimationHelp()V

    .line 1601
    :cond_11
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOperatePeek:Z

    if-eqz v6, :cond_12

    .line 1602
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFinishHandler:Landroid/os/Handler;

    const/16 v9, 0x64

    invoke-virtual {v6, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1606
    .end local v0    # "arrowLayout":Landroid/widget/LinearLayout;
    .end local v3    # "head":Landroid/widget/ImageView;
    .end local v4    # "tapLayout":Landroid/widget/RelativeLayout;
    .end local v5    # "text":Landroid/widget/TextView;
    :cond_12
    :sswitch_4
    iput-boolean v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsPressed:Z

    .line 1607
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualBooster:Z

    if-eqz v6, :cond_16

    .line 1608
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->cpuBoost600MS()V

    .line 1613
    :goto_4
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/PositionController;->endScale()V

    .line 1614
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->onMotionMoveEnd()V

    .line 1616
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMyGestureLongPressDetected:Z

    if-eqz v6, :cond_13

    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v6, :cond_13

    .line 1617
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v6, :cond_13

    .line 1618
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->resetPressedIndex()V

    .line 1621
    :cond_13
    iput-boolean v8, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMyGestureLongPressDetected:Z

    goto/16 :goto_0

    :cond_14
    move v6, v8

    .line 1542
    goto/16 :goto_2

    .line 1547
    :cond_15
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isBurstShotSettingIconPressed()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1548
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->setBurstShotSettingIconPressed(Z)V

    goto/16 :goto_3

    .line 1610
    :cond_16
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->cpuBoostRelease()V

    .line 1611
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->cpuBoostCancel()V

    goto :goto_4

    .line 1624
    :sswitch_5
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v6, :cond_0

    .line 1625
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6, p1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->touchAction(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 1632
    :cond_17
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->cpuBoost()V

    goto/16 :goto_1

    .line 1654
    :cond_18
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPeek(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1655
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01c1

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 1656
    .restart local v4    # "tapLayout":Landroid/widget/RelativeLayout;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1657
    .restart local v0    # "arrowLayout":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01c7

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1658
    .restart local v5    # "text":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f01c8

    invoke-virtual {v6, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 1659
    .restart local v3    # "head":Landroid/widget/ImageView;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v6

    if-nez v6, :cond_0

    .line 1660
    invoke-virtual {v4, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1661
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopTapAnimationHelp()V

    .line 1662
    if-eqz v0, :cond_19

    .line 1663
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1664
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->startArrayAnimationHelp()V

    .line 1666
    :cond_19
    if-eqz v5, :cond_1a

    .line 1667
    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1668
    :cond_1a
    if-eqz v3, :cond_0

    .line 1669
    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1677
    .end local v0    # "arrowLayout":Landroid/widget/LinearLayout;
    .end local v3    # "head":Landroid/widget/ImageView;
    .end local v4    # "tapLayout":Landroid/widget/RelativeLayout;
    .end local v5    # "text":Landroid/widget/TextView;
    :sswitch_6
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v6, :cond_1b

    .line 1678
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v6, p1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->touchAction(Landroid/view/MotionEvent;)V

    .line 1680
    :cond_1b
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v6, :cond_0

    .line 1681
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-nez v6, :cond_0

    .line 1682
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->scrollAfterLongPress(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 1689
    :sswitch_7
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->isCtrlKeyPressed:Z

    if-eqz v6, :cond_1d

    .line 1690
    invoke-virtual {p1, v12}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v6

    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v8

    if-nez v6, :cond_1c

    .line 1691
    const/high16 v6, 0x3fa00000    # 1.25f

    invoke-virtual {p0, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    goto/16 :goto_0

    .line 1692
    :cond_1c
    invoke-virtual {p1, v12}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v6

    const/high16 v8, -0x40800000    # -1.0f

    cmpl-float v6, v6, v8

    if-nez v6, :cond_0

    .line 1693
    const/high16 v6, 0x3f400000    # 0.75f

    invoke-virtual {p0, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    goto/16 :goto_0

    .line 1696
    :cond_1d
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/PositionController;->isInScale()Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 1697
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->endScale()V

    .line 1699
    :cond_1e
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/PositionController;->isSliding()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1700
    invoke-virtual {p1, v12}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v6

    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v8

    if-nez v6, :cond_1f

    .line 1701
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 1702
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->nextImage()Z

    goto/16 :goto_0

    .line 1703
    :cond_1f
    invoke-virtual {p1, v12}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v6

    const/high16 v8, -0x40800000    # -1.0f

    cmpl-float v6, v6, v8

    if-nez v6, :cond_0

    .line 1704
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 1705
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->previousImage()Z

    goto/16 :goto_0

    .line 1522
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_3
        0x2 -> :sswitch_6
        0x3 -> :sswitch_4
        0x6 -> :sswitch_2
        0x8 -> :sswitch_7
        0x105 -> :sswitch_1
        0x106 -> :sswitch_2
        0x3e8 -> :sswitch_5
    .end sparse-switch
.end method

.method public onUpdateImageFinished()V
    .locals 0

    .prologue
    .line 4813
    return-void
.end method

.method public onUpdatemageStarted()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4806
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v0, :cond_0

    .line 4807
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadImageFinished:Z

    .line 4808
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDecodeImageFinished:Z

    .line 4810
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2456
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->skipAnimation()V

    .line 2457
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/TileImageView;->freeTextures()V

    .line 2458
    const/4 v0, -0x3

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 2459
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    invoke-interface {v1, v4}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V

    .line 2458
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2461
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideUndoBar()V

    .line 2463
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->isInTransition()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2464
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyTransitionComplete()V

    .line 2466
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    if-eqz v1, :cond_2

    .line 2467
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryMotion;->pause()V

    .line 2469
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->deviceRotationUnLock(Landroid/content/Context;)V

    .line 2470
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    if-eqz v1, :cond_3

    .line 2471
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    sget-object v2, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_NONE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/MotionDetector;->setState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)V

    .line 2472
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/util/MotionDetector;->unregisterMotionListener()V

    .line 2473
    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    .line 2475
    :cond_3
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirstShowDialog:Z

    .line 2477
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsPaused:Z

    .line 2480
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v1, :cond_4

    .line 2481
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v1, :cond_4

    .line 2482
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->pause()V

    .line 2488
    :cond_4
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v1, :cond_5

    .line 2489
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    if-eqz v1, :cond_5

    .line 2490
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->pause()V

    .line 2494
    :cond_5
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelp(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2495
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->stopAniamtionHelp()V

    .line 2497
    :cond_6
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAgifMode:Z

    if-eqz v1, :cond_7

    .line 2498
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetAgifMode(ZLcom/sec/android/gallery3d/data/MediaItem;)V

    .line 2501
    :cond_7
    return-void
.end method

.method public previousImage()Z
    .locals 1

    .prologue
    .line 3688
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->slideToPrevPicture()Z

    move-result v0

    return v0
.end method

.method public previousImageForAirMotion()V
    .locals 3

    .prologue
    .line 3704
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevBound:I

    if-ltz v0, :cond_1

    .line 3705
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->scrollPage(II)V

    .line 3719
    :cond_0
    :goto_0
    return-void

    .line 3708
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsPressed:Z

    if-nez v0, :cond_0

    .line 3709
    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$7;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/PhotoView$7;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView(Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;)V

    .line 3717
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    const v1, 0x43968000    # 301.0f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->checkFlingVelocity(FF)V

    goto :goto_0
.end method

.method public removeContactPopupView()V
    .locals 1

    .prologue
    .line 4430
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4431
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->hideContactPopupView()V

    .line 4432
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->removeContactPopupView()V

    .line 4434
    :cond_0
    return-void
.end method

.method public removeOcrPopup()V
    .locals 1

    .prologue
    .line 4461
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    if-eqz v0, :cond_0

    .line 4462
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->deletePopup()V

    .line 4464
    :cond_0
    return-void
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 24
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 2621
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirst:Z

    move/from16 v19, v0

    if-eqz v19, :cond_0

    .line 2624
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    invoke-interface/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->reload()V

    .line 2628
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    move/from16 v19, v0

    if-nez v19, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    invoke-interface/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->isCamera()Z

    move-result v19

    if-eqz v19, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/PositionController;->isCenter()Z

    move-result v19

    if-eqz v19, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v19

    if-eqz v19, :cond_4

    const/4 v5, 0x1

    .line 2630
    .local v5, "full":Z
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirst:Z

    move/from16 v19, v0

    if-nez v19, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFullScreenCamera:Z

    move/from16 v19, v0

    move/from16 v0, v19

    if-eq v5, v0, :cond_2

    .line 2631
    :cond_1
    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFullScreenCamera:Z

    .line 2632
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirst:Z

    .line 2633
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onFullScreenChanged(Z)V

    .line 2634
    if-eqz v5, :cond_2

    .line 2635
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessage(I)Z

    .line 2641
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFullScreenCamera:Z

    move/from16 v19, v0

    if-eqz v19, :cond_5

    .line 2642
    const/4 v12, 0x0

    .line 2661
    .local v12, "neighbors":I
    :goto_1
    move v8, v12

    .local v8, "i":I
    :goto_2
    neg-int v0, v12

    move/from16 v19, v0

    move/from16 v0, v19

    if-lt v8, v0, :cond_b

    .line 2662
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v14

    .line 2664
    .local v14, "r":Landroid/graphics/Rect;
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseLargeThumbnail()Z

    move-result v19

    if-nez v19, :cond_3

    sget-boolean v19, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v19, :cond_a

    .line 2665
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v14, v8}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;I)V

    .line 2661
    :goto_3
    add-int/lit8 v8, v8, -0x1

    goto :goto_2

    .line 2628
    .end local v5    # "full":Z
    .end local v8    # "i":I
    .end local v12    # "neighbors":I
    .end local v14    # "r":Landroid/graphics/Rect;
    :cond_4
    const/4 v5, 0x0

    goto :goto_0

    .line 2646
    .restart local v5    # "full":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/PositionController;->getFilmRatio()F

    move-result v19

    const/16 v20, 0x0

    cmpl-float v19, v19, v20

    if-nez v19, :cond_6

    const/4 v10, 0x1

    .line 2647
    .local v10, "inPageMode":Z
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I

    move/from16 v19, v0

    and-int/lit8 v19, v19, 0x2

    if-eqz v19, :cond_7

    const/4 v9, 0x1

    .line 2648
    .local v9, "inCaptureAnimation":Z
    :goto_5
    if-eqz v10, :cond_9

    if-nez v9, :cond_9

    .line 2650
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    move/from16 v19, v0

    if-nez v19, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/PositionController;->isCenter()Z

    move-result v19

    if-eqz v19, :cond_8

    .line 2651
    const/4 v12, 0x0

    .restart local v12    # "neighbors":I
    goto :goto_1

    .line 2646
    .end local v9    # "inCaptureAnimation":Z
    .end local v10    # "inPageMode":Z
    .end local v12    # "neighbors":I
    :cond_6
    const/4 v10, 0x0

    goto :goto_4

    .line 2647
    .restart local v10    # "inPageMode":Z
    :cond_7
    const/4 v9, 0x0

    goto :goto_5

    .line 2653
    .restart local v9    # "inCaptureAnimation":Z
    :cond_8
    const/4 v12, 0x1

    .restart local v12    # "neighbors":I
    goto :goto_1

    .line 2656
    .end local v12    # "neighbors":I
    :cond_9
    const/4 v12, 0x3

    .restart local v12    # "neighbors":I
    goto :goto_1

    .line 2667
    .end local v9    # "inCaptureAnimation":Z
    .end local v10    # "inPageMode":Z
    .restart local v8    # "i":I
    .restart local v14    # "r":Landroid/graphics/Rect;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPictures:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v14}, Lcom/sec/android/gallery3d/ui/PhotoView$Picture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V

    goto :goto_3

    .line 2672
    .end local v14    # "r":Landroid/graphics/Rect;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->renderChild(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 2673
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBar:Lcom/sec/android/gallery3d/ui/UndoBarView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->renderChild(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 2675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->renderChild(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 2676
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_c

    .line 2677
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->renderChild(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 2682
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/PositionController;->advanceAnimation()Z

    move-result v4

    .line 2683
    .local v4, "animFlag":Z
    if-nez v4, :cond_d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmstripJump:Z

    move/from16 v19, v0

    if-eqz v19, :cond_d

    .line 2684
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmstripJump:Z

    .line 2685
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setImageChanging(Z)V

    .line 2687
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->checkFocusSwitching()V

    .line 2689
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsPaused:Z

    move/from16 v19, v0

    if-eqz v19, :cond_f

    .line 2785
    :cond_e
    :goto_6
    return-void

    .line 2701
    :cond_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    .line 2702
    .local v13, "positionController":Lcom/sec/android/gallery3d/ui/PositionController;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAgifMode:Z

    move/from16 v19, v0

    if-eqz v19, :cond_10

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/ui/PositionController;->isInScale()Z

    move-result v19

    if-nez v19, :cond_10

    .line 2703
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-object/from16 v19, v0

    check-cast v19, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface/range {v19 .. v19}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->onAgifPlayRequests()V

    .line 2704
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    .line 2735
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v16

    .line 2736
    .local v16, "w":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v6

    .line 2737
    .local v6, "h":I
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->centerX()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v17

    .line 2738
    .local v17, "x":I
    div-int/lit8 v18, v6, 0x2

    .line 2739
    .local v18, "y":I
    move/from16 v0, v16

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v19

    move/from16 v0, v19

    int-to-double v0, v0

    move-wide/from16 v20, v0

    const-wide/high16 v22, 0x4004000000000000L    # 2.5

    div-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-int v15, v0

    .line 2741
    .local v15, "s":I
    sget-boolean v19, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v19, :cond_11

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v19

    if-eqz v19, :cond_12

    .line 2742
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v20

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->min(II)I

    move-result v19

    move/from16 v0, v19

    int-to-double v0, v0

    move-wide/from16 v20, v0

    const-wide/high16 v22, 0x4016000000000000L    # 5.5

    div-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-int v15, v0

    .line 2745
    :cond_12
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingProgressState:I

    move/from16 v19, v0

    sget v20, Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_RUNNING:I

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_13

    .line 2746
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingText:Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;

    .line 2747
    .local v11, "m":Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getLoadingSpinner()Lcom/sec/android/gallery3d/ui/ProgressSpinner;

    move-result-object v14

    .line 2748
    .local v14, "r":Lcom/sec/android/gallery3d/ui/ProgressSpinner;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMEnabled()Z

    move-result v19

    if-eqz v19, :cond_14

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/ui/ProgressSpinner;->getHeight()I

    move-result v7

    .line 2749
    .local v7, "height":I
    :goto_7
    invoke-virtual {v14}, Lcom/sec/android/gallery3d/ui/ProgressSpinner;->getWidth()I

    move-result v19

    div-int/lit8 v19, v19, 0x2

    sub-int v19, v17, v19

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/ui/ProgressSpinner;->getHeight()I

    move-result v20

    div-int/lit8 v20, v20, 0x2

    sub-int v20, v18, v20

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v14, v0, v1, v2}, Lcom/sec/android/gallery3d/ui/ProgressSpinner;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 2750
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;->getWidth()I

    move-result v19

    div-int/lit8 v19, v19, 0x2

    sub-int v19, v17, v19

    div-int/lit8 v20, v7, 0x2

    add-int v20, v20, v18

    add-int/lit8 v20, v20, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v11, v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 2751
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    .line 2754
    .end local v7    # "height":I
    .end local v11    # "m":Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;
    .end local v14    # "r":Lcom/sec/android/gallery3d/ui/ProgressSpinner;
    :cond_13
    if-eqz v4, :cond_15

    .line 2755
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    .line 2756
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->checkAnimation(Z)V

    .line 2761
    :goto_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    move/from16 v19, v0

    if-eqz v19, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingProgressState:I

    move/from16 v19, v0

    sget v20, Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_RUNNING:I

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_e

    .line 2778
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/ui/PositionController;->getUseViewSize()Z

    move-result v19

    if-nez v19, :cond_e

    .line 2781
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-object/from16 v19, v0

    if-eqz v19, :cond_e

    .line 2782
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-object/from16 v19, v0

    if-eqz v19, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->getSelectonModeBarHeight()I

    move-result v19

    :goto_9
    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->rendererPhotoViewIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    goto/16 :goto_6

    .restart local v11    # "m":Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;
    .restart local v14    # "r":Lcom/sec/android/gallery3d/ui/ProgressSpinner;
    :cond_14
    move v7, v15

    .line 2748
    goto/16 :goto_7

    .line 2758
    .end local v11    # "m":Lcom/sec/android/gallery3d/glrenderer/MultiLineTexture;
    .end local v14    # "r":Lcom/sec/android/gallery3d/ui/ProgressSpinner;
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->checkAnimation(Z)V

    goto :goto_8

    .line 2782
    :cond_16
    const/16 v19, 0x0

    goto :goto_9
.end method

.method public resetAgifMode(ZLcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 3
    .param p1, "gif"    # Z
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v2, 0x0

    .line 4236
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4237
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAgifMode:Z

    .line 4241
    :goto_0
    return-void

    .line 4239
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAgifMode:Z

    goto :goto_0
.end method

.method public resetToFirstPicture()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2550
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->moveTo(I)V

    .line 2551
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->setFilmMode(Z)V

    .line 2552
    return-void
.end method

.method public resetToFullView()V
    .locals 1

    .prologue
    .line 4147
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->resetToFullView()V

    .line 4148
    return-void
.end method

.method public resetToFullView(Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;

    .prologue
    .line 4142
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->setAnimationListener(Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;)V

    .line 4143
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->resetToFullView()V

    .line 4144
    return-void
.end method

.method public resetToFullViewNoAnimation()V
    .locals 1

    .prologue
    .line 4151
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->resetToFullViewNoAnimation()V

    .line 4152
    return-void
.end method

.method public resetTransitionMode()V
    .locals 1

    .prologue
    .line 4218
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I

    .line 4219
    return-void
.end method

.method public resume()V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 2505
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsPaused:Z

    .line 2506
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    if-nez v1, :cond_0

    .line 2507
    new-instance v1, Lcom/sec/android/gallery3d/util/MotionDetector;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/util/MotionDetector;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/GalleryMotion;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    .line 2508
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    sget-object v2, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_NONE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/MotionDetector;->setState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)V

    .line 2509
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/MotionDetector;->setDefaultMotionCenterPosition(FF)V

    .line 2510
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    new-instance v2, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/ui/PhotoView$1;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/MotionDetector;->registerMotionListener(Lcom/samsung/android/motion/MRListener;)V

    .line 2512
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    if-eqz v1, :cond_1

    .line 2513
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryMotion;->resume()V

    .line 2516
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->initAlphaBlendingAnimation()V

    .line 2517
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setImageChanging(Z)V

    .line 2520
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/TileImageView;->prepareTextures()V

    .line 2521
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->skipToFinalPosition()V

    .line 2523
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirstShowDialog:Z

    .line 2525
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v1, :cond_2

    .line 2526
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v1, :cond_2

    .line 2527
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2528
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string/jumbo v1, "setting_face_tag"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2529
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->addContactPopupView()V

    .line 2530
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->resume()V

    .line 2536
    .end local v0    # "prefs":Landroid/content/SharedPreferences;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPanning(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2537
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->startArrayAnimationHelp()V

    .line 2538
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->startTapAnimationHelp()V

    .line 2546
    :cond_3
    :goto_0
    return-void

    .line 2539
    :cond_4
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpTilt(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2540
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->startTapAnimationHelp()V

    goto :goto_0

    .line 2541
    :cond_5
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPeek(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2542
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->startArrayAnimationHelp()V

    .line 2543
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->startTapAnimationHelp()V

    goto :goto_0
.end method

.method public resumeFaceIndicatorView()V
    .locals 3

    .prologue
    .line 5214
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v1, :cond_0

    .line 5215
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v1, :cond_0

    .line 5216
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 5217
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string/jumbo v1, "setting_face_tag"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5218
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->addContactPopupView()V

    .line 5219
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->resume()V

    .line 5223
    .end local v0    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method public setAboveFilmStripHeight(I)V
    .locals 1
    .param p1, "aboveFilmStripHeight"    # I

    .prologue
    .line 4864
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v0, :cond_0

    .line 4865
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4866
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setAboveFilmStripHeight(I)V

    .line 4869
    :cond_0
    return-void
.end method

.method public setAssignedName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 4407
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4408
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setAssignedName(Ljava/lang/String;Ljava/lang/String;)V

    .line 4410
    :cond_0
    return-void
.end method

.method public setCameraRelativeFrame(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "frame"    # Landroid/graphics/Rect;

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCameraRelativeFrame:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1038
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->updateCameraRect()V

    .line 1043
    return-void
.end method

.method public setCurContactLookupKey(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 4487
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4488
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setCurContactLookupKey(Ljava/lang/String;)V

    .line 4490
    :cond_0
    return-void
.end method

.method public setCurMediaSetKey(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 4401
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4402
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setCurMediaSetKey(Ljava/lang/String;Ljava/lang/String;)V

    .line 4404
    :cond_0
    return-void
.end method

.method public setCurrentFaceId(I)V
    .locals 1
    .param p1, "curFaceId"    # I

    .prologue
    .line 4368
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4369
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setCurrentFaceId(I)V

    .line 4371
    :cond_0
    return-void
.end method

.method public setCurrentPhoto()V
    .locals 0

    .prologue
    .line 4284
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->updateTakenTimeLable()V

    .line 4285
    return-void
.end method

.method public setCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "photo"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 4323
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 4324
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidateFaceIndicatorView(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 4327
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v0, :cond_0

    .line 4328
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    if-eqz v0, :cond_0

    .line 4329
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setPositionController(Lcom/sec/android/gallery3d/ui/PositionController;)V

    .line 4334
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    if-eqz v0, :cond_1

    .line 4335
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->setPositionController(Lcom/sec/android/gallery3d/ui/PositionController;)V

    .line 4336
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSpenOcrView:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->setPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 4338
    :cond_1
    return-void
.end method

.method public setCurrentPhotoForCA(Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    .locals 1
    .param p1, "photo"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "isShowBar"    # Z

    .prologue
    .line 4342
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-nez v0, :cond_1

    .line 4351
    :cond_0
    :goto_0
    return-void

    .line 4345
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v0, :cond_2

    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-nez v0, :cond_2

    instance-of v0, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v0, :cond_0

    .line 4346
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    if-eqz v0, :cond_0

    .line 4347
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->isOffContextualTag()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4348
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setContextualTagView(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    goto :goto_0
.end method

.method public setDmrEnabled()V
    .locals 2

    .prologue
    .line 5063
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 5064
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsDmrEnabled:Z

    .line 5065
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    .line 5066
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessage(I)Z

    .line 5068
    :cond_0
    return-void
.end method

.method public setFaceFeature(Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;)V
    .locals 1
    .param p1, "mFaceTagFeature"    # Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    .prologue
    .line 4419
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4420
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setFaceFeature(Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;)V

    .line 4421
    :cond_0
    return-void
.end method

.method public setFaceIndicatorGenericFocusIndex(IZ)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "update"    # Z

    .prologue
    .line 4902
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setGenericFocusIndex(IZ)V

    .line 4903
    return-void
.end method

.method public setFaceTagToggle(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 4355
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4356
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setFaceTagToggle(Z)V

    .line 4358
    :cond_0
    return-void
.end method

.method public setFilmMode(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2437
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    if-ne v0, p1, :cond_0

    .line 2445
    :goto_0
    return-void

    .line 2438
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    .line 2439
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->setFilmMode(Z)V

    .line 2440
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->setNeedFullImage(Z)V

    .line 2441
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z

    if-eqz v3, :cond_2

    :goto_2
    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->setFocusHintDirection(I)V

    .line 2443
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->updateActionBar()V

    .line 2444
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onFilmModeChanged(Z)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2440
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2441
    goto :goto_2
.end method

.method public setHdmi3D(Lcom/sec/android/gallery3d/ui/Hdmi3D;)V
    .locals 1
    .param p1, "hdmi3D"    # Lcom/sec/android/gallery3d/ui/Hdmi3D;

    .prologue
    .line 3318
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    if-eqz v0, :cond_0

    .line 3319
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->stopPresentation()V

    .line 3320
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/HdmiUtils;->setExternal3Dmode(Z)V

    .line 3321
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmiListener:Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/HdmiUtils;->removeHdmiConnectListener(Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;)V

    .line 3323
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    .line 3324
    return-void
.end method

.method public setHdmiPresentation(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 3383
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/HdmiUtils;->setExternal3Dmode(Z)V

    .line 3384
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    if-nez v1, :cond_1

    .line 3395
    :cond_0
    :goto_0
    return-void

    .line 3387
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->isRunning()Z

    move-result v0

    .line 3388
    .local v0, "isHdmiRunning":Z
    if-eqz p1, :cond_2

    if-nez v0, :cond_2

    .line 3389
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->startPresentation()V

    .line 3390
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmiListener:Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/HdmiUtils;->addHdmiConnectListener(Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;)V

    goto :goto_0

    .line 3391
    :cond_2
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    .line 3392
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->stopPresentation()V

    .line 3393
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHdmiListener:Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/HdmiUtils;->removeHdmiConnectListener(Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;)V

    goto :goto_0
.end method

.method public setHelpMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 4976
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpMode:I

    .line 4978
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 4979
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHelpViewingMode:I

    .line 4980
    :cond_0
    return-void
.end method

.method public setIsManualFDResumed(Z)V
    .locals 1
    .param p1, "isManualFD"    # Z

    .prologue
    .line 5204
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 5205
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setIsManualFDResumed(Z)V

    .line 5207
    :cond_0
    return-void
.end method

.method public setIsRotated(Z)V
    .locals 0
    .param p1, "rotated"    # Z

    .prologue
    .line 5089
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsRotated:Z

    .line 5090
    return-void
.end method

.method public setIsSelectionMode(Z)V
    .locals 0
    .param p1, "isSelectionMode"    # Z

    .prologue
    .line 528
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsSelectionMode:Z

    .line 529
    return-void
.end method

.method public setListener(Lcom/sec/android/gallery3d/ui/PhotoView$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    .prologue
    .line 3196
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    .line 3197
    return-void
.end method

.method public setLoadingFailed()V
    .locals 5

    .prologue
    const/16 v4, 0x67

    .line 3374
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 3375
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    if-eqz v0, :cond_0

    .line 3376
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->removeMessages(I)V

    .line 3377
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v4, v2, v3}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 3380
    :cond_0
    return-void
.end method

.method public setManualFace(Landroid/net/Uri;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 4374
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-nez v0, :cond_0

    .line 4375
    const/4 v0, -0x1

    .line 4376
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setManualFace(Landroid/net/Uri;)I

    move-result v0

    goto :goto_0
.end method

.method public setModel(Lcom/sec/android/gallery3d/ui/PhotoView$Model;)V
    .locals 2
    .param p1, "model"    # Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    .prologue
    .line 734
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    .line 735
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/TileImageView;->setModel(Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;)V

    .line 736
    return-void
.end method

.method public setOffContextualTag(Z)V
    .locals 0
    .param p1, "isOff"    # Z

    .prologue
    .line 5097
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOffContextualTag:Z

    .line 5098
    return-void
.end method

.method public setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/view/View$OnGenericMotionListener;

    .prologue
    .line 4898
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    .line 4899
    return-void
.end method

.method public setOpenAnimationRect(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 3028
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->setOpenAnimationRect(Landroid/graphics/Rect;)V

    .line 3029
    return-void
.end method

.method public setPhotoViewIcon(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)V
    .locals 0
    .param p1, "photoViewIcon"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    .prologue
    .line 5210
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    .line 5211
    return-void
.end method

.method public setRotateAnim(Lcom/sec/android/gallery3d/anim/FloatAnimation;)V
    .locals 0
    .param p1, "rotateAnim"    # Lcom/sec/android/gallery3d/anim/FloatAnimation;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    .line 541
    return-void
.end method

.method public setScalePrepared(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 4872
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    if-eqz v0, :cond_0

    .line 4873
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->setScalePrepared(Z)V

    .line 4875
    :cond_0
    return-void
.end method

.method public setScreenSize(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 4726
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4727
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setScreenSize(II)V

    .line 4729
    :cond_0
    return-void
.end method

.method public setShowBarState(Z)V
    .locals 2
    .param p1, "mShowBars"    # Z

    .prologue
    .line 4777
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsShowBars:Z

    .line 4778
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4779
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mIsShowBars:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setStripStatus(Z)V

    .line 4782
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    if-eqz v0, :cond_1

    .line 4783
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setShowBarState(Z)V

    .line 4785
    :cond_1
    return-void
.end method

.method public setShowContextualTag(Z)V
    .locals 1
    .param p1, "mShowBars"    # Z

    .prologue
    .line 4788
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    if-eqz v0, :cond_0

    .line 4789
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContextualTagView:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setShowBarState(Z)V

    .line 4791
    :cond_0
    return-void
.end method

.method public setSideMirrorViewOnShowListener(Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;)V
    .locals 0
    .param p1, "sideMirrorViewOnShowListener"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;

    .prologue
    .line 3899
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mSideMirrorViewOnShowListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;

    .line 3900
    return-void
.end method

.method public setSwipingEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 2422
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGestureListener:Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView$MyGestureListener;->setSwipingEnabled(Z)V

    .line 2423
    return-void
.end method

.method public setTransitionMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 4210
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I

    .line 4211
    return-void
.end method

.method public setUnnamedCandidates(I[I)V
    .locals 1
    .param p1, "faceId"    # I
    .param p2, "ids"    # [I

    .prologue
    .line 4467
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4468
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setUnnamedCandidates(I[I)V

    .line 4470
    :cond_0
    return-void
.end method

.method public setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .prologue
    .line 4831
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 4832
    return-void
.end method

.method public setWantPictureCenterCallbacks(Z)V
    .locals 0
    .param p1, "wanted"    # Z

    .prologue
    .line 853
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mWantPictureCenterCallbacks:Z

    .line 854
    return-void
.end method

.method public showAdaptDisplayDialog(Z)V
    .locals 4
    .param p1, "isImage"    # Z

    .prologue
    .line 5041
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAdaptDisplayDialog:Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;

    if-nez v0, :cond_0

    .line 5042
    new-instance v0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAdaptDisplayDialog:Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;

    .line 5045
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirstShowDialog:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->checkGuideDialogState()Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->ADAPT_DISPLAY_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    if-ne v0, v1, :cond_1

    .line 5046
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mAdaptDisplayDialog:Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->showDialog()V

    .line 5048
    :cond_1
    return-void
.end method

.method public showContactPopupView()V
    .locals 1

    .prologue
    .line 4443
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4444
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->showContactPopupView()V

    .line 4446
    :cond_0
    return-void
.end method

.method public showFaceIndicatorView()V
    .locals 1

    .prologue
    .line 4455
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    if-eqz v0, :cond_0

    .line 4456
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->show()V

    .line 4458
    :cond_0
    return-void
.end method

.method public showMotionDialog(Z)V
    .locals 2
    .param p1, "isImage"    # Z

    .prologue
    .line 4288
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirstShowDialog:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->checkGuideDialogState()Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;->MOTION_DIALOG:Lcom/sec/android/gallery3d/ui/PhotoView$GuideDialogState;

    if-ne v0, v1, :cond_0

    .line 4291
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->startMotionTutorialDialog()V

    .line 4293
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mFirstShowDialog:Z

    .line 4294
    return-void
.end method

.method public showUndoBar(Z)V
    .locals 6
    .param p1, "deleteLast"    # Z

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x1

    .line 2570
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->removeMessages(I)V

    .line 2571
    iput v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBarState:I

    .line 2572
    if-eqz p1, :cond_0

    .line 2573
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBarState:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBarState:I

    .line 2574
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mUndoBar:Lcom/sec/android/gallery3d/ui/UndoBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/UndoBarView;->animateVisibility(I)V

    .line 2575
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v5, v2, v3}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 2576
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    invoke-interface {v0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onUndoBarVisibilityChanged(Z)V

    .line 2577
    :cond_1
    return-void
.end method

.method public startMFDByMenu(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 4695
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->startManualFD(II)V

    .line 4696
    return-void
.end method

.method public startMotionTutorialDialog()V
    .locals 4

    .prologue
    .line 4245
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-nez v0, :cond_1

    .line 4260
    :cond_0
    :goto_0
    return-void

    .line 4248
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4252
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionDialogOff()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4255
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionEngineUse()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionTiltUse()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4256
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->motionDialogInitialOn()V

    goto :goto_0

    .line 4258
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/app/GalleryMotion;->motionDialogInitialOff(Lcom/sec/android/gallery3d/util/MotionDetector;II)V

    goto :goto_0
.end method

.method public startScale(F)V
    .locals 2
    .param p1, "scale"    # F

    .prologue
    const/4 v1, 0x0

    .line 3782
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMyScaleListener:Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;

    if-nez v0, :cond_0

    .line 3787
    :goto_0
    return-void

    .line 3784
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isInScale()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3785
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMyScaleListener:Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    .line 3786
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mMyScaleListener:Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->onScale(Landroid/view/ScaleGestureDetector;F)Z

    goto :goto_0
.end method

.method public startSlideInAnimation(I)V
    .locals 3
    .param p1, "direction"    # I

    .prologue
    .line 4155
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    .line 4156
    .local v0, "a":Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->stopAnimation()V

    .line 4157
    packed-switch p1, :pswitch_data_0

    .line 4168
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 4160
    :pswitch_0
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I

    .line 4164
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->startHorizontalSlide()V

    .line 4170
    return-void

    .line 4157
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public stopScrolling()V
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->stopScrolling()V

    .line 731
    return-void
.end method

.method public switchToImage(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 2976
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->moveTo(I)V

    .line 2977
    return-void
.end method

.method public switchWithCaptureAnimation(I)Z
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 3036
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    .line 3037
    .local v0, "root":Lcom/sec/android/gallery3d/ui/GLRoot;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 3042
    :goto_0
    return v1

    .line 3038
    :cond_0
    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->lockRenderThread()V

    .line 3040
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchWithCaptureAnimationLocked(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 3042
    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    throw v1
.end method

.method public toggleScale()V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v5, 0x3f000000    # 0.5f

    const v4, 0x4013d70a    # 2.31f

    .line 3762
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    .line 3764
    .local v0, "controller":Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v1

    .line 3765
    .local v1, "scale":F
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3768
    cmpg-float v3, v1, v5

    if-gez v3, :cond_0

    .line 3769
    mul-float v3, v1, v4

    invoke-static {v5, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 3775
    .local v2, "toScale":F
    :goto_0
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayWidth:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplayHeight:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->zoomIn(FFF)V

    .line 3779
    .end local v2    # "toScale":F
    :goto_1
    return-void

    .line 3770
    :cond_0
    cmpg-float v3, v1, v6

    if-gez v3, :cond_1

    .line 3771
    mul-float v3, v1, v4

    invoke-static {v6, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .restart local v2    # "toScale":F
    goto :goto_0

    .line 3773
    .end local v2    # "toScale":F
    :cond_1
    const v3, 0x3fa66666    # 1.3f

    mul-float/2addr v4, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .restart local v2    # "toScale":F
    goto :goto_0

    .line 3777
    .end local v2    # "toScale":F
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->resetToFullView()V

    goto :goto_1
.end method
