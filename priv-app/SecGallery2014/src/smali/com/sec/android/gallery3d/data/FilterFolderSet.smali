.class public Lcom/sec/android/gallery3d/data/FilterFolderSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "FilterFolderSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "FilterSet"


# instance fields
.field private final mAlbums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field

.field private final mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private final mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mFiterFolder:Ljava/lang/String;

.field private final mPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mSupportShare:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p4, "folder"    # Ljava/lang/String;

    .prologue
    .line 42
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mPaths:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mAlbums:Ljava/util/ArrayList;

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mSupportShare:Z

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mFiterFolder:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 44
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 45
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 46
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mFiterFolder:Ljava/lang/String;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/FilterFolderSet;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/FilterFolderSet;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/FilterFolderSet;->isFilteredItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/FilterFolderSet;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/FilterFolderSet;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mPaths:Ljava/util/ArrayList;

    return-object v0
.end method

.method private buildCursorToPath(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p3, "isImage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 231
    .local p2, "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p1, :cond_2

    .line 232
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 233
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 234
    .local v1, "id":I
    if-eqz p3, :cond_1

    sget-object v2, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 235
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    :goto_1
    const/4 v3, 0x5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, "filePath":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/FilterFolderSet;->isFilteredItem(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 237
    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 234
    .end local v0    # "filePath":Ljava/lang/String;
    .end local v2    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    sget-object v2, Lcom/sec/android/gallery3d/data/LocalVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_1

    .line 241
    .end local v1    # "id":I
    :cond_2
    return-void
.end method

.method private isFilteredItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 105
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/FilterFolderSet;->isFilteredItem(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isFilteredItem(Ljava/lang/String;)Z
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 109
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private updateData()V
    .locals 10

    .prologue
    .line 114
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 115
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/ComboAlbumSet;

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    if-eqz v8, :cond_2

    .line 116
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/FilterFolderSet;->updateDataForComboAndLocalSet()V

    .line 161
    :cond_1
    return-void

    .line 120
    :cond_2
    const-string v0, "/filter/folder/1"

    .line 122
    .local v0, "basePath":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v5

    .local v5, "n":I
    :goto_0
    if-ge v4, v5, :cond_5

    .line 123
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8, v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    .line 124
    .local v6, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/{"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "}"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 125
    .local v2, "filteredPath":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "##"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mFiterFolder:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 127
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v8, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 128
    .local v3, "filteredSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v3, :cond_4

    .line 129
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 130
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v8

    if-gtz v8, :cond_3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v8

    if-lez v8, :cond_4

    .line 131
    :cond_3
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 137
    .end local v2    # "filteredPath":Ljava/lang/String;
    .end local v3    # "filteredSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v6    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_5
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 138
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v7

    .line 139
    .local v7, "total":I
    if-lez v7, :cond_1

    .line 142
    new-array v1, v7, [Lcom/sec/android/gallery3d/data/Path;

    .line 144
    .local v1, "buf":[Lcom/sec/android/gallery3d/data/Path;
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    new-instance v9, Lcom/sec/android/gallery3d/data/FilterFolderSet$1;

    invoke-direct {v9, p0, v7, v1}, Lcom/sec/android/gallery3d/data/FilterFolderSet$1;-><init>(Lcom/sec/android/gallery3d/data/FilterFolderSet;I[Lcom/sec/android/gallery3d/data/Path;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    .line 156
    const/4 v4, 0x0

    :goto_1
    if-ge v4, v7, :cond_1

    .line 157
    aget-object v8, v1, v4

    if-eqz v8, :cond_6

    .line 158
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mPaths:Ljava/util/ArrayList;

    aget-object v9, v1, v4

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method private updateDataForComboAndLocalSet()V
    .locals 11

    .prologue
    .line 164
    const-string v0, "/filter/folder/1"

    .line 165
    .local v0, "basePath":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v5

    .local v5, "n":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 166
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    .line 167
    .local v6, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/{"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "}"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 168
    .local v1, "filteredPath":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "##"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mFiterFolder:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 170
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v9, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 171
    .local v2, "filteredSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_1

    .line 172
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 173
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v9

    if-gtz v9, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v9

    if-lez v9, :cond_1

    .line 174
    :cond_0
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 179
    .end local v1    # "filteredPath":Ljava/lang/String;
    .end local v2    # "filteredSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v6    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 180
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v9, v9, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    if-eqz v9, :cond_4

    .line 181
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v10, 0x0

    invoke-direct {p0, v9, v10}, Lcom/sec/android/gallery3d/data/FilterFolderSet;->updateMediaItemData(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 191
    :cond_3
    return-void

    .line 184
    :cond_4
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v8

    .line 185
    .local v8, "total":I
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v9, Lcom/sec/android/gallery3d/data/ComboAlbumSet;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->getMediaSets()[Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    .line 186
    .local v7, "sets":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v3, 0x0

    :goto_1
    array-length v9, v7

    if-ge v3, v9, :cond_3

    .line 187
    aget-object v4, v7, v3

    .line 188
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-direct {p0, v4, v8}, Lcom/sec/android/gallery3d/data/FilterFolderSet;->updateMediaItemData(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 186
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private updateMediaItemData(Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 5
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "total"    # I

    .prologue
    .line 194
    if-nez p1, :cond_0

    .line 195
    const-string v3, "FilterSet"

    const-string v4, "mediaSet is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    .end local p1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    return-void

    .line 199
    .restart local p1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    instance-of v3, p1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    if-eqz v3, :cond_2

    .line 200
    const/4 v1, 0x0

    .line 201
    .local v1, "imageCursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 203
    .local v2, "videoCursor":Landroid/database/Cursor;
    :try_start_0
    check-cast p1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    .end local p1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v3, 0x6

    invoke-virtual {p1, v3}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getTotalMediaItems(I)[Landroid/database/Cursor;

    move-result-object v0

    .line 204
    .local v0, "cursors":[Landroid/database/Cursor;
    if-eqz v0, :cond_1

    array-length v3, v0

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 205
    const/4 v3, 0x0

    aget-object v1, v0, v3

    .line 206
    const/4 v3, 0x1

    aget-object v2, v0, v3

    .line 207
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mPaths:Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {p0, v1, v3, v4}, Lcom/sec/android/gallery3d/data/FilterFolderSet;->buildCursorToPath(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V

    .line 208
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mPaths:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/gallery3d/data/FilterFolderSet;->buildCursorToPath(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    :cond_1
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 212
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 211
    .end local v0    # "cursors":[Landroid/database/Cursor;
    :catchall_0
    move-exception v3

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 212
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3

    .line 215
    .end local v1    # "imageCursor":Landroid/database/Cursor;
    .end local v2    # "videoCursor":Landroid/database/Cursor;
    .restart local p1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    new-instance v3, Lcom/sec/android/gallery3d/data/FilterFolderSet$2;

    invoke-direct {v3, p0, p2}, Lcom/sec/android/gallery3d/data/FilterFolderSet$2;-><init>(Lcom/sec/android/gallery3d/data/FilterFolderSet;I)V

    invoke-virtual {p1, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    goto :goto_0
.end method


# virtual methods
.method public delete()V
    .locals 4

    .prologue
    .line 253
    new-instance v0, Lcom/sec/android/gallery3d/data/FilterFolderSet$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/FilterFolderSet$3;-><init>(Lcom/sec/android/gallery3d/data/FilterFolderSet;)V

    .line 262
    .local v0, "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mPaths:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 263
    return-void
.end method

.method public getBucketId()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v0, :cond_0

    .line 56
    const/4 v0, -0x1

    .line 58
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 8
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mPaths:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-static {v3, p1, p2, v4}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItemFromPath(Ljava/util/ArrayList;IILcom/sec/android/gallery3d/data/DataManager;)Ljava/util/ArrayList;

    move-result-object v2

    .line 80
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 81
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v4

    const-wide/16 v6, 0x4

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 83
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mSupportShare:Z

    .line 87
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    return-object v2
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getSupportedOperations()J
    .locals 4

    .prologue
    .line 245
    const-wide/32 v0, 0x20000005

    .line 246
    .local v0, "operation":J
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mSupportShare:Z

    if-nez v2, :cond_0

    .line 247
    const-wide/16 v2, -0x5

    and-long/2addr v0, v2

    .line 248
    :cond_0
    return-wide v0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/FilterFolderSet;->notifyContentChanged()V

    .line 102
    return-void
.end method

.method public reload()J
    .locals 4

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mDataVersion:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 93
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/FilterFolderSet;->updateData()V

    .line 94
    invoke-static {}, Lcom/sec/android/gallery3d/data/FilterFolderSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mDataVersion:J

    .line 96
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet;->mDataVersion:J

    return-wide v0
.end method
