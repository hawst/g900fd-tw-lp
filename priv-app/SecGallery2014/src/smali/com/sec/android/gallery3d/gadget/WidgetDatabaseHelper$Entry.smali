.class public Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
.super Ljava/lang/Object;
.source "WidgetDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation


# instance fields
.field public albumPath:Ljava/lang/String;

.field public frameType:I

.field public imageData:[B

.field public imageUri:Ljava/lang/String;

.field public relativePath:Ljava/lang/String;

.field public showInterval:I

.field public type:I

.field public widgetId:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(ILandroid/database/Cursor;)V
    .locals 6
    .param p1, "id"    # I
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->widgetId:I

    .line 130
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 131
    iget v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    if-nez v1, :cond_1

    .line 132
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->imageUri:Ljava/lang/String;

    .line 133
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->imageData:[B

    .line 140
    :cond_0
    :goto_0
    const/4 v1, 0x4

    :try_start_0
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    .line 141
    const/4 v1, 0x5

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->showInterval:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    :goto_1
    return-void

    .line 134
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    if-ne v1, v5, :cond_0

    .line 135
    :cond_2
    const/4 v1, 0x3

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PhotoDatabaseHelper"

    const-string v2, "no previous frame type. single frame assigned"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 144
    iput v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    .line 145
    iput v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->showInterval:I

    goto :goto_1
.end method

.method synthetic constructor <init>(ILandroid/database/Cursor;Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Landroid/database/Cursor;
    .param p3, "x2"    # Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$1;

    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;-><init>(ILandroid/database/Cursor;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$1;

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;-><init>()V

    return-void
.end method
