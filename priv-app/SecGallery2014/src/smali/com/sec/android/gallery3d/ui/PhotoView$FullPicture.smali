.class Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;
.super Ljava/lang/Object;
.source "PhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/PhotoView$Picture;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FullPicture"
.end annotation


# instance fields
.field private mIs3DTour:Z

.field private mIsCamera:Z

.field private mIsDeletable:Z

.field private mIsGolfShot:Z

.field private mIsPanorama:Z

.field private mIsStaticCamera:Z

.field private mIsVideo:Z

.field private mLoadingState:I

.field private mRotation:I

.field private mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

.field private mUseLoadingProgress:Z

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PhotoView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 1

    .prologue
    .line 1088
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1098
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mLoadingState:I

    .line 1099
    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Size;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    return-void
.end method

.method private drawTileView(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V
    .locals 24
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 1187
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v8

    .line 1188
    .local v8, "imageScale":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v6

    .line 1189
    .local v6, "viewW":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v7

    .line 1190
    .local v7, "viewH":I
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v4

    .line 1191
    .local v4, "cx":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v5

    .line 1192
    .local v5, "cy":F
    const/high16 v19, 0x3f800000    # 1.0f

    .line 1194
    .local v19, "scale":F
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 1195
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PositionController;->getFilmRatio()F

    move-result v11

    .line 1196
    .local v11, "filmRatio":F
    const/16 v20, 0x0

    .line 1199
    .local v20, "wantsCardEffect":Z
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsDeletable:Z

    if-eqz v3, :cond_5

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v11, v3

    if-nez v3, :cond_5

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    div-int/lit8 v23, v7, 0x2

    move/from16 v0, v23

    if-eq v3, v0, :cond_5

    const/16 v21, 0x1

    .line 1201
    .local v21, "wantsOffsetEffect":Z
    :goto_0
    if-eqz v20, :cond_7

    .line 1203
    move-object/from16 v0, p2

    iget v14, v0, Landroid/graphics/Rect;->left:I

    .line 1204
    .local v14, "left":I
    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    .line 1205
    .local v17, "right":I
    move/from16 v0, v17

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->calculateMoveOutProgress(III)F
    invoke-static {v14, v0, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2600(III)F

    move-result v16

    .line 1206
    .local v16, "progress":F
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v23, 0x3f800000    # 1.0f

    move/from16 v0, v16

    move/from16 v1, v23

    invoke-static {v0, v3, v1}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v16

    .line 1210
    const/4 v3, 0x0

    cmpg-float v3, v16, v3

    if-gez v3, :cond_0

    .line 1211
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move/from16 v0, v16

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getScrollScale(F)F
    invoke-static {v3, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2700(Lcom/sec/android/gallery3d/ui/PhotoView;F)F

    move-result v19

    .line 1212
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move/from16 v0, v16

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getScrollAlpha(F)F
    invoke-static {v3, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2800(Lcom/sec/android/gallery3d/ui/PhotoView;F)F

    move-result v9

    .line 1213
    .local v9, "alpha":F
    const/high16 v3, 0x3f800000    # 1.0f

    move/from16 v0, v19

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->interpolate(FFF)F
    invoke-static {v11, v0, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2900(FFF)F

    move-result v19

    .line 1214
    const/high16 v3, 0x3f800000    # 1.0f

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->interpolate(FFF)F
    invoke-static {v11, v9, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2900(FFF)F

    move-result v9

    .line 1216
    mul-float v8, v8, v19

    .line 1217
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->multiplyAlpha(F)V

    .line 1220
    sub-int v3, v17, v14

    if-gt v3, v6, :cond_6

    .line 1223
    int-to-float v3, v6

    const/high16 v23, 0x40000000    # 2.0f

    div-float v10, v3, v23

    .line 1230
    .local v10, "cxPage":F
    :goto_1
    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->interpolate(FFF)F
    invoke-static {v11, v10, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2900(FFF)F

    move-result v4

    .end local v9    # "alpha":F
    .end local v10    # "cxPage":F
    .end local v14    # "left":I
    .end local v16    # "progress":F
    .end local v17    # "right":I
    :cond_0
    :goto_2
    move-object/from16 v3, p0

    .line 1239
    invoke-direct/range {v3 .. v8}, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->setTileViewPosition(FFIIF)V

    .line 1242
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v3, v0, v1}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->preRender(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z

    move-result v13

    .line 1244
    .local v13, "isActive":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/TileImageView;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->renderChild(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 1246
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v3, v0, v1, v13}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->postRender(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)V

    .line 1250
    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    const/high16 v23, 0x3f000000    # 0.5f

    add-float v23, v23, v5

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-interface {v0, v3, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 1251
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v23

    move/from16 v0, v23

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    mul-float v3, v3, v19

    const/high16 v23, 0x3f000000    # 0.5f

    add-float v3, v3, v23

    float-to-int v0, v3

    move/from16 v18, v0

    .line 1253
    .local v18, "s":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsVideo:Z

    if-nez v3, :cond_1

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIs3DTour:Z

    if-eqz v3, :cond_2

    .line 1254
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v22

    .line 1255
    .local v22, "width":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v12

    .line 1257
    .local v12, "height":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1258
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v22

    invoke-virtual {v3, v0, v1, v2, v12}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->drawVideoPlayIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 1260
    .end local v12    # "height":I
    .end local v22    # "width":I
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mLoadingState:I

    if-nez v3, :cond_8

    .line 1261
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_RUNNING:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3400()I

    move-result v23

    move/from16 v0, v23

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingProgressState:I
    invoke-static {v3, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3302(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 1267
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mUseLoadingProgress:Z

    if-nez v3, :cond_4

    .line 1268
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_NONE:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3500()I

    move-result v23

    move/from16 v0, v23

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingProgressState:I
    invoke-static {v3, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3302(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 1275
    :cond_4
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 1276
    return-void

    .line 1199
    .end local v13    # "isActive":Z
    .end local v18    # "s":I
    .end local v21    # "wantsOffsetEffect":Z
    :cond_5
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 1228
    .restart local v9    # "alpha":F
    .restart local v14    # "left":I
    .restart local v16    # "progress":F
    .restart local v17    # "right":I
    .restart local v21    # "wantsOffsetEffect":Z
    :cond_6
    sub-int v3, v17, v14

    int-to-float v3, v3

    mul-float v3, v3, v19

    const/high16 v23, 0x40000000    # 2.0f

    div-float v10, v3, v23

    .restart local v10    # "cxPage":F
    goto/16 :goto_1

    .line 1232
    .end local v9    # "alpha":F
    .end local v10    # "cxPage":F
    .end local v14    # "left":I
    .end local v16    # "progress":F
    .end local v17    # "right":I
    :cond_7
    if-eqz v21, :cond_0

    .line 1233
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    div-int/lit8 v23, v7, 0x2

    sub-int v3, v3, v23

    int-to-float v3, v3

    int-to-float v0, v7

    move/from16 v23, v0

    div-float v15, v3, v23

    .line 1234
    .local v15, "offset":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getOffsetAlpha(F)F
    invoke-static {v3, v15}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3000(Lcom/sec/android/gallery3d/ui/PhotoView;F)F

    move-result v9

    .line 1235
    .restart local v9    # "alpha":F
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->multiplyAlpha(F)V

    goto/16 :goto_2

    .line 1262
    .end local v9    # "alpha":F
    .end local v15    # "offset":F
    .restart local v13    # "isActive":Z
    .restart local v18    # "s":I
    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mLoadingState:I

    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v3, v0, :cond_9

    .line 1263
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_NONE:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3500()I

    move-result v23

    move/from16 v0, v23

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingProgressState:I
    invoke-static {v3, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3302(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    goto :goto_3

    .line 1264
    :cond_9
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mLoadingState:I

    const/16 v23, 0x2

    move/from16 v0, v23

    if-ne v3, v0, :cond_3

    .line 1265
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_NONE:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3500()I

    move-result v23

    move/from16 v0, v23

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingProgressState:I
    invoke-static {v3, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3302(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    goto :goto_3
.end method

.method private setTileViewPosition(FFIIF)V
    .locals 12
    .param p1, "cx"    # F
    .param p2, "cy"    # F
    .param p3, "viewW"    # I
    .param p4, "viewH"    # I
    .param p5, "scale"    # F

    .prologue
    .line 1281
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageWidth()I

    move-result v4

    .line 1282
    .local v4, "imageW":I
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageHeight()I

    move-result v3

    .line 1283
    .local v3, "imageH":I
    int-to-float v9, v4

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    int-to-float v10, p3

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    sub-float/2addr v10, p1

    div-float v10, v10, p5

    add-float v1, v9, v10

    .line 1284
    .local v1, "centerX":F
    int-to-float v9, v3

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    move/from16 v0, p4

    int-to-float v10, v0

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    sub-float/2addr v10, p2

    div-float v10, v10, p5

    add-float v2, v9, v10

    .line 1286
    .local v2, "centerY":F
    int-to-float v9, v4

    sub-float v5, v9, v1

    .line 1287
    .local v5, "inverseX":F
    int-to-float v9, v3

    sub-float v6, v9, v2

    .line 1289
    .local v6, "inverseY":F
    iget v9, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mRotation:I

    sparse-switch v9, :sswitch_data_0

    .line 1295
    new-instance v9, Ljava/lang/RuntimeException;

    iget v10, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mRotation:I

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 1290
    :sswitch_0
    move v7, v1

    .local v7, "x":F
    move v8, v2

    .line 1297
    .local v8, "y":F
    :goto_0
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;
    invoke-static {v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/TileImageView;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mRotation:I

    move/from16 v0, p5

    invoke-virtual {v9, v7, v8, v0, v10}, Lcom/sec/android/gallery3d/ui/TileImageView;->setPosition(FFFI)Z

    .line 1298
    return-void

    .line 1291
    .end local v7    # "x":F
    .end local v8    # "y":F
    :sswitch_1
    move v7, v2

    .restart local v7    # "x":F
    move v8, v5

    .restart local v8    # "y":F
    goto :goto_0

    .line 1292
    .end local v7    # "x":F
    .end local v8    # "y":F
    :sswitch_2
    move v7, v5

    .restart local v7    # "x":F
    move v8, v6

    .restart local v8    # "y":F
    goto :goto_0

    .line 1293
    .end local v7    # "x":F
    .end local v8    # "y":F
    :sswitch_3
    move v7, v6

    .restart local v7    # "x":F
    move v8, v1

    .restart local v8    # "y":F
    goto :goto_0

    .line 1289
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method private updateSize()V
    .locals 4

    .prologue
    .line 1131
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsPanorama:Z

    if-eqz v2, :cond_0

    .line 1132
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getPanoramaRotation()I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2200(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mRotation:I

    .line 1139
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/TileImageView;

    move-result-object v2

    iget v1, v2, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    .line 1140
    .local v1, "w":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/TileImageView;

    move-result-object v2

    iget v0, v2, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    .line 1141
    .local v0, "h":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mRotation:I

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getRotated(III)I
    invoke-static {v3, v1, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2400(III)I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 1142
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mRotation:I

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getRotated(III)I
    invoke-static {v3, v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2400(III)I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    .line 1143
    return-void

    .line 1133
    .end local v0    # "h":I
    .end local v1    # "w":I
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsCamera:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsStaticCamera:Z

    if-nez v2, :cond_1

    .line 1134
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getCameraRotation()I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2300(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mRotation:I

    goto :goto_0

    .line 1136
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getImageRotation(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mRotation:I

    goto :goto_0
.end method


# virtual methods
.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 1147
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->drawTileView(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V

    .line 1156
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$500(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v0

    and-int/lit8 v0, v0, -0x2

    if-eqz v0, :cond_1

    .line 1162
    :cond_0
    :goto_0
    return-void

    .line 1159
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mWantPictureCenterCallbacks:Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2500(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isCenter()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1160
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsCamera:Z

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onPictureCenter(Z)V

    goto :goto_0
.end method

.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;I)V
    .locals 0
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "r"    # Landroid/graphics/Rect;
    .param p3, "diff"    # I

    .prologue
    .line 1167
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V

    .line 1168
    return-void
.end method

.method public forceSize()V
    .locals 3

    .prologue
    .line 1126
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->updateSize()V

    .line 1127
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->forceImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;)V

    .line 1128
    return-void
.end method

.method public getSize()Lcom/sec/android/gallery3d/ui/PhotoView$Size;
    .locals 1

    .prologue
    .line 1121
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    return-object v0
.end method

.method public isCamera()Z
    .locals 1

    .prologue
    .line 1178
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsCamera:Z

    return v0
.end method

.method public isDeletable()Z
    .locals 1

    .prologue
    .line 1183
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsDeletable:Z

    return v0
.end method

.method public reload()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1104
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/TileImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/TileImageView;->notifyModelInvalidated()V

    .line 1106
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isCamera(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsCamera:Z

    .line 1107
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isPanorama(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsPanorama:Z

    .line 1108
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isStaticCamera(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsStaticCamera:Z

    .line 1109
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isVideo(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsVideo:Z

    .line 1110
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->is3DTour(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIs3DTour:Z

    .line 1111
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isGolfShot(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsGolfShot:Z

    .line 1112
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isDeletable(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mIsDeletable:Z

    .line 1113
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getLoadingState(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mLoadingState:I

    .line 1114
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->useLoadingProgress(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->mUseLoadingProgress:Z

    .line 1115
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getScreenNail(I)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V

    .line 1116
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->updateSize()V

    .line 1117
    return-void
.end method

.method public setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V
    .locals 1
    .param p1, "s"    # Lcom/sec/android/gallery3d/ui/ScreenNail;

    .prologue
    .line 1173
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$FullPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTileView:Lcom/sec/android/gallery3d/ui/TileImageView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/TileImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/TileImageView;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V

    .line 1174
    return-void
.end method
