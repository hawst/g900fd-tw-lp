.class public Lcom/sec/android/gallery3d/data/ImageDiskCache$MetaData;
.super Ljava/lang/Object;
.source "ImageDiskCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/ImageDiskCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MetaData"
.end annotation


# instance fields
.field public isGif:Z

.field public isSound:Z

.field public isTemp:Z


# direct methods
.method public constructor <init>(ZZZ)V
    .locals 0
    .param p1, "issound"    # Z
    .param p2, "isgif"    # Z
    .param p3, "istemp"    # Z

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache$MetaData;->isSound:Z

    .line 166
    iput-boolean p2, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache$MetaData;->isGif:Z

    .line 167
    iput-boolean p3, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache$MetaData;->isTemp:Z

    .line 168
    return-void
.end method
