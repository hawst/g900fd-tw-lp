.class Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;
.super Ljava/lang/Object;
.source "GalleryMultiWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SplitMenuItem"
.end annotation


# instance fields
.field private ci:Landroid/content/pm/ComponentInfo;

.field private mCompName:Landroid/content/ComponentName;

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mLabel:Ljava/lang/String;

.field private switchWindow:Z

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Landroid/content/pm/ComponentInfo;Landroid/content/pm/PackageManager;)V
    .locals 3
    .param p2, "info"    # Landroid/content/pm/ComponentInfo;
    .param p3, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    const/4 v0, 0x0

    .line 342
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mLabel:Ljava/lang/String;

    .line 330
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 331
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mCompName:Landroid/content/ComponentName;

    .line 332
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->ci:Landroid/content/pm/ComponentInfo;

    .line 333
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->switchWindow:Z

    .line 343
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->ci:Landroid/content/pm/ComponentInfo;

    .line 344
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->ci:Landroid/content/pm/ComponentInfo;

    invoke-virtual {v0, p3}, Landroid/content/pm/ComponentInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mLabel:Ljava/lang/String;

    .line 345
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->ci:Landroid/content/pm/ComponentInfo;

    invoke-virtual {v0, p3}, Landroid/content/pm/ComponentInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 346
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->ci:Landroid/content/pm/ComponentInfo;

    iget-object v1, v1, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->ci:Landroid/content/pm/ComponentInfo;

    iget-object v2, v2, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mCompName:Landroid/content/ComponentName;

    .line 347
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Z)V
    .locals 3
    .param p2, "isSwitchWindow"    # Z

    .prologue
    const/4 v0, 0x0

    .line 335
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mLabel:Ljava/lang/String;

    .line 330
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 331
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mCompName:Landroid/content/ComponentName;

    .line 332
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->ci:Landroid/content/pm/ComponentInfo;

    .line 333
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->switchWindow:Z

    .line 336
    iput-boolean p2, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->switchWindow:Z

    .line 337
    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$000(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e025b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mLabel:Ljava/lang/String;

    .line 338
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v0

    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$000(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0203c2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 340
    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;)Landroid/content/pm/ComponentInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->ci:Landroid/content/pm/ComponentInfo;

    return-object v0
.end method


# virtual methods
.method public getCompName()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mCompName:Landroid/content/ComponentName;

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->mLabel:Ljava/lang/String;

    return-object v0
.end method

.method public isSwitchWindow()Z
    .locals 1

    .prologue
    .line 350
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->switchWindow:Z

    return v0
.end method
