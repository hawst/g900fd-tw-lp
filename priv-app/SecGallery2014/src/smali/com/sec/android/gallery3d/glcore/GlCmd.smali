.class public Lcom/sec/android/gallery3d/glcore/GlCmd;
.super Ljava/lang/Object;
.source "GlCmd.java"


# static fields
.field public static CMD_TYPE_LAYER:I

.field public static CMD_TYPE_MESSSAGE:I

.field public static CMD_TYPE_OBJ:I

.field public static CMD_TYPE_SYS:I

.field public static CMD_TYPE_VIEW:I


# instance fields
.field public mCmd:I

.field public mCmdType:I

.field public mExpTime:J

.field public mParm1:I

.field public mParm2:I

.field public mParm3:I

.field public mParmObj:Ljava/lang/Object;

.field public mThis:Ljava/lang/Object;

.field public mValid:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_SYS:I

    .line 5
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_LAYER:I

    .line 6
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_VIEW:I

    .line 7
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_OBJ:I

    .line 8
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_MESSSAGE:I

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 2
    .param p1, "cmd"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParmObj:Ljava/lang/Object;

    .line 15
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mExpTime:J

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mValid:Z

    .line 21
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    .line 22
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm1:I

    .line 23
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm2:I

    .line 24
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm3:I

    .line 25
    return-void
.end method

.method public constructor <init>(IIIIJ)V
    .locals 3
    .param p1, "cmd"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "delay"    # J

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParmObj:Ljava/lang/Object;

    .line 15
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mExpTime:J

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mValid:Z

    .line 28
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    .line 29
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm1:I

    .line 30
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm2:I

    .line 31
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm3:I

    .line 32
    iput-wide p5, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mExpTime:J

    .line 33
    return-void
.end method

.method public constructor <init>(IIIILjava/lang/Object;)V
    .locals 2
    .param p1, "cmd"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParmObj:Ljava/lang/Object;

    .line 15
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mExpTime:J

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mValid:Z

    .line 36
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    .line 37
    iput-object p5, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParmObj:Ljava/lang/Object;

    .line 38
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm1:I

    .line 39
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm2:I

    .line 40
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm3:I

    .line 41
    return-void
.end method

.method public constructor <init>(IIIILjava/lang/Object;J)V
    .locals 2
    .param p1, "cmd"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "obj"    # Ljava/lang/Object;
    .param p6, "delay"    # J

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParmObj:Ljava/lang/Object;

    .line 15
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mExpTime:J

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mValid:Z

    .line 44
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    .line 45
    iput-object p5, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParmObj:Ljava/lang/Object;

    .line 46
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm1:I

    .line 47
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm2:I

    .line 48
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm3:I

    .line 49
    iput-wide p6, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mExpTime:J

    .line 50
    return-void
.end method


# virtual methods
.method public getCmd()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    return v0
.end method
