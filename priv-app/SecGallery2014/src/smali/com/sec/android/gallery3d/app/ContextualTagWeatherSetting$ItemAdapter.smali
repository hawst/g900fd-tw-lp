.class Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ContextualTagWeatherSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mItems:[Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;Landroid/content/Context;[Ljava/lang/String;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "items"    # [Ljava/lang/String;

    .prologue
    .line 381
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    .line 382
    const v0, 0x7f03003f

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 383
    iput-object p3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;->mItems:[Ljava/lang/String;

    .line 384
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 389
    if-nez p2, :cond_1

    .line 390
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 391
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03003f

    invoke-virtual {v1, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 396
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .local v2, "v":Landroid/view/View;
    :goto_0
    const v3, 0x7f0f00ad

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 397
    .local v0, "ctv":Landroid/widget/CheckedTextView;
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;->mItems:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 398
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeather:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->access$200(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 399
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    # setter for: Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListPosition:I
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->access$302(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;I)I

    .line 400
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->access$400(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 407
    :cond_0
    :goto_1
    return-object v0

    .line 393
    .end local v0    # "ctv":Landroid/widget/CheckedTextView;
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    move-object v2, p2

    .restart local v2    # "v":Landroid/view/View;
    goto :goto_0

    .line 402
    .restart local v0    # "ctv":Landroid/widget/CheckedTextView;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeather:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->access$200(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;->mItems:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 403
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->access$400(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, p1, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 404
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    # setter for: Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListPosition:I
    invoke-static {v3, p1}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->access$302(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;I)I

    goto :goto_1
.end method
