.class public Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSource;
.source "NearbySource.java"


# static fields
.field private static final NEARBY_DEVICE:I = 0x1

.field private static final NEARBY_DEVICESET:I = 0x0

.field private static final NEARBY_ITEM:I = 0x2

.field public static final PATH:Ljava/lang/String; = "/nearby"

.field public static final SCHEME:Ljava/lang/String; = "nearby"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

.field private mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

.field private mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

.field private mNearbyDeviceSet:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 30
    const-string v0, "nearby"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/remote/RemoteMediaSource;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 31
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->TAG:Ljava/lang/String;

    const-string v1, "Nearby Source Create!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 34
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/nearby"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 35
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/nearby/*"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/nearby/item/*/*"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 37
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    .line 38
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .line 39
    return-void
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 45
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 65
    :goto_0
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 47
    :pswitch_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->TAG:Ljava/lang/String;

    const-string v2, "Create Media Object : DeviceSet"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    new-instance v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    invoke-direct {v1, p1, v2, v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyDeviceSet:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    .line 49
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyDeviceSet:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    goto :goto_1

    .line 52
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/Device;

    move-result-object v0

    .line 53
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    if-eqz v0, :cond_0

    .line 54
    new-instance v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v1, p1, v2, v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/samsung/android/allshare/Device;)V

    goto :goto_1

    .line 56
    :cond_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->TAG:Ljava/lang/String;

    const-string v2, "can\'t get device"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    return-object v0
.end method

.method public getNearbyDeviceSet()Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;
    .locals 2

    .prologue
    .line 83
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyDeviceSet:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    if-nez v1, :cond_0

    .line 84
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 85
    .local v0, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const-string v1, "/nearby"

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyDeviceSet:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    .line 87
    .end local v0    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyDeviceSet:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    return-object v1
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->pause()V

    .line 71
    return-void
.end method

.method public requestSyncAll()V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->resume()V

    .line 76
    return-void
.end method
