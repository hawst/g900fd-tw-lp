.class Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$3;
.super Ljava/lang/Object;
.source "WifiDisplayAlertDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$3;->this$0:Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 92
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$3;->this$0:Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->access$300(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0f001d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 93
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$3;->this$0:Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->access$000(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->setAlertDialogOff()V

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$3;->this$0:Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->access$100(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 98
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$3;->this$0:Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->access$100(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->show()V

    .line 100
    :cond_1
    return-void
.end method
