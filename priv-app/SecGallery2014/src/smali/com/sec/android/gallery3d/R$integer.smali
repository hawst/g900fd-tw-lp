.class public final Lcom/sec/android/gallery3d/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final action_bar_home_button_padding_left:I = 0x7f0c0042

.field public static final album_rows_land:I = 0x7f0c0018

.field public static final album_rows_port:I = 0x7f0c0019

.field public static final album_view_album_count_font_color:I = 0x7f0c0062

.field public static final album_view_album_font_color:I = 0x7f0c0061

.field public static final album_view_default_mode:I = 0x7f0c0063

.field public static final album_view_end_affordance_row_degree_count:I = 0x7f0c0076

.field public static final album_view_grid_max_level:I = 0x7f0c0079

.field public static final album_view_grid_min_level:I = 0x7f0c0078

.field public static final album_view_mode_count:I = 0x7f0c0064

.field public static final albumset_dragndrop_bottom_margin:I = 0x7f0c001e

.field public static final albumset_dragndrop_left_margin:I = 0x7f0c001b

.field public static final albumset_dragndrop_right_margin:I = 0x7f0c001d

.field public static final albumset_dragndrop_top_margin:I = 0x7f0c001c

.field public static final albumset_rows_land:I = 0x7f0c0016

.field public static final albumset_rows_port:I = 0x7f0c0017

.field public static final albumset_title_left_margin:I = 0x7f0c001f

.field public static final alert_dialog_gridlist_in_dialog_num_column:I = 0x7f0c0055

.field public static final boundryeffect_viewcover_margin:I = 0x7f0c004a

.field public static final btn_text_disabled_color:I = 0x7f0c004b

.field public static final ca_text_bg_vertical_margin:I = 0x7f0c0013

.field public static final ca_text_bg_vertical_margin_for_date:I = 0x7f0c0004

.field public static final ca_text_horizontal_bottom_margin:I = 0x7f0c0008

.field public static final ca_text_horizontal_left_margin:I = 0x7f0c0014

.field public static final ca_text_horizontal_max_area:I = 0x7f0c0007

.field public static final ca_text_right_margin:I = 0x7f0c0009

.field public static final ca_text_size_big:I = 0x7f0c0001

.field public static final ca_text_size_for_date:I = 0x7f0c0012

.field public static final ca_text_size_normal:I = 0x7f0c0002

.field public static final ca_text_size_small:I = 0x7f0c0003

.field public static final ca_text_vertical_bottom_margin:I = 0x7f0c0015

.field public static final ca_text_vertical_left_margin:I = 0x7f0c0005

.field public static final ca_text_vertical_max_area:I = 0x7f0c0006

.field public static final check_box_height:I = 0x7f0c005e

.field public static final check_box_width:I = 0x7f0c005d

.field public static final default_album_label_text_size:I = 0x7f0c005b

.field public static final default_album_label_text_size_pick:I = 0x7f0c005c

.field public static final default_album_text_size:I = 0x7f0c005a

.field public static final detail_view_default_mode:I = 0x7f0c0074

.field public static final detail_view_mode_count:I = 0x7f0c0075

.field public static final event_view_sub_title_font_color:I = 0x7f0c004f

.field public static final event_view_suggestion_font_color:I = 0x7f0c0050

.field public static final event_view_title_font_color:I = 0x7f0c004e

.field public static final eventview_title_height:I = 0x7f0c0057

.field public static final expansion_height:I = 0x7f0c0060

.field public static final expansion_width:I = 0x7f0c005f

.field public static final gallery_search_title_offset_l:I = 0x7f0c004c

.field public static final gallery_search_title_offset_p:I = 0x7f0c004d

.field public static final glanimation_convset_time:I = 0x7f0c0059

.field public static final glgrid_glspread_anim_time:I = 0x7f0c001a

.field public static final glphotoview_albumset_drawlabel_bottom_margin:I = 0x7f0c0040

.field public static final glphotoview_albumset_drawlabel_font:I = 0x7f0c003f

.field public static final glphotoview_albumset_drawlabel_top_margin:I = 0x7f0c0041

.field public static final glphotoview_thumb_item_max:I = 0x7f0c003e

.field public static final help_panning_dx_coefficient_x:I = 0x7f0c0045

.field public static final help_panning_dx_coefficient_y:I = 0x7f0c0046

.field public static final help_panning_scale_value:I = 0x7f0c0047

.field public static final help_tipbubble_head_top_margin:I = 0x7f0c0043

.field public static final help_tipbubble_text_top_margin:I = 0x7f0c0044

.field public static final location_default_bottom_offset:I = 0x7f0c003c

.field public static final location_default_thumbnail_size:I = 0x7f0c003b

.field public static final location_mediaItem_offset:I = 0x7f0c003d

.field public static final mapview_marker_bitmap_topmargin:I = 0x7f0c0052

.field public static final max_video_recording_length:I = 0x7f0c0000

.field public static final newalbum_bitmap_bottom_margin:I = 0x7f0c0024

.field public static final newalbum_bitmap_right_margin:I = 0x7f0c0023

.field public static final newalbum_frame_bottom_margin:I = 0x7f0c0022

.field public static final noitem_screen_additional_top_margin:I = 0x7f0c0049

.field public static final noitem_screen_location_coef:I = 0x7f0c0048

.field public static final perspective_shuffle_border_width:I = 0x7f0c0051

.field public static final photo_view_grid_max_level:I = 0x7f0c007b

.field public static final photo_view_grid_min_level:I = 0x7f0c007a

.field public static final photoview_checkbox_land_multiframe_offset_right_2:I = 0x7f0c0034

.field public static final photoview_checkbox_land_multiframe_offset_right_3:I = 0x7f0c0035

.field public static final photoview_checkbox_land_multiframe_offset_right_4:I = 0x7f0c0036

.field public static final photoview_checkbox_land_right_margin_2:I = 0x7f0c0025

.field public static final photoview_checkbox_land_right_margin_3:I = 0x7f0c0028

.field public static final photoview_checkbox_land_right_margin_4:I = 0x7f0c002b

.field public static final photoview_checkbox_land_size_2:I = 0x7f0c0027

.field public static final photoview_checkbox_land_size_3:I = 0x7f0c002a

.field public static final photoview_checkbox_land_size_4:I = 0x7f0c002d

.field public static final photoview_checkbox_land_top_margin_2:I = 0x7f0c0026

.field public static final photoview_checkbox_land_top_margin_3:I = 0x7f0c0029

.field public static final photoview_checkbox_land_top_margin_4:I = 0x7f0c002c

.field public static final photoview_checkbox_port_multiframe_offset_right_1:I = 0x7f0c0037

.field public static final photoview_checkbox_port_multiframe_offset_right_2:I = 0x7f0c0038

.field public static final photoview_checkbox_port_right_margin_1:I = 0x7f0c002e

.field public static final photoview_checkbox_port_right_margin_2:I = 0x7f0c0031

.field public static final photoview_checkbox_port_size_1:I = 0x7f0c0030

.field public static final photoview_checkbox_port_size_2:I = 0x7f0c0033

.field public static final photoview_checkbox_port_top_margin_1:I = 0x7f0c002f

.field public static final photoview_checkbox_port_top_margin_2:I = 0x7f0c0032

.field public static final photoview_focus_padding_bottom:I = 0x7f0c003a

.field public static final photoview_focus_padding_right:I = 0x7f0c0039

.field public static final share_via_max_items_before_scroll:I = 0x7f0c0053

.field public static final share_via_max_items_before_scroll_for_tablet:I = 0x7f0c0054

.field public static final spinner_menu_vertical_offset:I = 0x7f0c0056

.field public static final text_horizontal_top_margin:I = 0x7f0c000b

.field public static final text_size_big:I = 0x7f0c000c

.field public static final text_size_small:I = 0x7f0c000d

.field public static final text_vertical_top_margin:I = 0x7f0c000a

.field public static final thumbnail_height_land:I = 0x7f0c000f

.field public static final thumbnail_height_port:I = 0x7f0c0011

.field public static final thumbnail_view_default_mode:I = 0x7f0c0068

.field public static final thumbnail_view_end_affordance_row_degree_count:I = 0x7f0c0077

.field public static final thumbnail_view_hide_split_mode_count:I = 0x7f0c006a

.field public static final thumbnail_view_new_album_background_color:I = 0x7f0c0072

.field public static final thumbnail_view_new_album_font_color:I = 0x7f0c0070

.field public static final thumbnail_view_new_album_mode_count:I = 0x7f0c0073

.field public static final thumbnail_view_new_album_select_count_font_color:I = 0x7f0c0071

.field public static final thumbnail_view_play_icon_size:I = 0x7f0c006c

.field public static final thumbnail_view_show_split_mode_count:I = 0x7f0c0069

.field public static final thumbnail_view_split_album_count_font_color:I = 0x7f0c0067

.field public static final thumbnail_view_split_album_font_color:I = 0x7f0c0065

.field public static final thumbnail_view_split_album_mode_count:I = 0x7f0c006d

.field public static final thumbnail_view_split_album_selected_font_color:I = 0x7f0c0066

.field public static final thumbnail_view_split_extra_gap:I = 0x7f0c006b

.field public static final thumbnail_width_land:I = 0x7f0c000e

.field public static final thumbnail_width_port:I = 0x7f0c0010

.field public static final time_folder_count_font_color:I = 0x7f0c006f

.field public static final time_folder_font_color:I = 0x7f0c006e

.field public static final time_group_check_y_position_more:I = 0x7f0c0058

.field public static final time_view_grid_max_level:I = 0x7f0c007d

.field public static final time_view_grid_min_level:I = 0x7f0c007c

.field public static final timeline_tag_font_color:I = 0x7f0c0021

.field public static final timeview_maxSpeed:I = 0x7f0c0020


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
