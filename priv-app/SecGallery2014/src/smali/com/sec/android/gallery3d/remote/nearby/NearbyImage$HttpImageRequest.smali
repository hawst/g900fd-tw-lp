.class Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;
.super Ljava/lang/Object;
.source "NearbyImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HttpImageRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private mGetRequest:Lorg/apache/http/client/methods/HttpGet;

.field private mType:I

.field private mUriString:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;Ljava/lang/String;I)V
    .locals 1
    .param p2, "uriString"    # Ljava/lang/String;
    .param p3, "type"    # I

    .prologue
    .line 338
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mGetRequest:Lorg/apache/http/client/methods/HttpGet;

    .line 339
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mUriString:Ljava/lang/String;

    .line 340
    iput p3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mType:I

    .line 341
    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;)Lorg/apache/http/client/methods/HttpGet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mGetRequest:Lorg/apache/http/client/methods/HttpGet;

    return-object v0
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 33
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 347
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mUriString:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 348
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$200()Ljava/lang/String;

    move-result-object v5

    const-string v8, "no uri to request"

    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    const/16 v16, 0x0

    .line 495
    :cond_0
    :goto_0
    return-object v16

    .line 352
    :cond_1
    new-instance v5, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest$1;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setCancelListener(Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;)V

    .line 361
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->generateKey()J
    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$000(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)J

    move-result-wide v6

    .line 362
    .local v6, "key":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$600(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v4

    .line 363
    .local v4, "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->get()Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    move-result-object v9

    .line 365
    .local v9, "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$700(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mType:I

    invoke-virtual/range {v4 .. v9}, Lcom/sec/android/gallery3d/data/ImageCacheService;->getImageData(Lcom/sec/android/gallery3d/data/Path;JILcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z

    move-result v20

    .line 366
    .local v20, "found":Z
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_2

    const/16 v16, 0x0

    .line 377
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    goto :goto_0

    .line 367
    :cond_2
    if-eqz v20, :cond_4

    .line 368
    :try_start_1
    new-instance v26, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 369
    .local v26, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v26

    iput-object v5, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 370
    iget-object v5, v9, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    iget v8, v9, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    iget v10, v9, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v5, v8, v10, v1}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodeUsingPool(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;[BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 371
    .local v16, "bitmap":Landroid/graphics/Bitmap;
    if-nez v16, :cond_3

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_3

    .line 372
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$200()Ljava/lang/String;

    move-result-object v5

    const-string v8, "decode cached failed "

    invoke-static {v5, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377
    :cond_3
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    goto :goto_0

    .end local v16    # "bitmap":Landroid/graphics/Bitmap;
    .end local v26    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 380
    const/16 v22, 0x0

    .line 381
    .local v22, "httpClient":Landroid/net/http/AndroidHttpClient;
    const/16 v16, 0x0

    .line 383
    .restart local v16    # "bitmap":Landroid/graphics/Bitmap;
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Nearby Image "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDataVersion:J
    invoke-static {v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$800(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)J

    move-result-wide v10

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v22

    .line 384
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mUriString:Ljava/lang/String;

    if-eqz v5, :cond_5

    const-string/jumbo v5, "wlan0"

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDevice:Lcom/samsung/android/allshare/Device;
    invoke-static {v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$900(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/samsung/android/allshare/Device;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/allshare/Device;->getNIC()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 385
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mUriString:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v21

    .line 386
    .local v21, "host":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mUriString:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getPort()I

    move-result v27

    .line 388
    .local v27, "port":I
    new-instance v28, Lorg/apache/http/HttpHost;

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    .line 389
    .local v28, "proxy":Lorg/apache/http/HttpHost;
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v5

    const-string v8, "http.route.default-proxy"

    move-object/from16 v0, v28

    invoke-interface {v5, v8, v0}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 392
    .end local v21    # "host":Ljava/lang/String;
    .end local v27    # "port":I
    .end local v28    # "proxy":Lorg/apache/http/HttpHost;
    :cond_5
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v5

    const v8, 0xea60

    invoke-static {v5, v8}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 393
    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mUriString:Ljava/lang/String;

    invoke-direct {v5, v8}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mGetRequest:Lorg/apache/http/client/methods/HttpGet;

    .line 394
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mGetRequest:Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v29

    .line 395
    .local v29, "response":Lorg/apache/http/HttpResponse;
    invoke-interface/range {v29 .. v29}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v31

    .line 396
    .local v31, "statusCode":I
    const/16 v5, 0xc8

    move/from16 v0, v31

    if-eq v0, v5, :cond_7

    .line 397
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$200()Ljava/lang/String;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v31

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " while retrieving bitmap : "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mUriString:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->setBroken(Z)V

    .line 399
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mType:I

    const/4 v8, 0x1

    if-ne v5, v8, :cond_6

    .line 400
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$1000(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v10, 0x7f0200a7

    invoke-virtual {v5, v8, v10}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v16

    .line 490
    .end local v16    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v22, :cond_0

    .line 491
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 492
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->close()V

    goto/16 :goto_0

    .line 377
    .end local v20    # "found":Z
    .end local v22    # "httpClient":Landroid/net/http/AndroidHttpClient;
    .end local v29    # "response":Lorg/apache/http/HttpResponse;
    .end local v31    # "statusCode":I
    :catchall_0
    move-exception v5

    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v8

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    throw v5

    .line 402
    .restart local v16    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v20    # "found":Z
    .restart local v22    # "httpClient":Landroid/net/http/AndroidHttpClient;
    .restart local v29    # "response":Lorg/apache/http/HttpResponse;
    .restart local v31    # "statusCode":I
    :cond_6
    :try_start_3
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$1100(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenPictureThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v16

    .line 490
    .end local v16    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v22, :cond_0

    .line 491
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 492
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->close()V

    goto/16 :goto_0

    .line 406
    .restart local v16    # "bitmap":Landroid/graphics/Bitmap;
    :cond_7
    :try_start_4
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result v5

    if-eqz v5, :cond_8

    .line 407
    const/16 v16, 0x0

    .line 490
    .end local v16    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v22, :cond_0

    .line 491
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 492
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->close()V

    goto/16 :goto_0

    .line 410
    .restart local v16    # "bitmap":Landroid/graphics/Bitmap;
    :cond_8
    :try_start_5
    invoke-interface/range {v29 .. v29}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result-object v19

    .line 411
    .local v19, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v19, :cond_19

    .line 412
    const/16 v23, 0x0

    .line 414
    .local v23, "inputStream":Ljava/io/InputStream;
    :try_start_6
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v23

    .line 418
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getMimeType()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getMimeType()Ljava/lang/String;

    move-result-object v5

    const-string v8, "bmp"

    invoke-virtual {v5, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 419
    invoke-static/range {v23 .. v23}, Lcom/sec/android/gallery3d/util/GalleryUtils;->convertInputStreamToByteArray(Ljava/io/InputStream;)[B

    move-result-object v17

    .line 421
    .local v17, "byteArray":[B
    const/4 v5, 0x0

    move-object/from16 v0, v17

    array-length v8, v0

    move-object/from16 v0, v17

    invoke-static {v0, v5, v8}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 431
    .end local v17    # "byteArray":[B
    :goto_1
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v5

    if-eqz v5, :cond_c

    .line 432
    const/4 v5, 0x0

    .line 478
    :try_start_7
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 479
    invoke-static/range {v23 .. v23}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 490
    if-eqz v22, :cond_9

    .line 491
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 492
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_9
    move-object/from16 v16, v5

    goto/16 :goto_0

    .line 423
    :cond_a
    :try_start_8
    new-instance v25, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 424
    .local v25, "option":Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x1

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v32

    .line 425
    .local v32, "thumbnailSize":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;
    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$1200(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/samsung/android/allshare/Item;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v5

    if-nez v5, :cond_b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getHeight()I

    move-result v8

    mul-int/2addr v5, v8

    mul-int v8, v32, v32

    mul-int/lit8 v8, v8, 0x2

    if-le v5, v8, :cond_b

    .line 427
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getHeight()I

    move-result v8

    move/from16 v0, v32

    invoke-static {v5, v8, v0}, Lcom/sec/android/gallery3d/common/BitmapUtils;->computeSampleSizeLarger(III)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 428
    :cond_b
    const/4 v5, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-static {v0, v5, v1}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v16

    goto :goto_1

    .line 435
    .end local v25    # "option":Landroid/graphics/BitmapFactory$Options;
    .end local v32    # "thumbnailSize":I
    :cond_c
    if-nez v16, :cond_f

    .line 436
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$200()Ljava/lang/String;

    move-result-object v8

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "cannot decode bitmap from InputStream : "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-nez v23, :cond_e

    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 438
    const/4 v5, 0x0

    .line 478
    :try_start_9
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 479
    invoke-static/range {v23 .. v23}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 490
    if-eqz v22, :cond_d

    .line 491
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 492
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_d
    move-object/from16 v16, v5

    goto/16 :goto_0

    .line 436
    :cond_e
    :try_start_a
    invoke-virtual/range {v23 .. v23}, Ljava/io/InputStream;->available()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_2

    .line 443
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;
    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$1200(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/samsung/android/allshare/Item;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Item;->getThumbnail()Landroid/net/Uri;

    move-result-object v5

    if-nez v5, :cond_11

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getWidth()I

    move-result v5

    if-eqz v5, :cond_10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getHeight()I

    move-result v5

    if-nez v5, :cond_11

    .line 444
    :cond_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDecodedWidth:I
    invoke-static {v5, v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$302(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;I)I

    .line 445
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDecodedHeight:I
    invoke-static {v5, v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$402(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;I)I

    .line 448
    :cond_11
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mType:I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    packed-switch v5, :pswitch_data_0

    .line 470
    :goto_3
    if-nez v16, :cond_16

    .line 471
    const/4 v5, 0x0

    .line 478
    :try_start_b
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 479
    invoke-static/range {v23 .. v23}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 490
    if-eqz v22, :cond_12

    .line 491
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 492
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_12
    move-object/from16 v16, v5

    goto/16 :goto_0

    .line 450
    :pswitch_0
    :try_start_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getHeight()I

    move-result v8

    if-le v5, v8, :cond_14

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getWidth()I

    move-result v24

    .line 451
    .local v24, "longSideLength":I
    :goto_4
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mType:I

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v5

    move/from16 v0, v24

    if-le v0, v5, :cond_15

    .line 452
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mType:I

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v5

    const/4 v8, 0x1

    move-object/from16 v0, v16

    invoke-static {v0, v5, v8}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeBySideLength(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 457
    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mUriString:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->getOrientationValue(Ljava/lang/String;)I

    move-result v30

    .line 458
    .local v30, "rotation":I
    const/4 v5, -0x1

    move/from16 v0, v30

    if-ne v0, v5, :cond_13

    const/16 v30, 0x0

    .end local v30    # "rotation":I
    :cond_13
    const/4 v5, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v30

    invoke-static {v0, v1, v5}, Lcom/sec/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 460
    goto :goto_3

    .line 450
    .end local v24    # "longSideLength":I
    :cond_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getHeight()I

    move-result v24

    goto :goto_4

    .line 455
    .restart local v24    # "longSideLength":I
    :cond_15
    const/4 v5, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-static {v0, v1, v5}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeBySideLength(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v16

    goto :goto_5

    .line 462
    .end local v24    # "longSideLength":I
    :pswitch_1
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mType:I

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v5

    const/4 v8, 0x1

    move-object/from16 v0, v16

    invoke-static {v0, v5, v8}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 464
    goto/16 :goto_3

    .line 466
    :pswitch_2
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mType:I

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v5

    const/4 v8, 0x1

    move-object/from16 v0, v16

    invoke-static {v0, v5, v8}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v16

    goto/16 :goto_3

    .line 473
    :cond_16
    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/common/BitmapUtils;->compressBitmap(Landroid/graphics/Bitmap;)[B

    move-result-object v15

    .line 474
    .local v15, "array":[B
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$200()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v8, "thumbnail image download"

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$1300(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v11

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mType:I

    move-object v10, v4

    move-wide v12, v6

    invoke-virtual/range {v10 .. v15}, Lcom/sec/android/gallery3d/data/ImageCacheService;->putImageData(Lcom/sec/android/gallery3d/data/Path;JI[B)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 478
    :try_start_d
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 479
    invoke-static/range {v23 .. v23}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 490
    if-eqz v22, :cond_0

    .line 491
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 492
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->close()V

    goto/16 :goto_0

    .line 478
    .end local v15    # "array":[B
    :catchall_1
    move-exception v5

    :try_start_e
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 479
    invoke-static/range {v23 .. v23}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v5
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 482
    .end local v19    # "entity":Lorg/apache/http/HttpEntity;
    .end local v23    # "inputStream":Ljava/io/InputStream;
    .end local v29    # "response":Lorg/apache/http/HttpResponse;
    .end local v31    # "statusCode":I
    :catch_0
    move-exception v18

    .line 485
    .local v18, "e":Ljava/lang/Exception;
    :try_start_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mGetRequest:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v5, :cond_17

    .line 486
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->mGetRequest:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v5}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 487
    :cond_17
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->access$200()Ljava/lang/String;

    move-result-object v5

    const-string v8, "Error while retrieving bitmap "

    invoke-static {v5, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 490
    if-eqz v22, :cond_18

    .line 491
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 492
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 495
    .end local v18    # "e":Ljava/lang/Exception;
    :cond_18
    :goto_6
    const/16 v16, 0x0

    goto/16 :goto_0

    .line 490
    .restart local v19    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v29    # "response":Lorg/apache/http/HttpResponse;
    .restart local v31    # "statusCode":I
    :cond_19
    if-eqz v22, :cond_18

    .line 491
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 492
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->close()V

    goto :goto_6

    .line 490
    .end local v19    # "entity":Lorg/apache/http/HttpEntity;
    .end local v29    # "response":Lorg/apache/http/HttpResponse;
    .end local v31    # "statusCode":I
    :catchall_2
    move-exception v5

    if-eqz v22, :cond_1a

    .line 491
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 492
    invoke-virtual/range {v22 .. v22}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_1a
    throw v5

    .line 448
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 332
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
