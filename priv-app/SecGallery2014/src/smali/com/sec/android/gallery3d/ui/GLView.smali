.class public Lcom/sec/android/gallery3d/ui/GLView;
.super Ljava/lang/Object;
.source "GLView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "WrongCall"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/GLView$OnClickListener;
    }
.end annotation


# static fields
.field private static final FLAG_INVISIBLE:I = 0x1

.field private static final FLAG_LAYOUT_REQUESTED:I = 0x4

.field private static final FLAG_SET_MEASURED_SIZE:I = 0x2

.field public static final INVISIBLE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GLView"

.field public static final VISIBLE:I


# instance fields
.field private mAnimation:Lcom/sec/android/gallery3d/anim/CanvasAnimation;

.field private mBackgroundColor:[F

.field protected final mBounds:Landroid/graphics/Rect;

.field private mComponents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/ui/GLView;",
            ">;"
        }
    .end annotation
.end field

.field protected mGLViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

.field protected mGLViewAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

.field private mLastHeightSpec:I

.field private mLastWidthSpec:I

.field protected mMeasuredHeight:I

.field protected mMeasuredWidth:I

.field private mMotionTarget:Lcom/sec/android/gallery3d/ui/GLView;

.field protected final mPaddings:Landroid/graphics/Rect;

.field protected mParent:Lcom/sec/android/gallery3d/ui/GLView;

.field private mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

.field protected mScrollHeight:I

.field protected mScrollWidth:I

.field protected mScrollX:I

.field protected mScrollY:I

.field private mTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

.field private mViewFlags:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    .line 70
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mPaddings:Landroid/graphics/Rect;

    .line 79
    iput v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    .line 81
    iput v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMeasuredWidth:I

    .line 82
    iput v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMeasuredHeight:I

    .line 84
    iput v2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mLastWidthSpec:I

    .line 85
    iput v2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mLastHeightSpec:I

    .line 87
    iput v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mScrollY:I

    .line 88
    iput v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mScrollX:I

    .line 89
    iput v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mScrollHeight:I

    .line 90
    iput v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mScrollWidth:I

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mGLViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

    return-void
.end method

.method private removeOneComponent(Lcom/sec/android/gallery3d/ui/GLView;)V
    .locals 9
    .param p1, "component"    # Lcom/sec/android/gallery3d/ui/GLView;

    .prologue
    const/4 v5, 0x0

    .line 191
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMotionTarget:Lcom/sec/android/gallery3d/ui/GLView;

    if-ne v2, p1, :cond_0

    .line 192
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 193
    .local v0, "now":J
    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 195
    .local v8, "cancelEvent":Landroid/view/MotionEvent;
    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 196
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    .line 198
    .end local v0    # "now":J
    .end local v8    # "cancelEvent":Landroid/view/MotionEvent;
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/GLView;->onDetachFromRoot()V

    .line 199
    const/4 v2, 0x0

    iput-object v2, p1, Lcom/sec/android/gallery3d/ui/GLView;->mParent:Lcom/sec/android/gallery3d/ui/GLView;

    .line 200
    return-void
.end method

.method private setBounds(IIII)Z
    .locals 4
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 386
    sub-int v1, p3, p1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    if-ne v1, v2, :cond_0

    sub-int v1, p4, p2

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    if-eq v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 388
    .local v0, "sizeChanged":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 389
    return v0

    .line 386
    .end local v0    # "sizeChanged":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V
    .locals 1
    .param p1, "component"    # Lcom/sec/android/gallery3d/ui/GLView;

    .prologue
    .line 157
    iget-object v0, p1, Lcom/sec/android/gallery3d/ui/GLView;->mParent:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    iput-object p0, p1, Lcom/sec/android/gallery3d/ui/GLView;->mParent:Lcom/sec/android/gallery3d/ui/GLView;

    .line 167
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    if-eqz v0, :cond_2

    .line 168
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/ui/GLView;->onAttachToRoot(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    .line 170
    :cond_2
    return-void
.end method

.method public attachToRoot(Lcom/sec/android/gallery3d/ui/GLRoot;)V
    .locals 1
    .param p1, "root"    # Lcom/sec/android/gallery3d/ui/GLRoot;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mParent:Lcom/sec/android/gallery3d/ui/GLView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 132
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->onAttachToRoot(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    .line 133
    return-void

    .line 131
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bounds()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    return-object v0
.end method

.method public createAccessibilityNodeInfo(ILcom/sec/android/gallery3d/glcore/GlRootView;)Landroid/view/accessibility/AccessibilityNodeInfo;
    .locals 5
    .param p1, "virtualDescendantId"    # I
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 583
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v2

    .line 584
    .local v2, "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual {p2, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 585
    const/4 v0, 0x0

    .line 586
    .local v0, "component":Lcom/sec/android/gallery3d/ui/GLView;
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponentCount()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 587
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/GLView;->getComponent(I)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v0

    .line 588
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/GLView;->getGLViewAccessibility()Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 589
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/GLView;->getGLViewAccessibility()Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

    move-result-object v4

    invoke-virtual {v4, p1, v2}, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;->createAccessibilityNodeInfo(ILandroid/view/accessibility/AccessibilityNodeInfo;)Landroid/view/accessibility/AccessibilityNodeInfo;

    .line 586
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 592
    :cond_1
    return-object v2
.end method

.method public detachFromRoot()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mParent:Lcom/sec/android/gallery3d/ui/GLView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->onDetachFromRoot()V

    .line 139
    return-void

    .line 137
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchDragEvent(Landroid/view/DragEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 530
    const/4 v0, 0x0

    return v0
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 526
    const/4 v0, 0x0

    return v0
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 562
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    move v1, v2

    .line 568
    :goto_0
    return v1

    .line 564
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 565
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 566
    const/4 v1, 0x1

    goto :goto_0

    .line 564
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 568
    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 331
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v2, v0

    .line 332
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    .line 333
    .local v3, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    .line 334
    .local v10, "action":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMotionTarget:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v0, :cond_0

    .line 335
    if-nez v10, :cond_3

    .line 336
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 337
    .local v1, "cancel":Landroid/view/MotionEvent;
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 338
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMotionTarget:Lcom/sec/android/gallery3d/ui/GLView;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;IILcom/sec/android/gallery3d/ui/GLView;Z)Z

    .line 339
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMotionTarget:Lcom/sec/android/gallery3d/ui/GLView;

    .line 352
    .end local v1    # "cancel":Landroid/view/MotionEvent;
    :cond_0
    if-eqz v10, :cond_1

    const/16 v0, 0x3e8

    if-eq v10, v0, :cond_1

    const/16 v0, 0x3ea

    if-eq v10, v0, :cond_1

    const/16 v0, 0x8

    if-eq v10, v0, :cond_1

    const/16 v0, 0x3eb

    if-ne v10, v0, :cond_7

    .line 359
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponentCount()I

    move-result v0

    add-int/lit8 v11, v0, -0x1

    .local v11, "i":I
    :goto_0
    if-ltz v11, :cond_7

    .line 360
    invoke-virtual {p0, v11}, Lcom/sec/android/gallery3d/ui/GLView;->getComponent(I)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v8

    .line 361
    .local v8, "component":Lcom/sec/android/gallery3d/ui/GLView;
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/GLView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_6

    .line 359
    :cond_2
    add-int/lit8 v11, v11, -0x1

    goto :goto_0

    .line 341
    .end local v8    # "component":Lcom/sec/android/gallery3d/ui/GLView;
    .end local v11    # "i":I
    :cond_3
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMotionTarget:Lcom/sec/android/gallery3d/ui/GLView;

    const/4 v9, 0x0

    move-object v4, p0

    move-object v5, p1

    move v6, v2

    move v7, v3

    invoke-virtual/range {v4 .. v9}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;IILcom/sec/android/gallery3d/ui/GLView;Z)Z

    .line 342
    const/4 v0, 0x3

    if-eq v10, v0, :cond_4

    const/4 v0, 0x1

    if-eq v10, v0, :cond_4

    const/16 v0, 0x3e9

    if-ne v10, v0, :cond_5

    .line 347
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMotionTarget:Lcom/sec/android/gallery3d/ui/GLView;

    .line 349
    :cond_5
    const/4 v0, 0x1

    .line 368
    :goto_1
    return v0

    .line 362
    .restart local v8    # "component":Lcom/sec/android/gallery3d/ui/GLView;
    .restart local v11    # "i":I
    :cond_6
    const/4 v9, 0x1

    move-object v4, p0

    move-object v5, p1

    move v6, v2

    move v7, v3

    invoke-virtual/range {v4 .. v9}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;IILcom/sec/android/gallery3d/ui/GLView;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 363
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMotionTarget:Lcom/sec/android/gallery3d/ui/GLView;

    .line 364
    const/4 v0, 0x1

    goto :goto_1

    .line 368
    .end local v8    # "component":Lcom/sec/android/gallery3d/ui/GLView;
    .end local v11    # "i":I
    :cond_7
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->onTouch(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method protected dispatchTouchEvent(Landroid/view/MotionEvent;IILcom/sec/android/gallery3d/ui/GLView;Z)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "component"    # Lcom/sec/android/gallery3d/ui/GLView;
    .param p5, "checkBounds"    # Z

    .prologue
    .line 314
    iget-object v1, p4, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    .line 315
    .local v1, "rect":Landroid/graphics/Rect;
    iget v0, v1, Landroid/graphics/Rect;->left:I

    .line 316
    .local v0, "left":I
    iget v2, v1, Landroid/graphics/Rect;->top:I

    .line 317
    .local v2, "top":I
    if-eqz p5, :cond_0

    invoke-virtual {v1, p2, p3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 318
    :cond_0
    neg-int v3, v0

    int-to-float v3, v3

    neg-int v4, v2

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 319
    invoke-virtual {p4, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 320
    int-to-float v3, v0

    int-to-float v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 321
    const/4 v3, 0x1

    .line 325
    :goto_0
    return v3

    .line 323
    :cond_1
    int-to-float v3, v0

    int-to-float v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 325
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method dumpTree(Ljava/lang/String;)V
    .locals 5
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 487
    const-string v2, "GLView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponentCount()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 489
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponent(I)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "...."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/GLView;->dumpTree(Ljava/lang/String;)V

    .line 488
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 491
    :cond_0
    return-void
.end method

.method public getBackgroundColor()[F
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBackgroundColor:[F

    return-object v0
.end method

.method public getBoundsOf(Lcom/sec/android/gallery3d/ui/GLView;Landroid/graphics/Rect;)Z
    .locals 6
    .param p1, "descendant"    # Lcom/sec/android/gallery3d/ui/GLView;
    .param p2, "out"    # Landroid/graphics/Rect;

    .prologue
    .line 434
    const/4 v2, 0x0

    .line 435
    .local v2, "xoffset":I
    const/4 v3, 0x0

    .line 436
    .local v3, "yoffset":I
    move-object v1, p1

    .line 437
    .local v1, "view":Lcom/sec/android/gallery3d/ui/GLView;
    :goto_0
    if-eq v1, p0, :cond_1

    .line 438
    if-nez v1, :cond_0

    const/4 v4, 0x0

    .line 446
    :goto_1
    return v4

    .line 439
    :cond_0
    iget-object v0, v1, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    .line 440
    .local v0, "bounds":Landroid/graphics/Rect;
    iget v4, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v4

    .line 441
    iget v4, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    .line 442
    iget-object v1, v1, Lcom/sec/android/gallery3d/ui/GLView;->mParent:Lcom/sec/android/gallery3d/ui/GLView;

    .line 443
    goto :goto_0

    .line 444
    .end local v0    # "bounds":Landroid/graphics/Rect;
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/GLView;->getWidth()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/GLView;->getHeight()I

    move-result v5

    add-int/2addr v5, v3

    invoke-virtual {p2, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 446
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public getComponent(I)Lcom/sec/android/gallery3d/ui/GLView;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 149
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/GLView;

    return-object v0
.end method

.method public getComponentCount()I
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    return-object v0
.end method

.method public getGLViewAccessibility()Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mGLViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

    return-object v0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getMeasuredHeight()I
    .locals 1

    .prologue
    .line 423
    iget v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMeasuredHeight:I

    return v0
.end method

.method public getMeasuredWidth()I
    .locals 1

    .prologue
    .line 419
    iget v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMeasuredWidth:I

    return v0
.end method

.method public getPaddings()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mPaddings:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getVisibility()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    return v0
.end method

.method protected initAccessibilityListener()V
    .locals 1

    .prologue
    .line 575
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mGLViewAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    .line 576
    return-void
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    .line 222
    .local v0, "root":Lcom/sec/android/gallery3d/ui/GLRoot;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->requestRender()V

    .line 223
    :cond_0
    return-void
.end method

.method public layout(IIII)V
    .locals 6
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 376
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/ui/GLView;->setBounds(IIII)Z

    move-result v1

    .line 377
    .local v1, "sizeChanged":Z
    iget v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    .line 382
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/GLView;->onLayout(ZIIII)V

    .line 383
    return-void
.end method

.method public lockRendering()V
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->lockRenderThread()V

    .line 476
    :cond_0
    return-void
.end method

.method public measure(II)V
    .locals 3
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    .line 393
    iget v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mLastWidthSpec:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mLastHeightSpec:I

    if-ne p2, v0, :cond_1

    iget v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_1

    .line 407
    :cond_0
    return-void

    .line 398
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mLastWidthSpec:I

    .line 399
    iput p2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mLastHeightSpec:I

    .line 401
    iget v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    .line 402
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onMeasure(II)V

    .line 403
    iget v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    .line 404
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should call setMeasuredSize() in onMeasure()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onAttachToRoot(Lcom/sec/android/gallery3d/ui/GLRoot;)V
    .locals 3
    .param p1, "root"    # Lcom/sec/android/gallery3d/ui/GLRoot;

    .prologue
    .line 459
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    .line 460
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponentCount()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 461
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponent(I)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/ui/GLView;->onAttachToRoot(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    .line 460
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 463
    :cond_0
    return-void
.end method

.method protected onDetachFromRoot()V
    .locals 3

    .prologue
    .line 466
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponentCount()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 467
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponent(I)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/GLView;->onDetachFromRoot()V

    .line 466
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 469
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    .line 470
    return-void
.end method

.method public onGenericMotionCancel()V
    .locals 0

    .prologue
    .line 559
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 495
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    move v1, v2

    .line 501
    :goto_0
    return v1

    .line 497
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 498
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 499
    const/4 v1, 0x1

    goto :goto_0

    .line 497
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 501
    goto :goto_0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 515
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    move v1, v2

    .line 522
    :goto_0
    return v1

    .line 517
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 518
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 519
    const/4 v1, 0x1

    goto :goto_0

    .line 517
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 522
    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 505
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    move v1, v2

    .line 511
    :goto_0
    return v1

    .line 507
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 508
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 509
    const/4 v1, 0x1

    goto :goto_0

    .line 507
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 511
    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changeSize"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 428
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    .line 410
    return-void
.end method

.method public onStylusButtonEvent(Landroid/view/MotionEvent;I)V
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "clipboardId"    # I

    .prologue
    .line 542
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v5, v7

    .line 543
    .local v5, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v6, v7

    .line 545
    .local v6, "y":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponentCount()I

    move-result v7

    add-int/lit8 v1, v7, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 546
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/GLView;->getComponent(I)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v0

    .line 547
    .local v0, "component":Lcom/sec/android/gallery3d/ui/GLView;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/GLView;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_1

    .line 545
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 549
    :cond_1
    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    .line 550
    .local v3, "rect":Landroid/graphics/Rect;
    iget v2, v3, Landroid/graphics/Rect;->left:I

    .line 551
    .local v2, "left":I
    iget v4, v3, Landroid/graphics/Rect;->top:I

    .line 552
    .local v4, "top":I
    invoke-virtual {v3, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 553
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onStylusButtonEvent(Landroid/view/MotionEvent;I)V

    goto :goto_1

    .line 556
    .end local v0    # "component":Lcom/sec/android/gallery3d/ui/GLView;
    .end local v2    # "left":I
    .end local v3    # "rect":Landroid/graphics/Rect;
    .end local v4    # "top":I
    :cond_2
    return-void
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 309
    const/4 v0, 0x0

    return v0
.end method

.method protected onVisibilityChanged(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 450
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponentCount()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 451
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/GLView;->getComponent(I)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v0

    .line 452
    .local v0, "child":Lcom/sec/android/gallery3d/ui/GLView;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/GLView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 453
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->onVisibilityChanged(I)V

    .line 450
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 456
    .end local v0    # "child":Lcom/sec/android/gallery3d/ui/GLView;
    :cond_1
    return-void
.end method

.method public performAction(IILandroid/os/Bundle;)Z
    .locals 5
    .param p1, "virtualViewId"    # I
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 596
    const/4 v0, 0x0

    .line 597
    .local v0, "component":Lcom/sec/android/gallery3d/ui/GLView;
    const/4 v3, 0x0

    .line 598
    .local v3, "result":Z
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponentCount()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 599
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/GLView;->getComponent(I)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v0

    .line 600
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/GLView;->getGLViewAccessibility()Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 601
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/GLView;->getGLViewAccessibility()Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;

    move-result-object v4

    invoke-virtual {v4, p1, p2, p3}, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;->performAction(IILandroid/os/Bundle;)Z

    move-result v3

    .line 598
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 604
    :cond_1
    return v3
.end method

.method public removeAllComponents()V
    .locals 3

    .prologue
    .line 184
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 185
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/GLView;

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/GLView;->removeOneComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 187
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 188
    return-void
.end method

.method public removeComponent(Lcom/sec/android/gallery3d/ui/GLView;)Z
    .locals 2
    .param p1, "component"    # Lcom/sec/android/gallery3d/ui/GLView;

    .prologue
    const/4 v0, 0x0

    .line 174
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return v0

    .line 175
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mComponents:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->removeOneComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 177
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 242
    const/4 v2, 0x0

    .line 243
    .local v2, "transitionActive":Z
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/GLView;->mTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/GLView;->mTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    invoke-static {}, Lcom/sec/android/gallery3d/ui/AnimationTime;->get()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;->calculate(J)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->invalidate()V

    .line 245
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/GLView;->mTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;->isActive()Z

    move-result v2

    .line 247
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->renderBackground(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 248
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save()V

    .line 249
    if-eqz v2, :cond_1

    .line 250
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/GLView;->mTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    invoke-virtual {v3, p0, p1}, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;->applyContentTransform(Lcom/sec/android/gallery3d/ui/GLView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 252
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponentCount()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 253
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/GLView;->getComponent(I)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/gallery3d/ui/GLView;->renderChild(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 252
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 255
    :cond_2
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 256
    if-eqz v2, :cond_3

    .line 257
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/GLView;->mTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    invoke-virtual {v3, p0, p1}, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;->applyOverlay(Lcom/sec/android/gallery3d/ui/GLView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 259
    :cond_3
    return-void
.end method

.method protected renderBackground(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 1
    .param p1, "view"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBackgroundColor:[F

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBackgroundColor:[F

    invoke-interface {p1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->clearBuffer([F)V

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;->isActive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;->applyBackground(Lcom/sec/android/gallery3d/ui/GLView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 282
    :cond_1
    return-void
.end method

.method protected renderChild(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/ui/GLView;)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "component"    # Lcom/sec/android/gallery3d/ui/GLView;

    .prologue
    .line 285
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/ui/GLView;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p2, Lcom/sec/android/gallery3d/ui/GLView;->mAnimation:Lcom/sec/android/gallery3d/anim/CanvasAnimation;

    if-nez v3, :cond_0

    .line 306
    :goto_0
    return-void

    .line 288
    :cond_0
    iget-object v3, p2, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/GLView;->mScrollX:I

    sub-int v1, v3, v4

    .line 289
    .local v1, "xoffset":I
    iget-object v3, p2, Lcom/sec/android/gallery3d/ui/GLView;->mBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/GLView;->mScrollY:I

    sub-int v2, v3, v4

    .line 291
    .local v2, "yoffset":I
    int-to-float v3, v1

    int-to-float v4, v2

    invoke-interface {p1, v3, v4}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 293
    iget-object v0, p2, Lcom/sec/android/gallery3d/ui/GLView;->mAnimation:Lcom/sec/android/gallery3d/anim/CanvasAnimation;

    .line 294
    .local v0, "anim":Lcom/sec/android/gallery3d/anim/CanvasAnimation;
    if-eqz v0, :cond_1

    .line 295
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/anim/CanvasAnimation;->getCanvasSaveFlags()I

    move-result v3

    invoke-interface {p1, v3}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 296
    invoke-static {}, Lcom/sec/android/gallery3d/ui/AnimationTime;->get()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/anim/CanvasAnimation;->calculate(J)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->invalidate()V

    .line 301
    :goto_1
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/anim/CanvasAnimation;->apply(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 303
    :cond_1
    invoke-virtual {p2, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 304
    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 305
    :cond_2
    neg-int v3, v1

    int-to-float v3, v3

    neg-int v4, v2

    int-to-float v4, v4

    invoke-interface {p1, v3, v4}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    goto :goto_0

    .line 299
    :cond_3
    const/4 v3, 0x0

    iput-object v3, p2, Lcom/sec/android/gallery3d/ui/GLView;->mAnimation:Lcom/sec/android/gallery3d/anim/CanvasAnimation;

    goto :goto_1
.end method

.method public requestLayout()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 227
    iget v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    .line 228
    iput v2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mLastHeightSpec:I

    .line 229
    iput v2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mLastWidthSpec:I

    .line 230
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mParent:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v1, :cond_1

    .line 231
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mParent:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/GLView;->requestLayout()V

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    .line 235
    .local v0, "root":Lcom/sec/android/gallery3d/ui/GLRoot;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->requestLayoutContentPane()V

    goto :goto_0
.end method

.method public setBackgroundColor([F)V
    .locals 0
    .param p1, "color"    # [F

    .prologue
    .line 271
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mBackgroundColor:[F

    .line 272
    return-void
.end method

.method public setIntroAnimation(Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;)V
    .locals 1
    .param p1, "intro"    # Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    .line 263
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mTransition:Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/anim/StateTransitionAnimation;->start()V

    .line 264
    :cond_0
    return-void
.end method

.method protected setMeasuredSize(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 413
    iget v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    .line 414
    iput p1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMeasuredWidth:I

    .line 415
    iput p2, p0, Lcom/sec/android/gallery3d/ui/GLView;->mMeasuredHeight:I

    .line 416
    return-void
.end method

.method public setPaddings(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 538
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mPaddings:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 539
    return-void
.end method

.method public setPaddings(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "paddings"    # Landroid/graphics/Rect;

    .prologue
    .line 534
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mPaddings:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 535
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getVisibility()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 122
    :goto_0
    return-void

    .line 115
    :cond_0
    if-nez p1, :cond_1

    .line 116
    iget v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    .line 120
    :goto_1
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->onVisibilityChanged(I)V

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->invalidate()V

    goto :goto_0

    .line 118
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mViewFlags:I

    goto :goto_1
.end method

.method public startAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V
    .locals 3
    .param p1, "animation"    # Lcom/sec/android/gallery3d/anim/CanvasAnimation;

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    .line 99
    .local v0, "root":Lcom/sec/android/gallery3d/ui/GLRoot;
    if-nez v0, :cond_0

    .line 100
    const-string v1, "GLView"

    const-string v2, "root null"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/ui/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :goto_0
    return-void

    .line 103
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mAnimation:Lcom/sec/android/gallery3d/anim/CanvasAnimation;

    .line 104
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mAnimation:Lcom/sec/android/gallery3d/anim/CanvasAnimation;

    if-eqz v1, :cond_1

    .line 105
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mAnimation:Lcom/sec/android/gallery3d/anim/CanvasAnimation;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/anim/CanvasAnimation;->start()V

    .line 106
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/GLView;->mAnimation:Lcom/sec/android/gallery3d/anim/CanvasAnimation;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->registerLaunchedAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V

    .line 108
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/GLView;->invalidate()V

    goto :goto_0
.end method

.method public unlockRendering()V
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/GLView;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    .line 482
    :cond_0
    return-void
.end method
