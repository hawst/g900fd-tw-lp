.class public Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;
.super Lcom/sec/android/gallery3d/remote/RemoteBaseDBHelper;
.source "SNSDatabase.java"


# static fields
.field public static final DATABASE_NAME:Ljava/lang/String; = "sns.db"

.field public static final DATABASE_VERSION:I = 0x5f


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const-string/jumbo v0, "sns.db"

    const/16 v1, 0x5f

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/remote/RemoteBaseDBHelper;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 31
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 35
    new-instance v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;-><init>()V

    const-string/jumbo v1, "source_id"

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->createTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;-><init>()V

    const-string/jumbo v1, "source_id"

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->createTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 41
    const-string v0, "photos"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->dropTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 42
    const-string v0, "albums"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->dropTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 44
    return-void
.end method
