.class Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;
.super Ljava/lang/Object;
.source "GlCanvas.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlCanvas;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CanvasSetInfo"
.end annotation


# instance fields
.field private mCount:[I

.field private mItem:[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

.field private mKeySet:[I

.field private mTypeCount:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "typeSize"    # I

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mKeySet:[I

    .line 244
    new-array v0, p1, [[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mItem:[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    .line 245
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mCount:[I

    .line 246
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mTypeCount:I

    .line 247
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mKeySet:[I

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;[I)[I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;
    .param p1, "x1"    # [I

    .prologue
    .line 236
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mKeySet:[I

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;

    .prologue
    .line 236
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mTypeCount:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;
    .param p1, "x1"    # I

    .prologue
    .line 236
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mTypeCount:I

    return p1
.end method

.method static synthetic access$104(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;

    .prologue
    .line 236
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mTypeCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mTypeCount:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mItem:[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;)[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;
    .param p1, "x1"    # [[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    .prologue
    .line 236
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mItem:[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mCount:[I

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;[I)[I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;
    .param p1, "x1"    # [I

    .prologue
    .line 236
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mCount:[I

    return-object p1
.end method
