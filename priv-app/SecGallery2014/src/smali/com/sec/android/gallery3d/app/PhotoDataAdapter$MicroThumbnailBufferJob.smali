.class Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;
.super Ljava/lang/Object;
.source "PhotoDataAdapter.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MicroThumbnailBufferJob"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/sec/android/gallery3d/ui/ScreenNail;",
        ">;"
    }
.end annotation


# instance fields
.field private mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

.field private mIndex:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/samsung/gallery/decoder/CacheInterface;I)V
    .locals 0
    .param p2, "ci"    # Lcom/sec/samsung/gallery/decoder/CacheInterface;
    .param p3, "index"    # I

    .prologue
    .line 2071
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2072
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    .line 2073
    iput p3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;->mIndex:I

    .line 2074
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 7
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v4, 0x0

    .line 2078
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;->mIndex:I

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getImageEntryByIndex(I)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    move-result-object v1

    .line 2079
    .local v1, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)[Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;->mIndex:I

    rem-int/lit8 v6, v6, 0x28

    aget-object v3, v5, v6

    .line 2080
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    if-nez v3, :cond_1

    .line 2093
    :cond_0
    :goto_0
    return-object v4

    .line 2082
    :cond_1
    iget-object v5, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v5, :cond_2

    .line 2083
    iget-object v4, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    goto :goto_0

    .line 2085
    :cond_2
    const/4 v2, 0x0

    .line 2086
    .local v2, "info":Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->get()Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    move-result-object v0

    .line 2088
    .local v0, "data":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    const/4 v6, 0x2

    invoke-virtual {v5, v3, v6}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->getCacheBuffer(Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2090
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 2093
    if-eqz v2, :cond_0

    new-instance v4, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v6, v6, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    iget v6, v6, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->BUFFER_TYPE_MTN:I

    invoke-direct {v4, v5, v2, v6}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;-><init>(Lcom/sec/samsung/gallery/decoder/CacheInterface;Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;I)V

    goto :goto_0

    .line 2090
    :catchall_0
    move-exception v4

    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    throw v4
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 2067
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    return-object v0
.end method
