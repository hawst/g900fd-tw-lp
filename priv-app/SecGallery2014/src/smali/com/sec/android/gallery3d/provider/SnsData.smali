.class public Lcom/sec/android/gallery3d/provider/SnsData;
.super Ljava/lang/Object;
.source "SnsData.java"


# instance fields
.field public mAccountName:Ljava/lang/String;

.field public mAccountType:Ljava/lang/String;

.field public mResPackage:Ljava/lang/String;

.field public mText:Ljava/lang/String;

.field public mTime:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/SnsData;->mAccountName:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/SnsData;->mAccountType:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/SnsData;->mResPackage:Ljava/lang/String;

    .line 29
    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/SnsData;->mText:Ljava/lang/String;

    .line 30
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/SnsData;->mTime:Ljava/lang/Long;

    .line 31
    return-void
.end method


# virtual methods
.method public setSnsData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0
    .param p1, "account_name"    # Ljava/lang/String;
    .param p2, "account_type"    # Ljava/lang/String;
    .param p3, "res_package"    # Ljava/lang/String;
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "timestamp"    # Ljava/lang/Long;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/gallery3d/provider/SnsData;->mAccountName:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/sec/android/gallery3d/provider/SnsData;->mAccountType:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lcom/sec/android/gallery3d/provider/SnsData;->mResPackage:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/sec/android/gallery3d/provider/SnsData;->mText:Ljava/lang/String;

    .line 39
    iput-object p5, p0, Lcom/sec/android/gallery3d/provider/SnsData;->mTime:Ljava/lang/Long;

    .line 40
    return-void
.end method
