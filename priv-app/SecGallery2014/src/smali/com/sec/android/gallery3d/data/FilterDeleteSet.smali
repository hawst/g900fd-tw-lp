.class public Lcom/sec/android/gallery3d/data/FilterDeleteSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "FilterDeleteSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;,
        Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;
    }
.end annotation


# static fields
.field private static final REQUEST_ADD:I = 0x1

.field private static final REQUEST_CLEAR:I = 0x3

.field private static final REQUEST_REMOVE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "FilterDeleteSet"


# instance fields
.field private final mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mCurrent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;",
            ">;"
        }
    .end annotation
.end field

.field private mRequests:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;",
            ">;"
        }
    .end annotation
.end field

.field private mShowAllGroup:Z

.field private mShowGroupIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mUseBurstShotGrouping:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v2, 0x0

    .line 77
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mRequests:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowGroupIds:Ljava/util/ArrayList;

    .line 74
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowAllGroup:Z

    .line 75
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mUseBurstShotGrouping:Z

    .line 78
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 79
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 80
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mBurstShotGrouping:Z

    if-eqz v0, :cond_0

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mUseBurstShotGrouping:Z

    .line 83
    :cond_0
    return-void
.end method

.method private sendRequest(ILcom/sec/android/gallery3d/data/Path;I)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "indexHint"    # I

    .prologue
    .line 320
    new-instance v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;

    invoke-direct {v0, p1, p2, p3}, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;-><init>(ILcom/sec/android/gallery3d/data/Path;I)V

    .line 321
    .local v0, "r":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mRequests:Ljava/util/ArrayList;

    monitor-enter v2

    .line 322
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mRequests:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->notifyContentChanged()V

    .line 325
    return-void

    .line 323
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public addDeletion(Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "indexHint"    # I

    .prologue
    .line 333
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->sendRequest(ILcom/sec/android/gallery3d/data/Path;I)V

    .line 334
    return-void
.end method

.method public addShowGroupId(Ljava/lang/Long;)Z
    .locals 2
    .param p1, "groupId"    # Ljava/lang/Long;

    .prologue
    const/4 v0, 0x0

    .line 362
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowAllGroup:Z

    if-eqz v1, :cond_1

    .line 369
    :cond_0
    :goto_0
    return v0

    .line 365
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 368
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 369
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public clearDeletion()V
    .locals 3

    .prologue
    .line 341
    const/4 v0, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->sendRequest(ILcom/sec/android/gallery3d/data/Path;I)V

    .line 342
    return-void
.end method

.method public clearShowGroupId()V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 379
    return-void
.end method

.method public getBaseSet()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getIndexOfItemEx(Lcom/sec/android/gallery3d/data/Path;I)I
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "hint"    # I

    .prologue
    .line 400
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getIndexOfItemEx(Lcom/sec/android/gallery3d/data/Path;I)I

    move-result v0

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 13
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    if-gtz p2, :cond_1

    .line 191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 234
    :cond_0
    return-object v0

    .line 193
    :cond_1
    add-int v8, p1, p2

    add-int/lit8 v2, v8, -0x1

    .line 194
    .local v2, "end":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 197
    .local v7, "n":I
    const/4 v3, 0x0

    .line 198
    .local v3, "i":I
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v7, :cond_2

    .line 199
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;

    .line 200
    .local v1, "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    iget v8, v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->index:I

    sub-int/2addr v8, v3

    if-le v8, p1, :cond_7

    .line 203
    .end local v1    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    :cond_2
    move v4, v3

    .line 204
    .local v4, "j":I
    :goto_1
    if-ge v4, v7, :cond_3

    .line 205
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;

    .line 206
    .restart local v1    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    iget v8, v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->index:I

    sub-int/2addr v8, v4

    if-le v8, v2, :cond_8

    .line 215
    .end local v1    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    :cond_3
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mBurstShotGrouping:Z

    if-eqz v8, :cond_a

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mUseBurstShotGrouping:Z

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 216
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    add-int v9, p1, v3

    sub-int v10, v4, v3

    add-int/2addr v10, p2

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowGroupIds:Ljava/util/ArrayList;

    iget-boolean v12, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowAllGroup:Z

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItemForBurstshot(IILjava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 217
    .local v0, "base":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 218
    :cond_4
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->invalidateCacheForBurstshot()V

    .line 219
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    add-int v9, p1, v3

    sub-int v10, v4, v3

    add-int/2addr v10, p2

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowGroupIds:Ljava/util/ArrayList;

    iget-boolean v12, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowAllGroup:Z

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItemForBurstshot(IILjava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 220
    const-string v9, "FilterDeleteSet"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "itemArray size is 0, new size is "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-nez v0, :cond_9

    const/4 v8, 0x0

    :goto_2
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_5
    :goto_3
    add-int/lit8 v6, v4, -0x1

    .local v6, "m":I
    :goto_4
    if-lt v6, v3, :cond_0

    .line 228
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;

    .line 229
    .restart local v1    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    iget v8, v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->index:I

    add-int v9, p1, v3

    sub-int v5, v8, v9

    .line 230
    .local v5, "k":I
    if-eqz v0, :cond_6

    .line 231
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 227
    :cond_6
    add-int/lit8 v6, v6, -0x1

    goto :goto_4

    .line 198
    .end local v0    # "base":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v4    # "j":I
    .end local v5    # "k":I
    .end local v6    # "m":I
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 204
    .restart local v4    # "j":I
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 220
    .end local v1    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    .restart local v0    # "base":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_9
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    goto :goto_2

    .line 223
    .end local v0    # "base":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_a
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    add-int v9, p1, v3

    sub-int v10, v4, v3

    add-int/2addr v10, p2

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    .restart local v0    # "base":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    goto :goto_3
.end method

.method public getMediaItemCount()I
    .locals 4

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 99
    .local v0, "count":I
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mBurstShotGrouping:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mUseBurstShotGrouping:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v1, v1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 100
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowGroupIds:Ljava/util/ArrayList;

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowAllGroup:Z

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItemCountForBurstshot(Ljava/util/ArrayList;Z)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int v0, v1, v2

    .line 101
    if-nez v0, :cond_0

    .line 102
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->invalidateCacheForBurstshot()V

    .line 103
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowGroupIds:Ljava/util/ArrayList;

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowAllGroup:Z

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItemCountForBurstshot(Ljava/util/ArrayList;Z)I

    move-result v0

    .line 104
    const-string v1, "FilterDeleteSet"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "itemArray size is 0, new size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int v0, v1, v2

    goto :goto_0
.end method

.method public getMediaItemEx(II)Ljava/util/ArrayList;
    .locals 13
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    if-gtz p2, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 185
    :cond_0
    return-object v0

    .line 142
    :cond_1
    add-int v8, p1, p2

    add-int/lit8 v2, v8, -0x1

    .line 143
    .local v2, "end":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 146
    .local v7, "n":I
    const/4 v3, 0x0

    .line 147
    .local v3, "i":I
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v7, :cond_2

    .line 148
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;

    .line 149
    .local v1, "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    iget v8, v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->index:I

    sub-int/2addr v8, v3

    if-le v8, p1, :cond_7

    .line 152
    .end local v1    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    :cond_2
    move v4, v3

    .line 153
    .local v4, "j":I
    :goto_1
    if-ge v4, v7, :cond_3

    .line 154
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;

    .line 155
    .restart local v1    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    iget v8, v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->index:I

    sub-int/2addr v8, v4

    if-le v8, v2, :cond_8

    .line 164
    .end local v1    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    :cond_3
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mBurstShotGrouping:Z

    if-eqz v8, :cond_a

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mUseBurstShotGrouping:Z

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 165
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    add-int v9, p1, v3

    sub-int v10, v4, v3

    add-int/2addr v10, p2

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowGroupIds:Ljava/util/ArrayList;

    iget-boolean v12, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowAllGroup:Z

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItemForBurstshot(IILjava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 166
    .local v0, "base":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 167
    :cond_4
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->invalidateCacheForBurstshot()V

    .line 168
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    add-int v9, p1, v3

    sub-int v10, v4, v3

    add-int/2addr v10, p2

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowGroupIds:Ljava/util/ArrayList;

    iget-boolean v12, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowAllGroup:Z

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItemForBurstshot(IILjava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 169
    const-string v9, "FilterDeleteSet"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "itemArray size is 0, new size is "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-nez v0, :cond_9

    const/4 v8, 0x0

    :goto_2
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :cond_5
    :goto_3
    add-int/lit8 v6, v4, -0x1

    .local v6, "m":I
    :goto_4
    if-lt v6, v3, :cond_0

    .line 179
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;

    .line 180
    .restart local v1    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    iget v8, v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->index:I

    add-int v9, p1, v3

    sub-int v5, v8, v9

    .line 181
    .local v5, "k":I
    if-eqz v0, :cond_6

    .line 182
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 178
    :cond_6
    add-int/lit8 v6, v6, -0x1

    goto :goto_4

    .line 147
    .end local v0    # "base":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v4    # "j":I
    .end local v5    # "k":I
    .end local v6    # "m":I
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 153
    .restart local v4    # "j":I
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 169
    .end local v1    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    .restart local v0    # "base":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_9
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    goto :goto_2

    .line 172
    .end local v0    # "base":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_a
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    add-int v9, p1, v3

    sub-int v10, v4, v3

    add-int/2addr v10, p2

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemEx(II)Ljava/util/ArrayList;

    move-result-object v0

    .restart local v0    # "base":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    goto :goto_3
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNumberOfDeletions()I
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getShowGroupId()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowGroupIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    return-object v0
.end method

.method public isCameraRoll()Z
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v0

    return v0
.end method

.method public isShowAllGroup()Z
    .locals 1

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowAllGroup:Z

    return v0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 329
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->notifyContentChanged()V

    .line 330
    return-void
.end method

.method public reload()J
    .locals 23

    .prologue
    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mDataVersion:J

    move-wide/from16 v20, v0

    cmp-long v18, v18, v20

    if-lez v18, :cond_0

    const/4 v13, 0x1

    .line 241
    .local v13, "newData":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mRequests:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    monitor-enter v20

    .line 242
    if-nez v13, :cond_1

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mRequests:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 243
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mDataVersion:J

    move-wide/from16 v18, v0

    monitor-exit v20

    .line 316
    :goto_1
    return-wide v18

    .line 240
    .end local v13    # "newData":Z
    :cond_0
    const/4 v13, 0x0

    goto :goto_0

    .line 245
    .restart local v13    # "newData":Z
    :cond_1
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mRequests:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v6, v0, :cond_6

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mRequests:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;

    .line 247
    .local v15, "r":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;
    iget v0, v15, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;->type:I

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_0

    .line 245
    :cond_2
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 250
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 252
    .local v12, "n":I
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_4
    if-ge v9, v12, :cond_3

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->path:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v18, v0

    iget-object v0, v15, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;->path:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 255
    :cond_3
    if-ne v9, v12, :cond_2

    .line 256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    new-instance v19, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;

    iget-object v0, v15, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;->path:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v21, v0

    iget v0, v15, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;->indexHint:I

    move/from16 v22, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;-><init>(Lcom/sec/android/gallery3d/data/Path;I)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 278
    .end local v6    # "i":I
    .end local v9    # "j":I
    .end local v12    # "n":I
    .end local v15    # "r":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;
    :catchall_0
    move-exception v18

    monitor-exit v20
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v18

    .line 252
    .restart local v6    # "i":I
    .restart local v9    # "j":I
    .restart local v12    # "n":I
    .restart local v15    # "r":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;
    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 262
    .end local v9    # "j":I
    .end local v12    # "n":I
    :pswitch_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 263
    .restart local v12    # "n":I
    const/4 v9, 0x0

    .restart local v9    # "j":I
    :goto_5
    if-ge v9, v12, :cond_2

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->path:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v18, v0

    iget-object v0, v15, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;->path:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 265
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_3

    .line 263
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 272
    .end local v9    # "j":I
    .end local v12    # "n":I
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_3

    .line 277
    .end local v15    # "r":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Request;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mRequests:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    .line 278
    monitor-exit v20
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_c

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;

    move-object/from16 v0, v18

    iget v11, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->index:I

    .line 285
    .local v11, "minIndex":I
    move v10, v11

    .line 286
    .local v10, "maxIndex":I
    const/4 v6, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v6, v0, :cond_7

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;

    .line 288
    .local v4, "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    iget v0, v4, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->index:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-static {v0, v11}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 289
    iget v0, v4, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->index:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-static {v0, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 286
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 292
    .end local v4    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v12

    .line 293
    .restart local v12    # "n":I
    add-int/lit8 v18, v11, -0x5

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 294
    .local v5, "from":I
    add-int/lit8 v18, v10, 0x5

    move/from16 v0, v18

    invoke-static {v0, v12}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 295
    .local v17, "to":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v18, v0

    sub-int v19, v17, v5

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v8

    .line 296
    .local v8, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 297
    .local v16, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;>;"
    const/4 v6, 0x0

    :goto_7
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v6, v0, :cond_b

    .line 298
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 299
    .local v7, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v7, :cond_9

    .line 297
    :cond_8
    :goto_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    .line 300
    :cond_9
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v14

    .line 302
    .local v14, "p":Lcom/sec/android/gallery3d/data/Path;
    const/4 v9, 0x0

    .restart local v9    # "j":I
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v9, v0, :cond_8

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;

    .line 304
    .restart local v4    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    iget-object v0, v4, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->path:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    if-ne v0, v14, :cond_a

    .line 305
    add-int v18, v5, v6

    move/from16 v0, v18

    iput v0, v4, Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;->index:I

    .line 306
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_8

    .line 302
    :cond_a
    add-int/lit8 v9, v9, 0x1

    goto :goto_9

    .line 312
    .end local v4    # "d":Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;
    .end local v7    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v9    # "j":I
    .end local v14    # "p":Lcom/sec/android/gallery3d/data/Path;
    :cond_b
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mCurrent:Ljava/util/ArrayList;

    .line 315
    .end local v5    # "from":I
    .end local v8    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v10    # "maxIndex":I
    .end local v11    # "minIndex":I
    .end local v12    # "n":I
    .end local v16    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/FilterDeleteSet$Deletion;>;"
    .end local v17    # "to":I
    :cond_c
    invoke-static {}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->nextVersionNumber()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mDataVersion:J

    .line 316
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mDataVersion:J

    move-wide/from16 v18, v0

    goto/16 :goto_1

    .line 247
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public removeDeletion(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 337
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->sendRequest(ILcom/sec/android/gallery3d/data/Path;I)V

    .line 338
    return-void
.end method

.method public setShowAllGroup(Z)V
    .locals 0
    .param p1, "showAll"    # Z

    .prologue
    .line 382
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mShowAllGroup:Z

    .line 383
    return-void
.end method

.method public setUseBurstShotGrouping(Z)V
    .locals 0
    .param p1, "useGrouping"    # Z

    .prologue
    .line 390
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->mUseBurstShotGrouping:Z

    .line 391
    return-void
.end method
