.class public Lcom/sec/android/gallery3d/gadget/EasyWidgetService;
.super Landroid/widget/RemoteViewsService;
.source "EasyWidgetService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;,
        Lcom/sec/android/gallery3d/gadget/EasyWidgetService$EmptySource;
    }
.end annotation


# static fields
.field public static final EXTRA_WIDGET_TYPE:Ljava/lang/String; = "widget-type"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/widget/RemoteViewsService;-><init>()V

    .line 87
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onGetViewFactory(Landroid/content/Intent;)Landroid/widget/RemoteViewsService$RemoteViewsFactory;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 53
    const-string v2, "appWidgetId"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 54
    .local v0, "id":I
    const-string/jumbo v2, "widget-type"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 56
    .local v1, "type":I
    new-instance v3, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v2, v0, v1}, Lcom/sec/android/gallery3d/gadget/EasyWidgetService$PhotoRVFactory;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;II)V

    return-object v3
.end method
