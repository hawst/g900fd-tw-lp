.class public Lcom/sec/android/gallery3d/glcore/GlCanvasEx;
.super Ljava/lang/Object;
.source "GlCanvasEx.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    }
.end annotation


# static fields
.field public static final CANVAS_HEIGHT:I = 0x200

.field public static final CANVAS_SIZE_COUNT:I = 0x4

.field public static final CANVAS_WIDTH:I = 0x200


# instance fields
.field private final mConfig:Landroid/graphics/Bitmap$Config;

.field private mInfoSet:[Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvasEx;->mConfig:Landroid/graphics/Bitmap$Config;

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvasEx;->mInfoSet:[Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;

    .line 19
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlCanvasEx;->createCanvas()V

    .line 20
    return-void
.end method

.method private createCanvas()V
    .locals 8

    .prologue
    const/16 v7, 0x200

    const/4 v6, 0x4

    .line 40
    new-array v3, v6, [Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlCanvasEx;->mInfoSet:[Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;

    .line 41
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v6, :cond_0

    .line 42
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlCanvasEx;->mConfig:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v7, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 43
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 44
    .local v1, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlCanvasEx;->mInfoSet:[Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;

    new-instance v4, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;

    const/4 v5, -0x1

    invoke-direct {v4, p0, v5, v1, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;-><init>(Lcom/sec/android/gallery3d/glcore/GlCanvasEx;ILandroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    aput-object v4, v3, v2

    .line 41
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 46
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    :cond_0
    return-void
.end method


# virtual methods
.method public doneAvailable(Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;)V
    .locals 1
    .param p1, "info"    # Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;

    .prologue
    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mDone:Z

    .line 81
    return-void
.end method

.method public finish()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 25
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    .line 26
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlCanvasEx;->mInfoSet:[Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;

    aget-object v1, v2, v0

    .line 27
    .local v1, "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    const/4 v2, -0x1

    iput v2, v1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mTid:I

    .line 28
    iget-object v2, v1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 29
    iput-object v3, v1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mBitmap:Landroid/graphics/Bitmap;

    .line 30
    iput-object v3, v1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mCanvas:Landroid/graphics/Canvas;

    .line 31
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mOnBinding:Z

    .line 25
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33
    .end local v1    # "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    :cond_0
    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlCanvasEx;->mInfoSet:[Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;

    .line 34
    return-void
.end method

.method public getAllocated()Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    .locals 4

    .prologue
    .line 87
    monitor-enter p0

    .line 88
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_1

    .line 89
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlCanvasEx;->mInfoSet:[Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;

    aget-object v1, v2, v0

    .line 90
    .local v1, "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    iget v2, v1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mTid:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mDone:Z

    if-eqz v2, :cond_0

    .line 91
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mOnBinding:Z

    .line 92
    monitor-exit p0

    .line 96
    .end local v1    # "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    :goto_1
    return-object v1

    .line 88
    .restart local v1    # "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    .end local v1    # "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    :cond_1
    monitor-exit p0

    .line 96
    const/4 v1, 0x0

    goto :goto_1

    .line 95
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getAvailable(I)Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    .locals 6
    .param p1, "textureID"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 51
    const/4 v0, -0x1

    .line 53
    .local v0, "emptyIndex":I
    monitor-enter p0

    .line 54
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    const/4 v3, 0x4

    if-ge v1, v3, :cond_0

    .line 55
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlCanvasEx;->mInfoSet:[Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;

    aget-object v2, v3, v1

    .line 56
    .local v2, "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    iget v3, v2, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mTid:I

    if-ne v3, p1, :cond_2

    iget-boolean v3, v2, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mOnBinding:Z

    if-nez v3, :cond_2

    .line 57
    move v0, v1

    .line 64
    .end local v2    # "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    :cond_0
    if-ne v0, v4, :cond_4

    .line 65
    const/4 v2, 0x0

    .line 72
    .restart local v2    # "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    if-eqz v2, :cond_1

    .line 74
    iget-object v3, v2, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mCanvas:Landroid/graphics/Canvas;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v5, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 76
    :cond_1
    return-object v2

    .line 60
    :cond_2
    :try_start_1
    iget v3, v2, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mTid:I

    if-ne v3, v4, :cond_3

    if-ne v0, v4, :cond_3

    .line 61
    move v0, v1

    .line 54
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 67
    .end local v2    # "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlCanvasEx;->mInfoSet:[Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;

    aget-object v2, v3, v0

    .line 68
    .restart local v2    # "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    iput p1, v2, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mTid:I

    .line 69
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mDone:Z

    .line 70
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mOnBinding:Z

    goto :goto_1

    .line 72
    .end local v2    # "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method public releaseAllocated(I)V
    .locals 3
    .param p1, "textureID"    # I

    .prologue
    .line 103
    monitor-enter p0

    .line 104
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    .line 105
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlCanvasEx;->mInfoSet:[Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;

    aget-object v1, v2, v0

    .line 106
    .local v1, "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    iget v2, v1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mTid:I

    if-ne v2, p1, :cond_1

    .line 107
    const/4 v2, -0x1

    iput v2, v1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mTid:I

    .line 108
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mOnBinding:Z

    .line 109
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;->mDone:Z

    .line 113
    .end local v1    # "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    :cond_0
    monitor-exit p0

    .line 114
    return-void

    .line 104
    .restart local v1    # "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    .end local v1    # "info":Lcom/sec/android/gallery3d/glcore/GlCanvasEx$Info;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
