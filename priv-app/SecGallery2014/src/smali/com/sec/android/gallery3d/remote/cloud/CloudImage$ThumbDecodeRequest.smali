.class Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;
.super Ljava/lang/Object;
.source "CloudImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/cloud/CloudImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ThumbDecodeRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final mType:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/cloud/CloudImage;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;I)V
    .locals 0
    .param p2, "type"    # I

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    iput p2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->mType:I

    .line 156
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v11, 0x2

    const/4 v7, 0x0

    const/4 v10, 0x1

    .line 159
    const/4 v3, 0x0

    .line 160
    .local v3, "isImageBroken":Z
    sget-object v6, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;->mBaseImageUri:Landroid/net/Uri;

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    iget-object v8, v8, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v8, v8, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->source_id:Ljava/lang/String;

    invoke-static {v6, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 162
    .local v5, "uri":Landroid/net/Uri;
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v6

    if-eqz v6, :cond_1

    move-object v0, v7

    .line 206
    :cond_0
    :goto_0
    return-object v0

    .line 165
    :cond_1
    invoke-interface {p1, v11}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    move-result v6

    if-nez v6, :cond_2

    move-object v0, v7

    .line 166
    goto :goto_0

    .line 167
    :cond_2
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    # getter for: Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->access$000(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v6, v5}, Lcom/sec/android/cloudagent/CloudStore$API;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 168
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_3

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_pathname:Ljava/lang/String;

    if-eqz v6, :cond_3

    .line 169
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 170
    .local v4, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v6, v4, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 171
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_pathname:Ljava/lang/String;

    const/16 v8, 0x400

    iget v9, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->mType:I

    invoke-static {p1, v6, v4, v8, v9}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodeThumbnail(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 174
    .end local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_3
    invoke-interface {p1, v10}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    move-result v6

    if-nez v6, :cond_4

    move-object v0, v7

    .line 175
    goto :goto_0

    .line 177
    :cond_4
    if-eqz v0, :cond_6

    .line 178
    iget v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->mType:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_5

    .line 179
    iget v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->mType:I

    invoke-static {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v6

    invoke-static {v0, v6, v10}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 181
    :cond_5
    iget v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->mType:I

    if-ne v6, v11, :cond_0

    .line 182
    iget v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->mType:I

    invoke-static {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v6

    invoke-static {v0, v6, v10}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 184
    const/4 v6, 0x0

    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/common/BitmapUtils;->cropThumnailBitmap(Landroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 187
    :cond_6
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->thumb_pathname:Ljava/lang/String;

    if-eqz v6, :cond_8

    .line 188
    const/4 v2, 0x0

    .line 190
    .local v2, "is":Ljava/io/InputStream;
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    # getter for: Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->access$100(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 191
    if-eqz v2, :cond_7

    .line 192
    const/4 v3, 0x1

    .line 193
    iget v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->mType:I

    if-ne v6, v10, :cond_9

    .line 194
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    # getter for: Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v7}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->access$200(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v7

    invoke-interface {v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0200a7

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 201
    :cond_7
    :goto_1
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 204
    .end local v2    # "is":Ljava/io/InputStream;
    :cond_8
    :goto_2
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    invoke-virtual {v6, v3}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->setBroken(Z)V

    goto/16 :goto_0

    .line 196
    .restart local v2    # "is":Ljava/io/InputStream;
    :cond_9
    :try_start_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    # getter for: Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v7}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->access$300(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v7

    invoke-interface {v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenPictureThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 198
    :catch_0
    move-exception v1

    .line 199
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 201
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_2

    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v6

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v6
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 151
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
