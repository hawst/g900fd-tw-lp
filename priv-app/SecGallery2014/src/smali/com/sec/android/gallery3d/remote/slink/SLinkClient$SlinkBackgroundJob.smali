.class Lcom/sec/android/gallery3d/remote/slink/SLinkClient$SlinkBackgroundJob;
.super Landroid/os/AsyncTask;
.source "SLinkClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/slink/SLinkClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SlinkBackgroundJob"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# static fields
.field public static final CHECK_PROCESS:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$SlinkBackgroundJob;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;Lcom/sec/android/gallery3d/remote/slink/SLinkClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/remote/slink/SLinkClient;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/remote/slink/SLinkClient$1;

    .prologue
    .line 268
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$SlinkBackgroundJob;-><init>(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Integer;)Ljava/lang/Long;
    .locals 3
    .param p1, "arg0"    # [Ljava/lang/Integer;

    .prologue
    .line 283
    const-wide/16 v0, 0x0

    .line 284
    .local v0, "nRet":J
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$SlinkBackgroundJob;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->access$400(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isPlatformEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 285
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$SlinkBackgroundJob;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->access$400(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    .line 286
    const-wide/16 v0, 0x1

    .line 288
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 268
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$SlinkBackgroundJob;->doInBackground([Ljava/lang/Integer;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Long;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Long;

    .prologue
    .line 275
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 276
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 277
    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SlinkBackgroundJob: slink wake up ok!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 268
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$SlinkBackgroundJob;->onPostExecute(Ljava/lang/Long;)V

    return-void
.end method
