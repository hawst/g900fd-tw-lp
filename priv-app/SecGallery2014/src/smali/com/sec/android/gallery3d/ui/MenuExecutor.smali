.class public Lcom/sec/android/gallery3d/ui/MenuExecutor;
.super Ljava/lang/Object;
.source "MenuExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/MenuExecutor$MediaOperation;,
        Lcom/sec/android/gallery3d/ui/MenuExecutor$ConfirmDialogListener;,
        Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;
    }
.end annotation


# static fields
.field public static final EXECUTION_RESULT_CANCEL:I = 0x3

.field public static final EXECUTION_RESULT_FAIL:I = 0x2

.field public static final EXECUTION_RESULT_SUCCESS:I = 0x1

.field private static final MSG_DO_SHARE:I = 0x4

.field private static final MSG_TASK_COMPLETE:I = 0x1

.field private static final MSG_TASK_START:I = 0x3

.field private static final MSG_TASK_UPDATE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MenuExecutor"


# instance fields
.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mDialog:Landroid/app/ProgressDialog;

.field private final mHandler:Landroid/os/Handler;

.field private mPaused:Z

.field private final mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private mWaitOnStop:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "selectionManager"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 96
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/SelectionManager;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 97
    new-instance v0, Lcom/sec/android/gallery3d/ui/MenuExecutor$1;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/ui/MenuExecutor$1;-><init>(Lcom/sec/android/gallery3d/ui/MenuExecutor;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mHandler:Landroid/os/Handler;

    .line 137
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/MenuExecutor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/MenuExecutor;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->stopTaskAndDismissDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/MenuExecutor;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/MenuExecutor;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/MenuExecutor;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/MenuExecutor;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/MenuExecutor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/MenuExecutor;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mPaused:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/MenuExecutor;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/MenuExecutor;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/MenuExecutor;ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/MenuExecutor;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->onMenuClicked(ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/MenuExecutor;Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/MenuExecutor;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->onProgressStart(Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/ui/MenuExecutor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;ILcom/sec/android/gallery3d/data/Path;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/MenuExecutor;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p3, "x3"    # I
    .param p4, "x4"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->execute(Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;ILcom/sec/android/gallery3d/data/Path;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ui/MenuExecutor;ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/MenuExecutor;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->onProgressUpdate(ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/ui/MenuExecutor;ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/MenuExecutor;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->onProgressComplete(ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    return-void
.end method

.method private static createProgressDialog(Landroid/content/Context;II)Landroid/app/ProgressDialog;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "titleId"    # I
    .param p2, "progressMax"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 74
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 75
    .local v0, "dialog":Landroid/app/ProgressDialog;
    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 76
    invoke-virtual {v0, p2}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 77
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 78
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 79
    if-le p2, v2, :cond_0

    .line 80
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 82
    :cond_0
    return-object v0
.end method

.method private execute(Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;ILcom/sec/android/gallery3d/data/Path;)Z
    .locals 14
    .param p1, "manager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p2, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p3, "cmd"    # I
    .param p4, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 382
    const/4 v6, 0x1

    .line 383
    .local v6, "result":Z
    const-string v7, "MenuExecutor"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Execute cmd: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " for "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p4

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 386
    .local v8, "startTime":J
    sparse-switch p3, :sswitch_data_0

    .line 424
    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 388
    :sswitch_0
    move-object/from16 v0, p4

    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/data/DataManager;->delete(Lcom/sec/android/gallery3d/data/Path;)V

    .line 426
    :cond_0
    :goto_0
    const-string v7, "MenuExecutor"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "It takes "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long/2addr v12, v8

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ms to execute cmd for "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p4

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    return v6

    .line 391
    :sswitch_1
    const/16 v7, 0x5a

    move-object/from16 v0, p4

    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/data/DataManager;->rotate(Lcom/sec/android/gallery3d/data/Path;I)V

    goto :goto_0

    .line 394
    :sswitch_2
    const/16 v7, -0x5a

    move-object/from16 v0, p4

    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/data/DataManager;->rotate(Lcom/sec/android/gallery3d/data/Path;I)V

    goto :goto_0

    .line 397
    :sswitch_3
    const/4 v5, 0x0

    .line 398
    .local v5, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz p1, :cond_1

    .line 399
    move-object/from16 v0, p4

    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v5

    .line 400
    :cond_1
    if-eqz v5, :cond_0

    .line 401
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaObject;->getCacheFlag()I

    move-result v2

    .line 402
    .local v2, "cacheFlag":I
    const/4 v7, 0x2

    if-ne v2, v7, :cond_2

    .line 403
    const/4 v2, 0x1

    .line 407
    :goto_1
    invoke-virtual {v5, v2}, Lcom/sec/android/gallery3d/data/MediaObject;->cache(I)V

    goto :goto_0

    .line 405
    :cond_2
    const/4 v2, 0x2

    goto :goto_1

    .line 412
    .end local v2    # "cacheFlag":I
    .end local v5    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    :sswitch_4
    const/4 v3, 0x0

    .line 413
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz p1, :cond_3

    .line 414
    move-object/from16 v0, p4

    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v3

    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 415
    .restart local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    const/4 v7, 0x2

    new-array v4, v7, [D

    .line 416
    .local v4, "latlng":[D
    if-eqz v3, :cond_4

    .line 417
    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatLong([D)V

    .line 418
    :cond_4
    const/4 v7, 0x0

    aget-wide v10, v4, v7

    const/4 v7, 0x1

    aget-wide v12, v4, v7

    invoke-static {v10, v11, v12, v13}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 419
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v10, 0x0

    aget-wide v10, v4, v10

    const/4 v12, 0x1

    aget-wide v12, v4, v12

    invoke-static {v7, v10, v11, v12, v13}, Lcom/sec/android/gallery3d/util/GalleryUtils;->showOnMap(Landroid/content/Context;DD)V

    goto :goto_0

    .line 386
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0000 -> :sswitch_3
        0x7f0f026e -> :sswitch_0
        0x7f0f0280 -> :sswitch_2
        0x7f0f0281 -> :sswitch_1
        0x7f0f02af -> :sswitch_4
    .end sparse-switch
.end method

.method private getIntentBySingleSelectedPath(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 232
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 233
    .local v0, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->getSingleSelectedPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 234
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaType(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->getMimeType(I)Ljava/lang/String;

    move-result-object v1

    .line 235
    .local v1, "mimeType":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getContentUri(Lcom/sec/android/gallery3d/data/Path;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    return-object v3
.end method

.method public static getMimeType(I)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 371
    packed-switch p0, :pswitch_data_0

    .line 376
    :pswitch_0
    const-string v0, "*/*"

    :goto_0
    return-object v0

    .line 373
    :pswitch_1
    const-string v0, "image/*"

    goto :goto_0

    .line 375
    :pswitch_2
    const-string/jumbo v0, "video/*"

    goto :goto_0

    .line 371
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private getSingleSelectedPath()Lcom/sec/android/gallery3d/data/Path;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 222
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelected(Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 223
    .local v0, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz v0, :cond_1

    .line 224
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v1, :cond_0

    :goto_0
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 225
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/Path;

    .line 227
    :goto_1
    return-object v1

    :cond_0
    move v1, v2

    .line 224
    goto :goto_0

    .line 227
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private onMenuClicked(ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    .locals 2
    .param p1, "action"    # I
    .param p2, "listener"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .prologue
    .line 239
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->onMenuClicked(ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;ZZ)V

    .line 240
    return-void
.end method

.method private onProgressComplete(ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    .locals 4
    .param p1, "result"    # I
    .param p2, "listener"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 173
    return-void
.end method

.method private onProgressStart(Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 169
    return-void
.end method

.method private onProgressUpdate(ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "listener"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 165
    return-void
.end method

.method private static setMenuItemVisible(Landroid/view/Menu;IZ)V
    .locals 1
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "itemId"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 217
    invoke-interface {p0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 218
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 219
    :cond_0
    return-void
.end method

.method private stopTaskAndDismissDialog()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v0, :cond_2

    .line 141
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mWaitOnStop:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 143
    :cond_1
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    .line 144
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mTask:Lcom/sec/android/gallery3d/util/Future;

    .line 146
    :cond_2
    return-void
.end method

.method public static updateMenuForPanorama(Landroid/view/Menu;ZZ)V
    .locals 2
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "shareAsPanorama360"    # Z
    .param p2, "disablePanorama360Options"    # Z

    .prologue
    const/4 v1, 0x0

    .line 209
    const v0, 0x7f0f02df

    invoke-static {p0, v0, p1}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 210
    if-eqz p2, :cond_0

    .line 211
    const v0, 0x7f0f0280

    invoke-static {p0, v0, v1}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 212
    const v0, 0x7f0f0281

    invoke-static {p0, v0, v1}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 214
    :cond_0
    return-void
.end method

.method public static updateMenuOperation(Landroid/view/Menu;J)V
    .locals 19
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "supported"    # J

    .prologue
    .line 176
    const-wide/16 v14, 0x1

    and-long v14, v14, p1

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_0

    const/4 v4, 0x1

    .line 177
    .local v4, "supportDelete":Z
    :goto_0
    const-wide/16 v14, 0x2

    and-long v14, v14, p1

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_1

    const/4 v9, 0x1

    .line 178
    .local v9, "supportRotate":Z
    :goto_1
    const-wide/16 v14, 0x8

    and-long v14, v14, p1

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_2

    const/4 v3, 0x1

    .line 179
    .local v3, "supportCrop":Z
    :goto_2
    const-wide/16 v14, 0x800

    and-long v14, v14, p1

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_3

    const/4 v13, 0x1

    .line 180
    .local v13, "supportTrim":Z
    :goto_3
    const-wide/32 v14, 0x10000

    and-long v14, v14, p1

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_4

    const/4 v7, 0x1

    .line 181
    .local v7, "supportMute":Z
    :goto_4
    const-wide/16 v14, 0x4

    and-long v14, v14, p1

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_5

    const/4 v11, 0x1

    .line 182
    .local v11, "supportShare":Z
    :goto_5
    const-wide/16 v14, 0x20

    and-long v14, v14, p1

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_6

    const/4 v10, 0x1

    .line 183
    .local v10, "supportSetAs":Z
    :goto_6
    const-wide/16 v14, 0x10

    and-long v14, v14, p1

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_7

    const/4 v12, 0x1

    .line 184
    .local v12, "supportShowOnMap":Z
    :goto_7
    const-wide/16 v14, 0x100

    and-long v14, v14, p1

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_8

    const/4 v2, 0x1

    .line 185
    .local v2, "supportCache":Z
    :goto_8
    const-wide/16 v14, 0x200

    and-long v14, v14, p1

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_9

    const/4 v5, 0x1

    .line 186
    .local v5, "supportEdit":Z
    :goto_9
    const-wide/16 v14, 0x400

    and-long v14, v14, p1

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_a

    const/4 v6, 0x1

    .line 187
    .local v6, "supportInfo":Z
    :goto_a
    const-wide/32 v14, 0x20000

    and-long v14, v14, p1

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_b

    const/4 v8, 0x1

    .line 188
    .local v8, "supportPrint":Z
    :goto_b
    invoke-static {}, Landroid/support/v4/print/PrintHelper;->systemSupportsPrint()Z

    move-result v14

    and-int/2addr v8, v14

    .line 190
    const v14, 0x7f0f026e

    move-object/from16 v0, p0

    invoke-static {v0, v14, v4}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 191
    const v14, 0x7f0f0280

    move-object/from16 v0, p0

    invoke-static {v0, v14, v9}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 192
    const v14, 0x7f0f0281

    move-object/from16 v0, p0

    invoke-static {v0, v14, v9}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 193
    const v14, 0x7f0f02a8

    move-object/from16 v0, p0

    invoke-static {v0, v14, v3}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 194
    const v14, 0x7f0f02e1

    move-object/from16 v0, p0

    invoke-static {v0, v14, v13}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 195
    const v14, 0x7f0f02e2

    move-object/from16 v0, p0

    invoke-static {v0, v14, v7}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 197
    const v14, 0x7f0f02df

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v14, v15}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 198
    const v14, 0x7f0f026c

    move-object/from16 v0, p0

    invoke-static {v0, v14, v11}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 199
    const v14, 0x7f0f029a

    move-object/from16 v0, p0

    invoke-static {v0, v14, v10}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 200
    const v14, 0x7f0f02af

    move-object/from16 v0, p0

    invoke-static {v0, v14, v12}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 201
    const v14, 0x7f0f029c

    move-object/from16 v0, p0

    invoke-static {v0, v14, v5}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 203
    const v14, 0x7f0f029b

    move-object/from16 v0, p0

    invoke-static {v0, v14, v6}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 204
    const v14, 0x7f0f0268

    move-object/from16 v0, p0

    invoke-static {v0, v14, v8}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->setMenuItemVisible(Landroid/view/Menu;IZ)V

    .line 205
    return-void

    .line 176
    .end local v2    # "supportCache":Z
    .end local v3    # "supportCrop":Z
    .end local v4    # "supportDelete":Z
    .end local v5    # "supportEdit":Z
    .end local v6    # "supportInfo":Z
    .end local v7    # "supportMute":Z
    .end local v8    # "supportPrint":Z
    .end local v9    # "supportRotate":Z
    .end local v10    # "supportSetAs":Z
    .end local v11    # "supportShare":Z
    .end local v12    # "supportShowOnMap":Z
    .end local v13    # "supportTrim":Z
    :cond_0
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 177
    .restart local v4    # "supportDelete":Z
    :cond_1
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 178
    .restart local v9    # "supportRotate":Z
    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 179
    .restart local v3    # "supportCrop":Z
    :cond_3
    const/4 v13, 0x0

    goto/16 :goto_3

    .line 180
    .restart local v13    # "supportTrim":Z
    :cond_4
    const/4 v7, 0x0

    goto/16 :goto_4

    .line 181
    .restart local v7    # "supportMute":Z
    :cond_5
    const/4 v11, 0x0

    goto/16 :goto_5

    .line 182
    .restart local v11    # "supportShare":Z
    :cond_6
    const/4 v10, 0x0

    goto/16 :goto_6

    .line 183
    .restart local v10    # "supportSetAs":Z
    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_7

    .line 184
    .restart local v12    # "supportShowOnMap":Z
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_8

    .line 185
    .restart local v2    # "supportCache":Z
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_9

    .line 186
    .restart local v5    # "supportEdit":Z
    :cond_a
    const/4 v6, 0x0

    goto/16 :goto_a

    .line 187
    .restart local v6    # "supportInfo":Z
    :cond_b
    const/4 v8, 0x0

    goto/16 :goto_b
.end method


# virtual methods
.method public destroy()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->stopTaskAndDismissDialog()V

    .line 160
    return-void
.end method

.method public onMenuClicked(ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;ZZ)V
    .locals 8
    .param p1, "action"    # I
    .param p2, "listener"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;
    .param p3, "waitOnStop"    # Z
    .param p4, "showDialog"    # Z

    .prologue
    const/4 v1, 0x1

    .line 245
    sparse-switch p1, :sswitch_data_0

    .line 289
    :goto_0
    :sswitch_0
    return-void

    .line 247
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectAllMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->deSelectAll()V

    goto :goto_0

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->selectAll()V

    goto :goto_0

    .line 259
    :sswitch_2
    const-string v0, "android.intent.action.EDIT"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->getIntentBySingleSelectedPath(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v7

    .line 261
    .local v7, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v1, 0x0

    invoke-static {v7, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 265
    .end local v7    # "intent":Landroid/content/Intent;
    :sswitch_3
    const-string v0, "android.intent.action.ATTACH_DATA"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->getIntentBySingleSelectedPath(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v7

    .line 267
    .restart local v7    # "intent":Landroid/content/Intent;
    const-string v0, "mimeType"

    invoke-virtual {v7}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 268
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 269
    .local v6, "activity":Landroid/app/Activity;
    const v0, 0x7f0e0064

    invoke-virtual {v6, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 274
    .end local v6    # "activity":Landroid/app/Activity;
    .end local v7    # "intent":Landroid/content/Intent;
    :sswitch_4
    const v2, 0x7f0e0041

    .local v2, "title":I
    :goto_1
    move-object v0, p0

    move v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    .line 288
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->startAction(IILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;ZZ)V

    goto :goto_0

    .line 277
    .end local v2    # "title":I
    :sswitch_5
    const v2, 0x7f0e0056

    .line 278
    .restart local v2    # "title":I
    goto :goto_1

    .line 280
    .end local v2    # "title":I
    :sswitch_6
    const v2, 0x7f0e0055

    .line 281
    .restart local v2    # "title":I
    goto :goto_1

    .line 283
    .end local v2    # "title":I
    :sswitch_7
    const v2, 0x7f0e0054

    .line 284
    .restart local v2    # "title":I
    goto :goto_1

    .line 245
    :sswitch_data_0
    .sparse-switch
        0x7f0f0001 -> :sswitch_1
        0x7f0f026e -> :sswitch_4
        0x7f0f0280 -> :sswitch_6
        0x7f0f0281 -> :sswitch_5
        0x7f0f029a -> :sswitch_3
        0x7f0f029c -> :sswitch_2
        0x7f0f02a8 -> :sswitch_0
        0x7f0f02af -> :sswitch_7
    .end sparse-switch
.end method

.method public onMenuClicked(Landroid/view/MenuItem;Ljava/lang/String;Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    .locals 4
    .param p1, "menuItem"    # Landroid/view/MenuItem;
    .param p2, "confirmMsg"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .prologue
    .line 324
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 326
    .local v0, "action":I
    if-eqz p2, :cond_1

    .line 327
    if-eqz p3, :cond_0

    invoke-interface {p3}, Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;->onConfirmDialogShown()V

    .line 328
    :cond_0
    new-instance v1, Lcom/sec/android/gallery3d/ui/MenuExecutor$ConfirmDialogListener;

    invoke-direct {v1, p0, v0, p3}, Lcom/sec/android/gallery3d/ui/MenuExecutor$ConfirmDialogListener;-><init>(Lcom/sec/android/gallery3d/ui/MenuExecutor;ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    .line 329
    .local v1, "cdl":Lcom/sec/android/gallery3d/ui/MenuExecutor$ConfirmDialogListener;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e00db

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e0046

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 338
    .end local v1    # "cdl":Lcom/sec/android/gallery3d/ui/MenuExecutor$ConfirmDialogListener;
    :goto_0
    return-void

    .line 336
    :cond_1
    invoke-direct {p0, v0, p3}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->onMenuClicked(ILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    goto :goto_0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mPaused:Z

    .line 155
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    .line 156
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mPaused:Z

    .line 150
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 151
    :cond_0
    return-void
.end method

.method public startAction(IILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    .locals 6
    .param p1, "action"    # I
    .param p2, "title"    # I
    .param p3, "listener"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .prologue
    .line 341
    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->startAction(IILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;ZZ)V

    .line 342
    return-void
.end method

.method public startAction(IILcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;ZZ)V
    .locals 6
    .param p1, "action"    # I
    .param p2, "title"    # I
    .param p3, "listener"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;
    .param p4, "waitOnStop"    # Z
    .param p5, "showDialog"    # Z

    .prologue
    const/4 v5, 0x0

    .line 346
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelected(Z)Ljava/util/ArrayList;

    move-result-object v1

    .line 347
    .local v1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->stopTaskAndDismissDialog()V

    .line 349
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 350
    .local v0, "activity":Landroid/app/Activity;
    if-eqz p5, :cond_0

    if-eqz v1, :cond_0

    .line 351
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v0, p2, v3}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->createProgressDialog(Landroid/content/Context;II)Landroid/app/ProgressDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    .line 352
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    .line 356
    :goto_0
    new-instance v2, Lcom/sec/android/gallery3d/ui/MenuExecutor$MediaOperation;

    invoke-direct {v2, p0, p1, v1, p3}, Lcom/sec/android/gallery3d/ui/MenuExecutor$MediaOperation;-><init>(Lcom/sec/android/gallery3d/ui/MenuExecutor;ILjava/util/ArrayList;Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    .line 357
    .local v2, "operation":Lcom/sec/android/gallery3d/ui/MenuExecutor$MediaOperation;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getBatchServiceThreadPoolIfAvailable()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v3

    invoke-virtual {v3, v2, v5}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mTask:Lcom/sec/android/gallery3d/util/Future;

    .line 358
    iput-boolean p4, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mWaitOnStop:Z

    .line 359
    return-void

    .line 354
    .end local v2    # "operation":Lcom/sec/android/gallery3d/ui/MenuExecutor$MediaOperation;
    :cond_0
    iput-object v5, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    goto :goto_0
.end method

.method public startAction(IILjava/util/ArrayList;Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    .locals 7
    .param p1, "action"    # I
    .param p2, "title"    # I
    .param p4, "listener"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 472
    .local p3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->startAction(IILjava/util/ArrayList;Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;ZZ)V

    .line 473
    return-void
.end method

.method public startAction(IILjava/util/ArrayList;Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;ZZ)V
    .locals 5
    .param p1, "action"    # I
    .param p2, "title"    # I
    .param p4, "listener"    # Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;
    .param p5, "waitOnStop"    # Z
    .param p6, "showDialog"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .local p3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v4, 0x0

    .line 477
    move-object v1, p3

    .line 478
    .local v1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->stopTaskAndDismissDialog()V

    .line 480
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 481
    .local v0, "activity":Landroid/app/Activity;
    if-eqz p6, :cond_0

    .line 482
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v0, p2, v3}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->createProgressDialog(Landroid/content/Context;II)Landroid/app/ProgressDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    .line 483
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    .line 487
    :goto_0
    new-instance v2, Lcom/sec/android/gallery3d/ui/MenuExecutor$MediaOperation;

    invoke-direct {v2, p0, p1, v1, p4}, Lcom/sec/android/gallery3d/ui/MenuExecutor$MediaOperation;-><init>(Lcom/sec/android/gallery3d/ui/MenuExecutor;ILjava/util/ArrayList;Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    .line 488
    .local v2, "operation":Lcom/sec/android/gallery3d/ui/MenuExecutor$MediaOperation;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getBatchServiceThreadPoolIfAvailable()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v3

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mTask:Lcom/sec/android/gallery3d/util/Future;

    .line 489
    iput-boolean p5, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mWaitOnStop:Z

    .line 490
    return-void

    .line 485
    .end local v2    # "operation":Lcom/sec/android/gallery3d/ui/MenuExecutor$MediaOperation;
    :cond_0
    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    goto :goto_0
.end method

.method public startSingleItemAction(ILcom/sec/android/gallery3d/data/Path;)V
    .locals 4
    .param p1, "action"    # I
    .param p2, "targetPath"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v3, 0x0

    .line 362
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 363
    .local v0, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;

    .line 365
    new-instance v1, Lcom/sec/android/gallery3d/ui/MenuExecutor$MediaOperation;

    invoke-direct {v1, p0, p1, v0, v3}, Lcom/sec/android/gallery3d/ui/MenuExecutor$MediaOperation;-><init>(Lcom/sec/android/gallery3d/ui/MenuExecutor;ILjava/util/ArrayList;Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    .line 366
    .local v1, "operation":Lcom/sec/android/gallery3d/ui/MenuExecutor$MediaOperation;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getBatchServiceThreadPoolIfAvailable()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v2

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mTask:Lcom/sec/android/gallery3d/util/Future;

    .line 367
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor;->mWaitOnStop:Z

    .line 368
    return-void
.end method
