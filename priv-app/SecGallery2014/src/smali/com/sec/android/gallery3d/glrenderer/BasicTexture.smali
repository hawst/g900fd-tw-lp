.class public abstract Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
.super Ljava/lang/Object;
.source "BasicTexture.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glrenderer/Texture;


# static fields
.field private static DEBUG:Z = false

.field private static final MAX_TEXTURE_SIZE:I = 0x1000

.field protected static final STATE_ERROR:I = -0x1

.field protected static final STATE_LOADED:I = 0x1

.field protected static final STATE_UNLOADED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "BasicTexture"

.field protected static final UNSPECIFIED:I = -0x1

.field private static mAllocCount:I

.field private static sInFinalizer:Ljava/lang/ThreadLocal;


# instance fields
.field protected mCanvasRef:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

.field private mHasBorder:Z

.field protected mHeight:I

.field protected mId:I

.field protected mState:I

.field private mTexture:Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

.field protected mTextureHeight:I

.field protected mTextureWidth:I

.field protected mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->sInFinalizer:Ljava/lang/ThreadLocal;

    .line 55
    sput-boolean v1, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->DEBUG:Z

    .line 56
    sput v1, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mAllocCount:I

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;-><init>(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 74
    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 1
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "id"    # I
    .param p3, "state"    # I

    .prologue
    const/4 v0, -0x1

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mId:I

    .line 41
    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mWidth:I

    .line 42
    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mHeight:I

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mCanvasRef:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .line 61
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->setAssociatedCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 62
    iput p2, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mId:I

    .line 63
    iput p3, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mState:I

    .line 68
    iput-object p0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mTexture:Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

    .line 70
    return-void
.end method

.method private freeResource()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 195
    sget-boolean v1, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 196
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mCanvasRef:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mAllocCount:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mAllocCount:I

    .line 197
    :cond_0
    const-string v1, "BasicTexture"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "freeResource = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", canvas = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mCanvasRef:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", state= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", alloc = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mAllocCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mCanvasRef:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .line 201
    .local v0, "canvas":Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mId:I

    if-eq v1, v4, :cond_2

    .line 202
    invoke-interface {v0, p0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->unloadTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)Z

    .line 203
    iput v4, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mId:I

    .line 205
    :cond_2
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mState:I

    .line 206
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->setAssociatedCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 207
    return-void
.end method

.method public static inFinalizer()Z
    .locals 1

    .prologue
    .line 220
    sget-object v0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->sInFinalizer:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getHeight()I

    move-result v5

    move-object v0, p1

    move-object v1, p0

    move v2, p2

    move v3, p3

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIII)V

    .line 160
    return-void
.end method

.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "w"    # I
    .param p5, "h"    # I

    .prologue
    .line 164
    move-object v0, p1

    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIII)V

    .line 165
    return-void
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 211
    sget-object v0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->sInFinalizer:Ljava/lang/ThreadLocal;

    const-class v1, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->recycle()V

    .line 213
    sget-object v0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->sInFinalizer:Ljava/lang/ThreadLocal;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 214
    return-void
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mHeight:I

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mId:I

    return v0
.end method

.method protected abstract getTarget()I
.end method

.method public getTexture()Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mTexture:Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

    return-object v0
.end method

.method public getTextureHeight()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mTextureHeight:I

    return v0
.end method

.method public getTextureWidth()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mTextureWidth:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mWidth:I

    return v0
.end method

.method public hasBorder()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mHasBorder:Z

    return v0
.end method

.method public isFlippedVertically()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public isLoaded()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 175
    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract onBind(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z
.end method

.method public recycle()V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->freeResource()V

    .line 182
    return-void
.end method

.method protected setAssociatedCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 3
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mCanvasRef:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .line 79
    if-eqz p1, :cond_0

    .line 80
    invoke-interface {p1, p0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setAllTextues(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)V

    .line 82
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 83
    if-eqz p1, :cond_1

    sget v0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mAllocCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mAllocCount:I

    .line 84
    :cond_1
    const-string v0, "BasicTexture"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAssociatedCanvas = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", canvas = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", alloc = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mAllocCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_2
    return-void
.end method

.method protected setBorder(Z)V
    .locals 0
    .param p1, "hasBorder"    # Z

    .prologue
    .line 154
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mHasBorder:Z

    .line 155
    return-void
.end method

.method public setSize(II)V
    .locals 5
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/16 v2, 0x1000

    const/4 v1, 0x0

    .line 94
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mWidth:I

    .line 95
    iput p2, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mHeight:I

    .line 98
    if-lez p1, :cond_2

    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->nextPowerOf2(I)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mTextureWidth:I

    .line 99
    if-lez p2, :cond_3

    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->nextPowerOf2(I)I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mTextureHeight:I

    .line 104
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mTextureWidth:I

    if-gt v0, v2, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mTextureHeight:I

    if-le v0, v2, :cond_1

    .line 105
    :cond_0
    const-string v0, "BasicTexture"

    const-string/jumbo v2, "texture is too large: %d x %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mTextureWidth:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x1

    iget v4, p0, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mTextureHeight:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 108
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 98
    goto :goto_0

    :cond_3
    move v0, v1

    .line 99
    goto :goto_1
.end method

.method public yield()V
    .locals 0

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->freeResource()V

    .line 191
    return-void
.end method
